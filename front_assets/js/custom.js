//Smooth scroll start...
	$(function() {
	  $('.scroll_nav a[href*="#"]:not([href="#"])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  if (target.length) {
			$('html, body').animate({
			  scrollTop: target.offset().top-70
			}, 1000);
			return false;
		  }
		}
	  });
	});
	//Smooth scroll end...

$(".slider").click(function(){
	
	//alert($(this).height()+"px and w:"+$(this).width());
});
$(document).ready(function(){
	$("body").append('<div class="top_button_wrap container"><div class="top_button">&#61538;</div></div>');
	$(".top_button").click(function() {
	  $("html, body").animate({ scrollTop: 0 }, "slow");
	  return false;
	});
	if(window.pageYOffset >= 160 && $(window).width()>=768){
			$(".header_wrapper").addClass("scroll_efct");
			$(".fix_fix").css("display", "block");
	} 
	$(document).scroll(function(){
		if(window.pageYOffset >= 160 && $(window).width()>=768){
			$(".header_wrapper").addClass("scroll_efct");
			$(".fix_fix").css("display", "block");
			$(".top_button_wrap").fadeIn(400);
		} else if($(window).width()>=768){
			$(".header_wrapper").removeClass("scroll_efct");
			$(".fix_fix").css("display", "none");
			$(".top_button_wrap").fadeOut(400);
		}
	});
/****************************SELECTION SCRIPT START*******************/

	var li_first = $(".cat_drop ul li:first-child").html();
	$("#catagory").val(li_first);
	$("#catagory").on("click",function(){
		$(".cat_drop").slideToggle(500);
		
	});
	$(".cat_drop ul li").click(function(){
		var list_chart = $(this).html();
		var value = $(this).attr("value");
		$("#catagory").val(list_chart);
		$("#catagory_value").val(value);
		$(".cat_drop").slideToggle(500);
	});

	var li_first2 = $(".loc_drop ul li:first-child").html();
	$("#location").val(li_first2);
	$("#location").on("click",function(){
		$(".loc_drop").slideToggle(500);
	});
	$(".loc_drop ul li").click(function(){
		var list_chart = $(this).html();
		var value = $(this).attr("value");
		$("#location").val(list_chart);
		$("#location_value").val(value);
		$(".loc_drop").slideToggle(500);
	});
	$(document.body).on("click",function(){
		$(".cat_drop").slideUp();
	});

	$("html").on("click",function(){
		$(".loc_drop").slideUp();
	});
	
	$("#catagory").on("click",function(e){
		e.stopPropagation();
		$(".loc_drop").slideUp();
	});
	$("#location").on("click",function(e){
		e.stopPropagation();
		$(".cat_drop").slideUp();
	});

/****************************SELECTION SCRIPT END*******************/


	$(".top_menu_left ul li.link_option").on("click",function(){
		$(this).css("background-color","#392d51");
		$(".top_drop_down").stop().css("background-color","#3d2d5b").slideToggle(300);
	});
	$(window).on("resize",function(){
		if($(window).width()<991){
			$(".top_drop_down").hide();
		}else{
			$(".top_drop_down").show();
		}
		if($(window).width()>991){
			$(".top_drop_down").css("background-color","#433267");
		}
		
	});

	$(window).scroll(startCounter);
	function startCounter() {
		if ($(window).scrollTop() > $(".number_wrapper").offset().top-$(window).height()) {
			$(window).off("scroll", startCounter);
			$('.count').each(function () {
				var $this = $(this);
				jQuery({ Counter: 0 }).animate({ Counter: $this.text() }, {
					duration: 3000,
					easing: 'swing',
					step: function () {
						$this.text(Math.ceil(this.Counter));
					}
				});
			});
		}
	}
	$('.tab_option').click(function(){
		$('.tab_option div').removeClass("tab_option_active");
		$(this).children().addClass("tab_option_active");
		$('.tg_tab').removeClass("tg_tab_show");
		$('#'+$(this).attr("tab-id")).addClass("tg_tab_show");
	});
//Category page ad control start....
	$(window).on('resize load', function(){
		$(".cat_pg_ad1").css("min-height", $(".cat_pg_ad2").height()+"px");
	});
//Category page ad control end....
	
//Slider control start....
	$('.carousel-indicators  li').on('mouseover',function(){
		$(this).trigger('click');
	});
//Slider control end....
	//menu..........
	$(".mobile_menu .slide_dwn").click(function(){
		$(this).toggleClass("arrow_ctrl");
		$(this).children("ul").slideToggle(500);
	});
	$(".menu_switch_new").click(function(){
		$(this).toggleClass("menu_cross");
		$(".mobile_menu_wrapper").toggleClass("width_ctrl");
	});

});