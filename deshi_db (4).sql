-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 01, 2019 at 12:49 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `deshi_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `additional_img`
--

CREATE TABLE `additional_img` (
  `addi_id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `img_file` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addi_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `additional_img`
--

INSERT INTO `additional_img` (`addi_id`, `offer_id`, `img_file`, `addi_date`) VALUES
(1, 1, '723095949', '2018-06-04 06:37:48'),
(2, 2, '2021074090', '2018-06-04 06:53:26'),
(3, 3, '115889835', '2018-06-04 06:58:27'),
(4, 4, '314341266', '2018-06-04 07:04:27'),
(5, 5, '2083065464', '2018-06-04 07:06:39'),
(6, 6, '1147763825', '2018-06-04 08:22:47'),
(7, 7, '1904137431', '2018-06-04 08:27:28'),
(8, 8, '482423405', '2018-06-04 08:30:01'),
(9, 9, '714411987', '2018-06-04 08:32:39'),
(10, 10, '1036183671', '2018-06-04 08:36:15'),
(11, 11, '1809577445', '2018-06-04 08:38:20'),
(12, 12, '1287334488', '2018-06-04 08:39:42'),
(13, 13, '1787887880', '2018-06-04 09:43:33'),
(14, 14, '310107637', '2018-06-04 09:45:40'),
(15, 15, '1034170636', '2018-06-04 09:47:47'),
(16, 16, '938422455', '2018-06-04 10:23:54'),
(17, 17, '1122952757', '2018-06-04 10:25:16'),
(18, 18, '1579368318', '2018-06-04 10:32:27'),
(19, 19, '329612513', '2018-06-04 10:34:55'),
(20, 20, '275229523', '2018-06-04 10:37:49'),
(21, 21, '466707404', '2018-06-04 10:40:41'),
(22, 22, '85764017', '2018-06-04 10:42:13'),
(23, 23, '1582849961', '2018-06-04 12:24:46'),
(24, 24, '1896271077', '2018-06-04 12:28:01'),
(25, 25, '1732993285', '2018-06-04 12:34:01'),
(26, 26, '1693819294', '2018-06-04 12:36:23'),
(27, 27, '1043906328', '2018-06-05 00:26:55'),
(28, 28, '1776405099', '2018-06-05 00:29:50'),
(29, 29, '1614668621', '2018-06-05 00:32:38'),
(30, 30, '576612970', '2018-06-05 00:34:51'),
(31, 31, '1590842188', '2018-06-05 01:54:09'),
(32, 32, '435100927', '2018-06-07 00:08:42'),
(33, 33, '2137799462', '2018-06-07 00:57:16'),
(34, 34, '475453921', '2018-06-07 04:29:27'),
(35, 35, '98364410', '2018-06-07 04:41:54'),
(36, 36, '1494361083', '2018-06-08 04:02:53'),
(37, 37, '2018354912', '2018-06-08 04:08:07'),
(38, 38, '1316680312', '2018-06-08 04:19:04'),
(39, 39, '1838017539', '2018-06-08 04:24:56'),
(40, 40, '170627227', '2018-06-14 00:44:14'),
(41, 41, '1877876808', '2018-06-14 00:45:51'),
(42, 42, '598137997hcfinal_logi.png', '2018-07-09 05:49:09'),
(43, 43, '737656138', '2018-07-10 00:56:05'),
(44, 44, '1841361236', '2018-07-21 11:17:45'),
(45, 45, '25625909', '2018-07-22 02:32:29');

-- --------------------------------------------------------

--
-- Table structure for table `advertise`
--

CREATE TABLE `advertise` (
  `id` int(11) NOT NULL,
  `advertise_position` int(11) NOT NULL DEFAULT '1',
  `advertise_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `advertise`
--

INSERT INTO `advertise` (`id`, `advertise_position`, `advertise_image`, `status`, `updated_at`) VALUES
(1, 1, '1353362083ed.jpg', 1, '2018-07-09 23:30:54');

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `area_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `area_title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_title_bangla` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_st` int(1) NOT NULL DEFAULT '1',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`area_id`, `division_id`, `district_id`, `area_title`, `area_title_bangla`, `description`, `area_st`, `add_date`) VALUES
(1, 2, 43, 'Muradpur', 'মুরাদপুর', NULL, 1, '2018-08-30 04:06:17'),
(2, 2, 43, '2 no Gate', '২ নং গেইট', NULL, 1, '2018-07-28 09:18:51'),
(3, 3, 1, 'Mirpur 1', 'মীরপুর ১', NULL, 1, '2018-07-28 09:19:14');

-- --------------------------------------------------------

--
-- Table structure for table `catagory`
--

CREATE TABLE `catagory` (
  `catagory_id` int(11) NOT NULL,
  `catagory_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `catagory_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_by` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `menu_st` int(11) NOT NULL DEFAULT '1',
  `add_date` datetime NOT NULL,
  `catagory_pic` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top_menu_st` int(1) NOT NULL DEFAULT '0',
  `featured_cat_st` int(1) NOT NULL DEFAULT '0',
  `front_page_show_st` int(1) NOT NULL DEFAULT '0',
  `total_ad` int(11) NOT NULL DEFAULT '0',
  `top_menu_sl` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `catagory`
--

INSERT INTO `catagory` (`catagory_id`, `catagory_title`, `catagory_description`, `add_by`, `menu_st`, `add_date`, `catagory_pic`, `top_menu_st`, `featured_cat_st`, `front_page_show_st`, `total_ad`, `top_menu_sl`) VALUES
(1, 'Fashion & Lifestyle', '', '1', 1, '2018-06-04 05:00:45', '1564221249fashion.jpg', 1, 1, 1, 1, 0),
(2, 'Mobile & Gadget', '', '1', 1, '2018-06-04 05:02:40', '1047714492mobile.jpg', 1, 1, 1, 2, 2),
(3, 'Food & Dine Offers', '', '1', 1, '2018-06-04 05:03:16', '880601228food.jpg', 1, 1, 1, 3, 1),
(4, 'Home Appliance', '', '1', 1, '2018-06-04 05:04:53', '681334306home.jpg', 1, 1, 0, 4, 3),
(5, 'Health & Beauty', '', '1', 0, '2018-06-04 05:06:01', '1228356100health.jpg', 0, 0, 1, 5, 2),
(6, 'Gift & Jewellery', '', '1', 1, '2018-06-04 05:07:41', '1446370173jewelry.jpg', 1, 0, 0, 10, 5),
(7, 'Telecom Operator', '', '1', 2, '2018-06-04 05:12:27', '2054983879telecom.jpg', 0, 0, 0, 11, 0),
(8, 'Grocery & Living', '', '1', 1, '2018-06-04 05:14:18', '217377920grocery.jpg', 1, 1, 1, 5, 4),
(9, 'Tours & Travels', '', '1', 1, '2018-06-04 05:16:00', '930472554travel.jpg', 0, 0, 0, 9, 0),
(12, 'Furniture', '', '1', 1, '2018-06-11 11:30:23', '7961489furniture.jpg', 0, 0, 0, 0, 0),
(13, 'Ecommerce', '', '1', 1, '2018-06-11 11:31:58', '133668265ecommerce.jpg', 0, 0, 0, 0, 0),
(14, 'Web Hosting & Domain', '', '1', 1, '2018-06-11 11:34:04', '1876297719web.jpg', 0, 0, 0, 0, 0),
(15, 'Computer & Laptop', '', '1', 1, '2018-06-11 11:34:24', '1464049192computer.jpg', 1, 0, 0, 0, 5),
(16, 'Property & Development', '', '1', 1, '2018-06-11 11:34:46', '1796563885property.jpg', 0, 0, 0, 0, 0),
(17, 'Training & Education', '', '1', 1, '2018-06-12 05:03:33', '1046667263education.jpg', 0, 0, 0, 0, 0),
(18, 'Telecom Operator', NULL, '1', 0, '2018-08-13 05:18:18', 'offer_18.jpg', 0, 0, 0, 0, 15),
(21, 'Amer category', NULL, '1', 0, '2018-08-13 14:27:11', 'offer_21.jpg', 0, 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `main_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8_unicode_ci,
  `fb_link` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_link` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linked_link` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about_us` text COLLATE utf8_unicode_ci,
  `about_us_image` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `privacy_policy` text COLLATE utf8_unicode_ci,
  `terms_condition` text COLLATE utf8_unicode_ci,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `login_id`, `name`, `logo`, `website`, `address`, `phone`, `email`, `main_image`, `details`, `fb_link`, `twitter_link`, `google_link`, `linked_link`, `about_us`, `about_us_image`, `privacy_policy`, `terms_condition`, `created_at`, `updated_at`) VALUES
(1, 1, 'Demo', 'logo.png', 'http://demo.com/', '21/a hasina manjil, east nasirabad, chittagong , Bangladesh', '23424adf', 'support@demo.com', NULL, '', 'https://www.facebook.com/rcreationbd/', 'https://www.twitter.com/rcreationbd/', 'https://www.google.com/rcreationbd/', 'https://www.linked.com/rcreationbd/', ' \r\n                                                    \r\n                                                    \r\n                                                    \r\n                                                   <div style=\"margin: 0px 14.3906px 0px 28.7969px; padding: 0px; width: 436.797px; float: left; font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\"><h2 style=\"margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px;\">What is Lorem Ipsum?</h2><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify;\"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify;\"><span style=\"color: inherit; font-family: DauphinPlain; font-size: 24px;\">Why do we use it?</span></p><blockquote style=\"margin-bottom: 15px; padding: 0px; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, cevolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</blockquote></div>                                                                                                                                                                                                            ', 'about_us_image.png', NULL, NULL, '2017-05-30 00:00:00', '2018-05-08 01:08:52');

-- --------------------------------------------------------

--
-- Table structure for table `corporate`
--

CREATE TABLE `corporate` (
  `corporate_id` int(11) NOT NULL,
  `corporate_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `add_date` datetime NOT NULL,
  `add_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ctype_colour`
--

CREATE TABLE `ctype_colour` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fe_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name_ben` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `table_name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `string_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ctype_colour`
--

INSERT INTO `ctype_colour` (`id`, `name`, `color_code`, `fe_image`, `add_date`, `name_ben`, `table_name`, `string_name`) VALUES
(1, 'Voucher', '#45437A', '', '2018-08-15 06:26:47', '', 'voucher', 'Get Voucher'),
(2, 'get_code', '#FF5856', '', '2018-07-09 12:51:01', '', 'utm_ofr', 'Get Coupon'),
(3, 'get_sms', '#ff5252', '', '2018-07-09 12:51:01', '', 'offer_sms', 'Get SMS'),
(4, 'online_deal', '#B31C98', '', '2018-07-09 12:51:01', '', 'online_deal', 'Check Sale'),
(5, 'activate_deal', '#6BD3E0', '', '2018-07-09 12:51:01', '', 'utm_ofr', 'Promotional'),
(6, 'direct_sale', '#4517F3', '', '2018-08-15 06:26:38', '', 'direct_sale', 'Buy'),
(7, 'custom_link', '#33d9b2', '', '2018-07-09 12:51:01', '', 'custom_ofr', 'Link');

-- --------------------------------------------------------

--
-- Table structure for table `custom_ofr`
--

CREATE TABLE `custom_ofr` (
  `custom_ofr` int(11) NOT NULL,
  `offer_id` bigint(20) NOT NULL,
  `offer_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_values` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_add_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `direct_sale`
--

CREATE TABLE `direct_sale` (
  `sale_id` int(11) NOT NULL,
  `offer_id` bigint(20) DEFAULT '0',
  `sale_price` double NOT NULL DEFAULT '0',
  `add_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(3) UNSIGNED NOT NULL,
  `division_id` int(2) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `bn_name` varchar(50) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `website` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `division_id`, `name`, `bn_name`, `lat`, `lon`, `website`) VALUES
(1, 3, 'Dhaka', 'ঢাকা', 23.7115253, 90.4111451, 'www.dhaka.gov.bd'),
(2, 3, 'Faridpur', 'ফরিদপুর', 23.6070822, 89.8429406, 'www.faridpur.gov.bd'),
(3, 3, 'Gazipur', 'গাজীপুর', 24.0022858, 90.4264283, 'www.gazipur.gov.bd'),
(4, 3, 'Gopalganj', 'গোপালগঞ্জ', 23.0050857, 89.8266059, 'www.gopalganj.gov.bd'),
(5, 3, 'Jamalpur', 'জামালপুর', 24.937533, 89.937775, 'www.jamalpur.gov.bd'),
(6, 3, 'Kishoreganj', 'কিশোরগঞ্জ', 24.444937, 90.776575, 'www.kishoreganj.gov.bd'),
(7, 3, 'Madaripur', 'মাদারীপুর', 23.164102, 90.1896805, 'www.madaripur.gov.bd'),
(8, 3, 'Manikganj', 'মানিকগঞ্জ', 0, 0, 'www.manikganj.gov.bd'),
(9, 3, 'Munshiganj', 'মুন্সিগঞ্জ', 0, 0, 'www.munshiganj.gov.bd'),
(10, 3, 'Mymensingh', 'ময়মনসিং', 0, 0, 'www.mymensingh.gov.bd'),
(11, 3, 'Narayanganj', 'নারায়াণগঞ্জ', 23.63366, 90.496482, 'www.narayanganj.gov.bd'),
(12, 3, 'Narsingdi', 'নরসিংদী', 23.932233, 90.71541, 'www.narsingdi.gov.bd'),
(13, 3, 'Netrokona', 'নেত্রকোনা', 24.870955, 90.727887, 'www.netrokona.gov.bd'),
(14, 3, 'Rajbari', 'রাজবাড়ি', 23.7574305, 89.6444665, 'www.rajbari.gov.bd'),
(15, 3, 'Shariatpur', 'শরীয়তপুর', 0, 0, 'www.shariatpur.gov.bd'),
(16, 3, 'Sherpur', 'শেরপুর', 25.0204933, 90.0152966, 'www.sherpur.gov.bd'),
(17, 3, 'Tangail', 'টাঙ্গাইল', 0, 0, 'www.tangail.gov.bd'),
(18, 5, 'Bogra', 'বগুড়া', 24.8465228, 89.377755, 'www.bogra.gov.bd'),
(19, 5, 'Joypurhat', 'জয়পুরহাট', 0, 0, 'www.joypurhat.gov.bd'),
(20, 5, 'Naogaon', 'নওগাঁ', 0, 0, 'www.naogaon.gov.bd'),
(21, 5, 'Natore', 'নাটোর', 24.420556, 89.000282, 'www.natore.gov.bd'),
(22, 5, 'Nawabganj', 'নবাবগঞ্জ', 24.5965034, 88.2775122, 'www.chapainawabganj.gov.bd'),
(23, 5, 'Pabna', 'পাবনা', 23.998524, 89.233645, 'www.pabna.gov.bd'),
(24, 5, 'Rajshahi', 'রাজশাহী', 0, 0, 'www.rajshahi.gov.bd'),
(25, 5, 'Sirajgonj', 'সিরাজগঞ্জ', 24.4533978, 89.7006815, 'www.sirajganj.gov.bd'),
(26, 6, 'Dinajpur', 'দিনাজপুর', 25.6217061, 88.6354504, 'www.dinajpur.gov.bd'),
(27, 6, 'Gaibandha', 'গাইবান্ধা', 25.328751, 89.528088, 'www.gaibandha.gov.bd'),
(28, 6, 'Kurigram', 'কুড়িগ্রাম', 25.805445, 89.636174, 'www.kurigram.gov.bd'),
(29, 6, 'Lalmonirhat', 'লালমনিরহাট', 0, 0, 'www.lalmonirhat.gov.bd'),
(30, 6, 'Nilphamari', 'নীলফামারী', 25.931794, 88.856006, 'www.nilphamari.gov.bd'),
(31, 6, 'Panchagarh', 'পঞ্চগড়', 26.3411, 88.5541606, 'www.panchagarh.gov.bd'),
(32, 6, 'Rangpur', 'রংপুর', 25.7558096, 89.244462, 'www.rangpur.gov.bd'),
(33, 6, 'Thakurgaon', 'ঠাকুরগাঁও', 26.0336945, 88.4616834, 'www.thakurgaon.gov.bd'),
(34, 1, 'Barguna', 'বরগুনা', 0, 0, 'www.barguna.gov.bd'),
(35, 1, 'Barisal', 'বরিশাল', 0, 0, 'www.barisal.gov.bd'),
(36, 1, 'Bhola', 'ভোলা', 22.685923, 90.648179, 'www.bhola.gov.bd'),
(37, 1, 'Jhalokati', 'ঝালকাঠি', 0, 0, 'www.jhalakathi.gov.bd'),
(38, 1, 'Patuakhali', 'পটুয়াখালী', 22.3596316, 90.3298712, 'www.patuakhali.gov.bd'),
(39, 1, 'Pirojpur', 'পিরোজপুর', 0, 0, 'www.pirojpur.gov.bd'),
(40, 2, 'Bandarban', 'বান্দরবান', 22.1953275, 92.2183773, 'www.bandarban.gov.bd'),
(41, 2, 'Brahmanbaria', 'ব্রাহ্মণবাড়িয়া', 23.9570904, 91.1119286, 'www.brahmanbaria.gov.bd'),
(42, 2, 'Chandpur', 'চাঁদপুর', 23.2332585, 90.6712912, 'www.chandpur.gov.bd'),
(43, 2, 'Chittagong', 'চট্টগ্রাম', 22.335109, 91.834073, 'www.chittagong.gov.bd'),
(44, 2, 'Comilla', 'কুমিল্লা', 23.4682747, 91.1788135, 'www.comilla.gov.bd'),
(45, 2, 'Cox\'s Bazar', 'কক্স বাজার', 0, 0, 'www.coxsbazar.gov.bd'),
(46, 2, 'Feni', 'ফেনী', 23.023231, 91.3840844, 'www.feni.gov.bd'),
(47, 2, 'Khagrachari', 'খাগড়াছড়ি', 23.119285, 91.984663, 'www.khagrachhari.gov.bd'),
(48, 2, 'Lakshmipur', 'লক্ষ্মীপুর', 22.942477, 90.841184, 'www.lakshmipur.gov.bd'),
(49, 2, 'Noakhali', 'নোয়াখালী', 22.869563, 91.099398, 'www.noakhali.gov.bd'),
(50, 2, 'Rangamati', 'রাঙ্গামাটি', 0, 0, 'www.rangamati.gov.bd'),
(51, 7, 'Habiganj', 'হবিগঞ্জ', 24.374945, 91.41553, 'www.habiganj.gov.bd'),
(52, 7, 'Maulvibazar', 'মৌলভীবাজার', 24.482934, 91.777417, 'www.moulvibazar.gov.bd'),
(53, 7, 'Sunamganj', 'সুনামগঞ্জ', 25.0658042, 91.3950115, 'www.sunamganj.gov.bd'),
(54, 7, 'Sylhet', 'সিলেট', 24.8897956, 91.8697894, 'www.sylhet.gov.bd'),
(55, 4, 'Bagerhat', 'বাগেরহাট', 22.651568, 89.785938, 'www.bagerhat.gov.bd'),
(56, 4, 'Chuadanga', 'চুয়াডাঙ্গা', 23.6401961, 88.841841, 'www.chuadanga.gov.bd'),
(57, 4, 'Jessore', 'যশোর', 23.16643, 89.2081126, 'www.jessore.gov.bd'),
(58, 4, 'Jhenaidah', 'ঝিনাইদহ', 23.5448176, 89.1539213, 'www.jhenaidah.gov.bd'),
(59, 4, 'Khulna', 'খুলনা', 22.815774, 89.568679, 'www.khulna.gov.bd'),
(60, 4, 'Kushtia', 'কুষ্টিয়া', 23.901258, 89.120482, 'www.kushtia.gov.bd'),
(61, 4, 'Magura', 'মাগুরা', 23.487337, 89.419956, 'www.magura.gov.bd'),
(62, 4, 'Meherpur', 'মেহেরপুর', 23.762213, 88.631821, 'www.meherpur.gov.bd'),
(63, 4, 'Narail', 'নড়াইল', 23.172534, 89.512672, 'www.narail.gov.bd'),
(64, 4, 'Satkhira', 'সাতক্ষীরা', 0, 0, 'www.satkhira.gov.bd'),
(65, 5, 'sdcscdscds', 'csdcsdc', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` int(2) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `bn_name` varchar(50) NOT NULL,
  `position` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `name`, `bn_name`, `position`) VALUES
(1, 'Barisal', 'বরিশাল', 6),
(2, 'Chittagong', 'চট্টগ্রাম', 2),
(3, 'Dhaka', 'ঢাকা', 1),
(4, 'Khulna', 'খুলনা', 4),
(5, 'Rajshahi', 'রাজশাহী', 3),
(6, 'Rangpur', 'রংপুর', 7),
(7, 'Sylhet', 'সিলেট', 5);

-- --------------------------------------------------------

--
-- Table structure for table `main_offer`
--

CREATE TABLE `main_offer` (
  `offer_id` int(11) NOT NULL,
  `post_by` int(11) NOT NULL,
  `offer_from` int(11) NOT NULL,
  `offer_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_title_type` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `of_post_date` date NOT NULL,
  `offer_desc` text COLLATE utf8_unicode_ci,
  `offer_terms` text COLLATE utf8_unicode_ci,
  `offer_featured_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ctype` int(11) NOT NULL DEFAULT '0',
  `catagories` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tags` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sdate` date DEFAULT NULL,
  `edate` date DEFAULT NULL,
  `show_expire_date` date NOT NULL,
  `climitamnt` int(11) NOT NULL DEFAULT '0',
  `climit` int(1) NOT NULL DEFAULT '0',
  `target_people` int(200) DEFAULT NULL,
  `cdate` datetime NOT NULL,
  `cdevice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cbrowser` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_store_type` int(1) NOT NULL DEFAULT '0',
  `discount` double NOT NULL DEFAULT '0',
  `inner_discount` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_seen` int(11) NOT NULL DEFAULT '0',
  `total_like` int(11) NOT NULL DEFAULT '0',
  `total_rating` double NOT NULL DEFAULT '0',
  `rated_user_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_hot` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `offer_tag` int(15) DEFAULT NULL COMMENT '1=Deal of the day,2=Highlighted Deal,3=Both'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `main_offer`
--

INSERT INTO `main_offer` (`offer_id`, `post_by`, `offer_from`, `offer_title`, `profile_name`, `offer_title_type`, `of_post_date`, `offer_desc`, `offer_terms`, `offer_featured_file`, `ctype`, `catagories`, `tags`, `sdate`, `edate`, `show_expire_date`, `climitamnt`, `climit`, `target_people`, `cdate`, `cdevice`, `cip`, `cbrowser`, `offer_store_type`, `discount`, `inner_discount`, `total_seen`, `total_like`, `total_rating`, `rated_user_id`, `is_hot`, `status`, `updated_at`, `offer_tag`) VALUES
(1, 1, 2, 'OnePlus 5T - Smartphone Get 10% OFF', 'OnePlus_5T_-_Smartphone_Get_10%_OFF_1', '6', '2018-06-04', '<h2>OnePlus 5T - Smartphone - 6.01&quot; - 8GB RAM - 128GB ROM - 20MP Camera - Midnight Black</h2>\r\n\r\n<p>Key Features</p>\r\n\r\n<ul>\r\n	<li>6.01&quot; 1080P AMOLED Display</li>\r\n	<li>Qualcomm Snapdragon 835</li>\r\n	<li>Octa-core 10nm up to 2.45GHz Processor</li>\r\n	<li>GPU: Adreno 540</li>\r\n	<li>8GB RAM and 128GB ROM</li>\r\n	<li>16MP+20MP Dual Rear Camera</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '', '1444287941oneplust.jpg', 5, '2', 'mobile, oneplus,', '2018-06-04', '2018-06-04', '2018-06-04', 0, 0, 1, '2018-08-03 07:32:37', '', '', '', 1, 10, '10% OFF', 46, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(2, 1, 9, 'Gucci Ladies Replica Watch, Gucci High copy womens wrist watch', 'Gucci_Ladies_Replica_Watch_Gucci_High_copy_womens_wrist_watch_2', '6', '2018-06-04', '<h1>Gucci Ladies Replica Watch, Gucci High copy womens wrist watch</h1>\r\n\r\n<p>Gucci Ladies Replica Watch, female or girls to wear a new branded Gucci replica/copy wrist watch. This watch looks great for fashion conscious ladies. Shop it with guaranteed color sustainability. Features: Quartz, decorated with high quality stone, simple crown to adjust time, stainless steel band. &ldquo;Gucci&rdquo; Written on the white Dial.</p>\r\n\r\n<h2><strong>Useful Link for Shopping more from&nbsp;<a href=\"https://ankur.com.bd/\">Ankur</a>&nbsp;&hellip;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</strong></h2>\r\n\r\n<p><strong><a href=\"https://www.facebook.com/ankur.com.bd/\">Our Facebook Page</a></strong></p>\r\n\r\n<ul>\r\n	<li><a href=\"https://ankur.com.bd/\">Online Shopping in Bangladesh</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/\">Watches in BD</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/health-beauty-products/skin-care/pakistani-night-cream/\">Pakistani Night Cream</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/\">Replica Watches in BD</a></li>\r\n	<li><a href=\"https://ankur.com.bd/\">Ã Â¦â€¦Ã Â¦Â¨Ã Â¦Â²Ã Â¦Â¾Ã Â¦â€¡Ã Â¦Â¨ Ã Â¦Â¶Ã Â¦ÂªÃ Â¦Â¿Ã Â¦â€š Ã Â¦Â¬Ã Â¦Â¾Ã Â¦â€šÃ Â¦Â²Ã Â¦Â¾Ã Â¦Â¦Ã Â§â€¡Ã Â¦Â¶</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/health-beauty-products/\">Beauty &amp; Health Products</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/leather-products/\">Leather Wallet BD</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/curren-watches/\">Curren Watches in BD</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/mens-watches/\">Mens Watches in BD</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/rolex-replica-watch/\">Rolex Replica Watch</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/ladies-watches/\">Ladies/Women Watches</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/naviforce-watch/\">Naviforce Watches in Bangladesh</a></li>\r\n</ul>\r\n\r\n<p>Old Replica Gucci ladies or womens bangle diamond watch store or showroom cheapest online prices list on sale Gucci bd.</p>\r\n', '', '1700832284replica.jpg', 4, '1', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 06:53:26', '', '', '', 1, 0, 'Replica Watch', 264, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(3, 1, 9, 'Tissot Watch or Tissot Replica Wrist Watch', 'Tissot_Watch_or_Tissot_Replica_Wrist_Watch_3', '3', '2018-06-04', '<h2><strong>Tissot Watch or Tissot Replica Wrist Watch</strong></h2>\r\n\r\n<p>This is a new Tissot Watch, copy or replica hand watch but really a master copy to wear. No one can even guess it&rsquo;s authenticity or originality. Features: 3 hands, automatic date, stylish crown, Roman numeric number, double lock band of stainless steel&nbsp;&amp;&nbsp;heavy-type wrist watch for watch lovers. Grab the future with this Ankur product.</p>\r\n\r\n<p><strong>Useful Link for Shopping more from&nbsp;<a href=\"https://ankur.com.bd/\">Ankur</a>&nbsp;&hellip;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</strong></p>\r\n\r\n<p><strong><a href=\"https://www.facebook.com/ankur.com.bd/\">Our Facebook Page</a></strong></p>\r\n\r\n<ul>\r\n	<li><a href=\"https://ankur.com.bd/\">Online Shopping in Bangladesh</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/\">Watches in BD</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/health-beauty-products/skin-care/pakistani-night-cream/\">Pakistani Night Cream</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/\">Replica Watches in BD</a></li>\r\n	<li><a href=\"https://ankur.com.bd/\">Ã Â¦â€¦Ã Â¦Â¨Ã Â¦Â²Ã Â¦Â¾Ã Â¦â€¡Ã Â¦Â¨ Ã Â¦Â¶Ã Â¦ÂªÃ Â¦Â¿Ã Â¦â€š Ã Â¦Â¬Ã Â¦Â¾Ã Â¦â€šÃ Â¦Â²Ã Â¦Â¾Ã Â¦Â¦Ã Â§â€¡Ã Â¦Â¶</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/health-beauty-products/\">Beauty &amp; Health Products</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/leather-products/\">Leather Wallet BD</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/curren-watches/\">Curren Watches in BD</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/mens-watches/\">Mens Watches in BD</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/rolex-replica-watch/\">Rolex Replica Watch</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/ladies-watches/\">Ladies/Women Watches</a></li>\r\n	<li><a href=\"https://ankur.com.bd/product-category/watches/naviforce-watch/\">Naviforce Watches in Bangladesh</a></li>\r\n</ul>\r\n', '', '851441722re.jpg', 4, '1', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 06:58:27', '', '', '', 1, 0, 'Replica Wrist', 1, 0, 0, '', 1, 1, '2019-05-08 04:36:11', NULL),
(5, 1, 10, '100 Tk Discount on Purchase Over 400 Tk at SILKYGIRL', '100_Tk_Discount_on_Purchase_Over_400_Tk_at_SILKYGIRL_5', '1', '2018-06-04', '<p><strong>Save 100Tk Flat. Enter code at checkout: &#39;SILKY100&#39;<br />\r\nClick:&nbsp;<a href=\"https://www.silkygirl.com.bd/product-category/jewellery-shop/\">Shop</a><br />\r\nFb Page:&nbsp;<a href=\"https://www.facebook.com/SilkygirlBD/\">Here</a></strong></p>\r\n\r\n<ul>\r\n	<li>Click on the &ldquo;Get Code&rdquo; button and a Pop-up window will appear with the code already there for you to copy. By clicking the &ldquo;Copy&rdquo; button, our server will automatically copy the code for you and open the merchant&rsquo;s website in a separate tab from whom you want to purchase your product. During the completion of the checkout process, you will paste the code in their &ldquo;Promotional Code&rdquo; section and you will get your discount on the price of your chosen item.</li>\r\n</ul>\r\n', '', '1858900528silky girl.png', 2, '1', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 07:06:39', '', '', '', 1, 0, '100tk OFF', 191, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(6, 1, 11, 'Digital Sensor Speaker', 'Digital_Sensor_Speaker_6', '1', '2018-06-04', '<p>This product is an induction speaker, amplified electromagnetic induction, no need to connect a Bluetooth!</p>\r\n\r\n<p>Simply phone, tablet, placed on the mutual inductance speaker can amplify the sound. In operation, the phone must be placed on the speakers.With Aux interface can also be connected to a computer using a cell phone, audio cable with their own needs.Currently, in addition to for Huawei glory 6 Plus, millet note, millet 4, the product is compatible with most brands have external speakers tablet series.</p>\r\n\r\n<p>Power supply:</p>\r\n', '', '1833509417016950842adff986b6bf972c31f936ff-550x300.jpg', 4, '2', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 08:22:47', '', '', '', 1, 0, 'Auto Sensor', 176, 1, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(7, 1, 12, 'EUROASIA - 15% Discount on all Item for GP STAR Customers.', 'EUROASIA_-_15%_Discount_on_all_Item_for_GP_STAR_Customers._7', '3', '2018-06-04', '<p>Terms and Condition</p>\r\n\r\n<ul>\r\n	<li><strong>Products</strong>:&nbsp;Mattress &amp; Mattress protector, Foam, Pillow, Divan, Toshok, Cushion, Comforter, Baby Set, Latex Foam, Memory Foam etc</li>\r\n	<li>No delivery charges (Free)</li>\r\n	<li>Discount can be availed as many times as a GP STAR can.</li>\r\n	<li><strong>Address:&nbsp;</strong>Sena Kalyan Bhaban, 14th&nbsp;Floor, 195, Motijheel C/A, Dhaka - 1000</li>\r\n	<li><strong>Hotline/Care Line:&nbsp;</strong>01708158805</li>\r\n	<li><strong>Facebook:&nbsp;</strong><a href=\"https://www.facebook.com/EuroasiaMattressBD\" target=\"_blank\">https://www.facebook.com/EuroasiaMattressBD</a></li>\r\n	<li><strong>Offer Valid Till:&nbsp;</strong>31 December 2019</li>\r\n</ul>\r\n', '', '1735035524f6d859786306b2bd55d6c5e27cca7b07-550x300.jpg', 5, '2', 'new tag1', '2018-06-04', '2018-06-04', '2018-12-31', 0, 0, 1, '2018-07-28 16:05:26', '', '', '', 1, 15, '15% OFF', 41, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(8, 1, 12, 'Metro Kitchens - 12% off on all food (except drinks/water/tea) to GP STAR', 'Metro_Kitchens_-_12_off_on_all_food_(except_drinkswatertea)_to_GP_STAR_8', '3', '2018-06-04', '<p>&nbsp;</p>\r\n\r\n<p>Terms and Condition</p>\r\n\r\n<ul>\r\n	<li>Metro Kitchens - 12% off on all food (except drinks/water/tea) to GP STAR</li>\r\n	<li><strong>Outlet Address:&nbsp;</strong>Metro Kitchens, 422, Abdus Sobhan Dhali Boro Madrasha goli, Apollo Hospital road, Vatara, Solmaid, Dhaka - 1229.</li>\r\n	<li><strong>Hotline:&nbsp;</strong>01708485398</li>\r\n	<li><strong>Facebook:&nbsp;</strong><a href=\"https://www.facebook.com/metrokitchensbd\" target=\"_blank\">https://www.facebook.com/metrokitchensbd</a></li>\r\n	<li><strong>Offer Valid Till:&nbsp;</strong>31 December 2018</li>\r\n</ul>\r\n', '', '1621493086f6d859786306b2bd55d6c5e27cca7b07-550x300.jpg', 5, '3', '', '2018-06-04', '2018-09-30', '2018-06-04', 0, 0, 1, '2018-06-04 08:30:01', '', '', '', 1, 12, '12% OFF', 92, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(9, 1, 12, 'PLAIRE - 10% Discount for GP STAR Customers.', 'PLAIRE_-_10_Discount_for_GP_STAR_Customers._9', '1', '2018-06-04', '<p>&nbsp;</p>\r\n\r\n<p>Terms and Condition</p>\r\n\r\n<ul>\r\n	<li>10% discount on all items of PLAIRE outlet</li>\r\n	<li>VAT, AIT or other applicable charges will be added</li>\r\n	<li>Discount can be availed as many times as a GP STAR can</li>\r\n	<li><strong>Address:&nbsp;</strong>House 5, Block- G, Banani 11, 1213 Dhaka, Bangladesh</li>\r\n	<li>PLAIRE, House # 39/1, Road # 2, Dhanmondi Dhaka</li>\r\n	<li>Phone: 01915589512</li>\r\n	<li><strong>Offer valid till:&nbsp;</strong>30th June 2018</li>\r\n</ul>\r\n', '', '54928695240c54a918fa67e887788840a1b83c14e-550x300.jpg', 5, '3', '', '2018-06-04', '2018-06-30', '2018-06-04', 0, 0, 1, '2018-06-04 08:32:39', '', '', '', 1, 10, '10% OFF', 118, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(10, 1, 13, '15% discount on all food items at Laziz Bistro Coxbazar for Priyojon Customer.', '15_discount_on_all_food_items_at_Laziz_Bistro_Coxbazar_for_Priyojon_Customer._10', '3', '2018-06-04', '<p>&nbsp;</p>\r\n\r\n<p><strong>Deal Terms &amp; Highlights:</strong></p>\r\n\r\n<ul>\r\n	<li><strong>Offer</strong>:&nbsp;15% discount on all food items at Laziz Bistro Coxbazar for Priyojon Customer.</li>\r\n	<li>Laziz Bistro will Offer 15% discount on all food items</li>\r\n	<li>This offer will be valid till <strong>30th October 2018</strong></li>\r\n	<li>To avail the offer, Banglalink Priyojon Customer has to type &ldquo;LAZIZ&rdquo; and send the SMS to 2012</li>\r\n	<li>Email Address :&nbsp;<a href=\"mailto:info@laziz-bistro.com\">info@laziz-bistro.com</a></li>\r\n</ul>\r\n', '', '294922127b8cc261d54ddc567b4555de2f7fb1364-550x300.jpg', 5, '3', '', '2018-06-04', '2018-08-31', '2018-06-04', 0, 0, 1, '2018-06-04 08:36:15', '', '', '', 1, 15, 'Priyojon Customer', 84, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(11, 1, 13, '10% discount on all food items at Sea Lamp Restaurant, Cox Bazar', '10%_discount_on_all_food_items_at_Sea_Lamp_Restaurant_Cox_Bazar_11', '3', '2018-06-04', '<p><strong>Deal Terms &amp; Highlights:</strong></p>\r\n\r\n<ul>\r\n	<li><strong>Offer</strong>:&nbsp;10% discount on all food items at Sea Lamp Restaurant, Cox Bazar</li>\r\n	<li>This offer will be valid till <strong>30th October 2018</strong></li>\r\n	<li>To avail the offer, Banglalink Priyojon Customer has to type &ldquo;SEALAMP&rdquo; and send the SMS to 2012</li>\r\n</ul>\r\n', '', '1140123018540d2fced79b1607fd6dfd28e5d1d2e9-550x300.jpg', 1, '3', '', '2018-06-04', '2018-06-04', '2018-06-04', 0, 0, 1, '2018-07-25 16:49:34', '', '', '', 1, 10, '10% OFF', 23, 0, 0, '', 0, 3, '2019-05-07 04:57:08', 1),
(12, 1, 12, '20% off on Ala Carte Menu at VENI VIDI VICI to GP STAR customers.', '20_off_on_Ala_Carte_Menu_at_VENI_VIDI_VICI_to_GP_STAR_customers._12', '2', '2018-06-04', '<p>&nbsp;</p>\r\n\r\n<p>Deal Highlights</p>\r\n\r\n<ul>\r\n	<li><strong>Offer</strong>:&nbsp;20% off on Ala Carte Menu at <strong>VENI VIDI VICI</strong> to GP STAR customers.</li>\r\n	<li>Address: Veni Vidi Vici, House # 48, Road # 4, Sector # 3, Uttara, Dhaka.</li>\r\n	<li>Contact: 01705737888</li>\r\n	<li>Offer Valid Till: 30 July 2018</li>\r\n	<li>Facebook: <a href=\"https://www.facebook.com/venividivicidhaka\" target=\"_blank\">https://www.facebook.com/venividivicidhaka</a></li>\r\n	<li>* Discount can be availed as many times as a GP STAR can.</li>\r\n</ul>\r\n', '', '18525009322cacfa7eee0f30e5491da62fb1647b52-550x300.jpg', 4, '3', '', '2018-06-04', '2018-06-30', '2018-09-30', 0, 0, 1, '2018-06-04 08:39:42', '', '', '', 1, 20, '20% OFF', 143, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(13, 1, 2, 'Veet Cream 15g buy 1 get 1 free', 'Veet_Cream_15g_buy_1_get_1_free_13', '4', '2018-06-04', '<p>Key Features</p>\r\n\r\n<ul>\r\n	<li>Product Type: Hair Removal Cream</li>\r\n	<li>Brand: Veet</li>\r\n	<li>Capacity: 15g</li>\r\n	<li>Flavor: Sensitive</li>\r\n	<li>Offer: Buy 1 Get 1 Free</li>\r\n</ul>\r\n', '', '1698285123veet.jpg', 4, '5', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 09:43:33', '', '', '', 1, 0, '1 Get 1 Free', 174, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(14, 1, 2, 'TRESemme Ionic Strength Shampoo And Conditioner Combo', 'TRESemme_Ionic_Strength_Shampoo_And_Conditioner_Combo_14', '3', '2018-06-04', '<p>Key Features</p>\r\n\r\n<ul>\r\n	<li>Product Type : Hair Care Combo</li>\r\n	<li>Brand: TRESemm&eacute;</li>\r\n	<li>Ionic Strength Shampoo - 190ml</li>\r\n	<li>Ionic Strength Conditioner - 190ml</li>\r\n	<li>Gender: Women</li>\r\n</ul>\r\n', '', '197687153950off.jpg', 4, '5', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 09:45:40', '', '', '', 1, 50, '50% OFF', 203, 0, 4, '', 1, 1, '2019-05-12 10:36:11', NULL),
(15, 1, 2, 'Real Techniques Core Collection 4 Pieces Makeup Brush Set - Copper', 'Real_Techniques_Core_Collection_4_Pieces_Makeup_Brush_Set_-_Copper_15', '1', '2018-06-04', '<p>Key Features</p>\r\n\r\n<ul>\r\n	<li>Look pixel-perfect even in harsh light</li>\r\n	<li>Ultra-plush, synthetic taklon bristles</li>\r\n	<li>hand-cut and 100% cruelty-free&nbsp;</li>\r\n	<li>Extended aluminum handles</li>\r\n	<li>Light Weight and easy to use</li>\r\n</ul>\r\n', '', '20871309584p.jpg', 4, '5', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 09:47:47', '', '', '', 1, 0, '4 Pieces', 155, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(16, 1, 2, 'Lanvin Jeanne Lanvin EDP - 100ml', 'Lanvin_Jeanne_Lanvin_EDP_-_100ml_16', '3', '2018-06-04', '<p>Key Features</p>\r\n\r\n<ul>\r\n	<li>Product Type: Perfume</li>\r\n	<li>Lanvin</li>\r\n	<li>Capacity: 100ml</li>\r\n	<li>Gender: Women</li>\r\n</ul>\r\n', '', '268591872lav.jpg', 2, '5', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 10:23:54', '', '', '', 1, 50, '50% OFF', 146, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(17, 1, 2, 'Jaguar Jaguar Classic Black For Men - 100 ml EDT', 'Jaguar_Jaguar_Classic_Black_For_Men_-_100_ml_EDT_17', '6', '2018-06-04', '<p>Key Features</p>\r\n\r\n<ul>\r\n	<li>Product Type: Perfume</li>\r\n	<li>Capacity: 100 ml EDT</li>\r\n	<li>Fragrance Impression: Vetiver, Nutmeg, Green Apple, Mandarin Orange, Cedar, Cardamom, Black Tea, Musk, Geranium, Marine Accord, Sandalwood, Moss, Orange and Tonka.</li>\r\n	<li>Color: Black</li>\r\n	<li>Gender: Men</li>\r\n</ul>\r\n', '', '274356834jagu.jpg', 2, '5', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 10:25:16', '', '', '', 1, 0, '46% OFF', 150, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(18, 1, 5, 'Sunvital Premium Apple Juice (Buy 1 Get 1)', 'Sunvital_Premium_Apple_Juice_(Buy_1_Get_1)_18', '4', '2018-06-04', '<p>Chaldal.com is an online shop in Dhaka, Bangladesh. We believe time is valuable to our fellow Dhaka residents, and that they should not have to waste hours in traffic, brave bad weather and wait in line just to buy basic necessities like eggs! This is why Chaldal delivers everything you need right at your door-step and at no additional cost.</p>\r\n', '', '1191571390chal.jpg', 4, '8', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 10:32:27', '', '', '', 1, 0, '1 Get 1 Free', 170, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(19, 1, 5, 'ACI Savlon Handwash Refill Combo (Free Container)', 'ACI_Savlon_Handwash_Refill_Combo_(Free_Container)_19', '2', '2018-06-04', '<p>Chaldal.com is an online shop in Dhaka, Bangladesh. We believe time is valuable to our fellow Dhaka residents, and that they should not have to waste hours in traffic, brave bad weather and wait in line just to buy basic necessities like eggs! This is why Chaldal delivers everything you need right at your door-step and at no additional cost.</p>\r\n', '', '301343395t.jpg', 2, '8', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 10:34:55', '', '', '', 1, 0, 'Free Container', 176, 0, 0, '', 1, 1, '2019-05-12 10:36:51', NULL),
(20, 1, 5, 'Axe Body Perfume Signature Intense Value Pack', 'Axe_Body_Perfume_Signature_Intense_Value_Pack_20', '6', '2018-06-04', '<p>Chaldal.com is an online shop in Dhaka, Bangladesh. We believe time is valuable to our fellow Dhaka residents, and that they should not have to waste hours in traffic, brave bad weather and wait in line just to buy basic necessities like eggs! This is why Chaldal delivers everything you need right at your door-step and at no additional cost.</p>\r\n', '', '773209577sss.jpg', 4, '8', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 10:37:49', '', '', '', 1, 0, '100tk OFF', 135, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(21, 1, 7, 'Mithai Ramadan Offer Package-3', 'Mithai_Ramadan_Offer_Package-3_21', '1', '2018-06-04', '<p><strong>Product Details:</strong></p>\r\n\r\n<p>Mithai Ramadan Offer Package-3<br />\r\nBrand: Mithai<br />\r\nPackage includes:<br />\r\nMithai Mawa Laddu 1Kg 34910<br />\r\nDry Lalmohone 0.5Kg 33153<br />\r\nPorabari Chamcham 1Kg 33160<br />\r\nTasty &amp; sweet<br />\r\nColor: As given picture.<br />\r\n<br />\r\n<strong>Note:<br />\r\nProduct delivery duration may vary due to product availability in stock.</strong><br />\r\n<br />\r\nN.B: Minimum order amount is Tk. 500. To buy this food item, you must order food items of a minimum of Tk 500 of Mithai. To add other food items of Mithai to your cart, please <a href=\"https://www.othoba.com/mithai\">click here.</a><br />\r\n<br />\r\n<strong>Coverage Area:</strong> Products will be delivered within the following Dhaka City areas from the nearby Tasty Treat/Mithai outlets:<strong> Mirpur, Mohammadpur</strong><br />\r\n<strong>Delivery Time:&nbsp;</strong>Product will be delivered within 1-2 working hours after your order is confirmed by our customer care representative.<br />\r\n<strong>Delivery charge:</strong> Free</p>\r\n', '', '854941563mithai.png', 4, '8', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 10:40:41', '', '', '', 1, 10, '10% OFF', 138, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(22, 1, 7, 'Black Forest Cake 1kg 46648', 'Black_Forest_Cake_1kg_46648_22', '3', '2018-06-04', '<p><strong>Product details:</strong></p>\r\n\r\n<p>Black forest cake<br />\r\nItem code:&nbsp;46648<br />\r\nFlavor: Black forest<br />\r\nNet Weight: 1kg<br />\r\nBrand: Tasty Treat</p>\r\n\r\n<p><strong><strong>Note:</strong></strong></p>\r\n\r\n<p><strong><strong>&nbsp;*Delivery Time:</strong> Product will be delivered within 1-4 working hours after your order is confirmed by our customer care representative.</strong></p>\r\n\r\n<p><strong>&nbsp;*You can provide us your custom wish message to get them written on the cake. Please use the special instruction text-box in the chekout page to enter your text. Wish text should be short &amp; smart so that we can write them on your ordered cake. If you don&#39;t need any custom wish text, simply skip it.</strong></p>\r\n\r\n<p><strong>&nbsp;*Product delivery duration may vary due to product availability in stock.</strong></p>\r\n\r\n<p><strong>&nbsp;*Minimum order amount is Tk. 500. To buy this food item, you must order food items of a minimum of Tk 500 of <strong>Tasty Treat</strong>. To add other food items of <strong>Tasty Treat</strong> to your cart, please <a href=\"https://www.othoba.com/tasty-treat\" target=\"_blank\"><strong>click here.</strong></a></strong></p>\r\n', '', '2025709853cake.png', 2, '8', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 10:42:13', '', '', '', 1, 15, '15% OFF', 163, 7, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(23, 1, 3, 'MAVERICK Men\'s Loafer-96431A42', 'MAVERICK_Mens_Loafer-96431A42_23', '3', '2018-06-04', '<h2>Description:</h2>\r\n\r\n<ul>\r\n	<li>Product Type: Men&#39;s Loafer</li>\r\n	<li>Color: BEIGE&nbsp;</li>\r\n	<li>Main Material: Leather</li>\r\n	<li>Gender: Male</li>\r\n</ul>\r\n', '', '1955585326loafer.png', 4, '1', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 12:24:46', '', '', '', 1, 20, '20% Cashback', 2, 0, 8, '20,88', 1, 1, '2019-05-09 05:34:01', NULL),
(24, 1, 3, ' Men\'s Gabardine Pant-GP 106', 'Mens_Gabardine_Pant-GP_106_24', '1', '2018-06-04', '<h2>Description:</h2>\r\n\r\n<ul>\r\n	<li>Product Name: Men&#39;s Gabardine Pant</li>\r\n	<li>Quality: Export Quality</li>\r\n	<li>Fabrics: 100% Cotton</li>\r\n	<li>Colour: Same as Picture</li>\r\n	<li>Fashionable and Trendy</li>\r\n	<li>Gender: Men</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '', '655687235grab.png', 2, '1', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 12:28:01', '', '', '', 1, 20, '20% OFF', 158, 0, 0, '', 1, 1, '2019-05-07 04:57:08', 2),
(25, 1, 3, 'Mobile Phone, Card & Money Holder (1881 D)', 'Mobile_Phone_Card_&_Money_Holder_(1881_D)_25', '6', '2018-06-04', '<h2>Descriptions:</h2>\r\n\r\n<ul>\r\n	<li>Brand Name: Baellerry</li>\r\n	<li>Material: PU Leather &amp; Metal Frame</li>\r\n	<li>Width: 9.2cm, Height: 2.5cm, Length: 17.2cm</li>\r\n	<li>Origin: China</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '', '1431971715card.png', 4, '2', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 12:34:01', '', '', '', 1, 9, '9% OFF', 99, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(26, 1, 3, 'Mobile Clip-on Lens With 3-in-1 Effect', 'Mobile_Clip-on_Lens_With_3-in-1_Effect_26', '3', '2018-06-04', '<ul>\r\n	<li>Universal Clip-on Lens With 3-in-1 Effect (Fish-eye, Wide-Angle, Macro)</li>\r\n	<li>The Camera Lens is attached to a convenient cord and clip so you can leave it attached to your phone or digital camera for quick access to fun shots.&nbsp;&nbsp;</li>\r\n	<li>The MRS Universal Clip-on Lens comes with a handy carrying pouch for you to easily carry these lens&#39; around, so you can take better pictures anytime you want with these special effects lens.</li>\r\n</ul>\r\n', '', '441944011mobileclip.png', 2, '2', '', '2018-06-04', '0000-00-00', '2018-06-04', 0, 0, 1, '2018-06-04 12:36:23', '', '', '', 1, 0, '25% Discount', 140, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(27, 1, 13, '20% Discount on their day long Lunch & Dinner Cruise Package for Priyojon Customer.', '20_Discount_on_their_day_long_Lunch_&_Dinner_Cruise_Package_for_Priyojon_Customer._27', '7', '2018-06-05', '<p><strong>Deal Terms &amp; Highlights:</strong></p>\r\n\r\n<ul>\r\n	<li><strong>Offer</strong>:&nbsp;20% Discount on their day long Lunch &amp; Dinner Cruise Package for Priyojon Customer.</li>\r\n	<li>20% Discount on their day long Lunch &amp; Dinner Cruise Package.</li>\r\n	<li>This offer will be valid till <strong>30th October 2018.</strong></li>\r\n	<li>To avail this offer, Banglalink Priyojon Customer has to type &ldquo;DNRCR&rdquo; and send the SMS to 2012.</li>\r\n	<li>Email Address :&nbsp;<a href=\"http://www.dhakadinnercruise.com/\" target=\"_blank\">www.dhakadinnercruise.com</a></li>\r\n	<li>HOTLINE + 88 019 222 55550</li>\r\n</ul>\r\n', '', '19119036100dcf7e1a79db0810615720e658cc5512-550x300.jpg', 5, '3', '', '2018-06-05', '2018-08-31', '2018-06-05', 0, 0, 1, '2018-06-05 00:26:55', '', '', '', 1, 20, '20% OFF', 51, 0, 0, '', 0, 1, '2019-05-07 04:57:08', NULL),
(28, 1, 12, '10% off on BREAD & Beyond products for GP STAR Customers.', '10_off_on_BREAD_&_Beyond_products_for_GP_STAR_Customers._28', '8', '2018-06-05', '<p>&nbsp;</p>\r\n\r\n<p>There are 10 different Outlets in Dhaka. These are:</p>\r\n\r\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\r\n	<thead>\r\n		<tr>\r\n			<th>\r\n			<p><strong>Outlet Name</strong></p>\r\n			</th>\r\n			<th>\r\n			<p><strong>Outlet Location &amp; Address</strong></p>\r\n			</th>\r\n			<th>\r\n			<p><strong>Outlet contact Number</strong></p>\r\n			</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p><strong>Dhanmondi</strong></p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>House# 56, Road# 3/A<br />\r\n			(Near Japan Bangladesh Friendship Hospital),<br />\r\n			Satmosjid road, Dhanmondi.</p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>01841320944</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p><strong>Banani</strong></p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>UAE Moitry Complex (Ground floor),<br />\r\n			Kamal Ataturk Avenue,<br />\r\n			Banani.</p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>01841320946</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p><strong>Gulshan-1</strong></p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>SAM Tower (Ground floor),<br />\r\n			Road# 22 (Opposite of Saimon Center),<br />\r\n			Gulshan-1, Dhaka-1212</p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>01841320948</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p><strong>Khilgaon</strong></p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>Khilgaon Chowdhury Para,<br />\r\n			Plot # C 577(A),<br />\r\n			Khilgaon.</p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>01841321018</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p><strong>Uttara, Jasimuddin Road</strong></p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>House# 14, Road# 1,<br />\r\n			Sector# 3, Jasimuddin Avenue,<br />\r\n			Uttara.</p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>01841321014</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p><strong>Gulshan - 2</strong></p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>Shop # 4, Gulshan Bhaban Market,<br />\r\n			(Ground floor), Gulshan Circle-2,<br />\r\n			Dhaka-1212.</p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>01841321016</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p><strong>Gulshan &ndash; North</strong></p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>Gulshan North Club Limited,<br />\r\n			House # 4/A, Road # 71,<br />\r\n			Gulshan-2, Dhaka-1212.</p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>01841321019</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p><strong>Panthapath</strong></p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>House # 20/3, Sundoram Plaza (Beside Panthapath Jame Masjid),<br />\r\n			West Panthapath, Kolabagan,<br />\r\n			Dhaka-1205.</p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>01841321027</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p><strong>Mohammadpur</strong></p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>Holding # 8, Mohammadi Housing Main Road,<br />\r\n			(Opposite of Housing Society Gate # 3),<br />\r\n			Mohammadpur, Dhaka-1207.</p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>01841321038</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p><strong>IDB, Agargaon</strong></p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>IDB Bhaban,<br />\r\n			E/8-A, Rokeya Sharani,<br />\r\n			Agargaon.</p>\r\n			</td>\r\n			<td style=\"vertical-align:bottom\">\r\n			<p>01841321012</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><strong>Facebook page address::&nbsp;</strong><a href=\"http://www.facebook.com/breadnbeyond.dhaka\" target=\"_blank\">www.facebook.com/breadnbeyond.dhaka</a></p>\r\n\r\n<p><strong>Hotlinee:&nbsp;</strong>01841320944</p>\r\n\r\n<p><strong>Valid Till</strong>: 31 October 2018</p>\r\n\r\n<p>&nbsp;</p>\r\n', '', '1704824168d5ac4b7eb71883c9e9c9749dbf13301d-550x300.jpg', 4, '3', '', '2018-06-05', '2018-07-31', '2018-09-30', 0, 0, 1, '2018-06-05 00:29:50', '', '', '', 1, 0, '20% OFF', 97, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(29, 1, 14, '10% off on Lunch and Dinner Buffet', '10_off_on_Lunch_and_Dinner_Buffet_29', '7', '2018-06-05', '<p>Deal Highlights</p>\r\n\r\n<ul>\r\n	<li><strong>Offer 1:</strong> 10% off on Lunch and Dinner Buffet</li>\r\n	<li>Specializes in Buffet at Uttara</li>\r\n	<li>Kindly present the SMS before availing the offer</li>\r\n	<li>15% Vat Applicable</li>\r\n	<li>Use your SMS within 3 days&nbsp;</li>\r\n</ul>\r\n\r\n<p>Deal Terms</p>\r\n\r\n<ul>\r\n	<li>Offer is valid: <strong>30th June, 2018</strong></li>\r\n	<li>Offer &amp; outlet timings: 11:00 AM to 11:00 PM, Everyday</li>\r\n	<li>This offer cannot be clubbed with other existing offers</li>\r\n	<li>Appointment is recommended</li>\r\n	<li>Images are for representation purpose only</li>\r\n</ul>\r\n', '', '344939501ca67d0c8800dc28f4d1694513df55008-550x300.jpg', 5, '3', '', '2018-06-05', '2018-06-30', '2018-07-31', 0, 0, 1, '2018-06-05 00:32:38', '', '', '', 1, 10, '10% OFF', 111, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(30, 1, 12, 'GP STAR Offer in Gazipur.', 'GP_STAR_Offer_in_Gazipur._30', '8', '2018-06-05', '<p>&nbsp;</p>\r\n\r\n<p>Terms and Condition</p>\r\n\r\n<ul>\r\n	<li>\r\n	<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\r\n		<thead>\r\n			<tr>\r\n				<th style=\"text-align:center\">\r\n				<ul>\r\n					<li>Partner&rsquo;s Name</li>\r\n				</ul>\r\n				</th>\r\n				<th style=\"text-align:center\">\r\n				<p>Type of Outlet</p>\r\n				</th>\r\n				<th style=\"text-align:center\">\r\n				<p>Address</p>\r\n				</th>\r\n				<th style=\"text-align:center\">\r\n				<p>Offer Details</p>\r\n				</th>\r\n				<th style=\"text-align:center\">\r\n				<p>How to Avail the Offer</p>\r\n				</th>\r\n			</tr>\r\n		</thead>\r\n		<tbody>\r\n			<tr>\r\n				<td style=\"text-align:center\">\r\n				<p>Fullstop Lounge &amp; Restaurant</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Restaurant</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Mr. Mohammad Motalib (Sumon), Chairman,</p>\r\n\r\n				<p>D-175/1, Jorpukur Road, Joydebpur, Gazipur.</p>\r\n\r\n				<p>Mob: 01711627999, 01762630630</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>12% Discount on all Food items</p>\r\n\r\n				<p>(Except beverage)</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Write: FULLSTOP Total Bill Amount (Tk.)</p>\r\n\r\n				<p>and send to 9000 port</p>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td style=\"text-align:center\">\r\n				<p>Chicken Chilli</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Restaurant</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Mr. Kawsarul Hasan (Sujon), Manager,</p>\r\n\r\n				<p>Rajbari Road (Opposite Side of SP Office), Joydebpur,</p>\r\n\r\n				<p>Gazipur. Mob: 01730913751</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>15% Discount on all Food items</p>\r\n\r\n				<p>(Except beverage)</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Write: CC Total Bill Amount (Tk.)</p>\r\n\r\n				<p>and send to 9000 port</p>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td style=\"text-align:center\">\r\n				<p>Nurjahan Fashion</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Life Style</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Mr. Nazrul Islam, Proprietor,</p>\r\n\r\n				<p>Home Trust Zamir Plaza, D-196/1, Jorpukur Road,</p>\r\n\r\n				<p>Joydebpur, Gazipur. Mob: 01711661344</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>12% Discount on all Garments items</p>\r\n\r\n				<p>(Except Cosmetics items)</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Write: NUR Total Price (Tk.)</p>\r\n\r\n				<p>and send to 9000 port</p>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td style=\"text-align:center\">\r\n				<p>Nakshi Katha Fashion House</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Life Style</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Mr. Towhid Zamir (Shibli), Proprietor,</p>\r\n\r\n				<p>Home Trust Zamir Plaza, D-196/1, Jorpukur Road,</p>\r\n\r\n				<p>Joydebpur, Gazipur. Mob: 01764345653</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>10% Discount on all Garments items</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Write: NAKSHI Total Price (Tk.)</p>\r\n\r\n				<p>and send to 9000 port</p>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td style=\"text-align:center\">\r\n				<p>UK Beauty Gallery</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Life Style</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Mr. Anisur Rahman Khan, Director,</p>\r\n\r\n				<p>Shop No - 18, Home Trust Zamir Plaza, D-196/1,</p>\r\n\r\n				<p>Jorpukur Road, Joydebpur, Gazipur. Mob: 01714497141</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>10% Discount on all Shoes, Cosmetics</p>\r\n\r\n				<p>and Bag items</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Write: UK <strong>&nbsp;</strong>Total Price (Tk.)</p>\r\n\r\n				<p>and send to 9000 port</p>\r\n				</td>\r\n			</tr>\r\n			<tr>\r\n				<td style=\"text-align:center\">\r\n				<p>APSON Beauty Parlor</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Life Style</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Mrs. Sufia Begum, Proprietor,</p>\r\n\r\n				<p>D-171/1, Rajbari Road, Joydebpur,</p>\r\n\r\n				<p>Gazipur. Mob: 01712622851</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>15% Discount on all Beauty Service</p>\r\n\r\n				<p>for Women</p>\r\n				</td>\r\n				<td style=\"text-align:center\">\r\n				<p>Write: APSON Total bill Amount (Tk.)</p>\r\n\r\n				<p>and send to 9000 port</p>\r\n				</td>\r\n			</tr>\r\n		</tbody>\r\n	</table>\r\n\r\n	<p><strong>Offer Valid Till:&nbsp;</strong>31 December 2018</p>\r\n	</li>\r\n</ul>\r\n', '', '1171628761c2f3e8c76213b71cd9b8d7b4612e81aa-550x300.jpg', 2, '3', '', '2018-06-05', '2018-10-31', '2018-11-10', 0, 0, 1, '2018-06-05 00:34:51', '', '', '', 1, 0, 'Only Gazipur', 155, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(31, 1, 15, 'Apple watch & more upto 17% OFF at Kiksha Collection', 'Apple_watch_&_more_upto_17_OFF_at_Kiksha_Collection_31', '9', '2018-06-05', '<h1>Apple Watch Series 3 42mm GPS</h1>\r\n\r\n<h2>Quick Overview</h2>\r\n\r\n<ul>\r\n	<li>Weight 0.3500</li>\r\n	<li>Display Size(Inches) 1.6</li>\r\n	<li>Connectivity Wi-Fi (802.11b/g/n 2.4GHz); Bluetooth 4.2</li>\r\n	<li>CPU Speed(Ghz) W2 chip</li>\r\n	<li>Processor Dual Core</li>\r\n	<li>Battery mAh Up to 18 hours of battery life</li>\r\n</ul>\r\n\r\n<p><strong>10 Days Replacement Warrenty and 3 Month Service warrenty</strong><br />\r\n<strong>Product service will be given by as the availability of the parts</strong></p>\r\n', '', '1464539468kikwatch.png', 2, '2', '', '2018-06-05', '2018-06-30', '2018-07-31', 0, 0, 1, '2018-06-05 01:54:09', '', '', '', 1, 0, 'upto 17% OFF', 102, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(32, 1, 12, 'Oppo F7 Pro with 01711XXXXXX series SIM free', 'Oppo_F7_Pro_with_01711XXXXXX_series_SIM_free_32', '2', '2018-06-07', '<h2>Conditions:</h2>\r\n\r\n<p>01711 series 4G SIM FREE</p>\r\n\r\n<ul>\r\n	<li>Home Delivery of Free 01711 series SIM is available(along with biometric registration) only inside Dhaka city.</li>\r\n	<li>If You live Outside Dhaka, You have to collect the free 01711 series SIM from the nearest Grameenphone Center(GPC) by showing the GP Shop invoices as evidence of purchases.</li>\r\n	<li>Customer will get one SIM Card with purchase of each smartphone worth 9,000 Taka</li>\r\n	<li>Click the link to find the locations of your nearest GPC:&nbsp;<strong>&nbsp;</strong><a href=\"https://www.grameeenphone.com/gpc-list\" target=\"_BLANK\">https://www.grameeenphone.com/gpc-list</a></li>\r\n</ul>\r\n\r\n<p>By proceeding for checkout, I agree with terms and conditions mentioned above</p>\r\n\r\n<p>&nbsp;</p>\r\n', '', '751763518Oppo_F7_Pro_GPShop_2_1.png', 4, '2', '', '2018-06-07', '0000-00-00', '2018-06-07', 0, 0, 1, '2018-06-07 00:08:42', '', '', '', 1, 0, '01711 SIM FREE', 622, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(33, 1, 16, 'Anker SoundCore Mini Bluetooth Speaker', 'Anker_SoundCore_Mini_Bluetooth_Speaker_33', '7', '2018-06-07', '<h2>Anker SoundCore Mini Bluetooth Speaker</h2>\r\n\r\n<ul>\r\n	<li>Sound Driver: 5W</li>\r\n	<li>Connectivity: Micro SD; AUX</li>\r\n	<li>Bluetooth 4.0</li>\r\n	<li>Connection Range: 66ft</li>\r\n	<li>Playback time: 15 hours</li>\r\n</ul>\r\n\r\n<h3>Compact And Powerful</h3>\r\n\r\n<p>Super-portable Bluetooth speaker with incredible sound quality. A 5W audio driver and a passive subwoofer ensure you get crystal-clear sound and powerful bass from a speaker you can literally take anywhere.</p>\r\n\r\n<h3>Advanced Bluetooth Technology</h3>\r\n\r\n<p>Bluetooth 4.0 seamlessly connects to any Bluetooth device. A 66ft transmission range and a noise-cancelling microphone enable hands-free calling.</p>\r\n\r\n<h3>Unstoppable Music</h3>\r\n\r\n<p>SoundCore mini boasts micro SD support, AUX-in jack, and an FM radio&mdash;so even if your phone stops, your music keeps going.</p>\r\n\r\n<h3>Listen Longer</h3>\r\n\r\n<p>SoundCore mini supplies you with 15 hours of continuous playtime, more than double that of similar-sized speakers, so you can listen for longer.</p>\r\n', '', '2029305427blutooth.png', 4, '2', '', '2018-06-07', '0000-00-00', '2018-06-07', 0, 0, 1, '2018-06-07 00:57:16', '', '', '', 1, 13, '13% OFF', 246, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(34, 1, 17, 'Xiaomi Mi A1 4GB/64GB - 29% OFF only at trendytracker', 'Xiaomi_Mi_A1_4GB64GB_-_29_OFF_only_at_trendytracker_34', '7', '2018-06-07', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Manufacturer</td>\r\n			<td>Xiaomi</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Model</td>\r\n			<td>Mi&nbsp;A1</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Network</td>\r\n			<td>Supports Volte/4G/3G/2G on&nbsp;compatible networks<br />\r\n			GSM&nbsp;/ HSPA&nbsp;/ LTE<br />\r\n			2G&nbsp;bands GSM 850&nbsp;/ 900&nbsp;/ 1800&nbsp;/ 1900&nbsp;&mdash; SIM 1&nbsp;&amp; SIM 2<br />\r\n			3G&nbsp;bands HSDPA 850&nbsp;/ 900&nbsp;/ 1900&nbsp;/ 2100<br />\r\n			4G&nbsp;bands LTE band 1(2100), 3(1800), 5(850), 7(2600), 8(900), 38(2600), 39(1900), 40(2300), 41(2500)<br />\r\n			Speed HSPA, LTE<br />\r\n			GPRS Yes<br />\r\n			EDGE Yes<br />\r\n			*To&nbsp;be&nbsp;confirmed</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Dimensions</td>\r\n			<td>155.4&times;75.8&times;7.3&nbsp;mm (6.12&times;2.98&times;0.29&nbsp;in)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Weight</td>\r\n			<td>165&nbsp;g (5.82&nbsp;oz)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>SIM</td>\r\n			<td>Dual SIM (Nano-SIM, dual stand-by)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Display type</td>\r\n			<td>IPS LCD capacitive touchscreen, 16M colors</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Size</td>\r\n			<td>5.5 inches (~70.1% screen-to-body ratio)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Resolution</td>\r\n			<td>1920&times;1080 pixels (~403 ppi pixel density)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>OS</td>\r\n			<td>Android&nbsp;OS, v7.1.1 (Nougat)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Chipset</td>\r\n			<td>Qualcomm MSM8953 Snapdragon 625</td>\r\n		</tr>\r\n		<tr>\r\n			<td>CPU</td>\r\n			<td>Octa-core 2.0&nbsp;GHz Cortex-A53</td>\r\n		</tr>\r\n		<tr>\r\n			<td>GPU</td>\r\n			<td>Adreno 506 650MHz</td>\r\n		</tr>\r\n		<tr>\r\n			<td>RAM</td>\r\n			<td>4&nbsp;GB</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Memory</td>\r\n			<td>64&nbsp;GB</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Card slot</td>\r\n			<td>microSD, up&nbsp;to&nbsp;128&nbsp;GB</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Primary camera</td>\r\n			<td>2x&nbsp;Optical zoom Dual 12&nbsp;MP (26mm, f/2.2; 50mm, f/2.6), phase detection autofocus, 2x&nbsp;optical zoom, dual-LED (dual tone) flash</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Features</td>\r\n			<td>1/2.9&Prime; sensor size, 1.25 &micro;m @ 27mm &amp;&nbsp;1.0 &micro;m @ 52mm pixel size, geo-tagging, touch focus, face detection, HDR, panorama</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Video</td>\r\n			<td>4K&nbsp;/ 1080p&nbsp;/ 720p video, 30&nbsp;fps<br />\r\n			720p slow-mo video, 120 fps</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Secondary camera</td>\r\n			<td>5MP 1.12&mu;m, f/2</td>\r\n		</tr>\r\n		<tr>\r\n			<td>WLAN</td>\r\n			<td>Wi-Fi 802.11&nbsp;a/b/g/n/ac, dual-band, DLNA, hotspot<br />\r\n			2.4/5G WiFi&nbsp;/ WIFI Direct&nbsp;/ WiFi Display</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bluetooth</td>\r\n			<td>4.2/HID</td>\r\n		</tr>\r\n		<tr>\r\n			<td>GPS</td>\r\n			<td>Yes, with A-GPS, GLONASS, BDS</td>\r\n		</tr>\r\n		<tr>\r\n			<td>USB</td>\r\n			<td>Type-C 1.0 reversible connector fast charge 3.0</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Sensors</td>\r\n			<td>Fingerprint (rear-mounted), accelerometer, gyroscope, proximity, compass, ambient light sensor, hall sensor</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Battery</td>\r\n			<td>Non-removable 3080mAh (typ)&nbsp;/ 3000mAh (min)<br />\r\n			5V&nbsp;/ 2A&nbsp;charging<br />\r\n			Reversible USB Type-C</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Color</td>\r\n			<td>Gold</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Package</td>\r\n			<td>1&nbsp;x Phone<br />\r\n			1&nbsp;x Power adapter<br />\r\n			1&nbsp;x USB Type-C cable<br />\r\n			1&nbsp;x SIM needle<br />\r\n			1&nbsp;x User manua</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', '1156700940black_56.jpg', 2, '2', '', '2018-06-07', '0000-00-00', '2018-06-07', 0, 0, 1, '2018-06-07 04:29:27', '', '', '', 1, 29, '29% OFF', 163, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(35, 1, 17, ' NO.1 F6 Smartwatch 9.4% Off now at 2899tk', 'NO.1_F6_Smartwatch_9.4_Off_now_at_2899tk_35', '1', '2018-06-07', '<h2>Quick Overview</h2>\r\n\r\n<p>IP68 Waterproof Sleep Monitor Remote Camera Finding Phon</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h1>NO.1 F6 Smartwatch</h1>\r\n\r\n<p>NO.1 F6 smartwatch is a device that makes you live in healthier and more intelligent lifestyle. It helps to manage your sports situation that you can know your body condition better. It tracks steps and shows you how you&#39;re stacking up against your daily goals. Besides, you can share your data on Facebook and Twitter, which may encourage you and your friends to do exercise and raise interest in sports.</p>\r\n\r\n<p>Main Features:<br />\r\nÃ¢â€”Â Perfect Personal Health Tracker: sleeping monitor, heart rate monitor<br />\r\nÃ¢â€”Â NRF51822 Chip: features low consumption detailed with high performance, flexible and quickly<br />\r\nÃ¢â€”Â Ultra-long Battery Life: it consumes less power and can generally standby for 120 days<br />\r\nÃ¢â€”Â Data Synchronism: download APP named Fundo Pro to save and sync the data to achieve your healthy goal<br />\r\nÃ¢â€”Â Lift or turn over your hands, light up the screen<br />\r\nÃ¢â€”Â Support firmware upgrade</p>\r\n\r\n<p>Functions:<br />\r\nÃ¢â€”Â Finding phone notification<br />\r\nÃ¢â€”Â Alarm setting<br />\r\nÃ¢â€”Â Support language: Simplified Chinese, Traditional Chinese, English, Japanese, Spanish, German, Italian, French, Russian, Portuguese, Czech, Polish<br />\r\nÃ¢â€”Â Compatible for iOS 9.0 and above, Android 4.4 and above</p>\r\n', '', '1511025133Screenshot_1.png', 4, '2', '', '2018-06-07', '0000-00-00', '2018-06-07', 0, 0, 1, '2018-06-07 04:41:54', '', '', '', 1, 9.4, '9.% OFF', 267, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(36, 1, 16, 'General Split 1.5 TON Air Conditioner/Ceiling Type AC 5 TON (ABG54A)', 'General_Split_1.5_TON_Air_ConditionerCeiling_Type_AC_5_TON_(ABG54A)_36', '10', '2018-06-08', '<p>The General 1.5 Ton AC ASG-18ABC is a powerful and reliable air conditioner for your home. General AC is renowned for it&rsquo;s powerful cooling capability and durability. The ASG-18ABC lives up to this reputation. This 1.5 ton AC is the best solution for all your cooling needs. Be it those hot summer months or the mild temperature of spring, this AC gives you the most comfortable cooling you could ask for. And the promise of great service and durability of General is always there to give you 100% peace of mind.</p>\r\n', '', '2134987451ac.png', 4, '4', '', '2018-06-08', '0000-00-00', '2018-06-08', 0, 0, 1, '2018-06-08 04:02:53', '', '', '', 1, 28, 'upto 28% OFF', 218, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(37, 1, 17, 'Xiaomi Amazfit BIP Global Version', 'Xiaomi_Amazfit_BIP_Global_Version_37', '1', '2018-06-08', '<p><strong>Main Features:</strong><br />\r\nÃ¢â€”Â GPS + GLONASS dual positioning<br />\r\nÃ¢â€”Â Sleep monitoring<br />\r\nCarefully monitor your sleep quality<br />\r\nÃ¢â€”Â Heart rate monitor<br />\r\nConsiderate monitor contact designed on the behind of the watch, take care of your health anytime and anywhere<br />\r\nÃ¢â€”Â Bluetooth 4.0<br />\r\nConnected with mobile phone App, stable, fast and super low consumption<br />\r\nÃ¢â€”Â 1.28 inch with 2.5D Corning Gorilla Glass + AF coating film reflective colorful screen<br />\r\nBest suitable screen size with high definition picture displaying gives you great experience<br />\r\nÃ¢â€”Â About endurance time<br />\r\n45 days ( half an hour running time a week or 100 notifications every day or 10 percent screen luminance )<br />\r\nÃ¢â€”Â About standby time<br />\r\nfour months ( time display, sports monitoring, and sleep monitoring )<br />\r\n22 hours ( GPS continuous trail record )</p>\r\n', '', '1807058851amaz.png', 4, '2', '', '2018-06-08', '2018-06-17', '2018-06-08', 0, 0, 1, '2018-06-08 04:08:07', '', '', '', 1, 24.3, '24.3% OFF', 93, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(38, 1, 17, 'Z66 IP67 Waterproof Smart Bracelet', 'Z66_IP67_Waterproof_Smart_Bracelet_38', '7', '2018-06-08', '<h1>Z66 IP67 Waterproof Smart Bracelet</h1>\r\n\r\n<p>Ã£â‚¬ÂMultifunction Fitness TrackerÃ£â‚¬â€˜Heart Rate Monitor, pedometer tracker, Distance, Calories Burned, Sleep Monitor, Call and SMS Notification.<br />\r\nÃ£â‚¬ÂFashional DesignÃ£â‚¬â€˜OLED large screen display, Snap-type quick release wrist strap,without any tools easy replacement Four colors of the strap, fashion wild.<br />\r\nÃ£â‚¬ÂHigh-quality materialÃ£â‚¬â€˜With alloy material,in the box of integrated molding,after a multi-layer surface grinding,IP67 waterproof,anti-corrosion,anti-sweat.<br />\r\nÃ£â‚¬ÂLarge capacity batteryÃ£â‚¬â€˜Built in low power consumption Bluetooth chip and 100mAh rechargeable battery. the standby time can be 18days.<br />\r\nÃ£â‚¬ÂCompatible with SmartphoneÃ£â‚¬â€˜The app supports mobile phone Bluetooth 4.0 or above, Smart Bracelet Compatible with Android 4.4 or above , IOS 7.0 or above</p>\r\n\r\n<p>Functions<br />\r\nDisplay by 0.95&quot; OLEDÃ¯Â¼Å’96*64 pixcels<br />\r\nHeart rate detection<br />\r\nClockÃ¯Â¼Å’step, distance, calorie and power display<br />\r\nSport detection(step, distance, calories)<br />\r\nSleep detection (sleep time, sleep qualityÃ¯Â¼â€°<br />\r\nIncoming call(display contact)and messageÃ¯Â¼Ë†display the contents of the messageÃ¯Â¼â€°<br />\r\nAlarm clock remind<br />\r\nFirmware update by OTA<br />\r\nData synchronization to APP<br />\r\nAPP source: Download from google play for Android or AppStore for IOS<br />\r\nDownload by scan Qr code</p>\r\n\r\n<p>Apps name: Smilerun</p>\r\n', '', '1740738993z66.png', 2, '2', '', '2018-06-08', '0000-00-00', '2018-06-08', 0, 0, 1, '2018-06-08 04:19:04', '', '', '', 1, 0, '2% OFF', 3, 0, 0, '', 0, 1, '2019-05-09 06:40:11', NULL);
INSERT INTO `main_offer` (`offer_id`, `post_by`, `offer_from`, `offer_title`, `profile_name`, `offer_title_type`, `of_post_date`, `offer_desc`, `offer_terms`, `offer_featured_file`, `ctype`, `catagories`, `tags`, `sdate`, `edate`, `show_expire_date`, `climitamnt`, `climit`, `target_people`, `cdate`, `cdevice`, `cip`, `cbrowser`, `offer_store_type`, `discount`, `inner_discount`, `total_seen`, `total_like`, `total_rating`, `rated_user_id`, `is_hot`, `status`, `updated_at`, `offer_tag`) VALUES
(39, 1, 17, 'Oppo F5 4GB 32GB- Black 1200tk Discount', 'Oppo_F5_4GB_32GB-_Black_1200tk_Discount_39', '1', '2018-06-08', '<table>\r\n	<tbody>\r\n		<tr>\r\n			<th>Brand</th>\r\n			<td>Oppo</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Weight</th>\r\n			<td>0.1520</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Display Size(Inches)</th>\r\n			<td>6.00</td>\r\n		</tr>\r\n		<tr>\r\n			<th>System Storage</th>\r\n			<td>32</td>\r\n		</tr>\r\n		<tr>\r\n			<th>RAM(GB)</th>\r\n			<td>4</td>\r\n		</tr>\r\n		<tr>\r\n			<th>SIM Card</th>\r\n			<td>Dual Nano SIM</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Processor</th>\r\n			<td>Octa Core</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Connectivity</th>\r\n			<td>Wi-Fi: 2.4/5GHz 802.11 a/b/g/n; Bluetooth: v4.2; OTG; GPS</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Battery mAh</th>\r\n			<td>3200mAh</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Back Camera</th>\r\n			<td>16.0 MP</td>\r\n		</tr>\r\n		<tr>\r\n			<th>Phone Model</th>\r\n			<td>F5</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', '2012712739opf5.png', 2, '--select a Category--', '', '2018-06-08', '0000-00-00', '2018-06-08', 0, 0, 1, '2018-06-08 04:24:56', '', '', '', 1, 0, '1200tk OFF', 0, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(41, 1, 18, 'Unbeatable Low Price Offer at Priyoshop upto 2500 tk OFF', 'Unbeatable_Low_Price_Offer_at_Priyoshop_upto_2500_tk_OFF_41', '1', '2018-06-14', '<p><a href=\"http://PriyoShop.com/\" target=\"_blank\">PriyoShop.com</a> -Ã Â¦Â Ã Â¦Â¶Ã Â§ÂÃ Â¦Â°Ã Â§Â Ã Â¦Â¹Ã Â¦Â²Ã Â§â€¹ Unbeatable Low Price Offer! Ã Â¦â€¦Ã Â¦Â°Ã Â§ÂÃ Â¦Â¡Ã Â¦Â¾Ã Â¦Â° Ã Â¦â€¢Ã Â¦Â¨Ã Â¦Â«Ã Â¦Â¾Ã Â¦Â°Ã Â§ÂÃ Â¦Â®Ã Â§â€¡Ã Â¦Â° Ã Â¦Â¸Ã Â¦Â®Ã Â§Å¸ Xiaomi Ã Â¦Â«Ã Â§â€¹Ã Â¦Â¨Ã Â§â€¡ Ã Â¦Â¸Ã Â¦Â°Ã Â§ÂÃ Â¦Â¬Ã Â§â€¹Ã Â¦Å¡Ã Â§ÂÃ Â¦Å¡ Ã Â§Â¨Ã Â§Â«Ã Â§Â¦Ã Â§Â¦ Ã Â¦Å¸Ã Â¦Â¾Ã Â¦â€¢Ã Â¦Â¾ Ã Â¦ÂªÃ Â¦Â°Ã Â§ÂÃ Â¦Â¯Ã Â¦Â¨Ã Â§ÂÃ Â¦Â¤ Ã Â¦â€ºÃ Â¦Â¾Ã Â§Å“! Ã Â¦ÂÃ Â¦â€¡ Ã Â¦â€¦Ã Â¦Â«Ã Â¦Â¾Ã Â¦Â° Ã Â§Â§Ã Â§Â¨ Ã Â¦Â¤Ã Â¦Â¾Ã Â¦Â°Ã Â¦Â¿Ã Â¦â€“ Ã Â¦ÂªÃ Â¦Â°Ã Â§ÂÃ Â¦Â¯Ã Â¦Â¨Ã Â§ÂÃ Â¦Â¤Ã Â¥Â¤ Ã Â¦Â­Ã Â¦Â¿Ã Â¦Å“Ã Â¦Â¿Ã Â¦Å¸:<a href=\"https://goo.gl/QLW2jj\" target=\"_blank\">https://goo.gl/QLW2jj</a></p>\r\n\r\n<p>Ã Â¦Â«Ã Â§â€¹Ã Â¦Â¨Ã Â§â€¡ Ã Â¦â€¦Ã Â¦Â°Ã Â§ÂÃ Â¦Â¡Ã Â¦Â¾Ã Â¦Â° Ã Â¦â€¢Ã Â¦Â°Ã Â¦Â¾Ã Â¦Â° Ã Â¦Å“Ã Â¦Â¨Ã Â§ÂÃ Â¦Â¯ Ã Â¦â€¢Ã Â¦Â² Ã Â¦â€¢Ã Â¦Â°Ã Â§ÂÃ Â¦Â¨Ã Â¦Æ’ 09636-102030, 01717-864118, 01775-463764 (Ã Â¦Â¸Ã Â¦â€¢Ã Â¦Â¾Ã Â¦Â² Ã Â§Â¯Ã Â¦Å¸Ã Â¦Â¾ Ã Â¦Â¹Ã Â¦Â¤Ã Â§â€¡ Ã Â¦Â¸Ã Â§â€¡Ã Â¦Â¹Ã Â§â€¡Ã Â¦Â°Ã Â¦Â¿ Ã Â¦ÂªÃ Â¦Â°Ã Â§ÂÃ Â¦Â¯Ã Â¦Â¨Ã Â§ÂÃ Â¦Â¤), Ã Â¦â€¦Ã Â¦Â¥Ã Â¦Â¬Ã Â¦Â¾ Ã Â¦â€¢Ã Â§â€¹Ã Â¦Â¡Ã Â¦Â¸Ã Â¦Â¹ Ã Â¦â€ Ã Â¦ÂªÃ Â¦Â¨Ã Â¦Â¾Ã Â¦Â° Ã Â¦Â Ã Â¦Â¿Ã Â¦â€¢Ã Â¦Â¾Ã Â¦Â¨Ã Â¦Â¾ Ã Â¦â€œ Ã Â¦Â«Ã Â§â€¹Ã Â¦Â¨ Ã Â¦Â¨Ã Â¦Â®Ã Â§ÂÃ Â¦Â¬Ã Â¦Â° Ã Â¦â€ Ã Â¦Â®Ã Â¦Â¾Ã Â¦Â¦Ã Â§â€¡Ã Â¦Â° Ã Â¦â€¡Ã Â¦Â¨Ã Â¦Â¬Ã Â¦â€¢Ã Â§ÂÃ Â¦Â¸ Ã Â¦â€¢Ã Â¦Â°Ã Â§ÂÃ Â¦Â¨Ã Â¥Â¤</p>\r\n\r\n<p># Ã Â§Â¨ Ã Â¦Â¬Ã Â¦â€ºÃ Â¦Â°Ã Â§â€¡Ã Â¦Â° Ã Â¦Â¬Ã Â§ÂÃ Â¦Â°Ã Â§ÂÃ Â¦Â¯Ã Â¦Â¾Ã Â¦Â¨Ã Â§ÂÃ Â¦Â¡ Ã Â¦â€œÃ Â§Å¸Ã Â§ÂÃ Â¦Â¯Ã Â¦Â¾Ã Â¦Â°Ã Â§â€¡Ã Â¦Â¨Ã Â§ÂÃ Â¦Å¸Ã Â¦Â¿<br />\r\n# Ã Â¦Â¬Ã Â¦Â¿Ã Â¦â€¢Ã Â¦Â¾Ã Â¦Â¶ Ã Â¦ÂªÃ Â§â€¡Ã Â¦Â®Ã Â§â€¡Ã Â¦Â¨Ã Â§ÂÃ Â¦Å¸Ã Â§â€¡ Ã Â§Â©Ã Â§Â¦Ã Â§Â¦ Ã Â¦Å¸Ã Â¦Â¾Ã Â¦â€¢Ã Â¦Â¾ Ã Â¦â€¡Ã Â¦Â¨Ã Â¦Â¸Ã Â§ÂÃ Â¦Å¸Ã Â§ÂÃ Â¦Â¯Ã Â¦Â¾Ã Â¦Â¨Ã Â§ÂÃ Â¦Â¡ Ã Â¦â€¢Ã Â§ÂÃ Â¦Â¯Ã Â¦Â¾Ã Â¦Â¶Ã Â¦Â¬Ã Â§ÂÃ Â¦Â¯Ã Â¦Â¾Ã Â¦â€¢ Ã Â¦ÂÃ Â¦â€ºÃ Â¦Â¾Ã Â§Å“Ã Â¦Â¾ Ã Â¦Â¨Ã Â¦â€”Ã Â¦Â¦ Ã Â§Â§Ã Â§Â¦,Ã Â§Â¦Ã Â§Â¦Ã Â§Â¦ Ã Â¦Å¸Ã Â¦Â¾Ã Â¦â€¢Ã Â¦Â¾ Ã Â¦ÂªÃ Â¦Â°Ã Â§ÂÃ Â¦Â¯Ã Â¦Â¨Ã Â§ÂÃ Â¦Â¤ Ã Â¦â€¢Ã Â§ÂÃ Â¦Â¯Ã Â¦Â¾Ã Â¦Â¶Ã Â¦Â¬Ã Â§ÂÃ Â¦Â¯Ã Â¦Â¾Ã Â¦â€¢<br />\r\n# Ã Â¦Â¢Ã Â¦Â¾Ã Â¦â€¢Ã Â¦Â¾- Ã Â¦Â¬Ã Â§ÂÃ Â¦Â¯Ã Â¦Â¾Ã Â¦â€šÃ Â¦â€¢Ã Â¦â€¢- Ã Â¦Â¢Ã Â¦Â¾Ã Â¦â€¢Ã Â¦Â¾ Ã Â¦ÂÃ Â§Å¸Ã Â¦Â¾Ã Â¦Â° Ã Â¦Å¸Ã Â¦Â¿Ã Â¦â€¢Ã Â§â€¡Ã Â¦Å¸ Ã Â¦Å“Ã Â¦Â¿Ã Â¦Â¤Ã Â¦Â¾Ã Â¦Â° Ã Â¦Â¸Ã Â§ÂÃ Â¦Â¯Ã Â§â€¹Ã Â¦â€”!<br />\r\n# No Cost EMI Ã Â¦Â¸Ã Â§ÂÃ Â¦Â¬Ã Â¦Â¿Ã Â¦Â§Ã Â¦Â¾</p>\r\n\r\n<p>VERIFY WARRANTY:<br />\r\nÃ¢ËœÅ¾ Please visit <a href=\"http://xiaomibangladesh.com.bd/IMEI/check_IMEI\" target=\"_blank\">http://xiaomibangladesh.com.bd/IMEI/check_IMEI</a></p>\r\n', '', '2571428530083618_xiaomi-redmi-note-5-3gb32gb-smartphone_415.jpg', 4, '2', '', '2018-06-14', '2018-07-12', '2018-07-31', 0, 0, 1, '2018-06-14 00:45:51', '', '', '', 1, 0, '2500tk OFF', 0, 0, 0, '', 1, 1, '2019-05-07 10:17:35', NULL),
(42, 1, 2, 'R3 Smart Bracelet BP Monitor', 'R3_Smart_Bracelet_BP_Monitor_42', '1', '2018-07-09', '', '', '1788469172hclogo.png', 1, '1', '', '2018-07-09', '0000-00-00', '2018-07-09', 0, 0, 1, '2018-07-09 05:49:09', NULL, NULL, NULL, 1, 5, '5', 3, 1, 7, '20,87', 1, 1, '2019-05-26 07:09:02', NULL),
(43, 1, 7, 'Dettol Handwash Family Pack ATL00042', 'Dettol_Handwash_Family_Pack_ATL00042_43', '7', '2018-07-10', '<p><strong>Product details:</strong></p>\r\n\r\n<p>Dettol Handwash Family Pack<br />\r\nBrand: Dettol<br />\r\nType: Handwash<br />\r\nColor: As given picture<br />\r\n<br />\r\n<strong>Package</strong> <strong>includes:</strong></p>\r\n\r\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"height:181px; width:485px\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"height:30px; text-align:center; vertical-align:middle; width:378.2px\">Items</td>\r\n			<td style=\"height:30px; text-align:center; vertical-align:middle; width:101.8px\">Code</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"height:30px; text-align:center; vertical-align:middle; width:378.2px\">Dettol Handwash 200 ml Pump Cool X 1( Toy Free)</td>\r\n			<td style=\"height:30px; text-align:center; vertical-align:middle; width:101.8px\">BD035014</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"height:30px; text-align:center; vertical-align:middle; width:378.2px\">Dettol Handwash 200 ml Pump Re-energize X 1</td>\r\n			<td style=\"height:30px; text-align:center; vertical-align:middle; width:101.8px\">BD035015</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"height:30px; text-align:center; vertical-align:middle; width:378.2px\">Dettol Handwash 170 ml Refill Poly Sensitive X 1</td>\r\n			<td style=\"height:30px; text-align:center; vertical-align:middle; width:101.8px\">BD002077</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"height:30px; text-align:center; vertical-align:middle; width:378.2px\">Dettol Handwash 170 ml Refill Poly Skincare X 1</td>\r\n			<td style=\"height:30px; text-align:center; vertical-align:middle; width:101.8px\">3017305</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"height:30px; text-align:center; vertical-align:middle; width:378.2px\">Dettol Handwash 170 ml Refill Poly Re-energize X 1</td>\r\n			<td style=\"height:30px; text-align:center; vertical-align:middle; width:101.8px\">BD002078</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Note: Product delivery duration may vary due to product availability in stock.</p>\r\n\r\n<p>Disclaimer: The actual color of the physical product may slightly vary due to the deviation of lighting sources, photography or your device display settings.</p>\r\n', '', '493453478Screenshot_1.png', 4, '3', '', '2018-07-10', '0000-00-00', '2018-07-31', 0, 0, 1, '2018-07-10 00:56:05', NULL, NULL, NULL, 1, 40, '40tk OFF', 2, 0, 0, '', 0, 1, '2019-05-07 10:55:45', NULL),
(45, 1, 2, '26th march', '26th_march_44', '1', '2018-07-19', '', '', '3052537414043_912116828996570_3092996040807153664_n.jpg', 1, '3', '--select tag-', '2018-07-19', '0000-00-00', '2018-07-19', 0, 0, 1, '2018-07-19 04:33:20', NULL, NULL, NULL, 1, 5, '5', 88, 0, 0, '', 1, 2, '2019-05-07 10:11:45', NULL),
(56, 1, 2, 'This is test offer by Bagdoommmm', 'This_is_test_offer_by_Bagdoommmm_56', '6', '2018-07-30', '<p>cdsdddcsccd</p>\r\n', '<p>dcscscsdc</p>\r\n', 'offer_56.jpg', 1, '1', 'new tag1,new tag3', '2018-07-30', '2018-07-30', '2018-07-30', 0, 0, 1, '2018-07-30 14:43:12', NULL, NULL, NULL, 1, 10, '10', 70, 0, 0, '', 1, 2, '2019-05-07 04:57:08', NULL),
(57, 1, 2, 'new offer tesst', 'new_offer_tesst_57', '3', '2018-07-30', '<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 'offer_57.jpeg', 1, '2', 'new tag2,new tag3', '2018-07-30', '2018-08-17', '2018-08-08', 0, 0, 1, '2018-07-30 14:44:40', NULL, NULL, NULL, 1, 10, 'Discount Offer', 74, 0, 0, '', 1, 2, '2019-05-07 04:57:08', NULL),
(58, 1, 19, 'Get TK 1500 OFF At Daraz on minimum purchase order: TK 20,000', 'Get_TK_1500_OFF_At_Daraz_on_minimum_purchase_order:_TK_20_000_58', '4', '2018-07-30', '<h1>The most Safe &amp; Stable auto trading system.</h1>\r\n\r\n<p>GFX Scalp EA is a result of long years hard working, best market analysis, secure methodologies and real terms testing Forex auto robot system.</p>\r\n\r\n<ul>\r\n	<li>Proven ultimate auto trading system.</li>\r\n	<li>Lifetime safe and stable trading opportunity.</li>\r\n	<li>Consistent with any market conditions.</li>\r\n</ul>\r\n', '', NULL, 1, '1', NULL, '2018-07-30', '2018-08-31', '2018-07-30', 0, 0, 1, '2018-07-30 14:46:53', NULL, NULL, NULL, 1, 10, 'Free Headset', 59, 0, 0, '', 0, 2, '2019-05-07 04:57:08', NULL),
(59, 1, 19, 'Gift Offer', 'Gift_Offer_59', '3', '2018-07-30', '<h3>(STOP) listening to all the bull forex traders that are telling you lies. Forex trading is all about having a strategy that works let me show you how to make real money without getting to blow your account, 90% of retail traders lose money, let me help you to be part of the 10% <a href=\"https://www.facebook.com/hashtag/massiveaction?source=feed_text\">#MassiveAction</a> <a href=\"https://www.facebook.com/hashtag/progressivetraders?source=feed_text\">#ProgressiveTraders</a></h3>\r\n', '', 'offer_59.png', 3, '1', '5', '2018-07-30', '1970-01-01', '2018-07-30', 0, 0, 1, '2018-07-30 15:14:43', NULL, NULL, NULL, 1, 20, '2500tk OFF', 88, 0, 0, '', 0, 2, '2019-05-07 04:57:08', NULL),
(61, 1, 16, 'HUAWEI Nova 3i 4GB/128GB With Free HUAWEI Gift Box', 'HUAWEI_Nova_3i_4GB_128GB_With_Free_HUAWEI_Gift_Box_61', '2', '2018-07-31', '<h2>HUAWEI nova 3i</h2>\r\n\r\n<ul>\r\n	<li>OS: Android 8.1</li>\r\n	<li>EMUI 8.2</li>\r\n	<li>Display: 6.3&quot; TFT LCD; FHD+ 2340 x 1080, 409 PPI; 16.7 million colors</li>\r\n	<li>Chipset: Hisilicon Kirin 710</li>\r\n	<li>CPU: Octa-core (4x2.2 GHz Cortex-A73 &amp; 4x1.7 GHz Cortex-A53)</li>\r\n	<li>Memory: RAM 4GB + ROM 128GB</li>\r\n	<li>Memory card: support up to 256GB MicroSD Card</li>\r\n	<li>Video File Format:&nbsp;3gp, mp4</li>\r\n	<li>Audio File Format:&nbsp;mp3, mp4, 3gp, ogg, aac, flac, midi</li>\r\n	<li>Rear Camera: 16mp + 2MP, F/2.2 aperture, supports autofocus</li>\r\n	<li>Front Camera: 24MP+ 2MP, F/2.0 aperture, supports fixed focal length</li>\r\n	<li>Sensors: Gravity; Ambient Light; Proximity; Gyroscope; Compass; Fingerprint</li>\r\n	<li>Battery: 3340 mAh</li>\r\n	<li>Network: 2G; 3G; 4G</li>\r\n	<li>Dimension:&nbsp;Width:75.2 mm;&nbsp;Height:157.6 mm;&nbsp;Depth:7.6 mm;&nbsp;Height:; 157.6 mm</li>\r\n</ul>\r\n\r\n<p>Huawei nova 3i provides three color models for you to choose from, Pearl White, Black, and Iris Purple. With beautiful hues of color on the back glass and metal mid-frame, you will enjoy wonderful visual and handling experience. The 6.3-inch FHD+ (2340 x 1080) provides a wide view of your world while fitting in your pocket.</p>\r\n', '', 'offer_61.png', 4, '2', 'nova, mobile, ', '2018-07-31', '1970-01-01', '2018-09-30', 0, 0, 1, '2018-07-31 07:35:36', NULL, NULL, NULL, 1, 0, 'Gift Offer', 175, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(63, 1, 7, 'Amass Closet 4 Drawer Sunflower 918090', 'Amass_Closet_4_Drawer_Sunflower_918090_63', '10', '2018-08-04', '<p><strong>Product details:</strong></p>\r\n\r\n<p>Amass Closet 4 Drawer Sunflower<br />\r\nItem code: 918090<br />\r\nBrand: RFL Plastics<br />\r\nDimension: L-44 x W-46 x H-74 cm<br />\r\nWeight: 6.923 kg<br />\r\nMaterial: Polypropylene &amp; acrylonitrile butadiene styrene<br />\r\nDurable<br />\r\nBig storage capacity<br />\r\nInsect repellant additive<br />\r\nKnocked down facility for easy installation<br />\r\nVery easy to clean<br />\r\nAttractive colors &amp; designs<br />\r\nEasily movable due to presence of wheels at bottom&nbsp; Alternate of wooden wardrobes<br />\r\nThe number of drawers are available in this item are 4 drawer<br />\r\nColor: As given picture.</p>\r\n\r\n<p>Note: Product delivery duration may vary due to product availability in stock.<br />\r\n<br />\r\nDisclaimer: The color of the physical product may slightly vary due to the deviation of lighting sources, photography or your device display settings.</p>\r\n', '', 'offer_63.png', 4, '4', 'closer,', '2018-08-04', '2018-09-30', '2018-08-04', 0, 0, 1, '2018-08-04 06:49:10', NULL, NULL, NULL, 1, 10, '288', 66, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(64, 1, 5, 'Eid Ul Adha chaldal offer save up to 30%', 'Eid_Ul_Adha_chaldal_offer_save_up_to_30%_64', '10', '2018-08-09', '<p>deyg.gergt,ertr&#39;/et.ert&#39;;rt&#39;rt&#39;/r.treterter</p>\r\n\r\n<p>tre</p>\r\n\r\n<p>trtr</p>\r\n', '', 'offer_64.jpg', 2, '1', 'abc,efg.gdf,', '2018-08-09', '2019-04-30', '2018-08-10', 0, 0, 1, '2018-08-19 14:08:43', NULL, NULL, NULL, 1, 0, '', 31, 0, 0, '', 0, 1, '2019-05-07 04:57:08', NULL),
(65, 1, 2, '10% Discount on Winner Ice Box', '10%_Discount_on_Winner_Ice_Box_65', '12', '2018-08-12', '<p>Winner Ice Box</p>\r\n\r\n<p>মাংস ফ্রেশ পরিবহনে উইনার বক্স নিয়ে এলো ঝামেলাবিহীন সমধান<br />\r\n১০% ছাড়</p>\r\n\r\n<p>এই অফার টি চলবে Best Buy এর সকল আউটলেটে<br />\r\nকলঃ ০৯৬১৩৭৩৭৭৭৭</p>\r\n', '', 'offer_65.jpg', 5, '4', 'ice box,', '2018-08-12', '2019-01-31', '2018-08-22', 0, 0, 1, '2018-08-19 14:09:15', NULL, NULL, NULL, 1, 10, '10', 33, 0, 0, '', 0, 1, '2019-05-07 04:57:08', NULL),
(66, 1, 19, 'Get 200 tk OFF on first Female fashion product purchase', 'Get_200_tk_OFF_on_first_Female_fashion_product_purchase_66', '1', '2018-08-14', '<p>details</p>\r\n', '', 'offer_66.png', 2, '3', 'notag,', '2018-08-14', '1970-01-01', '2018-08-14', 0, 0, 1, '2018-08-14 18:02:26', NULL, NULL, NULL, 1, 0, '20% OFF', 65, 0, 0, '', 1, 2, '2019-05-07 04:57:08', NULL),
(67, 1, 12, '2GB at Tk 38 - validity 2 Days', '2GB_at_Tk_38_-_validity_2_Days_67', '1', '2018-08-15', '<p><strong>Terms &amp; Conditions:</strong></p>\r\n\r\n<ul>\r\n	<li>2GB for 2 Days (Activation+1) days at Taka 38</li>\r\n	<li>Activation code: *121*3242#</li>\r\n	<li>This campaign will run until further notice</li>\r\n	<li>Offer applicable for all GP customers</li>\r\n	<li>No&nbsp;auto renew&nbsp;feature&nbsp;applicable</li>\r\n	<li>Unused data volume will be carried forward if the customer purchases the same campaign offer (2GB at 38 Taka) within the active validity period</li>\r\n	<li>Dial *121*1*4# to know internet balance</li>\r\n	<li>To cancel your internet offer, dial *121*3041#</li>\r\n	<li>This offer is not valid for Skitto customers</li>\r\n	<li>All the&nbsp;<a href=\"https://www.grameenphone.com/personal/internet/packages\" target=\"_blank\">terms and conditions</a>&nbsp;of internet packs will also be applicable here</li>\r\n</ul>\r\n', '', 'offer_67.jpg', 5, '7', 'internet offers, ', '2018-08-15', '2018-12-31', '2018-12-31', 0, 0, 1, '2018-08-15 14:51:21', NULL, NULL, NULL, 1, 0, 'Internet Offer', 110, 0, 0, '', 0, 1, '2019-05-07 04:57:08', NULL),
(68, 1, 13, 'Less than 1paisa in all Operators', 'Less_than_1paisa_in_all_Operators_68', '1', '2018-08-15', '<h2>One Rate: Less than 1paisa in all Operators</h2>\r\n\r\n<p>Now you can call for less than 1 paisa/second from your Banglalink number to any operator. Just recharge either Tk. 139, Tk.79, or Tk. 39 and enjoy different validity period of an awesome tariff.</p>\r\n\r\n<p><strong>Offer Details:</strong></p>\r\n\r\n<ul>\r\n	<li>All Banglalink prepaid and Call &amp; Control customers are eligible for these offers of Tk.39, Tk.79 &amp; Tk.139 recharge</li>\r\n	<li>This special call rate is applicable for local numbers only</li>\r\n	<li>The special call rate will be activated instantly on recharging either Tk. 139, Tk.79 or Tk. 39</li>\r\n	<li>This special call rate will not apply to bonus/bundle minutes. In the instance of customers have bonus/bundle minutes, such minutes will be consumed first for calling</li>\r\n	<li>Customer can avail the offer multiple times during campaign period. On multiple recharges, longer validity will prevail</li>\r\n	<li>After the special tariff validity expires, the previous offer/package based tariff will re-apply</li>\r\n	<li>VAT+SD+SC are applicable on rates</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '<p><strong>Details at a glance:</strong></p>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>MRP/Recharge amount</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Any-Net Tariff</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Validity</strong></p>\r\n			</td>\r\n			<td>\r\n			<p><strong>Opt-Out Dial</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>39</p>\r\n			</td>\r\n			<td>\r\n			<p>0.9 paisa/ second</p>\r\n			</td>\r\n			<td>\r\n			<p>7 days</p>\r\n			</td>\r\n			<td>\r\n			<p>*166*39#</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>79</p>\r\n			</td>\r\n			<td>\r\n			<p>0.9 paisa/ second</p>\r\n			</td>\r\n			<td>\r\n			<p>15 days</p>\r\n			</td>\r\n			<td>\r\n			<p>*166*179#</p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>139</p>\r\n			</td>\r\n			<td>\r\n			<p>0.9 paisa/ second</p>\r\n			</td>\r\n			<td>\r\n			<p>30 days</p>\r\n			</td>\r\n			<td>\r\n			<p>*166*139#</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'offer_68.jpg', 5, '7', 'voice offer, telecom offers', '2018-08-15', '2018-11-10', '2018-12-30', 0, 0, 1, '2018-08-15 14:56:02', NULL, NULL, NULL, 1, 0, 'Voice Offer', 134, 0, 0, '', 1, 1, '2019-05-09 06:03:13', NULL),
(69, 1, 49, 'Return robi network get 10GB data @ tk 101', 'Return_robi_network_get_10GB_data___tk_101_69', '1', '2018-08-15', '<p><strong>Return Robi&rsquo;s 4.5 G Network and enjoy 10 GB @ Tk101</strong></p>\r\n\r\n<ul>\r\n	<li>5 GB for any use</li>\r\n	<li>5 GB for 4G use only</li>\r\n	<li>Validity 15 days (24 hrs)</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Other Notes</strong></p>\r\n\r\n<ul>\r\n	<li>To check eligibility, customers can send free SMS from any Robi number in the following format: A018XXXXXXXX to 8050 or can dial *8050# put silent number by following instruction.</li>\r\n	<li>Offer valid till further notice</li>\r\n	<li>Robi can close or add/remove conditions of the offer at any time with proper permission from concerned authority</li>\r\n</ul>\r\n\r\n<p><br />\r\nRead more at https://www.robi.com.bd/current-offers/win-back#GAPqtwbgO0wutoDR.99</p>\r\n', '<ul>\r\n	<li>Robi/authority reserves the right to change, modify, cancel/terminate/stop this competition/contest/ quiz/campaign without assigning any reason whatsoever</li>\r\n	<li>Any decision in relation to the contest/ campaign/competition given by the Authority/Robi shall be considered and or construed as full and final</li>\r\n	<br />\r\n	<li>Read more at https://www.robi.com.bd/current-offers/win-back#GAPqtwbgO0wutoDR.99</li>\r\n</ul>\r\n', 'offer_69.jpg', 5, '7', 'return offer, win back,', '2018-08-15', '2018-08-31', '2019-01-31', 0, 0, 1, '2018-08-19 14:11:08', NULL, NULL, NULL, 1, 0, '10GB Data', 176, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(70, 1, 3, '100 Tk Discount on Purchase Over 400 Tk at SILKYGIRL', '100_Tk_Discount_on_Purchase_Over_400_Tk_at_SILKYGIRL_70', '1', '2018-08-15', '<p>here are details</p>\r\n', '', NULL, 3, '1', 'crazy offer,', '2018-08-15', '2018-08-17', '2018-08-30', 0, 0, 1, '2018-08-15 15:05:55', NULL, NULL, NULL, 0, 0, '100', 71, 0, 0, '', 1, 2, '2019-05-07 04:57:08', NULL),
(71, 1, 65, ' Mega Deal Enjoy 25% off on entire menu.', 'Mega_Deal_Enjoy_25%_off_on_entire_menu._71', '1', '2018-12-26', '<h2>Pan Pizza-Super Supremes</h2>\r\n\r\n<h2>Pan Pizza-Supremes</h2>\r\n\r\n<h2>Pan Pizza-Favorites</h2>\r\n\r\n<h2>Pan Pizza- Veg Delights</h2>\r\n\r\n<h2>Pasta</h2>\r\n\r\n<h2>Salad</h2>\r\n\r\n<h2>Appetisers</h2>\r\n\r\n<h2>Risotto</h2>\r\n\r\n<h2>Cheesy Bites</h2>\r\n\r\n<h2>Golden Surprise</h2>\r\n\r\n<h2>Hand Stretched Thin</h2>\r\n\r\n<p>&nbsp;</p>\r\n', '<p>on all get 25% OFF for unlimited time, untill stop.</p>\r\n', 'offer_71.jpg', 4, '3', 'pizza', '2018-12-26', '0000-00-00', '0000-00-00', 0, 0, 1, '2018-12-26 16:11:30', NULL, NULL, NULL, 1, 25, '25% OFF', 22, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(72, 1, 67, 'Get 100 TK OFF on first purchase of set lingerie', 'Get_100_TK_OFF_on_first_purchase_of_set_lingerie_72', '1', '2019-01-03', '<p>&lt;div class=&quot;col-sm-6&quot;&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;div class=&quot;well &quot;&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;h2&gt;{{ text_new_customer }}&lt;/h2&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;p&gt;&lt;strong&gt;{{ text_register }}&lt;/strong&gt;&lt;/p&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;p&gt;{{ text_register_account }}&lt;/p&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;a href=&quot;{{ register }}&quot; class=&quot;btn btn-primary&quot;&gt;{{ button_continue }}&lt;/a&gt;&lt;/div&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/div&gt;</p>\r\n', '<p>&lt;div class=&quot;col-sm-6&quot;&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;div class=&quot;well &quot;&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;h2&gt;{{ text_new_customer }}&lt;/h2&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;p&gt;&lt;strong&gt;{{ text_register }}&lt;/strong&gt;&lt;/p&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;p&gt;{{ text_register_account }}&lt;/p&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;a href=&quot;{{ register }}&quot; class=&quot;btn btn-primary&quot;&gt;{{ button_continue }}&lt;/a&gt;&lt;/div&gt;<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/div&gt;</p>\r\n', 'offer_72.jpg', 5, '1', 'men boxer, CK boxer', '2019-01-03', '0000-00-00', '0000-00-00', 0, 0, 1, '2019-01-03 05:04:54', NULL, NULL, NULL, 1, 100, '-100', 121, 0, 0, '', 1, 1, '2019-05-09 07:01:11', NULL),
(73, 1, 2, 'OnePlus 7 Pro - Smartphone - 6.01&quot; - 8GB RAM - 128GB ROM - 20MP Camera - Midnight Black Get 10% OFF', 'OnePlus_5T_-_Smartphone_Get_10%_OFF_1', '13', '2018-06-04', '<h2>OnePlus 7 Pro - Smartphone - 6.01&quot; - 8GB RAM - 128GB ROM - 20MP Camera - Midnight Black</h2>\r\n\r\n<p>Key Features</p>\r\n\r\n<ul>\r\n	<li>6.01&quot; 1080P AMOLED Display</li>\r\n	<li>Qualcomm Snapdragon 835</li>\r\n	<li>Octa-core 10nm up to 2.45GHz Processor</li>\r\n	<li>GPU: Adreno 540</li>\r\n	<li>8GB RAM and 128GB ROM</li>\r\n	<li>16MP+20MP Dual Rear Camera</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '', '1444287941oneplust.jpg', 5, '2', 'mobile, oneplus,', '2018-06-04', '2018-06-04', '2018-06-04', 0, 0, 1, '2018-08-03 07:32:37', '', '', '', 1, 10, '10% OFF', 46, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL),
(74, 1, 2, 'xyzzzzzzzz', 'OnePlus_5T_-_Smartphone_Get_10%_OFF_1', '13', '2018-06-04', '<h2>OnePlus 7 Pro - Smartphone - 6.01&quot; - 8GB RAM - 128GB ROM - 20MP Camera - Midnight Black</h2>\r\n\r\n<p>Key Features</p>\r\n\r\n<ul>\r\n	<li>6.01&quot; 1080P AMOLED Display</li>\r\n	<li>Qualcomm Snapdragon 835</li>\r\n	<li>Octa-core 10nm up to 2.45GHz Processor</li>\r\n	<li>GPU: Adreno 540</li>\r\n	<li>8GB RAM and 128GB ROM</li>\r\n	<li>16MP+20MP Dual Rear Camera</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n', '', '1444287941oneplust.jpg', 5, '2', 'mobile, oneplus,', '2018-06-04', '2018-06-04', '2018-06-04', 0, 0, 1, '2018-08-03 07:32:37', '', '', '', 1, 10, '10% OFF', 46, 0, 0, '', 1, 1, '2019-05-07 04:57:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `marchant_details`
--

CREATE TABLE `marchant_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `profile_headline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ad_banner_headline` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `division_id` int(2) NOT NULL DEFAULT '0',
  `district_id` int(3) NOT NULL DEFAULT '0',
  `area_id` int(5) NOT NULL DEFAULT '0',
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `about_us` text COLLATE utf8_unicode_ci,
  `support_email` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_link` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin_link` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_link` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pinterest_link` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram_link` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_link` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `map_long` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `map_lati` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `marchant_details`
--

INSERT INTO `marchant_details` (`id`, `user_id`, `profile_headline`, `ad_banner_headline`, `division_id`, `district_id`, `area_id`, `address`, `about_us`, `support_email`, `facebook_link`, `twitter_link`, `linkedin_link`, `gplus_link`, `pinterest_link`, `instagram_link`, `youtube_link`, `updated_at`, `map_long`, `map_lati`) VALUES
(1, 2, '', '', 3, 1, 3, 'Dhaka', '<p>www.daraz.com.bd</p>\r\n', '', '', '', '', '', '', '', '', '2018-08-01 06:17:10', '', ''),
(2, 3, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-04 05:26:30', '', ''),
(3, 4, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-04 05:27:10', '', ''),
(4, 5, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-04 05:27:55', '', ''),
(5, 6, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-04 05:28:29', '', ''),
(6, 7, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-04 05:28:54', '', ''),
(7, 8, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-04 05:34:32', '', ''),
(8, 9, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-04 06:50:21', '', ''),
(9, 10, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-04 07:02:10', '', ''),
(10, 11, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-04 08:20:21', '', ''),
(11, 12, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-04 08:25:15', '', ''),
(12, 13, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-04 08:34:30', '', ''),
(13, 14, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-05 00:31:13', '', ''),
(14, 15, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-05 01:49:14', '', ''),
(15, 16, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-07 00:44:41', '', ''),
(16, 17, '', '', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2018-06-07 04:24:48', '', ''),
(17, 18, '', '', 3, 1, 3, 'Dhaka', '<p>www.priyoshop.com</p>\r\n', '', '', '', '', '', '', '', '', '2018-08-02 04:21:39', '', ''),
(18, 19, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-21 06:40:20', '', ''),
(19, 42, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-12 00:44:07', '', ''),
(20, 43, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-12 01:33:53', '', ''),
(21, 44, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-13 10:52:03', '', ''),
(22, 45, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-14 11:46:24', '', ''),
(23, 49, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-14 23:08:06', '', ''),
(24, 63, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-13 00:37:13', '', ''),
(25, 65, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-26 10:03:08', '', ''),
(26, 67, 'Lingeire & Beauty Store BD.', 'Get exclsive collection on female fashion', 2, 43, 2, 'neaz mansion, 36 sughondha R/A, Panchlaish, Chittagong.', '<p>In-Depth Reviews</p>\r\n\r\n<p>Generate as many website reviews as you like and track your progress in real time.<br />\r\n<br />\r\nWith our in-depth website analysis learn how to improve your website rankings &amp; online visibility through SEO, social media, usability and much more. Track &amp; fix your website&rsquo;s weaknesses! Try WooRank&#39;s Website Review Tool with a 14-day Trial!</p>\r\n', 'www.chupti.com', 'https://www.facebook.com/chuptibd', 'https://www.facebook.com/chuptibd', 'https://www.facebook.com/chuptibd', NULL, NULL, 'https://www.facebook.com/chuptibd', 'https://www.facebook.com/chuptibd', '2019-01-02 11:06:42', '', ''),
(27, 86, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-02 10:25:56', '', ''),
(28, 89, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-09 09:25:15', '', ''),
(29, 90, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-09 09:27:49', '', ''),
(30, 91, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-09 09:36:02', '', ''),
(31, 92, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-11 06:07:30', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `offer_highlights`
--

CREATE TABLE `offer_highlights` (
  `id` int(11) NOT NULL,
  `offer_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `offer_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `offer_type` int(11) NOT NULL,
  `expire_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=pending;1=active;2=block;',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `offer_highlights`
--

INSERT INTO `offer_highlights` (`id`, `offer_id`, `offer_title`, `offer_type`, `expire_date`, `status`, `created_at`, `updated_at`) VALUES
(1, '1,2,3', 'My Highlights', 0, '2018-08-31 00:00:00', 2, '2018-08-01 12:56:46', '2019-01-02 23:09:14'),
(2, '3,5,6,7', 'New Offer', 1, '2018-08-09 00:00:00', 2, '2018-08-01 12:57:14', '2018-08-19 08:47:54'),
(3, '10,12', 'Offer Highlihts', 0, '2018-08-15 00:00:00', 2, '2018-08-01 12:57:37', '2018-08-19 08:02:19'),
(4, '7,8,9', 'New Offer2', 1, '2018-08-09 00:00:00', 2, '2018-08-01 12:59:12', '2018-08-19 08:02:26'),
(5, '8,9,27,67,68', 'Deal Offer', 1, '2018-08-03 00:00:00', 2, '2018-08-01 08:11:06', '2018-09-04 10:53:45'),
(6, '1,2,3,5,10,12', 'Korbani Offer', 0, '2018-08-31 00:00:00', 2, '2018-08-01 08:13:05', '2019-01-02 23:09:06'),
(7, '5,6,7,8,9', 'my deal', 1, '2018-08-23 00:00:00', 2, '2018-08-09 13:06:09', '2018-09-04 10:53:34'),
(8, '72', 'New Year Offer', 1, '2019-01-10 00:00:00', 2, '2019-01-03 05:08:25', '2019-01-02 23:09:10'),
(9, '72', 'New Year Offer', 0, '2019-01-10 00:00:00', 1, '2019-01-03 05:09:58', '2019-01-02 23:09:58');

-- --------------------------------------------------------

--
-- Table structure for table `offer_sms`
--

CREATE TABLE `offer_sms` (
  `sms_id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL DEFAULT '0',
  `sms` text COLLATE utf8_unicode_ci,
  `mobile_number` text COLLATE utf8_unicode_ci,
  `is_multiple` tinyint(1) NOT NULL DEFAULT '0',
  `add_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `offer_sms`
--

INSERT INTO `offer_sms` (`sms_id`, `offer_id`, `sms`, `mobile_number`, `is_multiple`, `add_date`) VALUES
(1, 59, 'Get awesome eid special OFF upto 2000tk at chupti. come and get this offer.', '8801812377032', 0, '2018-07-30 15:14:43'),
(2, 70, 'Get upto 100tk OFF on all purchase on Deshioffer for very few days', NULL, 0, '2018-08-15 15:05:55'),
(3, 71, 'Picaboo Sms Offer', NULL, 0, '2018-08-30 06:29:06');

-- --------------------------------------------------------

--
-- Table structure for table `offer_type`
--

CREATE TABLE `offer_type` (
  `offer_id` int(11) NOT NULL,
  `offer_type_tilte` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_st` int(1) DEFAULT NULL,
  `add_date` datetime DEFAULT NULL,
  `add_by` int(11) DEFAULT NULL,
  `offer_type` int(11) DEFAULT '1' COMMENT '1=external deal 2= in-house deal',
  `color_code` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `offer_type`
--

INSERT INTO `offer_type` (`offer_id`, `offer_type_tilte`, `offer_img`, `offer_st`, `add_date`, `add_by`, `offer_type`, `color_code`) VALUES
(1, 'Exclusive Offer', '', 1, '2018-06-04 06:17:26', 1, 0, '#FF2D00'),
(2, 'Gift Offer', '', 1, '2018-06-04 06:18:20', 1, 0, '#9E2BFF'),
(3, 'Eid Special', '', 1, '2018-06-04 06:18:42', 1, 0, '#FF6C6C'),
(4, '1 & 1 Offer', '', 1, '2018-06-04 06:19:20', 1, 0, '#1396FF'),
(5, 'Cashback Offer', '', 1, '2018-06-04 06:19:45', 1, 0, '#A7FF6E'),
(6, 'Flash Sale', '', 1, '2018-06-04 06:20:17', 1, 0, '#FF0097'),
(7, 'Special Offer', '', 1, '2018-06-05 00:21:38', 1, 0, '#634C9F'),
(8, 'GP Star Offer', '', 1, '2018-06-05 00:22:29', 1, 0, '#FF7F5D'),
(9, 'Crazy Eid Offer', '', 1, '2018-06-05 00:23:53', 1, 0, '#15C6FF'),
(10, 'Free Shipping', '', 1, '2018-06-08 03:56:47', 1, 0, '#D3D3D3'),
(11, 'new offer title test', 'offer_11.jpg', 1, '2018-07-30 14:41:30', 1, 0, '42BAFF'),
(12, 'Korbani Offer', NULL, 1, '2018-08-12 06:45:41', 1, 1, '#DBFFDD'),
(13, 'Direct Deal', NULL, 1, '2018-08-12 06:45:41', 1, 1, '#DBFFDC');

-- --------------------------------------------------------

--
-- Table structure for table `online_deal`
--

CREATE TABLE `online_deal` (
  `online_deal_id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL DEFAULT '0',
  `ref_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `online_deal`
--

INSERT INTO `online_deal` (`online_deal_id`, `offer_id`, `ref_link`, `add_date`) VALUES
(1, 1, 'https://www.daraz.com.bd/oneplus-5t-smartphone-6.01-8gb-ram-128gb-rom-20mp-camera-midnight-black-453287.html', '2018-06-04 06:37:48'),
(2, 2, 'https://ankur.com.bd/product/gucci-ladies-replica-watch/', '2018-06-04 06:53:26'),
(3, 3, 'https://ankur.com.bd/product/tissot-watch/', '2018-06-04 06:58:27'),
(4, 6, 'https://ajkerbuy.com/product/digital-sensor-speaker/', '2018-06-04 08:22:47'),
(5, 12, 'https://www.grameenphone.com/star-program/special-offers/veni-vidi-vici', '2018-06-04 08:39:42'),
(6, 13, 'https://www.daraz.com.bd/veet-cream-15g-buy-1-get-1-free-357698.html', '2018-06-04 09:43:33'),
(7, 14, 'https://www.daraz.com.bd/tresemme-ionic-strength-shampoo-and-conditioner-combo-322769.html', '2018-06-04 09:45:40'),
(8, 15, 'https://www.daraz.com.bd/real-techniques-core-collection-4-pieces-makeup-brush-set-copper-383597.html', '2018-06-04 09:47:47'),
(9, 18, 'https://chaldal.com/offers', '2018-06-04 10:32:27'),
(10, 20, 'https://chaldal.com/offers', '2018-06-04 10:37:49'),
(11, 21, 'https://www.othoba.com/mithai-ramadan-offer-package-3', '2018-06-04 10:40:41'),
(12, 23, 'https://www.bagdoom.com/men/shoes/maverick-men-s-loafer-96431a42.html', '2018-06-04 12:24:46'),
(13, 25, 'https://www.bagdoom.com/mobile-phone-card-money-holder-1881-d.html', '2018-06-04 12:34:01'),
(14, 28, 'https://www.grameenphone.com/star-program/special-offers/bread-beyond', '2018-06-05 00:29:50'),
(15, 32, 'https://www.grameenphone.com/shop/phones/product/oppo-f7-pro#tab-package-specification1', '2018-06-07 00:08:42'),
(16, 33, 'https://www.pickaboo.com/campaigns/mobile-mela/anker-soundcore-mini-bluetooth-speaker.html', '2018-06-07 00:57:16'),
(17, 35, 'https://www.trendy-tracker.com/no-1-f6-smartwatch.html', '2018-06-07 04:41:54'),
(18, 36, 'https://www.pickaboo.com/electronics-appliances/home-appliance/air-conditioner.html/?utm_source=Facebook&utm_medium=banner&utm_campaign=AC+Sale', '2018-06-08 04:02:53'),
(19, 37, 'https://www.trendy-tracker.com/xiaomi-amazfit-bip-price-in-bangladesh.html', '2018-06-08 04:08:07'),
(20, 40, 'https://priyoshop.com/unbeatableprice', '2018-06-14 00:44:14'),
(21, 41, 'https://priyoshop.com/unbeatableprice', '2018-06-14 00:45:51'),
(22, 43, 'https://www.othoba.com/dettol-handwash-family-pack-atl00042', '2018-07-10 00:56:05'),
(23, 44, 'https://www.trendy-tracker.com/huawei-color-band-a2-price-bd.html', '2018-07-21 11:17:45'),
(24, 45, 'https://kiksha.com/umidigi-s2-lite-smartphone-4gb-32gb-pre-book', '2018-07-22 02:32:29'),
(25, 61, 'https://www.pickaboo.com/mobile-tablet/mobile-phone/huawei/huawei-nova-3i-4gb-128gb.html?utm_source=facebook&utm_medium=single+image+post&utm_campaign=nova+3i+launch', '2018-07-31 07:35:36'),
(26, 63, 'https://www.othoba.com/amass-closet-4-drawer-sunflower-918090', '2018-08-04 06:49:11'),
(27, 71, 'https://www.foodpanda.com.bd/chain/cl3hs/pizza-hut', '2018-12-26 16:09:53'),
(28, 71, 'https://www.foodpanda.com.bd/chain/cl3hs/pizza-hut', '2018-12-26 16:11:30');

-- --------------------------------------------------------

--
-- Table structure for table `order_transaction`
--

CREATE TABLE `order_transaction` (
  `id` int(11) NOT NULL,
  `transaction_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auto_gen_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `total_amount` double NOT NULL DEFAULT '0',
  `total_paid` int(11) NOT NULL DEFAULT '0',
  `paid_by` int(11) NOT NULL DEFAULT '0' COMMENT '1-cash 2-check 3-bkash',
  `bank_acc_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `check_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bkash_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `check_pass_date` datetime DEFAULT NULL,
  `tx_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_transaction`
--

INSERT INTO `order_transaction` (`id`, `transaction_no`, `auto_gen_id`, `order_id`, `total_amount`, `total_paid`, `paid_by`, `bank_acc_no`, `check_no`, `bkash_no`, `check_pass_date`, `tx_id`, `created_at`, `updated_at`, `status`) VALUES
(1, '45672345', '123456', 1, 200, 0, 3, NULL, NULL, '01673438726', NULL, 'fsdfwsfsdf33423423dfsd', NULL, '2019-05-30 08:28:28', 1),
(2, '45672345', 'cjkcjadjkc\r\n', 2, 200, 0, 3, NULL, NULL, '01673438726', NULL, 'fsdfwsfsdf33423423dfsd', NULL, '2019-05-30 08:28:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `post_offer_catagory`
--

CREATE TABLE `post_offer_catagory` (
  `post_of_type_id` int(11) NOT NULL,
  `offer_id` int(11) DEFAULT '0',
  `of_type` int(11) DEFAULT '0',
  `add_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_offer_catagory`
--

INSERT INTO `post_offer_catagory` (`post_of_type_id`, `offer_id`, `of_type`, `add_date`) VALUES
(1, 1, 2, '2018-06-04 06:37:48'),
(2, 2, 1, '2018-06-04 06:53:26'),
(3, 3, 1, '2018-06-04 06:58:27'),
(4, 4, 1, '2018-06-04 07:04:27'),
(5, 5, 1, '2018-06-04 07:06:39'),
(6, 6, 2, '2018-06-04 08:22:47'),
(7, 7, 2, '2018-06-04 08:27:28'),
(8, 8, 3, '2018-06-04 08:30:01'),
(9, 9, 3, '2018-06-04 08:32:39'),
(10, 10, 3, '2018-06-04 08:36:15'),
(11, 11, 3, '2018-06-04 08:38:20'),
(12, 12, 3, '2018-06-04 08:39:42'),
(13, 13, 5, '2018-06-04 09:43:33'),
(14, 14, 5, '2018-06-04 09:45:40'),
(15, 15, 5, '2018-06-04 09:47:47'),
(16, 16, 5, '2018-06-04 10:23:54'),
(17, 17, 5, '2018-06-04 10:25:16'),
(18, 18, 8, '2018-06-04 10:32:27'),
(19, 19, 8, '2018-06-04 10:34:55'),
(20, 20, 8, '2018-06-04 10:37:49'),
(21, 21, 8, '2018-06-04 10:40:41'),
(22, 22, 8, '2018-06-04 10:42:13'),
(23, 23, 1, '2018-06-04 12:24:46'),
(24, 24, 1, '2018-06-04 12:28:01'),
(25, 25, 2, '2018-06-04 12:34:01'),
(26, 26, 2, '2018-06-04 12:36:23'),
(27, 27, 3, '2018-06-05 00:26:55'),
(28, 28, 3, '2018-06-05 00:29:50'),
(29, 29, 3, '2018-06-05 00:32:38'),
(30, 30, 3, '2018-06-05 00:34:51'),
(31, 31, 2, '2018-06-05 01:54:09'),
(32, 32, 2, '2018-06-07 00:08:42'),
(33, 33, 2, '2018-06-07 00:57:16'),
(34, 34, 2, '2018-06-07 04:29:27'),
(35, 35, 2, '2018-06-07 04:41:54'),
(36, 36, 4, '2018-06-08 04:02:53'),
(37, 37, 2, '2018-06-08 04:08:07'),
(38, 38, 2, '2018-06-08 04:19:04'),
(39, 39, 0, '2018-06-08 04:24:56'),
(40, 40, 2, '2018-06-14 00:44:14'),
(41, 41, 2, '2018-06-14 00:45:51'),
(42, 42, 1, '2018-07-09 05:49:09'),
(43, 43, 3, '2018-07-10 00:56:05'),
(44, 44, 2, '2018-07-21 11:17:45'),
(45, 45, 2, '2018-07-22 02:32:29');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `sliderid` int(11) NOT NULL,
  `slidertitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slider_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `adddate` date DEFAULT NULL,
  `status` int(1) NOT NULL,
  `slider_sl` int(11) NOT NULL,
  `slidertitle_bang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_bang` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`sliderid`, `slidertitle`, `slider_link`, `img`, `description`, `adddate`, `status`, `slider_sl`, `slidertitle_bang`, `description_bang`, `updated_at`) VALUES
(2, 'Eid Sale uptp 70% OFF', 'http://deshioffer.com/', '126220446slider_deshioffer3.png', 'Eid Sale', '2018-06-04', 1, 2, '', '', '2019-01-02 10:14:05'),
(3, 'Deshioffer Largest Offer Platform BD', '', '741383127slider_deshioffer.png', 'Largest offer', '2018-06-04', 1, 3, '', '', '2018-11-29 10:23:26'),
(5, 'Deshioffer All Over', '', NULL, 'Deshioffer besi offer', '2018-06-04', 2, 1, '', '', '2018-12-22 09:53:29'),
(6, 'Welcome to Deshioffer', '', 'Welcome_to_Deshioffer_6.png', 'Welcome to Deshioffer', '2018-12-22', 1, 1, '', '', '2018-12-22 09:56:52');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE `subscription` (
  `id` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subscribed_category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subscribed_store` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`id`, `email`, `subscribed_category`, `subscribed_store`, `created_at`, `updated_at`) VALUES
(1, 'xyz@gmail.com', '1', '2', '2018-08-29 13:55:40', '2018-08-29 07:55:54'),
(2, 'freejoy16@yahoo.com', '', '67', '2019-01-02 17:09:12', '2019-01-02 11:09:12');

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `tag_id` int(11) NOT NULL,
  `tag_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag_add_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`tag_id`, `tag_title`, `tag_add_date`) VALUES
(1, 'new tag1', '2018-07-25 17:50:57'),
(2, 'new tag2', '2018-07-25 17:51:19'),
(3, 'new tag3', '2018-07-25 18:08:21'),
(4, 'new tag4', '2018-07-25 18:08:38'),
(5, 'bangla', '2018-07-30 15:11:03');

-- --------------------------------------------------------

--
-- Table structure for table `target_people`
--

CREATE TABLE `target_people` (
  `offer_id` int(11) NOT NULL,
  `target_tilte` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target_st` int(11) NOT NULL DEFAULT '0',
  `add_date` datetime DEFAULT NULL,
  `add_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `target_ppl_ofr`
--

CREATE TABLE `target_ppl_ofr` (
  `of_tid` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL DEFAULT '0',
  `target_p_type` int(11) NOT NULL DEFAULT '0',
  `tdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `userlog`
--

CREATE TABLE `userlog` (
  `log_id` int(11) NOT NULL,
  `loginid` int(11) NOT NULL DEFAULT '0',
  `logintime` datetime DEFAULT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `userlog`
--

INSERT INTO `userlog` (`log_id`, `loginid`, `logintime`, `user_name`) VALUES
(1, 1, '2018-06-04 04:10:29', 'admin'),
(2, 1, '2018-06-04 04:59:31', 'admin'),
(3, 1, '2018-06-04 06:12:07', 'admin'),
(4, 1, '2018-06-04 08:09:05', 'admin'),
(5, 1, '2018-06-04 09:34:18', 'admin'),
(6, 1, '2018-06-04 09:35:53', 'admin'),
(7, 1, '2018-06-04 12:20:15', 'admin'),
(8, 1, '2018-06-05 00:14:16', 'admin'),
(9, 1, '2018-06-05 01:06:04', 'admin'),
(10, 1, '2018-06-05 01:45:59', 'admin'),
(11, 1, '2018-06-06 23:59:18', 'admin'),
(12, 1, '2018-06-07 04:22:54', 'admin'),
(13, 1, '2018-06-08 03:53:47', 'admin'),
(14, 1, '2018-06-09 03:17:17', 'admin'),
(15, 1, '2018-06-10 02:52:22', 'admin'),
(16, 1, '2018-06-10 10:16:06', 'admin'),
(17, 1, '2018-06-11 01:50:38', 'admin'),
(18, 1, '2018-06-11 03:53:29', 'admin'),
(19, 1, '2018-06-11 06:26:42', 'admin'),
(20, 1, '2018-06-11 10:52:13', 'admin'),
(21, 1, '2018-06-11 11:23:26', 'admin'),
(22, 1, '2018-06-12 00:18:50', 'admin'),
(23, 1, '2018-06-12 04:52:47', 'admin'),
(24, 1, '2018-06-14 00:23:40', 'admin'),
(25, 1, '2018-06-15 04:32:56', 'admin'),
(26, 1, '2018-06-15 12:44:43', 'admin'),
(27, 1, '2018-06-21 11:12:31', 'admin'),
(28, 1, '2018-06-21 11:13:36', 'admin'),
(29, 1, '2018-06-22 23:47:55', 'admin'),
(30, 1, '2018-06-26 02:57:16', 'admin'),
(31, 1, '2018-06-28 01:54:13', 'admin'),
(32, 1, '2018-06-28 01:54:53', 'admin'),
(33, 1, '2018-06-30 23:32:00', 'admin'),
(34, 1, '2018-07-02 23:31:52', 'admin'),
(35, 1, '2018-07-04 00:28:53', 'admin'),
(36, 1, '2018-07-05 08:58:45', 'admin'),
(37, 1, '2018-07-08 03:57:34', 'admin'),
(38, 1, '2018-07-09 00:06:01', 'admin'),
(39, 1, '2018-07-09 12:51:27', 'admin'),
(40, 1, '2018-07-09 12:51:40', 'admin'),
(41, 1, '2018-07-09 05:44:26', 'admin'),
(42, 1, '2018-07-09 12:17:57', 'admin'),
(43, 1, '2018-07-10 00:47:38', 'admin'),
(44, 1, '2018-07-10 23:34:38', 'admin'),
(45, 1, '2018-07-20 00:48:31', 'admin'),
(46, 1, '2018-07-21 06:26:32', 'admin'),
(47, 1, '2018-07-21 10:42:47', 'admin'),
(48, 1, '2018-07-21 14:05:23', 'admin'),
(49, 1, '2018-07-22 00:33:32', 'admin'),
(50, 1, '2018-07-22 02:29:37', 'admin'),
(51, 1, '2018-07-22 07:29:55', 'admin'),
(52, 1, '2018-07-26 06:11:23', 'admin'),
(53, 1, '2018-07-26 06:26:20', 'admin'),
(54, 1, '2018-07-27 23:34:28', 'admin'),
(55, 1, '2018-07-28 00:49:29', 'admin'),
(56, 1, '2018-07-28 01:39:11', 'admin'),
(57, 1, '2018-07-28 04:12:58', 'admin'),
(58, 1, '2018-07-28 23:10:43', 'admin'),
(59, 1, '2018-07-29 00:33:57', 'admin'),
(60, 1, '2018-07-29 01:47:53', 'admin'),
(61, 1, '2018-07-29 04:21:23', 'admin'),
(62, 1, '2018-08-01 03:15:25', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL DEFAULT '0',
  `district_id` int(11) NOT NULL DEFAULT '0',
  `area_id` int(11) NOT NULL DEFAULT '0',
  `dob` date DEFAULT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=male;2=female',
  `merital_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=single;2=married',
  `fav_product` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `liked_offer_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `division_id`, `district_id`, `area_id`, `dob`, `gender`, `merital_status`, `fav_product`, `liked_offer_id`, `updated_at`) VALUES
(1, 22, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-08-04 09:42:11'),
(2, 23, 2, 43, 1, '2018-08-04', 1, 1, '35', NULL, '2018-09-02 06:22:49'),
(8, 38, 2, 43, 1, '0000-00-00', 1, 1, '', NULL, '2018-08-07 04:27:31'),
(10, 40, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-08-07 05:23:49'),
(11, 41, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-08-11 04:43:29'),
(12, 46, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-08-14 11:56:42'),
(13, 47, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-08-14 12:44:19'),
(14, 48, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-08-14 12:53:52'),
(15, 50, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-08-14 23:30:12'),
(18, 53, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-08-16 07:48:17'),
(19, 54, 2, 43, 2, '0000-00-00', 1, 2, '', NULL, '2018-08-17 00:46:21'),
(20, 55, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-08-29 08:02:06'),
(22, 57, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-09-08 17:37:06'),
(23, 58, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-09-14 01:32:55'),
(24, 59, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-09-23 10:08:55'),
(25, 60, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-10-19 08:42:02'),
(26, 61, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-11-01 19:32:59'),
(27, 62, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-11-13 08:27:33'),
(28, 64, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-12-20 22:14:56'),
(29, 66, 0, 0, 0, NULL, 1, 1, '', NULL, '2018-12-31 10:51:58'),
(30, 68, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-01-08 12:29:33'),
(31, 69, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-01-13 22:07:36'),
(32, 70, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-01-20 13:02:22'),
(33, 71, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-01-24 09:58:56'),
(34, 72, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-03-14 13:30:27'),
(35, 73, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-03-20 18:17:36'),
(36, 74, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-03-29 19:05:09'),
(37, 75, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-04-07 06:50:54'),
(38, 76, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-04-07 06:52:15'),
(39, 77, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-04-07 07:03:46'),
(40, 78, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-04-07 08:16:47'),
(41, 79, 0, 0, 0, NULL, 1, 1, '38', NULL, '2019-04-08 08:32:54'),
(42, 80, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-04-09 07:06:09'),
(43, 81, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-04-09 07:21:48'),
(44, 82, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-04-09 07:55:47'),
(45, 83, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-04-09 08:38:28'),
(46, 84, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-04-09 08:42:24'),
(47, 85, 0, 0, 0, NULL, 1, 1, '38', NULL, '2019-04-09 11:59:15'),
(48, 85, 0, 0, 0, NULL, 1, 1, 'Z66_IP67_Waterproof_Smart_Bracelet_38', NULL, '2019-04-09 13:03:29'),
(49, 85, 0, 0, 0, NULL, 1, 1, 'Z66_IP67_Waterproof_Smart_Bracelet_38', NULL, '2019-04-09 13:03:32'),
(50, 85, 0, 0, 0, NULL, 1, 1, '38', NULL, '2019-04-09 13:05:05'),
(54, 87, 2, 49, 1, '2019-05-01', 2, 1, '', NULL, '2019-05-29 08:14:22'),
(56, 20, 4, 59, 0, '2019-05-14', 2, 1, '72,42', '', '2019-05-26 07:09:02'),
(57, 20, 4, 59, 0, '2019-05-14', 2, 1, '72,42', '', '2019-05-26 07:09:02'),
(58, 88, 0, 0, 0, NULL, 1, 1, '3', NULL, '2019-05-09 07:19:30'),
(59, 93, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-05-13 08:53:37'),
(60, 94, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-05-22 09:23:11'),
(61, 95, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-05-23 11:43:27'),
(62, 96, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-05-25 08:04:17'),
(63, 97, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-05-26 05:23:19'),
(64, 99, 0, 0, 0, NULL, 1, 1, '', NULL, '2019-05-27 06:55:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_login`
--

CREATE TABLE `user_login` (
  `loginid` int(150) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usertype` int(150) DEFAULT NULL COMMENT '1=Admin,2=Merchant,3=User',
  `verify_status` int(150) DEFAULT NULL COMMENT '1=active,0=Inactive',
  `activation_key` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activation_status` tinyint(1) NOT NULL DEFAULT '0',
  `is_publish` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=unpublish;1=publish;',
  `profile_pic` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'user_default.png',
  `cover_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_on_web` int(150) DEFAULT '1' COMMENT '1=yes,0=no,2=brand,3=brand and catagory both',
  `delete_status` int(150) DEFAULT NULL COMMENT '1=Delete from system,0=Final Delete',
  `is_brand` int(120) DEFAULT '1',
  `is_feature` int(11) NOT NULL DEFAULT '0' COMMENT '1=feature 0= unfeature ',
  `top_store` tinyint(1) NOT NULL,
  `total_post` decimal(10,2) DEFAULT NULL,
  `subscribed_category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subscribed_store` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fav_offer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `newsletter_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` decimal(10,2) DEFAULT NULL,
  `sms_balance` decimal(10,2) DEFAULT NULL,
  `website` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_login`
--

INSERT INTO `user_login` (`loginid`, `name`, `username`, `password`, `contact`, `email`, `usertype`, `verify_status`, `activation_key`, `activation_status`, `is_publish`, `profile_pic`, `cover_image`, `show_on_web`, `delete_status`, `is_brand`, `is_feature`, `top_store`, `total_post`, `subscribed_category`, `subscribed_store`, `fav_offer`, `newsletter_id`, `balance`, `sms_balance`, `website`, `add_date`) VALUES
(1, 'admin_1', 'admin', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '00000000000000', 'admin@gmail.com', 1, 1, NULL, 0, 1, 'admin.jpg', NULL, 1, NULL, NULL, 0, 0, NULL, '', '2', NULL, NULL, NULL, NULL, NULL, '2018-06-04 03:00:00'),
(2, 'Daraz_2', 'Daraz', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '018122222200', 'partner@daraz.com', 2, 1, NULL, 0, 1, 'Daraz_2.jpg', 'merchant_cover2.jpg', 0, NULL, 0, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, 'http://www.facebook.com', '2018-06-04 05:23:10'),
(3, 'Bagdoom_3', 'Bagdoom', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '1111111', 'partner@bagdoom.com', 2, 1, NULL, 0, 1, 'Bagdoom_3.PNG', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-04 05:26:30'),
(4, 'Ajkerdeal_4', 'Ajkerdeal', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '111111', 'partner@ajkerdeal.com', 2, 1, NULL, 0, 1, '481850700ajkerdeal.png', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-04 05:27:10'),
(5, 'Chaldal_5', 'Chaldal', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '111111', 'partner@chaldal.com', 2, 1, NULL, 0, 1, '2114389081chaldal.png', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-04 05:27:55'),
(6, 'Rokomari_6', 'Rokomari', 'a3pJZHJLOVZrRHFFZW1yd0xsLzVhdz09', '111111', 'partner@rokomari.com', 2, 1, NULL, 0, 1, '874158396rokomari.jpg', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-04 05:28:29'),
(7, 'Othoba_7', 'Othoba', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '111111', 'partner@othoba.com', 2, 1, NULL, 0, 1, '1422647687othoba.png', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-04 05:28:54'),
(8, 'Bkash_8', 'Bkash', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '111111', 'partner@bkash.com', 2, 0, NULL, 0, 1, '112555825bkash.png', NULL, 0, 1, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-04 05:34:32'),
(9, 'Ankur_9', 'Ankur', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '555555', 'partner@aunkur', 2, 1, NULL, 0, 1, '1080097837ankur.png', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-04 06:50:21'),
(10, 'Silkygirl_10', 'Silkygirl', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '1111111', 'partner@silkygirl.com', 2, 1, NULL, 0, 1, 'Silkygirl_10.jpg', NULL, 0, NULL, 0, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-04 07:02:10'),
(11, 'Ajkerbuy_11', 'Ajkerbuy', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '111111', 'partner@ajkerbuy.com', 2, 1, NULL, 0, 1, '1302436363noimage.png', NULL, 0, NULL, 0, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-04 08:20:21'),
(12, 'Grameenphone_12', 'Grameenphone', 'a3pJZHJLOVZrRHFFZW1yd0xsLzVhdz09', '111111', 'partner@grameenphone.com', 2, 1, NULL, 0, 1, '823087982gp.png', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-04 08:25:15'),
(13, 'Banglalink_13', 'Banglalink', 'a3pJZHJLOVZrRHFFZW1yd0xsLzVhdz09', '1111111', 'partner@banglalink.com', 2, 1, NULL, 0, 1, '838754368banglalink.png', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-04 08:34:30'),
(14, 'Royal_Cuisine_14', 'Royal Cuisine', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '111111', 'partner@royalcuisine.com', 2, 1, NULL, 0, 1, '227070437royalcuisine.png', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-05 00:31:13'),
(15, 'Kiksha_15', 'Kiksha', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '1111111', 'partner@kiksha.com', 2, 1, NULL, 0, 1, '1849681050kiksha.png', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-05 01:49:14'),
(16, 'Pickaboo', 'Pickaboo', 'dnlWYzBvWlNuY3lqYjhIZW5jZFNaSkZQK2EvbDdQOWhiRUZYaHEwR1QreEl6MnA4MXQvMTh2QkpHRTFkMkNodVJZZ3ZHbURPaEFGb0c2Q2IrdjJpUFlVYXVZMEE1TFVkRlBUWEVMaVpKdVJ2d3Y5aGpaY1FGTnVrdDk3d2JMWjhrcXZDcCtDb096UWo5NStEYkdIVWNFcGdBRExVbVF4bDFpV05JNUE4N3Fialc2T3hkUFUvcVlEYzVLQm', '111111', 'partner@pickaboo.com', 2, 1, NULL, 0, 1, '333798149pick.png', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-07 00:44:41'),
(17, 'Trendy-tracker_17', 'Trendy-tracker', 'ZVoyUllpVHRacXZVTWhjNFFpZ2JEampHeTFURisxNkZmV1JFWnBoK1FWTGtyTTBTWHY2UndJTlg1TlpwOEtFbw==', '880 1975 155556', 'partner@trendytracker.com', 2, 1, NULL, 0, 1, '351241579trendytracker.png', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-07 04:24:48'),
(18, 'PriyoShop_18', 'PriyoShop', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '111111', 'partner@priyoshop.com', 2, 1, NULL, 0, 1, '19917828390072813.png', NULL, 0, NULL, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-14 00:40:21'),
(19, 'Chupti_19', 'Chupti', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '01785000000', 'info@chupti.com', 2, 0, NULL, 0, 1, '67510737513335773_251353161899552_6749055785502583136_n.png', NULL, 0, 1, 1, 0, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-21 06:40:20'),
(20, 'user_20', 'user', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '012284658', 'user@gmail.com', 3, 0, NULL, 0, 1, 'merchant20.jpg', NULL, 1, 1, NULL, 0, 0, NULL, '9,1,15,21,13', '13,9,18', NULL, '4,5', NULL, NULL, NULL, '2018-08-04 11:41:00'),
(23, 'Tanvir_23', 'Tanvir', 'ZGJzQlBGektjNHpZTEVGR3dSWEpKZz09', '123457', 'tanvir@gmail.com', 3, 1, NULL, 0, 1, 'merchant23.jpg', NULL, 1, NULL, NULL, 0, 0, NULL, '8,12,13,5,6,7,9', '3,12,13,16,17,18', NULL, NULL, NULL, NULL, NULL, '2018-08-04 03:45:00'),
(42, 'RFL_42', 'RFL', 'bDhPNURsNXFVZWxpY1hPcVcrMFVGUT09', '11111113', 'rfl@deshioffer.com', 2, 0, NULL, 0, 0, 'merchant42.png', NULL, 1, 1, 1, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-12 06:44:00'),
(44, 'Amori_44', 'Deshioffer', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '343432432432', 'amori@deshioffer.com', 2, 0, NULL, 0, 0, 'merchant44.png', NULL, 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-13 04:52:00'),
(45, 'Deshioffer_45', 'Deshioffer', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '0178494756', 'info@deshioffer.com', 2, 0, NULL, 0, 1, 'merchant45.png', NULL, 0, 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-14 05:46:00'),
(46, 'test_46', 'test', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '01896758391', 'test@gmail.com', 3, 0, NULL, 0, 1, 'user46.png', NULL, 1, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-14 05:56:00'),
(47, 'ejoy_47', 'ejoy', 'YWwxY0lZaXVHR2VEdVRRdHFvbFBjUT09', '01785599133', 'freejoy16@gmail.com', 3, 0, 'tDqfUlk2C4ewKNWxsbou3ESBRLXYMg6T5vP8FHcjyVJ10Qpdmz', 0, 1, 'user_default.png', NULL, 1, 1, 1, 0, 0, NULL, '2,3', NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-14 12:44:19'),
(48, 'hello_48', 'hello', 'alRCeStkQjJRZGZYLzlJaVliek1hUT09', '23423432', 'gfxscalpea@gmail.com', 3, 0, 'P4JCbhYgqjroRuKIH0ms5EvNBV6xzf2t7QDw9L8UWSeGd3TnOX', 0, 1, 'user_default.png', NULL, 1, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-14 12:53:52'),
(49, 'Robi_49', 'Robi', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '018138893893', 'robi@gmail.com', 2, 1, NULL, 0, 1, 'merchant49.png', NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-15 05:08:00'),
(50, 'Aman_50', 'Aman', 'MFhXN0VIQW1zRTRpOGVLbGp1SGNVQT09', '01677003893', 'aman.rabby@gmail.com', 3, 1, 'ZnPfpEs046jR9FiergqxNSdHJbyUtaOkTCu3ImAzQVXBMwlc1h', 1, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, '49', NULL, NULL, NULL, NULL, NULL, '2018-08-14 23:30:12'),
(53, 'xyz_53', 'xyz', 'ZGJzQlBGektjNHpZTEVGR3dSWEpKZz09', '8801812377032', 'tanvirnstu@gmail.com', 3, 1, 'fLD1X7iKoMqws4YpknO2WAHEZztar6e8I5UQv0Cmlc9xduRGyj', 1, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-16 07:48:17'),
(54, 'Alauddin_54', 'Alauddin', 'ZlIxR3JXc3BEVEpHQjIvcVJSVU95QT09', '8801785599133', 'freejoy16@gmail.com', 3, 0, 'w3dYieOoWq7D48zKbclhu6rMaPfFEtS5VQgCmI9v2NxpJHjBAU', 0, 1, 'user_default.png', NULL, 1, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-17 00:43:23'),
(55, 'tanvir_55', 'tanvir', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801634734658', 'xy@gmail.com', 3, 1, 'QWZpj1wxz0dlR68hVEfuYkJGTaMy3gOL2mBbv9SFc4tKrDNXPH', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-29 08:02:06'),
(57, '_57', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 0, 'jCVNlKyqDobeMHLxt18is0dAIEhWrmGuSUaz53Rfvn2PTc6FYk', 0, 1, 'user_default.png', NULL, 1, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-08 17:37:06'),
(58, '_58', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 0, 'mRWud82c6zDZlB9k1wgO0XrbChS3EVUjqiLQoxTytfNvIA74Fa', 0, 1, 'user_default.png', NULL, 1, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-14 01:32:55'),
(59, '_59', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 0, 'FPh6miEANH4z0rxWUuMjdvq9tBK3eI2kVf7lZDs8g5YJyGQpoX', 0, 1, 'user_default.png', NULL, 1, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-09-23 10:08:55'),
(60, '_60', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 0, 'Ip9sulQHncCidv6XrZYE4O0gzPUSaJob3ewM5f7KjABxVRG8ND', 0, 1, 'user_default.png', NULL, 1, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-19 08:42:02'),
(61, '_61', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 0, '75kC6rYPKTifI8EmO3NtbBd2hWAVjZDLXS1MzU9xapFHenwcvR', 0, 1, 'user_default.png', NULL, 1, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-01 19:32:59'),
(62, '_62', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 0, '1V3kb2WUFtjC4HpvsJSPqxX6REcf7DlzTionw58ygK9GZLNrQB', 0, 1, 'user_default.png', NULL, 1, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-13 08:27:33'),
(63, 'rcreation_63', 'rcreation', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '01816306190', 'ratulking@gmail.com', 2, 1, NULL, 0, 1, 'merchant63.png', NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-13 06:37:00'),
(64, '_64', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 0, 'Xrisozlvb9acJFNuTLI8RPWk0HGSqApdEjQyUD62Vem15fB4KY', 0, 1, 'user_default.png', NULL, 1, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-20 22:14:56'),
(65, 'Foodpanda_65', 'Foodpanda', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '000', 'info@foodpanda.com', 2, 1, NULL, 0, 1, 'merchant65.png', NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-26 04:03:00'),
(66, '_66', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 0, '8zje95vRl7qZuKcdJ0YiDp26obQWHSFwn3gBVUf1tyNIaXGTLk', 0, 1, 'user_default.png', NULL, 1, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-31 10:51:58'),
(67, 'chupti_67', 'chupti', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '1625343047', 'support@chupti.com', 2, 1, NULL, 0, 1, 'merchant67.png', NULL, 0, NULL, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-02 04:24:00'),
(68, '_68', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 1, 'rOcixSaMJeTEd0PpDkVKBXhznLyogqjNZ8GvU49tQ7WH5Rw13u', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-08 12:29:32'),
(69, '_69', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 1, 'Fr8uS1lywOZWq56YmnsRJbAQPTHc9340f7IvoeptKxjCBDd2kN', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-13 22:07:36'),
(70, '_70', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 1, 'olrwKO5JFQYAUiW7ZeunhRtSDTHjmNya90dMbGsfk3C1EVzILX', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-20 13:02:22'),
(71, 'Jadroo_E-commerce_Ltd___71', 'Jadroo E-commerce Ltd. ', 'UGdYMDVXbEQwcGtBZEYvYW5CUmVQZz09', '8801714130954', 'jadrooecom@gmail.com', 3, 1, 'zVRWjvEiDTJl6GK9H7MwXS3UNBFPfxteAsQdroaLOpyCuIY18h', 1, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-01-24 09:58:56'),
(72, '_72', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 1, 'tn71VFipxl4KQ2T5fHukwcYgEDX0ZOvys3orUqzda8PeNJSRWM', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-14 13:30:27'),
(73, '_73', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 1, 'yvpNAEx0teFSznkTMRC9ZW6jo4rLQmfOiJXgu1PaUlBqDc7dHK', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-20 18:17:36'),
(74, '_74', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', NULL, 3, 1, 'EL3QNDTXUhYdJ1VKG2RC9OsviMrqlzAZtu50pFfkjny4Baboxc', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-29 19:05:09'),
(75, 'sufi_75', 'sufi', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801673438726', 'sufian@gmail.com', 3, 1, 'dwV6NWIpcoKnmiM4TQjYtkxHfRXzGPJZUAhrFsbauEe05COq8D', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-07 08:50:54'),
(76, 'sufi_76', 'sufi', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801673438725', 'mdyakub3028@gmail.com', 3, 1, 'hJzGQn1Cbq4cUZXr68etvVgHWsfF5pa3SoY9dLEu7DIyTOjRBx', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-07 08:52:15'),
(77, 'ssss_77', 'ssss', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801673428756', 's@gmail.com', 3, 1, 'GLpBxklmM1zQ8rKvIwWbTHXyc37PVhUZE4aufog09CnJAOqRFd', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-07 09:03:46'),
(78, 'sss_78', 'sss', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801673438721', 'admin@rcreation.com', 3, 1, 'o8rxZnbtj2HFPh6LpMRK9WBgEk7DQAmYisvfTJeXylqNcO1Czw', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-07 10:16:47'),
(79, 'aaa_79', 'aaa', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801234567890', 'admin@rcreation1.com', 3, 1, 'MtZ4Krx30mzdIe8VW2vqQBHcTwAf9YFpJ61nykS5OElDs7gjXU', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-07 10:20:33'),
(80, 'sfsdfsdfs_80', 'sfsdfsdfs', 'R0FxUXBpdFVEYmZBWjhMYjhNYkNIZz09', '8801673438723', 'aaa@gmail', 3, 1, 'bxoqVSRAeznt67l3pM2fLwdKJ9srGQUNCyH4FYmDj8051XkuZc', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-09 09:06:09'),
(81, 'aa_81', 'aa', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801673435678', 'admin@rcreation2.com', 3, 1, 'r8YlfiBSOTEkFy17Ia6qMQzsUogxGpeRAX54NZ30Jt2PHc9WCL', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-09 09:21:48'),
(82, 'asd_82', 'asd', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801673438972', 'sufi@gmail.com', 3, 1, 'BiPOLWyDr2cKzAGIUJbspYte3NZfjuT1lXa6gqhxVSm4vRwd8n', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-09 09:55:47'),
(83, 'as_83', 'as', 'YnQxVHNWRGV1aHM0WVZXTUhJc1JFUT09', '8801673438734', 'as@gmail.com', 3, 1, 'QuqcOYU8jSKea3rmPH5nFb7RdXT0gZfWCDsIkVNlzxt1MLEo6J', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-09 10:38:28'),
(84, 'aa3_84', 'aa3', 'R0FxUXBpdFVEYmZBWjhMYjhNYkNIZz09', '8801673338723', 'admin@rcre333ation.com', 3, 1, 'vj3egVAJFm09LW2rUDozbOIk4ctEGdZ8MnRwHYapy57fuTxliX', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-09 10:42:24'),
(85, 'as_85', 'as', 'R0FxUXBpdFVEYmZBWjhMYjhNYkNIZz09', '8801234567895', 'admin@rcre345ation.com', 3, 1, 'TLsCq1Ub0lJ54G87BMdPc3KFtRoOeW2HhAZES6VmukrigDjyY9', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-04-09 10:44:07'),
(86, 'fcrcrfcrfc_86', 'fcrcrfcrfc', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '123456785', 'rfcrcrfc@gmail.com', 2, 1, NULL, 0, 1, 'merchant86.PNG', NULL, 0, NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-02 04:25:00'),
(87, 'arif_87', 'arif', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801673435670', 'arif@gmail.com', 3, 1, 'NLDJO60kMWFpawbjvZxueslXhtfr278SdHiRQo5ATzmycIqPKE', 0, 1, 'Capture_PNG_87.PNG', NULL, 1, 0, 1, 0, 0, NULL, '3,7,9,6,15,5,13,21', '16,12,6,4', NULL, '4,5', NULL, NULL, NULL, '2019-05-04 11:12:35'),
(88, 'rasel_88', 'rasel', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801673435679', 'rasel@gmail.com', 3, 1, 'iTztYaWNEK0kSj4XsCrV5DZyM9p7IJQUL3vclu8fgGomF1dwnR', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, '3', NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-08 10:17:45'),
(91, 'asif_91', 'asif', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '453534', 'asik@gmail.com', 2, 1, NULL, 0, 1, 'merchant91.jpg', 'merchant_cover91.jpg', 1, NULL, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-09 03:36:00'),
(92, 'kamrul_92', 'kamrul', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '345435345', 'kmrul@gmail.com', 2, 1, NULL, 0, 1, 'merchant92.PNG', 'merchant_cover92.jpg', 1, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'http://www.facebook.com', '2019-05-11 12:07:00'),
(93, 'asdasds_93', 'asdasds', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801676435670', 'admdfdfdfin@rcreation.com', 3, 1, 'oHLbP8Ixl7yruzMAJnpdkD5WcjE90OeCwV6GKtqBZmShavTFXN', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-13 10:53:37'),
(94, 'asraf_94', 'asraf', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801673435370', 'asraf@gmail.com', 3, 1, 'VB92wWH8KsELDOTjiCvX147zadSglRu3Yfyp6UJMnFxoIhcmrN', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-22 11:23:10'),
(95, 'akbor_95', 'akbor', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801673435671', 'akbor@gmail.com', 3, 1, '2OQwJXSgimR4ynExd1uNKactZoHMef98z7FAkBUp0VGjrThLlD', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-23 13:43:26'),
(96, 'sakon_96', 'sakon', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801673435612', 'sakon@gmail.com', 3, 1, 'bMEyJUgPO8WXnhYSduvs4ktBZGqQ9jlTcNHewaF2zpVKLAD6fC', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-25 10:04:16'),
(97, 'sufiq_97', 'sufiq', 'bVlYZE03YXZScnZaZnBhaDJPQml0Zz09', '8801673435672', 'sddskfh@gmail.com', 3, 0, 'gWP7iroOMBDj3hUaCt0XxNEvdYGTqA2Lfz1e4pHJIlwyusS8kb97', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-26 07:23:19'),
(98, NULL, NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', '', 3, 0, NULL, 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-27 08:54:52'),
(99, '_99', NULL, 'M1Z6U3h2U1ljZ1QwQURmY2wvd2s4UT09', '880', '', 3, 0, 'ohTeRwlCUdmZzn8EQMOxNrGu6i3tJ1SV5ykK0fLcpAH27jsIB999', 0, 1, 'user_default.png', NULL, 1, 0, 1, 0, 0, NULL, '1,2', '1,2', NULL, NULL, NULL, NULL, NULL, '2019-05-27 08:55:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_offer_view`
--

CREATE TABLE `user_offer_view` (
  `id` int(11) NOT NULL,
  `user_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_id` int(11) NOT NULL DEFAULT '0',
  `created_at` date DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_offer_view`
--

INSERT INTO `user_offer_view` (`id`, `user_ip`, `offer_id`, `created_at`, `updated_at`) VALUES
(13, '::1', 23, '2019-05-08', '2019-05-08 04:37:25'),
(14, '::1', 42, '2019-05-08', '2019-05-08 05:30:17'),
(15, '::1', 38, '2019-05-08', '2019-05-08 07:53:52'),
(16, '::1', 72, '2019-05-08', '2019-05-08 09:41:21'),
(17, '127.0.0.1', 42, '2019-05-09', '2019-05-09 04:36:29'),
(18, '127.0.0.1', 23, '2019-05-09', '2019-05-09 05:33:04'),
(19, '127.0.0.1', 68, '2019-05-09', '2019-05-09 06:03:13'),
(20, '127.0.0.1', 38, '2019-05-09', '2019-05-09 06:40:11'),
(21, '127.0.0.1', 72, '2019-05-09', '2019-05-09 07:01:10'),
(22, '::1', 14, '2019-05-12', '2019-05-12 10:36:11'),
(23, '::1', 19, '2019-05-12', '2019-05-12 10:36:51');

-- --------------------------------------------------------

--
-- Table structure for table `user_order`
--

CREATE TABLE `user_order` (
  `id` int(11) NOT NULL,
  `gen_order_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `offer_id` int(11) NOT NULL DEFAULT '0',
  `total_amount` double NOT NULL DEFAULT '0',
  `paid_amount` double NOT NULL DEFAULT '0',
  `payment_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'unpaid',
  `discount` double NOT NULL DEFAULT '0',
  `vat` double NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_order`
--

INSERT INTO `user_order` (`id`, `gen_order_id`, `user_id`, `offer_id`, `total_amount`, `paid_amount`, `payment_status`, `discount`, `vat`, `created_at`, `updated_at`) VALUES
(1, 'mvsdkhvksdnvk644y8', 87, 73, 200, 0, 'unpaid', 0, 0, '2019-05-30 00:00:00', '2019-05-30 14:24:49'),
(2, 'aapopop34', 87, 74, 200, 0, 'unpaid', 0, 0, '2019-05-28 00:00:00', '2019-05-30 14:24:49');

-- --------------------------------------------------------

--
-- Table structure for table `user_rating`
--

CREATE TABLE `user_rating` (
  `id` int(11) NOT NULL,
  `store_offer_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `rating` double NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `review` text COLLATE utf8_unicode_ci,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1=offer rating 2- user rating',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=active 2= inactive',
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_rating`
--

INSERT INTO `user_rating` (`id`, `store_offer_id`, `user_id`, `rating`, `name`, `email`, `review`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 14, 20, 4.5, 'asss', 'admin@rcreation.com', 'adadasdas', 1, 1, NULL, '2019-05-06 08:01:55'),
(2, 14, 20, 4.5, 'dffdf', 'sdf@gmail.com', 'sgfgdfg', 1, 1, '2019-05-06 00:00:00', '2019-05-06 08:02:58'),
(3, 14, 20, 4, 'mobarak', 'admin@rcreation.com', 'dfgsfgsdgdfg', 1, 1, '2019-05-06 00:00:00', '2019-05-06 09:42:27'),
(4, 14, 87, 4, 'arif', 'admin@rcreation.com', 'ddfdf', 1, 1, '2019-05-06 00:00:00', '2019-05-06 11:19:06'),
(5, 14, 87, 4, 'asss', 'admin@rcreation.com', 'fdfsdfs', 1, 1, '2019-05-06 00:00:00', '2019-05-06 11:58:45'),
(6, 42, 20, 4.5, 'jknjk', 'admin@rcreation.com', 'dfsdgfss', 1, 1, '2019-05-07 00:00:00', '2019-05-07 04:54:57'),
(7, 42, 20, 4, 'asdasdas', 'admin@rcreation.com', 'asdasdasdas', 1, 1, '2019-05-07 00:00:00', '2019-05-07 04:58:15'),
(8, 42, 87, 3, 'sss', 'admin@rcreation.com', '', 1, 1, '2019-05-07 00:00:00', '2019-05-07 06:19:16'),
(9, 23, 87, 4, 'arif', 'admin@rcreation.com', 'good', 1, 1, '2019-05-08 00:00:00', '2019-05-08 04:39:00'),
(10, 23, 20, 3.5, 'user', 'admin@rcreation.com', 'bad', 1, 1, '2019-05-08 00:00:00', '2019-05-08 04:39:53'),
(11, 23, 20, 4, 'user', 'admin@rcreation.com', 'good', 1, 1, '2019-05-08 00:00:00', '2019-05-08 04:48:22'),
(12, 23, 88, 4, 'rasel', 'admin@rcreation.com', 'good', 1, 1, '2019-05-09 00:00:00', '2019-05-09 05:34:01'),
(13, 15, 1, 3.5, 'sfsdfsd', 'admin@rcreation.com', 'sdfsdfsf', 2, 1, '2019-05-11 00:00:00', '2019-05-11 08:22:32'),
(14, 15, 1, 4, 'fsdfsd', 'admin@rcreation.com', 'afsfs', 2, 1, '2019-05-11 00:00:00', '2019-05-11 08:23:19'),
(15, 91, 87, 4, 'arif', 'admin@rcreation.com', 'sfsf', 2, 1, '2019-05-12 00:00:00', '2019-05-12 04:44:26'),
(16, 91, 88, 3.5, 'zdfsdfsd', 'admin@rcreation.com', 'dfdfsdfsd', 2, 1, '2019-05-12 00:00:00', '2019-05-12 05:40:09'),
(17, 92, 87, 4, 'sadsad', 'admin@rcreation.com', 'sfgdfgdf', 2, 1, '2019-05-12 00:00:00', '2019-05-12 08:06:26');

-- --------------------------------------------------------

--
-- Table structure for table `utm_ofr`
--

CREATE TABLE `utm_ofr` (
  `utmid` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL DEFAULT '0',
  `utm_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `utm_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `add_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `utm_ofr`
--

INSERT INTO `utm_ofr` (`utmid`, `offer_id`, `utm_code`, `utm_link`, `add_date`) VALUES
(1, 4, 'silky100', 'https://www.silkygirl.com.bd/product-category/jewellery-shop/', '2018-06-04 07:04:27'),
(2, 5, 'silky100', 'https://www.silkygirl.com.bd/product-category/jewellery-shop/', '2018-06-04 07:06:39'),
(3, 16, 'XCVBTR', 'https://www.daraz.com.bd/lanvin-jeanne-lanvin-edp-100ml-174406.html', '2018-06-04 10:23:54'),
(4, 17, 'OP9654PO', 'https://www.daraz.com.bd/jaguar-jaguar-classic-black-for-men-100-ml-edt-11085.html', '2018-06-04 10:25:16'),
(5, 19, 'AN98PL', 'https://chaldal.com/offers', '2018-06-04 10:34:55'),
(6, 22, 'AN98PL', 'https://www.othoba.com/black-forest-cake-1kg-46648', '2018-06-04 10:42:13'),
(7, 24, 'BG094DO', 'https://www.bagdoom.com/men/clothing/pant/men-s-gabardine-pant-gp-106.html', '2018-06-04 12:28:01'),
(8, 26, 'XCVBTR', 'https://www.bagdoom.com/mobile-clip-on-lens-with-3-in-1-effect.html', '2018-06-04 12:36:23'),
(9, 30, 'XCVBTR', 'https://www.grameenphone.com/star-program/special-offers/star-offer-gazipur', '2018-06-05 00:34:51'),
(10, 31, 'KIKW06', 'https://kiksha.com/offer-zone/electronics/smart-deal?utm_source=EA185FB20&utm_medium=FB%20GS', '2018-06-05 01:54:09'),
(11, 34, 'MIA164GB', 'https://www.trendy-tracker.com/xiaomi-mi-a1-4gb-64gb.html', '2018-06-07 04:29:27'),
(12, 38, 'TRZ66DY', 'https://www.trendy-tracker.com/z66-ip67-waterproof-smart-bracelet.html', '2018-06-08 04:19:04'),
(13, 39, '1200TKF5', 'https://www.trendy-tracker.com/oppo-f5-4gb-32gb.html', '2018-06-08 04:24:56'),
(14, 64, 'sefedf', 'https://bikroy.com/bn/ads/chattogram/electronics', '2018-08-09 12:48:21'),
(15, 66, 'XCVBTR', 'http://bdforexpro.com', '2018-08-14 18:02:26'),
(16, 64, 'sefedf', '', '2018-08-19 14:08:43');

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `voucher_id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL DEFAULT '0',
  `voucher_price` double NOT NULL DEFAULT '0',
  `voucher_code` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`voucher_id`, `offer_id`, `voucher_price`, `voucher_code`, `add_date`) VALUES
(1, 42, 500, '750', '2018-07-09 05:49:09'),
(2, 55, 500, 'XYZABC001', '2018-07-30 14:10:46'),
(3, 56, 500, 'XYZAB', '2018-07-30 14:42:34'),
(4, 57, 1200, '1001', '2018-07-30 14:44:40'),
(5, 58, 200, '656hrf34', '2018-07-30 14:46:53'),
(6, 60, 111, '33', '2018-07-31 06:16:25'),
(7, 62, 90, 'XYZABC001', '2018-08-04 06:36:59'),
(8, 72, 1500, 'WRT10', '2018-08-30 11:23:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `additional_img`
--
ALTER TABLE `additional_img`
  ADD PRIMARY KEY (`addi_id`);

--
-- Indexes for table `advertise`
--
ALTER TABLE `advertise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`area_id`);

--
-- Indexes for table `catagory`
--
ALTER TABLE `catagory`
  ADD PRIMARY KEY (`catagory_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corporate`
--
ALTER TABLE `corporate`
  ADD PRIMARY KEY (`corporate_id`);

--
-- Indexes for table `ctype_colour`
--
ALTER TABLE `ctype_colour`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_ofr`
--
ALTER TABLE `custom_ofr`
  ADD PRIMARY KEY (`custom_ofr`);

--
-- Indexes for table `direct_sale`
--
ALTER TABLE `direct_sale`
  ADD PRIMARY KEY (`sale_id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_offer`
--
ALTER TABLE `main_offer`
  ADD PRIMARY KEY (`offer_id`);

--
-- Indexes for table `marchant_details`
--
ALTER TABLE `marchant_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_highlights`
--
ALTER TABLE `offer_highlights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer_sms`
--
ALTER TABLE `offer_sms`
  ADD PRIMARY KEY (`sms_id`);

--
-- Indexes for table `offer_type`
--
ALTER TABLE `offer_type`
  ADD PRIMARY KEY (`offer_id`);

--
-- Indexes for table `online_deal`
--
ALTER TABLE `online_deal`
  ADD PRIMARY KEY (`online_deal_id`);

--
-- Indexes for table `order_transaction`
--
ALTER TABLE `order_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_offer_catagory`
--
ALTER TABLE `post_offer_catagory`
  ADD PRIMARY KEY (`post_of_type_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`sliderid`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`tag_id`);

--
-- Indexes for table `target_people`
--
ALTER TABLE `target_people`
  ADD PRIMARY KEY (`offer_id`);

--
-- Indexes for table `target_ppl_ofr`
--
ALTER TABLE `target_ppl_ofr`
  ADD PRIMARY KEY (`of_tid`);

--
-- Indexes for table `userlog`
--
ALTER TABLE `userlog`
  ADD PRIMARY KEY (`log_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_login`
--
ALTER TABLE `user_login`
  ADD PRIMARY KEY (`loginid`);

--
-- Indexes for table `user_offer_view`
--
ALTER TABLE `user_offer_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_order`
--
ALTER TABLE `user_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_rating`
--
ALTER TABLE `user_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `utm_ofr`
--
ALTER TABLE `utm_ofr`
  ADD PRIMARY KEY (`utmid`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `additional_img`
--
ALTER TABLE `additional_img`
  MODIFY `addi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `advertise`
--
ALTER TABLE `advertise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `area_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `catagory`
--
ALTER TABLE `catagory`
  MODIFY `catagory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `corporate`
--
ALTER TABLE `corporate`
  MODIFY `corporate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ctype_colour`
--
ALTER TABLE `ctype_colour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `custom_ofr`
--
ALTER TABLE `custom_ofr`
  MODIFY `custom_ofr` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `direct_sale`
--
ALTER TABLE `direct_sale`
  MODIFY `sale_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` int(2) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `main_offer`
--
ALTER TABLE `main_offer`
  MODIFY `offer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `marchant_details`
--
ALTER TABLE `marchant_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `offer_highlights`
--
ALTER TABLE `offer_highlights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `offer_sms`
--
ALTER TABLE `offer_sms`
  MODIFY `sms_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `offer_type`
--
ALTER TABLE `offer_type`
  MODIFY `offer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `online_deal`
--
ALTER TABLE `online_deal`
  MODIFY `online_deal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `order_transaction`
--
ALTER TABLE `order_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `post_offer_catagory`
--
ALTER TABLE `post_offer_catagory`
  MODIFY `post_of_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `sliderid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `target_people`
--
ALTER TABLE `target_people`
  MODIFY `offer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `target_ppl_ofr`
--
ALTER TABLE `target_ppl_ofr`
  MODIFY `of_tid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `userlog`
--
ALTER TABLE `userlog`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `user_login`
--
ALTER TABLE `user_login`
  MODIFY `loginid` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `user_offer_view`
--
ALTER TABLE `user_offer_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `user_order`
--
ALTER TABLE `user_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_rating`
--
ALTER TABLE `user_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `utm_ofr`
--
ALTER TABLE `utm_ofr`
  MODIFY `utmid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
