<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Pagination Config
 * 
 * Just applying codeigniter's standard pagination config with twitter 
 * bootstrap stylings
 *
 * @author		TechArise Team
 * @link		http://codeigniter.com/user_guide/libraries/pagination.html
 * @email		info@techarise.com
 * 
 * @file		pagination.php
 * @version		1.0.0.1
 * @date		24/09/2017
 * 
 * Copyright (c) 2017
 */
/* -------------------------------------------------------------------------- */

$config['per_page'] = 10;
$config['num_links'] = 2;

$config['use_page_numbers'] = TRUE;
$config['page_query_string'] = FALSE;

$config['query_string_segment'] = TRUE;
$config['full_tag_open'] = '<ul class="pagination">';
$config['full_tag_close'] = '</ul>';

$config['first_link'] = '<span class="page-link pg_nxt">&laquo; First</span>';
$config['first_tag_open'] = '<li class="page-item">';
$config['first_tag_close'] = '</li>';

$config['last_link'] = '<span class="page-link pg_nxt">Last &raquo;</span>';
$config['last_tag_open'] = '<li class="next page">';
$config['last_tag_close'] = '</li>';

$config['next_link'] = '<span class="page-link pg_nxt">Next &raquo;</span>';
$config['next_tag_open'] = '<li class="page-item">';
$config['next_tag_close'] = '</li>';

$config['prev_link'] = '<span class="page-link pg_nxt">&laquo; Previous</span>';
$config['prev_tag_open'] = '<li class="page-item">';
$config['prev_tag_close'] = '</li>';

$config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
$config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';

$config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
$config['num_tag_close'] = '</span></li>';

$config['anchor_class'] = 'follow_link';

?>