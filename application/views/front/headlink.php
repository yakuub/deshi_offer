<!DOCTYPE html>
<base href="<?php echo base_url();?>">
<html>
<head>
    <title>Deshioffer | দেশীঅফার : Discounts, Offers, Deals, Coupons, Cashback in Bangladesh.</title>
    <meta name="Description" content="Save money with Deshioffer ! Get exclusive offers & discounts for your everyday’s shopping through our amazing collection of up-to-date deals, coupons and cashback offers from top and local brands in Bangladesh.">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Facebook Share -->
<!--     <meta property="og:url"        content="https://www.your-domain.com/your-page.html" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Your Website Title" />
    <meta property="og:description"   content="Your description" />
    <meta property="og:image"         content="https://www.your-domain.com/path/image.jpg" /> -->



    
    <link rel="shortcut icon" type="image/ico" href="front_assets/images/favicon.ico" />
    <link rel="stylesheet" href="front_assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="front_assets/css/custom.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="front_assets/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="front_assets/css/star-rating.css">
    <link rel="stylesheet" href="front_assets/css/select2.css">
    <link rel="stylesheet" href="front_assets/css/custom-heart.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/hover.css/2.3.1/css/hover.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">


</head>



