<div class=""> 
  <div class="fix_fix"></div>


  <div class="container-fluid nav_wrap">
    <div class="container main_nav_wrap">
      <div class="main_nav">
        <ul>
         <li class='droppable'>
          <a href='javascript:void(0);'>TOP CATEGORIES</a>
          <div class='mega-menu'>
            <div class="container cf">
              <div class="col-md-3  p_0">
                <!-- required for floating -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tabs-left sideways">
                  <h5>POPULAR CATEGORIES</h5>
                  <?php foreach ($category as $key => $value) {?>
                    <li <?php if($key==0){echo 'class="active"';}?>><a href="#cat<?=($key+1);?>" data-toggle="tab"><?=$value['catagory_title'];?> <i class="fa fa-angle-right pull-right right-arrow"></i></a></li>
                  <?php } ?>
                                       <!--  <li><a href="#cat2" data-toggle="tab">Electronics  <i class="fa fa-angle-right pull-right right-arrow"></i></a></li>
                                        <li><a href="#cat3" data-toggle="tab">Food  <i class="fa fa-angle-right pull-right right-arrow"></i></a></li>
                                        <li><a href="#cat4" data-toggle="tab">Furniture  <i class="fa fa-angle-right pull-right right-arrow"></i></a></li>
                                        <li><a href="#cat5" data-toggle="tab">Shoes  <i class="fa fa-angle-right pull-right right-arrow"></i></a></li>
                                       
                                        <li><a href="#cat6" data-toggle="tab">Services <i class="fa fa-angle-right pull-right right-arrow"></i></a></li>
                                        <li><a href="#cat7" data-toggle="tab">Business <i class="fa fa-angle-right pull-right right-arrow"></i></a></li>
                                        <li><a href="#cat8" data-toggle="tab">Gadgets<i class="fa fa-angle-right pull-right right-arrow"></i></a></li> -->
                                        <!--  no tab contents -->

                                        <li><a href="category">All Categories</a></li> 

                                      </ul>

                                    </div>
                                    <div class="col-md-9 p_0">
                                      <!-- Tab panes -->
                                      <div class="tab-content tab_mega_cnt">
                                        <?php foreach ($category as $key => $value) {?>
                                          <div class="tab-pane <?php if($key==0){echo "active";}?>" id="cat<?=($key+1);?>">
                                            <div class="sub_mega_head">
                                              <h4 class="sub_cat_header pull-left"> <?=$value['catagory_title'];?></h4>
                                              <a href="category/offer/<?=$value['catagory_title'];?>" id="all_elements" class="pull-right"><h4> All <?=$value['catagory_title'];?> </h4></a>
                                            </div>
                                            
                                            <div class="container">
                                              <div class="row">
                                                <?php foreach ($cat_brand[$key] as $b_key => $b_value) { 
                                                  if($b_value['catagories']==$value['catagory_id']){?>
                                                    <a href="store/offer/<?=$b_value['name'];?>" class="cat_element_link">
                                                      <div class="col-md-2 cat-elements">
                                                        <div>
                                                          <img src="uploads/user/<?=$b_value['profile_pic'];?>" alt="">
                                                        </div>
                                                        <span><?=$b_value['username'];?></span>
                                                        <span><?=$store_offer[$key][$b_key];?> offers Available</span>
                                                      </div>
                                                    </a>
                                                  <?php } } ?>
                                                  <div class="col-md-offset-2"></div>
                                                  <!-- <div class="col-md-2 cat-elements"></div> -->
                                                </div>
                                              </div>

                                            </div>
                                          <?php } ?>

                                        </div>
                                      </div>
                                    </div>
                                    <!-- .container -->
                                  </div>
                                  <!-- .mega-menu -->
                                </li>
                                <li class='droppable'>
                                  <a href='javascript:void(0);'>TOP STORES</a>
                                  <div class='mega-menu top-store-menu'>
                                    <div class="container cf">
                                      <?php foreach ($top_store as $key => $value) {?>

                                        <div class="col-md-3 ">
                                          <ul class="nav nav-tabs sideways nav-list-item">
                                            <li><a href="store/offer/<?=$value['name'];?>"><?=$value['username'];?></a></li>
                                          </ul>
                                        </div>
                                      <?php } ?>   

                                      <p><a href="store" class="view-all" style="white-space: normal;height: auto; padding-left: 25px;">View all stores</a></p>

                                      <!-- .ul-reset -->
                                    </div>
                                    <!-- .container -->
                                  </div>
                                  <!-- .mega-menu -->
                                </li>
                                <!-- .droppable -->


                                <li class='droppable'>
                                  <a href='javascript:void(0);'>TOP BRANDS</a>
                                  <div class='mega-menu top-brand-menu'>
                                    <div class="container cf">

                                      <?php foreach ($top_brand as $key => $value) {?>

                                        <div class="col-md-3 ">
                                          <ul class="nav nav-tabs sideways nav-list-item">
                                            <li><a class="hvr-underline-from-left" href="store/offer/<?=$value['name'];?>"><?=$value['username'];?></a></li>
                                          </ul>
                                        </div>
                                      <?php } ?>   

                                      <!-- <p><a href="" class="view-all" style="white-space: normal;height: auto; padding-left: 25px;">View all stores</a></p> -->

                                      <!-- .ul-reset -->
                                    </div>
                                    <!-- .container -->
                                  </div>
                                  <!-- .mega-menu -->
                                </li>


                                <div class="dod_dropdwn">
                                  <li><a href="javascript:void(0);" class="dropbtn">DEALS OF THE DAY</a></i>
                                    <i class="fa fa-angle-down dod-arrow"></i>
                                  </li>


                                  <?php if (count($deals_offer)>0) { ?>

                                    <div class="dod_dpc">
                                     <?php foreach ($deals_offer as $key => $value) {?>
                                      <a href="offer/deals_offer/<?=$value['offer_title']?>"><?=$value['offer_title']?></a>

                                    <?php  } ?>
                                  </div>
                                <?php }?>


                              </div>

                              <div class="dod_dropdwn">
                                <li><a href="javascript:void(0);" class="dropbtn">NEAR ME</a></li>


                                <div class="dod_dpc" style="display: none;">
                                  <a href="#">Link 1</a>
                                  <a href="#">Link 2</a>
                                  <a href="#">Link 3</a>
                                  <a href="#">Link 1</a>
                                  <a href="#">Link 2</a>
                                  <a href="#">Link 3</a>

                                </div>
                              </div>


                              <!--  <span class="nav-last pull-right"><a href="">NEW YEAR OFFER</a></span> -->
                              <span class="glow">
                                <?php if (count($special_offer)>0) { ?>
                                  <span class="nav-last pull-right minor">
                                    <a href="offer/special_offer/<?=$special_offer[0]['offer_title']?>"><?=$special_offer[0]['offer_title']?></a>
                                  </span>
                                <?php }?>   
                              </span>


                    <!--  <li class="extended_menu">MORE &nbsp;
            <ul>
              <li><a href="all_category.html">All Categories</a></li>
              <li><a href="category_offer.html">Dropdown Item 2</a></li>
              <li><a href="category_offer.html">Dropdown Item 3</a></li>
              <li><a href="category_offer.html">Dropdown Item 4</a></li>
              <li><a href="category_offer.html">Dropdown Item 5</a></li>
            </ul>
          </li> -->
          <div class="clearfix"></div>
        </ul>
      </div>
    </div>
  </div>