 <div class="modal fade" id="sms_modal" role="dialog"></div>

 <div class="modal fade" id="coupon_modal" role="dialog"></div>

 <div class="modal fade" id="vouchar_modal" role="dialog"></div> 
 <div class="modal fade" id="deal_activate" role="dialog"></div> 

 <?php if ($this->session->flashdata('Successfully')): ?>
  <script>
    swal({
      title: "Done",
      text: "<?php echo $this->session->flashdata('Successfully'); ?>",
      timer: 1500,
      showConfirmButton: false,
      type: 'success'
    });
  </script>
<?php endif; ?> 

<?php if($this->session->userdata('scc_alt')){ ?>
  <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
    <a href="javascript:;" class="alert-link"><?=$this->session->userdata('scc_alt');?></a>
  </div>
<?php } $this->session->unset_userdata('scc_alt');?>

<?php if($this->session->userdata('error_alt')){ ?>
  <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
    <a href="javascript:;" class="alert-link"><?=$this->session->userdata('error_alt');?></a>
  </div>
<?php } $this->session->unset_userdata('error_alt');?>


<!-- Rating & Review Modal Start -->

<div class="modal fade" id="review_modal" role="dialog">
  <div class="modal-dialog modal-lg" style="background: white;">

    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>

    </div>
    <div class="modal-body"> 

      <div class="col-md-offset-2 col-md-8">
        <div class="tab-content">
          <div class="tab-pane fade in active" id="login_usr">
            <form action="offer/save_review_post/<?=$offer_details[0]['main_off_id']?>/<?=$offer_details[0]['profile_name']?>" method="post">
              <label for="name" class="control-label">Rating*</label>
              <span class="text-left">
                <input id="" required="" style="font-size: 30px !important;" name="user_rating" type="text" class="rating" data-size="xs">
              </span>

              <label for="name" class="control-label col-md-12 label_rv">Name*
              </label>
              <input type="text" name="name" required="" class="form-control input_rv">
              <label for="email" class="control-label col-md-12 label_rv">E-mail*
              </label>

              <input type="email" name="email" required="" class="form-control input_rv" style="margin-bottom: 10px !important;">

              <label for="review" class="control-label col-md-12 label_rv">Review*
              </label>

              <textarea class="ckeditor form-control input_rv" name="review" id="review" cols="20" rows="3"></textarea>

              <div class="col-md-12 text-right pr_0">
                <input required="" style="margin: 50px 0; background-color: black;border:none" type="submit" class="btn btn-primary" value="submit review">
              </div>

            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- Rating & Review Modal End -->

<!-- Modal -->
<div class="modal fade " id="merch_dt_modal" role="dialog">
  <div class="modal-dialog modal-lg  merch_modal">
   <div class="modal-content merch_dt_cnt">
    <div class="modal-header" style="padding: 0; border: none;">
     <button type="button" class="close mech_cl_btn" data-dismiss="modal">&times;</button>

   </div>
   <div class="modal-body merch_dt_body">
     <div class="col-md-12 merch_dt_box p_0">
      <div class="col-xs-12 profile_img_box" >
       <img src="uploads/user/<?=$store_info[0]['profile_pic'];?>" alt="" class="img-responsive center-block" style="width:200px;height:100px;">
       <span class="text-center" style="font-family:bold;font-size:20px;">
         <p><?=$store_info[0]['profile_headline']?></p>
       </span>
     </div>
     <!--         <div class="clearfix"></div> -->
     <div class="col-md-offset-4 col-md-8 p_0">
       <div class=" col-md-4 rating-top-modal rating_star border_ryt" style="">
        <?php if($this->session->userdata('type')==3){ 
          if($rating_info_individual_user!=null){
           ?>  
           <input readonly="" id="input-id" value="<?=$rating_info_individual_user[0]['rating']?>" type="text" class="rating mr-offr-rating" data-size="xs" >

         <?php } else { ?>
          <input readonly="" id="input-id" type="text"  value="<?=$total_rating?>" class="rating mr-offr-rating" data-size="xs" >

        <?php } } else{?>

          <input readonly="" id="input-id" type="text"  value="<?=$total_rating?>" class="rating mr-offr-rating" data-size="xs" >

        <?php } ?>

      </div>

      <div class="col-md-3">

       <?php if($rating_info_individual_user!=null){
         ?>  
         <a href="javascript:void(0)"><?=$rating_info_individual_user[0]['rating']?> Out of 5</a>

       <?php } else { ?>

         <a href="javascript:void(0)">0 Out of 5</a>

       <?php } ?>
     </div>

     <div class="col-md-3">
       <?=$rating_info[0]['total_row']?> Ratings
     </div>
   </div>
                              <!-- <div class="col-md-5 mt-12 t-vote" style="padding-left: 10px;">
                                 <span style="font-size: 18px; line-height: 0">211 Votes</span>
                               </div> --> 
                               <div class="col-md-12">
                                <?php if($store_info[0]['verify_status']==1){?>

                                  <p class="text-center txt_ver"><span style="font-weight: 600; color: #0ed00e">Verified Store</span> </p>
                                <?php } else { ?>

                                  <p class="text-center txt_ver"><span style="font-weight: 600; color: #0ed00e">UnVerified Store</span> </p>

                                <?php } ?>

                              </div>
                              <div class="col-md-12 nav_dt_links">
                               <ul class="nav nav-tabs merc_nav_tabs col-md-7">
                                <li class="active"><a data-toggle="tab" href="#About">About</a></li>
                                <li><a data-toggle="tab" href="#contact">Contact</a></li>
                              </ul>
                              <div class="col-md-5 merc_right_btn">

                               <?php if($this->session->userdata('type')==3){  ?>

                                <span onclick="get_store_subscribe_modal('<?=$store_info[0]['loginid'];?>')" class="btn btn-default" style="border-radius: 0;    width: 50%;
                                padding: 5px;">Follow</span>&nbsp;
                                <span><?=$total_followers?> Followers</span>

                              <?php } else { ?>

                                <span data-toggle="modal" data-target="#login_modal" class="btn btn-default" style="border-radius: 0;    width: 50%;
                                padding: 5px;">Follow</span>&nbsp;
                                <span>211 Followers</span>
                               

                              <?php } ?>


                              
                            </div>
                          </div>
                        </div>
                        <div class="tab-content merc_tab_content">
                         <div id="About" class="tab-pane fade in active">
                          <h3>About <?=$store_info[0]['profile_headline']?></h3>
                          <p>
                            <?=$store_info[0]['about_us']?>;
                          </p>

                        </div>
                        <div id="contact" style="height: 950px;" class="tab-pane fade">
                          <h5 class="text-center"><strong>LOCATION</strong></h5>
                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4341.922886055763!2d90.4120701455606!3d23.7791899050831!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x19bd7392015966ba!2sBagdoom.com!5e0!3m2!1sen!2sbd!4v1519810466097" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                          <div class="col-md-12 merch_dt_social">
                           <h5 class="text-center">Connect with us here</h5>
                           <div class="merc_contants row">
                            <div class="col-md-6 col-xs-6">
                             <div class="row merc_contants_elem">
                              <div class="col-md-1"><i class="fa fa-phone gren_color"></i></div>
                              <div class="col-md-3">Hotline</div>
                              <div class="col-md-7"><?=$store_info[0]['contact'];?></div>
                            </div>
                            <div class="row merc_contants_elem">
                             <div class="col-md-1"><i class="fa fa-phone gren_color"></i></div>
                             <div class="col-md-3">Contact </div>
                             <div class="col-md-7">+88 096 06 11 77</div>
                           </div>
                         </div>
                         <div class="col-md-6 col-xs-6">
                          <div class="row merc_contants_elem">
                           <?php if($store_info[0]['email']!=''){?>
                            <div class="col-md-1"><i class="fa fa-envelope google_red"></i></div>
                            <div class="col-md-3">Email</div>
                            <div class="col-md-7"><?=$store_info[0]['email'];?></div>
                          <?php } ?>
                        </div>

                        <div class="row merc_contants_elem">
                         <?php if($store_info[0]['email']!=''){?>
                          <div class="col-md-1"><i class="fa fa-globe" style="color:blue;"></i></div>
                          <div class="col-md-3">Web</div>
                          <div class="col-md-7"><?=$store_info[0]['email'];?></div>
                        <?php } ?>
                      </div>
                                          <!-- <div class="row merc_contants_elem">
                                             <?php if($store_info[0]['support_email']!=''){?>
                                                <div class="col-md-1"><i class="fa fa-link"></i></div>
                                                <div class="col-md-3">Web</div>
                                                <div class="col-md-7"><?=$store_info[0]['support_email'];?></div>
                                             <?php } ?>
                                           </div> -->
                                         </div>
                                       </div>
                                       <!-- new line start -->
                                       <div class="merc_contants col-md-12">
                                         <div class="col-md-6 col-xs-6">
                                           <div class="row merc_contants_elem">

                                             <div class="col-md-1"><i class="fa fa-link viber_purple"></i></div>
                                             <div class="col-md-3">Viber</div>
                                             <div class="col-md-7">+675979868</div>

                                           </div>
                                           <div class="row merc_contants_elem">

                                             <div class="col-md-1"><i class="fa fa-google-plus  google_red"></i></div>
                                             <div class="col-md-3">Goole</div>
                                             <div class="col-md-7">@info.bagdoom.com</div>

                                           </div>
                                         </div>
                                         <div class="col-md-6 col-xs-6">
                                           <div class="row merc_contants_elem">

                                             <div class="col-md-2"><i class="fa fa-facebook fb_blue"></i></div>
                                             <div class="col-md-3">Facebook</div>
                                             <div class="col-md-7">@info.bagdoom.com</div>

                                           </div>
                                           <div class="row merc_contants_elem">

                                            <div class="col-md-1"><i class="fa fa-youtube-play google_red"></i></div>
                                            <div class="col-md-3">Youtube</div>
                                            <div class="col-md-7">@info.bagdoom.com</div>

                                          </div>
                                        </div>
                                      </div>
                                      <!-- new line end -->


                                      <div class="merc_location">
                                       <div class="col-md-6">
                                        <h5><strong> Corporate Office :</strong></h5>
                                        <p>
                                          Service Center 283/8928 6th Floor Road #22 Ghulshan 1-Dhaka 1212...
                                        </p>

                                      </div>
                                      <div class="col-md-6">
                                        <h5><strong>Dhaka Office:</strong></h5>
                                        <p>
                                          Service Center 283/8928 6th Floor Road #22 Ghulshan 1-Dhaka 1212...
                                        </p>
                                      </div>
                                      <di
                                      v class="col-md-6">
                                      <h5><strong>Chittagong Office:</strong></h5>
                                      <p>
                                        Service Center 283/8928 6th Floor Road #22 Ghulshan 1-Dhaka 1212...
                                      </p>
                                    </div>
                                  </div>
                                  <br>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer merch_dt_footer">
                          </div>
                        </div>
                      </div>
                    </div>

                    <!-- Store Rating Modal Start-->

                    <div class="modal fade" id="review_modal_store" role="dialog">
                      <div class="modal-dialog modal-lg" style="background: white;">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body">

                          <div class="col-md-offset-2 col-md-8">
                            <div class="tab-content">
                              <div class="tab-pane fade in active" id="login_usr">
                                <form action="store/save_store_review_post/<?=$store_info[0]['name']?>" method="post">
                                  <label for="name" class="control-label">Rating*</label>
                                  <span class="text-left">
                                    <input id="" required="" name="user_rating" type="text" class="rating" data-size="xs">
                                  </span>

                                  <label for="name" class="control-label col-md-12 label_rv">Name*
                                  </label>
                                  <input type="text" name="name" required="" class="form-control input_rv">
                                  <label for="email" class="control-label col-md-12 label_rv">E-mail*
                                  </label>

                                  <input type="email" name="email" required="" class="form-control input_rv" style="margin-bottom: 10px !important;">

                                  <label for="review" class="control-label col-md-12 label_rv">Review*
                                  </label>
                                  <textarea class="ckeditor form-control input_rv" name="review" id="review" cols="20" rows="3"></textarea>

                                  <div class="col-md-12 text-right pr_0">
                                    <input required="" style="margin: 50px 0; background-color: black;border:none" type="submit" class="btn btn-primary" value="submit review">
                                  </div>

                                </form>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>

                    <!-- Store Rating Modal End-->

                    <!-- Login Modal Start-->

                    <div class="modal fade" id="login_modal" role="dialog" style="padding-right: 0">
                      <div class="modal-dialog modal-lg">
                        <div class="col-md-5 p_0">
                          <div class="form_tips">
                            <button type="button" class="close_mobile" data-dismiss="modal">&times;</button>

                            <i class="fa fa-cogs tool_icon" aria-hidden="true"></i>
                            <h4 class="text-center p_0">WHY SIGNUP</h4>
                            <span class="text-center" style="display: block; margin-bottom: 25px; font-family: lato; text-decoration: underline;">What you can get from here!</span>
                            <ul style="padding: 0px 30px 10px;">
                              <li> Receive email alerts for new Offers from your favorite stores. </li>
                              <li> Get newsletters with the best deals,coupons & Offers.</li>
                              <li> Save time getting your favorite offers in one place.</li>
                              <li> Get Updated with the latest features and benefits.</li>
                            </ul>
                            <h4 style="text-align:center;">With all Never miss any deal . . .</h4>
                          </div>
                        </div>
                        <div class="col-md-7 p_0">
                          <div class="form_main">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>

                              <ul class="nav_header nav nav-tabs modal_head">

                                <li class="active"><a data-toggle="tab" href="#login_user">Login</a></li>
                                <li>&nbsp;/&nbsp;</li>
                                <li class=""><a data-toggle="tab" onclick="sign_up()" href="#signup_usr"> Sign-up</a></li>

                              </ul>
                            </div>
                            <div class="modal-body">

                              <div id="loading_img_login_modal" style="display:none;position: fixed;z-index: 10;top: 40%;left: 66%;width: 9%;">
                                <img style="width: 100%" src="front_assets/loading-spinner-blue.gif">
                              </div>

                              <div class="alert alert-danger alrt_form" id="err_div_login_modal" style="display: none;">
                                <strong id="err_msg_login_modal">Email of Password didn't match</strong>
                              </div>
                              <div class="tab-content">
                                <div class="tab-pane fade in active" id="login_user">
                                  <form class="form-signin">

                                    <input type="text" class="form-control" name="email" placeholder="Email Address" id="username_login_1" required=""/>

                                    <input type="password" id="password_login" class="form-control" autocomplete="new-password" name="password" placeholder="Password" required="" />

                                    <div class="col-md-5 p_0"><a href="javascript:void(0);" onclick="login_check()" class="btn btn-success btn-block" style="width: 40%;float: left;margin-right: 50px;">Login</a></div>
                                    <div class="col-md-7 social_form_m">
                                      <span>or login with</span>
                                      <a href="#" class="fa fa_icons fa-facebook"></a>
                                      <a href="#" class="fa fa_icons fa-google"></a>
                                    </div>
                                    <a href="javascript:void(0)" class="recover_pass">Forgot password?</a>
                                  </form>

                                  <form class="form-fg-pass" id="forget_form">

                                    <div class="col-md-12 text-center">
                                      <img style="width: 50px;height: 50px;" src="front_assets/images/forgot-password-icon-9.png">

                                      <h5><b>Forget Your Password ?</b></h5>
                                      <h5>Not to worry, we got you! Let's get you a new password.</h5>
                                      <hr>
                                    </div>

                                    <input type="email" class="form-control" name="email" placeholder="Email Address" id="email_forget" required="" autofocus="" />

                                    <div class="text-center" style="margin-bottom: 10px;">
                                      <button onclick="forget_pass()" class="btn  btn-danger btn-block " type="submit" style="width: 40%;float: left;margin-right: 79px;">Reset Password</button>

                                      <button class="b2login btn  btn-info btn-block" type="button" style="width: 40%;">Back to login</button>

                                      <!-- <a href="javascript:void(0)" class="b2login text-center">Back to login</a> -->
                                    </div>

                                    <div class="col-md-12" id="email_sent_div" style="display: none; color:green ;font-size: 14px;margin-bottom: 10px;">We have sent an email to your email address. Please check and change your password.</div>

                                    <div class="col-md-12" id="email_unsent_div" style="display: none; color:red ;font-size: 14px;margin-bottom: 10px;">Sorry, no user exists on our system with this email</div>

                                  </form>

                                </div>

                                <div class="tab-pane fade in" id="signup_usr">
                                  <div class="alert alert-danger alrt_form" id="err_div_reg_modal" style="display: none">
                                    <label id="m_msg"></label>
                                  </div>

                                  <div class="alert alert-info alrt_form" id="s_div" style="display: none">
                                    <label id="s_msg"></label>
                                  </div>

                                  <div class="form-signup">

                                    <input type="text" class="form-control" name="username" placeholder="Name" id="m_name" required="" autofocus="" />

                                    <input type="email" class="form-control" name="email" id="m_email" placeholder="Email Address" required="" autofocus="" />

                                    <div class="col-md-2 col-xs-4  text-center p-response" style="padding-right: 0;padding-right: 0;padding-left: 0"><span style="padding: 8px 8px;display: block;font-weight: bold;border-bottom:1px solid #ccc;height: 34px;width: 100%;line-height: 19px;">+88</span>
                                    </div>

                                    <div class="col-md-10 col-xs-8  in-box-num" style="padding: 0">
                                      <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="m_contact" name="contact" placeholder="Contact Number" maxlength="11" autocomplete="off" required="" />
                                      </div>
                                    </div>

                                    <input type="password" class="form-control" name="password" id="m_password" placeholder="Password" required="" autocomplete="new-password" />

                                    <input type="password" class="form-control" name="c_password" id="con_password" placeholder="Confirm Password" required="" />

                                    <label class="checkbox agreement">
                                      <input type="checkbox" checked value="agreement" id="tick" name="agreement" onchange="document.getElementById('agreement').disabled = !this.checked;"> By signing up I agree with Terms & Conditions
                                    </label>

                                    <button onclick="registration_user_check()" class="btn btn-block btn-success" style="width: 28%;margin-right: 79px;" id="agreement" type="button">Sign-up</button>

                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>

                        </div>
                      </div>
                    </div>


                    <!-- Login Modal End-->

                    <!-- =================Qr Modal Div Start ========= -->
                    <div class="modal fade" id="Qr_modal" role="dialog" style="padding-right: 0">
                      <div class="modal-dialog modal-lg">
                        <div class="col-md-12">
                          <div class="Qr_form">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <span class="text-center">
                                <img style="width: 50px;height: 50px;" src="front_assets/images/white.png"class=" col-md-offset-6" >
                                <h5 class="text-center mr_auto" style="color:#1188EF;">
                                  <b>Get Entry Pass for Bangladesh vs India match on July  3rd 2019</b>
                                </h5>
                              </span>
                            </div>
                            <div class="modal-body">
                              <div class="col-md-5">
                                <img style="width: 250px;
                                height: 230px;
                                padding: 10px;" src="front_assets/images/qr_img.png">
                              </div>

                              <div class="col-md-6 text-center">

                                <h5>
                                  <b>Print Or Save the QR to get the exclusive discount!!</b>
                                </h5>
                                <br>
                                <div class="col-md-12">
                                  <div class="col-md-6">
                                    <button class="btn btn-info btn-block btn_width" type="submit" style="width:100%; !important">Print QR</button>
                                  </div>

                                  <div class="col-md-6">
                                    <button class=" btn btn-info btn-block btn_width" type="submit" style="width:100%; !important">Save QR</button>
                                  </div>
                                </div>
                                <div class="row text-center" style="font-size: 14px;margin-top:50px !important;">
                                  <div class="col-md-8 col-md-offset-2 col-xs-4">
                                    <i class="fa fa-clock-o text-danger sof_icon_4">&nbsp;Ends In:</i> &nbsp;35 Days 11:05:20
                                  </div>
                                  <div class="col-md-8 col-md-offset-2 col-xs-4 text-center" style="font-size: 12px;margin-top:20px !important;">

                                    <i class="fa fa-eye  sof_icon_3"></i> Views: 200 Preyosi Offer
                                  </div>
                                </div>
                                <hr>
                              </div>

                            </div>

                          </div>

                        </div>
                      </div>
                    </div>
                    <!-- =================Qr Modal Div End ========= -->

                    <!-- ========== Coupun Modal div start  ========== -->

                    <div class="modal fade" id="copon_copy_modal" role="dialog" style="padding-right: 0">
                      <div class="modal-dialog modal-lg">
                        <div class="col-md-12">
                          <div class="Qr_form">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <span class="text-center">
                                <img style="width: 50px;height: 50px;" src="front_assets/images/white.png"class=" col-md-offset-6" >
                                <h5 class="text-center mr_auto" style="color:#1188EF;">
                                  <b>10% Flat Sale for Women Eye Lash at Preyosi</b>
                                </h5>
                              </span>
                            </div>
                            <div class="modal-body ">

                              <div class="col-md-12 text-center grey_background_color">

                                <h5>
                                  <b>Copy this code & use during checkout at &nbsp;<span style="color:blue;">Preyosi.com</span></b>
                                </h5>
                                <br>
                                <div class="col-md-12">

                                  <div class="col-md-12 ">
                                    <form action="">
                                      <input type="text" class="copun_input" placeholder="797858">

                                      <input class="btn btn-info btn_width sign_up_submit copy_code_sub" type="submit" value="Copy Code">
                                    </form>

                                  </div>
                                </div>
                                <div class="row text-center" style="font-size: 14px;margin-top:55px !important;">
                                  <div class="col-md-8 col-md-offset-2 col-xs-4">

                                    <b>  Get Registered to get <span style="color:#10da10;">Cashback</span> every of your purchase&nbsp;<i class="fa fa fa-caret-right sof_icon_3"></i>&nbsp;<span style="color:red;">Sign Up</span></b>
                                  </div>

                                </div>

                                <hr>
                              </div>
                              <br>
                              <div class="text-center" style="margin-bottom: 10px;">
                                <div class="col-md-12" style="margin-top: 20px;">
                                  <div class="col-md-6">
                                    <span class="text-center">Visit <span style="color:#10da10;">120</span> more Preyosi Offer</span>

                                  </div>
                                  <div class="col-md-6">
                                    <span class="text-center"><span style="color:red;">Offers Ends</span> 31 Days 17:23:50</span>

                                  </div>
                                </div>
                              </div>

                            </div>

                          </div>

                        </div>
                      </div>
                    </div>

                    <!-- ========== Coupon Modal div end  ========== -->


                    <!-- =============== Sms Offer Modal div Start ========-->

                    <div class="modal fade" id="mobile_number_form" role="dialog" style="padding-right: 0">

                    </div>
                    <!-- =============== Sms Offer Modal div End ========-->

                    <!-- ================= Offer activte Modal Start ============= -->
                    <div class="modal fade" id="offer_active" role="dialog" style="padding-right: 0">
                      <div class="modal-dialog modal-lg">
                        <div class="col-md-12">
                          <div class="offer_active_form">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <span class="text-center">
                                <img style="width: 50px;height: 50px;" src="front_assets/images/white.png"class=" col-md-offset-6" >
                                <h5 class="text-center mr_auto" style="color:#1188EF;">
                                  <b>10% Flat Sale for Women Eye Lash at Preyosi</b>
                                </h5>
                              </span>
                            </div>
                            <div class="modal-body ">

                              <div class="col-md-12 text-center grey_background_color" style="padding:32px !important;">
                                <div class="col-md-12">
                                  <div class="col-md-12 ">
                                    <form action="">
                                      <input type="text" class="copun_input" placeholder="OFFER ACTIVATED">

                                    </form>

                                  </div>
                                </div>
                                <div class="row text-center" style="font-size: 14px;margin-top:55px !important;">
                                  <div class="col-md-8 col-md-offset-2 col-xs-4">

                                    <b>Registered user get <span style="color:#10da10;">Cashback</span> every of your purchase&nbsp;<i class="fa fa fa-caret-right sof_icon_3"></i>&nbsp;<span style="color:red;">Sign Up</span></b>
                                  </div>

                                </div>


                              </div>
                              <br>
                              <div class="text-center" style="margin-bottom: 10px;">
                                <div class="col-md-12 " style="margin-top: 30px;">


                                  <span class="text-center">Visit
                                   <span style="color:#10da10;">
                                   120</span> 
                                   more <span style="color:#10da10;">Preyosi</span> Offer and get highest discount....

                                 </span>



                               </div>
                             </div>

                           </div>

                         </div>

                       </div>
                     </div>
                   </div>
                   <!-- ================= Offer activte Modal End ============= -->


                   <!-- =========   Redirect Modal Div Start======= -->
                   <div class="modal fade" id="redirect_modal" role="dialog" style="padding-right: 0">
                    <div class="modal-dialog modal-lg">
                      <div class="col-md-12">
                        <div class="rediredct_form">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                          </div>
                          <div class="modal-body ">

                            <div class="col-md-12 text-center ">

                              <div class="row text-center" style="font-size: 14px;margin-top:25px !important;">
                                <div class="col-md-8 col-md-offset-2 col-xs-4">

                                  <b class="text_capitalize">Iphone  X launching offer 3% OFF at big 24 for 1st 15 sale !</b><br>

                                </div>
                                <div class="col-md-8 col-md-offset-2">
                                 <h5> <span class="text_capitalize google_red">Please wait while we are redirecting you to bagdoom</span></h5>
                               </div>

                             </div>


                           </div>

                           <div class="text-center" style="margin-bottom: 10px;">
                            <div class="col-md-12" style="margin-top: 20px;">
                              <div class="col-md-6 col-md-offset-2">
                                <span class="text-center ">
                                 <img style="width: 50px;height: 50px;" src="front_assets/images/white.png"class=" " >

                               </span>
                               <span class="text-center ">
                                 <img style="width: 50px;height: 50px;float:right;" src="front_assets/images/bagdoom_pic.jpg" class="" >

                               </span>

                             </div>

                           </div>
                         </div>

                       </div>

                     </div>

                   </div>
                 </div>
               </div>
               <!-- =========   Redirect Modal Div ENd======= -->
               <div class="container_fluid subscribe_nl">
                <h4 class="text-center">SUBSRIBE TO EMAIL ALERTS FOR NEW OFFERS & DEALS</h4>

                <div class="footer_s_form sign_up_form text-center">
                  <form action="">
                    <input type="text" class="footer_signup" placeholder="Enter your email">
                                    <!--
                                    -->
                                    <input class="sign_up_submit footer_sub" type="submit" value="SUBSCRIBE">
                                  </form>
                                </div>

                              </div>

                              <style>
                                @media screen and (max-width:479px)
                              </style>
                              <div class="footer_wrapper container-fluid">
                                <div class="container footer">
                                  <div class="">
                                    <div class="col-md-3 col-xs-10 col-sm-4 logo ft_lg">
                                      <a href="index.html"><img class="img-responsive" src="images/logof.png" alt=""></a>

                                      <ul class="footer_details_list">
                                        <li><i class="fa fa-map"></i>Ams Point, Sirajuddoula Road,</li>
                                        <li class="space">Didar Market,Chittagong</li>
                                        <li><i class="fa fa-phone"></i>99488448</li>
                                        <li><i class="fa fa-envelope"></i>info@deshioffer.com</li>
                                        <li><i class="fa fa-users"></i>Contacts</li>

                                      </ul>
                                    </div>
                                    <div class="col-sm-2 col-xs-6 call text-center">
                                      <p>General</p>
                                      <ul class="footer_list_terms">
                                        <li>About us</li>
                                        <li>Privacy policy</li>
                                        <li>Terms of use</li>
                                        <li>Press & Media</li>
                                        <li>Blog </li>
                                        <li>FAQ's</li>
                                      </ul>
                                    </div>
                                    <div class="col-sm-2 call text-center footer-hide">
                                      <p>HELP</p>
                                      <ul class="footer_list_terms">
                                        <li>FAQ's</li>
                                        <li>How it Works</li>
                                        <li>Contacts</li>
                                      </ul>
                                    </div>
                                    <div class="col-sm-2 col-xs-6 call text-center">
                                      <p>Partner</p>

                                      <ul class="footer_list_terms">
                                        <li>Post Offers</li>
                                        <li>Partner's Benefits</li>
                                        <li>Advertisements</li>
                                        <li>Special Promotion</li>
                                        <li> Contacts </li>
                                        <li>How it Works</li>
                                      </ul>
                                    </div>

                                    <div class="col-sm-3 text-center col-xs-12">
                                      <i class="fa fa-facebook footer_icon_sc"></i>
                                      <i class="fa fa-twitter footer_icon_sc"></i>
                                      <i class="fa fa-google-plus footer_icon_sc"></i>
                                      <i class="fa fa-pinterest footer_icon_sc"></i>
                                      <i class="fa fa-instagram footer_icon_sc"></i>

                                      <p style="margin-top: 15px;">
                                        <a href=""><img class="img-responsive center-block" src="front_assets/images/comingsoongoogle.png" width="70%;" alt=""></a>
                                      </p>
                                      <p>
                                        <a href=""><img class="img-responsive center-block" src="front_assets/images/apple-app.png" width="70%;" alt=""></a>
                                      </p>

                                    </div>
                                  </div>
                                </div>
                                <!--  FOOTER CLASS CLOSED HERE -->
                                <div class="container copyright">

                                  <div class="col-md-5 p_0"><img src="front_assets/images/payment.png" alt=""></div>

                                  <div class="col-md-offset-2 col-md-5 text-right ft-cr">Copyright -
                                    <?=date('Y');?>, All Right Reserved by Deshioffer Inc</div>
                                  </div>
                                  <!--  COPYRIGHT CLASS CLOSED HERE -->
                                </div>
                                <!--  FOOTER_WRAPPER CLASS CLOSED HERE -->

                              </div>