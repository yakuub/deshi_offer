<script src="front_assets/js/jquery-1.10.2.min.js"></script>   

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->
<script src="front_assets/js/bootstrap.min.js"></script>
<script src="front_assets/js/notify.js"></script>
<!-- <script src="front_assets/owl-carousel/owl.carousel.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js"></script>
<script src="front_assets/js/custom.js"></script>
<script src="front_assets/js/star-rating.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
<script src="front_assets/js/select2.js"></script>
<!-- <script src="http://cdn.ckeditor.com/4.3.4/standard/ckeditor.js"></script> -->

<style type="text/css">
  .popover-title {
    text-align: center;
  }

  .custom-popover li {
    border: none!important;
    text-align: center;
    width:100px;
  }

  .custom-popover li:nth-child(2) {
    border-top: 1px solid #ccc!important;
  }

  .custom-popover li:last-child {
    border-top: 1px solid #ccc!important;
  }
</style>

<script type="text/javascript">
  $(document).ready(function() {
    $('[data-toggle="popover"]').popover({
      html: true,
      content: function() {
        return $('#popover-content').html();
      }
    });


  });


</script>

<script>
  $(".form-fg-pass").css("display","none");
  $(".recover_pass").click(function () {
    $(".form-signin").css("display","none")
    $(".form-fg-pass").css("display","block")
        // $("#").show();   
      });

  $(".b2login").click(function () {
    $(".form-signin").css("display","block")
    $(".form-fg-pass").css("display","none")

        // $("#").show();   
      });
    </script>

    <script type="text/javascript">

      $('#cat_list').select2({
        placeholder: "Select report type",
        allowClear: true,
        width:"200px"
      }).on("select2-blur", function(e) { log ("blur");});

      $(document).ready(function() {

        var district_id=$("#district option:selected" ).val();

        $("#shipment").html('');

        $("#total").html('');
         // alert(district_id);
         
         

         $.ajax({  
          url:"<?=site_url('offer/get_all_area')?>",  
          method:"POST",
          data:{district_id:district_id},  
          dataType:"json",  
          success:function(data)  
          { 

            // alert(data['charge_info'][0]['shipment_charge']);



            $('#home_delivery_charge').html('Home Delivery TK. '+data['charge_info'][0]['shipment_charge']);

            $('#home_delivary_charge_hidden').val(data['charge_info'][0]['shipment_charge']);

            $('#home_delivary_charge_hidden').html(data['charge_info'][0]['shipment_charge']);

            $("#shipment").html(data['charge_info'][0]['shipment_charge']);

            $("#total").html(parseFloat(data['charge_info'][0]['shipment_charge'])+parseFloat($("#subtotal").html()));


            $.each(data['area'], function (key, value) {
                              // $.each(value, function (key, value) {
                                $("#area").append('<option value="' + value.area_id + '">' + value.area_title + '</option>');
                              });
                          // });




                        }

                      });


       });
     </script>

<!-- <script type="text/javascript">

    ClassicEditor
    .create( document.querySelector( '#editor' ), {
            // toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
        } )
    .then( editor => {
        window.editor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
    
 
  </script> -->

  <script type="text/javascript">

    $('#forget_form').on('submit', function (e) {

      e.preventDefault();

      var email=$('#email_forget').val();

      $('#email_unsent_div').hide();
      $('#email_sent_div').hide();

      $("#loading_img_login_modal").show();

      $.ajax({
        url: "<?php echo site_url('login/forget_password');?>",
        type: "post",
        data: {email:email},
        success: function(msg)
        {
          if(msg==1)
          {
            $("#loading_img_login_modal").hide();
            $('#email_sent_div').show();

            
         }
         else
         {
            $('#email_unsent_div').show();
             $("#loading_img_login_modal").hide();
         }

       }
     });

    });
    
  </script>

  <script type="text/javascript">

  </script>

  <script>
    $('.droppable').hover(function(){
      $('.dim').fadeIn(0);
    },function(){
      $('.dim').fadeOut(0);
    });

  </script>

  <script>
    $(function() {
      $('.droppable').on('mouseover', function() {
        $('.overlay').show();
      });
      $('.droppable').on('mouseout', function() {
        $('.overlay').hide();
      });
    });

  </script>
  <script>
    $(".modal-trigger").click(function(){
      $(".mobile_menu_wrapper").removeClass("width_ctrl");
      $(".menu_switch_new").removeClass("menu_cross");

    });
  </script>


  <script>

    document.addEventListener("keydown", function(event) {

     if(($("#login_modal").data('bs.modal') || {}).isShown)
     {
      $('#login_modal').modal({backdrop: 'static', keyboard: false}); 
            //console.log(event.keyCode);
            if(event.keyCode==13)
            {
              login_check();
            }
          }


        });

    function sign_up(argument) {

      $("#err_div_login_modal").hide();

    }

    function login(argument) {

      $("#err_div_reg_modal").hide();

    }


    function registration_user_check() 
    {
      var username=$("#m_name").val();
      var email=$("#m_email").val();
      var contact=$("#m_contact").val();
        //contact='880'+contact;
        var password=$("#m_password").val();
        var con_password=$("#con_password").val();

         // $("#err_div_login_modal").hide();

         $("#err_div_reg_modal").hide();
         $("#s_div").hide();





         if(username=='')
         {
          $("#err_div_reg_modal").show();
          $("#m_msg").html('Please Enter your Name');
          $("#agreement").prop( "disabled", false );
          return false;

        }

        else if(email=='')
        {
          $("#err_div_reg_modal").show();
          $("#m_msg").html('Please Enter your Email');
          $("#agreement").prop( "disabled", false );
          return false;
        }

        else if(contact=='')
        {
          $("#err_div_reg_modal").show();
          $("#m_msg").html('Please Enter your contact number');
          $("#agreement").prop( "disabled", false );
          return false;
        }

        else if(password=='')
        {
          $("#err_div_reg_modal").show();
          $("#m_msg").html('Please Enter Password');
          $("#agreement").prop( "disabled", false );
          return false;
        }
        else if(contact.length != 11)
        {
          $("#err_div_reg_modal").show();
          $("#m_msg").html('Please Enter A Valid Phone Number of 11 digit');
          $("#agreement").prop( "disabled", false );

          return false;
        }

        else if(password!=con_password)
        {
          $("#err_div_reg_modal").show();
          $("#m_msg").html('Password not matched');
          $("#agreement").prop( "disabled", false );
          return false;
        }

        else
        {
          ok=false;

          $("#loading_img_login_modal").show();

          $("#agreement").prop( "disabled", true );

          $.ajax({
            url: "<?php echo site_url('registration/check_email_phone_unique');?>",
            type: "post",
            data: {email:email,contact:contact},
            success: function(msg)
            {
              var email=$("#m_email").val();
              var contact=$("#m_contact").val();
                 //console.log(msg);return;
                 if(msg=='success')
                 {
                   $.ajax({

                    url: "<?php echo site_url('registration/registration_user');?>",
                    type: "post",
                    data: {username:username,password:password,email:email,contact:contact},
                    success: function(msg)
                    {
                     if(msg==1)
                     {
                      ok=true;



                      window.location.href = "<?=base_url();?>registration/registration_successfull";
                    }

                    else
                    {
                     $("#m_msg").html('<span class="label label-danger">Something Wrong !!!</span>');
                   }
                 }      
               });
                 }

                 else
                 {

                  $("#err_div_reg_modal").show();
                  $("#loading_img_login_modal").hide();
                  $("#m_msg").html(msg);
                  $("#agreement").prop( "disabled", false );
                  ok=false;
                } 

                return ok;


              }   

            });



        }
        // else
        // {
        //     return false;
        //     $("#err_div_reg_modal").show();
        //     $("#m_msg").html('All field must be filled out');
        // }

       //return false;
     }


     function login_check() 
     {
      var username=$("#username_login_1").val();
      var password=$("#password_login").val();

      // alert(username);
      // alert(password);

      if(username=='' || password=='')
      {
        $("#err_div_login_modal").show();
        $("#err_msg_login_modal").html('* Fields must be filled out');
      }
      else
      {
        $("#loading_img_login_modal").show();

        $.ajax({
          url: "<?php echo site_url('login/ajax_login_check');?>",
          type: "post",
          data: {username:username,password:password},
          success: function(msg)
          {
                    //alert(msg);

                    console.log(msg);
                    if(msg==0)
                    {
                      $("#err_div_login_modal").show();
                      $("#err_msg_login_modal").html('Email or Password is incorrect.');
                    }
                    
                    else if(msg=="not_verify")
                    {
                        $("#err_div_login_modal").show();
                      $("#err_msg_login_modal").html('Account is not verified');  
                    }

                    else
                    {
                      $("#login_submit").prop( "disabled", true );
                      if(msg==1)
                      {
                        window.location.href = "<?=base_url();?>admin";
                      }
                      
                      else if(msg==2)
                      {
                         window.location.href = "<?=base_url();?>merchant"; 
                      }

                      else
                      {
                        window.location.href = "<?=base_url();?>user";
                      }

                    }
                   //$("#mynotify").modal('show');
                   $("#loading_img_login_modal").hide();
                   
                 }      
               });
      }
    }


    function new_user_login_check() 
    {
      var username=$("#username_login_1").val();
      var password=$("#password_login").val();
      if(username=='' || password=='')
      {
        $("#err_div_login_modal").show();
        $("#err_msg_login_modal").html('* Fields must be filled out');
      }
      else
      {
        $("#loading_img_login_modal").show();

        $.ajax({
          url: "<?php echo site_url('login/new_user_login_check');?>",
          type: "post",
          data: {username:username,password:password},
          success: function(msg)
          {
                    //alert(msg);

                    console.log(msg);
                    if(msg==0)
                    {
                      $("#err_div_login_modal").show();
                      $("#err_msg_login_modal").html('Email or Password is incorrect.');
                    }

                    else
                    {
                      $("#login_submit").prop( "disabled", true );
                      if(msg==1)
                      {
                        window.location.href = "<?=base_url();?>admin";
                      }



                      else
                      {
                        window.location.href = "<?=base_url();?>home";
                      }

                    }
                   //$("#mynotify").modal('show');
                   $("#loading_img_login_modal").hide();
                   
                 }      
               });
      }
    }

    function validateEmail(email) 
    {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    function email_cheker()
    {
      var email=$("#m_email").val();
      var cc=validateEmail(email);

      if(cc)
      {

        $.ajax({
          url: "<?php echo site_url('registration/check_email_unique');?>",
          type: "post",
          data: {email:email},
          success: function(msg)
          {
           if(msg==1)
           {
            $("#m_msg").html('<span class="label label-danger">Email already exists !!!</span>');
            $("#m_submit_btn").hide();
          }

          else
          {
           $("#m_msg").html('<span class="label label-success"><i class="fa fa-check-square"></i> Verified Email</span>');
           $("#m_submit_btn").show();
         }
       }      
     }); 
      }
    }





  </script>

  <script>
    function get_search_result() 
    {
      var search_item=$("#search").val();
      $("#search_result_div").fadeIn();
            //console.log(search_item);
            $.ajax({
              url: "<?php echo site_url('home/get_search_result');?>",
              type: "post",
              data: {search_item:search_item},
              success: function(msg)
              {
                   //console.log(msg);
                   $("#search_result_ul").html(msg);
                 }      
               });
          }

          function search_div_hide() 
          {
            $("#search_result_div").fadeOut();
          }
        </script>

        <script>
          function get_code(offer_id) 
          {
            $.ajax({
              url: "<?php echo site_url('home/get_code_modal');?>",
              type: "post",
              data: {offer_id:offer_id},
              success: function(msg)
              {
                    //console.log(msg);

                    $("#coupon_modal").html(msg);
                    $("#coupon_modal").modal('show');

                  }      
                });
          }


          function get_sms(offer_id) 
          {
            $.ajax({
              url: "<?php echo site_url('home/get_sms_modal');?>",
              type: "post",
              data: {offer_id:offer_id},
              success: function(msg)
              {
                    //console.log(msg);

                    $("#mobile_number_form").html(msg);
                    $("#mobile_number_form").modal('show');

                  }      
                });
          }


          function send_sms_ajax() 
          {

            var number=$('#number_to').val();

            if(number == "")
            {
              $('#num_validation').show();
              return;
            }
            else
            {
              $('#num_validation').hide();
            }

            var sms_msg=$('#sms_msg').val();


            $.notify("Message Sent Successfully","success");

            $("#mobile_number_form").modal('hide');

            var username='deshioffer';
            var password='Doconnect@2014';
            var from='Deshioffer';
            var to=88+number;
            var message=sms_msg;

            $.ajax({
              url:'https://api.mobireach.com.bd/SendTextMessage',
              data:{Username:username,Password:password,From:from,To:to,Message:message},
              method: "POST",
              dataType:"json", 
              success: function()
              {

              }
            });
          }


          function get_vouchar(offer_id) 
          {
            $.ajax({
              url: "<?php echo site_url('home/get_qr_code_modal');?>",
              type: "post",
              data: {offer_id:offer_id},
              success: function(msg)
              {
                    //console.log(msg);

                    $("#vouchar_modal").html(msg);
                    $("#vouchar_modal").modal('show');

                  }      
                });
          }


          function deal_activate(offer_id) 
          {
            $.ajax({
              url: "<?php echo site_url('home/open_deal_activate');?>",
              type: "post",
              data: {offer_id:offer_id},
              success: function(msg)
              {
                    //console.log(msg);

                    $(window.location.hash).modal("show");

                    $("#deal_activate").html(msg);
                    $("#deal_activate").modal('show');

                  }      
                });
          }



        </script>


        <script>
          function copy_cod(offer_id) 
          {
            var copyText = document.getElementById("coupon_btn_txt_"+offer_id);
            copyText.select();
            $("#coupon_btn_"+offer_id).val('Copied');
            document.execCommand("copy");

            $('#offer_activate').show();



            var url = $("#url_link_"+offer_id).attr('href'); 
            window.open(url, '_blank');
            //alert("Copied the text: " + copyText.value);
          }
        </script>

        <script type="text/javascript">

         $("#district").append('<option value="0">Select District Name</option>');
         $(document).on('change','#division', function(event)
         {
           var division_id=$( "#division option:selected" ).val();
           $("#shipment").html('');

           $("#total").html('');
         // alert(division_id);
         if(division_id==0)
         {
          $("#district").empty();
          $("#district").append('<option value="0">Select District Name</option>');

          $("#area").empty();
          $("#area").append('<option value="0">Select Area</option>');
        }
        else
        {

         $("#district").empty();
         $("#district").append('<option value="0">Select District Name</option>');

         $("#area").empty();
         $("#area").append('<option value="0">Select Area</option>');

         $.ajax({  
          url:"<?=site_url('offer/get_all_district')?>",  
          method:"POST",
          data:{division_id:division_id},  
          dataType:"json",  
          success:function(data)  
          { 
           $.each(data, function (key, value) {

            $("#district").append('<option value="' + value.id + '">' + value.name + '</option>');
          });
         },
         error: function(e) 
         {
          alert(e);
        }
      });

       }
     });

         $("#area").append('<option value="0">Select Area Name</option>');

         $(document).on('change','#district', function(event)
         {

          $('#home_delivery_charge').html('Home Delivery TK. ');

          $('#home_delivary_charge_hidden').val(0);

          var district_id=$("#district option:selected" ).val();

          $("#shipment").html('');

          $("#total").html('');
         // alert(district_id);
         
         if(district_id==0)
         {
          $("#area").empty();
          $("#area").append('<option value="0">Select Area Name</option>');
        }
        else
        {

         $("#area").empty();
         $("#area").append('<option value="0">Select Area Name</option>');

         $.ajax({  
          url:"<?=site_url('offer/get_all_area')?>",  
          method:"POST",
          data:{district_id:district_id},  
          dataType:"json",  
          success:function(data)  
          { 

            // alert(data['charge_info'][0]['shipment_charge']);



            $('#home_delivery_charge').html('Home Delivery TK. '+data['charge_info'][0]['shipment_charge']);

            $('#home_delivary_charge_hidden').val(data['charge_info'][0]['shipment_charge']);

            $("#shipment").html(data['charge_info'][0]['shipment_charge']);

            $("#total").html(parseFloat(data['charge_info'][0]['shipment_charge'])+parseFloat($("#subtotal").html()));


            $.each(data['area'], function (key, value) {
                              // $.each(value, function (key, value) {
                                $("#area").append('<option value="' + value.area_id + '">' + value.area_title + '</option>');
                              });
                          // });




                        },
                        error: function(e) 
                        {
                          alert(e);
                        }
                      });

       }
     });
   </script>


   <script type="text/javascript">


    function common_fun(argument) {

    }


    function increase_qty(id,name,price) {



     $.ajax({
      url: "<?php echo site_url('offer/increase_qty');?>",
      type: "post",
      data: {id:id,name:name,price:price},
      success: function(msg)
      {
        $('#offer_order_cart_ajax').html(msg);

        var home_delivary_charge_hidden =$('#home_delivary_charge_hidden').html();

        $("#shipment").html(home_delivary_charge_hidden);

        if(home_delivary_charge_hidden=="")
        {
          home_delivary_charge_hidden=0;
        }


        $("#total").html(parseFloat(home_delivary_charge_hidden)+parseFloat($("#subtotal").html()));

      }      
    });
   }

   function decrease_qty(id,name,price,qty) {

    var quantity=qty-1;

    $.ajax({
      url: "<?php echo site_url('offer/decrease_qty');?>",
      type: "post",
      data: {id:id,name:name,quantity:quantity,price:price},
      success: function(msg)
      {
                    //console.log(msg);

                    $('#offer_order_cart_ajax').html(msg);

                    var home_delivary_charge_hidden =$('#home_delivary_charge_hidden').html();

                    $("#shipment").html(home_delivary_charge_hidden);

                    if(home_delivary_charge_hidden=="")
                    {
                      home_delivary_charge_hidden=0;
                    }


                    $("#total").html(parseFloat(home_delivary_charge_hidden)+parseFloat($("#subtotal").html()));

                  }      
                });
  }


  function follow_store(store_id) {

    $.ajax({
      url: "<?php echo site_url('store/follow_store');?>",
      type: "post",
      data: {store_id:store_id},
      success: function(msg)
      {

      } 

    });
    
  }
</script>