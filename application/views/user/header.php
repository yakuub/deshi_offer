    <div class="dim"></div>
    <div class="mobile_menu_wrapper">
      <div class="menu_switch_new">
        <span></span>
        <span></span>
        <span></span>
      </div>

      <div class="mobile_menu">
        <a class="modal-trigger" data-toggle="modal" data-target="#login_modal" >  <i class="fa fa-user-circle" aria-hidden="true"></i> <span>Welcome</span> </a>
        <ul>

          <li><a href="category">All Categories</a></li>
          <li><a href="store">All Stores</a></li>
          <li><a href="merchant">Merchant Section</a></li>

          <li><a href="javascript:void(0);">Near Me</a></li>
          <li class="slide_dwn"><a onclick="return false" href="">Deals of the day</a>
           <?php if (count($deals_offer)>0) { ?>

            <ul>
              <?php foreach ($deals_offer as $key => $value) {?> 
                <li>
                  <a href="offer/deals_offer/<?=$value['offer_title']?>"><?=$value['offer_title']?></a>
                </li>

              <?php }?>

            </ul>
          <?php }?>
        </li>
        <li class="glow mobile-glow">
          <?php if (count($special_offer)>0) { ?>
            <span class="minor">
             <a href="offer/special_offer/<?=$special_offer[0]['offer_title']?>"><?=$special_offer[0]['offer_title']?></a>
           </span>
         <?php }?>
       </span>
     </li>
     <li><a href=""></a></li>
   </ul>
 </div>
</div>
<!--MOBILE MENU WRAPPER CLOSED-->
<div class="subscribe_fix_div">
  <div>
    <p>Don't Miss Any Offer, Subscribe to Deshioffer </p>
  </div>
  <form action="">
    <input placeholder="Enter Your Name" type="text">
    <input placeholder="Enter Your Email" type="text">
    <input type="submit" value="Submit">
  </form>
  <span class="form_close">&times;</span>
</div>
<div class="container-fluid top_menu_wrap">
  <div class="container top_menu_bar">
    <div class="top_menu_right">
      <ul>
        <li><a href=""><span>&#xf00a;</span>Advertise with us</a></li>
        <li><a href=""><span>&#xf1b3;</span>Merchant Section</a></li>
      </ul>
    </div>
    <!--TOP_MENU_RIGHT CLASS CLOSED HERE-->
    <div class="top_menu_left">
      <ul class="quick_links">
        <li class="link_option"><span>&#61642;</span>Quick Links <span style="float:right;font-size:16px;">&#61655;</span></li>
        <div class="top_drop_down">
          <li><a href=""><span>&#61977;</span>Hot Offers</a></li>
          <li><a href=""><span>&#xf290;</span>Popular Stores</a></li>
          <li><a href=""><span>&#xf06c;</span>Popular Catagories</a></li>
        </div>
        <!--TOP_DROP_DOWN CLASS CLOSED HERE-->
      </ul>
      <!--QUICK_LINKS CLASS CLOSED HERE-->
    </div>
    <!--TOP_MENU_LEFT CLASS CLOSED HERE-->
  </div>
  <!--TOP_MENU_BAR CLASS CLOSED HERE-->
</div>
<!--CONTAINER-FLUID CLASS CLOSED HERE-->
<div class="container-fluid header_wrapper">
  <div class="container header">
    <div class="col-md-2 col-xs-10 col-sm-4 logo">
      <a href=""><img class="img-responsive" src="user_back_asset/images/logo.png" alt=""></a>
    </div>
    <div class="col-xs-2 col-sm-6 src_btn">
      <span class="glyphicon glyphicon-search"></span>
      <div class="clearfix"></div>
    </div>
    <div class="form_wrap col-md-8 col-xs-2 col-sm-8">
      <form action="">
        <div class="col-sm-12 col-md-12 form_input">
          <span class="search_icon">&#61442;</span>
          <input type="text" id="search" placeholder="Search Offers By Name, Catagory, Location . . . .">
          <input class="search_btn" type="submit" value="Show Offers">
        </div> 
      </form>
    </div>

    <!-- loaded popover content -->
    <div id="popover-content" style="width: 100px !important; display: none;">
     <ul class="list-group custom-popover">
      <li class="list-group-item"><a href="user">My Panel</a></li>
      <li class="list-group-item"> <a href="logout">Sign Out</a> </li>
    </ul>
  </div>


  <div class="col-md-2">
    <div class="login_signup">
      <div class="user_info">
        <?php if($this->session->userdata('login_id')==''){?>

          <i class="fa fa-user-circle" style="float: left;" aria-hidden="true"></i>

          <span style="margin-left: 5px;" class="login_u" data-toggle="modal" data-target="#login_modal">Login</span>
          <!-- Modal -->
          <hr class="log_underline">

          <span style="margin-left: 5px;" class="signup_u" data-toggle="modal" data-target="#login_modal">Signup</span>


        <?php } else{ ?>
         <img style=" width: 45px; height: 45px;border-radius: 50%;" src="uploads/user/profile_pic/<?=$this->session->userdata('p_image');?>" alt="">

         <span style="margin-left: 3px;"><a id="open" style="color: white;" href="user">
          <?php

          if(strlen(($this->session->userdata('name'))) <= 6)
          {
            echo $this->session->userdata('name');
          }

          else
          {
            echo  substr($this->session->userdata('name'),0,6).'...';
          }
          ?>
        </a><i style="color: white;margin-left: 5px;" data-toggle="popover" data-placement="bottom"  class="fa fa-caret-down"></i>
      </span>

    <?php } ?> 
  </div>
</div>
</div>
</div>
<!--HEADRER CLASS CLOSED HERE-->
</div>