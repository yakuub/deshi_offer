 
<!-- <script src="user_back_asset/js/jquery-1.10.2.min.js"></script> -->


<!-- <script src="user_back_asset/js/jquery-1.7.1.min.js"></script> -->

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> -->


<script src="user_back_asset/js/bootstrap.min.js"></script>
<!-- <script src="user_back_asset/owl-carousel/owl.carousel.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js"></script>
<script src="user_back_asset/js/custom.js"></script>
<script src="user_back_asset/js/star-rating.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.js"></script>
<script src="front_assets/js/notify.js"></script>
<!-- <script src="user_back_asset/js/select2.js"></script> -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<script src="user_back_asset/js/jasny-bootstrap.min.js"></script> 


<style type="text/css">
  .popover-title {
    text-align: center;
  }

  .custom-popover li {
    border: none!important;
    text-align: center;
    width:100px;
  }

  .custom-popover li:nth-child(2) {
    border-top: 1px solid #ccc!important;
  }

  .custom-popover li:last-child {
    border-top: 1px solid #ccc!important;
  }
</style>

<script type="text/javascript">
  $(document).ready(function() {
    $('[data-toggle="popover"]').popover({
      html: true,
      content: function() {
        return $('#popover-content').html();
      }
    });


  });


</script>




<script>

  $('.droppable').hover(function(){
    $('.dim').fadeIn(0);
  },function(){
    $('.dim').fadeOut(0);
  });
</script>


<script>
  $(function() {
    $('.droppable').on('mouseover', function() {
      $('.overlay').show();
    });
    $('.droppable').on('mouseout', function() {
      $('.overlay').hide();
    });
  });



</script>

<script>


  $(".modal-trigger").click(function(){
    $(".mobile_menu_wrapper").removeClass("width_ctrl");
    $(".menu_switch_new").removeClass("menu_cross");

  });

  
</script>

<script>
  function check_login()
  {
    var email=$("#login_email").val();
    var password=$("#login_password").val();
    var ok=0;
    $.ajax({
      url: "<?php echo site_url('login/ajax_login_check');?>",
      type: "post",
      data: {email:email,password:password},
      success: function(msg)
      {
        if(msg==0)
        {

          $("#login_msg").html('<span class="label label-danger">Email or Password is incorrect</span>');
          ok=1;
          return false;
        }
        else
        {
                  //return true;
                }
                //console.log(msg);
              }      
            }); 
    if(ok==1)
    {
      return false;
    }
  }
</script>

<script>
  function get_code(offer_id) 
  {
    $.ajax({
      url: "<?php echo site_url('home/get_code_modal');?>",
      type: "post",
      data: {offer_id:offer_id},
      success: function(msg)
      {
                    //console.log(msg);
                    
                    $("#coupon_modal").html(msg);
                    $("#coupon_modal").modal('show');
                    
                  }      
                });
  }


  function get_sms(offer_id) 
  {
    $.ajax({
      url: "<?php echo site_url('home/get_sms_modal');?>",
      type: "post",
      data: {offer_id:offer_id},
      success: function(msg)
      {
                    //console.log(msg);
                    
                    $("#sms_modal").html(msg);
                    $("#sms_modal").modal('show');
                    
                  }      
                });
  }


  function get_vouchar(offer_id) 
  {
    $.ajax({
      url: "<?php echo site_url('home/get_qr_code_modal');?>",
      type: "post",
      data: {offer_id:offer_id},
      success: function(msg)
      {
                    //console.log(msg);
                    
                    $("#vouchar_modal").html(msg);
                    $("#vouchar_modal").modal('show');
                    
                  }      
                });
  }



</script>

<script type="text/javascript">

  function close_notification(arg)
  {
    // alert('hi');

    var notification_id=arg;

    $.ajax({
      url: "<?php echo site_url('user/close_notification');?>",
      type: "post",
      data: {notification_id:notification_id},
      success: function(msg)
      {
         $("#notification").html(msg);
          $("#notification_outer").html($("#notification_inner").val());
      }      
    });
  }

  function close_notification_all(arg)
  {
    // alert('hi');

 

    $.ajax({
      url: "<?php echo site_url('user/close_notification_all');?>",
      type: "post",
      success: function(msg)
      {
         $("#notification").html(msg);
         $("#notification_outer").html($("#notification_inner").val());
      }      
    });
  }
</script>