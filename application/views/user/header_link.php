<!DOCTYPE html>

<base href="<?php echo base_url();?>">

<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" />

  <link rel="stylesheet" type="text/css" href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css">

  <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>




  <link rel="shortcut icon" type="image/ico" href="images/favicon.ico" />
  <link rel="stylesheet" href="user_back_asset/css/bootstrap.min.css">
  <link rel="stylesheet" href="user_back_asset/css/custom.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="user_back_asset/owl-carousel/owl.theme.css">
  <link rel="stylesheet" href="user_back_asset/css/star-rating.css">
  <!-- <link rel="stylesheet" href="user_back_asset/css/select2.css"> -->
  <link rel="stylesheet" href="user_back_asset/css/custom-heart.css">
  <link rel="stylesheet" href="user_back_asset/css/mobile-sidebar.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.css">

  <link rel="stylesheet" href="user_back_asset/css/jasny-bootstrap.css">

  <style type="text/css">
    .p_o_table td:not(:first-child), th:not(:first-child) {
      text-align: left;
    }
    .odtable td, th:not(:first-child) {
      text-align: left;
    }

    .od_summary_table{
      text-align: right;
    }


    
  </style>


</head>


