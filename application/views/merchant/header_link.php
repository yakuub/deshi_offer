<!DOCTYPE html>
<base href="<?php echo base_url();?>">
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" type="image/ico" href="images/favicon.ico" />
  <link rel="stylesheet" href="user_back_asset/css/bootstrap.min.css">
  <link rel="stylesheet" href="user_back_asset/css/custom.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="user_back_asset/owl-carousel/owl.theme.css">
  <link rel="stylesheet" href="user_back_asset/css/star-rating.css">
  <link rel="stylesheet" href="user_back_asset/css/select2.css">
  <link rel="stylesheet" href="user_back_asset/css/custom-heart.css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
