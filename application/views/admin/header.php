<!DOCTYPE html>
<base href="<?php echo base_url();?>">
<html>
<head>
    <title>Deshioffer - First, Largest & Leading offer platform in Bangladesh.</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <link href="favicon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="back_asset/css/main.css?version=2.6" rel="stylesheet">
    <link rel="stylesheet" href="back_asset/public/default.css" type="text/css">
    <link rel="stylesheet" href="back_asset/public/style.css" type="text/css">
    <link rel="stylesheet" href="back_asset/bower_components/bootstrap/dist/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="back_asset/bower_components/jasny-bootstrap/css/jasny-bootstrap.css" type="text/css">
    <link href="back_asset/icon_fonts_assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="//fast.fonts.net/cssapi/175a63a1-3f26-476a-ab32-4e21cbdb8be2.css" rel="stylesheet" type="text/css">
    <link href="back_asset/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="back_asset/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="back_asset/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="back_asset/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="back_asset/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="back_asset/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">

    <script src="back_asset/public/jscolor.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <style type='text/css'>
        .control:checked ~ .conditional,
        #immigrant:checked ~ .conditional,
        #required-2:checked ~ .conditional
        #option-2:checked ~ .conditional {
            clip: auto;
            height: auto;
            margin: 0;
            overflow: visible;
            position: static;
            width: auto;
        }

        .control:not(:checked) ~ .conditional,
        #immigrant:not(:checked) ~ .conditional,
        #required-2:not(:checked) ~ .conditional,
        #option-2:not(:checked) ~ .conditional {
            border: 0;
            clip: rect(0 0 0 0);
            height: 1px;
            margin: -1px;
            overflow: hidden;
            padding: 0;
            position: absolute;
            width: 1px;
        }


    </style>
</head>


