</div>
<div class="display-type"></div>
</div>
<script src="back_asset/bower_components/jquery/dist/jquery.min.js"></script>
<script src="back_asset/bower_components/moment/moment.js"></script>
<script src="back_asset/bower_components/chart.js/dist/Chart.min.js"></script>
<script src="back_asset/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="back_asset/bower_components/ckeditor/ckeditor.js"></script>
<script src="back_asset/bower_components/bootstrap-validator/dist/validator.min.js"></script>
<script src="back_asset/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="back_asset/bower_components/dropzone/dist/dropzone.js"></script>
<script src="back_asset/bower_components/editable-table/mindmup-editabletable.js"></script>
<script src="back_asset/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="back_asset/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="back_asset/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="back_asset/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>
<script src="back_asset/bower_components/bootstrap/js/dist/util.js"></script>
<script src="back_asset/bower_components/bootstrap/js/dist/tab.js"></script>
<script src="back_asset/js/main.js?version=2.6"></script>
<script src="back_asset/bower_components/bootstrap/dist/js/bootstrap.js"></script>
<script src="back_asset/bower_components/jasny-bootstrap/js/jasny-bootstrap.js"></script>
<script src="back_asset/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="back_asset/public/zebra_datepicker.js"></script>
<script type="text/javascript" src="back_asset/public/core.js"></script>


<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-42863888-9', 'auto');
	ga('send', 'pageview');
</script>

<script type="text/javascript">
	CKEDITOR.replace( 'offer_terms' );
	CKEDITOR.add            
</script>



<script type="text/javascript">
	$(document).ready(function() {

		$.ajax({
			url: "<?php echo site_url('admin/get_all_main_weekly_offer');?>",
			method:"POST",
			dataType:"json",
			success: function(data)
			{
				var offers=[];

				$.each(data['main_offer_info'], function (key, value) {

					$("#weekly_offer").append('<option value="' + value.offer_id + '">' + value.offer_title + '</option>');

				});

				

				$.each(data['weekly_offer_info'], function (key, value) {	

					offers.push(value.offer_id);

				});

				
				$('#weekly_offer').val(offers);

			}
		});

		$("#weekly_offer").select2({
			multiple: true,
		});


	});

	

	


	function get_all_weekly_offer() {
		$.ajax({
			url: "<?php echo site_url('admin/get_all_weekly_offer');?>",
			type: "post",
			success: function(msg)
			{
				$('#weekly_offer').val(['1', '2']);
			}
		});
	}
</script>

