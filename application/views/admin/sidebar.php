       <!-- Mobile Sidebar -->
       <div class="menu-mobile menu-activated-on-click color-scheme-dark">
        <div class="mm-logo-buttons-w">
          <a class="mm-logo" href="index.php"><img src="uploads/user_default.png/logo.png"><span>Clean Admin</span></a>
          <div class="mm-buttons">
            <div class="content-panel-open">
              <div class="os-icon os-icon-grid-circles"></div>
            </div>
            <div class="mobile-menu-trigger">
              <div class="os-icon os-icon-hamburger-menu-1"></div>
            </div>
          </div>
        </div>
        <div class="menu-and-user">
          <div class="logged-user-w">
            <div class="avatar-w">
              <img src="uploads/user_default.png" alt="" >
            </div>
            <div class="logged-user-info-w">
              <div class="logged-user-name">
                Mr.Joy
              </div>
              <div class="logged-user-role">
                Administrator
              </div>
            </div>
          </div>
          <ul class="main-menu">
            <li class="has-menu">
              <a href="admin">
                <div class="icon-w">
                  <div class="os-icon os-icon-window-content"></div>
                </div>
                <span>Dashboard</span></a>

              </li>
              <li class="has-sub-menu">
                <a href="javascript:void(0);">
                  <div class="icon-w">

                    <div class="os-icon os-icon-emoticon-smile"></div>
                  </div>
                  <span>Manage Admin</span></a>
                  <ul class="sub-menu">
                   <li>
                    <a href="admin/add_admin">Add Admin</a>
                  </li>
                  <li>
                    <a href="admin/admin_list">Admin List</a>
                  </li>
                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="javascript:void(0);">
                  <div class="icon-w">
                    <div class="os-icon os-icon-user-male-circle2"></div>
                  </div>
                  <span>Manage Merchant</span></a>
                  <ul class="sub-menu">
                    <li>
                      <a href="admin/add_merchant">Add Merchant</a>
                    </li>
                    <li>
                      <a href="admin/merchant_list">Merchant List</a>
                    </li>
                  </ul>
                </li> 
                <li class="has-sub-menu">
                  <a href="javascript:void(0);">
                    <div class="icon-w">
                      <div class="os-icon os-icon-user-male-circle"></div>
                    </div>
                    <span>Manage User</span></a>
                    <ul class="sub-menu">
                      <li>
                        <a href="admin/add_user">Add User</a>
                      </li>
                      <li>
                        <a href="admin/user_list">User List</a>
                      </li>
                    </ul>
                  </li>

                  <li class="has-sub-menu">
                    <a href="javascript:void(0);">
                      <div class="icon-w">
                        <div class="os-icon os-icon-delivery-box-2"></div>
                      </div>
                      <span>Manage Slider</span></a>
                      <ul class="sub-menu">
                        <li>
                          <a href="admin/add_slider">Add Slider</a>
                        </li>
                        <li>
                          <a href="admin/slider">Slider List</a>
                        </li>

                      </ul>
                    </li> 


                    <li class="has-sub-menu">
                      <a href="javascript:void(0);">
                        <div class="icon-w">
                          <div class="os-icon os-icon-delivery-box-2"></div>
                        </div>
                        <span>Manage Preference</span></a>
                        <ul class="sub-menu">
                          <li>
                            <a href="admin/add_preference">Add Preference</a>
                          </li>
                          <li>
                            <a href="admin/preference_list">Preference List</a>
                          </li>

                        </ul>
                      </li> 


                      <li class="has-sub-menu">
                        <a href="javascript:void(0);">
                          <div class="icon-w">
                            <div class="os-icon os-icon-delivery-box-2"></div>
                          </div>
                          <span>Manage Area</span></a>
                          <ul class="sub-menu">
                            <li>
                              <a href="admin/add_division">Add Division</a>
                            </li>
                            <li>
                              <a href="admin/add_district">Add District</a>
                            </li>
                            <li>
                              <a href="admin/add_area">Add Area</a>
                            </li>

                          </ul>
                        </li>
                        <li class="has-sub-menu">
                          <a href="javascript:void(0);">
                            <div class="icon-w">
                              <div class="os-icon os-icon-grid-squares"></div>
                            </div>
                            <span>Manage Category</span></a>
                            <ul class="sub-menu">
                              <li>
                                <a href="admin/add_category">Add Category</a>
                              </li>
                 <!--  <li>
                    <a href="javascript:void(0);">Category List</a>
                  </li> -->


                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="javascript:void(0);">
                  <div class="icon-w">
                    <div class="os-icon os-icon-newspaper"></div>
                  </div>
                  <span>Manage Offer</span></a>

                  <ul class="sub-menu">
                    <li>
                      <a href="admin/add_offer_type">Add Offer Type</a>
                    </li>
                    <li>
                      <a href="admin/add_target_people">Add Target People</a>
                    </li>
                    <li>
                      <a href="admin/add_new_offer">Add New Offer</a>
                    </li>
                    <li>
                      <a href="admin/new_offer_list">Offer List</a>
                    </li>
                    <li>
                      <a href="admin/highlight_offer_list">Highlted Offer List</a>
                    </li>
                    <li>
                      <a href="admin/offer_title_type_list">Offer Title type</a>
                    </li> 
                    <li>
                      <a href="admin/active_offer_list">Active Offer</a>
                    </li>
                    <li>
                      <a href="admin/upcoming_offer_list">Upcoming Offer</a>
                    </li>
                    <li>
                      <a href="admin/expired_offer_list">Expired Offer</a>
                    </li>
                    <li>
                      <a href="admin/deleted_offer_list">Deleted Offer</a>
                    </li>
                  </ul>
                </li>
                <li class="has-sub-menu">
                  <a href="javascript:void(0);">
                    <div class="icon-w">
                      <div class="os-icon os-icon-tasks-checked"></div>
                    </div>
                    <span>Boost</span></a>
                    <ul class="sub-menu">
                      <li>
                        <a href="javascript:void(0);">Boost Offer</a>
                      </li>
                      <li>
                        <a href="javascript:void(0);">Boost Collection</a>
                      </li>

                    </ul>
                  </li>
                  <li class="has-sub-menu">
                    <a href="javascript:void(0);">
                      <div class="icon-w">
                        <div class="os-icon os-icon-grid-squares"></div>
                      </div>
                      <span>Manage Subscriber</span></a>
                      <ul class="sub-menu">
                        <li>
                          <a href="javascript:void(0);">Add New Subscriber</a>
                        </li>
                        <li>
                          <a href="javascript:void(0);">Subscriber List</a>
                        </li>

                      </ul>
                    </li>
                    <li class="has-sub-menu">
                      <a href="javascript:void(0);">
                        <div class="icon-w">
                          <div class="os-icon os-icon-grid-squares"></div>
                        </div>
                        <span>Manage Promotional Mail</span></a>
                        <ul class="sub-menu">
                          <li>
                            <a href="javascript:void(0);">Mail Subject</a>
                          </li>
                          <li>
                            <a href="javascript:void(0);">Add Email Template</a>
                          </li> 
                          <li>
                            <a href="javascript:void(0);">Send Mail</a>
                          </li>
                          <li>
                            <a href="javascript:void(0);">Mail List</a>
                          </li>
                        </ul>
                      </li>	  
                      <li class="has-sub-menu">
                        <a href="javascript:void(0);">
                          <div class="icon-w">
                            <div class="os-icon os-icon-grid-squares"></div>
                          </div>
                          <span>Manage Promotional SMS</span></a>
                          <ul class="sub-menu">
                            <li>
                              <a href="javascript:void(0);">SMS Subject</a>
                            </li>
                            <li>
                              <a href="javascript:void(0);">Add SMS Template</a>
                            </li> 
                            <li>
                              <a href="javascript:void(0);">Send SMS</a>
                            </li>
                            <li>
                              <a href="javascript:void(0);">SMS List</a>
                            </li>
                          </ul>
                        </li>	
                        <li class="has-sub-menu">
                          <a href="javascript:void(0);">
                            <div class="icon-w">
                              <div class="os-icon os-icon-grid-squares"></div>
                            </div>
                            <span>Manage Advertisement</span></a>
                            <ul class="sub-menu">
                              <li>
                                <a href="javascript:void(0);">Advertise Title</a>
                              </li>
                              <li>
                                <a href="javascript:void(0);">Set Advertise</a>
                              </li> 
                              <li>
                                <a href="javascript:void(0);">Advertise List</a>
                              </li>

                            </ul>
                          </li>	
                          <li class="has-sub-menu">
                            <a href="javascript:void(0);">
                              <div class="icon-w">
                                <div class="os-icon os-icon-grid-squares"></div>
                              </div>
                              <span>Manage Support</span></a>
                              <ul class="sub-menu">
                                <li>
                                  <a href="javascript:void(0);">Add Support Title</a>
                                </li>
                                <li>
                                  <a href="javascript:void(0);">Query List</a>
                                </li> 


                              </ul>
                            </li>	
                            <li class="has-sub-menu">
                              <a href="javascript:void(0);">
                                <div class="icon-w">
                                  <div class="os-icon os-icon-grid-squares"></div>
                                </div>
                                <span>Manage Web CMS</span></a>
                                <ul class="sub-menu">
                                  <li>
                                    <a href="javascript:void(0);">Add New Data</a>
                                  </li>
                                  <li>
                                    <a href="javascript:void(0);">Page Content</a>
                                  </li> 
                                  <li>
                                    <a href="javascript:void(0);">Contact Request</a>
                                  </li> 

                                </ul>
                              </li>	 
                            </ul>
                            <div class="mobile-menu-magic">
                              <h4>
                               Deshi-Offer Admin
                             </h4>

                           </div>
                         </div>
                       </div>


                       <!-- Desktop Sidebar -->
                       <div class="desktop-menu menu-side-w menu-activated-on-click">
                        <div class="logo-w">
                          <a class="logo" href="index.html"><img src="back_asset/img/logo.png"><span>Clean Admin</span></a>
                        </div>
                        <div class="menu-and-user">
                          <div class="logged-user-w">
                            <div class="avatar-w">
                              <img alt="" src="uploads/user_default.png">
                            </div>
                            <div class="logged-user-info-w">
                              <div class="logged-user-name">
                                Maria Gomez
                              </div>
                              <div class="logged-user-role">
                                Administrator
                              </div>
                            </div>
                            <div class="logged-user-menu">
                              <div class="avatar-w">
                                <img alt="" src="uploads/user_default.png">
                              </div>
                              <div class="logged-user-info-w">
                                <div class="logged-user-name">
                                  Maria Gomez
                                </div>
                                <div class="logged-user-role">
                                  Administrator
                                </div>
                              </div>
                              <div class="bg-icon">
                                <i class="os-icon os-icon-wallet-loaded"></i>
                              </div>
                              <ul>
                                <li>
                                  <a href="javascript:void(0);"><i class="os-icon os-icon-mail-01"></i><span>Incoming Mail</span></a>
                                </li>
                                <li>
                                  <a href="users_profile_big.html"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
                                </li>
                                <li>
                                  <a href="users_profile_small.html"><i class="os-icon os-icon-coins-4"></i><span>Billing Details</span></a>
                                </li>
                                <li>
                                  <a href="javascript:void(0);"><i class="os-icon os-icon-others-43"></i><span>Notifications</span></a>
                                </li>
                                <li>
                                  <a href="logout"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <ul class="main-menu">
                            <li class="has-menu">
                              <a href="admin">
                                <div class="icon-w">
                                  <div class="os-icon os-icon-window-content"></div>
                                </div>
                                <span>Dashboard</span></a>

                              </li>
                              <li class="has-sub-menu">
                                <a href="javascript:void(0);">
                                  <div class="icon-w">

                                    <div class="os-icon os-icon-emoticon-smile"></div>
                                  </div>
                                  <span>Manage Admin</span></a>
                                  <ul class="sub-menu">
                                    <li>
                                      <a href="admin/add_admin">Add Admin</a>
                                    </li>
                                    <li>
                                      <a href="admin/admin_list">Admin List</a>
                                    </li>
                                  </ul>
                                </li>
                                <li class="has-sub-menu">
                                  <a href="javascript:void(0);">
                                    <div class="icon-w">
                                      <div class="os-icon os-icon-user-male-circle2"></div>
                                    </div>
                                    <span>Manage Merchant</span></a>
                                    <ul class="sub-menu">
                                      <li>
                                        <a href="admin/add_merchant">Add Merchant</a>
                                      </li>
                                      <li>
                                        <a href="admin/merchant_list">Merchant List</a>
                                      </li>
                                    </ul>
                                  </li>

                                  <li class="has-sub-menu">
                                    <a href="javascript:void(0);">
                                      <div class="icon-w">
                                        <div class="os-icon os-icon-user-male-circle2"></div>
                                      </div>
                                      <span>Manage Weekly Offer</span></a>
                                      <ul class="sub-menu">
                                        <li>
                                          <a href="admin/add_weekly_offer">Add Weekly Offer</a>
                                        </li>
                                       
                                      </ul>
                                    </li>

                                    <li class="has-sub-menu">
                                      <a href="javascript:void(0);">
                                        <div class="icon-w">
                                          <div class="os-icon os-icon-user-male-circle"></div>
                                        </div>
                                        <span>Manage User</span></a>
                                        <ul class="sub-menu">
                                          <li>
                                            <a href="admin/add_user">Add User</a>
                                          </li>
                                          <li>
                                            <a href="admin/user_list">User List</a>
                                          </li>
                                        </ul>
                                      </li>
                                      <li class="has-sub-menu">
                                        <a href="javascript:void(0);">
                                          <div class="icon-w">
                                            <div class="os-icon os-icon-delivery-box-2"></div>
                                          </div>
                                          <span>Manage Slider</span></a>
                                          <ul class="sub-menu">
                                            <li>
                                              <a href="admin/add_slider">Add Slider</a>
                                            </li>
                                            <li>
                                              <a href="admin/slider">Slider List</a>
                                            </li>

                                          </ul>
                                        </li>

                                        <li class="has-sub-menu">
                                          <a href="javascript:void(0);">
                                            <div class="icon-w">
                                              <div class="os-icon os-icon-delivery-box-2"></div>
                                            </div>
                                            <span>Manage Preference</span></a>
                                            <ul class="sub-menu">
                                              <li>
                                                <a href="admin/add_preference">Add Preference</a>
                                              </li>
                                              <li>
                                                <a href="admin/preference_list">Preference List</a>
                                              </li>

                                            </ul>
                                          </li> 
                                          <li class="has-sub-menu">
                                            <a href="javascript:void(0);">
                                              <div class="icon-w">
                                                <div class="os-icon os-icon-delivery-box-2"></div>
                                              </div>
                                              <span>Manage Area</span></a>
                                              <ul class="sub-menu">
                                                <li>
                                                  <a href="admin/add_division">Add Division</a>
                                                </li>
                                                <li>
                                                  <a href="admin/add_district">Add District</a>
                                                </li>
                                                <li>
                                                  <a href="admin/add_area">Add Area</a>
                                                </li>

                                              </ul>
                                            </li>
                                            <li class="has-sub-menu">
                                              <a href="javascript:void(0);">
                                                <div class="icon-w">
                                                  <div class="os-icon os-icon-grid-squares"></div>
                                                </div>
                                                <span>Manage Category</span></a>
                                                <ul class="sub-menu">
                                                  <li>
                                                    <a href="admin/add_category">Add Category</a>
                                                  </li>
                 <!--  <li>
                    <a href="javascript:void(0);">Category List</a>
                  </li> -->


                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="javascript:void(0);">
                  <div class="icon-w">
                    <div class="os-icon os-icon-newspaper"></div>
                  </div>
                  <span>Manage Offer</span></a>
                  <ul class="sub-menu">
                    <li>
                      <a href="admin/add_offer_type">Add Offer Type</a>
                    </li>
                    <li>
                      <a href="admin/add_target_people">Add Target People</a>
                    </li>
                    <li>
                      <a href="admin/add_new_offer">Add New Offer</a>
                    </li>
                    <li>
                      <a href="admin/new_offer_list">Offer List</a>
                    </li>

                    <li>
                      <a href="admin/highlight_offer_list">Highlted Offer List</a>
                    </li>

                    <li>
                      <a href="admin/offer_title_type_list">Offer Title type</a>
                    </li> 
                    <li>
                      <a href="admin/active_offer_list">Active Offer</a>
                    </li>
                    <li>
                      <a href="admin/upcoming_offer_list">Upcoming Offer</a>
                    </li>
                    <li>
                      <a href="admin/expired_offer_list">Expired Offer</a>
                    </li>
                    <li>
                      <a href="admin/deleted_offer_list">Deleted Offer</a>
                    </li>
                  </ul>
                </li>
                <li class="has-sub-menu">
                  <a href="javascript:void(0);">
                    <div class="icon-w">
                      <div class="os-icon os-icon-tasks-checked"></div>
                    </div>
                    <span>Boost</span></a>
                    <ul class="sub-menu">
                      <li>
                        <a href="javascript:void(0);">Boost Offer</a>
                      </li>
                      <li>
                        <a href="javascript:void(0);">Boost Collection</a>
                      </li>

                    </ul>
                  </li>
                  <li class="has-sub-menu">
                    <a href="javascript:void(0);">
                      <div class="icon-w">
                        <div class="os-icon os-icon-grid-circles"></div>
                      </div>
                      <span>Manage Subscriber</span></a>
                      <ul class="sub-menu">
                        <li>
                          <a href="javascript:void(0);">Add New Subscriber</a>
                        </li>
                        <li>
                          <a href="javascript:void(0);">Subscriber List</a>
                        </li>

                      </ul>
                    </li>
                    <li class="has-sub-menu">
                      <a href="javascript:void(0);">
                        <div class="icon-w">
                          <div class="os-icon os-icon-grid-squares"></div>
                        </div>
                        <span>Manage Promotional Mail</span></a>
                        <ul class="sub-menu">
                          <li>
                            <a href="javascript:void(0);">Mail Subject</a>
                          </li>
                          <li>
                            <a href="javascript:void(0);">Add Email Template</a>
                          </li> 
                          <li>
                            <a href="javascript:void(0);">Send Mail</a>
                          </li>
                          <li>
                            <a href="javascript:void(0);">Mail List</a>
                          </li>
                        </ul>
                      </li>	  
                      <li class="has-sub-menu">
                        <a href="javascript:void(0);">
                          <div class="icon-w">
                            <div class="os-icon os-icon-text-input"></div>
                          </div>
                          <span>Manage Promotional SMS</span></a>
                          <ul class="sub-menu">
                            <li>
                              <a href="javascript:void(0);">SMS Subject</a>
                            </li>
                            <li>
                              <a href="javascript:void(0);">Add SMS Template</a>
                            </li> 
                            <li>
                              <a href="javascript:void(0);">Send SMS</a>
                            </li>
                            <li>
                              <a href="javascript:void(0);">SMS List</a>
                            </li>
                          </ul>
                        </li>	
                        <li class="has-sub-menu">
                          <a href="javascript:void(0);">
                            <div class="icon-w">
                              <div class="os-icon os-icon-delivery-box-2"></div>
                            </div>
                            <span>Manage Advertisement</span></a>
                            <ul class="sub-menu">
                              <li>
                                <a href="javascript:void(0);">Advertise Title</a>
                              </li>
                              <li>
                                <a href="javascript:void(0);">Set Advertise</a>
                              </li> 
                              <li>
                                <a href="javascript:void(0);">Advertise List</a>
                              </li>

                            </ul>
                          </li>	
                          <li class="has-sub-menu">
                            <a href="javascript:void(0);">
                              <div class="icon-w">
                                <div class="os-icon os-icon-ui-55"></div>
                              </div>
                              <span>Manage Support</span></a>
                              <ul class="sub-menu">
                                <li>
                                  <a href="javascript:void(0);">Add Support Title</a>
                                </li>
                                <li>
                                  <a href="javascript:void(0);">Query List</a>
                                </li> 


                              </ul>
                            </li>	
                            <li class="has-sub-menu">
                              <a href="javascript:void(0);">
                                <div class="icon-w">
                                  <div class="os-icon os-icon-tasks-checked"></div>
                                </div>
                                <span>Manage Web CMS</span></a>
                                <ul class="sub-menu">
                                  <li>
                                    <a href="javascript:void(0);">Add New Data</a>
                                  </li>
                                  <li>
                                    <a href="javascript:void(0);">Page Content</a>
                                  </li> 
                                  <li>
                                    <a href="javascript:void(0);">Contact Request</a>
                                  </li> 
                                  <li>
                                    <a href="logout">Log Out</a>
                                  </li> 

                                </ul>
                              </li>	 
                            </ul>
                            <div class="side-menu-magic">
                              <h4>
                                Deshi-offer Admin
                              </h4>


                            </div>
                          </div>
                        </div>