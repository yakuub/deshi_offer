<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends MX_Controller {


    function __construct()
	{
		$this->load->model('home/home_model');
		$this->load->model('product_model');

		date_default_timezone_set('Asia/Dhaka');
		if($this->session->userdata('language_select')=='bangla')
        {
            $this->lang->load('front', 'bangla');
        }
        else
        {
            $this->lang->load('front', 'english');
        } 

    }
	public function index()
	{
		/// Common Date Start
	    	$area_retreive_id=$this->session->userdata('area_retreive_id');
			$data['active']='product';
			$data['header']=$this->home_model->select_with_where('*','id=1','company');
			$data['brand']=$this->home_model->select_all_acending('brand','brand_name');
			$data['category']=$this->home_model->select_all_acending('category','position');
			$data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
			$data['low_amt']=0;
			$data['high_amt']=800000;
			$data['cat_active']=0;
			$data['sub_cat_active']=0;
		// Common Data End
	    //page Data Starts
			$data['size_list']=$this->home_model->select_all('size');
			$data['color_list']=$this->home_model->select_all('color');
			$data['page_title']='All Products';
			$data['cat_active']='';
			$data['product_list']=$this->home_model->select_condition_decending_with_limit('product','status=1',12);
		$this->load->view('index',$data);
	}

	public function filter($cat_name='',$sub_cat_name='',$price='',$order_col='',$order_type='')
	{
		// Color, Size, Brand can be multiple. Thats why query through ajax for these purpose.

		
		/// Common Date Start
	    	$area_retreive_id=$this->session->userdata('area_retreive_id');
			$data['active']='product';
			$data['header']=$this->home_model->select_with_where('*','id=1','company');
			$data['brand']=$this->home_model->select_all_acending('brand','brand_name');
			$data['category']=$this->home_model->select_all_acending('category','position');
			$data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
			$data['size_list']=$this->home_model->select_all('size');
			$data['color_list']=$this->home_model->select_all('color');
		// Common Data End
	    //page Data Starts
			$data['page_title']='All Products';
			$data['low_amt']=0;
			$data['high_amt']=800000;
			$data['cat_active']=0;
			$data['sub_cat_active']=0;
			$low_amt=0;

			
			if($this->input->get('price'))
			{
				$price_range=$this->input->get('price');
				$split_price=explode('৳', $price_range);
				$low_price_split=explode('-', $split_price[1]);
				$high_price_split=explode(' ', $split_price[2]);
				$low_amt=$low_price_split[0];
				$high_amt=$high_price_split[0];
				$data['low_amt']=$low_amt;
				$data['high_amt']=$high_amt;
				//echo $low_amt.'-'.$high_amt;die();
				//echo "<pre>";print_r($low_price_split);die();
			}
			
			//print_r($get_sub_cat);die();

			$data['product_list']=$this->product_model->get_item_ajax_data('status=1','created_at','DESC');

			if($cat_name!='')
			{
				// category
				$get_cat=$this->home_model->select_with_where('*','name="'.$cat_name.'"','category');
				$cat_id=$get_cat[0]['id'];
				$data['cat_active']=$cat_id;

				if($this->input->get('price'))
				{
					$data['product_list']=$this->product_model->get_item_ajax_data('status=1 AND cat_id='.$cat_id.' AND price >='.$low_amt.' AND price<='.$high_amt,'created_at','DESC');
				}

				else
				{
					$data['product_list']=$this->product_model->get_item_ajax_data('status=1 AND cat_id='.$cat_id,'created_at','DESC');
				}

				
			}

			if($cat_name!='' && $sub_cat_name!='')
			{
				// category
				$get_cat=$this->home_model->select_with_where('*','name="'.$cat_name.'"','category');
				$cat_id=$get_cat[0]['id'];
				$data['cat_active']=$cat_id;

				// Sub Category
				$get_sub_cat=$this->home_model->select_with_where('*','cat_id='.$cat_id.' AND sub_name="'.$sub_cat_name.'"','sub_category');
				$sub_cat_id=$get_sub_cat[0]['id'];
				$data['sub_cat_active']=$sub_cat_id;

				if($this->input->get('price'))
				{
					$data['product_list']=$this->product_model->get_item_ajax_data('status=1 AND cat_id='.$cat_id.' AND sub_cat_id='.$sub_cat_id.' AND price >='.$low_amt.' AND price<='.$high_amt,'created_at','DESC');
				}

				else
				{
					$data['product_list']=$this->product_model->get_item_ajax_data('status=1 AND cat_id='.$cat_id.' AND sub_cat_id='.$sub_cat_id,'created_at','DESC');
				}
			}
		$this->load->view('index',$data);
	}

	public function product_details($name='')
	{
		/// Common Date Start
			
			$data['active']='product';
			$data['header']=$this->home_model->select_with_where('*','id=1','company');
			$data['brand']=$this->home_model->select_all_acending('brand','brand_name');
			$data['category']=$this->home_model->select_all_acending('category','position');
			$data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
		// Common Data End
		// page Data starts

			$data['product_details']=$this->product_model->product_details('profile_name="'.$name.'"');
			$data['related_product']=$this->home_model->select_condition_decending_with_limit('product','status=1 AND cat_id='.$data['product_details'][0]['p_cat_id'].' AND p_id!='.$data['product_details'][0]['p_id'],6);

			$data['color_list']=$this->home_model->select_all('color');
			$data['size_list']=$this->home_model->select_all('size');

			$data['latest_list']=$this->home_model->select_condition_decending_with_limit('product','status=1','12');
			//echo "<pre>"; print_r($data['product_details']);die();
		// page Data End
		$this->load->view('product_details',$data);
	}

	public function quick_view_product()
	{
		$p_id=$this->input->post('p_id');
		$data['product_details']=$this->product_model->product_details('product.p_id='.$p_id);
		$this->load->view('quick_view', $data);
	}

	public function category($name='')
	{
		/// Common Date Start
	    	$area_retreive_id=$this->session->userdata('area_retreive_id');
			$data['active']='category';
			$data['header']=$this->home_model->select_with_where('*','login_id='.$area_retreive_id,'company');
			$data['brand']=$this->home_model->select_all_acending('brand','brand_name');
			$data['category']=$this->home_model->select_all_acending('category','name');
			$data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
		// Common Data End
	    //page Data Starts
			$data['page_title']='Product-'.$name;
			$data['left_cat']=$name;
			$data['left_sub_cat']='';
			$cat_id=$this->home_model->select_with_where('id','REPLACE(name, " ", "%20") LIKE "'.$name.'"','category');
			$data['product_list']=$this->home_model->select_condition_decending_with_limit('product','status=1 AND user_id='.$area_retreive_id.' AND cat_id='.$cat_id[0]['id'],12);
			$this->load->view('index',$data);
	}

	public function sub_category($name='',$sub_name)
	{
		/// Common Date Start
	    	$area_retreive_id=$this->session->userdata('area_retreive_id');
			$data['active']='category';
			$data['header']=$this->home_model->select_with_where('*','login_id='.$area_retreive_id,'company');
			$data['brand']=$this->home_model->select_all_acending('brand','brand_name');
			$data['category']=$this->home_model->select_all_acending('category','name');
			$data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
		// Common Data End
	    //page Data Starts
			$data['page_title']='Product/ '.str_replace("%20"," ",$name).'/ '.str_replace("%20"," ",$sub_name);
			$data['left_cat']=str_replace("%20"," ",$name);
			$data['left_sub_cat']=str_replace("%20"," ",$sub_name);
			$cat_id=$this->home_model->select_with_where('id','REPLACE(name, " ", "%20") LIKE "'.$name.'"','category');
			$sub_cat_id=$this->home_model->select_with_where('id','REPLACE(sub_name, " ", "%20") LIKE "'.$sub_name.'"','sub_category');
			$data['product_list']=$this->home_model->select_condition_decending_with_limit('product','status=1 AND user_id='.$area_retreive_id.' AND cat_id='.$cat_id[0]['id'].' AND sub_cat_id='.$sub_cat_id[0]['id'],12);
			$this->load->view('index',$data);
	}

	public function get_search_item()
	{
		$area_retreive_id=$this->session->userdata('area_retreive_id');
		$search_tag=$this->input->post('search_tag');
		$cat_id_search=$this->input->post('cat_id_search');
		$cat_id=0;$sub_cat_id=0;
		//print_r(explode('|', $cat_id_search));
		
		$explit=explode('|', $cat_id_search);
		$cat_id=$explit[0];
		if(array_key_exists(1,$explit))
		{
			$sub_cat_id=$explit[1];
		}
		
		$product_list=array();

		
		if($search_tag==''){
			if($sub_cat_id!=0 && $cat_id!=0)
			{
				$product_list=$this->home_model->select_condition_decending_with_limit('product','status=1 AND user_id='.$area_retreive_id.' AND cat_id='.$cat_id.' AND sub_cat_id='.$sub_cat_id,15);
			}

			else if($cat_id!=0)
			{
				$product_list=$this->home_model->select_condition_decending_with_limit('product','status=1 AND user_id='.$area_retreive_id.' AND cat_id='.$cat_id,15);
			}

			else
			{
				$product_list=$this->home_model->select_condition_decending_with_limit('product','status=1 AND user_id='.$area_retreive_id,15);
			}
		}
		else
		{
			if($sub_cat_id!=0 && $cat_id!=0)
			{
				$product_list=$this->home_model->select_condition_decending_with_limit('product','status=1 AND user_id='.$area_retreive_id.' AND cat_id='.$cat_id.' AND sub_cat_id='.$sub_cat_id.' AND p_name LIKE "'.$search_tag.'%"',15);
			}

			else if($cat_id!=0)
			{
				$product_list=$this->home_model->select_condition_decending_with_limit('product','status=1 AND user_id='.$area_retreive_id.' AND cat_id='.$cat_id.' AND p_name LIKE "'.$search_tag.'%"',15);
			}

			else
			{
				$product_list=$this->home_model->select_condition_decending_with_limit('product','status=1 AND user_id='.$area_retreive_id.' AND p_name LIKE "'.$search_tag.'%"',15);
			}
		}

		echo '<li onclick="close_search()" style="color:red;cursor:pointer;" class="right">Close X</li>';
		if(count($product_list)>0)
		{ 
      		foreach ($product_list as $key => $value) 
      		{

      			echo '<li class="col-md-12">';
                    echo '<div class="col-md-3">';
                    if($value['main_image']==''){
                      echo '<img src="uploads/product/default.png" alt="">';
                    }
                    else
                    {
                    	echo '<img src="uploads/product/'.$value['main_image'].'" alt="">';
                    }
                    echo '</div>';
                    echo '<div class="col-md-9">';
                      echo '<h5 style="margin: 0px;font-weight: 600">'.$value['p_name'].'</h5>';
                      echo '<div class="price-box">';
                      if($value['discount']>0)
                      	{
                                echo '<p class="special-price"> 
                                		<span class="price">450.00৳</span> 
                                	</p>
                          		<p class="old-price"> 
                          			<span class="price-sep">-</span> <span class="price"> 500.00৳ </span> 
                          		</p>';
                        }
                        else
                        {
                        	echo '<p class="special-price"> 
                                		<span class="price">'.number_format($value['price'],'2','.','').'৳</span> 
                                	</p>';
                        }

                            echo '</div>';
                      echo '<button onclick="add_to_cart('.$value['p_id'].')" style="padding: 0px;margin-left: 0px" class="buy-btn"><i class="icon-shopping-cart"></i> Add to Cart</button>';
                    echo '</div>';
                echo '</li>';
      		}
      	}
      	else
      	{
      		echo '<li class="col-md-12"><h4> No Product Available</h4></li>';
      	}
	}


	public function get_item_ajax()
	{
		$area_retreive_id=$this->session->userdata('area_retreive_id');
		$cat_id=0;$sub_cat_id=0;
		$cat_id=$this->input->post('cat_id');
		$sub_cat_id=$this->input->post('sub_cat_id');
		$sort_section=$this->input->post('sort_section');
		$brand_checked=$this->input->post('brand_checked');
		$low_amt=floatval($this->input->post('low_amt'));
		$high_amt=floatval($this->input->post('high_amt'));

		$order_type;$order_col;
		if($sort_section=='date_desc')
		{
			$order_type='DESC';
			$order_col='created_at';
		}

		if($sort_section=='date_asc')
		{
			$order_type='ASC';
			$order_col='created_at';
		}

		if($sort_section=='price_desc')
		{
			$order_type='DESC';
			$order_col='d_price';
		}

		if($sort_section=='price_asc')
		{
			$order_type='ASC';
			$order_col='d_price';
		}


		$product_list=array();

		if($cat_id==0 && $sub_cat_id==0)
		{
			$product_list=$this->product_model->get_item_ajax_data('status=1 AND user_id='.$area_retreive_id,$brand_checked,$order_col,$order_type);
		}
		//echo "<pre>";print_r($product_list);die();
		if($cat_id!=0 && $sub_cat_id==0)
		{
			$product_list=$this->product_model->get_item_ajax_data('status=1 AND user_id='.$area_retreive_id.' AND cat_id='.$cat_id,$brand_checked,$order_col,$order_type);
		}

		if($cat_id!=0 && $sub_cat_id!=0)
		{
			$product_list=$this->product_model->get_item_ajax_data('status=1 AND user_id='.$area_retreive_id.' AND cat_id='.$cat_id.' AND sub_cat_id='.$sub_cat_id,$brand_checked,$order_col,$order_type);
		}


		if(count($product_list)>0){ 
      	foreach ($product_list as $key => $value) {
      	if($value['d_price']>=$low_amt && $value['d_price']<=$high_amt){
      	echo '<li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">';
        echo '<div class="col-item">';
          echo '<div class="product-image-area"> <a class="product-image" title="Sample Product" href="product/product_details/'.$value['profile_name'].'">'; 
            if($value['main_image']!='')
            {
              echo '<img style="height: 285px;width: 100%" src="uploads/product/'.$value['main_image'].'" class="img-responsive" alt="'.$value['p_name'].'" />'; 
            } 
            else
            { 
              echo '<img style="height: 285px;width: 100%" src="uploads/product/default.png" class="img-responsive" alt="'.$value['p_name'].'" />'; 
            }
          echo '</a>';
            echo '<div class="hover_fly">'; 
              echo '<a onclick="add_to_cart('.$value['p_id'].')" class="exclusive ajax_add_to_cart_button" href="JavaScript:0;" title="Add to cart">
                <div><i class="icon-shopping-cart"></i><span>Add to cart</span></div>
              </a> ';

              echo '<a class="quick-view" href="javascript:0;" onclick="quick_view('.$value['p_id'].')">
                <div><i class="icon-eye-open"></i><span>Quick view</span></div>
              </a> 
            </div>
          </div>
          <div class="info">
            <div class="info-inner">';
              echo '<div class="item-title"> <a title="'.$value['p_name'].'" href="product/product_details/'.$value['profile_name'].'"> '.$value['p_name'].'</a> </div>';
              
              echo '<div class="item-content">';
                
                echo '<div class="price-box">';
                if($value['discount']==0)
                {
                  echo '<p class="special-price"> <span class="price">'.number_format($value['price'],2,'.','').'&#x9f3;</span> </p>';
                } 
                else 
                { 
                  echo '<p class="special-price"> <span class="price">';
                  $price=$value['price']-(($value['price']*$value['discount'])/100);

                	echo number_format($price,2,'.','').'&#x9f3;</span> </p>';
                 	echo '<p class="old-price"> <span class="price-sep">-</span> <span class="price"> '.number_format($value['price'],2,'.','').'&#x9f3; </span> </p>';
                }
                echo '</div>';
              echo '</div>'; 
            echo '</div>';
            echo '<div class="clearfix"> </div>';
          echo '</div>';
        echo '</div>';
        echo '<div id="loading_gif_'.$value['p_id'].'" style="display:none;position: absolute;width: 100px;left: 34%;top: 22%;">';
          		echo '<img style="width: 100%" src="front_assets/images/loading.gif" alt="">
        </div>
      </li>';
      echo '<input type="hidden" class="last_id" value="'.$value['p_id'].'">';

      }} }else
      {
      	echo '<li><h3><i class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></i> No product available</h3></li>';
       }
	}

	public function get_item_ajax_on_scroll()
	{
		$last_id=$this->input->post('last_id');
		$area_retreive_id=$this->session->userdata('area_retreive_id');
		$cat_id=0;$sub_cat_id=0;
		$cat_id=$this->input->post('cat_id');
		$sub_cat_id=$this->input->post('sub_cat_id');
		$sort_section=$this->input->post('sort_section');
		$brand_checked=$this->input->post('brand_checked');
		$low_amt=floatval($this->input->post('low_amt'));
		$high_amt=floatval($this->input->post('high_amt'));

		$order_type;$order_col;
		if($sort_section=='date_desc')
		{
			$order_type='DESC';
			$order_col='created_at';
		}

		if($sort_section=='date_asc')
		{
			$order_type='ASC';
			$order_col='created_at';
		}

		if($sort_section=='price_desc')
		{
			$order_type='DESC';
			$order_col='d_price';
		}

		if($sort_section=='price_asc')
		{
			$order_type='ASC';
			$order_col='d_price';
		}


		$product_list=array();

		if($cat_id==0 && $sub_cat_id==0)
		{
			$product_list=$this->product_model->get_item_ajax_data('status=1 AND user_id='.$area_retreive_id.' AND p_id<'.$last_id,$brand_checked,$order_col,$order_type);
		}
		//echo "<pre>";print_r($product_list);die();
		if($cat_id!=0 && $sub_cat_id==0)
		{
			$product_list=$this->product_model->get_item_ajax_data('status=1 AND user_id='.$area_retreive_id.' AND cat_id='.$cat_id.' AND p_id<'.$last_id,$brand_checked,$order_col,$order_type);
		}

		if($cat_id!=0 && $sub_cat_id!=0)
		{
			$product_list=$this->product_model->get_item_ajax_data('status=1 AND user_id='.$area_retreive_id.' AND cat_id='.$cat_id.' AND sub_cat_id='.$sub_cat_id.' AND p_id<'.$last_id,$brand_checked,$order_col,$order_type);
		}


		if(count($product_list)>0){ 
      	foreach ($product_list as $key => $value) {
      	if($value['d_price']>=$low_amt && $value['d_price']<=$high_amt){
      	echo '<li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">';
        echo '<div class="col-item">';
          echo '<div class="product-image-area"> <a class="product-image" title="Sample Product" href="product/product_details/'.$value['profile_name'].'">'; 
            if($value['main_image']!='')
            {
              echo '<img style="height: 285px;width: 100%" src="uploads/product/'.$value['main_image'].'" class="img-responsive" alt="'.$value['p_name'].'" />'; 
            } 
            else
            { 
              echo '<img style="height: 285px;width: 100%" src="uploads/product/default.png" class="img-responsive" alt="'.$value['p_name'].'" />'; 
            }
          echo '</a>';
            echo '<div class="hover_fly">'; 
              echo '<a onclick="add_to_cart('.$value['p_id'].')" class="exclusive ajax_add_to_cart_button" href="JavaScript:0;" title="Add to cart">
                <div><i class="icon-shopping-cart"></i><span>Add to cart</span></div>
              </a> ';

              echo '<a class="quick-view" href="javascript:0;" onclick="quick_view('.$value['p_id'].')">
                <div><i class="icon-eye-open"></i><span>Quick view</span></div>
              </a> 
            </div>
          </div>
          <div class="info">
            <div class="info-inner">';
              echo '<div class="item-title"> <a title="'.$value['p_name'].'" href="product/product_details/'.$value['profile_name'].'"> '.$value['p_name'].'</a> </div>';
              
              echo '<div class="item-content">';
                
                echo '<div class="price-box">';
                if($value['discount']==0)
                {
                  echo '<p class="special-price"> <span class="price">'.number_format($value['price'],2,'.','').'&#x9f3;</span> </p>';
                } 
                else 
                { 
                  echo '<p class="special-price"> <span class="price">';
                  $price=$value['price']-(($value['price']*$value['discount'])/100);

                	echo number_format($price,2,'.','').'&#x9f3;</span> </p>';
                 	echo '<p class="old-price"> <span class="price-sep">-</span> <span class="price"> '.number_format($value['price'],2,'.','').'&#x9f3; </span> </p>';
                }
                echo '</div>';
              echo '</div>'; 
            echo '</div>';
            echo '<div class="clearfix"> </div>';
          echo '</div>';
        echo '</div>';
        echo '<div id="loading_gif_'.$value['p_id'].'" style="display:none;position: absolute;width: 100px;left: 34%;top: 22%;">';
          		echo '<img style="width: 100%" src="front_assets/images/loading.gif" alt="">
        </div>
      </li>';
       echo '<input type="hidden" class="last_id" value="'.$value['p_id'].'">';

      }} }else
      {
      	echo '<li id="no_item_found"><h3><i class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></i> No product available</h3></li>';
       }
		
	}


}
