<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends CI_Model
{
   
    
    public function product_details($condition)
    {
        $this->db->select('*,product.cat_id as p_cat_id,product.sub_cat_id as p_sub_cat_id');
        $this->db->from('product');
        $this->db->join('category','category.id=product.cat_id','LEFT');
        $this->db->join('sub_category','sub_category.id=product.sub_cat_id','LEFT');
        $this->db->join('brand','brand.id=product.brand_id','LEFT');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_product_main_image($id)
    {
        $this->db->select('main_image');
        $this->db->from('product');
        $this->db->where('p_id',$id);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function get_product_details($id)
    {
        $this->db->select('p_id,main_image,p_name,profile_name,price,discount');
        $this->db->from('product');
        $this->db->where('p_id',$id);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function get_item_ajax_data($condition,$order_col,$order_type)
    {
        $this->db->select('*,(price-(price*discount)/100) as d_price');
        $this->db->from('product');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by($order_col,$order_type);
        $this->db->limit(12);
        $result=$this->db->get();
        return $result->result_array();
    }
   
}
?>