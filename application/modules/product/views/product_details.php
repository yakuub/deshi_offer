<?php $this->load->view('front/headlink');?>
  <body>
        <!--Header Start-->
        <?php $this->load->view('front/head_nav');?>
        <!--End of Header Area-->
                <!--Breadcrumb Start-->
        <div class="breadcrumb-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumbs">
                            <ul>
                                <li class="home"><a href="">Home</a><span>/ </span></li>
                                <li><a href="product">Product</a><span>/ </span></li>
                                <li><strong><?=$product_details[0]['profile_name'];?></strong></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End of Breadcrumb-->
        <!--Product Details Area Start-->
        <div class="product-deails-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9">
                        <div class="product-details-content row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="zoomWrapper">
                                    <div id="img-1" class="zoomWrapper single-zoom">
                                        <a href="#">
                                            <img id="zoom1" src="uploads/product/<?=$product_details[0]['main_image'];?>" data-zoom-image="uploads/product/<?=$product_details[0]['main_image'];?>" alt="<?=$product_details[0]['p_name'];?>">
                                        </a>
                                    </div>
                                    <div class="product-thumb row">
                                        <ul class="p-details-slider" id="gallery_01">
                                            <li class="col-md-4">
                                                <a class="elevatezoom-gallery" href="#" data-image="uploads/product/<?=$product_details[0]['main_image'];?>" data-zoom-image="uploads/product/<?=$product_details[0]['main_image'];?>"><img src="uploads/product/<?=$product_details[0]['main_image'];?>" alt=""></a>
                                            </li>
                                            <?php $g_image=explode(',',$product_details[0]['gallery_image']);?>
                                            <?php foreach ($g_image as $key => $img) {?>
                                            <li class="col-md-4">
                                                <a class="elevatezoom-gallery" href="#" data-image="uploads/product/<?=$img;?>" data-zoom-image="uploads/product/<?=$img;?>"><img src="uploads/product/<?=$img;?>" alt=""></a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="product-details-conetnt">
                                    <div class="shipping">
                                        <div class="single-service">
                                            <span class="fa fa-truck"></span>
                                            <div class="service-text-container">
                                                <h3>fREE SHIPPING</h3>
                                                <p>One order over $99</p>
                                            </div>   
                                        </div>
                                        <div class="single-service">
                                            <span class="fa fa-cube"></span>
                                            <div class="service-text-container">
                                                <h3>UNLIMITED UNLIMITED</h3>
                                                <p>Lorem ipsum dolor sit amet</p>
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="product-name">
                                        <h1><?=$product_details[0]['p_name'];?></h1>
                                    </div>
                                    <!-- <p class="no-rating no-margin"><a href="#">Be the first to review this product</a></p> -->
                                    <p class="availability">Availability: <span>In stock</span></p>
                                    <div class="price-box">
                                        <?php if($product_details[0]['discount']>0){ ?>
                                        <p class="old-price">
                                            <span class="price"><?=number_format($product_details[0]['price'],2);?>&#x9f3;</span>
                                        </p>
                                        
                                        <p class="special-price">
                                            <span class="price"><?=number_format($product_details[0]['price']-(($product_details[0]['price']*$product_details[0]['discount'])/100),2);?>&#x9f3;</span>
                                        </p>  
                                        <?php } else { ?>    
                                        <p class="special-price">
                                            <span class="price"><?=number_format($product_details[0]['price'],2);?>&#x9f3;</span>
                                        </p>
                                        <?php } ?>                       
                                    </div>
                                    <div class="details-description">
                                        <?=htmlentities(strip_tags(word_limiter($product_details[0]['description'],50)));?>
                                      
                                    </div>
                                    <input type="hidden" id="size_id" value="0"> 
                                    <input type="hidden" id="color_id" value="0"> 
                                    <div class="popular-tags">

                                        <ul class="tag-list"> 
                                            <span style="font-size: 14px">Size:</span> 
                                            <?php $p_size=explode(',', $product_details[0]['available_size']);?>
                                            <?php foreach ($size_list as $key => $value) {?>  
                                            <?php foreach ($p_size as $s_key => $size) {?>
                                            <?php if($size==$value['id']){ ?>
                                                <li><a title="<?=$value['size_title'];?>" href="javascript:0" onclick="add_size('<?=$value['id'];?>')"><?=$value['size_code'];?></a></li>
                                            <?php } } } ?>
                                        </ul>
                                    </div>

                                    <div class="popular-tags">
                                        <ul class="tag-list"> 
                                            <span style="font-size: 14px">Color:</span> 
                                            <?php $p_color=explode(',', $product_details[0]['available_color']);?>
                                            <?php foreach ($color_list as $key => $value) {?>  
                                            <?php foreach ($p_color as $s_key => $color) {?>
                                            <?php if($color==$value['id']){ ?>
                                                <li><a title="<?=$value['color_title'];?>" href="javascript:0" onclick="add_color('<?=$value['id'];?>')"><i class="fa fa-square" style="color:<?=$value['color_code'];?>"></i></a></li>
                                            <?php } } } ?>
                                        </ul>
                                    </div>


                                    <div class="add-to-cart-qty">
                                        <!-- <div class="timer">
                                            <div data-countdown="2022/01/01" class="timer-grid"></div>
                                        </div> -->
                                        <div class="cart-qty-button">
                                            <label for="qty">Qty:</label>
                                            <input type="text" class="input-text qty" value="1" maxlength="12" id="cart_qty" name="qty">
                                            <button  onclick="add_to_cart('<?=$product_details[0]['p_id'];?>')" class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="p-details-tab">
                                    <ul role="tablist" class="nav nav-tabs">
                                        <li class="active" role="presentation"><a data-toggle="tab" role="tab" aria-controls="description" href="#description">Product Description</a></li>
                                        <!-- <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="reviews" href="#reviews">Reviews</a></li>
                                        <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="tags" href="#tags">Product Tags</a></li> -->
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                                <div class="tab-content review product-details">
                                    <div id="description" class="tab-pane active" role="tabpanel">
                                        <?=htmlentities(strip_tags($product_details[0]['description']));?>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="product-carousel-area section-top-padding">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title"><h2>Recent Products</h2></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="upsell-product-details-carousel">
                                    <?php foreach ($latest_list as $key => $value) {?>
                             
                                <div class="col-md-3">
                                <!--item 1 start-->
                                <div class="single-product-item">
                                    <div class="new-product-label"><span>new</span></div>
                                    <div class="single-product clearfix">
                                        <?php $g_image=explode(',',$value['gallery_image']);?>
                                        <a href="product/product_details/<?=$value['profile_name'];?>">
                                            <span class="product-image">
                                                <img src="uploads/product/<?=$value['main_image'];?>" alt="">
                                            </span>
                                            <span class="product-image hover-image">
                                                <img src="uploads/product/<?=$g_image[0];?>" alt="">
                                            </span>
                                        </a>
                                        <div class="button-actions clearfix"> 
                                            <button class="button add-to-cart" type="button">
                                                <span><i class="fa fa-shopping-cart"></i></span>
                                            </button>
                                            <ul class="add-to-links">
                                                <li class="quickview">
                                                    <a class="btn-quickview modal-view" href="javascript:0;" onclick="quickview_product_ajax('<?=$value['p_id'];?>')">
                                                        <i class="fa fa-search-plus"></i>
                                                    </a>
                                                </li>
                                                <!-- <li>
                                                    <a class="link-wishlist" href="wishlist.html">
                                                        <i class="fa fa-heart"></i>
                                                    </a>
                                                </li>                                                         <li>
                                                    <a class="link-compare" href="#">
                                                        <i class="fa fa-retweet"></i>
                                                    </a>
                                                </li>     -->                   
                                            </ul>
                                        </div>
                                    </div>
                                    <h2 class="single-product-name"><a href="product/product_details/<?=$value['profile_name'];?>"><?=$value['p_name'];?></a></h2>
                                    <div class="price-box">
                                        <?php if($value['discount']>0){ ?>
                                        <p class="old-price">
                                            <span class="price"><?=number_format($value['price'],2);?>&#x9f3;</span>
                                        </p>
                                        <?php } ?>
                                        <p class="special-price">
                                            <span class="price"><?=number_format($value['price']-(($value['price']*$value['discount'])/100),2);?>&#x9f3;</span>
                                        </p>                      
                                    </div>
                                </div><!--end of item 1-->
                            </div><!--end of col-md-3-->
                            <?php } ?>
                                </div>
                            </div>
                        </div>    
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="single-products-category">
                            <div class="section-title"><h2>Related PRODUCTS</h2></div>
                            <div class="category-products">
                                <?php foreach ($related_product as $key => $value) {?>
                                <div class="product-items">
                                    <div class="p-category-image">
                                        <a href="#">
                                            <img alt="" style="height: 130px" src="uploads/product/<?=$value['main_image'];?>">
                                        </a>
                                    </div>
                                    <div class="p-category-text">
                                        <h2 class="category-product-name"><a href="#"><?=$value['p_name'];?></a></h2>
                                        <div class="price-box">
                                            <?php if($value['discount']>0){ ?>
                                            <p class="old-price">
                                                <span class="price"><?=number_format($value['price'],2);?>&#x9f3;</span>
                                            </p>
                                            
                                            <p class="special-price">
                                                <span class="price"><?=number_format($value['price']-(($value['price']*$value['discount'])/100),2);?>&#x9f3;</span>
                                            </p>  
                                            <?php } else { ?>    
                                            <p class="special-price">
                                                <span class="price"><?=number_format($value['price'],2);?>&#x9f3;</span>
                                            </p>
                                            <?php } ?>                  
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <!-- <div class="sidebar-content">
                            <div class="section-title no-margin"><h2>Popular Tags</h2></div>
                            <div class="popular-tags">
                                <ul class="tag-list">                  
                                    <li><a href="#">Curae</a></li>
                                    <li><a href="#">Vestibulum</a></li>
                                    <li><a href="#">ante</a></li>
                                    <li><a href="#">cubilia</a></li>
                                    <li><a href="#">et</a></li>
                                    <li><a href="#">faucibus</a></li>
                                    <li><a href="#">in</a></li>
                                    <li><a href="#">ipsum</a></li>
                                    <li><a href="#">luctus</a></li>
                                    <li><a href="#">orci</a></li>
                                    <li><a href="#">posuere</a></li>
                                    <li><a href="#">primis</a></li>
                                    <li><a href="#">ultrices</a></li>
                                </ul>
                                <div class="tag-actions">
                                    <a href="#">View All Tags</a>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar-content">
                            <div class="section-title no-margin"><h2>Compare Products </h2></div>
                            <div class="block-content">
                                <p class="empty">You have no items to compare.</p>
                            </div>
                        </div>
                        <div class="sidebar-content">
                            <div class="banner-box">
                                <a href="#"><img alt="" src="front_assets/img/banner/14.jpg"></a>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!--End of Product Details Area-->

        <?php $this->load->view('front/footer');?>
        <?php $this->load->view('front/footerlink');?>
      
    </body>
</html>