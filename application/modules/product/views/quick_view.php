<div class="product-view">
            <div class="product-essential">
             
                <div class="product-img-box col-lg-6 col-sm-6 col-xs-12">
                  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                      <div class="carousel-inner" style="margin-top: 0px;">   
                        <div class="item active">
                          <img src="uploads/product/<?=$product_details[0]['main_image'];?>" alt="<?=$product_details[0]['p_name'];?>" style="width:100%;height: 426px;">
                        </div>
                      </div>
                    </div>
                </div>
                <div class="product-shop col-lg-6 col-sm-6 col-xs-12">
             
                  <div class="product-name">
                    <h1><?=$product_details[0]['p_name'];?></h1>
                  </div>
                  <div class="ratings">             
                    <p class="rating-links"> <a href="#" class="widget-title"><?=$product_details[0]['name'];?></a> <span class="separator">|</span> <a href="#"><?=$product_details[0]['sub_name'];?></a> </p>
                  </div>
                  <p class="availability in-stock">Availability: <span>In stock</span></p>
                  <div class="price-block">
                    <div class="price-box">
                      <?php if($product_details[0]['discount']>0){?>
                      <p class="old-price"> <span class="price-label">Regular Price:</span> <span class="price"><?=number_format($product_details[0]['price'],2,'.','');?>&#x9f3;</span> </p>

                      <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php $price=$product_details[0]['price']-(($product_details[0]['price']*$product_details[0]['discount'])/100);?>
                        <?=number_format($price,2,'.','');?>&#x9f3;
                       </span>
                         </p>
                      <?php } else{ ?>
                      <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?=number_format($product_details[0]['price'],2,'.','');?>&#x9f3;</span> </p>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="short-description">
                    <h2>Quick Overview</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla.</p>
                  </div>
                  <div class="add-to-box">
                    <div class="add-to-cart">
                      <label for="qty">Quantity:</label>
                      <div class="pull-left">
                        <div class="custom pull-left">
                          
                          <input type="number" class="input-text qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                         
                        </div>
                      </div>
                      <button onclick="add_to_cart_specific('<?=$product_details[0]['p_id'];?>')" class="button btn-cart" title="Add to Cart" type="button"><span><i class="icon-basket"></i> Add to Cart</span></button>
                      
                    </div>
                  </div>

                </div>
             
            </div>
          </div>
