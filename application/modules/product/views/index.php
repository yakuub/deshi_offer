<?php $this->load->view('front/headlink');?>
<link rel="stylesheet" href="front_assets/css/checkbox.css">
  <body>
        <!--Header Start-->
        <?php $this->load->view('front/head_nav');?>
        <!--End of Header Area-->
                <!--Breadcrumb Start-->
        <div class="breadcrumb-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="breadcrumbs">
                            <ul>
                                <li class="home"><a href="">Home</a><span>/ </span></li>
                                <li><strong>Shop Grid</strong></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End of Breadcrumb-->
        <!--Banner Imgae Area Start-->
        <!-- <div class="banner-image-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="category-image"><img alt="women" src="front_assets/img/banner/13.jpg"></div>
                    </div>
                </div>
            </div>
        </div> -->
        <!--End of Banner Imgae Area-->
        <!--Shop Main Area Start-->
        <div class="shop-main-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="sidebar-content">
                            <div class="section-title"><h2>Category</h2></div>
                            <div class="sidebar-category-list">
                                <ul>
                                    <?php foreach ($category as $key => $value) {?>
                                    <li <?php if($value['id']==$cat_active){echo "class='active'";} ?>><a href="product/filter/<?=$value['name'];?>"><?=$value['name'];?></a></li>
                                   <?php } ?>
                                </ul>
                            </div>
                            <input type="hidden" id="cat_id" value="<?=$cat_active;?>">
                            <input type="hidden" id="sub_cat_id" value="<?=$sub_cat_active;?>">
                            <div class="section-title border-none"><h2>Price</h2></div>
                            <div class="sidebar-category-list">
                                <form action="<?=base_url(uri_string());?>" method="get">
                                <div class="price_filter">
                                    <div id="slider-range"></div>
                                    <div class="price_slider_amount">
                                       <div class="slider-values">
                                            <input type="text" id="amount" name="price"  placeholder="Add Your Price" /> 
                                        </div>
                                    </div>
                                    <button id="search_price" type="submit" class="button"><span><span>Search</span></span></button>
                                </div>
                                </form>
                            </div>
                            
                        </div>

                        <div class="sidebar-content">
                            <div class="section-title no-margin"><h2>Color</h2></div>
                 
                            <div class="form-group row">
                                <?php foreach ($color_list as $key => $value) {?>
                                <div class="col-sm-2">
                                    <input onchange="product_filter_ajax()" value="<?=$value['id'];?>" type="checkbox" class="color_list" name="color_list[]" id="checkbox_id_<?=$key;?>"  />
                                    <div class="btn-group">
                                        <label for="checkbox_id_<?=$key;?>" class="checkbox_btn" style="background: <?=$value['color_code'];?>">
                                            <span class="fa fa-check-square-o fa-lg"></span>
                                            <span class="fa fa-square-o fa-lg"></span>
                                        </label>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                           
                        </div>

                        <div class="sidebar-content">
                            <div class="section-title no-margin"><h2>Size</h2></div>
                            <div class="popular-tags">
                                <ul class="tag-list"> 
                                    <?php foreach ($size_list as $key => $value) {?>
                                    <li><a href="javascript:0" title="<?=$value['size_title'];?>"><?=$value['size_code'];?></a></li>
                                    <?php } ?>
                                </ul>
                                
                            </div>

                            
                        </div>
                        
                        <div class="sidebar-content">
                            <div class="section-title no-margin"><h2>Brand</h2></div>
                            <div class="popular-tags">
                                <ul class="tag-list"> 
                                    <?php foreach ($brand as $key => $value) {?>
                                    <li><a href="#"><?=$value['brand_name'];?></a></li>
                                    <?php } ?>
                                </ul>
                                
                            </div>
                        </div>
                       
                    </div>
                    <div class="col-md-9">
                        <div class="shop-item-filter">
                            <div class="shop-tab clearfix">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs navbar-left" role="tablist">
                                    <li role="presentation" class="active"><a href="#grid" class="grid-view" aria-controls="grid" role="tab" data-toggle="tab">Grid</a></li>
                                    <li role="presentation"><a href="#list" class="list-view" aria-controls="list" role="tab" data-toggle="tab">List</a></li>
                                </ul>
                                <div class="filter-by">
                                    <h4>Short by </h4>
                                    <form action="#">
                                        <div class="select-filter">
                                            <select>
                                              <option value="color">Position</option>
                                              <option value="name">Name</option>
                                              <option value="brand">Brand</option>
                                            </select> 
                                        </div>
                                    </form>               
                                </div>
                                <p class="page floatright">Per Page</p>
                                <div class="filter-by floatright">
                                    <h4>Show </h4>
                                    <form action="#">
                                        <div class="select-filter">
                                            <select>
                                              <option value="10">12</option>
                                              <option value="20">16</option>
                                              <option value="30">20</option>
                                            </select> 
                                        </div>
                                    </form> 
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div> 
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="grid">
                                <div class="row">
                                    <?php foreach ($product_list as $key => $value) {?>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="single-product-item">
                                    <div class="sale-product-label"><span>feature</span></div>
                                    <div class="single-product clearfix">
                                        <?php $g_image=explode(',',$value['gallery_image']);?>
                                        <a href="product/product_details/<?=$value['profile_name'];?>">
                                            <span class="product-image">
                                                <img src="uploads/product/<?=$value['main_image'];?>" alt="">
                                            </span>
                                            <span class="product-image hover-image">
                                                <img src="uploads/product/<?=$g_image[0];?>" alt="">
                                            </span>
                                        </a>
                                        <div class="button-actions clearfix"> 
                                            <button class="button add-to-cart" type="button">
                                                <span><i class="fa fa-shopping-cart"></i></span>
                                            </button>
                                            <ul class="add-to-links">
                                                <li class="quickview">
                                                    <a class="btn-quickview modal-view" href="javascript:0;" onclick="quickview_product_ajax('<?=$value['p_id'];?>')">
                                                        <i class="fa fa-search-plus"></i>
                                                    </a>
                                                </li>
                                                <!-- <li>
                                                    <a class="link-wishlist" href="wishlist.html">
                                                        <i class="fa fa-heart"></i>
                                                    </a>
                                                </li>                                                         <li>
                                                    <a class="link-compare" href="#">
                                                        <i class="fa fa-retweet"></i>
                                                    </a>
                                                </li>     -->                   
                                            </ul>
                                        </div>
                                    </div>
                                    <h2 class="single-product-name"><a href="product/product_details/<?=$value['profile_name'];?>"><?=$value['p_name'];?></a></h2>
                                    <div class="price-box">
                                        <?php if($value['discount']>0){ ?>
                                        <p class="old-price">
                                            <span class="price"><?=number_format($value['price'],2);?>&#x9f3;</span>
                                        </p>
                                        
                                        <p class="special-price">
                                            <span class="price"><?=number_format($value['price']-(($value['price']*$value['discount'])/100),2);?>&#x9f3;</span>
                                        </p>  
                                        <?php } else { ?>    
                                        <p class="special-price">
                                            <span class="price"><?=number_format($value['price'],2);?>&#x9f3;</span>
                                        </p>
                                        <?php } ?>                
                                    </div>
                                </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                               
                        </div>   
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pagination-content">
                                    <div class="pagination-button">
                                        <ul class="pagination">
                                            <li class="current"><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#"><i class="fa fa-caret-right"></i></a></li>
                                        </ul>
                                        <span><strong>Page: </strong></span>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
        <!--End of Shop Main Area-->

        <?php $this->load->view('front/footer');?>
        <?php $this->load->view('front/footerlink');?>


        <script>
            /*----------------------------
            Price-slider active
        ------------------------------ */  
            $( "#slider-range" ).slider({
                range: true,
                min: 0,
                max: 800000,
                values: [ <?=$low_amt;?>, <?=$high_amt;?> ],
                slide: function( event, ui ) {
                $( "#amount" ).val( "৳" + ui.values[ 0 ] + "-৳" + ui.values[ 1 ] );
                }
            });
            $( "#amount" ).val( "৳" + $( "#slider-range" ).slider( "values", 0 ) +
               "-৳" + $( "#slider-range" ).slider( "values", 1 ) ); 
        </script>


        <script>
            function product_filter_ajax() 
            {
                $("#loading_img").show();
                var cat_id=$("#cat_id").val();
                var sub_cat_id=$("#sub_cat_id").val();
                var price_range=$("#amount").val();

                //var sort_by=$("#my-select-height-2").val();

                var color_list = [];

                $('input[class="color_list"]:checked').each(function() {
                   color_list.push(this.value); 
                   
                });


                //console.log(range+' sort:'+sort_by);
               
                // $.ajax({
                //     url: "<?php echo site_url('search/doctor_filter_ajax');?>",
                //     type: "post",
                //     data: {sp_id:sp_id,type_id:type_id,area_id:area_id,sort_by:sort_by,range:range,day_list:day_list},
                //     success: function(msg)
                //     {
                //       $("#ajax_div").html(msg);
                //       $("#loading_img").hide();
                //       //console.log(msg);
                //     }      
                // }); 
            }
        </script>
      
    </body>
</html>