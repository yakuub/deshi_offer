<div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="modal-product">
                    <div class="product-images">
                        <div class="main-image images">
                            <img alt="" src="uploads/product/<?=$product_details[0]['main_image'];?>">
                        </div>
                    </div>
                    <div class="product-info">
                        <h1><?=$product_details[0]['p_name'];?></h1>
                        <div class="price-box">
                            <?php if($product_details[0]['discount']>0){ ?>
                            <p class="old-price">
                                <span class="price"><?=number_format($product_details[0]['price'],2);?>&#x9f3;</span>
                            </p>
                            
                            <p class="special-price">
                                <span class="price"><?=number_format($product_details[0]['price']-(($product_details[0]['price']*$product_details[0]['discount'])/100),2);?>&#x9f3;</span>
                            </p>  
                            <?php } else { ?>    
                            <p class="special-price">
                                <span class="price"><?=number_format($product_details[0]['price'],2);?>&#x9f3;</span>
                            </p>
                            <?php } ?>     
                        </div>
                        <a target="_denim" href="product/product_details/<?=$product_details[0]['profile_name'];?>" class="see-all">See all features</a>
                        
                        <input type="hidden" id="size_id" value="0"> 
                        <input type="hidden" id="color_id" value="0"> 
                        <div class="popular-tags">

                            <ul class="tag-list"> 
                                <span style="font-size: 14px">Size:</span> 
                                <?php $p_size=explode(',', $product_details[0]['available_size']);?>
                                <?php foreach ($size_list as $key => $value) {?>  
                                <?php foreach ($p_size as $s_key => $size) {?>
                                <?php if($size==$value['id']){ ?>
                                    <li><a title="<?=$value['size_title'];?>" href="javascript:0" onclick="add_size('<?=$value['id'];?>')"><?=$value['size_code'];?></a></li>
                                <?php } } } ?>
                            </ul>
                        </div>

                        <div class="popular-tags">
                            <ul class="tag-list"> 
                                <span style="font-size: 14px">Color:</span> 
                                <?php $p_color=explode(',', $product_details[0]['available_color']);?>
                                <?php foreach ($color_list as $key => $value) {?>  
                                <?php foreach ($p_color as $s_key => $color) {?>
                                <?php if($color==$value['id']){ ?>
                                    <li><a title="<?=$value['color_title'];?>" href="javascript:0" onclick="add_color('<?=$value['id'];?>')"><i class="fa fa-square" style="color:<?=$value['color_code'];?>"></i></a></li>
                                <?php } } } ?>
                            </ul>
                        </div>
                        <div class="quick-add-to-cart">
                            
                            <div class="numbers-row">
                                <input type="number" id="cart_qty" value="1">
                            </div>
                            <button onclick="add_to_cart('<?=$product_details[0]['p_id'];?>')" class="single_add_to_cart_button"> Add to cart</button>
                            
                        </div>

                        <div class="quick-desc">
                            <?=htmlentities(strip_tags(word_limiter($product_details[0]['description'],50)));?>
                        </div>
                        <div class="social-sharing">
                            <!-- <div class="widget widget_socialsharing_widget">
                                <h3 class="widget-title-modal">Share this product</h3>
                                <ul class="social-icons">
                                    <li><a target="_blank" title="Facebook" href="#" class="facebook social-icon"><i class="fa fa-facebook"></i></a></li>
                                    <li><a target="_blank" title="Twitter" href="#" class="twitter social-icon"><i class="fa fa-twitter"></i></a></li>
                                    <li><a target="_blank" title="Pinterest" href="#" class="pinterest social-icon"><i class="fa fa-pinterest"></i></a></li>
                                    <li><a target="_blank" title="Google +" href="#" class="gplus social-icon"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a target="_blank" title="LinkedIn" href="#" class="linkedin social-icon"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div> -->
                        </div>
                    </div><!-- .product-info -->
                </div>
            </div>
        </div>
</div>
    