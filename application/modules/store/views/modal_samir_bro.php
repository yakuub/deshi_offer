               <!-- Modal -->
               <div class="modal fade" id="merch_dt_modal" role="dialog">
                  <div class="modal-dialog modal-md merch_modal">
                     <div class="modal-content merch_dt_cnt">
                        <div class="modal-header" style="padding: 0; border: none;">
                           <button type="button" class="close mech_cl_btn" data-dismiss="modal">&times;</button>
                      
                        </div>
                        <div class="modal-body merch_dt_body">
                           <div class="col-md-12 merch_dt_box p_0">
                              <div class="col-xs-12 profile_img_box" >
                                 <img src="front_assets/images/bagdoom_pic.jpg" alt="" class="img-responsive center-block" style="width:200px;height:100px;">
                                 <span class="text-center" style="font-family:bold;font-size:20px;">
                                   <p >Bagdoom Online Store</p>
                                  </span>
                              </div>
                              <!--         <div class="clearfix"></div> -->
                              <div class="col-md-offset-4 col-md-5 p_0">
                                 <div class=" col-md-6 rating-top-modal rating_star border_ryt" style="">
                               <input readonly="" id="input-id" value="<?=$rating_info_individual_user[0]['rating']?>" type="text" class="rating mr-offr-rating" data-size="xs" >
                                 </div>
                                 <div class="col-md-6">
                                   8299 Votes
                                 </div>
                              </div>
                              <!-- <div class="col-md-5 mt-12 t-vote" style="padding-left: 10px;">
                                 <span style="font-size: 18px; line-height: 0">211 Votes</span>
                              </div> --> 
                              <div class="col-md-12">
                                 <p class="text-center txt_ver"><span style="font-weight: 600; color: #0ed00e">Verified Store</span> </p>
                              </div>
                              <div class="col-md-12 nav_dt_links">
                                 <ul class="nav nav-tabs merc_nav_tabs col-md-7">
                                    <li class="active"><a data-toggle="tab" href="#About">About</a></li>
                                    <li><a data-toggle="tab" href="#contact">Contact</a></li>
                                 </ul>
                                 <div class="col-md-5 merc_right_btn">
                                    <span class="btn btn-default" style="border-radius: 0;    width: 50%;
                                    padding: 5px;">Follow</span>&nbsp;
                                       <span>211 Followers</span>
                                    </div>
                                 </div>
                              </div>
                              <div class="tab-content merc_tab_content">
                                 <div id="About" class="tab-pane fade in active">
                                    <h3>About loerm bagdoom </h3>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero quo, numquam delectus id quasi quis omnis necessitatibus praesentium libero quod!</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero quo, numquam delectus id quasi quis omnis necessitatibus praesentium libero quod!</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero quo, numquam delectus id quasi quis omnis necessitatibus praesentium libero quod!</p><br>

                                      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vero quo, numquam delectus id quasi quis omnis necessitatibus praesentium libero quod!</p>
                                 </div>
                                 <div id="contact" class="tab-pane fade">
                                    <h5 class="text-center"><strong>LOCATION</strong></h5>
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4341.922886055763!2d90.4120701455606!3d23.7791899050831!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x19bd7392015966ba!2sBagdoom.com!5e0!3m2!1sen!2sbd!4v1519810466097" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    <div class="col-md-12 merch_dt_social">
                                       <h5 class="text-center">Connect with us here</h5>
                                       <div class="merc_contants row">
                                          <div class="col-md-6 col-xs-6">
                                             <div class="row merc_contants_elem">
                                                <div class="col-md-1"><i class="fa fa-phone gren_color"></i></div>
                                                <div class="col-md-3">Hotline</div>
                                                <div class="col-md-7"><?=$store_info[0]['contact'];?></div>
                                             </div>
                                          <div class="row merc_contants_elem">
                                             <div class="col-md-1"><i class="fa fa-phone gren_color"></i></div>
                                             <div class="col-md-3">Contact </div>
                                             <div class="col-md-7">+88 096 06 11 77</div>
                                          </div>
                                       </div>
                                       <div class="col-md-6 col-xs-6">
                                          <div class="row merc_contants_elem">
                                             <?php if($store_info[0]['email']!=''){?>
                                                <div class="col-md-1"><i class="fa fa-envelope google_red"></i></div>
                                                <div class="col-md-3">Email</div>
                                                <div class="col-md-7"><?=$store_info[0]['email'];?></div>
                                             <?php } ?>
                                          </div>

                                           <div class="row merc_contants_elem">
                                             <?php if($store_info[0]['email']!=''){?>
                                                <div class="col-md-1"><i class="fa fa-globe" style="color:blue;"></i></div>
                                                <div class="col-md-3">Web</div>
                                                <div class="col-md-7"><?=$store_info[0]['email'];?></div>
                                             <?php } ?>
                                          </div>
                                          <!-- <div class="row merc_contants_elem">
                                             <?php if($store_info[0]['support_email']!=''){?>
                                                <div class="col-md-1"><i class="fa fa-link"></i></div>
                                                <div class="col-md-3">Web</div>
                                                <div class="col-md-7"><?=$store_info[0]['support_email'];?></div>
                                             <?php } ?>
                                          </div> -->
                                       </div>
                                    </div>
                                          <!-- new line start -->
                                          <div class="merc_contants col-md-12">
                                       <div class="col-md-6 col-xs-6">
                                       <div class="row merc_contants_elem">
                                           
                                           <div class="col-md-1"><i class="fa fa-link viber_purple"></i></div>
                                           <div class="col-md-3">Viber</div>
                                           <div class="col-md-7">+675979868</div>
                                      
                                     </div>
                                          <div class="row merc_contants_elem">
                                           
                                           <div class="col-md-1"><i class="fa fa-google-plus  google_red"></i></div>
                                           <div class="col-md-3">Goole</div>
                                           <div class="col-md-7">@info.bagdoom.com</div>
                                      
                                     </div>
                                       </div>
                                       <div class="col-md-6 col-xs-6">
                                       <div class="row merc_contants_elem">
                                           
                                           <div class="col-md-2"><i class="fa fa-facebook fb_blue"></i></div>
                                           <div class="col-md-3">Facebook</div>
                                           <div class="col-md-7">@info.bagdoom.com</div>
                                      
                                     </div>
                                          <div class="row merc_contants_elem">
                                           
                                                <div class="col-md-1"><i class="fa fa-youtube-play google_red"></i></div>
                                                <div class="col-md-3">Youtube</div>
                                                <div class="col-md-7">@info.bagdoom.com</div>
                                           
                                          </div>
                                       </div>
                                    </div>
                                          <!-- new line end -->


                                    <div class="merc_location">
                                       <div class="col-md-6">
                                          <h5><strong> Corporate Office :</strong></h5>
                                          <p>
                                                Service Center 283/8928 6th Floor Road #22 Ghulshan 1-Dhaka 1212...
                                          </p>
                                         
                                       </div>
                                       <div class="col-md-6">
                                          <h5><strong>Dhaka Office:</strong></h5>
                                          <p>
                                          Service Center 283/8928 6th Floor Road #22 Ghulshan 1-Dhaka 1212...
                                          </p>
                                       </div>
                                       <div class="col-md-6">
                                          <h5><strong>Chittagong Office:</strong></h5>
                                          <p>
                                          Service Center 283/8928 6th Floor Road #22 Ghulshan 1-Dhaka 1212...
                                          </p>
                                       </div>
                                    </div>
                                    <br>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer merch_dt_footer">
                        </div>
                     </div>
                  </div>
               </div>