<?php $this->load->view('front/headlink');?>
<body>
<?php $this->load->view('front/head_nav');?> 
<?php $this->load->view('front/mega_menu');?> 

<div class="container"><!--All store container start-->
    <div class="col-md-12 all_catagory"><!--All store start-->
      <div class="col-md-12 all_catagory_title"><!--title start-->
        <h1>Popular Stores & Brands</h1>
        <p>Browse popular brands and get the best Deals  &  Offers from your nearest stores.</p>
      </div> <!--title start-->


      <?php if(count($is_feature)>0){?>
      <div class="col-md-12 popular_cat">
        <div class="col-xs-12 heading">
        <div class="heading_icon">
          <img src="front_assets/images/resource/home/cart2.svg" alt="" class="img-responsive" id="cat_img"> 
        </div>
        <p class="fea_brnd">Featured Brands</p> 
      </div>
      <div class="clearfix"></div><br>
        <div class="row store_sec cat_pop_head">
          <?php foreach ($is_feature as $key => $value) {?>
            <div class="col-md-2 col-sm-3 col-xs-6 ">
              <a class="marchant_img col-md-12" href="store/offer/<?=$value['name'];?>">
                <img style="width: 100%; height: 100%" src="uploads/user/<?=$value['profile_pic'];?>" class="img-responsive" alt="">
                <div class="store_overlay"><?=$top_store_brand_offer[$key];?> Offers</div>
              </a>
            </div>
            <?php } ?>
          <div class="clearfix"></div>
        </div>
      </div>
      <?php } ?>
            <div class="col-md-12 f_w_ad"><!--full width banner-->
        <div class="col-sm-12">
          <a target="_blank" href="#"><img src="front_assets/images/ad_banner.png" alt="" class="img-responsive"></a>
        </div>
      </div><!--full width banner-->
      <div class="col-xs-12 heading">
        <div class="heading_icon">
          <img src="front_assets/images/resource/home/fstrs.svg" alt="" class="img-responsive" id="cat_img">
        </div>
        <p class="fea_brnd">All Brands & Stores</p>
      </div>
      </div>
       

        <div class="cat_link">
            <a style="cursor: pointer;" onclick="get_store_ajax('all')">#</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('a')">a</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('b')">b</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('c')">c</a> 
            <a style="cursor: pointer;" onclick="get_store_ajax('d')">d</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('e')">e</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('f')">f</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('g')">g</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('h')">h</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('i')">i</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('j')">j</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('k')">k</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('l')">l</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('m')">m</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('n')">n</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('o')">o</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('p')">p</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('q')">q</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('r')">r</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('s')">s</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('t')">t</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('u')">u</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('v')">v</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('w')">w</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('x')">x</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('y')">y</a>
            <a style="cursor: pointer;" onclick="get_store_ajax('z')">z</a>
          <div class="clearfix"></div>
            
        </div>
        <div id="loading_img_store_offer" style="display:none;position: fixed;z-index: 10;top: 35%;left: 45%;width: 5%;"><img style="width: 100%" src="front_assets/loading-spinner-blue.gif">     
          </div>
        
        <div class="row store_sec" id="store_ajax_div"> 
          <?php $this->load->view('include/store_ajax');?>
        </div>
    </div><!--All store end-->  
  </div><!--All store container end-->  
  <!-- <div class="container">
  
<div class="col-md-12 text-center">
    <ul class="pagination">
    <li><a href="#">Next</a></li>
    <li><a href="#">1</a></li>
    <li class="active"><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li><a href="#">Prev</a></li>

  </ul>
</div>
</div> -->
</div>
<?php $this->load->view('front/footer');?>
<?php $this->load->view('front/footerlink');?>

  <script>
    $(".floating_nav").hide();
    $(window).on('resize load', function(){
      $(".cat_pg_ad1").css("min-height", $(".cat_pg_ad2").height()+"px");
    });
    $(".subscribe").click(function(){
      $(".subscribe_fix_div").fadeIn(200);
      $(".subscribe_fix_div").css({"display": "-webkit-box", "display": "-ms-flexbox", "display":"flex"});
      $(".subscribe_fix_div .form_close").fadeIn(200);
    });
    $(".subscribe_fix_div .form_close").click(function(){
      $(".subscribe_fix_div").fadeOut(200);
      $(".subscribe_fix_div .form_close").fadeOut(200);
    });
    function search_ctrl(){   
      var width = $(window).width();
      if(width < 767){
        $(".form_wrap").css("display","none");
        $(".search_btn").attr("value","Go");
      }
      else{
        $(".form_wrap").css("display","block");
        $(".search_btn").attr("value","Show Offers");
      }
    }
    $(".src_btn").stop().click(function(){
      if($(window).width() < 767){
        $(this).siblings(".form_wrap").slideToggle(300);
      }
      else{
        $(this).siblings(".form_wrap").slideDown(0);
      }
    });
    search_ctrl();
    $(window).on("resize",search_ctrl);
    $(".post_offer_btn, .pop_up_wrap").hover(function(){
      $(".pop_up_wrap").stop().fadeIn(200);
    },function(){
      $(".pop_up_wrap").fadeOut(200);
    });

  </script>
      <script>

$('.droppable').hover(function(){
    $('.dim').fadeIn(0);
},function(){
    $('.dim').fadeOut(0);
});
    </script>
           <script>
    $(".form-fg-pass").css("display","none");
    $(".recover_pass").click(function () {
        $(".form-signin").css("display","none")
        $(".form-fg-pass").css("display","block")
        // $("#").show();   
    });

    $(".b2login").click(function () {
      $(".form-signin").css("display","block")
        $(".form-fg-pass").css("display","none")
        
        // $("#").show();   
    });
   </script>


   <script>
     function get_store_ajax(alp) 
     {
       $("#loading_img_store_offer").show();
        $.ajax({
            url: "<?php echo site_url('store/get_search_store');?>",
            type: "post",
            data: {alp:alp},
            success: function(msg)
            {
               //console.log(msg);
               $("#store_ajax_div").html(msg);
               $("#loading_img_store_offer").hide();
            }      
        });
     }
   </script>

</body>

</html>
