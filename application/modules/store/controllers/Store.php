<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');  

class Store extends MX_Controller {


	function __construct()
	{
		$this->load->model('home/home_model');
		$this->load->model('offer/offer_model');
		$this->load->model('store/store_model');
		$this->load->model('admin/admin_model');

		date_default_timezone_set('Asia/Dhaka');
		if($this->session->userdata('language_select')=='bangla')
		{
			$this->lang->load('front', 'bangla');
		}
		else
		{
			$this->lang->load('front', 'english');
		} 


	}
	public function index()
	{
		$data['active']='home';
		$data['header']=$this->home_model->select_with_where('*','id=1','company');
		// $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
		$data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

		$cur_date=date('Y-m-d');

		$data['cat_brand']=array();
		$data['store_offer']=array();
		foreach ($data['category'] as $key => $value) 
		{
			$data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

			foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
			{
				$data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND mf.edate>=DATE("'.$cur_date.'") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
			}	
			
		}

		$data['is_feature']=$this->home_model->select_condition_random_with_limit('user_login','is_feature=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

		$data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','top_store=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

		$data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND verify_status=1 AND is_publish=1','28');

		$data['offer_type']=$this->home_model->select_with_where('*','offer_st=1','offer_type');

		$data['offer_category']=$this->home_model->select_with_where('*','menu_st=1','catagory');

		$data['division']=$this->home_model->select_all_acending('divisions','position');


		
		
		$data['top_brand_feature']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND verify_status=1 AND is_publish=1','30');

		$data['top_store_brand_offer']=array();

		foreach ($data['top_brand_feature'] as $key => $value) 
		{
			$data['top_store_brand_offer'][$key]=count($this->home_model->select_with_where('*','status=1 AND offer_from='.$value['loginid'],'main_offer'));
		} 

		$data['offer_type']=$this->home_model->select_with_where('*','offer_st=1','offer_type');

		$data['offer_category']=$this->home_model->select_with_where('*','menu_st=1','catagory');

		$data['division']=$this->home_model->select_all_acending('divisions','position');
		
		$data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

		$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');

		//echo '<pre>';print_r($data['top_brand_feature']);die();


		$this->load->view('index',$data);
	}


	public function follow_store($value='')
	{
		$store_id=$this->input->post('store_id');

		$user_id=$this->session->userdata('login_id');

		$user_info=$this->admin_model->select_with_where('*','loginid="'.$user_id.'"','user_login');


		if(!in_array($store_id,explode(',',$user_info[0]['subscribed_store'])))
		{
			if($user_info[0]['subscribed_store']!=null)
			{
				$val['subscribed_store']=$user_info[0]['subscribed_store'].','.$store_id;

			}
			else
			{
				$val['subscribed_store']=$store_id;
			}

			$this->admin_model->update_function('loginid', $user_id, 'user_login',$val);
		}

	}


	public function offer($store_name='')
	{
		$data['active']='home';
		$data['header']=$this->home_model->select_with_where('*','id=1','company');
		// $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
		$data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC'); 

		$cur_date=date('Y-m-d');

		$data['cat_brand']=array();
		$data['store_offer']=array();
		foreach ($data['category'] as $key => $value) 
		{
			$data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

			foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
			{
				$data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND mf.edate>=DATE("'.$cur_date.'") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
			}	
			
		}

		$data['is_feature']=$this->home_model->select_condition_random_with_limit('user_login','is_feature=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

		$data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','top_store=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

		
		$data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND verify_status=1 AND is_publish=1','28');

		$data['offer_type']=$this->home_model->select_with_where('*','offer_st=1','offer_type');

		$data['offer_category']=$this->home_model->select_with_where('*','menu_st=1','catagory');

		$data['division']=$this->home_model->select_all_acending('divisions','position');


		$data['store_info']=$this->home_model->select_where_join('*','user_login','marchant_details','marchant_details.user_id=user_login.loginid','user_login.name="'.$store_name.'"');



		$data['store_offer_list']=$this->offer_model->get_offer_ci('mf.offer_from='.$data['store_info'][0]['loginid'],18,'mf.updated_at','DESC');

		//$data['store_offer_list2']=$this->offer_model->get_offer_ci('mf.offer_from='.$data['store_info'][0]['loginid'],'mf.updated_at','DESC',9);

		$data['office_address']=$this->home_model->get_address_details('md.user_id='.$data['store_info'][0]['loginid']);
		
		$data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

		$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');

		$store_name=explode('_', $store_name);
		$store_name=end($store_name);

		$data['rating_info_individual_user']=$this->home_model->select_with_where('*','user_id="'.$this->session->userdata('login_id').'" and status=1 and type=2 and store_offer_id="'.$store_name.'" ','user_rating');


		$data['rating_info']=$this->store_model->get_sum_count_all_rows('rating','status=1 and type=2 and store_offer_id="'.$store_name.'" ','user_rating');

		// "<pre>";print_r($data['rating_info']);die();

		

		if($data['rating_info']!=null && $data['rating_info'][0]['total_row']!=0)
		{
			$total_rating=$data['rating_info'][0]['total_rating']/$data['rating_info'][0]['total_row'];
		}
		else
		{
			$total_rating="";
		}

		$data['total_rating']=$total_rating; 

		$data['total_followers']=count($this->home_model->select_with_where('*','FIND_IN_SET("'.$store_name.'", subscribed_store)','user_login'));



		// "<pre>";print_r($data['total_followers']);die();

		$this->load->view('store_offer',$data);
	}


	
	public function save_store_rating()
	{

		$store_id=explode('_', $this->input->post('store_name'));

		$data = array('store_offer_id' =>end($store_id),
			'user_id' =>$this->session->userdata('login_id'),
			'rating' =>$this->input->post('rating_val'),
			'type' =>2,
			'created_at' =>date('Y-m-d'));

		$this->offer_model->insert('user_rating',$data);

	}

	public function store_offer_ajax()
	{
		$store_id=$this->input->post('store_id');
		$offer_type=$this->input->post('offer_type');
		$offer_category=$this->input->post('offer_category');
		$location_id=$this->input->post('location_id');
		$offer_store_type=$this->input->post('offer_store_type');
		$duration=$this->input->post('duration');

		$c_date=date('Y-m-d');

		$e_date=date('Y-m-d', strtotime($c_date. ' + '.$duration.' days'));
		//echo $e_date;


		//echo $offer_store_type;die();
		$condition='(mf.offer_store_type=0 OR mf.offer_store_type=1) AND user_login.loginid='.$store_id;

		if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration==0)
		{
			$condition='mf.offer_title_type='.$offer_type.' and (mf.offer_store_type=0 OR mf.offer_store_type=1) AND user_login.loginid='.$store_id;
		}

		if($offer_type==0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration==0)
		{
			$condition='mf.catagories='.$offer_category.' and (mf.offer_store_type=0 OR mf.offer_store_type=1) AND user_login.loginid='.$store_id;
		}

		if($offer_type==0 && $offer_category==0 && $location_id==0 && $offer_store_type!='' && $duration==0)
		{
			$condition='mf.offer_store_type='.$offer_store_type.' AND user_login.loginid='.$store_id;
		}

		if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration==0)
		{
			$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' and (mf.offer_store_type=0 OR mf.offer_store_type=1) AND user_login.loginid='.$store_id;
		}

		if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type!='' && $duration==0)
		{
			$condition='mf.offer_title_type='.$offer_type.' AND mf.offer_store_type='.$offer_store_type.' AND user_login.loginid='.$store_id;
		}

		if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type!='' && $duration==0)
		{
			$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.offer_store_type='.$offer_store_type.' AND user_login.loginid='.$store_id;
		}

		if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type!='' && $duration!=0)
		{
			$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.offer_store_type='.$offer_store_type.' AND mf.edate>="'.$e_date.'" AND user_login.loginid='.$store_id;
		}

		if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type!='' && $duration!=0)
		{
			$condition='mf.offer_title_type='.$offer_type.' AND mf.offer_store_type='.$offer_store_type.' AND mf.edate<="'.$e_date.'" AND user_login.loginid='.$store_id;
		}

		if($offer_type==0 && $offer_category==0 && $location_id==0 && $offer_store_type!='' && $duration!=0)
		{

			$condition='mf.offer_store_type='.$offer_store_type.' AND mf.edate<="'.$e_date.'" AND user_login.loginid='.$store_id;
		}

		if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
		{
			$condition='mf.offer_title_type='.$offer_type.' AND mf.edate>="'.$e_date.'" AND user_login.loginid='.$store_id;
		}


		if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
		{
			$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.edate<="'.$e_date.'" AND user_login.loginid='.$store_id;
		}


		if($offer_type==0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
		{
			$condition='(mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.edate<="'.$e_date.'" AND user_login.loginid='.$store_id;
		}

		$data['store_offer_list']=$this->offer_model->get_offer_ci($condition.' AND mf.status=1',18,'mf.updated_at','DESC');
		$this->load->view('include/store_offer_ajax', $data);
	}


	public function get_search_store()
	{
		$alp=$this->input->post('alp');
		if($alp=='all')
		{
			$data['top_brand_feature']=$this->home_model->select_condition_random('user_login','usertype=2 AND verify_status=1 AND is_publish=1');
		}
		else
		{
		    // $data['top_brand_feature']=$this->home_model->select_condition_random_with_limit('user_login','(is_brand=1 OR show_on_web=1) AND usertype=2 AND verify_status=1 AND is_publish=1 AND name like "'.$alp.'%"','30');

			$data['top_brand_feature']=$this->home_model->select_condition_random('user_login',' usertype=2 AND verify_status=1 AND is_publish=1 AND name like "'.$alp.'%"');
		}

		$data['top_store_brand_offer']=array();

		foreach ($data['top_brand_feature'] as $key => $value) 
		{
			$data['top_store_brand_offer'][$key]=count($this->home_model->select_with_where('*','status=1 AND offer_from='.$value['loginid'],'main_offer'));
		}

		$this->load->view('include/store_ajax', $data);
	}

	

	public function store_subscription_modal()
	{

		$data['loginid']=$this->input->post('loginid');

		$this->load->view('include/store_subscribe_modal', $data);

	}


	public function add_subscribed_store()
	{

		$email=$this->input->post('email');
		$loginid=$this->input->post('loginid');

		//echo '<pre>';print_r($loginid);die();

		$exist_subscribed_store=$this->home_model->select_with_where('*','email="'.$email.'"','user_login'); 

		$unreg_user_subscription=$this->home_model->select_with_where('*','email="'.$email.'"','subscription'); 

        //echo '<pre>';print_r($exist_subscribed_store);
        //echo '<pre>';print_r($unreg_user_subscription);die();

		if(count($exist_subscribed_store)>0)
		{
			$pre_subscribed_store=$exist_subscribed_store[0]['subscribed_store'];

			$subscribed_store_id=$this->home_model->subscribed_store($email,$loginid);

	       //echo '<pre>';print_r($subscribed_store_id);die();

			if(count($subscribed_store_id)>=1)
			{
				echo 1;
			}

			else
			{
				$data_update['subscribed_store']='';
				if($pre_subscribed_store=='' || $pre_subscribed_store==null || $pre_subscribed_store==0)
				{
					$data_update['subscribed_store']=$loginid;
				}
				else
				{
					$data_update['subscribed_store']=$pre_subscribed_store.','.$loginid;
				}

				$this->home_model->update_function('loginid',$exist_subscribed_store[0]['loginid'],'user_login',$data_update);
				echo 2;
			}
		}

		else 
		{
			$pre_subscribed_store=$unreg_user_subscription[0]['subscribed_store'];

			$subscribed_store_id=$this->home_model->unreg_email_subscribed_store($email,$loginid);

	       //echo '<pre>';print_r($subscribed_store_id);die();

			if(count($subscribed_store_id)>=1)
			{
				echo 1;
			}

			else
			{
				$data_sub['subscribed_store']='';

				if($pre_subscribed_store=='' || $pre_subscribed_store==null || $pre_subscribed_store==0)
				{
					$data_sub['email']=$email;
					$data_sub['subscribed_store']=$loginid;


					if (count($unreg_user_subscription)==0) 
					{
						$data_sub['created_at']= date('Y-m-d H:i:s');
						$this->home_model->insert_ret('subscription',$data_sub);

					}
					else
					{	
						$this->home_model->update_function('email',$email,'subscription',$data_sub);
					}


					echo 2;
				}
				else
				{

					$data_sub['subscribed_store']=$pre_subscribed_store.','.$loginid;

					$this->home_model->update_function('email',$email,'subscription',$data_sub);
					echo 2;
				}

			}
		}

	}


}
