<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends MX_Controller {
 //public $counter=0;
    function __construct() {
        parent::__construct();
       
    }
	
	public function index($id='')
	{
		
		$this->session->unset_userdata('login_id');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('reg_id');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('district_id');
		$this->session->unset_userdata('type');
		$this->session->unset_userdata('sub_tab');
		$this->session->set_userdata('log_scc','Successfully Logged-Out!');
		
		redirect('home','refresh');
		
		
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */