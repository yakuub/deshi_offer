<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->model('user/user_model');
        $this->load->model('home/home_model');
        $this->load->helper('inflector');
        $this->load->library('encrypt');

        $login_id=$this->session->userdata('login_id');
        $name=$this->session->userdata('name');

        if($login_id=='')
        {
            $this->session->unset_userdata('login_id');
            $this->session->unset_userdata('name');
            $this->session->unset_userdata('role');
            $this->session->set_userdata('log_err','Please Login First before place an order');
            redirect('login','refresh');
        }


    }

    public function index()
    {
        /// Common Date Start
            date_default_timezone_get('Asia/Dhaka');
            $charge=$this->input->post('charge');
            $total=(float)$this->input->post('total');
       
            $login_id=   $this->session->userdata('login_id');
            $date=date('Y-m-d h:i:a');
            $my_format=date('Ymd_his');

            $order_id='Order_'.$my_format.'_'.$login_id;
            $cart= $this->cart->contents(); 

            foreach ($cart as $key => $item) 
            {
                $data['order_id']=$order_id;
                $data['user_id']=$login_id;
                $data['product_id']=$item['product_id'];
                $data['qty']=$item['qty'];
                $data['price']=$item['price'];
                $data['discount']=$item['discount'];
                $data['color_id']=$item['color_id'];
                $data['size_id']=$item['size_id'];
                $data['created_at']=$date;

                $this->home_model->insert('customer_order',$data);

            }


            $data_tran['order_id']=$order_id;
            $data_tran['user_id']=$login_id;
            $data_tran['transaction_id']=$my_format.'-'.$login_id;
            $data_tran['payment_type']=1;
            //$data_tran['payment_details']='Cash On Deliver';
            $data_tran['amount_to_pay']=number_format($total,'2','.','');
            $data_tran['amount_paid']=0.00;
            $data_tran['charge']=$charge;
            $data_tran['created_at']=$date;
            $this->home_model->insert('transaction',$data_tran);

            $this->session->set_userdata('log_scc','Thank you for your order. We will contact with you as soon as possible');
            $this->cart->destroy();
            redirect('user','refresh');
    }

    
}
