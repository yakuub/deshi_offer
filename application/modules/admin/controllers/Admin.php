<?php   
if (!defined('BASEPATH'))
    exit('No direct script access allowed'); 

class Admin extends MX_Controller {
	
    public function __construct()
    {

      date_default_timezone_set('Asia/Dhaka');
      $this->load->model('admin_model');
      $email = $this->session->userdata('email');
      $type = $this->session->userdata('usertype');
      $login_id = $this->session->userdata('login_id');

      if($login_id=='' || $type==2 || $type==3)
      {
        $this->session->unset_userdata('login_id');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('usertype');
        $this->session->set_userdata('log_err','Enter Email and Password First');
        redirect('login','refresh');
    }

}
public function index()
{
  $this->load->view('index');

}





     /////////////////////********  Manage Division Start *******////////////////////////////


public function add_division()
{
    $data['division_list']=$this->admin_model->select_all('divisions');

        //echo '<pre>';print_r($data['division_list']);die();

    $this->load->view('division/add_division',$data);

}



public function add_division_list()
{
    $data['name'] = $this->input->post('name');
    $data['bn_name'] = $this->input->post('bn_name');
       // $data['position'] = $this->input->post('position');

    $division_id = $this->admin_model->insert_ret('divisions', $data);

    $this->session->set_flashdata('Successfully','Division Added Successfully');

    redirect('admin/add_division', 'refresh');

}

public function edit_division($id)
{

   $data['division_info']=$this->admin_model->select_with_where('*','id='.$id,'divisions');
   $data['division_list']=$this->admin_model->select_all('divisions');

   $this->load->view('division/edit_division',$data);

}


public function update_division_list($id)
{
    $data['name'] = $this->input->post('name');
    $data['bn_name'] = $this->input->post('bn_name');
        //$data['position'] = $this->input->post('position');

    $this->admin_model->update_function('id',$id,'divisions', $data);

    $this->session->set_flashdata('Successfully','Division Updated Successfully');

    redirect('admin/add_division', 'refresh');

}

public function delete_division($id)
{

    $this->admin_model->delete_function('divisions', 'id', $id);

    $this->session->set_flashdata('Successfully','Division Deleted Successfully');
    

    redirect('admin/add_division', 'refresh');
}


    ///////////////////////********  Manage Division End *******//////////////////////////////




   ////////////////////********  Manage District Start *******/////////////////////////////


public function add_district()
{
    $data['division_list']=$this->admin_model->select_all('divisions');

    $data['district_list']=$this->admin_model->select_left_join('*,districts.id as dis_id,divisions.name as div_name,districts.name as dis_name','districts','divisions','divisions.id=districts.division_id');

        //echo '<pre>';print_r($data['district_list']);die();

    $this->load->view('district/add_district',$data);

}



public function add_district_list()
{
    $data['name'] = $this->input->post('name');
    $data['bn_name'] = $this->input->post('bn_name');
    $data['division_id'] = $this->input->post('division_id');

    $district_id = $this->admin_model->insert_ret('districts', $data);

    
    $this->session->set_flashdata('Successfully','Districts Added Successfully');

    redirect('admin/add_district', 'refresh');

}

public function edit_district($id)
{

    $data['division_list']=$this->admin_model->select_all('divisions');

    $data['division_info']=$this->admin_model->select_left_join('*,districts.id as dis_id,divisions.name as div_name,districts.name as dis_name','districts','divisions','divisions.id=districts.division_id');

    $data['district_list']=$this->admin_model->select_where_join('*,districts.id as dis_id,divisions.name as div_name,districts.name as dis_name,districts.bn_name as dis_bn_name','districts','divisions','divisions.id=districts.division_id','districts.id='.$id);

       // echo '<pre>';print_r($data['district_list']);die();

    $this->load->view('district/edit_district',$data);

}


public function update_district_list($id)
{
    $data['name'] = $this->input->post('name');
    $data['bn_name'] = $this->input->post('bn_name');
    $data['division_id'] = $this->input->post('division_id');


    $this->admin_model->update_function('id',$id,'districts', $data);

    $this->session->set_flashdata('Successfully','Districts Updated Successfully');

    redirect('admin/add_district', 'refresh');

}

public function delete_district($offer_id)
{
    $data['offer_st'] =2;
    $this->admin_model->update_function('offer_id', $offer_id, 'offer_type', $data);

        //echo "<pre>";print_r($data);die();

    redirect('admin/add_district', 'refresh');
}


    ///////////////////////********  Manage District End *******///////////////////////////


    ///////////////////////********  Manage Slider Start *******///////////////////////////

public function slider()
{

    $data['slider_list']=$this->admin_model->select_with_where('*','status!=2','slider');
    $this->load->view('slider/slider_list',$data);
}

public function add_slider()
{

    $this->load->view('slider/add_slider');

}


public function add_slider_post()
{

    $data['slidertitle'] = $this->input->post('slidertitle');
    $data['slidertitle_bang'] = $this->input->post('slidertitle_bang');
    $data['slider_sl'] = $this->input->post('slider_sl');
    $data['slider_link'] = $this->input->post('slider_link');
    $data['description'] = $this->input->post('description');
    $data['description_bang'] = $this->input->post('description_bang');
    $data['status'] = 1;
    $data['adddate'] =date('Y-m-d h:i:a');

    $id = $this->admin_model->insert_ret('slider', $data);

        // $name=$_FILES["file"]['name'];

    $i_ext = explode('.', $_FILES['file']['name']);
    $target_path = 'slider_' . $id . '.' . end($i_ext);
    $size = getimagesize($_FILES['file']['tmp_name']);
    if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/slider/' . $target_path)) {
        $data_img['img'] = $target_path;
    }

    $this->admin_model->update_function('sliderid', $id, 'slider', $data_img);

    if ($size[0] == 1200 || $size[1] == 500) {

    } else {
            $imageWidth = 1200; //Contains the Width of the Image

            $imageHeight = 500;

            $this->resize($imageWidth, $imageHeight, "uploads/slider/" . $target_path, "uploads/slider/" . $target_path);
        }

        $this->session->set_flashdata('Successfully','Added Successfully');

        redirect('admin/slider','refresh');
    }


    public function edit_slider($sliderid)
    {

        $data['slider_info']=$this->admin_model->select_with_where('*','sliderid='.$sliderid, 'slider');

        //echo '<pre>';print_r($data['slider_info']);die();

        $this->load->view('slider/edit_slider',$data);
        
    }

    public function update_slider_post($sliderid)
    {

        $data['slidertitle'] = $this->input->post('slidertitle');
        $data['slidertitle_bang'] = $this->input->post('slidertitle_bang');
        $data['slider_sl'] = $this->input->post('slider_sl');
        $data['slider_link'] = $this->input->post('slider_link');
        $data['description'] = $this->input->post('description');
        $data['description_bang'] = $this->input->post('description_bang');
        
        $this->admin_model->update_function('sliderid',$sliderid,'slider',$data);

        $pre_image_name = $this->input->post('pre_img');
        
        $path_to_file = 'uploads/slider/'.$pre_image_name;

        $mt=$data['slidertitle']."_".$sliderid;
        $pname= preg_replace('/[ ,]+/', '_', trim($mt));
        $pname= str_replace('/', '_', $pname);
        $pname= str_replace('.', '_', $pname);


        
        if($_FILES["file"]['name'])
        {
            if (unlink($path_to_file)) {}

                $i_ext = explode('.', $_FILES['file']['name']);
            $target_path = $pname.'.'.end($i_ext);
            $size = getimagesize($_FILES['file']['tmp_name']);

            if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/slider/' . $target_path))
            {
                $data_img['img'] = $target_path;
            }

            $this->admin_model->update_function('sliderid', $sliderid, 'slider', $data_img);

            if ($size[0] == 1200 || $size[1] == 500) {

            } else {
                $imageWidth = 1200; //Contains the Width of the Image

                $imageHeight = 500;

                $this->resize($imageWidth, $imageHeight, "uploads/slider/" . $target_path, "uploads/slider/" . $target_path);
            }
        }
        $this->session->set_flashdata('Successfully','Updated Successfully');

        redirect('admin/slider','refresh');
    }

    public function update_slider_status()
    {
        $sliderid=$this->input->post('sliderid');
        $data['status']=$this->input->post('status');

        $this->admin_model->update_function('sliderid', $sliderid, 'slider', $data);
        redirect('admin/slider','refresh');
    }


    public function delete_slider($sliderid,$name)
    {

        $path_to_file = 'uploads/slider/'.$name;
        if(unlink($path_to_file)) {

            $data['status']=2;
            $data['img']=NULL;

            $this->admin_model->update_function('sliderid',$sliderid,'slider',$data);
            //echo 'deleted successfully';
            $this->session->set_flashdata('Successfully','Deleted Successfully');

        }else {
             //echo 'errors occured';
           $this->session->set_flashdata('Error','Data is not Deleted');
       }
       redirect('admin/slider','refresh');
   }

    ///////////////////////********  Manage Slider End *******///////////////////////////





    //////////////////********  Manage Area Start *******///////////////////////////////


   public function add_area()
   {
    $data['division_list']=$this->admin_model->select_all_name_ascending('name','divisions');
    $data['district_list']=$this->admin_model->select_all_name_ascending('name','districts');

    $data['area_list']=$this->admin_model->select_where_left_join_two('*,districts.id as dis_id,divisions.name as div_name,districts.name as dis_name','area','divisions','divisions.id=area.division_id','districts','districts.id=area.district_id','area_st=1');

       // echo '<pre>';print_r($data['area_list']);die();

    $this->load->view('area/add_area',$data);

}



public function add_area_list()
{
    $data['area_title'] = $this->input->post('area_title');
    $data['area_title_bangla'] = $this->input->post('area_title_bangla');
    $data['division_id'] = $this->input->post('division_id');
    $data['district_id'] = $this->input->post('district_id');
    $data['area_st'] = 1;
    $data['add_date'] = date('Y-m-d H:i:s');

    $area_id = $this->admin_model->insert_ret('area', $data);


    $this->session->set_flashdata('Successfully','Area Added Successfully');

    redirect('admin/add_area', 'refresh');

}

public function edit_area($area_id)
{
    $data['division_list']=$this->admin_model->select_all_name_ascending('name','divisions');
    $data['district_list']=$this->admin_model->select_all_name_ascending('name','districts');

    $data['area_list']=$this->admin_model->select_where_left_join_two('*,districts.id as dis_id,divisions.name as div_name,districts.name as dis_name','area','divisions','divisions.id=area.division_id','districts','districts.id=area.district_id','area_st=1');

    $data['area_info']=$this->admin_model->select_where_left_join_two('*,districts.id as dis_id,divisions.name as div_name,districts.name as dis_name','area','divisions','divisions.id=area.division_id','districts','districts.id=area.district_id','area_id='.$area_id);

        //echo '<pre>';print_r($data['area_info']);die();

    $this->load->view('area/edit_area',$data);

}


public function update_area_list($area_id)
{
    $data['area_title'] = $this->input->post('area_title');
    $data['area_title_bangla'] = $this->input->post('area_title_bangla');
    $data['division_id'] = $this->input->post('division_id');
    $data['district_id'] = $this->input->post('district_id');


    $this->admin_model->update_function('area_id',$area_id,'area', $data);

    $this->session->set_flashdata('Successfully','Area Updated Successfully');

    redirect('admin/add_area', 'refresh');

}

public function delete_area($area_id)
{
    $data['area_st'] = 2;
    $this->admin_model->update_function('area_id', $area_id, 'area', $data);

    $this->session->set_flashdata('Successfully','Area Deleted Successfully');

    redirect('admin/add_area', 'refresh');
}


    ///////////////////////********  Manage Area End *******//////////////////////////////////



    ///////////////////////********  Offer Type Start *******////////////////////////////////


public function add_offer_type()
{
    $data['offer_type_list']=$this->admin_model->select_with_where('*','offer_st=1','offer_type');

        //echo '<pre>';print_r($data['offer_type_list']);die();

    $this->load->view('offer/offer_type/add_offer_type',$data);

}



public function add_offer_type_post()
{
    $data['offer_type_tilte'] = $this->input->post('offer_type_tilte');
    $data['offer_type'] = $this->input->post('offer_type');
    $data['offer_st'] = 1;
    $data['color_code'] = '#'.$this->input->post('color_code');
    $data['add_by'] = $this->input->post('add_by');
    $data['add_date'] = date('Y-m-d H:i:s');

    $offer_type_id = $this->admin_model->insert_ret('offer_type', $data);

    if ($_FILES['file']['name']) {
        $i_ext       = explode('.', $_FILES['file']['name']);
        $target_path = 'offer_' . $offer_type_id . '.' . end($i_ext);
        $size        = getimagesize($_FILES['file']['tmp_name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/offer/' . $target_path)) {
            $data_img['offer_img'] = $target_path;
        }

        $this->admin_model->update_function('offer_id', $offer_type_id, 'offer_type', $data_img);

        if ($size[0] == 1200 || $size[1] == 500) {

        } else {
                    $imageWidth = 1200; //Contains the Width of the Image
                    
                    $imageHeight = 500;
                    
                    $this->resize($imageWidth, $imageHeight, "uploads/offer/" . $target_path, "uploads/offer/" . $target_path);
                }
                
            }


            $this->session->set_flashdata('Successfully','Added Successfully');

            redirect('admin/add_offer_type', 'refresh');

        }

        public function edit_offer_type($offer_id)
        {
            $data['offer_type_list']=$this->admin_model->select_with_where('*','offer_id='.$offer_id,'offer_type');

            $data['offer_type_info']=$this->admin_model->select_with_where('*','offer_st=1','offer_type');

        //echo '<pre>';print_r($data['offer_type_list']);die();

            $this->load->view('offer/offer_type/edit_offer_type',$data);

        }


        public function update_offer_type_post($offer_id)
        {
            $data['offer_type_tilte'] = $this->input->post('offer_type_tilte');
            $data['offer_type'] = $this->input->post('offer_type');
            $data['color_code'] = '#'.$this->input->post('color_code');


            $this->admin_model->update_function('offer_id',$offer_id,'offer_type', $data);

            $mt=$data['offer_type_tilte']."_".$offer_id;
            $pname= preg_replace('/[ ,]+/', '_', trim($mt));
            $pname= str_replace('/', '_', $pname);
            $pname= str_replace('.', '_', $pname);

            $pre_image_name = $this->input->post('pre_img');
            $path_to_file = 'uploads/offer/'.$pre_image_name;

            if ($_FILES["file"]['name']) {
                if (unlink($path_to_file)) {}
                    $i_ext       = explode('.', $_FILES['file']['name']);
                $target_path = $pname.'.'.end($i_ext);
                $size        = getimagesize($_FILES['file']['tmp_name']);

                if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/offer/' . $target_path)) {
                    $data_img['offer_img'] = $target_path;
                }

                $this->admin_model->update_function('offer_id',$offer_id,'offer_type', $data_img);

                if ($size[0] == 1200 || $size[1] == 500) {

                } else {
                $imageWidth = 1200; //Contains the Width of the Image
                
                $imageHeight = 500;
                
                $this->resize($imageWidth, $imageHeight, "uploads/offer/" . $target_path, "uploads/offer/" . $target_path);
            }
        }

        $this->session->set_flashdata('Successfully','Updated Successfully');
        
        redirect('admin/add_offer_type', 'refresh');
        
    }

    public function delete_offer_type($offer_id)
    {
        $data['offer_st'] = 2;
        $this->admin_model->update_function('offer_id', $offer_id, 'offer_type', $data);
        
        $this->session->set_flashdata('Successfully','Deleted Successfully');
        
        redirect('admin/add_offer_type', 'refresh');
    }



    ///////////////////////********  Offer Type End *******////////////////////////////////


    ///////////////////********  Target People Start *******////////////////////////////////
    


    public function add_target_people()
    {
        $data['target_people_list']=$this->admin_model->select_with_where('*','target_st=1','target_people');

        //echo '<pre>';print_r($data['target_people_list']);die();

        $this->load->view('offer/target_people/add_target_people',$data);
        
    }



    public function add_target_people_list()
    {
        $data['target_tilte'] = $this->input->post('target_tilte'); 
        $data['target_st'] = 1;
        $data['add_by'] = $this->input->post('add_by');
        $data['add_date'] = date('Y-m-d H:i:s');

        $offer_type_id = $this->admin_model->insert_ret('target_people', $data);

        if ($_FILES['file']['name']) {
            $i_ext       = explode('.', $_FILES['file']['name']);
            $target_path = 'target_people_' . $offer_type_id . '.' . end($i_ext);
            $size        = getimagesize($_FILES['file']['tmp_name']);
            if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/target_people/' . $target_path)) {
                $data_img['target_img'] = $target_path;
            }

            $this->admin_model->update_function('offer_id', $offer_type_id, 'target_people', $data_img);

            if ($size[0] == 1200 || $size[1] == 500) {

            } else {
                    $imageWidth = 1200; //Contains the Width of the Image
                    
                    $imageHeight = 500;
                    
                    $this->resize($imageWidth, $imageHeight, "uploads/target_people/" . $target_path, "uploads/target_people/" . $target_path);
                }
                
            }

            $this->session->set_flashdata('Successfully','Added Successfully');

            redirect('admin/add_target_people', 'refresh');

        }

        public function edit_target_people($offer_id)
        {
            $data['target_people_list']=$this->admin_model->select_with_where('*','offer_id='.$offer_id,'target_people');

            $data['target_people_info']=$this->admin_model->select_with_where('*','target_st=1','target_people');

        //echo '<pre>';print_r($data['offer_type_list']);die();

            $this->load->view('offer/target_people/edit_target_people',$data);

        }


        public function update_target_people_list($offer_id)
        {
            $data['target_tilte'] = $this->input->post('target_tilte'); 

            $this->admin_model->update_function('offer_id',$offer_id,'target_people', $data);

            $mt=$data['target_tilte']."_".$offer_id;
            $pname= preg_replace('/[ ,]+/', '_', trim($mt));
            $pname= str_replace('/', '_', $pname);
            $pname= str_replace('.', '_', $pname);




            $pre_image_name = $this->input->post('pre_img');
            $path_to_file = 'uploads/target_people/'.$pre_image_name;

            if ($_FILES["file"]['name']) {
             if (unlink($path_to_file)) {}
                $i_ext       = explode('.', $_FILES['file']['name']);
            $target_path = $pname.'.'.end($i_ext);
            $size        = getimagesize($_FILES['file']['tmp_name']);
            
            if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/target_people/' . $target_path)) {
                $data_img['target_img'] = $target_path;
            }
            
            $this->admin_model->update_function('offer_id',$offer_id,'target_people', $data_img);
            
            if ($size[0] == 1200 || $size[1] == 500) {

            } else {
                $imageWidth = 1200; //Contains the Width of the Image
                
                $imageHeight = 500;
                
                $this->resize($imageWidth, $imageHeight, "uploads/target_people/" . $target_path, "uploads/target_people/" . $target_path);
            }
        }

        $this->session->set_flashdata('Successfully','Updated Successfully');
        
        redirect('admin/add_target_people', 'refresh');
        
    }

    public function delete_target_people($offer_id)
    {
        $data['target_st'] = 2;
        $this->admin_model->update_function('offer_id', $offer_id, 'target_people', $data);
        
        //echo "<pre>";print_r($data);die();
        
        redirect('admin/add_target_people', 'refresh');
    }


    ///////////////////////********  Target People End *******//////////////////////////////


    //////////////////////********  New Offer Starts *******//////////////////////////////


    public function add_new_offer()
    {
        $data['offer_for_list']=$this->admin_model->select_with_where('*','usertype=2 AND verify_status=1','user_login');

        $data['offer_type_list']=$this->admin_model->select_with_where('*','offer_st=1','offer_type');

        $data['cat_list']=$this->admin_model->select_with_where('*','menu_st!=2','catagory');

        $data['tag_list']=$this->admin_model->select_all('tag');

        //echo '<pre>';print_r($data['offer_type_list']);die();

        $this->load->view('offer/main_offer/add_offer',$data);
        
    }

    public function add_new_offer_post()
    {
        $data['post_by'] = $this->input->post('post_by');
        $data['offer_from'] = $this->input->post('offer_from'); 
        $data['offer_title'] = $this->input->post('offer_title'); 
        $data['offer_title_type'] = $this->input->post('offer_title_type');
        $data['sdate'] = date("Y-m-d", strtotime($this->input->post('sdate')));

        if (($this->input->post('exp_type')==0))
        {
            $data['edate'] = '0000-00-00';
            $data['show_expire_date'] = '0000-00-00';    
        }
        else 
        {
          $data['edate']=date("Y-m-d", strtotime($this->input->post('edate')));
          $data['show_expire_date'] = date("Y-m-d", strtotime($this->input->post('show_expire_date')));  
      }   


      $data['of_post_date'] = date("Y-m-d", strtotime($this->input->post('of_post_date')));



      $data['offer_store_type'] = date("Y-m-d", strtotime($this->input->post('offer_store_type')));

      $data['offer_store_type'] = $this->input->post('offer_store_type');
      $data['is_hot'] = $this->input->post('hot_status');
      $data['discount'] = $this->input->post('dvalue');
      $data['inner_discount'] = $this->input->post('inner_discount'); 
      $data['of_post_date'] = date("Y-m-d", strtotime($this->input->post('of_post_date')));

      $data['offer_desc'] = $this->input->post('offer_desc');
      $data['offer_terms'] = $this->input->post('offer_terms');
      $data['catagories'] = $this->input->post('catagories');

      $data['climit'] = $this->input->post('limit');

      if ($data['climit']==1) {

        $data['climitamnt']=$this->input->post('coupon_total');
    }
    else{
        $data['climitamnt']=0;
    }

    $data['ctype'] = $this->input->post('type');
    $data['target_people'] = $this->input->post('target_people_normal');


    $data['tags'] = '';
    $tags         = $this->input->post('tags');
    if($this->input->post('tags')){
        $data['tags'] = implode(',', $tags);
    }

        //echo '<pre>';print_r($data['tags']);die();
    $data['status'] = 1;
    $data['cdate'] = date('Y-m-d H:i:s');

    $offer_id = $this->admin_model->insert_ret('main_offer', $data);


    $data_pro_name['profile_name'] = preg_replace('/[ ,]+/', '_', trim($data['offer_title']));
    $data_pro_name['profile_name'] = $data_pro_name['profile_name'] . '_' . $offer_id;
    $data_pro_name['profile_name'] = str_replace("'", '', $data_pro_name['profile_name']);
    $data_pro_name['profile_name'] = str_replace("/", '_', $data_pro_name['profile_name']);
    $data_pro_name['profile_name'] = str_replace("@", '_', $data_pro_name['profile_name']);
    $data_pro_name['profile_name'] = str_replace("%", '_', $data_pro_name['profile_name']);


    $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_pro_name);



    if ($_FILES['file']['name']) {
        $i_ext       = explode('.', $_FILES['file']['name']);
        $target_path = 'offer_' . $offer_id . '.' . end($i_ext);
        $size        = getimagesize($_FILES['file']['tmp_name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/offer/' . $target_path)) {
            $data_img['offer_featured_file'] = $target_path;
        }

        $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_img);

        if ($size[0] == 1200 || $size[1] == 500) {

        } else {
                    $imageWidth = 1200; //Contains the Width of the Image
                    
                    $imageHeight = 500;
                    
                    $this->resize($imageWidth, $imageHeight, "uploads/offer/" . $target_path, "uploads/offer/" . $target_path);
                }
                
            }

            if ($data['ctype']=="buy_it") {

                $data_v['offer_id']=$offer_id;
                $data_v['voucher_price']=$this->input->post('coupon_price');
                $data_v['voucher_code']=$this->input->post('coupon_code');
                $data_v['add_date'] = date('Y-m-d H:i:s');

                $voucher_id = $this->admin_model->insert_ret('voucher', $data_v);


                $data_vtype['ctype'] = 1;
                $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_vtype);
            }

            elseif ($data['ctype']=="get_code") {

                $data_u['offer_id']=$offer_id;
                $data_u['utm_code']=$this->input->post('utm_code');
                $data_u['utm_link']=$this->input->post('ref_link');
                $data_u['add_date'] = date('Y-m-d H:i:s');

                $online_deal_id = $this->admin_model->insert_ret('utm_ofr', $data_u);

                $data_gtype['ctype'] = 2;
                $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_gtype);
            }


            elseif ($data['ctype']=="get_sms") {

                $data_s['offer_id']=$offer_id;
                $data_s['sms']=$this->input->post('sms');
                $data_s['add_date'] = date('Y-m-d H:i:s');

                $offer_sms_id = $this->admin_model->insert_ret('offer_sms', $data_s);

                $data_stype['ctype'] = 3;
                $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_stype);
            }

            elseif ($data['ctype']=="online_deal") {

                $data_o['offer_id']=$offer_id;
                $data_o['ref_link']=$this->input->post('ref_link');
                $data_o['add_date'] = date('Y-m-d H:i:s');

                $online_deal_id = $this->admin_model->insert_ret('online_deal', $data_o);

                $data_otype['ctype'] = 4;
                $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_otype);
            }
            elseif ($data['ctype']=="activate_deal") {

                // $data_a['offer_id']=$offer_id;
                // $data_a['utm_code']=$this->input->post('utm_code');
                // $data_a['utm_link']=$this->input->post('utm_link');
                // $data_a['add_date'] = date('Y-m-d H:i:s');

                // $online_deal_id = $this->admin_model->insert_ret('utm_ofr', $data_a);

                $data_actype['ctype'] = 5;
                $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_actype);
            }


            elseif ($data['ctype']=="direct_sale") {

                $data_d['offer_id']=$offer_id;
                $data_d['sale_price']=$this->input->post('price');
                $data_d['add_date'] = date('Y-m-d H:i:s');

                $direct_sale_id = $this->admin_model->insert_ret('direct_sale', $data_d);

                $data_dtype['ctype'] = 6;
                $data_dtype['price'] = $this->input->post('price');;

                $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_dtype);
            }




            elseif ($data['ctype']=="custom_link") {

                $data_c['offer_id']=$offer_id;
                $data_c['offer_title']=$this->input->post('title');
                $data_c['offer_values']=$this->input->post('valu');
                $data_c['add_date'] = date('Y-m-d H:i:s');

                $online_deal_id = $this->admin_model->insert_ret('custom_ofr', $data_c);

                $data_ctype['ctype'] = 7;
                $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_ctype);
            }

            else{}


            // insert into notification table

            $offer_from=$this->input->post('offer_from'); //store_id

        $cat=$this->input->post('catagories');

        $all_sub_store=$this->admin_model->select_with_where('*','FIND_IN_SET("'.$offer_from.'", subscribed_store)','user_login');



        // "<pre>";print_r($all_sub_cat);die();

        foreach ($all_sub_store as $key => $value) {

            $val['type']=6;
            $val['content']="You have an offer of";
            $val['redirect_link']="offer/offer_details/".$data_pro_name['profile_name'];
            $val['created_at']=date('Y-m-d h:i:s');
            $val['user_id']=$value['loginid'];
            $val['offer_id']=$offer_id;
            $val1['notification_purpose']=2;

            $this->admin_model->insert('notifications',$val);
            


        }

        $all_sub_cat=$this->admin_model->select_with_where('*','FIND_IN_SET("'.$cat.'", subscribed_category)','user_login');

        foreach ($all_sub_cat as $key => $value) {

            $val1['type']=6;
            $val1['content']="You have an offer";
            $val1['redirect_link']="offer/offer_details/".$data_pro_name['profile_name'];
            $val['created_at']=date('Y-m-d h:i:s');
            $val1['user_id']=$value['loginid'];
            $val1['offer_id']=$offer_id;
            $val1['notification_purpose']=1;



            $is_exist_offer_id=$this->admin_model->select_with_where('*','view_status=2 and offer_id="'.$offer_id.'" and user_id="'.$val1['user_id'].'"','notifications');

            if($is_exist_offer_id==null)
            {

                $this->admin_model->insert('notifications',$val1);
            }


        }


        $this->session->set_flashdata('Successfully','Offer Added Successfully');



        redirect('admin/new_offer_list', 'refresh');

    }


    public function new_offer_list()
    {

        $data['offer_for_list']=$this->admin_model->select_with_where('*','usertype=2 AND verify_status=1','user_login');

        $data['offer_type_list']=$this->admin_model->select_with_where('*','offer_st=1','offer_type');

        $data['cat_list']=$this->admin_model->select_all('catagory');

        $current_date=date('Y-m-d');

        $data['new_offer_list']=$this->admin_model->select_where_join('*,main_offer.offer_id as moff_id,offer_type.offer_id as oo_id','main_offer','offer_type','offer_type.offer_id=main_offer.offer_title_type','status=1 AND edate="0000-00-00" OR edate>="'.$current_date.'"');


        $pagination_condition = 'status=1 AND edate="0000-00-00" OR edate>="'.$current_date.'"';

        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() . 'admin/new_offer_list';
        $page_number=$this->uri->segment(3);


        $config['total_rows'] = count($data['new_offer_list']);

        $config['uri_segment'] = 3;

        $data['count_offers']=$config['total_rows'];
        $config['suffix'] = '';

        $data['total_count'] = $config['total_rows'];
        $data['page_links'] ='';

        if (empty($page_number))
        {
            $page_number = 1;
        }

        $offset = ($page_number - 1) * $this->pagination->per_page;
        $page = ($page_number) ? $page_number : 0;
        $this->admin_model->setPageNumber($this->pagination->per_page);
        $this->admin_model->setOffset($offset);
        $this->pagination->initialize($config);

        $data['page_links'] = $this->pagination->create_links();

        $data['new_offer_list'] = $this->admin_model->offer_with_pagination($pagination_condition,$this->pagination->per_page,$offset);


       // echo '<pre>';print_r($data['new_offer_list']);die();

        $this->load->view('offer/main_offer/offer_list',$data);

    }

    public function filter_offer_list()
    {


        $firstday=$this->input->post('firstday');
        $lastday=$this->input->post('lastday');
        $type=$this->input->post('type');

        // $cuurent_date=date('Y-m-d');
        // $cuurent_month=date('m');

        $data['offer_type_list']=$this->admin_model->select_with_where('*','offer_st=1','offer_type');

        if ($type=='all') 
        {
            $data['new_offer_list']=$this->admin_model->select_where_left_join('*,main_offer.offer_id as moff_id,offer_type.offer_id as oo_id','main_offer','offer_type','offer_type.offer_id=main_offer.offer_title_type','status=1 AND main_offer.sdate>="'.$firstday.'" AND edate<="'.$lastday.'"'); 
        }

        else
        {
            $data['new_offer_list']=$this->admin_model->select_where_left_join('*,main_offer.offer_id as moff_id,offer_type.offer_id as oo_id','main_offer','offer_type','offer_type.offer_id=main_offer.offer_title_type','status=1 AND offer_title_type='.$type); 
        }



        //echo '<pre>';print_r($data['new_offer_list']);die();

        $this->load->view('offer/main_offer/include/offer_filter_ajax',$data);


    }





    public function highlight_offer_list()
    {
        $data['highlight_offer']=$this->admin_model->select_where_left_join('*,offer_highlights.offer_title as h_offer_title','offer_highlights','main_offer','main_offer.offer_id=offer_highlights.offer_id','offer_highlights.status=1');

        //echo '<pre>';print_r($data['highlight_offer']);die();

        $this->load->view('offer/main_offer/highlight_offer_list',$data); 
    }


    public function add_product_highlights()
    {

        $data['offer_checked']='';

        $offer_checked = $this->input->post('offer_checked');

        $offer_no=count($offer_checked);

        if ($offer_no>0) {

            foreach ($offer_checked as $value) {
                $data['offer_checked'].=$value.',';
            }

        }

        $data['offer_checked']=substr($data['offer_checked'], 0,-1);


       //echo '<pre>';print_r( $data['offer_checked']);die();

        $this->load->view('offer/main_offer/add_product_highlights',$data);

    } 

    public function save_product_highlights()
    {

        $data['offer_id'] = $this->input->post('offer_checked');

        $data['offer_title']=$this->input->post('offer_title');
        $data['offer_type']=$this->input->post('offer_type');
        $data['expire_date']=date("Y-m-d", strtotime($this->input->post('expire_date')));
        $data['created_at']=date('Y-m-d H:i:s');

        $product_highlights_id=$this->admin_model->insert_ret('offer_highlights',$data);

        redirect('admin/new_offer_list', 'refresh');

    }

    public function delete_highlited_offer($id)
    {
        $data['status'] = 2;
        $this->admin_model->update_function('id', $id, 'offer_highlights', $data);

        $this->session->set_flashdata('Successfully','Deleted Successfully');

        redirect('admin/highlight_offer_list', 'refresh');
    }


    public function view_highlited_offer($id)
    {

        $data['highlighted_offer_list']=$this->admin_model->select_with_where('*','offer_highlights.id='.$id,'offer_highlights');

        $off_id=$data['highlighted_offer_list'][0]['offer_id'];


        $data['highlited_offer']=$this->admin_model->select_with_where('*','main_offer.offer_id in ('.$off_id.')','main_offer');

        $data['highlight_id']=$id;

        $current_date=date('Y-m-d');

        $data['offer_list']=$this->admin_model->select_with_where('*','status=1 AND edate>="'.$current_date.'"','main_offer');

        //echo '<pre>';print_r($data['offer_list']);die();

        $this->load->view('offer/main_offer/view_highlited_offer', $data);
    }


    public function delete_highlited_offer_id($id,$offer_id)
    {   
        //echo '<pre>';print_r($offer_id);die();
        $offer_highlights=$this->admin_model->highlighted_products($id,$offer_id);

        $highlited_offer_id=explode(',', $offer_highlights[0]['offer_id']);

        //echo '<pre>';print_r($offer_id);die();
        $data['offer_id']=NULL;

        foreach ($highlited_offer_id as $value) 
        {
          if ($value==$offer_id)
          {

          }
          else 
          {
              $data['offer_id'].=$value.',';
          }  
      }
      $data['offer_id']=substr($data['offer_id'], 0,-1);

      $this->admin_model->update_function('id', $id, 'offer_highlights', $data);

      $this->session->set_flashdata('Successfully','Deleted Successfully');

      redirect('admin/view_highlited_offer/'.$id, 'refresh');
  }


  public function add_highlighted_offer($id)
  {        
    $offer_id = $this->input->post('offer_id');


    $offer_id=implode(',',$offer_id);

        //echo '<pre>';print_r($offer_id);die();

    $highlighted_offer=$this->admin_model->select_with_where('*','id='.$id,'offer_highlights');

        //echo '<pre>';print_r($highlighted_offer);die();
    $data['offer_id']=NULL;

    $data['offer_id'].=$highlighted_offer[0]['offer_id'].','.$offer_id.',';

    $data['offer_id']=substr($data['offer_id'], 0,-1);

    $this->admin_model->update_function('id', $id, 'offer_highlights', $data);
    $this->session->set_flashdata('Successfully','Added Successfully');

    redirect('admin/view_highlited_offer/'.$id, 'refresh');

}





public function edit_new_offer($offer_id)
{

    $data['offer_for_list']=$this->admin_model->select_with_where('*','usertype=2 AND verify_status=1','user_login');

    $data['offer_type_list']=$this->admin_model->select_with_where('*','offer_st=1','offer_type');

    $data['cat_list']=$this->admin_model->select_with_where('*','menu_st!=2','catagory');


    $data['new_offer_list']=$this->admin_model->select_where_left_join_eight('*,main_offer.offer_id as moff_id,main_offer.offer_title as m_offer_title','main_offer','offer_type','offer_type.offer_id=main_offer.offer_title_type','voucher','main_offer.offer_id=voucher.offer_id','utm_ofr','main_offer.offer_id=utm_ofr.offer_id','offer_sms','main_offer.offer_id=offer_sms.offer_id','online_deal','main_offer.offer_id=online_deal.offer_id','direct_sale','main_offer.offer_id=direct_sale.offer_id','custom_ofr','main_offer.offer_id=custom_ofr.offer_id','tag','tag.tag_title=main_offer.tags','main_offer.offer_id='.$offer_id);

    $data['tag_list']=$this->admin_model->select_all('tag');

    $data['tag'] = $data['new_offer_list'][0]['tags'];
    $data['tag'] = explode(',', $data['tag']);

        //echo '<pre>';print_r($data['tag_list']);die();
       // echo '<pre>';print_r($data['new_offer_list']);die();




    $this->load->view('offer/main_offer/edit_offer',$data);

}


public function update_new_offer_post($offer_id)
{
    $data['post_by'] = $this->input->post('post_by');
    $data['offer_from'] = $this->input->post('offer_from'); 
    $data['offer_title'] = $this->input->post('offer_title'); 
    $data['offer_title_type'] = $this->input->post('offer_title_type');
    $data['sdate'] = date("Y-m-d", strtotime($this->input->post('sdate')));

    $data['of_post_date'] = date("Y-m-d", strtotime($this->input->post('of_post_date')));

    if (($this->input->post('exp_type')==0))
    {
        $data['edate'] = '0000-00-00';
        $data['show_expire_date'] = '0000-00-00';  
    }
    else 
    {
      $data['edate']=date("Y-m-d", strtotime($this->input->post('edate')));
      $data['show_expire_date'] = date("Y-m-d", strtotime($this->input->post('show_expire_date')));  
  }

  $data['offer_store_type'] = date("Y-m-d", strtotime($this->input->post('offer_store_type')));

  $data['offer_store_type'] = $this->input->post('offer_store_type');
  $data['is_hot'] = $this->input->post('hot_status');
  $data['discount'] = $this->input->post('dvalue');
  $data['inner_discount'] = $this->input->post('inner_discount'); 
  $data['of_post_date'] = date("Y-m-d", strtotime($this->input->post('of_post_date')));

  $data['offer_desc'] = $this->input->post('offer_desc');
  $data['offer_terms'] = $this->input->post('offer_terms');
  $data['catagories'] = $this->input->post('catagories');

  $data['climit'] = $this->input->post('limit');

  if ($data['climit']==1) {

    $data['climitamnt']=$this->input->post('coupon_total');
}
else{
    $data['climitamnt']=0;
}


$data['ctype'] = $this->input->post('type');
$data['target_people'] = $this->input->post('target_people_normal');

$data['tags'] = array();
$tags         = $this->input->post('tags');
$data['tags'] = implode(',', $tags);


$data['status'] = 1;
$data['cdate'] = date('Y-m-d H:i:s');


$this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data);


$data_pro_name['profile_name'] = preg_replace('/[ ,]+/', '_', trim($data['offer_title']));
$data_pro_name['profile_name'] = $data_pro_name['profile_name'] . '_' . $offer_id;
$data_pro_name['profile_name'] = str_replace("'", '', $data_pro_name['profile_name']);
$data_pro_name['profile_name'] = str_replace("/", '_', $data_pro_name['profile_name']);
$data_pro_name['profile_name'] = str_replace("@", '_', $data_pro_name['profile_name']);


$this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_pro_name);


$pre_image_name = $this->input->post('pre_img');
$path_to_file = 'uploads/offer/'.$pre_image_name;

if ($_FILES["file"]['name']) {
    // if (unlink($path_to_file)) {}
    $i_ext       = explode('.', $_FILES['file']['name']);
    $target_path = $data_pro_name['profile_name'].'.'.end($i_ext);
    $size        = getimagesize($_FILES['file']['tmp_name']);

    if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/offer/' . $target_path)) {
        $data_img['offer_featured_file'] = $target_path;
    }

    $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_img);

    if ($size[0] == 1200 || $size[1] == 500) {

    } else {
                $imageWidth = 1200; //Contains the Width of the Image
                
                $imageHeight = 500;
                
                $this->resize($imageWidth, $imageHeight, "uploads/offer/" . $target_path, "uploads/offer/" . $target_path);
            }
        }




        if ($data['ctype']=="buy_it") {

            $data_v['offer_id']=$offer_id;
            $data_v['voucher_price']=$this->input->post('coupon_price');
            $data_v['voucher_code']=$this->input->post('coupon_code');
            $data_v['add_date'] = date('Y-m-d H:i:s');

            $voucher_id = $this->admin_model->insert_ret('voucher', $data_v);


            $data_vtype['ctype'] = 1;
            $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_vtype);
        }

        elseif ($data['ctype']=="get_code") {

            $data_u['offer_id']=$offer_id;
            $data_u['utm_code']=$this->input->post('utm_code');
            $data_u['utm_link']=$this->input->post('ref_link');
            $data_u['add_date'] = date('Y-m-d H:i:s');

            $online_deal_id = $this->admin_model->insert_ret('utm_ofr', $data_u);

            $data_gtype['ctype'] = 2;
            $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_gtype);
        }


        elseif ($data['ctype']=="get_sms") {

            $data_s['offer_id']=$offer_id;
            $data_s['sms']=$this->input->post('sms');
            $data_s['add_date'] = date('Y-m-d H:i:s');

            $offer_sms_id = $this->admin_model->insert_ret('offer_sms', $data_s);

            $data_stype['ctype'] = 3;
            $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_stype);
        }

        elseif ($data['ctype']=="online_deal") {

            $data_o['offer_id']=$offer_id;
            $data_o['ref_link']=$this->input->post('ref_link');
            $data_o['add_date'] = date('Y-m-d H:i:s');

            $online_deal_id = $this->admin_model->insert_ret('online_deal', $data_o);

            $data_otype['ctype'] = 4;
            $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_otype);
        }
        elseif ($data['ctype']=="activate_deal") {

                // $data_a['offer_id']=$offer_id;
                // $data_a['utm_code']=$this->input->post('utm_code');
                // $data_a['utm_link']=$this->input->post('utm_link');
                // $data_a['add_date'] = date('Y-m-d H:i:s');

                // $online_deal_id = $this->admin_model->insert_ret('utm_ofr', $data_a);

            $data_actype['ctype'] = 5;
            $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_actype);
        }


        elseif ($data['ctype']=="direct_sale") {

            $data_d['offer_id']=$offer_id;
            $data_d['sale_price']=$this->input->post('price');
            $data_d['add_date'] = date('Y-m-d H:i:s');

            $this->admin_model->update_function('offer_id',$offer_id,'direct_sale', $data_d);

            $data_dtype['ctype'] = 6;
            $data_dtype['price'] = $this->input->post('price');;
            $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_dtype);
        }




        elseif ($data['ctype']=="custom_link") {

            $data_c['offer_id']=$offer_id;

            $offer_title=$this->input->post('offer_title');
            $data_c['offer_title']='';
            $offer_values=$this->input->post('valu');
            $data_c['offer_values']='';


            foreach ($offer_id as $key => $val) 
            {
               $data_c['offer_id']=$val;
               $data_c['offer_title']=$offer_title[$key];

               $data_c['offer_values']=$offer_values[$key];

               $data_c['offer_add_date']=date('Y-m-d H:i:s');

               $online_deal_id = $this->admin_model->insert_ret('custom_ofr', $data_c);
           }

           $data_ctype['ctype'] = 7;
           $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data_ctype);
       }

       else{}


          $this->session->set_flashdata('Successfully','Offer Updated Successfully');


      redirect('admin/new_offer_list', 'refresh');

  }

  public function delete_new_offer($offer_id)
  {
    $data['status'] = 2;
    $this->admin_model->update_function('offer_id', $offer_id, 'main_offer', $data);

    $this->session->set_flashdata('Successfully','Deleted Successfully');

    redirect('admin/new_offer_list', 'refresh');
}


public function offer_title_type_list()
{

    $data['offer_title_type_list']=$this->admin_model->select_all('ctype_colour');

       //echo "<pre>";print_r($data['offer_title_type_list']);die();

    $this->load->view('offer/main_offer/offer_type_title_list', $data);
}  

public function edit_offer_title_type($id)
{

    $data['offer_title_type_list']=$this->admin_model->select_with_where('*','id='.$id,'ctype_colour');

    $data['offer_title_type']=$this->admin_model->select_all('ctype_colour');

      // echo "<pre>";print_r($data['offer_title_type_list']);die();

    $this->load->view('offer/main_offer/edit_offer_type_title_list', $data);
}

public function update_offer_title_type_list($id)
{
    $data['name'] = $this->input->post('name');
    $data['string_name'] = $this->input->post('string_name');
    $data['color_code'] = '#'.$this->input->post('color_code');


    $this->admin_model->update_function('id',$id,'ctype_colour', $data);

    $this->session->set_flashdata('Successfully','Updated Successfully');

    redirect('admin/offer_title_type_list', 'refresh');

}


public function active_offer_list()
{

    $current_date=date('Y-m-d');

    $data['active_offer_list']=$this->admin_model->select_where_join('*,main_offer.offer_id as moff_id,offer_type.offer_id as oo_id','main_offer','offer_type','offer_type.offer_id=main_offer.offer_title_type','status!=2 AND edate>="'.$current_date.'"');

     //echo "<pre>";print_r($data['active_offer_list']);die();

    $this->load->view('offer/main_offer/active_offer_list', $data);

}

public function upcoming_offer_list()
{

  $current_date=date('Y-m-d');

  $data['upcoming_offer_list']=$this->admin_model->select_where_join('*,main_offer.offer_id as moff_id,offer_type.offer_id as oo_id','main_offer','offer_type','offer_type.offer_id=main_offer.offer_title_type','sdate>="'.$current_date.'"');

       //echo "<pre>";print_r($data['upcoming_offer_list']);die();

  $this->load->view('offer/main_offer/upcoming_offer_list', $data);
}

public function expired_offer_list()
{
   $current_date=date('Y-m-d');

   $data['expired_offer_list']=$this->admin_model->select_where_join('*,main_offer.offer_id as moff_id,offer_type.offer_id as oo_id','main_offer','offer_type','offer_type.offer_id=main_offer.offer_title_type','edate<"'.$current_date.'"');

      // echo "<pre>";print_r($data['expired_offer_list']);die();

   $this->load->view('offer/main_offer/expired_offer_list', $data);
} 

public function deleted_offer_list()
{

    $data['deleted_offer_list']=$this->admin_model->select_where_join('*,main_offer.offer_id as moff_id,offer_type.offer_id as oo_id','main_offer','offer_type','offer_type.offer_id=main_offer.offer_title_type','status=2');

       //echo "<pre>";print_r($data['deleted_offer_list']);die();

    $this->load->view('offer/main_offer/deleted_offer_list', $data);
}



    //////////////////////********  New Offer Ends *******///////////////////////////////


    /////////////////////********  Tag Starts *******///////////////////////////////

public function add_tag()
{

    $this->load->view('offer/tag/add_tag');

}



public function add_tag_post()
{
    $data['tag_title'] = $this->input->post('tag_title'); 

    $data['tag_add_date'] = date('Y-m-d H:i:s');

    $tag_id = $this->admin_model->insert_ret('tag', $data);

    $this->session->set_flashdata('Successfully','Added Successfully');

    redirect('admin/add_new_offer', 'refresh');

}


    ///////////////////********  Tag Ends *******//////////////////////////////////


     ///////////////////////********  Category Start *******////////////////////////////////


public function add_category()
{
    $data['catagory_list']=$this->admin_model->select_with_where('*','menu_st!=2','catagory');

        //echo '<pre>';print_r($data['catagory_list']);die();

    $this->load->view('category/add_category',$data);

}



public function add_category_post()
{
    $data['catagory_title'] = $this->input->post('catagory_title');
    $data['menu_st'] = $this->input->post('menu_st');
    $data['featured_cat_st'] = $this->input->post('featured_cat_st');
    $data['front_page_show_st'] = $this->input->post('front_page_show_st');
    $data['top_menu_st'] = $this->input->post('top_menu_st');
    $data['top_menu_sl'] = $this->input->post('top_menu_sl');
    $data['add_by'] = $this->input->post('add_by');
    $data['add_date'] = date('Y-m-d H:i:s');

    $category_id = $this->admin_model->insert_ret('catagory', $data);

    if ($_FILES['file']['name']) {
        $i_ext       = explode('.', $_FILES['file']['name']);
        $target_path = 'offer_' . $category_id . '.' . end($i_ext);
        $size        = getimagesize($_FILES['file']['tmp_name']);
        if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/category/' . $target_path)) {
            $data_img['catagory_pic'] = $target_path;
        }

        $this->admin_model->update_function('catagory_id', $category_id, 'catagory', $data_img);

        if ($size[0] == 1200 || $size[1] == 500) {

        } else {
                    $imageWidth = 1200; //Contains the Width of the Image
                    
                    $imageHeight = 500;
                    
                    $this->resize($imageWidth, $imageHeight, "uploads/category/" . $target_path, "uploads/category/" . $target_path);
                }
                
            }


            $this->session->set_flashdata('Successfully','Category Added Successfully');

            redirect('admin/add_category', 'refresh');

        }

        public function edit_category($category_id)
        {
            $data['category_info']=$this->admin_model->select_with_where('*','catagory_id='.$category_id,'catagory');

            $data['catagory_list']=$this->admin_model->select_all('catagory');

        //echo '<pre>';print_r($data['category_info']);die();

            $this->load->view('category/edit_category',$data);

        }


        public function update_category_post($category_id)
        {
            $data['catagory_title'] = $this->input->post('catagory_title');
            $data['menu_st'] = $this->input->post('menu_st');
            $data['featured_cat_st'] = $this->input->post('featured_cat_st');
            $data['front_page_show_st'] = $this->input->post('front_page_show_st');
            $data['top_menu_st'] = $this->input->post('top_menu_st');
            $data['top_menu_sl'] = $this->input->post('top_menu_sl');


            $this->admin_model->update_function('catagory_id',$category_id,'catagory', $data);

            $mt=$data['catagory_title']."_".$category_id;
            $pname= preg_replace('/[ ,]+/', '_', trim($mt));
            $pname= str_replace('/', '_', $pname);
            $pname= str_replace('.', '_', $pname);


            $pre_image_name = $this->input->post('pre_img');
            $path_to_file = 'uploads/category/'.$pre_image_name;

            if ($_FILES["file"]['name']) {
                if (unlink($path_to_file)){}
                    $i_ext       = explode('.', $_FILES['file']['name']);
                $target_path = $pname.'.'.end($i_ext);
                $size        = getimagesize($_FILES['file']['tmp_name']);

                if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/category/' . $target_path)) {
                    $data_img['catagory_pic'] = $target_path;
                }

                $this->admin_model->update_function('catagory_id',$category_id,'catagory', $data);

                if ($size[0] == 1200 || $size[1] == 500) {

                } else {
                $imageWidth = 1200; //Contains the Width of the Image
                
                $imageHeight = 500;
                
                $this->resize($imageWidth, $imageHeight, "uploads/category/" . $target_path, "uploads/category/" . $target_path);
            }
        }

        $this->session->set_flashdata('Successfully','Updated Successfully');
        
        redirect('admin/add_category', 'refresh');
        
    }

    public function delete_category($category_id)
    {
        $data['menu_st'] = 2;
        $this->admin_model->update_function('catagory_id', $category_id, 'catagory', $data);
        
        $this->session->set_flashdata('Successfully','Deleted Successfully');
        
        redirect('admin/add_category', 'refresh');
    }

    public function show_cat_front_page()
    {
        $front_page_show_st = $this->input->post('front_page_show_st');

        $get_cat = count($this->admin_model->select_with_where('*','front_page_show_st='.$front_page_show_st,'catagory'));
        
       //echo '<pre>';print_r($get_cat);

        if($get_cat>=5)
        {
            echo 0;

        }

        else
        {
            echo 1;
        }



    }



    ///////////////////////********  Category End *******////////////////////////////////


    /////////////////////////////********Admin Start*******//////////////////////////////
    

    public function add_admin()
    {
        $this->load->view('user/add_admin');
        
    }

    public function save_admin()
    {
        $data['email'] = $this->input->post('email'); 
        $data['username'] = $this->input->post('name'); 
        $data['contact'] = $this->input->post('contact');
        $data['usertype'] =1;
        $data['verify_status'] =1;
        $data['add_date'] =date('Y-m-d h:i:a');
        $password = $this->encryptIt($this->input->post('password')); 
        $con_password = $this->encryptIt($this->input->post('conpass'));

        $data['password']=$password;

        if ($password==$con_password) {


            $loginid=$this->admin_model->insert_ret('user_login',$data);


            //below for profile name update 

            $mt=$data['username']."_".$loginid;
            $pname= preg_replace('/[ ,]+/', '_', trim($mt));
            $pname= str_replace('/', '_', $pname);
            $pname= str_replace('.', '_', $pname);
            $name['name']=$pname;
            $this->admin_model->update_function('loginid', $loginid, 'user_login',$name);

            //below part for image upload

            if ($_FILES['files']['name'])
            {
                $i_ext       = explode('.',$_FILES['files']['name']);
                $target_path = 'admin' . $loginid . '.' . end($i_ext);
                $size        = getimagesize($_FILES['files']['tmp_name']);
                if (move_uploaded_file($_FILES['files']['tmp_name'], 'uploads/user/' . $target_path)) {
                    $data_img['profile_pic'] = $target_path;
                }

                    // $this->admin_model->insert_ret('user_login',$data);
                $this->admin_model->update_function('loginid', $loginid, 'user_login', $data_img);

                if ($size[0] == 1200 || $size[1] == 500) {

                } else {
                        $imageWidth = 1200; //Contains the Width of the Image
                        
                        $imageHeight = 500;
                        
                        $this->resize($imageWidth, $imageHeight, "uploads/user/" . $target_path, "uploads/user/" . $target_path);
                    }
                }


                $this->session->set_flashdata('Successfully','Added Successfully');

                redirect('admin/admin_list', 'refresh');
            }
            else{

                $this->session->set_flashdata('Error','Password Does Not Match');

                redirect('admin/add_admin', 'refresh');
            }


        }

        public function admin_list()
        {

            $data['merchant']=$this->admin_model->select_with_where('*','usertype=1 and verify_status=1','user_login');

        //echo '<pre>';print_r($data['merchant']);die();

            $this->load->view('user/admin_list',$data);
        }


        public function edit_admin_info($loginid)
        {


            $data['user_info']=$this->admin_model->select_with_where('*','loginid='.$loginid,'user_login');

        //echo '<pre>';print_r($data['user_info']);die();

            $this->load->view('user/edit_admin',$data);

        }


        public function update_admin_info($loginid)
        {
            $data['email'] = $this->input->post('email'); 
            $data['username'] = $this->input->post('username'); 
            $data['contact'] = $this->input->post('contact');


            
            $this->admin_model->update_function('loginid', $loginid, 'user_login',$data);


            // //below for profile name update 

            $mt=$data['username']."_".$loginid;
            $pname= preg_replace('/[ ,]+/', '_', trim($mt));
            $pname= str_replace('/', '_', $pname);
            $pname= str_replace('.', '_', $pname);
            $name['name']=$pname;
            $this->admin_model->update_function('loginid', $loginid, 'user_login',$name);

            //below part for image upload

            $pre_image_name = $this->input->post('pre_img');
            $path_to_file = 'uploads/user/'.$pre_image_name;



            if ($_FILES["file"]['name']) {

                if (unlink($path_to_file)) {}

                    $i_ext       = explode('.', $_FILES['file']['name']);
                $target_path = $pname.'.'.end($i_ext);
                $size        = getimagesize($_FILES['file']['tmp_name']);
                
                if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/user/' . $target_path)) {
                    $data_img['profile_pic'] = $target_path;
                }
                
                $this->admin_model->update_function('loginid', $loginid, 'user_login',$data_img);
                
                if ($size[0] == 1200 || $size[1] == 500) {

                } else {
                    $imageWidth = 1200; //Contains the Width of the Image
                    
                    $imageHeight = 500;
                    
                    $this->resize($imageWidth, $imageHeight, "uploads/user/" . $target_path, "uploads/user/" . $target_path);
                }
            }


            $this->session->set_flashdata('Successfully','Added Successfully');

            redirect('admin/admin_list', 'refresh');



        }

        public function delete_admin($user_id)
        {
            $data['delete_status'] =1;
            $data['verify_status'] =0;

            $this->admin_model->update_function('loginid', $user_id, 'user_login', $data);

            $this->session->set_flashdata('Successfully','Deleted Successfully');

            redirect('admin/admin_list', 'refresh');
        }


    /////////////////////////********Admin End*******//////////////////////////////



   /////////////////////////********Merchant Start*******//////////////////////////////


        public function add_merchant()
        {
            $this->load->view('merchant/add_merchant');

        }

        public function save_merchant()
        {
            $data['email'] = $this->input->post('emailaddress');
            $data['username'] = $this->input->post('username');
            $data['is_feature']=$this->input->post('featured');
            $data['top_store']=$this->input->post('top_stores');
            $data['is_brand']=$this->input->post('is_brand');
            $data['contact']=$this->input->post('contact_no');
            $data['password']=$this->encryptIt($this->input->post('password'));
            $data['website']=$this->input->post('website');

            $data['usertype'] =2;
            $data['add_date'] =date('Y-m-d h:i:a');
            $data['verify_status'] =1;

            $data['is_publish']=$this->input->post('is_publish');


            $loginid=$this->admin_model->insert_ret('user_login',$data);

            $data_details['user_id']=$loginid;
            $merchant_details_id=$this->admin_model->insert_ret('marchant_details',$data_details);

        //below for profile name update 

            $mt=$data['username']."_".$loginid;
            $pname= preg_replace('/[ ,]+/', '_', trim($mt));
            $pname= str_replace('/', '_', $pname);
            $pname= str_replace('.', '_', $pname);
            $name['name']=$pname;

            $this->admin_model->update_function('loginid', $loginid, 'user_login',$name);

        //upper for profile name update 
        //echo $_FILES['files']['name'];

        //below part for image upload
            if ($_FILES['files']['name'])
            {
                $i_ext       = explode('.',$_FILES['files']['name']);
                $target_path = 'merchant' . $loginid . '.' . end($i_ext);
                $size        = getimagesize($_FILES['files']['tmp_name']);
                if (move_uploaded_file($_FILES['files']['tmp_name'], 'uploads/user/' . $target_path)) {
                    $data_img['profile_pic'] = $target_path;
                }
                

                $this->admin_model->update_function('loginid', $loginid, 'user_login', $data_img);
                
                if ($size[0] == 1200 || $size[1] == 500) {

                } else {
                    $imageWidth = 1200; //Contains the Width of the Image
                    
                    $imageHeight = 500;
                    
                    $this->resize($imageWidth, $imageHeight, "uploads/user/" . $target_path, "uploads/user/" . $target_path);
                }
            }




        //upper part for image upload


        // Cover image
            if ($_FILES['cover_image']['name'])
            {
                $i_ext       = explode('.',$_FILES['cover_image']['name']);
                $target_path = 'merchant_cover' . $loginid . '.' . end($i_ext);
                $size        = getimagesize($_FILES['cover_image']['tmp_name']);
                if (move_uploaded_file($_FILES['cover_image']['tmp_name'], 'uploads/Merchant/' . $target_path)) {
                    $data_img['cover_image'] = $target_path;
                }
                

                $this->admin_model->update_function('loginid', $loginid, 'user_login', $data_img);
                
                if ($size[0] == 1200 || $size[1] == 500) {

                } else {
                    $imageWidth = 1200; //Contains the Width of the Image
                    
                    $imageHeight = 500;
                    
                    $this->resize($imageWidth, $imageHeight, "uploads/user/" . $target_path, "uploads/user/" . $target_path);
                }
            } 
            $this->session->set_flashdata('Successfully','New Merchant Successfully Added');

            redirect('admin/add_merchant','refresh');

        }

        public function merchant_list()
        {

            $data['merchant']=$this->admin_model->select_where_left_join('*,marchant_details.id as md_id','marchant_details','user_login','user_login.loginid=marchant_details.user_id','usertype=2 and verify_status=1');



        //echo '<pre>';print_r($data['merchant']);die();

            $this->load->view('merchant/merchant_list',$data);
        }


        public function edit_merchant_info($loginid)
        {


            $data['merchant_info']=$this->admin_model->select_with_where('*','loginid='.$loginid,'user_login');

        //echo '<pre>';print_r($data['merchant_info']);die();

            $this->load->view('merchant/edit_merchant',$data);

        }


        public function update_merchant_info($loginid)
        {
            $data['email'] = $this->input->post('email'); 
            $data['username'] = $this->input->post('username');
            // $data['show_on_web']=$this->input->post('featured');
            $data['is_feature']=$this->input->post('featured');
            $data['top_store']=$this->input->post('top_stores');
            $data['is_brand']=$this->input->post('is_brand');
            $data['is_publish']=$this->input->post('is_publish');
            $data['contact']=$this->input->post('contact_no');
            $data['website']=$this->input->post('website');

            // "<pre>";print_r($data);die();


            $this->admin_model->update_function('loginid', $loginid, 'user_login', $data);


            $mt=$data['username']."_".$loginid;
            $pname= preg_replace('/[ ,]+/', '_', trim($mt));
            $pname= str_replace('/', '_', $pname);
            $pname= str_replace('.', '_', $pname);
            $name['name']=$pname;
            $this->admin_model->update_function('loginid', $loginid, 'user_login',$name);


            $pre_image_name = $this->input->post('pre_img');

            $path_to_file = 'uploads/user/'.$pre_image_name;

        //echo '<pre>';print_r($path_to_file);die();
            if ($_FILES["file"]['name']) 
            {
                if (unlink($path_to_file)){}

                    $i_ext       = explode('.', $_FILES['file']['name']);
                $target_path = $pname.'.'.end($i_ext);
                $size        = getimagesize($_FILES['file']['tmp_name']);
                
                if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/user/' . $target_path)) {
                    $data_img['profile_pic'] = $target_path;
                }
                
                $this->admin_model->update_function('loginid', $loginid, 'user_login',$data_img);

                // echo '<pre>';print_r($data_img);die();
                
                if ($size[0] == 1200 || $size[1] == 500) {

                } else {
                    $imageWidth = 1200; //Contains the Width of the Image
                    
                    $imageHeight = 500;
                    
                    $this->resize($imageWidth, $imageHeight, "uploads/user/" . $target_path, "uploads/user/" . $target_path);
                }


            }


            // Cover image
            if ($_FILES['cover_image']['name'])
            {
                $i_ext       = explode('.',$_FILES['cover_image']['name']);
                $target_path = 'merchant_cover' . $loginid . '.' . end($i_ext);
                $size        = getimagesize($_FILES['cover_image']['tmp_name']);
                if (move_uploaded_file($_FILES['cover_image']['tmp_name'], 'uploads/Merchant/' . $target_path)) {
                    $data_img['cover_image'] = $target_path;
                }
                

                $this->admin_model->update_function('loginid', $loginid, 'user_login', $data_img);
                
                if ($size[0] == 1200 || $size[1] == 500) {

                } else {
                    $imageWidth = 1200; //Contains the Width of the Image
                    
                    $imageHeight = 500;
                    
                    $this->resize($imageWidth, $imageHeight, "uploads/user/" . $target_path, "uploads/user/" . $target_path);
                }
            } 


            $this->session->set_flashdata('Successfully','Updated Successfully');

            redirect('admin/merchant_list', 'refresh');

        }

        public function delete_merchant($loginid)
        {
            $exist_offer=count($this->admin_model->select_where_left_join('*','main_offer','user_login','user_login.loginid=main_offer.offer_from','usertype=2 AND verify_status=1 AND user_login.loginid='.$loginid));

       // echo '<pre>';print_r($exist_offer);die();


            if ($exist_offer==0) 
            {
                $data['delete_status'] =1;
                $data['verify_status'] =0;

                $this->admin_model->update_function('loginid', $loginid, 'user_login', $data);

                $this->session->set_flashdata('Successfully','Deleted Successfully');

                redirect('admin/merchant_list', 'refresh');

            }
            else
            {
               $this->session->set_flashdata('Error','You Cannot Delete this Merchant'); 
               redirect('admin/merchant_list', 'refresh');
           }

       }





       public function add_merchant_details($id)
       {

        $data['division_list']=$this->admin_model->select_all_name_ascending('name','divisions');
        $data['district_list']=$this->admin_model->select_all_name_ascending('name','districts');

        $data['area_list']=$this->admin_model->select_all_name_ascending('area_title','area');

        $data['merchant_info']=$this->admin_model->select_where_left_join('*,marchant_details.id as md_id','marchant_details','user_login','user_login.loginid=marchant_details.user_id','marchant_details.user_id='.$id);

       // echo '<pre>';print_r($data['merchant_info']);die();

        $this->load->view('merchant/add_merchant_details',$data);
        
    }

    public function save_merchant_details($id)
    {

        $data['profile_headline'] = $this->input->post('profile_headline'); 
        $data['ad_banner_headline'] = $this->input->post('ad_banner_headline'); 
        $data['map_long'] = $this->input->post('map_long'); 
        $data['map_lati'] = $this->input->post('map_lati'); 
        $data['facebook_link'] = $this->input->post('facebook_link'); 
        $data['youtube_link'] = $this->input->post('youtube_link'); 
        $data['twitter_link'] = $this->input->post('twitter_link'); 
        $data['linkedin_link'] = $this->input->post('linkedin_link'); 
        $data['instagram_link'] = $this->input->post('instagram_link'); 
        $data['support_email'] = $this->input->post('support_email'); 
        $data['division_id'] = $this->input->post('division_id'); 
        $data['district_id'] = $this->input->post('district_id'); 
        $data['area_id'] = $this->input->post('area_id'); 
        $data['address'] = $this->input->post('address'); 
        $data['about_us'] = $this->input->post('about_us'); 
        

        $this->admin_model->update_function('id', $id, 'marchant_details', $data);

        // if ($_FILES['file']['name']) {
        //         $i_ext       = explode('.', $_FILES['file']['name']);
        //         $target_path = 'Marchant_Featured_Image' . $id . '.' . end($i_ext);
        //         $size        = getimagesize($_FILES['file']['tmp_name']);
        //         if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/Merchant/' . $target_path)) {
        //             $data_img['profile_pic'] = $target_path;
        //         }

        //         $this->admin_model->update_function('id', $id, 'marchant_details', $data_img);

        //         if ($size[0] == 1200 || $size[1] == 500) {

        //         } else {
        //             $imageWidth = 1200; //Contains the Width of the Image

        //             $imageHeight = 500;

        //             $this->resize($imageWidth, $imageHeight, "uploads/Merchant/" . $target_path, "uploads/Merchant/" . $target_path);
        //         }

        //     }

        $this->session->set_flashdata('Successfully','Added Successfully');
        
        redirect('admin/merchant_list', 'refresh');
        
    }


    
    /////////////////////////********Merchant End*******//////////////////////////////



    /////////////////////////********User Start*******//////////////////////////////
    

    public function add_user()
    {
        $this->load->view('user/add_user');
        
    }

    public function save_user()
    {
        $data['email'] = $this->input->post('email'); 
        $data['username'] = $this->input->post('name'); 
        $data['contact'] = $this->input->post('contact');
        $data['usertype'] =3;
        $data['verify_status'] =1;
        $data['add_date'] =date('Y-m-d h:i:a');
        $password = $this->encryptIt($this->input->post('password')); 
        $con_password = $this->encryptIt($this->input->post('conpass'));

        $data['password']=$password;

        if ($password==$con_password) {


            $loginid=$this->admin_model->insert_ret('user_login',$data);


            //below for profile name update 

            $mt=$data['username']."_".$loginid;
            $pname= preg_replace('/[ ,]+/', '_', trim($mt));
            $pname= str_replace('/', '_', $pname);
            $pname= str_replace('.', '_', $pname);
            $name['name']=$pname;
            $this->admin_model->update_function('loginid', $loginid, 'user_login',$name);

            //below part for image upload

            if ($_FILES['files']['name'])
            {
                $i_ext       = explode('.',$_FILES['files']['name']);
                $target_path = 'user' . $loginid . '.' . end($i_ext);
                $size        = getimagesize($_FILES['files']['tmp_name']);
                if (move_uploaded_file($_FILES['files']['tmp_name'], 'uploads/user/' . $target_path)) {
                    $data_img['profile_pic'] = $target_path;
                }

                    // $this->admin_model->insert_ret('user_login',$data);
                $this->admin_model->update_function('loginid', $loginid, 'user_login', $data_img);

                if ($size[0] == 1200 || $size[1] == 500) {

                } else {
                        $imageWidth = 1200; //Contains the Width of the Image
                        
                        $imageHeight = 500;
                        
                        $this->resize($imageWidth, $imageHeight, "uploads/user/" . $target_path, "uploads/user/" . $target_path);
                    }
                }

                $user_details['user_id']=$loginid;
                $loginid=$this->admin_model->insert_ret('user_details',$user_details);


                $this->session->set_flashdata('Successfully','Added Successfully');

                redirect('admin/user_list', 'refresh');
            }



            else{

                $this->session->set_flashdata('Error','Password Does Not Match');

                redirect('admin/add_user', 'refresh');
            }


        }

        public function user_list()
        {

            $data['merchant']=$this->admin_model->select_with_where('*','usertype=3 and verify_status=1','user_login');

        //echo '<pre>';print_r($data['merchant']);die();

            $this->load->view('user/user_list',$data);
        }


        public function edit_user_info($loginid)
        {


            $data['user_info']=$this->admin_model->select_with_where('*','loginid='.$loginid,'user_login');



            $this->load->view('user/edit_user',$data);

        }


        public function update_user_info($loginid)
        {
            $data['email'] = $this->input->post('email'); 
            $data['username'] = $this->input->post('username'); 
            $data['contact'] = $this->input->post('contact');



            
            $this->admin_model->update_function('loginid', $loginid, 'user_login',$data);

            
            //below for profile name update 

            $mt=$data['username']."_".$loginid;
            $pname= preg_replace('/[ ,]+/', '_', trim($mt));
            $pname= str_replace('/', '_', $pname);
            $pname= str_replace('.', '_', $pname);
            $name['name']=$pname;
            $this->admin_model->update_function('loginid', $loginid, 'user_login',$name);

            //below part for image upload

            $pre_image_name = $this->input->post('pre_img');

            $path_to_file = 'uploads/user/'.$pre_image_name;

            if ($_FILES["file"]['name']) {

               if (unlink($path_to_file)) {} 

                $i_ext       = explode('.', $_FILES['file']['name']);
            $target_path = $pname.'.'.end($i_ext);
            $size        = getimagesize($_FILES['file']['tmp_name']);

            if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/user/' . $target_path)) {
                $data_img['profile_pic'] = $target_path;
            }

            $this->admin_model->update_function('loginid', $loginid, 'user_login',$data_img);

            if ($size[0] == 1200 || $size[1] == 500) {

            } else {
                    $imageWidth = 1200; //Contains the Width of the Image
                    
                    $imageHeight = 500;
                    
                    $this->resize($imageWidth, $imageHeight, "uploads/user/" . $target_path, "uploads/user/" . $target_path);
                }
            }


            $this->session->set_flashdata('Successfully','Added Successfully');

            redirect('admin/user_list', 'refresh');


        }

        public function delete_user($user_id)
        {
            $data['delete_status'] =1;
            $data['verify_status'] =0;

            $this->admin_model->update_function('loginid', $user_id, 'user_login', $data);

            $this->session->set_flashdata('Successfully','Deleted Successfully');

            redirect('admin/user_list', 'refresh');
        }

        // Preference Start

        public function add_preference($value='')
        {
           $this->load->view('preference/add_preference'); 
       }

       public function add_preference_post($value='')
       {

         $data['preference_title'] = $this->input->post('preference_title');

         $data['created_at'] = date('Y-m-d H:i:s');

         $category_id = $this->admin_model->insert_ret('preference', $data);

         $this->session->set_flashdata('Successfully','Preferece Successfully Added');

         redirect("admin/add_preference");
     }

     public function preference_list($value='')
     {
         $data['preference_info']=$this->admin_model->select_all('preference');
         $this->load->view('preference/preference_list',$data);
     }

     public function preference_list_post($value='')
     {
        $this->load->view('preference/preference_list');
    }  

    public function edit_preference($id)
    {

     $data['preference_info']=$this->admin_model->select_with_where('*','id='.$id,'preference');


     $this->load->view('preference/edit_preference',$data);

 }


 public function update_preference($id)
 {
    $data['preference_title'] = $this->input->post('preference_title');

    $this->admin_model->update_function('id',$id,'preference', $data);

    $this->session->set_flashdata('Successfully','Preferece Updated Successfully');

    redirect('admin/preference_list', 'refresh');

} 


public function delete_preference($id)
{

    $this->admin_model->delete_function('preference', 'id', $id);

    $this->session->set_flashdata('Successfully','Division Deleted Successfully');
    

    redirect('admin/preference_list', 'refresh');
}

public function add_weekly_offer($value='')
{
    $data['main_offer_info']= $this->admin_model->select_with_where('*','status=1','main_offer');

    $data['weekly_offer_info']= $this->admin_model->select_with_where('offer_id','status=1','weekly_offer');
    // "<pre>";print_r(in_array(2, $data['weekly_offer_info']));die();

    $this->load->view('weekly_offer/add_weekly_offer',$data);
}

public function get_all_main_weekly_offer($value='')
{
    $data['weekly_offer_info']= $this->admin_model->select_with_where('offer_id','status=1','weekly_offer');

    $data['main_offer_info']= $this->admin_model->select_with_where('*','status=1','main_offer');
    echo json_encode($data);
}

public function update_weekly_offer($value='')
{
    $offer_id=$this->input->post('weekly_offer');

    $val['status']=2;

    $this->admin_model->update_function_all('weekly_offer', $val);

    for ($i=0; $i < count($offer_id) ; $i++) { 

        $data['offer_id']=$offer_id[$i];
        $data['operator_id']=$this->session->userdata('login_id');
        $data['created_at']=date('Y-m-d');


        
        $this->admin_model->insert('weekly_offer',$data);



    }

    redirect('admin/add_weekly_offer');
} 


    /////////////////////////********User End*******//////////////////////////////



public function get_district()
{
    $division_id=$this->input->post('division_id');
    $get_district=$this->admin_model->select_with_where('*', 'division_id='.$division_id, 'districts');

    echo "<option></option>";
    foreach ($get_district as $key => $value) {
        echo '<option value="'.$value["id"].'">'.$value["name"].' '.$value["bn_name"].'</option>';
    }
}

public function get_area()
{
    $district_id=$this->input->post('district_id');
    $get_area=$this->admin_model->select_with_where('*', 'district_id='.$district_id, 'area');

    echo "<option></option>";
    foreach ($get_area as $key => $value) {
        echo '<option value="'.$value["area_id"].'">'.$value["area_title"].'</option>';
    }
}






    ////////////////////********  General Function Starts *******/////////////////////////


function encryptIt($string)
{
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
    $secret_iv = 'This is my secret iv';
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    $output = base64_encode($output);
    return $output;
}


function decryptIt($string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    return $output;
}



public function resize($height, $width, $source, $destination)
{
    $this->load->library('image_lib');
    $config['image_library']        = 'gd2';
    $config['source_image']         = $source;
    $config['overwrite']            = TRUE;
    $image_config['quality']        = "100%";
    $image_config['maintain_ratio'] = FALSE;
    $config['height']               = $height;
    $config['width']                = $width;
        $config['new_image']            = $destination; //you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
    
    public function resize2($height, $width)
    {
        $this->load->library('image_lib');
        $config['image_library']        = 'gd2';
        $config['source_image']         = "uploads/watermark.png";
        $config['overwrite']            = TRUE;
        $image_config['quality']        = "100%";
        $image_config['maintain_ratio'] = FALSE;
        $config['height']               = $height;
        $config['width']                = $width;
        $config['new_image']            = "uploads/product/"; //you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
    
    
    
    private function set_upload_options($file_name, $folder_name)
    {
        //upload an image options
        $url = base_url();
        
        $config                  = array();
        $config['file_name']     = $file_name;
        $config['upload_path']   = 'uploads/' . $folder_name;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|svg|pdf';
        $config['max_size']      = '0';
        $config['overwrite']     = TRUE;
        
        return $config;
    }
    
    
    
    
    public function watermark_image($imageWidth, $imageHeight)
    {
        $file    = 'uploads/watermark.png';
        $newfile = 'uploads/watermark.png';
        
        if (!copy($file, $newfile)) {
            // echo "failed to copy $file...\n";
        } else {
            // echo "copied $file into $newfile\n";
        }
        
        //shell_exec("cp -r ".$file." ".$newfile);
        //echo $imageWidth.' '.$imageHeight;
        //die();
        $this->resize2($imageWidth, $imageHeight);
        
        
    }
    
    
    public function overlay($path)
    {
        $config['image_library']    = 'gd2';
        $config['source_image']     = 'uploads/product/' . $path;
        $config['wm_type']          = 'overlay';
        $config['wm_overlay_path']  = 'uploads/watermark.png'; //the overlay image
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $config['wm_padding']       = '0';
        // $config['wm_x_transp'] = '200';
        //$config['wm_y_transp'] = '20';
        $config['wm_opacity']       = '50';
        
        $this->image_lib->initialize($config);
        
        
        if (!$this->image_lib->watermark()) {
            echo $this->image_lib->display_errors();
        }
    }




}
