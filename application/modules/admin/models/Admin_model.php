<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    ////// Basic Model Function Starts ///////
    public function insert($table_name,$data)
    {
        $this->db->insert($table_name, $data);
    }

    public function insert_ret($tablename, $tabledata)
    {
        $this->db->insert($tablename, $tabledata);
        return $this->db->insert_id();
    }


    public function update_function($columnName, $columnVal, $tableName, $data)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->update($tableName, $data);
    }

     public function update_function_all($tableName, $data)
    {
        
        $this->db->update($tableName, $data);
    }

    public function update_function_with_multi_condition($tableName, $data, $cond)
    {
        $where = '( ' . $cond . ' )';
        $this->db->where($where);
        $this->db->update($tableName, $data);
    }


    public function delete_function_cond($tableName, $cond)
    {
        $where = '( ' . $cond . ' )';
        $this->db->where($where);
        $this->db->delete($tableName);
    }
    public function delete_function($tableName, $columnName, $columnVal)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->delete($tableName);
    }

    public function select_all($table_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function select_all_name_ascending($col_name,$table_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($col_name,'ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function select_all_decending($table_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by('created_at','DESC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


    public function select_with_where($selector, $condition, $tablename)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->result_array();
    }

      public function select_with_where_decending($selector, $condition, $tablename)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('created_at','DESC');
        $result = $this->db->get();
        return $result->result_array();
    } 

    public function select_with_where_decending_limit($selector, $condition, $tablename,$lim)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('created_at','DESC');
        $result = $this->db->get();
        return $result->result_array();
    } 

     public function select_join_where_limit($selector,$table_name,$join_table,$join_condition,$condition,$limit)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition);
        $this->db->where($condition);
        $this->db->order_by('created_at','DESC');
        $this->db->limit($limit);
        $result=$this->db->get();
        return $result->result_array();
    }


     function fav_products($offer_id)
        {
            $query = $this->db->query('SELECT * FROM user_details WHERE FIND_IN_SET('.$offer_id.',user_details.fav_product)');

            return $query->result_array();
        }


    function highlighted_products($id,$offer_id)
    {
        $query = $this->db->query('SELECT * FROM offer_highlights WHERE id='.$id.' AND FIND_IN_SET('.$offer_id.',offer_highlights.offer_id)');

        return $query->result_array();
    } 

    function user_favourite_offer($login_id,$offer_id)
    {
        $query = $this->db->query('SELECT * FROM user_login WHERE loginid='.$login_id.' AND FIND_IN_SET('.$offer_id.',user_login.fav_offer)');

        return $query->result_array();
    } 

    function user_favourite_store($login_id,$store_id)
    {
        $query = $this->db->query('SELECT * FROM user_login WHERE loginid='.$login_id.' AND FIND_IN_SET('.$store_id.',user_login.subscribed_store)');

        return $query->result_array();
    }

     function user_favourite_cat($login_id,$cat_id)
    {
        $query = $this->db->query('SELECT * FROM user_login WHERE loginid='.$login_id.' AND FIND_IN_SET('.$cat_id.',user_login.subscribed_category)');

        return $query->result_array();
    }
 

  public function order_by_last_row($selector, $condition, $tablename)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('id',"desc");
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function last_semester_info($selector,$tablename,$join_table,$join_condition,$condition)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $this->db->join($join_table,$join_condition,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('as_id',"desc");
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result_array();
    } 

    // public function last_semester_result($selector, $condition, $tablename)
    // {
    //     $this->db->select($selector);
    //     $this->db->from($tablename);
    //     $where = '(' . $condition . ')';
    //     $this->db->where($where);
    //     $this->db->order_by('sem_rslt_id',"desc");
    //     $this->db->limit(1);
    //     $result = $this->db->get();
    //     return $result->result_array();
    // } 

     public function order_by_last_row_where_left_join_six($selector,$tablename,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$condition)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $this->db->join($join_table5,$join_condition5,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('sem_rslt_id',"desc");
        $this->db->limit(1);
        $result = $this->db->get();
        return $result->result_array();
    } 


    public function get_email($email,$tablename){
        $this->db->select('*');
        $this->db->from($tablename);
        $this->db->where('email',$email);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_phone($phone,$tablename){
        $this->db->select('*');
        $this->db->from($tablename);
        $this->db->where('phone',$phone);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


 

    public function select_with_group_by_decending($selector, $tablename, $col_name,$group_by)
        {
            $this->db->select($selector);
            $this->db->from($tablename);
            $this->db->group_by($col_name);
            $this->db->order_by('created_at','DESC');
            $result = $this->db->get();
            return $result->result_array();
        }



     public function last_semester_result($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$condition,$col_name,$group_by)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');        
        $this->db->group_by($col_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('sem_rslt_id','ASC');
        $result=$this->db->get();
        return $result->result_array();
    }


        public function select_with_group_by($selector, $tablename, $col_name,$group_by)
        {
            $this->db->select($selector);
            $this->db->from($tablename);
            $this->db->group_by($col_name);
            $result = $this->db->get();
            return $result->result_array();
        }

        public function select_with_group_by_where($selector, $tablename, $col_name,$condition,$group_by)
        {
            $this->db->select($selector);
            $this->db->from($tablename);
            $this->db->group_by($col_name);
            $where = '(' . $condition . ')';
            $this->db->where($where);
            $result = $this->db->get();
            return $result->result_array();
        }

    public function select_order_with_where($selector, $condition, $tablename,$order_name,$order_by)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by($order_name,$order_by);
        $result = $this->db->get();
        return $result->result_array();
    }



    public function count_with_where($selector, $condition, $tablename)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->num_rows();
    }

    public function sum_with_where($selector, $condition,$col_name, $tablename)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->select_sum($col_name);
    }

    public function select_join($selector,$table_name,$join_table,$join_condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_left_join($selector,$table_name,$join_table,$join_condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $result=$this->db->get();
        return $result->result_array();
    }



    public function select_where_join($selector,$table_name,$join_table,$join_condition,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

   



    public function select_where_left_join($selector,$table_name,$join_table,$join_condition,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_where_left_join_group_by($selector,$table_name,$join_table,$join_condition,$condition,$col_name)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->group_by($col_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }


     public function select_where_left_join_two($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_where_left_join_two_group_by($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$condition,$col_name)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->group_by($col_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }


    // public function find_in_set_where_left_join_two($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1)
    // {
    //     $this->db->select($selector);
    //     $this->db->from($table_name);
    //     $this->db->join($join_table,$join_condition,'left');
    //     $this->db->join($join_table1,$join_condition1,'left');
    //     $this->db->where("FIND_IN_SET('','')");
    //     //$this->db->order_by($order_col,$order_action);
    //     $result=$this->db->get();
    //     return $result->result_array();
    // } 

       public function select_where_left_join_three($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }
     public function select_where_left_join_four_distinct($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $this->db->distinct();
        $result=$this->db->get();
        return $result->result_array();
    }

     public function select_where_left_join_four($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_where_left_join_four_group_by($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$condition,$col_name,$group_by)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');        
        $this->db->group_by($col_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result=$this->db->get();
        return $result->result_array();
    } 
    
    public function select_where_left_join_five($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }


    

    public function select_where_left_join_five_group_by($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$condition,$col_name,$group_by)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $this->db->group_by($col_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_where_left_join_six($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $this->db->join($join_table5,$join_condition5,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_where_left_join_two_sum($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$condition,$col_name)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->select_sum($col_name);
        return $result->result_array();
    }



    public function select_where_left_join_six_sum($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$condition,$col_name,$sum)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $this->db->join($join_table5,$join_condition5,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        $result->$this->db->select_sum('$col_name','$sum');
        return $result->result_array();
    }

    public function select_where_left_join_six_group_by($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$condition,$col_name,$group_by)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $this->db->join($join_table5,$join_condition5,'left');
        $this->db->group_by($col_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }


    public function select_where_left_join_seven($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$join_table6,$join_condition6,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $this->db->join($join_table5,$join_condition5,'left');
        $this->db->join($join_table6,$join_condition6,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_where_left_join_eight($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$join_table6,$join_condition6,$join_table7,$join_condition7,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $this->db->join($join_table5,$join_condition5,'left');
        $this->db->join($join_table6,$join_condition6,'left');
        $this->db->join($join_table7,$join_condition7,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

     public function select_where_left_join_nine($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$join_table6,$join_condition6,$join_table7,$join_condition7,$join_table8,$join_condition8,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $this->db->join($join_table5,$join_condition5,'left');
        $this->db->join($join_table6,$join_condition6,'left');
        $this->db->join($join_table7,$join_condition7,'left');
        $this->db->join($join_table8,$join_condition8,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_where_left_join_ten($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$join_table6,$join_condition6,$join_table7,$join_condition7,$join_table8,$join_condition8,$join_table9,$join_condition9,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $this->db->join($join_table5,$join_condition5,'left');
        $this->db->join($join_table6,$join_condition6,'left');
        $this->db->join($join_table7,$join_condition7,'left');
        $this->db->join($join_table8,$join_condition8,'left');
        $this->db->join($join_table9,$join_condition9,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_where_left_join_seven_group_by($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$join_table5,$join_condition5,$join_table6,$join_condition6,$condition,$col_name,$group_by)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $this->db->join($join_table5,$join_condition5,'left');
        $this->db->join($join_table6,$join_condition6,'left');
        $this->db->group_by($col_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

    


     public function select_where_two_join($selector,$table_name,$join_table,$join_condition,$join_table2,$join_condition2,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition);
        $this->db->join($join_table2,$join_condition2);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }


      public function select_where_join_order_by($selector,$table_name,$join_table,$join_condition,$condition,$order_col,$order_action)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

     public function get_merit($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);       
        $this->db->order_by('result_history.cgpa','DESC');
        $this->db->order_by('result_history.total_obtain_mark','DESC');
        $result = $this->db->get();
        return $result->result_array();
    }

     public function get_merit_archive($selector,$table_name,$join_table,$join_condition,$join_table1,$join_condition1,$join_table2,$join_condition2,$join_table3,$join_condition3,$join_table4,$join_condition4,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $this->db->join($join_table1,$join_condition1,'left');
        $this->db->join($join_table2,$join_condition2,'left');
        $this->db->join($join_table3,$join_condition3,'left');
        $this->db->join($join_table4,$join_condition4,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);       
        $this->db->order_by('result_archive.cgpa','DESC');
        $this->db->order_by('result_archive.total_avrg_mark','DESC');
        $result = $this->db->get();
        return $result->result_array();
    }

    ////// Basic Model Function End ///////


    public function exist_email($email)
    {
        $this->db->select('email');
        $this->db->from('login');
        $this->db->where('email',$email);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function get_last_product_code()
    {
        $this->db->select('p_code');
        $this->db->from('product');
        $this->db->order_by('p_id',"desc");
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

     public function get_last_sell_code()
    {
        $this->db->select('sell_code');
        $this->db->from('sell');
        $this->db->order_by('sell_id',"desc");
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


     public function get_last_buy_code()
    {
        $this->db->select('buy_code');
        $this->db->from('buy');
        $this->db->order_by('buy_id',"desc");
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_product_details($p_id)
    {
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('p_id',$p_id); 
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


    public function columns($database, $table)
    {
        //$query = "SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_DEFAULT, COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS  WHERE table_name = '$table'AND table_schema = '$database'";  
        $query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE table_name = '$table'
            AND table_schema = '$database'";    
        $result = $this->db->query($query) or die ("Schema Query Failed"); 
        $result=$result->result_array();
        return $result;
    }

    
    ///  For Union Two table
     function get_merged_result()
    {                   
        $this->db->select("name,id,phone_second as web");
       // $this->db->distinct();
        $this->db->from("registration");
        //$this->db->where_in("id",$model_ids);
        $this->db->get(); 
        $query1 = $this->db->last_query();

        $this->db->select("name,id,website as web");
       // $this->db->distinct();
        $this->db->from("company");
       // $this->db->where_in("id",$model_ids);

        $this->db->get(); 
        $query2 =  $this->db->last_query();
        $query = $this->db->query($query1." UNION ".$query2);

        return $query->result_array();
    }


    function get_subjects($department_id)
        {
            $query = $this->db->query('SELECT * FROM subject WHERE FIND_IN_SET('.$department_id.',subject.department_id) AND subject.status=1');

            return $query->result_array();
        }


    function semester_wise_result_per_std()
    {

        $query = $this->db->query('SELECT student.id as std_id,result.assign_semester_id,result.semester_no,sum(subject.credit) as total_credit,sum(result.total_term_mark) as sum_term_mark,((sum(result.gpa*subject.credit))/(sum(subject.credit))) as total_gpa_avg FROM student INNER JOIN result on result.student_id=student.id JOIN subject on subject.id=result.subject_id WHERE result.is_fail=0 group by result.student_id,result.assign_semester_id');

            return $query->result_array();
    }


    function student_total_complete_credit($department_id,$batch,$section)
    {
         $query = $this->db->query('SELECT sum(semester_wise_result.completed_credit) as total_std_credit,department.*,student.*,registration.name from semester_wise_result inner join student on student.id=semester_wise_result.student_id inner join login on login.id=student.login_id left join registration on registration.login_id=login.id left join department on department.id=student.department_id where login.status=1 AND login.type=4  AND student.grad_status=0 AND student.department_id='.$department_id.'  AND student.batch='.$batch.' AND student.section='.$section.' group by student_id');

            return $query->result_array();
    }


     public function setPageNumber($pageNumber) {
        $this->_pageNumber = $pageNumber;
    }
 
    public function setOffset($offset) {
        $this->_offset = $offset;
    }

    public function offer_with_pagination($condition,$limit,$start)
    {
        $this->db->limit($limit, $start);
        $this->db->select('*,main_offer.offer_id as moff_id,offer_type.offer_id as oo_id','main_offer');
        $this->db->from('main_offer');
        $this->db->join('offer_type','offer_type.offer_id=main_offer.offer_title_type');
        
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $query = $this->db->get();

        $result = $query->result_array();
        return $result;
    }


    // function std_total_cgpa($student_id)
    // {
    //      $query = $this->db->query('SELECT SUM(semester_wise_result.cgpa) as total_cgpa FROM student INNER JOIN semester_wise_result on semester_wise_result.student_id where semester_wise_result.student_id='.$student_id);

    //         return $query->result_array();
    // }



}
?>