<?php $this->load->view('admin/header'); ?>

  <body>
    <div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">

     <?php $this->load->view('admin/sidebar'); ?> 

        
        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="admin">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="admin/add_slider">Add Slider</a>
            </li>

          </ul>
          <div class="content-i">
            <div class="content-box">
              <div class="element-wrapper">
                <h6 class="element-header">
                 Slider List
                </h6>
                <div class="element-box">
                  <h5 class="form-header">
                      Slider List

                       <?php if ($this->session->flashdata('Successfully')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('Successfully'); ?>",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>

                    <?php if ($this->session->flashdata('Error')): ?>
                            <script>
                                swal({
                                    title: "Error",
                                    text: "<?php echo $this->session->flashdata('Error'); ?>",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'error'
                                });
                            </script>
                    <?php endif; ?>
                    
                  </h5>

                  <div class="table-responsive">
                    <table id="dataTable1" width="100%" class="table table-striped table-lightfont">
        					<thead>
        					<tr>
        					<th>Sl</th>
        					<th>Title</th>
        					<th class="text-center">Image </th>
                  <th>Slider Link</th>
        					<th class="text-center">Status</th>
        					<th class="text-center">Action</th>
        					
        				
        					</tr>
        					</thead>
        					<tfoot>
        					<tr>
        					<th>Sl</th>
        					<th>Title</th>
        					<th class="text-center">Image </th>
                  <th>Slider Link</th>
        					<th class="text-center">Status</th>
        					<th class="text-center">Action</th>
        					
				
        					</tr>
        					</tfoot>
					<tbody>
             <?php foreach ($slider_list as $key => $row) {	?>
                    <tr>
                        <td><?=($key+1);?></td>
                      
                        <td><?=$row['slidertitle'];?></td>
                                         
												<td class="text-center">
								            	<img class="table-image" height="50px" width="50px" src="uploads/slider/<?=$row['img'];?>" alt="">
								        </td>
                         <td><?=$row['slider_link'];?></td>
                         <td><?php 
                         if ($row['status']==1) 
                         {
                           echo 'Active';
                         }
                         elseif($row['status']==0)
                          {
                            echo 'Dective';
                          }
                          else
                            {
                              echo 'Block';
                            }?>
                       </td>

								            	<td>
                                <!-- <input type="checkbox" id="status_<?=$row['sliderid'];?>" data-size="mini" class="make-switch" <?php if($row['status']==1){echo 'checked';}?> onchange="status_change('<?=$row['sliderid'];?>')" data-on-text="<i class='fa fa-check'></i>" data-off-text="<i class='fa fa-times'></i>">&nbsp -->
                               
                                <?php if($row['status']==1){?>
                                   <input type="hidden" id="status_<?=$row['sliderid'];?>" value="0">
                                <button  class="btn btn-sm btn-danger" onclick="status_change('<?=$row['sliderid'];?>')">Deactive
                                </button>
                                <?php } else{?>
                                   <input type="hidden" id="status_<?=$row['sliderid'];?>" value="1">
                                <button id="status_<?=$row['sliderid'];?>" class="btn btn-sm btn-success" onclick="status_change('<?=$row['sliderid'];?>')">Active
                                </button>
                                  <?php } ?>
                                &nbsp;&nbsp;&nbsp;&nbsp;
					
                    				    <a href='admin/edit_slider/<?=$row['sliderid']?>'><i class='fa fa-pencil' Title='Edit'></i></a>
                    					&nbsp;&nbsp;&nbsp;&nbsp;
                    					<a href='admin/delete_slider/<?=$row['sliderid']?>/<?=$row['img'];?>'><i class='fa fa-trash-o' Title='Delete'></i></a>
                    					&nbsp;&nbsp;&nbsp;&nbsp;
                    					
					
					
				
                              </td>
                               
                            </tr>
                           
                           <?php } ?>


					</tbody>
					</table>
                  </div>
                </div>
              </div>


            </div>
          </div>
        </div>
      </div>

     <?php $this->load->view('admin/footer'); ?>

     <script type="text/javascript">

        function status_change(sliderid) 
        {
            var status=$('#status_'+sliderid).val();
            //alert(status);return;
            if(status==false)
            {
                status=0;
            }
            else{status=1;}
            
            $.ajax({
                    type: "post",
                    url: "<?php echo site_url('admin/update_slider_status');?>",
                    data : {sliderid:sliderid,status:status},                  
                    success : function(data)
                    {
                        location.reload();
                       // alert(msg);
                    }
                });  
        }
      </script>
   
  </body>
</html>
