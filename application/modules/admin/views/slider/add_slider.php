<?php $this->load->view('admin/header'); ?>

  <body>
    <div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">

     <?php $this->load->view('admin/sidebar'); ?> 

        
        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="admin">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="admin/slider">Slider List</a>
            </li>

          </ul>
          <div class="content-i">
            <div class="content-box"><div class="row">
  <div class="col-lg-12">
    <div class="element-wrapper">
      <h6 class="element-header">
        Add New Slider

	
	  <?php if ($this->session->flashdata('Successfully')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('Successfully'); ?>",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
      </h6>
      <div class="element-box">

      <form id="formValidate"  action="admin/add_slider_post" method="post" enctype="multipart/form-data">


		     <div class="form-group">
            <label for="">Slider Title<span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="slidertitle"  placeholder="Enter Slider Title" required="required" type="text">
         
          </div>

          <div class="form-group">
            <label for=""> Slider Title (Bangla) </label><input class="form-control" name="slidertitle_bang" placeholder="Enter Slider Title (Bangla)" type="text">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>

          <div class="form-group">
            <div class="form-group row">
            <label for="example-email-input" class="control-label col-md-3"> Slider Image  <span style="color:red;font-family:verdana">(*)</span></label>
           
             <div class="fileinput fileinput-new col-10" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;border: 1px solid; padding:1px;"><img src="back_asset/img_default.png" alt="Your Image" />
             
                </div>

                <div>
                  <span class="btn btn-info btn-file border"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>

                  <input type="file" name="file" required=""></span>
                  
                  <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                </div>
              </div>
           </div>
           </div>

           <div class="form-group">
            <label for=""> Slider Link </label><input class="form-control" name="slider_link" placeholder="Enter Slider Link" type="url">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>

          <div class="form-group">
            <label for=""> Slider Serial <span style="color:red;font-family:verdana">(*)</span> </label><input class="form-control" name="slider_sl" placeholder="Enter Slider Serial" type="number" required="">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>

          <div class="form-group">
            <label>Slider Description <span style="color:red;font-family:verdana">(*)</span></label><textarea class="form-control" name="description" rows="3" ></textarea>
        </div>
        <div class="form-group">
            <label>Slider Description (Bangla)</label><textarea class="form-control" name="description_bang" rows="3"></textarea>
        </div>

         
          
          <div class="form-buttons-w">          
          <input type="submit" name="submit" class="btn btn-primary" value="Add New">
          </div>




        </form>	
      </div>
    </div>
  </div>

</div>



            </div>
          </div>
        </div>
      </div>

     <?php $this->load->view('admin/footer'); ?>
   
  </body>
</html>
