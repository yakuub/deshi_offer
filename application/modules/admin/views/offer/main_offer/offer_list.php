<?php $this->load->view('admin/header'); ?>

  <body>
    <div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">

     <?php $this->load->view('admin/sidebar'); ?> 

                <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="index.html">Products</a>
            </li>
            <li class="breadcrumb-item">
              <span>Laptop with retina screen</span>
            </li>
          </ul>
          <div class="content-i">
            <div class="content-box">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Data Tables
                </h6>
                <div class="element-box">
                  <h5 class="form-header">
                   Offer List
                    <?php if ($this->session->flashdata('Successfully')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('Successfully'); ?>",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
                  </h5>

                  

                  <div class="form-buttons-w">

                <a onclick="deal_modal()" href="javascript:;" class="btn btn-sm btn-primary">
                    Deal Offer
                </a>

                </div> 

                <div class="form-buttons-w" style="margin-left: 20px">

                <div class="form-group row">

            
                  <label class="radio-inline">
                    <input type="radio" name="date_filter" id="this_week" value="0">&nbsp; This Week
                  </label>&nbsp;

                  <label class="radio-inline">
                    <input type="radio" name="date_filter" id="this_month" value="1"> &nbsp; This Month
                  </label>&nbsp;


                  <label class="radio-inline">
                  <input type="radio" name="date_filter" value="2">&nbsp;
                  </label>
                  <input class="form-control start_date" name="sdate" placeholder="Post Start Date" type="text" id="datepicker-example21" value=""> &nbsp;&nbsp;To &nbsp;&nbsp;

                  <input class="form-control end_date" name="edate" placeholder="Post Start Date" type="text" id="datepicker-example3" value=""> &nbsp;&nbsp;

                    <label for="">
                    <select class="form-control select2" name="type" id="type" name="offer_store_type">
                    <option value="all">All</option>
                    <?php foreach ($offer_type_list as $key => $row) { ?>
                      <option value="<?=$row['offer_id'];?>"><?=$row['offer_type_tilte'];?> </option>
                    <?php } ?>
                   
                   </select>
                    
                   <button onclick="offer_filter()" class="btn btn-sm btn-primary"> Filter</button>
                </div>
                     
              </div>

                <br>
                  <div class="table-responsive">

                    <table id="" width="100%" class="table table-striped table-lightfont">
                    <thead>
                    <tr>  
                    <th> <input type="checkbox" id="select_all" class="checkbox">Check All</th>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Featured Image</th>
                    <th>Start Date</th>
                    <th>End date</th>
                    <th>Action</th>

                    </tr>
                    </thead>


                    <tfoot>
                    <tr>
                   
                    <th></th>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Featured Image</th>
                    <th>Start Date</th>
                    <th>End date</th>
                    <th>Action</th>

                    </tr>
                    </tfoot>
                   
                    <tbody id="offer_div">
            
                        <?php $this->load->view('include/offer_filter_ajax'); ?>

                   </tbody>
               
                </table>

                  <span id="pagination">
                  <?php echo $page_links; ?>
                  </span>

                  </div>

                  
                  
                  <div class="modal" id="highlight_modal" role="dialog">
                  </div> 

                  <div id="loading_img_store_offer" style="display:none;position: fixed;z-index: 10;top: 35%;left: 50%;width: 5%;"><img style="width: 100%" src="front_assets/loading-spinner-blue.gif">     
                </div>
                  



                </div>
                </div>
              
              <div class="floated-chat-btn">
                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
              </div>
              <div class="floated-chat-w">
                <div class="floated-chat-i">
                  <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                  </div>
                  <div class="chat-head">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-messages">
                    <div class="message">
                      <div class="message-content">
                        Hi, how can I help you?
                      </div>
                    </div>
                    <div class="date-break">
                      Mon 10:20am
                    </div>
                    <div class="message">
                      <div class="message-content">
                        Hi, my name is Mike, I will be happy to assist you
                      </div>
                    </div>
                    <div class="message self">
                      <div class="message-content">
                        Hi, I tried ordering this product and it keeps showing me error code.
                      </div>
                    </div>
                  </div>
                  <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                      <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
     <?php $this->load->view('admin/footer'); ?>

       

        <script>
            
            function deal_modal() 
            {
               var offer_checked = [];


               $("input[name='offer_id[]']:checked").each(function ()
                {

                  offer_checked.push(parseInt($(this).val()));
                });

                //alert(offer_checked);

                $.ajax({
                    url: "<?php echo site_url('admin/add_product_highlights');?>",
                    type: "post",
                    data: {offer_checked:offer_checked,},
                    success: function(msg)
                    {
                      $('#highlight_modal').modal('show');
                      $("#highlight_modal").html(msg);

                    } 

                }); 
                //console.log(student_id);
            }
        </script>

        <script>

          function get_week_date() 
          {
              var curr = new Date; // get current date
              var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
              var last = first + 6; // last day is the first day + 6

              var firstday = new Date(curr.setDate(first)).toISOString();
              var lastday = new Date(curr.setDate(last)).toISOString();

              firstday=firstday.slice(0,-14);
              lastday=lastday.slice(0,-14);

              //console.log(firstday);
              //console.log(lastday);

              return [firstday,lastday];
             

              //get_appointment_data_ajax(firstday,lastday);
          }

            function get_month_date() 
          {
              var date = new Date();
              var firstDay = new Date(date.getFullYear(), date.getMonth(), 2);
              var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 1);

              firstday = firstDay.toISOString();
              lastday = lastDay.toISOString();

              firstday=firstday.slice(0,-14);
              lastday=lastday.slice(0,-14);

              return [firstday,lastday];

              //get_appointment_data_ajax(firstday,lastday);
          }


        </script>

        <script>
          
          

           function offer_filter() 
            {
              
               var date_filter=$("input[name='date_filter']:checked").val();
                var type = $("#type").val();
                var firstday;
                var lastday;

               $("#loading_img_store_offer").show();

               if (date_filter==0)
                {
                  var data=get_week_date();
                  firstday=data[0];
                  lastday=data[1];
                }
                else if (date_filter==1) 
                {
                  
                  var data=get_month_date();
                  firstday=data[0];
                  lastday=data[1];
                }
                else
                {
                   firstday = $(".start_date").val();
                   lastday = $(".end_date").val();
                }
                //console.log(firstday);
              
                  $.ajax({
                      url: "<?php echo site_url('admin/filter_offer_list');?>",
                      type: "post",
                      data: {type:type,firstday:firstday,lastday:lastday},
                      success: function(msg)
                      {

                        $("#offer_div").children().remove();
                        $("#offer_div").html(msg);
                        $("#loading_img_store_offer").hide();
                        $("#pagination").hide();

                 

                      } 

                  });
                
                //console.log(student_id);
            }
          
        </script>


  
        <script>
            $(document).ready(function(){

             $('#select_all').change(function(){
            
                  $('#offer_div input:checkbox').prop('checked', this.checked);
                  
                  // var oTable = $("#dataTable1").dataTable();
                  // var anNodes = $("#dataTable1 tbody tr");

                  // for (var i = 0; i < anNodes.length; ++i)
                  // {
                  //   var rowData = oTable.fnGetData( anNodes[i] );

                  //   console.log(rowData);
                  // }
                 
               });
            });
        </script>
   
  </body>
</html>
