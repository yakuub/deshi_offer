<?php $this->load->view('admin/header'); ?>

  <body>
    <div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">

     <?php $this->load->view('admin/sidebar'); ?> 

                <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="index.html">Products</a>
            </li>
            <li class="breadcrumb-item">
              <span>Laptop with retina screen</span>
            </li>
          </ul>
          <div class="content-i">
            <div class="content-box">
              <div class="element-wrapper">
                <h6 class="element-header">
                  Data Tables
                </h6>
                <div class="element-box">
                  <h5 class="form-header">
                   Highlited Offer List
                    <?php if ($this->session->flashdata('Successfully')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('Successfully'); ?>",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
                  </h5>

                  

                  <div class="table-responsive">

                    <table id="dataTable1" width="100%" class="table table-striped table-lightfont">
                    <thead>
                    <tr>  
                   
                    <th>Title</th>
                    <th>Type</th>                   
                    <th>Expire Date</th>                   
                    <th class="text-center">Action</th>

                    </tr>
                    </thead>


                    <tfoot>
                    <tr>
                   
                   
                    <th>Title</th>
                    <th>Type</th>                   
                    <th>Expire Date</th>                   
                    <th class="text-center">Action</th>

                    </tr>
                    </tfoot>
                   
                    <tbody>
              <?php foreach ($highlight_offer as $row) {?>
                 <tr>

                  

                  <td><?=$row['h_offer_title'];?></td>
                  <td><?php if ($row['offer_type']==0) 
                      {
                        echo 'Highlited Offer';
                      }
                      elseif ($row['offer_type']==1)
                      {
                        echo 'Deals Of The Day';
                      }
                      else
                      {
                        echo 'Offer Highlights';
                      }
                      ?>
                     
                  </td>
                  
                  <td><?=date('d-m-Y',strtotime($row['expire_date']));?></td>
                 
                  
                 <td class="text-center">
                  <a href="admin/view_highlited_offer/<?=$row['id']?>"><i class="fa fa-eye"></i></a> &nbsp
                  
                  <a href="admin/delete_highlited_offer/<?=$row['id']?>"><i class="fa fa-trash-o"></i></a>
                </td>

                 </tr>
          <?php } ?> 

                </tbody>

               
                </table>
                
                  </div>
                  
                  <div class="modal" id="highlight_modal" role="dialog">
                  </div> 


                </div>
                </div>
              
              <div class="floated-chat-btn">
                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
              </div>
              <div class="floated-chat-w">
                <div class="floated-chat-i">
                  <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                  </div>
                  <div class="chat-head">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-messages">
                    <div class="message">
                      <div class="message-content">
                        Hi, how can I help you?
                      </div>
                    </div>
                    <div class="date-break">
                      Mon 10:20am
                    </div>
                    <div class="message">
                      <div class="message-content">
                        Hi, my name is Mike, I will be happy to assist you
                      </div>
                    </div>
                    <div class="message self">
                      <div class="message-content">
                        Hi, I tried ordering this product and it keeps showing me error code.
                      </div>
                    </div>
                  </div>
                  <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                      <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
     <?php $this->load->view('admin/footer'); ?>

        <script>
            $(document).ready(function(){
 
             $('#mainChkBox').change(function(){
                 $(':checkbox').prop('checked', this.checked);
               });
            });
        </script>

        <script>
            
            function deal_modal() 
            {
               var offer_checked = [];


               $("input[name='offer_id[]']:checked").each(function ()
                {

                  offer_checked.push(parseInt($(this).val()));
                });

                //alert(offer_checked);

                $.ajax({
                    url: "<?php echo site_url('admin/add_product_highlights');?>",
                    type: "post",
                    data: {offer_checked:offer_checked,},
                    success: function(msg)
                    {
                      $('#highlight_modal').modal('show');
                      $("#highlight_modal").html(msg);

                    } 

                }); 
                //console.log(student_id);
            }
        </script>
   
  </body>
</html>
