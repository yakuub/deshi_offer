<?php $this->load->view('admin/header'); ?>

  <body>
    <div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">

     <?php $this->load->view('admin/sidebar'); ?> 

        
        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="dashboard.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="dashboard.php">Products</a>
            </li>
            <li class="breadcrumb-item">
              <span>Laptop with retina screen</span>
            </li>
          </ul>
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box">

            <div class="row">
          <h5 class="form-header">
         
          
           <?php if ($this->session->flashdata('Successfully')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('Successfully'); ?>",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>

          </h5>

         <div class="col-sm-12">
    
                <div class="element-box">
                  <h5 class="form-header">
                  Expired Offer List
                  </h5>
                  <br>

                  <div class="table-responsive">

                    <table id="dataTable1" width="100%" class="table table-striped table-lightfont">
                <thead>
                <tr>
               
                <th>Title</th>
                <th>Type</th>
                <th>Featured Image</th>

                </tr>
                </thead>


                <tfoot>
                <tr>
               
                <th>Title</th>
                <th>Type</th>
                <th>Featured Image</th>

                </tr>
                </tfoot>
               
                <tbody>
              <?php foreach ($expired_offer_list as $row) {?>
                 <tr>
                   <td><?=$row['offer_title'];?></td>
                  <td><?=$row['offer_type_tilte'];?></td>
                  <td><img class="table-image" height="50px" width="50px" src="uploads/offer/<?=$row['offer_featured_file'];?>" alt=""></td>
                 
                  
                 </tr>
          <?php } ?> 

                </tbody>

               
                </table>
                
                  </div>
                </div>
                    
        </div>
      </div>
              <div class="floated-chat-btn">
                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
              </div>
              <div class="floated-chat-w">
                <div class="floated-chat-i">
                  <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                  </div>
                  <div class="chat-head">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-messages">
                    <div class="message">
                      <div class="message-content">
                        Hi, how can I help you?
                      </div>
                    </div>
                    <div class="date-break">
                      Mon 10:20am
                    </div>
                    <div class="message">
                      <div class="message-content">
                        Hi, my name is Mike, I will be happy to assist you
                      </div>
                    </div>
                    <div class="message self">
                      <div class="message-content">
                        Hi, I tried ordering this product and it keeps showing me error code.
                      </div>
                    </div>
                  </div>
                  <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                      <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
     <?php $this->load->view('admin/footer'); ?>
   
  </body>
</html>
