<?php $this->load->view('admin/header'); ?>

  <body>
    <div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">

     <?php $this->load->view('admin/sidebar'); ?> 

        
        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="dashboard.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="dashboard.php">Products</a>
            </li>
            <li class="breadcrumb-item">
              <span>Laptop with retina screen</span>
            </li>
          </ul>
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box">

            <div class="row">
          <h5 class="form-header">
             Post New Deal/Offer
             
              <?php if ($this->session->flashdata('Successfully')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('Successfully'); ?>",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
          </h5>

  <div class="col-sm-12">
    <div class="element-wrapper">
      <div class="element-box">
	  
	       

        <form id="formValidate" action="admin/add_new_offer_post" method="post" enctype="multipart/form-data">

			<input type="hidden" name="post_by" value="<?=$this->session->userdata('login_id');?>">

	          <div class="form-group">
	            <label for=""> Offer For <span style="color:red;font-family:verdana">(*)</span></label>
             <select class="form-control select2" name="offer_from">
              <?php foreach ($offer_for_list as $key => $row) { ?>
                <option value="<?=$row['loginid'];?>"><?=$row['username'];?> </option>
            <?php } ?>
             </select>
	            <div class="help-block form-text with-errors form-control-feedback"></div>
	          </div>
            
            <div class="form-group">
              <label for=""> Offer Title <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="offer_title" data-error="Please input your Post Title" placeholder="Offer Title" required="required" type="text">
             
            </div>
           
	          <div class="form-group">
	            <label for="">Offer Type <span style="color:red;font-family:verdana">(*)</span></label>
              <select class="form-control select2" name="offer_title_type">
	               <?php foreach ($offer_type_list as $key => $row) { ?>
                  <?php
                    if($row['offer_type']==0)
                    {
                      $ot="Regular";
                    }
                    else
                    {
                      $ot="Occasional";
                    }?>
                <option value="<?=$row['offer_id'];?>"><?=$row['offer_type_tilte'] .'('. $ot .')';?> </option>
            <?php } ?>
	            </select>
	          </div>

            <div class="form-group row">
              <label for=""> Offer Expire Type <span style="color:red;font-family:verdana">(*)</span></label>
           &nbsp;&nbsp;&nbsp; 

              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" name="exp_type" id="type" type="radio"  onclick="showDate(this.value)" value="1" checked>Limited</label>
              </div>&nbsp;      
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" type="radio"  name="exp_type" id="type" onclick="showDate(this.value)" value="0" checked="">Un-Limited</label>
              </div>

          </div>

          <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for=""> Offer Start Date</label>

                  <input class="form-control" name="sdate" placeholder="Post Start Date" type="text" id="datepicker-example21" >
                </div>
              </div>

            </div>

         
            <div id="date_area" style="display: none;">
              <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for=""> Offer End Date</label>
                  <input class="form-control" name="edate" placeholder="Post End Date" type="text" id="datepicker-example2">
                </div>
              </div>
              </div>
              <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for=""> Offer Show Expire Date<span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" id="datepicker-example6" placeholder="Post Date" name="show_expire_date" type="text" value="<?php echo date('Y-m-d')?>">
                </div>
              </div>
            </div>
        </div>
     
       


            
            
        <!-- <div class="row">
              

            </div> -->
      <div class="form-group">
            <label for="">Offer Store Type<span style="color:red;font-family:verdana">(*)</span></label><select class="form-control select2" name="offer_store_type">
             
       <option value='1'>On-line Store </option>
        <option value='0'>Off-line Store</option>
       </select>
          </div> 
          
      <div class="form-group">
            <label for="">Is hot<span style="color:red;font-family:verdana">(*)</span></label><select class="form-control select2" name="hot_status">
             
       <option value='1'>Yes </option>
        <option value='0'>No</option>
       </select>
          </div> 
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for=""> Discount Value (Only Number)</label><input class="form-control"  placeholder="Discount Value" name="dvalue" type="text">
                </div>
              </div>

            </div> 
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="">Inner Discount Value</label><input class="form-control"  placeholder="Inner Discount Value" name="inner_discount" type="text">
                </div>
              </div>

            </div> 
      
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for=""> Offer Date <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" id="datepicker-example1" placeholder="Post Date" name="of_post_date" type="text" value="<?php echo date('Y-m-d')?>">
                </div>
              </div>

            </div>

            <div class="form-group">
              <label>Offer Description <span style="color:red;font-family:verdana">(*)</span></label><textarea class="form-control" id="ckeditor1" name="offer_desc" rows="3" required></textarea>
            </div> 
            <div class="form-group">
              <label>Offer Terms </label><textarea class="form-control" id="ckeditor1" name="offer_terms" rows="3"></textarea>
            </div> 

            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for=""> Deal Featured File <span style="color:red;font-family:verdana">(*)</span></label>
                  <input class="form-control" placeholder="Offer Title" name="file"  type="file">
                 
                </div>
              </div>

            </div>
            <!-- <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for=""> Deal Others File</label>
                  <input class="form-control" placeholder="Offer Title" name="file"  type="file" multiple>
                 
                </div>
              </div>

            </div> -->
      
          <div class="form-group row">
              <label for=""> Coupon Type <span style="color:red;font-family:verdana">(*)</span></label>
            &nbsp;&nbsp;&nbsp;  
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input"  name="type" id="voucher" type="radio"  onclick="showField(this.value)" value="buy_it" checked>Voucher</label>
              </div>&nbsp;      
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" type="radio"  name="type" onclick="showField(this.value)" value="get_code">Coupon</label>
              </div>&nbsp;

              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" name="type" type="radio" id="sms"  onclick="showField(this.value)" value="get_sms">SMS</label>
              </div>&nbsp;
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" type="radio" name="type" onclick="showField(this.value)" value="online_deal">Online Deal</label>
              </div>&nbsp;    
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" type="radio" name="type" onclick="showField(this.value)" value="activate_deal">Promotional</label>
              </div>
&nbsp;    
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" type="radio" name="type" onclick="showField(this.value)" value="direct_sale">Direct Sale</label>
              </div>
&nbsp;    
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" type="radio" name="type" onclick="showField(this.value)" value="custom_link">Custom</label>
              </div> 
          </div>
<!--below for conditional Form-->

       <div id="price_area" class="hide_area" style="display:block;">
     <div class="row">
              <div class="col-sm-12" >
                <div class="form-group">
                  <label for="">Coupon Price <span style="color:red;font-family:verdana">(*)</span></label>
         <input type="text" name="coupon_price" placeholder="Type Coupon Price" class="form-control">
              
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="">Coupon Code <span style="color:red;font-family:verdana">(*)</span></label>
         <input type="text" name="coupon_code" placeholder="Type Coupon Code" class="form-control">
              
                </div>
              </div>
            </div>
        </div>     
       <div id="direct_sale" class="hide_area" style="display: none;">
     <div class="row">
              <div class="col-sm-12" >
                <div class="form-group">
                  <label for="">Price <span style="color:red;font-family:verdana">(*)</span></label>
         <input type="text" name="price" placeholder="Type Sale Price" class="form-control">
              
                </div>
              </div>

            </div>
        </div>   
        <div id="sms_body_area" class="hide_area" style="display: none;">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="">SMS Body (Max 160 Word)<span style="color:red;font-family:verdana">(*)</span></label>
         
               <textarea class="form-control" id="ckeditor2" name="sms" rows="3" maxlength="160" onKeyDown="limitText(this.form.limitedtextarea,this.form.countdown,160);" 
onKeyUp="limitText(this.form.limitedtextarea,this.form.countdown,160);"></textarea>
                </div>

              </div>
          </div>
        </div>    
        <div id="coupon_code_area" class="hide_area" style="display: none;">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="">Type Code <span style="color:red;font-family:verdana">(*)</span></label>
         
              <input type="text" name="utm_code" placeholder="Type Coupon Code" class="form-control">
                </div>
              </div>

          </div>
        </div>
        <div id="reference_link_area" class="hide_area" style="display: none;">
            <div class="row">

              <div class="col-sm-12">
                <div class="form-group">
                  <label for="">Ref Link <span style="color:red;font-family:verdana">(*)</span></label>
         
              <input type="url" name="ref_link" placeholder="Type Ref Link" class="form-control">
                </div>
              </div>  
          </div>
        </div>
        <div id="custom" class="hide_area" style="display: none;">
           
            <div class="row">

       <div class="col-sm-6 nopadding">
  <div class="form-group">
    <input type="text" class="form-control" id="Schoolname" name="title[]" value="" placeholder="Title">
  </div>
</div>
<div class="col-sm-6 nopadding">
  <div class="form-group">
    <div class="input-group">
    <input type="text" class="form-control" id="Major" name="valu[]" value="" placeholder="Values">
        <div class="input-group-btn">
        <button class="btn btn-success" type="button"  onclick="education_fields();"> <span class="glyphicon glyphicon-plus" aria-hidden="true">(+)</span> </button>
      </div>
    </div>
  </div>
</div>



            </div>
        <div id="education_fields">
          
        </div>

        </div>  

        </hr>



          <div class="form-group">
            <label for=""> Offer Categories <span style="color:red;font-family:verdana">(*)</span></label>
            <select class="form-control select2" name="catagories" required>
            <?php foreach ($cat_list as $key => $row) { ?>
                 
                <option value="<?=$row['catagory_id'];?>"><?=$row['catagory_title'];?> </option>
            <?php } ?>

            </select>
          </div> 

          <div class="form-group">
              
               <label for=""> Offer tags<span style="color:red;font-family:verdana">(*)</span></label>

            <input type="text" name="tags[]" placeholder="Type Tags ( If multiple use , after each tag)" class="form-control" required="">
     
            <!--<label for=""> Offer tags <a href='admin/add_tag'><span style="color:red;font-family:verdana">(*)(Add New)</span></a></label>-->
            <!--<select class="form-control select2" name="tags[]" multiple>-->
            <!--  <?php foreach ($tag_list as $key => $row) { ?>-->
            <!--    <option value="<?=$row['tag_id'];?>">-->
            <!--      <?=$row['tag_title'];?> -->
            <!--    </option>-->
            <!--<?php } ?>-->

            <!--</select>-->
            
          </div> 

      <div id="txtHint"></div>

          </fieldset>
    <div class="form-group row">
              <label for=""> Coupon Limit Type <span style="color:red;font-family:verdana">(*)</span></label>
            &nbsp;&nbsp;&nbsp; 
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" type="radio"  checked="" onclick="showAmount(this.value)" id="unlimited" name="limit" value="0">Unlimited</label>
              </div>&nbsp;      
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" type="radio" onclick="showAmount(this.value)" id="limit" name="limit" value="1">Limited</label>
              </div>

             
             
              
            
          </div>

        <div id="amount_area" class="form-group required" style="display: none;">
                      <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for=""> Coupon Count <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" id="limit_amount" name="coupon_total" placeholder="Total Coupon" type="text">
                </div>
              </div>

            </div>

        </div>
                <div class="form-group row">
              <label for=""> Coupon Target <span style="color:red;font-family:verdana">(*)</span></label>
            &nbsp;&nbsp;&nbsp; 
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input" checked  type="checkbox" id="normal"  name="target_people_normal" value="1">Normal</label>
              </div>&nbsp;      
              <div class="form-check">
                <label class="form-check-label"><input class="form-check-input"  type="checkbox" id="Corporate" onclick="Corporate_man()" name="target_people_normal" value="2">Corporate</label>
              </div>
          </div>

        <fieldset id="corporate_body" style="display:none">
                       <div class="form-group row" >
     
            <label for="">Corporate Company</label>
            <select class="form-control select2" style="width:100%" name="corporate_com[]" multiple="true">
            

            </select>
        </div>

          </fieldset>    
         
	          
	          <div class="form-buttons-w">
	            <button class="btn btn-primary" name="submit" type="submit"> Submit</button>
	          </div>
	        </form>
	
      </div>
    </div>
              
			              
			  </div>
			</div>
              <div class="floated-chat-btn">
                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
              </div>
              <div class="floated-chat-w">
                <div class="floated-chat-i">
                  <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                  </div>
                  <div class="chat-head">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-messages">
                    <div class="message">
                      <div class="message-content">
                        Hi, how can I help you?
                      </div>
                    </div>
                    <div class="date-break">
                      Mon 10:20am
                    </div>
                    <div class="message">
                      <div class="message-content">
                        Hi, my name is Mike, I will be happy to assist you
                      </div>
                    </div>
                    <div class="message self">
                      <div class="message-content">
                        Hi, I tried ordering this product and it keeps showing me error code.
                      </div>
                    </div>
                  </div>
                  <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                      <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
     <?php $this->load->view('admin/footer'); ?>

  <script type="text/javascript">
    
          $(document).ready(function(){
             var type = $("[name=type]:checked").val();
            
             var limit = $("[name=limit]:checked").val();
             showField(type);
             showtypeField(type_voucher);
             showAmount(limit)
             
          });

          function showField(value){
            var limit = $("[name=limit]:checked").val();
            $('.hide_area').hide();
            $('.limited_unlimited').fadeIn();
            
            if(value=='buy_it')
            {
              showAmount(limit);
              $('#price_area').fadeIn();

            }
            else if(value=='utm')
            {
              showAmount(limit);
              $('#utmbody').fadeIn();

            }
            

            else if(value=='direct_sale')
            {
              showAmount(limit);
              $('#direct_sale').fadeIn();

            }
            else if(value=='get_sms')
            {
              showAmount(limit);
              $('#sms_body_area').fadeIn();

            }
            else if(value=='get_code')
            {
              showAmount(limit);
              $('#coupon_code_area').fadeIn();
              $('#reference_link_area').fadeIn();
              

            }
            else if(value=='online_deal')
            {
              $('#reference_link_area').fadeIn();
              $('.limited_unlimited').hide();
              $( "#unlimited" ).prop( "checked", true );

            }
            
            else if(value=='activate_deal')
            {
              //$('#reference_link_area').fadeIn();
              $('.limited_unlimited').hide();
              $('#amount_area').hide();
              $( "#unlimited" ).prop( "checked", true );
              
            }

            else if(value=='custom_link')
            {
              $('#custom').fadeIn();
            }
            else
              {
              
            }
          }





          function showtypeField(value)
          {
            //$('.hide_field').show();
            if(value==1)
            {
                  $('#old_price_hide').fadeOut();
              $('#label_price').text('Price');
              $('#new_price').attr('placeholder', 'Price');
              
            }
            else if(value==0)
            {   
              $('#old_price_hide').fadeIn();
              $('#label_price').text('New  Price');
            }
            
          }

          function showAmount(value){
            
            if(value=='1'){
              $('#amount_area').fadeIn();
            }else{
              $('#amount_area').hide();
            }
          } 

          function showDate(value){
            
            if(value=='1'){
              $('#date_area').show();
            }else{
              $('#date_area').hide();
            }
          }

          </script>


          <script type="text/javascript">
          function Corporate_man()
          {
            // Get the checkbox
            var Corporate = document.getElementById("Corporate");
            // Get the output text
            var text = document.getElementById("corporate_body");

            // If the checkbox is checked, display the output text
            if (Corporate.checked == true){
              text.style.display = "block";
            } else {
              text.style.display = "none";
            } 
          } 

          </script>

          <script type="text/javascript">


          $(document).ready(function(){
            $('#new_price').keyup(function() {
              $(this).val($(this).val().replace(/[^.\d]/, ''));
             });
             $('#old_price').keyup(function() {
              $(this).val($(this).val().replace(/[^.\d]/, ''));
             });
             
             //for number
             $('#limit_amount').keyup(function() {
              $(this).val($(this).val().replace(/[^\d]/, ''));
             });
          });
          </script>

          <script type="text/javascript">
          var room = 1;
          function education_fields() {
           
              room++;
              var objTo = document.getElementById('education_fields')
              var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeclass"+room);
            var rdiv = 'removeclass'+room;
              divtest.innerHTML = '<div class="row"><div class="col-sm-6 nopadding"><div class="form-group"><input type="text" class="form-control" id="Schoolname" name="title[]" value="" placeholder="Title"></div></div><div class="col-sm-6 nopadding"><div class="form-group"><div class="input-group"><input type="text" class="form-control" id="Major" name="valu[]" value="" placeholder="Values"><div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"><span class="glyphicon glyphicon-minus" aria-hidden="true">(-)</span></button></div></div></div></div><div class="clear"></div></div>';
              
              objTo.appendChild(divtest)
          }
             function remove_education_fields(rid) {
               $('.removeclass'+rid).remove();
             }
          </script>
          <script>
          // Get the modal
          var modal = document.getElementById('myModal');

          // Get the button that opens the modal
          var btn = document.getElementById("myBtn");

          // Get the <span> element that closes the modal
          var span = document.getElementsByClassName("close")[0];

          // When the user clicks the button, open the modal 
          btn.onclick = function() {
              modal.style.display = "block";
          }

          // When the user clicks on <span> (x), close the modal
          span.onclick = function() {
              modal.style.display = "none";
          }

          // When the user clicks anywhere outside of the modal, close it
          window.onclick = function(event) {
              if (event.target == modal) {
                  modal.style.display = "none";
              }
          }
</script>

   
  </body>
</html>
