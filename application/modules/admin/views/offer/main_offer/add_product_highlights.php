<div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header text-center">
         
          <h4 class="modal-title ">Offer Highlihts</h4>
        </div>
        <div class="modal-body">

          <form id="formValidate" action="admin/save_product_highlights" method="post" enctype="multipart/form-data">
            
         

            <input type='hidden' name="offer_checked" value="<?=$offer_checked?>">


            <div class="form-group">
              <label for=""> Offer Title <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" placeholder="Offer Title" name="offer_title" required="required" type="text">
              <div class="help-block form-text with-errors form-control-feedback"></div>
            </div>
            <div class="form-group">
              <label for=""> Offer Type <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control" name="offer_type">
                <option value="0">
                  HIGHLIGHTED OFFER
                </option>
                <option value="1">
                  DEALS OF THE DAY
                </option>
              </select>
            </div>
           
           <div class="form-group">
              <label for=""> Offer Expire Date</label><input class="form-control" name="expire_date" placeholder="Post Start Date" type="date" id="datepicker-example21" value="<?php echo date('Y-m-d')?>">
            </div>
            <div class="form-buttons-w modal-footer">
              <button class="btn btn-primary" name="submit" type="submit"> Submit</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </form>
        </div>
      
      </div>
      
    </div>

  


