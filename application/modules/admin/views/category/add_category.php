<?php $this->load->view('admin/header'); ?>

  <body>
    <div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">

     <?php $this->load->view('admin/sidebar'); ?> 

        
        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="dashboard.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="javascript:void(0)">Category</a>
            </li>
          
          </ul>
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box">

            <div class="row">
          <h5 class="form-header">
           Manage Category

           
            <?php if ($this->session->flashdata('Successfully')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('Successfully'); ?>",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
          </h5>

  <div class="col-sm-12">
    <div class="element-wrapper">
      <div class="element-box">
	  
	       

        <form id="formValidate" action="admin/add_category_post" method="post" enctype="multipart/form-data">

			<input type="hidden" name="add_by" value="<?=$this->session->userdata('login_id');?>">

	          <div class="form-group">
	            <label for=""> Catagory Title <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" placeholder="Catagory Title" name="catagory_title" required="required" type="text">
	            <div class="help-block form-text with-errors form-control-feedback"></div>
	          </div>

          
          <div class="form-group" >
            <label class="control-label"> Show on Menu <span style="color:red;font-family:verdana">(*)</span></label>
                                        
            <select name="top_menu_st" class="form-control select2" data-original-title="Show on Menu" data-placeholder="Select Show on Top Menu">
             <option></option>
              <option value="1">Yes</option>
              <option value="0">No</option>

            </select>                                

          </div>

          <div class="form-group" >
            <label class="control-label"> Show on Featured <span style="color:red;font-family:verdana">(*)</span></label>
                                        
            <select name="featured_cat_st" class="form-control select2" data-original-title="Show on Menu" data-placeholder="Select Show on Featured">
             <option></option>
              <option value="1">Yes</option>
              <option value="0" selected>No</option>

            </select>                                

          </div>

          <div class="form-group" >
            <div class="alert alert-danger alrt_form" id="err_div" style="display: none">
                <label id="m_msg"></label>
            </div>

            <label class="control-label"> Show on Front Page <span style="color:red;font-family:verdana">(*)</span></label>
                                        
            <select name="front_page_show_st" id="front_page_show_st" class="form-control select2" data-original-title="Show on Menu" data-placeholder="Select Show on Front Page" onchange="show_front_page()">
             <option></option>
              <option value="1">Yes</option>
              <option value="0">No</option>

            </select>                                

          </div>

            <div class="form-group" >
            <label class="control-label"> Is Published <span style="color:red;font-family:verdana">(*)</span></label>
                                        
            <select name="menu_st" id="menu_st" class="form-control select2" data-original-title="Show on Menu" data-placeholder="Select Show on Menu">
            
              <option selected value="1">Yes</option>
              <option value="0">No</option>

            </select>                                

          </div>

          <div class="form-group">
              <label for=""> Menu Serial <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" placeholder="Menu Serial" name="top_menu_sl" required="required" type="text">
              <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>

          <div class="form-group">
            <div class="form-group row">
            <label for="example-email-input" class="control-label col-md-3"> Image  <span style="color:red;font-family:verdana">(*)</span></label>
           
             <div class="fileinput fileinput-new col-10" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;border: 1px solid; padding:1px;"><img src="back_asset/img_default.png" alt="Your Image" />
             
                </div>

                <div>
                  <span class="btn btn-info btn-file border"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>

                  <input type="file" name="file" required=""></span>
                  
                  <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                </div>
              </div>
           </div>
           </div>
			   
	          <div class="form-buttons-w">
	            <button class="btn btn-primary" name="submit" id="submit" type="submit"> Submit</button>
	          </div>
	        </form>
	
         </div>
      </div>
                <div class="element-box">
                  <h5 class="form-header">
                   Added Catagory List 
                  </h5>

                  <div class="table-responsive">

              <table id="dataTable1" width="100%" class="table table-striped table-lightfont">
			          <thead>
			          <tr>
			         
			           <th>Catagory Title</th>
			           <th>Description</th>
			           <th>Menu Status</th>
					       <th>Edit</th>
					       <th>Delete</th>

			          </tr>
			          </thead>


			          <tfoot>
			          <tr>
			         
			           <th>Catagory Title</th>
			           <th>Description</th>
			           <th>Menu Status</th>
					       <th>Edit</th>
					       <th>Delete</th>

			          </tr>
			          </tfoot>
			         
			        <tbody>
			        <?php foreach ($catagory_list as $row) {?>
			           <tr>
                  <td><?=$row['catagory_title']?></td>
                  <td><?=$row['catagory_description']?></td>
			           	<td>
			           		<?php if ($row['menu_st']==0) 
			           		{
			           			echo 'Un-Publish';
			           		 
			           		} else{
			           			echo 'Publish';
			           		}?>
			           		
			           	</td>
			           	
			           	<td><a href="admin/edit_category/<?=$row['catagory_id']?>">Edit</a></td>
			           	<td><a href="admin/delete_category/<?=$row['catagory_id']?>">Delete</a></td>

			           </tr>
					<?php } ?> 

			          </tbody>

			         
			          </table>
			          
                  </div>
                </div>
			              
			  </div>
			</div>
              <div class="floated-chat-btn">
                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
              </div>
              <div class="floated-chat-w">
                <div class="floated-chat-i">
                  <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                  </div>
                  <div class="chat-head">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-messages">
                    <div class="message">
                      <div class="message-content">
                        Hi, how can I help you?
                      </div>
                    </div>
                    <div class="date-break">
                      Mon 10:20am
                    </div>
                    <div class="message">
                      <div class="message-content">
                        Hi, my name is Mike, I will be happy to assist you
                      </div>
                    </div>
                    <div class="message self">
                      <div class="message-content">
                        Hi, I tried ordering this product and it keeps showing me error code.
                      </div>
                    </div>
                  </div>
                  <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                      <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-panel">
              <div class="content-panel-close">
                <i class="os-icon os-icon-close"></i>
              </div><div class="element-wrapper">
  <h6 class="element-header">
    Agents List
  </h6>
  <div class="element-box-tp">
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar1.jpg">
        </div>
        <div class="pt-user-name">
          Mark Parson
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            Last Login:<strong>Online Now</strong>
          </li>
          <li>
            Tickets:<strong>12</strong>
          </li>
          <li>
            Response Time:<strong>2 hours</strong>
          </li>
        </ul>
        <div class="pt-btn">
          <a class="btn btn-success btn-sm" href="#">Send Message</a>
        </div>
      </div>
    </div>
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar3.jpg">
        </div>
        <div class="pt-user-name">
          Ken Morris
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            Last Login:<strong>Online Now</strong>
          </li>
          <li>
            Tickets:<strong>8</strong>
          </li>
          <li>
            Response Time:<strong>4 hours</strong>
          </li>
        </ul>
        <div class="pt-btn">
          <a class="btn btn-success btn-sm" href="#">Send Message</a>
        </div>
      </div>
    </div>
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar2.jpg">
        </div>
        <div class="pt-user-name">
          John Newman
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            Last Login:<strong>Online Now</strong>
          </li>
          <li>
            Tickets:<strong>14</strong>
          </li>
          <li>
            Response Time:<strong>1 hour</strong>
          </li>
        </ul>
        <div class="pt-btn">
          <a class="btn btn-danger btn-sm" href="#">Offline Now</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="element-wrapper">
  <h6 class="element-header">
    Side Tables
  </h6>
  <div class="element-box">
    <h5 class="form-header">
      Table in white box
    </h5>
    <div class="form-desc">You can put a table tag inside an <code>.element-box</code> class wrapper and add <code>.table</code> class to it to get something like this:
    </div>
    <div class="controls-above-table">
      <div class="row">
        <div class="col-sm-12">
          <a class="btn btn-sm btn-primary" href="#">Download CSV</a><a class="btn btn-sm btn-danger" href="#">Delete</a>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-lightborder">
        <thead>
          <tr>
            <th>
              Customer
            </th>
            <th class="text-center">
              Status
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              John Mayers
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Kelly Brans
            </td>
            <td class="text-center">
              <div class="status-pill red" data-title="Cancelled" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Tim Howard
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Joe Trulli
            </td>
            <td class="text-center">
              <div class="status-pill yellow" data-title="Pending" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Fred Kolton
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
</div>
<?php $this->load->view('admin/footer'); ?>

<script>
   function show_front_page() 
        {
            var front_page_show_st=$("#front_page_show_st").val();
            if (front_page_show_st==1) 
            {

              $("#err_div").hide();

              $.ajax({
                  url: "<?php echo site_url('admin/show_cat_front_page');?>",
                  type: "post",
                  data: {front_page_show_st:front_page_show_st},
                  success: function(msg)
                  {
                     if (msg==0) 
                     {
                        $("#err_div").show();
                        $("#m_msg").html('You Have Already Showed 5 Category on Front Page.Please Delete A Category to Show on Front Page');
                       
                        //submit btn disable

                        $('#submit').attr('disabled',true);
                     }
                    
                  }      
              });
            } 

            else
            {
              $("#err_div").hide();
              //submit btn enable
              $('#submit').attr('disabled',false);
            } 
        }
</script>
   
  </body>
</html>
