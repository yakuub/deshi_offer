<?php $this->load->view('admin/header'); ?>

<body>
    <div class="all-wrapper menu-side with-side-panel">
        <div class="layout-w">

            <?php $this->load->view('admin/sidebar'); ?>


            <div class="content-w">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="dashboard.php">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="dashboard.php">Products</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span>Laptop with retina screen</span>
                    </li>
                </ul>
                <div class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                      <div class="row">
                           
                        <div class="col-sm-12">
                          <div class="element-wrapper">
                              <div class="element-box">
                                  <form id="formValidate" method="post" action="admin/update_user_info/<?=$user_info[0]['loginid']?>" enctype="multipart/form-data">


                                      <h5 class="form-header">
                                          Add Details of
                                          <?=$user_info[0]['username']?>
                        
                                   <?php if ($this->session->flashdata('Error')): ?>
                                            <script>
                                                swal({
                                                    title: "Error",
                                                    text: "<?php echo $this->session->flashdata('Error'); ?>",
                                                    timer: 1500,
                                                    showConfirmButton: false,
                                                    type: 'error'
                                                });
                                            </script>
                                    <?php endif; ?>


                                      </h5>
                                      <div class="form-group">
                                        <label for=""> Email address</label><input class="form-control" name="email" value="<?=$user_info[0]['email']?>" data-error="Your email address is invalid" placeholder="Enter email" required="required" type="email">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                      </div>

                                      <div class="form-group">
                                        <label for="">Username </label><input class="form-control" name="username" value="<?=$user_info[0]['username']?>" placeholder="Enter Username" required="required" type="text">
                                     
                                      </div>

                                      <div class="form-group">
                                        <label for="">Contact No </label><input class="form-control" name="contact" value="<?=$user_info[0]['contact']?>" placeholder="Enter Contact No" required="required" type="text">
                                      
                                      </div>

                                      <!-- <div class="row">
                                        <div class="col-sm-12">
                                          <div class="form-group">
                                            <input type="hidden" name="pre_img" value="<?=$user_info[0]['profile_pic'];?>">

                                            <label for=""> Photo </span></label>
                                            
                                            <input class="form-control" name="file" type="file">
                                           
                                          </div>
                                        </div>

                                      </div> -->

                                      <div class="row">
                                        <div class="col-sm-12">
                                          <div class="form-group row">
                                          <label for="example-email-input" class="control-label col-md-3">Photo</label>
                                          <input type="hidden" name="pre_img" value="<?=$user_info[0]['profile_pic'];?>">
                                        <div class="fileinput fileinput-new col-10" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;border: 1px solid;"><img src="uploads/user/<?=$user_info[0]['profile_pic'];?>" alt="Your Image" />
                                                    <!-- <input type="file" name="file" > -->
                                                    </div>

                                                    <div>
                                                      <span class="btn btn-primary btn-file border"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="file" ></span>
                                                      <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                  </div>
                                      </div>
                                      </div>
                                    </div>

                                      <!-- <div class="row">
                                      <div class="col-sm-6">
                                        <div class="form-group">
                                          <label for=""> Password</label><input class="form-control" name="password" value="<?=$user_info[0]['password']?>"  data-minlength="6" placeholder="Password" required="required" type="password">
                                          <div class="help-block form-text text-muted form-control-feedback">
                                            Minimum of 6 characters
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-sm-6">
                                        <div class="form-group">
                                          <label for="">Confirm Password</label><input class="form-control" name="conpass" value="<?=$user_info[0]['password']?>"  data-match-error="Passwords don&#39;t match" placeholder="Confirm Password" required="required" type="password">
                                          <div class="help-block form-text with-errors form-control-feedback"></div>
                                        </div>
                                      </div>
                                    </div> -->



                                   <!--  <div class="form-check">

                                      <input type="checkbox" id="tick" onchange="document.getElementById('terms').disabled = !this.checked;" />

                                      <label class="form-check-label">
                                        <p>I agree to terms and conditions</p>
                                    </div> -->

                                    

                                      <div class="form-buttons-w">

                                          <button type="submit" class="btn btn-primary" name="terms" id="terms" > Update </button>

                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                                <!--Upper for Member Add-->
                        </div>
                        <div class="floated-chat-btn">
                            <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
                        </div>
                        <div class="floated-chat-w">
                            <div class="floated-chat-i">
                                <div class="chat-close">
                                    <i class="os-icon os-icon-close"></i>
                                </div>
                                <div class="chat-head">
                                    <div class="user-w with-status status-green">
                                        <div class="user-avatar-w">
                                            <div class="user-avatar">
                                                <img alt="" src="img/avatar1.jpg">
                                            </div>
                                        </div>
                                        <div class="user-name">
                                            <h6 class="user-title">
                                                John Mayers
                                            </h6>
                                            <div class="user-role">
                                                Account Manager
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="chat-messages">
                                    <div class="message">
                                        <div class="message-content">
                                            Hi, how can I help you?
                                        </div>
                                    </div>
                                    <div class="date-break">
                                        Mon 10:20am
                                    </div>
                                    <div class="message">
                                        <div class="message-content">
                                            Hi, my name is Mike, I will be happy to assist you
                                        </div>
                                    </div>
                                    <div class="message self">
                                        <div class="message-content">
                                            Hi, I tried ordering this product and it keeps showing me error code.
                                        </div>
                                    </div>
                                </div>
                                <div class="chat-controls">
                                    <input class="message-input" placeholder="Type your message here..." type="text">
                                    <div class="chat-extra">
                                        <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>

  <?php $this->load->view('admin/footer'); ?>

  <script>
    function get_district() 
        {
            var division_id=$("#division_id").val();

            $.ajax({
                url: "<?php echo site_url('admin/get_district');?>",
                type: "post",
                data: {division_id:division_id},
                success: function(msg)
                {
                    // alert(msg);
                   $("#district_id").html(msg);
                }      
            });  
        }



        function get_area() 
        {
            var district_id=$("#district_id").val();

            $.ajax({
                url: "<?php echo site_url('admin/get_area');?>",
                type: "post",
                data: {district_id:district_id},
                success: function(msg)
                {
                    //alert(msg);
                   $("#area_id").html(msg);
                }      
            });  
        }
  </script>

</body>

</html>