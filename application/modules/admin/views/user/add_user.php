<?php $this->load->view('admin/header'); ?>

  <body>
    <div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">

     <?php $this->load->view('admin/sidebar'); ?> 

        
        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="admin">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="admin">Add User</a>
            </li>

          </ul>
          <div class="content-i">
            <div class="content-box"><div class="row">
  <div class="col-lg-12">
    <div class="element-wrapper">
      <h6 class="element-header">
        Add New User

	
	  <?php if ($this->session->flashdata('Error')): ?>
                                            <script>
                                                swal({
                                                    title: "Error",
                                                    text: "<?php echo $this->session->flashdata('Error'); ?>",
                                                    timer: 1500,
                                                    showConfirmButton: false,
                                                    type: 'error'
                                                });
                                            </script>
                                    <?php endif; ?>

      </h6>
      <div class="element-box">
<form id="formValidate"  action="admin/save_user" method="post" enctype="multipart/form-data">

          <div class="form-group">
            <label for=""> Email address  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="email" data-error="Your email address is invalid" placeholder="Enter email" required="required" type="email">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>

  		    <div class="form-group">
              <label for="">User Login Name  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="name"  placeholder="Enter Username" required="required" type="text">
           
            </div>
          
		   
		  
		  		<div class="form-group">
            <label for="">Contact No<span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="contact"  placeholder="Enter Contact No" required="required" type="text">
          
          </div>

         <!--  <div class="form-group">
            <label for=""> Photo  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" placeholder="Offer Title" name="files"  required="required" type="file">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div> -->

           <div class="form-group">
             <div class="form-group row">
            <label for="example-email-input" class="control-label col-md-3"> Photo  <span style="color:red;font-family:verdana">(*)</span></label>
           
             <div class="fileinput fileinput-new col-10" data-provides="fileinput">
                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;border: 1px solid; padding:1px;"><img src="back_asset/img_default.png" alt="Your Image" />
             
                </div>

                <div>
                  <span class="btn btn-info btn-file border"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>

                  <input type="file" name="files" required=""></span>

                  <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                </div>
              </div>
              </div>
           </div>

          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for=""> Password  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="password" data-minlength="6" placeholder="Password" required="required" type="password">
                <div class="help-block form-text text-muted form-control-feedback">
                  Minimum of 6 characters
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="">Confirm Password  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="conpass" data-match-error="Passwords don&#39;t match" placeholder="Confirm Password" required="required" type="password">
                <div class="help-block form-text with-errors form-control-feedback"></div>
              </div>
            </div>
          </div>
		      <div class="form-check">

          <input type="checkbox" id="tick" onchange="document.getElementById('terms').disabled = !this.checked;" />

          <label class="form-check-label">
            <p>I agree to terms and conditions</p>
        </div>

        

          <div class="form-buttons-w">

              <button type="submit" class="btn btn-primary" name="terms" id="terms" disabled> Update </button>

          </div>
        </form>	
      </div>
    </div>
  </div>

</div>



            </div>
          </div>
        </div>
      </div>

     <?php $this->load->view('admin/footer'); ?>
   
  </body>
</html>
