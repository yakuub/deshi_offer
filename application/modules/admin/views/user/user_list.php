<?php $this->load->view('admin/header'); ?>

  <body>
    <div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">

     <?php $this->load->view('admin/sidebar'); ?> 

        
        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="admin">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="admin/add_user">Add User</a>
            </li>

          </ul>
          <div class="content-i">
            <div class="content-box">
              <div class="element-wrapper">
                <h6 class="element-header">
                 User List
                </h6>
                <div class="element-box">
                  <h5 class="form-header">
                      User List

                       <?php if ($this->session->flashdata('Successfully')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('Successfully'); ?>",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
                    
                  </h5>

                  <div class="table-responsive">
                    <table id="dataTable1" width="100%" class="table table-striped table-lightfont">
					<thead>
					<tr>
					<th>Sl</th>
					<th>Name</th>
					<th>Email </th>
					<th>Contact</th>
					<th>User Type</th>
					<th class="text-center">Profile Photo</th>
					<th class="text-center">Action</th>
				
					</tr>
					</thead>
					<tfoot>
					<tr>
					<th>Sl</th>
					<th>Name</th>
					<th>Email </th>
					<th>Contact</th>
					<th>User Type</th>
					<th class="text-center">Profile Photo</th>
					<th class="text-center">Action</th>
				
					</tr>
					</tfoot>
					<tbody>
	        <?php foreach ($merchant as $key => $row) {

			  if($row['usertype']==1)
				{
					$utitle="Admin";
				}
			 elseif($row['usertype']==2)
			   {
				 $utitle="Merchant";  
			   }
			 elseif($row['usertype']==3)
			   {
				 $utitle="User";  
			   }
			?>
	        <tr>
	            <td><?=($key+1);?></td>
	          
	            <td><a  href="javascript:;"><?=$row['username'];?></a></td>
	           
	            
	            <td><?=$row['email'];?></td>
	            <td><?=$row['contact'];?></td>
	             <td><?php echo $utitle?></td>
	            <td class="text-center">
	            	<img class="table-image" height="50px" width="50px" src="uploads/user/<?=$row['profile_pic'];?>" alt="">
	            </td>



					
				<td>  <a href='admin/edit_user_info/<?=$row['loginid']?>'><i class='fa fa-pencil' Title='Edit'></i></a>
					&nbsp;&nbsp;&nbsp;&nbsp;
					<a href='admin/delete_user/<?=$row['loginid']?>'><i class='fa fa-trash-o' Title='Delete'></i></a>
					&nbsp;&nbsp;&nbsp;&nbsp;
					
					
					
				
                </td>
                                               
	            </tr>
	           
	           <?php } ?>


					</tbody>
					</table>
                  </div>
                </div>
              </div>


            </div>
          </div>
        </div>
      </div>

     <?php $this->load->view('admin/footer'); ?>
   
  </body>
</html>
