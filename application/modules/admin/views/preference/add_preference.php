<?php $this->load->view('admin/header'); ?>

  <body>
    <div class="all-wrapper menu-side with-side-panel">
      <div class="layout-w">

     <?php $this->load->view('admin/sidebar'); ?> 

        
        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="admin">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="javascript:void(0)">Add Preference</a>
            </li>

          </ul>
          <div class="content-i">
            <div class="content-box"><div class="row">
  <div class="col-lg-12">
    <div class="element-wrapper">
      <h6 class="element-header">
        Add New Preference

	
	  <?php if ($this->session->flashdata('Successfully')): ?>
                            <script>
                                swal({
                                    title: "Done",
                                    text: "<?php echo $this->session->flashdata('Successfully'); ?>",
                                    timer: 1500,
                                    showConfirmButton: false,
                                    type: 'success'
                                });
                            </script>
                    <?php endif; ?>
      </h6>
      <div class="element-box">

      <form id="formValidate"  action="admin/add_preference_post" method="post" enctype="multipart/form-data">


		     <div class="form-group">
            <label for="">Preference Title<span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="preference_title"  placeholder="Enter Preference Title" required="required" type="text">
         
          </div>

          

         
          
          <div class="form-buttons-w">          
          <input type="submit" name="submit" class="btn btn-primary" value="Add New">
          </div>




        </form>	
      </div>
    </div>
  </div>

</div>



            </div>
          </div>
        </div>
      </div>

     <?php $this->load->view('admin/footer'); ?>
   
  </body>
</html>
