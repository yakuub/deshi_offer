<?php $this->load->view('admin/header'); ?>

<body>
  <div class="all-wrapper menu-side with-side-panel">
    <div class="layout-w">

     <?php $this->load->view('admin/sidebar'); ?> 


     <div class="content-w">
      <ul class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="admin">Home</a>
        </li>
        <li class="breadcrumb-item">
          <a href="javascript:void(0)">Preference List</a>
        </li>

      </ul>
      <div class="content-i">
        <div class="content-box">
          <div class="element-wrapper">
            <div class="element-box">
              <div class="table-responsive">
                <table id="dataTable1" width="100%" class="table table-striped table-lightfont">
                  <thead>
                    <tr>
                      <th>Sl</th>
                      <th>Preference Title</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>

                  <tbody>
                   <?php foreach ($preference_info as $key => $row) { ?>
                    <tr>
                      <td><?=($key+1);?></td>

                      <td><?=$row['preference_title'];?></td>

                      <td>
                        <a href='admin/edit_preference/<?=$row['id']?>'><i class='fa fa-pencil' Title='Edit'></i></a>
                       &nbsp;&nbsp;&nbsp;&nbsp;
                       <a href='admin/delete_preference/<?=$row['id']?>'><i class='fa fa-trash-o' Title='Delete'></i></a>
                     </td>

                   </tr>

                 <?php } ?>


               </tbody>
             </table>
           </div>
         </div>
       </div>
     </div>


   </div>
 </div>
</div>
</div>

<?php $this->load->view('admin/footer'); ?>

<script type="text/javascript">

  function status_change(sliderid) 
  {
    var status=$('#status_'+sliderid).val();
            //alert(status);return;
            if(status==false)
            {
              status=0;
            }
            else{status=1;}
            
            $.ajax({
              type: "post",
              url: "<?php echo site_url('admin/update_slider_status');?>",
              data : {sliderid:sliderid,status:status},                  
              success : function(data)
              {
                location.reload();
                       // alert(msg);
                     }
                   });  
          }
        </script>

      </body>
      </html>
