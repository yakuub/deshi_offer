<?php $this->load->view('admin/header'); ?>

<body>
  <div class="all-wrapper menu-side with-side-panel">
    <div class="layout-w">

      <?php $this->load->view('admin/sidebar'); ?>


      <div class="content-w">
        <ul class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="dashboard.php">Home</a>
          </li>
          <li class="breadcrumb-item">
            <a href="javascript:void(0)">Edit Merchant</a>
          </li>

        </ul>
        <div class="content-panel-toggler">
          <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
        </div>
        <div class="content-i">
          <div class="content-box">
            <div class="row">

              <div class="col-sm-12">
                <div class="element-wrapper">
                  <div class="element-box">
                    <form id="formValidate" method="post" action="admin/update_merchant_info/<?=$merchant_info[0]['loginid']?>" enctype="multipart/form-data">


                      <h5 class="form-header">
                        Edit Details of
                        <?=$merchant_info[0]['username']?>
                        
                        <?php if ($this->session->flashdata('Error')): ?>
                          <script>
                            swal({
                              title: "Error",
                              text: "<?php echo $this->session->flashdata('Error'); ?>",
                              timer: 1500,
                              showConfirmButton: false,
                              type: 'error'
                            });
                          </script>
                        <?php endif; ?> 


                      </h5>


                      <div class="form-group">
                        <label for="">Username </label><input class="form-control" name="username" value="<?=$merchant_info[0]['username']?>" placeholder="Enter Username"  type="text">

                      </div>

                      <div class="form-group">
                        <label for=""> Email address</label><input class="form-control" name="email" value="<?=$merchant_info[0]['email']?>" data-error="Your email address is invalid" placeholder="Enter email"  type="email">
                        <div class="help-block form-text with-errors form-control-feedback"></div>
                      </div>



                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group row">
                            <label for="example-email-input" class="control-label col-md-3">Logo</label>
                            <input type="hidden" name="pre_img" value="<?=$merchant_info[0]['profile_pic'];?>">
                            <div class="fileinput fileinput-new col-10" data-provides="fileinput">
                              <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;border: 1px solid;"><img src="uploads/user/<?=$merchant_info[0]['profile_pic'];?>" alt="Your Image" />
                                <!-- <input type="file" name="file" > -->
                              </div>

                              <div>
                                <span class="btn btn-primary btn-file border"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="file" ></span>
                                <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>


                      <div class="row">
                        <div class="col-sm-12">
                          <div class="form-group row">
                            <label for="example-email-input" class="control-label col-md-3">Cover Image</label>
                            <input type="hidden" name="" value="<?=$merchant_info[0]['cover_image'];?>">
                            <div class="fileinput fileinput-new col-10" data-provides="fileinput">
                              <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;border: 1px solid;"><img src="uploads/Merchant/<?=$merchant_info[0]['cover_image'];?>" alt="Your Image" />
                                <!-- <input type="file" name="file" > -->
                              </div>

                              <div>
                                <span class="btn btn-primary btn-file border"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="cover_image" ></span>
                                <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="">Is Featured  <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control select2" name="featured">
                         <option <?php if($merchant_info[0]['show_on_web']=='1')
                         {echo "selected";} ?> 
                         value="1">
                       Yes </option>
                       <option <?php if($merchant_info[0]['show_on_web']=='0')
                       {echo "selected";} ?> 
                       value="0" >No
                     </option>
                   </select>
                 </div>

                 <div class="form-group">
                  <label for="">Is Top Stores  <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control select2" name="top_stores">

                    <option <?php if($merchant_info[0]['top_store']=='1')
                    {echo "selected";} ?> 
                    value="1">
                  Yes </option>
                  <option <?php if($merchant_info[0]['top_store']=='0')
                  {echo "selected";} ?> 
                  value="0" >No
                </option>
              </select>
            </div>

            <div class="form-group">
              <label for="">Is Brand  <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control select2" name="is_brand">
                <option <?php if($merchant_info[0]['is_brand']=='1')
                {echo "selected";} ?> 
                value="1">
              Yes </option>
              <option <?php if($merchant_info[0]['is_brand']=='0')
              {echo "selected";} ?> 
              value="0" >No
            </option>
          </select>
        </div>

        <div class="form-group" >

          <label class="control-label"> Is Published <span style="color:red;font-family:verdana">(*)</span></label>

          <select name="is_publish" id="is_publish" class="form-control select2" data-original-title="Show on Menu" data-placeholder="Select Show on Menu">
           <option></option>

           <option <?php if($merchant_info[0]['is_publish']=='1')
           {echo "selected";} ?> 
           value="1">
           Yes
         </option>

         <option 
         <?php if($merchant_info[0]['is_publish']=='0')
         {echo "selected";} ?> 
         value="0">No
       </option>

     </select>                                

   </div>


   <div class="form-group">
    <label for="">Contact No  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="contact_no" id="contact_no" placeholder="Enter Contact No" value="<?=$merchant_info[0]['contact']?>" required="required" type="text">

  </div>

  <div class="form-group">
    <label for="website">Website  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="website" value="<?=$merchant_info[0]['website']?>" id="website"  placeholder="Enter Website" required="required" type="text">

  </div>


  <div class="form-buttons-w">

    <button type="submit" class="btn btn-primary" name="terms" id="terms"> Update </button>

  </div>
</form>
</div>
</div>
</div>

<!--Upper for Member Add-->
</div>
<div class="floated-chat-btn">
  <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
</div>
<div class="floated-chat-w">
  <div class="floated-chat-i">
    <div class="chat-close">
      <i class="os-icon os-icon-close"></i>
    </div>
    <div class="chat-head">
      <div class="user-w with-status status-green">
        <div class="user-avatar-w">
          <div class="user-avatar">
            <img alt="" src="img/avatar1.jpg">
          </div>
        </div>
        <div class="user-name">
          <h6 class="user-title">
            John Mayers
          </h6>
          <div class="user-role">
            Account Manager
          </div>
        </div>
      </div>
    </div>
    <div class="chat-messages">
      <div class="message">
        <div class="message-content">
          Hi, how can I help you?
        </div>
      </div>
      <div class="date-break">
        Mon 10:20am
      </div>
      <div class="message">
        <div class="message-content">
          Hi, my name is Mike, I will be happy to assist you
        </div>
      </div>
      <div class="message self">
        <div class="message-content">
          Hi, I tried ordering this product and it keeps showing me error code.
        </div>
      </div>
    </div>
    <div class="chat-controls">
      <input class="message-input" placeholder="Type your message here..." type="text">
      <div class="chat-extra">
        <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
      </div>
    </div>
  </div>
</div>
</div>

</div>
</div>

<?php $this->load->view('admin/footer'); ?>

<script>
  function get_district() 
  {
    var division_id=$("#division_id").val();

    $.ajax({
      url: "<?php echo site_url('admin/get_district');?>",
      type: "post",
      data: {division_id:division_id},
      success: function(msg)
      {
                    // alert(msg);
                    $("#district_id").html(msg);
                  }      
                });  
  }



  function get_area() 
  {
    var district_id=$("#district_id").val();

    $.ajax({
      url: "<?php echo site_url('admin/get_area');?>",
      type: "post",
      data: {district_id:district_id},
      success: function(msg)
      {
                    //alert(msg);
                    $("#area_id").html(msg);
                  }      
                });  
  }
</script>

</body>

</html>