<?php $this->load->view('admin/header'); ?>

<body>
  <div class="all-wrapper menu-side with-side-panel">
    <div class="layout-w">

     <?php $this->load->view('admin/sidebar'); ?> 


     <div class="content-w">
      <ul class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="admin">Home</a>
        </li>
        <li class="breadcrumb-item">
          <a href="admin/merchant_list">Merchant List</a>
        </li>

      </ul>
      <div class="content-i">
        <div class="content-box"><div class="row">
          <div class="col-lg-12">
            <div class="element-wrapper">
              <h6 class="element-header">
                Add New Merchant


                <?php if ($this->session->flashdata('Successfully')): ?>
                  <script>
                    swal({
                      title: "Done",
                      text: "<?php echo $this->session->flashdata('Successfully'); ?>",
                      timer: 1500,
                      showConfirmButton: false,
                      type: 'success'
                    });
                  </script>
                <?php endif; ?>
              </h6>
              <div class="element-box">

                <form id="formValidate"  action="admin/save_merchant" method="post" enctype="multipart/form-data">


                 <div class="form-group">
                  <label for="">Merchant Login Name  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="username"  placeholder="Enter Username" required="required" type="text">

                </div>

                <div class="form-group">
                  <label for=""> Email address  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="emailaddress" data-error="Your email address is invalid" placeholder="Enter email" required="required" type="email">
                  <div class="help-block form-text with-errors form-control-feedback"></div>
                </div>

                <div class="form-group">
                  <div class="form-group row">
                    <label for="example-email-input" class="control-label col-md-3"> Logo  <span style="color:red;font-family:verdana">(*)</span></label>

                    <div class="fileinput fileinput-new col-10" data-provides="fileinput">
                      <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;border: 1px solid; padding:1px;"><img src="back_asset/img_default.png" alt="Your Image" />

                      </div>

                      <div>
                        <span class="btn btn-info btn-file border"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>

                        <input type="file" name="files" required=""></span>

                        <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                </div> 


                <div class="form-group">
                  <div class="form-group row">
                    <label for="example-email-input" class="control-label col-md-3"> Cover Image  <span style="color:red;font-family:verdana">(*)</span></label>

                    <div class="fileinput fileinput-new col-10" data-provides="fileinput">
                      <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;border: 1px solid; padding:1px;"><img src="back_asset/img_default.png" alt="Your Image" />

                      </div>

                      <div>
                        <span class="btn btn-info btn-file border"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>

                        <input type="file" name="cover_image" required=""></span>

                        <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="">Is Featured  <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control select2" name="featured">
                    <option value="1" selected>
                     Yes
                   </option>
                   <option value="0">
                    No
                  </option>
                </select>
              </div>

              <div class="form-group">
                <label for="">Is Top Stores  <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control select2" name="top_stores">

                  <option value="1">
                   Yes
                 </option>
                 <option value="0">
                  No
                </option>
              </select>
            </div>

            <div class="form-group">
              <label for="">Is Brand  <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control select2" name="is_brand">
                <option value="1">
                 Yes
               </option>
               <option value="0">
                No
              </option>
            </select>
          </div>

          <div class="form-group" >
            <label class="control-label"> Is Published <span style="color:red;font-family:verdana">(*)</span></label>

            <select name="is_publish" id="is_publish" class="form-control select2" data-original-title="Show on Menu" data-placeholder="Select Show on Menu">

              <option selected value="1">Yes</option>
              <option value="0">No</option>

            </select>                                

          </div>

          <div class="form-group">
            <label for="">Contact No  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="contact_no" id="contact_no"  placeholder="Enter Contact No" required="required" type="number">

          </div>

          <div class="form-group">
            <label for="website">Website  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" value="http://" name="website" id="website"  placeholder="Enter Website" required="required" type="text">

          </div>

          <input class="form-control" name="password" data-minlength="6" placeholder="Password" required="required" type="hidden" value="123456">
          
          <div class="form-buttons-w">          
            <input type="submit" name="submit" class="btn btn-primary" value="Add New">
          </div>


          <!--<div class="row">-->
            <!--  <div class="col-sm-6">-->
              <!--    <div class="form-group">-->
                <!--      <label for=""> Password  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="password" data-minlength="6" placeholder="Password" required="required" type="password">-->
                <!--      <div class="help-block form-text text-muted form-control-feedback">-->
                  <!--        Minimum of 6 characters-->
                  <!--      </div>-->
                  <!--    </div>-->
                  <!--  </div>-->
                  <!--  <div class="col-sm-6">-->
                    <!--    <div class="form-group">-->
                      <!--      <label for="">Confirm Password  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="conpass" data-match-error="Passwords don&#39;t match" placeholder="Confirm Password" required="required" type="password">-->
                      <!--      <div class="help-block form-text with-errors form-control-feedback"></div>-->
                      <!--    </div>-->
                      <!--  </div>-->
                      <!--</div>-->


                    </form>	
                  </div>
                </div>
              </div>

            </div>



          </div>
        </div>
      </div>
    </div>

    <?php $this->load->view('admin/footer'); ?>

  </body>
  </html>
