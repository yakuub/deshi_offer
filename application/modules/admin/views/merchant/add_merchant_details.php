<?php $this->load->view('admin/header'); ?>

<body>
    <div class="all-wrapper menu-side with-side-panel">
        <div class="layout-w">

            <?php $this->load->view('admin/sidebar'); ?>


            <div class="content-w">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="dashboard.php">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="dashboard.php">Products</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span>Laptop with retina screen</span>
                    </li>
                </ul>
                <div class="content-panel-toggler">
                    <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
                </div>
                <div class="content-i">
                    <div class="content-box">
                      <div class="row">
                           
                        <div class="col-sm-12">
                          <div class="element-wrapper">
                              <div class="element-box">
                                  <form id="formValidate" method="post" action="admin/save_merchant_details/<?=$merchant_info[0]['md_id']?>" enctype="multipart/form-data">


                                      <h5 class="form-header">
                                          Add Details of
                                          <?=$merchant_info[0]['username']?>
                                      </h5>
                                      <div class="form-group">
                                          <label for="">Profile Headline</label><input class="form-control" name="profile_headline" value="<?=$merchant_info[0]['profile_headline']?>" placeholder="Enter Headline" type="text">

                                      </div>

                                      <!-- <div class="form-group">
                                          <label for="">Featured Image</label><input class="form-control" placeholder="Post Title" name="file" type="file">

                                      </div> -->

                                     <!-- <div class="form-group">-->
                                     <!-- <div class="form-group row">-->
                                     <!-- <label for="example-email-input" class="control-label col-md-3"> Featured Image </label>-->
                                     
                                     <!--  <div class="fileinput fileinput-new col-10" data-provides="fileinput">-->
                                     <!--     <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;border: 1px solid; padding:1px;"><img src="uploads/Merchant/<?=$merchant_info[0]['f_image'];?>" alt="Your Image" />-->
                                       
                                     <!--     </div>-->

                                     <!--     <div>-->
                                     <!--       <span class="btn btn-info btn-file border"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span>-->
                                     <!--       <input type="file" name="file"></span>-->
                                     <!--       <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>-->
                                     <!--     </div>-->
                                     <!--   </div>-->
                                     <!--</div>-->
                                     <!--</div>-->

                                      <div class="form-group">
                                          <label for="">Adv Banner Headline</label><input class="form-control" name="ad_banner_headline" value="<?=$merchant_info[0]['ad_banner_headline']?>" placeholder="Enter Adv Banner Headline" type="text">

                                      </div>
                                      <div class="form-group">
                                        <label for="">Select Division <span style="color:red;font-family:verdana">(*)</span></label>
                                        <select class="form-control select2" onchange="get_district()" name="division_id" id="division_id" data-placeholder="Select Division" required="">
                                        <option></option>
                                        <?php foreach ($division_list as $key => $row) { ?>
                                          <option <?php if ($merchant_info[0]['division_id']==$row['id']) 
                                          { 
                                            echo "selected";
                                          }?>
                                            
                                           value="<?=$row['id'];?>">
                                           <?=$row['name'];?> 
                                         </option>
                                      <?php } ?>
                                       </select>
                                      </div>
                                      <div class="form-group">
                                      <label for="">Select District <span style="color:red;font-family:verdana">(*)</span></label>
                                      <select class="form-control select2" onchange="get_area()" name="district_id" id="district_id" data-placeholder="Select District" required="">
                                      <option></option>
                                      <?php foreach ($district_list as $key => $row) { ?>
                                        <option <?php if ($merchant_info[0]['district_id']==$row['id']) 
                                        { 
                                          echo "selected";
                                        }?>
                                          
                                         value="<?=$row['id'];?>">
                                         <?=$row['name'];?> 
                                       </option>
                                    <?php } ?>
                                     </select>
                                    </div>

                                    <div class="form-group">
                                      <label for="">Select Area <span style="color:red;font-family:verdana">(*)</span></label>
                                      <select class="form-control select2"  name="area_id" id="area_id" data-placeholder="Select Area" required="">
                                      <option></option>
                                      <?php foreach ($area_list as $key => $row) { ?>
                                        <option <?php if ($merchant_info[0]['area_id']==$row['area_id']) 
                                        { 
                                          echo "selected";
                                        }?>
                                          
                                         value="<?=$row['area_id'];?>">
                                         <?=$row['area_title'];?> 
                                       </option>
                                    <?php } ?>
                                     </select>
                                    </div>
                                  
                                      <div class="form-group">
                                          <label>Address <span style="color:red;font-family:verdana">(*)</span></label><textarea class="form-control" name="address" rows="3" required=""><?=$merchant_info[0]['address']?></textarea>
                                      </div>
                                      <div class="form-group">
                                          <label>About Merchant<span style="color:red;font-family:verdana">(*)</span></label><textarea class="form-control" id="ckeditor1" name="about_us" rows="3" required=""><?=$merchant_info[0]['about_us']?></textarea>
                                      </div>
                                      <div class="form-group">
                                          <label for="">G-Map Longitude</label><input class="form-control" name="map_long" value="<?=$merchant_info[0]['map_long']?>" placeholder="Enter Adv G-Map Longitude" type="text">

                                      </div>
                                      <div class="form-group">
                                          <label for="">G-Map latitude</label><input class="form-control" name="map_lati" value="<?=$merchant_info[0]['map_lati']?>" placeholder="Enter Adv G-Map Latitude" type="text">

                                      </div>
                                      <div class="form-group">
                                          <label for="">Facebook Page</label><input class="form-control" name="facebook_link" value="<?=$merchant_info[0]['facebook_link']?>" placeholder="Enter Facebook Page" type="url">

                                      </div>
                                      <div class="form-group">
                                          <label for="">Youtube URL</label><input class="form-control" name="youtube_link" value="<?=$merchant_info[0]['youtube_link']?>" placeholder="Enter Youtube URL" type="url">

                                      </div>
                                      <div class="form-group">
                                          <label for="">Twitter URL</label><input class="form-control" name="twitter_link" value="<?=$merchant_info[0]['twitter_link']?>" placeholder="Enter Twitter URL" type="url">

                                      </div>
                                      <div class="form-group">
                                          <label for="">Link-din URL</label><input class="form-control" name="linkedin_link" value="<?=$merchant_info[0]['linkedin_link']?>" placeholder="Enter Link-din URL" type="url">

                                      </div>
                                      <div class="form-group">
                                          <label for="">Instragram URL</label><input class="form-control" name="instagram_link" value="<?=$merchant_info[0]['instagram_link']?>" placeholder="Enter Instragram URL" type="url">

                                      </div>
                                      <div class="form-group">
                                          <label for="">Support Email Add </label><input class="form-control" name="support_email" value="<?=$merchant_info[0]['support_email']?>" placeholder="Enter Support Contact No" type="text">

                                      </div>
                                      

                                      <div class="form-buttons-w">

                                          <input type="submit" name="submit" class="btn btn-primary" value="Add Data">
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                                <!--Upper for Member Add-->
                        </div>
                        <div class="floated-chat-btn">
                            <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
                        </div>
                        <div class="floated-chat-w">
                            <div class="floated-chat-i">
                                <div class="chat-close">
                                    <i class="os-icon os-icon-close"></i>
                                </div>
                                <div class="chat-head">
                                    <div class="user-w with-status status-green">
                                        <div class="user-avatar-w">
                                            <div class="user-avatar">
                                                <img alt="" src="img/avatar1.jpg">
                                            </div>
                                        </div>
                                        <div class="user-name">
                                            <h6 class="user-title">
                                                John Mayers
                                            </h6>
                                            <div class="user-role">
                                                Account Manager
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="chat-messages">
                                    <div class="message">
                                        <div class="message-content">
                                            Hi, how can I help you?
                                        </div>
                                    </div>
                                    <div class="date-break">
                                        Mon 10:20am
                                    </div>
                                    <div class="message">
                                        <div class="message-content">
                                            Hi, my name is Mike, I will be happy to assist you
                                        </div>
                                    </div>
                                    <div class="message self">
                                        <div class="message-content">
                                            Hi, I tried ordering this product and it keeps showing me error code.
                                        </div>
                                    </div>
                                </div>
                                <div class="chat-controls">
                                    <input class="message-input" placeholder="Type your message here..." type="text">
                                    <div class="chat-extra">
                                        <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>

  <?php $this->load->view('admin/footer'); ?>

  <script>
    function get_district() 
        {
            var division_id=$("#division_id").val();

            $.ajax({
                url: "<?php echo site_url('admin/get_district');?>",
                type: "post",
                data: {division_id:division_id},
                success: function(msg)
                {
                    // alert(msg);
                   $("#district_id").html(msg);
                }      
            });  
        }



        function get_area() 
        {
            var district_id=$("#district_id").val();

            $.ajax({
                url: "<?php echo site_url('admin/get_area');?>",
                type: "post",
                data: {district_id:district_id},
                success: function(msg)
                {
                    //alert(msg);
                   $("#area_id").html(msg);
                }      
            });  
        }
  </script>

</body>

</html>