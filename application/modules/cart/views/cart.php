<?php $this->load->view('front/headlink');?>
  <body>
        <!--Header Start-->
        <?php $this->load->view('front/head_nav');?>
        <!--End of Header Area-->
         <!--Cart Main Area Start-->
        <div class="cart-main-area area-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="page-title">
                            <h1>Shopping Cart</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <form action="order" method="post">
                            <div class="cart-table table-responsive" id="shopping-cart-table">
                                <?php $total=0; if(count($cart= $this->cart->contents())>0){ ?>
                                <?php $cart= $this->cart->contents(); ?>
                                <table >
                                    <thead>
                                        <tr>
                                            <th class="p-image"><strong>Image</strong></th>
                                            <th class="p-name"><strong>Product Name</strong></th>
                                            <th class="p-edit"><strong>Color</strong></th>
                                            <th class="p-edit"><strong>Size</strong></th>
                                            <th class="p-amount"><strong>Price(&#x9f3;)</strong></th>
                                            <th style="width: 90px" class="p-quantity"><strong>Qty</strong></th>
                                            <th class="p-total"><strong>SubTotal(&#x9f3;)</strong></th>
                                            <th class="p-times"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i=1;foreach ($cart as $item){ ?>
                                        <tr>
                                            <td class="p-image">
                                                <a href="product/product_details/<?=$item['profile_name'];?>"><img style="height: 95px;width: 70px" alt="" src="uploads/product/<?=$item['image'];?>" class="floatleft"></a>
                                            </td>
                                            <td class="p-name"><a href="product/product_details/<?=$item['profile_name'];?>"><?=$item['name'];?></a></td>
                                            <td><i class="fa fa-square" style="color: <?=$item['color_name'];?>;font-size: 25px"></i></td>
                                            <td ><?=$item['size_name'];?></td>
                                            <td class="p-amount"><?php if($item['discount']){?><s><?=number_format($item['price'],2);?></s><p><?=number_format(($item['price']-(($item['price']*$item['discount'])/100)),2);?></p><?php } else { ?><?=number_format($item['price'],2);}?></td>
                                            <?php $total+=($item['qty']*($item['price']-(($item['price']*$item['discount'])/100)));?>
                                            <td class="p-quantity"><input style="width: 100%" maxlength="12" type="number" value="<?=$item['qty'];?>" name="quantity"></td>
                                            <td class="p-total"><?=number_format(($item['qty']*($item['price']-(($item['price']*$item['discount'])/100))),2);?> &#x9f3;</td>
                                            <td class="p-action"><a href="javascript:0" onclick="remove_cart('<?=$item['rowid'];?>')"><i class="fa fa-times"></i></a></td>
                                        
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                            <td colspan="6" style="text-align: right;"><strong>Total:</strong></td>
                                            <td style="text-align: right;font-weight: 600"><?=number_format($total,2);?> &#x9f3;</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php } else { ?>
                                    <h4>Your Cart is Empty</h4>
                                    <?php } ?>
                                <div class="all-cart-buttons">
                                    <a href="product"><button class="button" type="button"><span>Continue Shopping</span></button></a>
                                    <?php if(count($this->cart->contents())>0){ ?>
                                    <div class="floatright">
                                        <a href="cart/destroy"><button class="button clear-cart" type="button"><span>Clear Shopping Cart</span></button></a>
                                        <!-- <button class="button" type="button"><span>Update Shopping Cart</span></button> -->
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="row">
                               
                                <div class="col-md-offset-4 col-md-4">
                                    <div class="shipping-discount">
                                        <div class="shipping-title">
                                            <h3>Payment Method</h3>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="postal-code">
                                                    <input checked type="radio" style="width: auto;float: left;margin-top: 20px">
                                                    <img src="front_assets/img/cod.png" alt="" style="width: 90px;">
                                                </div>
                                                <div class="buttons-set">
                                                    
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <input type="hidden" name="charge" value="60">
                                    <input type="hidden" name="total" value="<?=$total;?>">
                                    <div class="amount-totals">
                                        <p class="total">Subtotal <span>&#x9f3; <?=number_format($total,2);?></span></p>
                                        <p class="total" style="font-weight: 400;font-size: 13px;">Delivery Charge <span>&#x9f3; <?=number_format(60,2);?></span></p>
                                        <p class="total" style="font-weight: 700;font-size: 14px;">Grandtotal <span>&#x9f3; <?=number_format(($total+60),2);?></span></p>
                                        <button class="button" type="submit"><span>Place Your Order</span></button>
                                        <div class="clearfix"></div>
                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--End of Cart Main Area-->  

        <?php $this->load->view('front/footer');?>
        <?php $this->load->view('front/footerlink');?>
      
    </body>
</html>