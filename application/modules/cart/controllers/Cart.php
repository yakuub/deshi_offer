<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends MX_Controller {


    function __construct()
	{
		parent::__construct();

		$this->load->library('cart');
		$this->load->model('admin/admin_model');
		$this->load->model('home/home_model');
		$this->load->model('product/product_model');
    }
	public function index()
	{
		/// Common Date Start
			
			$data['active']='product';
			$data['header']=$this->home_model->select_with_where('*','id=1','company');
			$data['brand']=$this->home_model->select_all_acending('brand','brand_name');
			$data['category']=$this->home_model->select_all_acending('category','position');
			$data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
		// Common Data End
		
		// page Data End
		$this->load->view('cart',$data);
	}
	
	
	
	
public function add()
{
	$id =$this->input->post('product_id');
	$color_id =$this->input->post('color_id');
	$qty =$this->input->post('qty');
	$size_id =$this->input->post('size_id');
	$size_name=$this->admin_model->select_with_where('*','id='.$size_id,'size');
	$size_name=$size_name[0]['size_code'];
	$color_name=$this->admin_model->select_with_where('*','id='.$color_id,'color');
	$color_name=$color_name[0]['color_code'];

	$data['product_details']=$this->admin_model->select_with_where('*', 'p_id =' . $id, 'product');
	$product_name=$data['product_details'][0]['p_name'];
	$profile_name=$data['product_details'][0]['profile_name'];
	$product_price=$data['product_details'][0]['price'];
	$product_discount=$data['product_details'][0]['discount'];
	$image=$data['product_details'][0]['main_image'];
	$store_qty=$data['product_details'][0]['p_qty'];

	if($store_qty>=1)
	{

		$product_name = preg_replace('![^'.preg_quote('-').'a-z0-_9\s]+!', '', strtolower($product_name));
	// Replace all separator characters and whitespace by a single separator
		//$product_name = preg_replace('!['.preg_quote('-').'\s]+!u', '-', $product_name);

		// $product_name = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $product_name);
		// $product_name = preg_replace('/[[:^print:]]/', '', $product_name);
			
		
		$insert_data = array(
			'id' => $data['product_details'][0]['profile_name'].'_'.$color_id.'_'.$size_id,
			'product_id'=>$id,
			'name' => $product_name,	
			'profile_name' => $profile_name,	
			'color_id' => $color_id,	
			'size_id' => $size_id,
			'color_name' => $color_name,	
			'size_name' => $size_name,	
			'discount' => $product_discount,	
			'price' => $product_price,
			'qty' => $qty,
			'image' => $image,
		);

		$flag=TRUE;
		$check_product= $this->cart->contents();


		
		foreach ($check_product as $item) 
		{

            if (($item['id'] == $id)  )
            {
            
                $data = array(
                	'rowid'=>$item['rowid'],
                	'qty'=>$item['qty']+$qty
                	);
           		$this->cart->update($data);
				//$msg='Cart Item Successfully Updated';
                $flag = FALSE;
                break;
            }       
        }
     
        if($flag)
        {

        	$this->cart->insert($insert_data);
        }
		
		//print_r($check_product);die();
		
	}

	else
	{
		$this->session->set_userdata('qty_error','Product is out of stock !');
	}
}
public function destroy() 
{	
	$this->cart->destroy();
	redirect('cart','refresh');
}

public function remove() 
{
// Check rowid value.
	$rowid=$this->input->post('row_id');
	//echo $rowid;
	
	if ($rowid==="all")
	{
		// Destroy data which store in session.
		$this->cart->destroy();
	}
	else
	{
		// Destroy selected rowid in session.
		$data = array(
		'rowid' => $rowid,
		'qty' => 0
		);
	// Update cart data, after cancel.
		$this->cart->update($data);
	}

// This will show cancel data in cart.
	//redirect('shopping');
}



public function update_cart()
{

    // Recieve post values,calcute them and update

    $row_id = $_POST['row_id'] ;
    $qty = $_POST['qty'] ;
    $price = $_POST['price'] ;

    //echo '<pre>';
    //print_r($cart_info);
    //echo '</pre>';
     $cart_info= $this->cart->contents();


    foreach( $cart_info as $id => $cart)
    {
        if($cart['rowid']==$row_id)
        {

            $rowid = $cart['rowid'];
            $price = $price;
            $amount = $price * $qty;
            $qty = $qty;
           // echo $cart['id'];
             $p_id=$cart['id'];
             $get_qty=$this->admin_model->select_with_where('p_qty', 'p_id =' . $p_id, 'product');

            if($qty<=$get_qty[0]['p_qty'])
            {
	            $data = array(
	                'rowid' => $rowid,
	                'price' => $price,
	                'amount' => $amount,
	                'qty' => $qty
	                );
	            $this->cart->update($data);
	        }

	        else
	        {
	        	$this->session->set_userdata('qty_error','Product "Quantity" is out of stock !');
	        }
        }
    }
}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */