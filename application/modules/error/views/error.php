<?php $this->load->view('front/header_link');?>
   
    <body>
        <?php $this->load->view('front/header');?>
        <div class="clear"></div>       
        <section class="content-wrapper">
          <div class="container">
            <div class="std">
              <div class="page-not-found wow bounceInRight animated">
                <h2>404</h2>
                <h3><img src="images/signal.png" alt="signal">Oops! The Page you requested was not found!</h3>
                <div><a href="home" class="btn-home"><span>Back To Home</span></a></div>
              </div>
            </div>
          </div>
        </section>

       <?php $this->load->view('front/footer');?>
      <!-- Footer End-->
    

       
      <!-- Footer Link Starts-->

       <?php $this->load->view('front/footer_link');?>
      <!-- Footer Link End-->


    </body>
    
</html>
