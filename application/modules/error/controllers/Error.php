<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Error extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->model('home/home_model');
        if($this->session->userdata('language_select')=='bangla')
        {
            $this->lang->load('front', 'bangla');
        }
        else
        {
            $this->lang->load('front', 'english');
        }

    }

    public function index() {
        // Common Date Start
            $data['active']='Adds';
            $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
            $data['header']=$this->home_model->select_all('company');
        // Common Data End
      
        $this->load->view('error',$data);
       // $this->load->view('public_layouts/footer');
    }








}
