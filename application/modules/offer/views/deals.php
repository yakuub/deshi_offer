<?php $this->load->view('front/headlink');?>

<style>
  .redBackground{color:red}
  </style>

    <body>
        <?php $this->load->view('front/head_nav');?>
            <?php $this->load->view('front/mega_menu');?>

                <!--banner-->
                <!-- <div class="container">
                    <div class="col-md-12 f_w_ad">
                        <div class="col-sm-12">
                            <a target="_blank" href="#"><img src="front_assets/images/ad_banner.png" alt="" class="img-responsive"></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div> -->
                <!--banner end-->

                <div class="container product_filter">
                    <h1><?=$cat_name;?></h1>
                    <h4><?=$total_offer;?> AVAILABLE OFFERS</h4>
                    <div class="cat_follow">FOLLOW THIS CATEGORY</div>
                    <hr />
                    <h2><span> &#61616; </span> Filter Products</h2>
                    <span class="form_close">&times;</span>

                    <!-- <input type="hidden" id="offer_highlights_id" value="<?=$cat_id[0]['id'];?>"> -->

                     <form action="">
                          <select name="" id="offer_type" onchange="store_offer_list_ajax()">
                             <option value="0">Offer Type</option>
                             <?php foreach ($offer_type as $key => $value) {?>
                                <option value="<?=$value['offer_id'];?>"><?=$value['offer_type_tilte'];?></option>
                             <?php } ?>
                          </select>
                          <span></span>
                          <!-- <select name="" id="offer_category" onchange="store_offer_ajax()">
                             <option value="0">Offer Category</option>
                             <?php foreach ($offer_category as $key => $value) {?>
                                <option value="<?=$value['catagory_id'];?>"><?=$value['catagory_title'];?></option>
                             <?php } ?>
                          </select>
                          <span></span> -->
                         <!--  <select name="" id="location_id" onchange="store_offer_ajax()">
                             <option value="0">Location</option>
                             <?php foreach ($division as $key => $value) {?>
                                <option value="<?=$value['id'];?>"><?=$value['name'];?></option>
                             <?php } ?>
                          </select>
                          <span></span> -->
                          <select name="" id="offer_store_type" onchange="store_offer_list_ajax()">
                             <option value="">Both</option>
                             <option value="0">Store Offer</option>
                             <option value="1">Online Offer</option>
                          </select>
                          <span></span>
                          <select name="" id="duration" onchange="store_offer_list_ajax()">
                             <option value="0">Duration</option>
                             <option value="1">24 Hours</option>
                             <option value="2">3 Days</option>
                             <option value="7">7 Days</option>
                             <option value="15">15 Days</option>
                             <option value="30">30 Days</option>
                          </select>
                          <span></span>
                       </form>
                  </div>

                    <div class="container">
                        <!--product section one-->
                        <div class="col-md-12 product_section">
                            <div id="loading_img_store_offer" style="display:none;position: fixed;z-index: 10;top: 33%;left: 45%;width: 10%;">
                              <img style="width: 100%" src="front_assets/loading-spinner-blue.gif">     
                            </div>
                            <div class="product_wrap col-md-12" id="offer_show_div">
                                <?php $this->load->view('home/include/promo_offer');?>
                            </div>
                            <!--product wrap end-->

                        </div>
                        <!--product section one end-->
                        <!-- <div class="col-md-12 f_w_ad">
                            <div class="col-sm-12">
                                <a target="_blank" href="#"><img src="front_assets/images/ad_banner.png" alt="" class="img-responsive"></a>
                            </div>
                        </div> -->
                        <!--banner end-->


                        <div class="container">
                          <div class="col-md-12 mail_subscribe">
                          <div class="col-md-6 subscribe_title"><img src="front_assets/images/subscribe.svg" alt="subscribe to Deshioffer">
                            <p>Subscribe to email alerts for new Offers & Deals</p>
                            <div class="clearfix"></div>
                          </div>
                          <form class="col-md-6 subscribe_input" action=""><input type="text" id="subscribe" placeholder="Enter your Email to subscribe"><input value="▶" type="submit"></form>
                          <div class="clearfix"></div>
                        </div>
                  </div>
                  <!--product section two-->
                  <div class="col-md-12 product_section" id="product_section_extra">
                    <div class="product_wrap col-md-12">
                      <?php if($total_offer>8){?>
                        <?php $this->load->view('include/category_offer'); ?>
                      <?php } ?>
                    </div><!--product wrap end-->
                    
                  </div> <!--product section two end-->
                       
                    </div>
                    

        <?php $this->load->view('front/footer');?>
        <?php $this->load->view('front/footerlink');?>

        <script>
            $(".floating_nav").hide();
            $(".subscribe").click(function() {
                $(".subscribe_fix_div").fadeIn(200);
                $(".subscribe_fix_div").css({
                    "display": "-webkit-box",
                    "display": "-ms-flexbox",
                    "display": "flex"
                });
                $(".subscribe_fix_div .form_close").fadeIn(200);
            });
            $(".subscribe_fix_div .form_close").click(function() {
                $(".subscribe_fix_div").fadeOut(200);
                $(".subscribe_fix_div .form_close").fadeOut(200);
            });

            function search_ctrl() {
                var width = $(window).width();
                if (width < 767) {
                    $(".form_wrap").css("display", "none");
                    $(".search_btn").attr("value", "Go");
                } else {
                    $(".form_wrap").css("display", "block");
                    $(".search_btn").attr("value", "Show Offers");
                }
            }
            $(".src_btn").stop().click(function() {
                if ($(window).width() < 767) {
                    $(this).siblings(".form_wrap").slideToggle(300);
                } else {
                    $(this).siblings(".form_wrap").slideDown(0);
                }
            });
            search_ctrl();
            $(window).on("resize", search_ctrl);
            $(".post_offer_btn, .pop_up_wrap").hover(function() {
                $(".pop_up_wrap").stop().fadeIn(200);
            }, function() {
                $(".pop_up_wrap").fadeOut(200);
            });
        </script>

        <script>
            $('.droppable').hover(function() {
                $('.dim').fadeIn(0);
            }, function() {
                $('.dim').fadeOut(0);
            });
        </script>
        <script>
            $(".form-fg-pass").css("display", "none");
            $(".recover_pass").click(function() {
                $(".form-signin").css("display", "none")
                $(".form-fg-pass").css("display", "block")
                    // $("#").show();   
            });

            $(".b2login").click(function() {
                $(".form-signin").css("display", "block")
                $(".form-fg-pass").css("display", "none")

                // $("#").show();   
            });
        </script>

        <script>

          function add_favourite(offer_id) 
          {
            var login_id="<?=$this->session->userdata('login_id');?>"

             if(login_id=='')
              {
                $("#login_modal").modal('show');
                
              }
           
            else{
              $.ajax({
                        url: "<?php echo site_url('user/add_favourite');?>",
                        type: "post",
                        data: {offer_id:offer_id,login_id:login_id},
                        success: function(msg)
                        {
                            // alert(msg);
                            console.log(msg);
                           $('#favourite_'+offer_id).notify("Added To Favourite", "success");
                          

                        }      
                    });
              }  
            


          }
        </script>


        <script>

          $('.favourite').on('click',function(){
            $(this).toggleClass('redBackground');
        });

        </script>


       <!--  <script>
          function store_offer_list_ajax() 
          {
             $("#loading_img_store_offer").show();
             $("#product_section_extra").hide();
             var offer_highlights_id=$("#offer_highlights_id").val();;
             var offer_type=$("#offer_type").val();
             var offer_category=$("#offer_category").val();
             var location_id=$("#location_id").val();
             var offer_store_type=$("#offer_store_type").val();
             var duration=$("#duration").val();

             $.ajax({
                  url: "<?php echo site_url('offer/deal_offer_ajax');?>",
                  type: "post",
                  data: {offer_highlights_id:offer_highlights_id,offer_type:offer_type,offer_category:offer_category,location_id:location_id,offer_store_type:offer_store_type,duration:duration},
                  success: function(msg)
                  {
                      $("#offer_show_div").html(msg);
                      $("#loading_img_store_offer").hide();
                  }      
             }); 
          }
        </script> -->

    </body>

    </html>