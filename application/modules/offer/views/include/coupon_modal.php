
<div class="modal-dialog modal-lg">
  <div class="col-md-12">
    <div class="Qr_form">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <span class="text-center">
          <img style="width: 30px;height: 30px;" src="front_assets/images/white.png"class=" col-md-offset-6" >
          <h5 class="text-center mr_auto" style="color:#1188EF;">
            <b><?=$offer_details[0]['offer_title'];?></b>
          </h5>
        </span>
      </div>
      <div class="modal-body ">


        <div class="col-md-12" align="center" style="margin-bottom: 10px;">

          <span id="offer_activate" style="display: none;"  class="copun_input form-control"><p style="color: red; font-size: 20px;font-weight: bold">OFFER ACTIVATED</p>
          </span>

        </div>

        <div class="col-md-12 text-center" style="background-color: #f6f6f6">

          <h5>
            <b>Copy this code & use during checkout at &nbsp;<span style="color:blue;"><a href="store/offer/<?=$store_info[0]['name'];?>"><?=$store_info[0]['username'];?></a></span></b>
          </h5>
          <br>
          



          <div class="col-md-12">



            <div class="coupon_code_form">
             <div class="col-md-6 in-box-num col-md-offset-2" style="padding: 0"><input type="text" value="<?=$offer_details[0]['utm_code'];?>" class="form-control" id="coupon_btn_txt_<?=$offer_details[0]['main_off_id'];?>" readonly></div>
             <div class="col-md-2 in-box-sub" style="padding-left: 0;">
              <a target="_blank" id="url_link_<?=$offer_details[0]['main_off_id'];?>" href="<?=$offer_details[0]['utm_link'];?>"></a>
              <input type="submit" id="coupon_btn_<?=$offer_details[0]['main_off_id'];?>" value="COPY" class="btn btn-primary btn-response-100" onclick="copy_cod('<?=$offer_details[0]['main_off_id'];?>')">
            </div>
            
          </div>

        </div>
        
        <div class="row text-center" style="font-size: 14px;margin-top:55px !important;">
          <div class="col-md-8 col-md-offset-2 col-xs-4">

            <b>  Get Registered to get <span style="color:#10da10;">Cashback</span> every of your purchase&nbsp;<i class="fa fa fa-caret-right sof_icon_3"></i>&nbsp;<span style="color:red;" data-toggle="modal" data-target="#login_modal">Sign Up</span></b>
          </div>

        </div>

        <hr>
      </div>
      <br>
      <div class="text-center" style="margin-bottom: 10px;">
        <div class="col-md-12" style="margin-top: 20px;">
          <div class="col-md-6">
            <span>View<span style="color:#45BF44;font-weight: bold; "> <?=$total_offer;?></span> more <a href="store/offer/<?=$store_info[0]['name'];?>"><?=$store_info[0]['username'];?></a> Offers</span>

          </div>
          <div class="col-md-6">
            <span class="text-center"><span style="color:red;">Offers Ends</span><?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($offer_details[0]['edate']);
            $diff = $date2 - $date1;
            $day=round($diff / 86400);?>
            <?php if($day>0){?>
             Expire in <?=$day;?> Days.
           <?php } else { ?>
            Expire in Today
            <?php } ?></span>

          </div>
        </div>
      </div>

    </div>

  </div>

</div>
</div>
