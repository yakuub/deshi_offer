<div class="modal-dialog">
  <div class="modal-content md-sms-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div align="center">
        <img style="width: 50px;height: 50px;" src="front_assets/images/white.png">
        <h5 class="text-center mr_auto" style="color:#1188EF;">
          <b><?=$offer_details[0]['offer_title'];?></b>
        </h5>
      </div>
    </div>
    <div class="modal-body col-md-12 mb-body-sms">

      <span class="form-control text-center" style="color: red;font-style: italic;display: none" id="num_validation">Please Enter Mobile Number Please</span>

      <div id="loading_img" style="display:none;position: fixed;z-index: 10;top: 40%;left: 45%;width: 9%;">
       <img style="width: 100%" src="front_assets/loading-spinner-blue.gif">     
     </div>

     <div class="alert alert-danger alrt_form" id="err_div" style="display: none">
      <label id="m_msg"></label>
    </div>

    <div class="alert alert-info alrt_form" id="s_div" style="display: none">
      <label id="s_msg"></label>
    </div>

    <div class="form-sms col-md-12">
      <p class="text-center">Please enter your phone number, this will text this offer to your phone</p>
      <div class="form-inline">
       <input type="hidden" value="<?=$offer_id;?>" name="offer_id" id="offer_id_modal">

 
         <input type="hidden" id="sms_msg" name="Message" value="<?=$sms_offer_info[0]['sms']?>"/>


         <div class="col-md-2 col-xs-4  text-center p-response" style="padding-right: 0"><span style="background: #cccccc7d;padding: 8px 8px;display: block;font-weight: bold">+88</span>
         </div>
         <div class="col-md-7 col-xs-8  in-box-num" style="padding: 0">
          <div class="input-group" style="width: 100%;">
            <input type="text" id="number_to" name="To" maxlength="11" class="form-control" placeholder="Enter your 10 digit number here" style="height: 36px">
          </div>
        </div>
        <div class="col-md-3 in-box-sub" style="padding-left: 0;">
          <input type="submit" value="TEXT ME" onclick="send_sms_ajax()" class="btn btn-primary btn-response-100">
        </div> 
      <!-- </form> -->


    </div>

  </div>

  <p class="text-center">Use the SMS to avail the discount</p>

  <p style="text-align: center; display: block;">Visit <a href="store/offer/<?=$store_info[0]['username'];?>"><span style="color: #6D7DB5"><?=$store_info[0]['username'];?></span></a> outlet in <?=$store_info_details[0]['address']?></p>


</div>

<div class="modal-footer" style="border: 0">
  <div class="col-md-6">
    <p class="txt-res-left"> <span>View<span style="color:#45BF44;font-weight: bold; "> <?=$total_offer;?></span> more <a href=""><?=$store_info[0]['username'];?></a> Offers</span></p>

  </div>

  <div class="col-md-6"><p><span style="color:red;">Offer Ends:</span> <?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($offer_details[0]['edate']);
  $diff = $date2 - $date1;
  $day=round($diff / 86400);?>
  <?php if($day>0){?>
   Expire in <?=$day;?> Days.
 <?php } else { ?>
  Expire in Today
  <?php } ?></p></div>

</div>

</div>
</div>
