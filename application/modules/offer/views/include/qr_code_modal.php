
<div class="modal-dialog modal-lg">
  <div class="col-md-12">
    <div class="Qr_form">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div align="center">
          <img style="width: 30px;height: 30px;" src="front_assets/images/white.png">
          <h5 class="text-center mr_auto" style="color:#1188EF;">
            <b><?=$offer_details[0]['offer_title'];?></b>
          </h5>
        </div>
      </div>
      <div class="modal-body">
        <div class="col-md-5">
          <img style="width: 250px;
          height: 230px;
          padding: 10px;" src="front_assets/images/qr_img.png">
        </div>

        <div class="col-md-6 text-center">

          <h5>
            <b>Print Or Save the QR to get the exclusive discount!!</b>
          </h5>
          <br>
          <div class="col-md-12">
            <div class="col-md-6">
              <a href="<?=$qr_image;?>" class="btn btn-info btn-block btn_width" style="width:100%; !important">Print QR</a>
            </div>

            <div class="col-md-6">
              <button class="b2login btn btn-info btn-block btn_width" type="submit" onclick="save_voucher_image()" style="width:100%; !important">Save QR</button>
            </div>
          </div>
          <div class="row text-center" style="font-size: 14px;margin-top:50px !important;">
            <div class="col-md-8 col-md-offset-2 col-xs-4">
              <i class="fa fa-clock-o text-danger sof_icon_4">&nbsp;Ends In:</i> &nbsp;<?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($offer_details[0]['edate']);
              $diff = $date2 - $date1;
              $day=round($diff / 86400);?>
              <?php if($day>0){?>
               Expire in <?=$day;?> Days.
             <?php } else { ?>
              Expire in Today
            <?php } ?>
          </div>
          <div class="col-md-8 col-md-offset-2 col-xs-4 text-center" style="font-size: 12px;margin-top:20px !important;">

            <i class="fa fa-eye  sof_icon_3"></i> View: <?=$total_offer;?> <a href="store/offer/<?=$store_info[0]['name'];?>"><?=$store_info[0]['username'];?></a> Offer
          </div>
        </div>
        <hr>
      </div>

    </div>

  </div>

</div>
</div>
