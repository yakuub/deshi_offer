<?php $this->load->view('front/headlink');?> 

<style type="text/css">
  .rating-xs
  {
    font-size: 20px;
  }
</style>

<?php $this->load->view('front/head_nav');?> 
<?php $this->load->view('front/mega_menu');?> 

<div class="container">

  <div class="row">
    <div class="offr_details col-md-9">
     <div class="offr_d_br">
      <div class="col-md-6 text-center" style="
      padding-top: 12px;
      ">
      <span class="btn offer_exl " style="background: <?=$offer_details[0]['offer_color_code'];?>"><?=$offer_details[0]['offer_type_tilte'];?></span>
      <div class="single_offer_img">
        <img src="uploads/offer/<?=$offer_details[0]['offer_featured_file'];?>" alt="" width="auto" style="margin-bottom: 20px; max-width: 300px;
        max-height: 280px;">

      </div>
      <div class="col-md-12 v_all_off">
        <span><i class="fa fa-shopping-bag" style="color: #8914A8;"></i>
          <a href="store/offer/<?=$store_info[0]['name'];?>"><span class="text-danger"><strong>View all (<?=$store_total_ads;?>) </strong></span>
            <?=$store_info[0]['username'];?> Offers
          </span></a>
        </div>
      </div>

      <div class="col-md-1 separator"></div>
      <div class="col-md-6 offer-details-s" style="
      padding-top: 0px; 
      ">
      <div class="single_offer_details text-center">
        <h3><?=$offer_details[0]['offer_title'];?></h3>

        <div class="col-md-12 text-center">
         <span class="product_price">&#2547;<?=$offer_details[0]['price'];?></span>

       </div>
       <div class="col-md-12 text-center">
        <span class="previous_price">Regular Price &nbsp;<span class="price_line_mark">&#2547;<?=$offer_details[0]['price'];?></span></span>
      </div>

      <div class="percent_off col-md-12 ">

       <div class="col-md-3  text-center">
         <span class="" style="font-size: 15px; ">
           &#2547;<?=$offer_details[0]['discount'];?>
         </span>
         <p>You Save</p>
       </div>

       <div class="col-md-3 text-center border_ryt">

         <span><?=$offer_details[0]['price'];?></span>
         <p class="">Discounts</p>
       </div>
       <!-- <div class="col-md-1 br_sof"></div> -->
       <a href="offer/offer_payment/<?=$offer_details[0]['main_off_id']?>">
         <div class=" col-md-4  col-md-offset-1 btn btn_width" style="background-color:<?=$offer_details[0]['color_code'];?>;color: white">
           <img src="front_assets/images/btnimg.svg" class="offer_btn_icon border_ryt_white">&nbsp;
           BUY NOW
         </div>
       </a> 

     </div>

     <div class="cold-md-12 sof_dt"> 
      <div class="row text-center" style="font-size: 12px">
        <div class="col-md-8 col-md-offset-2 col-xs-4"> <i class="fa fa-clock-o text-danger sof_icon_4">&nbsp;Ends In:</i> &nbsp;
          <?php
          $datetime1 = new DateTime($offer_details[0]['edate']);
          $datetime2 = new DateTime(date('Y-m-d H:i:s'));
          $interval = $datetime1->diff($datetime2);
          echo $interval->format('%d')." Days ".$interval->format('%h').":".$interval->format('%i').":".$interval->format('%s'); ?>

        </div>
      </div>
    </div>
    <br>


    <div class="cold-md-12 rv_dt  border_top_bottom" style="padding-bottom:5px;padding-top:10px;">
      <span><i class="fa fa-check-circle sof_icon"></i>&nbsp;&nbsp;Verified Deal</span>
      <span><i class="fa fa-group sof_icon_2"></i>&nbsp;<?=count($order_details)?> Bought Already</span>

      <?php if($this->session->userdata('type')==3 && !in_array($offer_details[0]['main_off_id'], explode(',', $user_details[0]['fav_offer']))){?>

        <span ><a href="offer/save_offer/<?=$profile_name?>"><i class="fa fa-heart-o sof_icon_3"></i>&nbsp;&nbsp;Save Deal</a>
        </span>

      <?php } else if($this->session->userdata('type')==3 && in_array($offer_details[0]['main_off_id'], explode(',', $user_details[0]['fav_offer']))) {?>

        <span ><a href="offer/save_offer/<?=$profile_name?>"><i class="fa fa-heart sof_icon_3"></i>&nbsp;&nbsp;Already Saved
        </a>
      </span>
    <?php } else {?>

     <span ><a href="javaScript:void(0)" data-toggle="modal" data-target="#login_modal"><i class="fa fa-heart-o sof_icon_3"></i>&nbsp;&nbsp;Save Deal</a>
     </span>

   <?php } ?>

 </div>

 <?php if($offer_details[0]['offer_store_type']==0){?>
  <div class="col-md-12 rv_loc" style="padding: 7px; padding-left: 12px;">
    <div class="row">
      <span><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?=$store_info[0]['address'];?></span>
    </div>
  </div>
<?php } ?>

<div class="col-md-12" style="padding: 12px;">
  <div class="row text-align-response">
    <div class="col-md-6 col-xs-6 social-links-off" style="">
      <span>
        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fdeshioffer.com%2Foffer%2Foffer_details%2F<?php echo $offer_details[0]["profile_name"];?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore"><span class="fb-share-button" data-href="<?php echo base_url()?>offer/offer_details/<?php echo $offer_details[0]["profile_name"];?>" data-layout="button" data-size="small"><i class="fa fa-facebook rv_soc_link"></i></span></a>

        <a target="_blank" href="https://twitter.com/share?url=http%3A%2F%2Fdeshioffer.com%2Foffer%2Foffer_details%2F<?php echo $offer_details[0]["profile_name"];?>"><i class="fa fa-twitter rv_soc_link"></i></a>

        <a href="http://pinterest.com/pin/create/button/?url=http%3A%2F%2Fdeshioffer.com%2Foffer%2Foffer_details%2F<?php echo $offer_details[0]["profile_name"];?>" target="_blank"><i class="fa fa-pinterest rv_soc_link"></i></a>

        <a href="https://plus.google.com/share?url=http%3A%2F%2Fdeshioffer.com%2Foffer%2Foffer_details%2F<?php echo $offer_details[0]["profile_name"];?>" target="_blank"><i class="fa fa-google-plus rv_soc_link"></i></a>


        <a href="https://www.instagram.com/deshi_offer/" target="_blank"><i class="fa fa-instagram rv_soc_link"></i></a>



      </span>
    </div>




    <div class="col-md-6 col-xs-6 border_lft">
      <?php if($this->session->userdata('type')==3){ ?>




        <?php if ($rating_status=="done") { ?>

          <input id="" readonly="" value="<?=$offer_rating_individual[0]['rating']?>" name="user_rating" type="text"  class="rating" data-size="xs">

          <span>&nbsp;&nbsp;<?=count($rating_details)?> Reviewed</span>

        <?php } else { ?>

          <input id="" readonly="" value="" name="user_rating" type="text"  class="rating" data-size="xs">

          <a href="javaScript:void(0)" data-toggle="modal" data-target="#review_modal">
            &nbsp;&nbsp;<?=count($rating_details)?> Review</a>


          <?php } ?>




        <?php } else { ?>


         <input id="" readonly="" value="<?=$offer_rating?>" name="user_rating" type="text"  class="rating" data-size="xs">


         <a href="javaScript:void(0)" data-toggle="modal" data-target="#login_modal">
          &nbsp;&nbsp;<?=count($rating_details)?> Review</a>

        <?php } ?>
      </div>




      <!-- Load Facebook SDK for JavaScript -->




    </div>
  </div>




</div>

</div>
</div>
</div>





<div class="col-md-3">
  <div class="side-banners">
   <img src="front_assets/images/sidebar-bn.png" alt="" width="100%;">
 </div>
</div>
</div>




<div class="col-md-9">



  <ul class="nav nav-tabs navtab_offer">
    <li class="active"><a data-toggle="tab" href="#about_offer">About this offer</a></li>
    <li><a data-toggle="tab" href="#review_offer">Reviews-<?=count($rating_details)?></a></li>

  </ul>

  <div class="tab-content tabcnt_offer">
    <div id="about_offer" class="tab-pane fade in active">
     <div class="" style="padding:20px">
      <?=$offer_details[0]['offer_terms'];?>
    </div>


    <div class="deals_specification" style="margin-top:20px">
      <h4 style="border-bottom: 1px solid #80808073;padding-bottom:17px;">Deals Specification</h4>
      <div class="" style="margin-top:10px;">
        <div class="col-md-6">
          <ul class="off-dt-list">
            <?=$offer_details[0]['offer_desc'];?>
          </ul>
        </div>
        <div class="col-md-6">
          <ul class="off-dt-list">
            <?=$offer_details[0]['offer_terms'];?>
          </ul>
        </div>
      </div> 
    </div>



    <div class="avail  mrgin_top">
      <h4 style="border-bottom: 1px solid #80808073;padding-bottom:17px;">How to avail</h4>
      
      
      <ul class="off-dt-list">

      </ul>



    </div>






    <div class="off_m_location">
      <div class="map_off col-md-8">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4341.922886055763!2d90.4120701455606!3d23.7791899050831!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x19bd7392015966ba!2sBagdoom.com!5e0!3m2!1sen!2sbd!4v1519810466097" width="100%" height="285" frameborder="0" style="border:0" allowfullscreen=""></iframe>
      </div>
      <div class="col-md-4">
        <div class="loc_dt_offer">
          <h5 class="text-center"><a href="store/offer/<?=$store_info[0]['name'];?>"><?=$store_info[0]['username'];?></a></h5>
          <ul class="loc_dt_ul" style="padding: 0; text-align: left;">

           <li><div class="row"><span class="col-md-2"><i class="fa fa-hand-o-right" aria-hidden="true"></i></span><span class="col-md-10"><?=$store_info[0]['address'];?></span></div></li>
           <li><div class="row"><span class="col-md-2"><i class="fa fa-phone-square" aria-hidden="true"></i></span><span class="col-md-10"><?=$store_info[0]['contact'];?></span></div></li>
           <?php if($store_info[0]['support_email']!=''){?>
             <li><div class="row"><span class="col-md-2"><i class="fa fa-inbox" aria-hidden="true"></i></span><span class="col-md-10"><?=$store_info[0]['support_email'];?></span></div></li>
           <?php } ?>
           <?php if($store_info[0]['website_link']!=''){?>
            <li><div class="row"><span class="col-md-2"><i class="fa fa-inbox" aria-hidden="true"></i></span><span class="col-md-10"><?=$store_info[0]['website_link'];?></span></div></li>
          <?php } ?>
          <?php if($store_info[0]['facebook_link']!=''){?>
           <li><div class="row"><span class="col-md-2"><i class="fa fa-facebook-square" aria-hidden="true"></i></span><span class="col-md-10"><?=$store_info[0]['facebook_link'];?></span></div></li>
         <?php } ?>
       </ul>
     </div>
   </div>

 </div>


</div>


<div id="review_offer" class="tab-pane fade">
  <div class="col-md-12 rv_header">
    <h3 class="pull-left">Customer reviews</h3>
    <?php if($this->session->userdata('type')==3) 
    {

      if($rating_status=="not done") { ?>
        <h3 class="pull-right cl"><a href="javaScript:void(0)" data-target="#review_modal" data-toggle="modal">Write your reviews</a></h3>

      <?php } else { ?>

        <h3 class="pull-right cl"><a href="javaScript:void(0)">Reviewed</a></h3>


      <?php } } else { ?>

        <h3 class="pull-right cl"><a href="javaScript:void(0)" data-target="#login_modal" data-toggle="modal">Write your reviews</a></h3>

      <?php } ?>

    </div>




    <div class="off_reviews">

      <?php foreach ($rating_details as $key => $value) { ?>

       <div class="col-md-6 rv_box"> 
        <div class="off_rv_box col-md-12"> 
         <span class="rating-sz">
           <input id="" type="text" readonly="" value="<?=$value['rating']?>" class="rating mr-offr-rating" data-size="xs">
         </span>

         <p class="text-center txt_rv"><?=$value['review']?></p>

         <span class="pull-left rev_name"><?=$value['username']?></span>
         <span class="pull-right rev_date"><i><?=date("d-M-Y", strtotime($value['created_at']));?></i></span>

       </div>

     </div>

   <?php } ?>

 </div>
</div>



</div>

</div>
<div class="col-md-3 rel_off">
  <h4 class="" style=" border-bottom: 1px solid #80808073; padding-bottom: 12px;margin-bottom: 0">Related offers</h4>
  <div class="rel_w_img col-md-12 container">

    <?php foreach ($related_offer as $key => $value) {?>
      <div class="rel_offer "><div class="row hvr-glow" style="overflow:hidden; margin-bottom: 5px;">
        <div class="col-md-3 col-sm-6 pr_0 col-xs-4"><img class="img_moff img-responsive imgreloff" src="uploads/offer/<?=$value['offer_featured_file'];?>" width="100%" height="60px" alt="" style="border: 3px solid #fff;">
        </div>
        <div class="col-md-9 col-sm-6 col-xs-7 rel_off_box">
          <p><a style="text-decoration:none;" href="offer/offer_details/<?=$value['profile_name'];?>"><?=word_limiter(strip_tags($value['offer_title']),7);?></a></p>
        </div></div></div>
      <?php } ?>


    </div>



    <h4 class="" style=" border-bottom: 1px solid #80808073; padding-bottom: 12px;margin-bottom: 0">More Store</h4>
    <div class="m_store col-md-12">

      <div class="row off_bt">
       <div class="col-md-4 col-xs-4"><img class=" img_mstore" src="front_assets/images/boy.jpg"  alt=""></div>
       <div class="col-md-4 col-xs-4"><img class=" img_mstore" src="front_assets/images/boy.jpg"  alt=""></div>
       <div class="col-md-4 col-xs-4"><img class=" img_mstore" src="front_assets/images/boy.jpg"  alt=""></div>

     </div>
     <div class="row off_bt">
       <div class="col-md-4 col-xs-4"><img class=" img_mstore" src="front_assets/images/boy.jpg"  alt=""></div>
       <div class="col-md-4 col-xs-4"><img class=" img_mstore" src="front_assets/images/boy.jpg"  alt=""></div>
       <div class="col-md-4 col-xs-4"><img class=" img_mstore" src="front_assets/images/boy.jpg"  alt=""></div>

     </div>

   </div>


 </div>



</div>




<?php $this->load->view('front/footer');?>
<?php $this->load->view('front/footerlink');?>



<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2"></script>

<script>
  function get_sms_ajax() 
  {

   var number=$("#number").val();

   var offer_id = $('#offer_id_modal').val();

   $("#err_div").hide();
   $("#s_div").hide();

   if(number=='')
   {
    $("#err_div").show();
    $("#m_msg").html('Please Enter your Phone Number');

    return false;
  }
  else if(number.length != 10)
  {
    $("#err_div").show();
    $("#m_msg").html('Please Enter A Valid Phone Number of 10 digit without 0');

    return false;
  }

  $.ajax({
    url: "<?php echo site_url('home/send_sms');?>",
    type: "post",
    data: {number:number,offer_id:offer_id,},
    success: function(msg)
    {

                    //alert(msg);
                    if(msg==0)
                    {
                      $("#err_div").show();
                      $("#m_msg").html('Something Wrong. Please try again');
                    }
                    else if(msg==1)
                    {
                      $("#err_div").show();
                      $("#m_msg").html('Already Sent SMS');
                    }
                    else
                    {
                      $("#s_div").show();
                      $("#s_msg").html('SMS Successfully Send.');
                    }

                  }      
                });  

}
</script>

<script>
  function save_voucher_image() 
  {

   var offer_id = $('#offer_id_modal').val();

   $.ajax({
    url: "<?php echo site_url('home/save_image');?>",
    type: "post",
    data: {offer_id:offer_id,},
    success: function(msg)
    {

                  // window.open(msg);


                }      
              });  

 }
</script>

<script>
  $(".floating_nav").hide();
  $(".owl-slide").owlCarousel({
    items:1,
    responsive: false,
    navigation : true,
    navigationText : ["&#61700;","&#61701;"],
    autoPlay : 2000,
    stopOnHover : true,
    pagination : false,
    paginationNumbers: false
  });
  $(".product_filter h2").click(function(){
    $(".product_filter form").fadeIn(200);
    $(".product_filter .form_close").fadeIn(200);
  });
  $(".product_filter .form_close").click(function(){
    $(".product_filter form").fadeOut(200);
    $(".product_filter .form_close").fadeOut(200);
  });
  $(".subscribe").click(function(){
    $(".subscribe_fix_div").fadeIn(200);
    $(".subscribe_fix_div").css({"display": "-webkit-box", "display": "-ms-flexbox", "display":"flex"});
    $(".subscribe_fix_div .form_close").fadeIn(200);
  });
  $(".subscribe_fix_div .form_close").click(function(){
    $(".subscribe_fix_div").fadeOut(200);
    $(".subscribe_fix_div .form_close").fadeOut(200);
  });
  function search_ctrl(){   
    var width = $(window).width();
    if(width < 767){
      $(".form_wrap").css("display","none");
      $(".search_btn").attr("value","Go");
    }
    else{
      $(".form_wrap").css("display","block");
      $(".search_btn").attr("value","Show Offers");
    }
  }
  $(".src_btn").stop().click(function(){
    if($(window).width() < 767){
      $(this).siblings(".form_wrap").slideToggle(300);
    }
    else{
      $(this).siblings(".form_wrap").slideDown(0);
    }
  });
  search_ctrl();
  $(window).on("resize",search_ctrl);
  $(".post_offer_btn, .pop_up_wrap").hover(function(){
    $(".pop_up_wrap").stop().fadeIn(200);
  },function(){
    $(".pop_up_wrap").fadeOut(200);
  });
</script>



<script>
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>
<script>
  $(".form-fg-pass").css("display","none");
  $(".recover_pass").click(function () {
    $(".form-signin").css("display","none")
    $(".form-fg-pass").css("display","block")
        // $("#").show();   
      });

  $(".b2login").click(function () {
    $(".form-signin").css("display","block")
    $(".form-fg-pass").css("display","none")

        // $("#").show();   
      });
    </script>



    <script>
      var copyBtn = document.querySelector('#coupon_btn');
      copyBtn.addEventListener('click', function () {
        var copiedObj = document.querySelector('#coupon_btn_txt');
  // select the contents
  copiedObj.select();
  
  document.execCommand('copy'); // or 'cut'
}, false);

</script>

<script>
// Set the date we're counting down to
var tomorrow = new Date("<?=$offer_details[0]['edate'];?>");
tomorrow.setDate(tomorrow.getDate() + 1);
var countDownDate = tomorrow.getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var offset=6;
    var now = new Date().getTime();
    //var now=calcTime(6);
    //now = now.getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("demo").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";
    
    // If the count down is over, write some text 
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("demo").innerHTML = "EXPIRED";
    }
  }, 1000);



function calcTime(offset) {

    // create Date object for current location
    d = new Date();
    
    // convert to msec
    // add local time zone offset 
    // get UTC time in msec
    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    
    // create new Date object for different city
    // using supplied offset
    nd = new Date(utc + (3600000*offset));
    
    // return time as a string
    return nd;

  }

</script>

</body>

</html>
