<?php $this->load->view('front/headlink');?> 

<?php $this->load->view('front/head_nav');?>

<?php $this->load->view('front/mega_menu');?>   





<?php if($this->session->userdata('login_id')==''){ ?>

  <div class="container">
    <div style="font-weight: bold;font-size: 15px;margin-top: 15px;margin-bottom: 15px;color:#5f5f5f;font-style: italic ">Already Registered ? Please <button  data-toggle="modal" data-target="#login_modal" class="btn btn-primary">Login</button></div>
  </div>


  <form method="POST" action="offer/order_submit_new_user/<?=$offer_id?>" onsubmit="return validation_check()">
    <div class="container">

      <div class="row">
        <div class="col-md-4">



          <div class="element_box" style="height: 750px;background: #FFFFFF; border: 1px solid #d7d7d7">

            <span class="btn btn-primary form-control text-left"><span class="badge">1</span>&nbsp;BILLING ADDRESS</span>

            <div class="form-group">
              <span  class="form-control text-center" style="color: white;display: none;background-color: red"  id="o_msg"></span>
            </div>

            <div class="col-md-12" style="padding: 15px;">





              <div class="form-group">

                <label class="input-group input-group-sm" for="username">Name:</label>

                <input type="text" class="form-control input-group input-group-sm" id="o_username" placeholder="Enter Last Name" name="username"  required="">
              </div>



              <div class="form-group">

                <label class="input-group input-group-sm" for="email">Email:</label>

                <input type="email" onchange="number_email_check()" class="form-control input-group input-group-sm" id="o_email" placeholder="Enter email" name="email"  required="">
              </div>


              <div class="form-group">

                <label class="input-group input-group-sm" for="contact">Mobile No:</label>

                <input type="text" onchange="number_email_check()" class="form-control input-group input-group-sm" id="o_contact" maxlength="11" placeholder="Enter Mobile No" name="contact"  required="">
              </div>


              <div class="form-group">

                <label class="input-group input-group-sm" for="address">Address:</label>

                <textarea name="address" id="address" class="form-control" required="" rows="1"></textarea>

              </div>


              <div class="form-group">

                <label class="input-group input-group-sm" for="country">Division:</label>



                <select class="input-group input-group-sm form-control" name="division" id="division" required="">

                  <option value="0">Select Division</option>

                  <?php foreach ($division as $key => $value) { ?>

                    <option value="<?=$value['id']?>"><?=$value['name']?></option>

                  <?php } ?>
                </select>
              </div>

              <div class="form-group">

                <label class="input-group input-group-sm" for="district">District/State:</label>

                <select class="input-group input-group-sm form-control" name="district" id="district">



                </select>
              </div>

              <div class="form-group">

               <label class="input-group input-group-sm" for="area">Area:</label>

               <select class="input-group input-group-sm form-control" name="area" id="area" required="">


               </select>

             </div>



             <!-- <div class="form-group">

              <label class="input-group input-group-sm" for="postal_code">Zip/Postal:</label>

              <input type="text" class="form-control input-group input-group-sm" id="postal_code" placeholder="Enter Zip" name="postal_code"  required="">
            </div> -->


            <div class="col-md-6">
              <label class="input-group input-group-sm" for="password">Password:</label>

              <input type="password" class="form-control input-group input-group-sm" id="o_password" placeholder="Enter Password" name="password"  required="">
            </div>

            <div class="col-md-6">
              <label class="input-group input-group-sm" for="o_confirm_password">Confirm Password:</label>

              <input type="password" class="form-control input-group input-group-sm" id="o_confirm_password" placeholder="Enter Confirm Password" name="confirm_password"  required="">
            </div>



            

          </div>


        </div>

      </div>


      <div class="col-md-4">
        <div class="element_box" style="height: 300px;background: #FFFFFF; border: 1px solid #d7d7d7">

          <span class="btn btn-primary form-control text-left"><span class="badge">2</span>&nbsp;DELIVERY METHOD</span>

          <span class="text-center form-control" style="font-weight: bold;" id="home_delivery_charge"></span>

          <input type="hidden" id="home_delivary_charge_hidden" name="shipping_cost">


        </div>

      </div>

      <div class="col-md-4">
        <div class="element_box" style="height: 300px;background: #FFFFFF; border: 1px solid #d7d7d7">
          <span class="btn btn-primary form-control text-left"><span class="badge">3</span>&nbsp;PAYMENT METHOD</span>


          <div class="col-md-12" style="padding: 15px;">
            <div class="input-group" style="margin-bottom: 5px">
              <span class="input-group-addon">
                <input checked="" type="radio" name="payment_type" aria-label="...">
              </span>
              <span><img style="width: 95px;height: 30px;border: 1px solid #d7d7d7; margin-left: 5px;" src="front_assets/images/bkash.jpg"></span>
            </div>

            <div class="input-group" style="margin-bottom: 5px">
              <span class="input-group-addon">
                <input  type="radio" name="payment_type" aria-label="...">
              </span>
              <span><img style="width: 95px;height: 30px;border: 1px solid #d7d7d7; margin-left: 5px;" src="front_assets/images/sslcommerz.png"></span>
            </div>
          </div>

          <!-- <div class="input-group" style="margin-bottom: 5px">
            <span class="input-group-addon">
              <input type="radio" aria-label="...">
            </span>
            <span></span>
          </div>

          <div class="input-group" style="margin-bottom: 5px"> 
          <span class="input-group-addon">
            <input type="radio" aria-label="...">
          </span>
          <span></span>
        </div> -->


      </div>



    </div>

    <div class="col-md-8" id="offer_order_cart_ajax">

      <?php $this->load->view('offer_order_cart_ajax');?> 
    </div>



  </div>

  <div class="row">
    <div class="text-right">
      <button type="submit" id="submit_btn" class="btn btn-primary">Place Order</button>
      <p>By clicking/tapping Place Order, I agree the <a href="">Terms & Conditions</a></p>
    </div>
  </div>



   <!--  <div class="element_box" style="padding: 30px; margin-top: 50px; height: 250;margin-bottom: 50px; width: 530px; background: #FFFFFF; border: 2px solid #F5F5F5">


   </div> -->


 </div>

</form>

<?php }

// existed user
else { ?>


  <form method="POST" action="offer/order_submit_old_user/<?=$offer_id?>">
    <div class="container">



      <div class="row">


        <div class="col-md-4">

          <div class="element_box" style="height: 620px;background: #FFFFFF; border: 1px solid #d7d7d7">

            <span class="btn btn-primary form-control text-left"><span class="badge">1</span>&nbsp;BILLING ADDRESS</span>

            <div class="col-md-12" style="padding: 15px;">



              <div class="form-group">

                <label class="input-group input-group-sm" for="name">Name:</label>

                <input type="text" class="form-control input-group input-group-sm" id="name" placeholder="Enter Name" value="<?=$user_info[0]['username']?>" readonly name="name"  required="">
              </div>



             <!--  <div class="form-group">

                <label class="input-group input-group-sm" for="email">Email:</label>

                <input type="text" class="form-control input-group input-group-sm" id="username_login" placeholder="Enter email" name="email" value="<?=$user_info[0]['email']?>"  required="">
              </div> -->


              <div class="form-group">

                <label class="input-group input-group-sm" for="mobile_no">Mobile No:</label>

                <input type="text" class="form-control input-group input-group-sm" id="mobile_no" placeholder="Enter Mobile No" name="mobile_no" readonly  required="" value="<?=$user_info[0]['contact']?>">
              </div>

              <!-- <span class=""><center><h3 style="font-weight: bold;color: green">Shipment Info</h3></center></span> -->


              <div class="form-group">

                <label class="input-group input-group-sm" for="address">Address:</label>

                <textarea name="address" name="address" required="" id="address" class="form-control" rows="1"><?=$user_info[0]['address']?></textarea>

              </div>


              <div class="form-group">

                <label class="input-group input-group-sm" for="division">Division:</label>

                <select class="input-group input-group-sm form-control" name="division" id="division">

                  <?php foreach ($division as $key => $value)
                  { if($value['id']==$user_info[0]['division_id']){ ?>

                    <option selected="" value="<?=$value['id']?>"><?=$value['name']?></option>

                  <?php } else { ?>

                    <option value="<?=$value['id']?>"><?=$value['name']?></option>

                  <?php }} ?>

                </select>
              </div>

              <div class="form-group">

                <label class="input-group input-group-sm" for="district">District/State:</label>

                <select class="input-group input-group-sm form-control" name="district" id="district">

                  <?php foreach ($district as $key => $value)
                  { if($value['id']==$user_info[0]['district_id']){ ?>

                    <option selected="" value="<?=$value['id']?>"><?=$value['name']?></option>

                  <?php } else { ?>

                    <option value="<?=$value['id']?>"><?=$value['name']?></option>

                  <?php }} ?>
                </select>
              </div>

              <div class="form-group">

                <label class="input-group input-group-sm" for="area">Area:</label>
                <select class="input-group input-group-sm form-control" name="area" id="area">

                 <?php foreach ($area as $key => $value)
                 { if($value['area_id']==$user_info[0]['area_id']){ ?>

                  <option selected="" value="<?=$value['area_id']?>"><?=$value['area_title']?></option>

                <?php } else { ?>

                 <option value="<?=$value['area_id']?>"><?=$value['area_title']?></option>

               <?php }} ?>
             </select>

           </div>


         </div>

       </div>

     </div>






     <div class="col-md-4">
      <div class="element_box" style="height: 300px;background: #FFFFFF; border: 1px solid #d7d7d7">

        <span class="btn btn-primary form-control text-left"><span class="badge">2</span>&nbsp;DELIVERY METHOD</span>

        

        <span class="text-center form-control" style="font-weight: bold;" id="home_delivery_charge"></span>

        <input type="hidden" id="home_delivary_charge_hidden" name="shipping_cost">

      </div>

    </div>

    <div class="col-md-4">
      <div class="element_box" style="height: 300px;background: #FFFFFF; border: 1px solid #d7d7d7">
        <span class="btn btn-primary form-control text-left"><span class="badge">3</span>&nbsp;PAYMENT METHOD</span>


        <div class="col-md-12" style="padding: 15px;">
          <div class="input-group" style="margin-bottom: 5px">
            <span class="input-group-addon">
              <input type="radio" value="1" checked="" name="pay" aria-label="...">
            </span>
            <span><img style="width: 95px;height: 30px;border: 1px solid #d7d7d7; margin-left: 5px;" src="front_assets/images/bkash.jpg"></span>
          </div>

          <div class="input-group" style="margin-bottom: 5px">
            <span class="input-group-addon">
              <input type="radio" value="2"  name="pay" aria-label="...">
            </span>
            <span><img style="width: 95px;height: 30px;border: 1px solid #d7d7d7; margin-left: 5px;" src="front_assets/images/sslcommerz.png"></span>
          </div>
        </div>

          <!-- <div class="input-group" style="margin-bottom: 5px">
            <span class="input-group-addon">
              <input type="radio" aria-label="...">
            </span>
            <span></span>
          </div>

          <div class="input-group" style="margin-bottom: 5px"> 
          <span class="input-group-addon">
            <input type="radio" aria-label="...">
          </span>
          <span></span>
        </div> -->


      </div>



    </div>





    <div class="col-md-8" id="offer_order_cart_ajax">

      <?php $this->load->view('offer_order_cart_ajax');?> 
    </div>

  </div>

  <div class="row">
    <div class="text-right">
      <button type="submit" class="btn btn-primary">Place Order</button>
      <p>By clicking/tapping Place Order, I agree the <a href="">Terms & Conditions</a></p>
    </div>
  </div>



   <!--  <div class="element_box" style="padding: 30px; margin-top: 50px; height: 250;margin-bottom: 50px; width: 530px; background: #FFFFFF; border: 2px solid #F5F5F5">


   </div> -->


 </div>

</form>


<?php } ?>




<?php $this->load->view('front/footer');?>
<?php $this->load->view('front/footerlink');?>



<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2"></script>

<script>
  function get_sms_ajax() 
  {

   var number=$("#number").val();

   var offer_id = $('#offer_id_modal').val();

   $("#err_div").hide();
   $("#s_div").hide();

   if(number=='')
   {
    $("#err_div").show();
    $("#m_msg").html('Please Enter your Phone Number');

    return false;
  }
  else if(number.length != 10)
  {
    $("#err_div").show();
    $("#m_msg").html('Please Enter A Valid Phone Number of 10 digit without 0');

    return false;
  }

  $.ajax({
    url: "<?php echo site_url('home/send_sms');?>",
    type: "post",
    data: {number:number,offer_id:offer_id,},
    success: function(msg)
    {

                    //alert(msg);
                    if(msg==0)
                    {
                      $("#err_div").show();
                      $("#m_msg").html('Something Wrong. Please try again');
                    }
                    else if(msg==1)
                    {
                      $("#err_div").show();
                      $("#m_msg").html('Already Sent SMS');
                    }
                    else
                    {
                      $("#s_div").show();
                      $("#s_msg").html('SMS Successfully Send.');
                    }

                  }      
                });  

}
</script>

<script>
  function save_voucher_image() 
  {

   var offer_id = $('#offer_id_modal').val();

   $.ajax({
    url: "<?php echo site_url('home/save_image');?>",
    type: "post",
    data: {offer_id:offer_id,},
    success: function(msg)
    {

                  // window.open(msg);


                }      
              });  

 }
</script>

<script>
  $(".floating_nav").hide();
  $(".owl-slide").owlCarousel({
    items:1,
    responsive: false,
    navigation : true,
    navigationText : ["&#61700;","&#61701;"],
    autoPlay : 2000,
    stopOnHover : true,
    pagination : false,
    paginationNumbers: false
  });
  $(".product_filter h2").click(function(){
    $(".product_filter form").fadeIn(200);
    $(".product_filter .form_close").fadeIn(200);
  });
  $(".product_filter .form_close").click(function(){
    $(".product_filter form").fadeOut(200);
    $(".product_filter .form_close").fadeOut(200);
  });
  $(".subscribe").click(function(){
    $(".subscribe_fix_div").fadeIn(200);
    $(".subscribe_fix_div").css({"display": "-webkit-box", "display": "-ms-flexbox", "display":"flex"});
    $(".subscribe_fix_div .form_close").fadeIn(200);
  });
  $(".subscribe_fix_div .form_close").click(function(){
    $(".subscribe_fix_div").fadeOut(200);
    $(".subscribe_fix_div .form_close").fadeOut(200);
  });
  function search_ctrl(){   
    var width = $(window).width();
    if(width < 767){
      $(".form_wrap").css("display","none");
      $(".search_btn").attr("value","Go");
    }
    else{
      $(".form_wrap").css("display","block");
      $(".search_btn").attr("value","Show Offers");
    }
  }
  $(".src_btn").stop().click(function(){
    if($(window).width() < 767){
      $(this).siblings(".form_wrap").slideToggle(300);
    }
    else{
      $(this).siblings(".form_wrap").slideDown(0);
    }
  });
  search_ctrl();
  $(window).on("resize",search_ctrl);
  $(".post_offer_btn, .pop_up_wrap").hover(function(){
    $(".pop_up_wrap").stop().fadeIn(200);
  },function(){
    $(".pop_up_wrap").fadeOut(200);
  });
</script>



<script>
  $(document).ready(function(){
  // Add smooth scrolling to all links
  $("a").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
</script>
<script>
  $(".form-fg-pass").css("display","none");
  $(".recover_pass").click(function () {
    $(".form-signin").css("display","none")
    $(".form-fg-pass").css("display","block")
        // $("#").show();   
      });

  $(".b2login").click(function () {
    $(".form-signin").css("display","block")
    $(".form-fg-pass").css("display","none")

        // $("#").show();   
      });
    </script>



    <script>
      var copyBtn = document.querySelector('#coupon_btn');
      copyBtn.addEventListener('click', function () {
        var copiedObj = document.querySelector('#coupon_btn_txt');
  // select the contents
  copiedObj.select();
  
  document.execCommand('copy'); // or 'cut'
}, false);

</script>

<script>
// Set the date we're counting down to
var tomorrow = new Date("<?=$offer_details[0]['edate'];?>");
tomorrow.setDate(tomorrow.getDate() + 1);
var countDownDate = tomorrow.getTime();

// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var offset=6;
    var now = new Date().getTime();
    //var now=calcTime(6);
    //now = now.getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    // Output the result in an element with id="demo"
    document.getElementById("demo").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";
    
    // If the count down is over, write some text 
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("demo").innerHTML = "EXPIRED";
    }
  }, 1000);



function calcTime(offset) {

    // create Date object for current location
    d = new Date();
    
    // convert to msec
    // add local time zone offset 
    // get UTC time in msec
    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    
    // create new Date object for different city
    // using supplied offset
    nd = new Date(utc + (3600000*offset));
    
    // return time as a string
    return nd;

  }

</script>


<script type="text/javascript">

  function validation_check(argument) {
    var username=$("#o_username").val();
    var email=$("#o_email").val();
    var contact=$("#o_contact").val();
        //contact='880'+contact;
        var password=$("#o_password").val();
        var con_password=$("#o_confirm_password").val();


        // alert(password);
        // alert(con_password);

        if(username=='')
        {
          $("#o_msg").show();
          $("#o_msg").html('Please Enter your Name');
          return false;

        }

        else if(email=='')
        {
          $("#o_msg").show();
          $("#o_msg").html('Please Enter your Email');
          return false;
        }

        else if(contact=='')
        {
          $("#o_msg").show();
          $("#o_msg").html('Please Enter your contact number');
          return false;
        }

        else if(password=='')
        {
          $("#o_msg").show();
          $("#o_msg").html('Please Enter Password');
          return false;
        }
        else if(contact.length != 11)
        {
          $("#o_msg").show();
          $("#o_msg").html('Please Enter A Valid Phone Number of 11 digit');

          return false;
        }

        else if(password!=con_password)
        {
          $("#o_msg").show();
          $("#o_msg").html('Password do not match');
          return false;
        }


      }



      function number_email_check(argument) {

       var email=$("#o_email").val();
       var contact=$("#o_contact").val();

       $.ajax({
        url: "<?php echo site_url('offer/check_email_phone_unique');?>",
        type: "post",
        data: {email:email,contact:contact},
        success: function(msg)
        {

          if(msg=="success")
          {
            $("#submit_btn").prop( "disabled", false );
            $("#o_msg").hide();

          }
          else
          {
            $("#o_msg").show();
            $("#o_msg").html(msg);

            $("#submit_btn").prop( "disabled", true );

          }
        }   

      });



     }

   </script>

 </body>

 </html>
