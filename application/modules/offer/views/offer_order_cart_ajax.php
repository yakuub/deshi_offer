
      <div class="element_box" style="margin-top:5px;height: 300px;background: #FFFFFF; border: 1px solid #d7d7d7">
        <span class="btn btn-primary form-control text-left"><span class="badge"><i class="fa fa-check" aria-hidden="true"></i></span>&nbsp;ORDER REVIEW</span>

        <div class="col-md-12" style="padding: 15px;">
         <!--  <div class="form-inline">
            <label>PROMO CODE</label>

            <input class="form-control" type="text" name="">

            <span class="btn btn-primary">Apply Promo</span>
          </div> -->

          <table class="table table-bordered" style="margin-top:10px;">
            <thead>
              <th>Product Name</th>
              <th>Price (TK)</th>
              <th>Qty</th>
              <th>Subtotal (TK)</th>
            </thead>
            <tbody>

              <?php $cart= $this->cart->contents(); ?>
              <?php 

              foreach ($cart as $item){ ?>

                <input type="hidden" value="<?=$item['id']?>" name="offer_id">
                
                <input type="hidden" value="<?=$item['price']?>" name="price">

                <tr>
                  <td><?=$item['name']?></td>
                  <td><?=$item['price']?></td>
                  <input type="hidden" value="<?=$item['qty']?>" name="quantity">
                  <td align="center">
                    <span class="btn-sm btn-info" onclick="increase_qty('<?=$item['id']?>','<?=$item['name']?>','<?=$item['price']?>')" ><i class="fa fa-plus" aria-hidden="true"></i></span>&nbsp;&nbsp;<span><?=$item['qty']?></span>&nbsp;&nbsp;<span class="btn-sm btn-info" onclick="decrease_qty('<?=$item['id']?>','<?=$item['name']?>','<?=$item['price']?>','<?=$item['qty']?>')"><i class="fa fa-minus" aria-hidden="true"></i></span>
                  </td>
                  <td><?=$item['subtotal']?></td>
                </tr>

              <?php } ?>

                <tr>
                  <td align="right" colspan="3">Subtotal</td>
                  <td><span id="subtotal"><?=$item['subtotal']?></span></td>
                </tr>

                <input type="hidden" id="subtotal_hidden" value="<?=$item['subtotal']?>" name="total_amount">

                <tr>
                  <td align="right" colspan="3">Shiping & Handling</td>
                  <td><span id="shipment"></span></td>
                </tr>

                <tr>
                  <td align="right" colspan="3">Grand Total</td>
                  <td><span id="total"></span></td>
                </tr>

              </tbody>
            </table>

          </div>



        </div>
