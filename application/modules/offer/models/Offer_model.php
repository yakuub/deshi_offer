<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Offer_model extends CI_Model
{
    ////// Basic Model Function Starts ///////
    public function insert($table_name,$data)
    {
     $this->db->insert($table_name, $data);
 }

 public function insert_ret($tablename, $tabledata)
 {
    $this->db->insert($tablename, $tabledata);
    return $this->db->insert_id();
}

public function update_function($columnName, $columnVal, $tableName, $data)
{
    $this->db->where($columnName, $columnVal);
    $this->db->update($tableName, $data);
}

public function update_function2($condition,$tableName, $data)
{
    $this->db->where($condition);
    $this->db->update($tableName, $data);
}

public function delete_function_cond($tableName, $cond)
{
    $where = '( ' . $cond . ' )';
    $this->db->where($where);
    $this->db->delete($tableName);
}
public function delete_function($tableName, $columnName, $columnVal)
{
    $this->db->where($columnName, $columnVal);
    $this->db->delete($tableName);
}

public function select_all($table_name)
{
   $this->db->select('*');
   $this->db->from($table_name);
   $query = $this->db->get();
   $result = $query->result_array();
   return $result;
}

public function select_all_decending($table_name)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $this->db->order_by('created_at','DESC');
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

public function select_all_acending($table_name,$col_name)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $this->db->order_by($col_name,'ASC');
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

public function select_condition_decending($table_name,$condition)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('created_at','DESC');
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


public function select_condition_decending_with_limit($table_name,$condition,$limit)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('created_at','DESC');
    $this->db->limit($limit);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


public function select_condition_random_with_limit($table_name,$condition,$limit)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('created_at','RANDOM');
    $this->db->limit($limit);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


public function select_with_where($selector, $condition, $tablename)
{
   $this->db->select($selector);
   $this->db->from($tablename);
   $where = '(' . $condition . ')';
   $this->db->where($where);
   $result = $this->db->get();
   return $result->result_array();
}

public function select_where_join($selector,$table_name,$join_table,$join_condition,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition);
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}


public function select_where_join_limit($selector,$table_name,$join_table,$join_condition,$condition,$limit)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition);
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $this->db->limit($limit);
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_left_join($selector,$table_name,$join_table,$join_condition,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

    ////// Basic Model Function End ///////




public function columns($database, $table)
{
        //$query = "SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_DEFAULT, COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS  WHERE table_name = '$table'AND table_schema = '$database'";  
    $query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = '$table'
    AND table_schema = '$database'";    
    $result = $this->db->query($query) or die ("Schema Query Failed"); 
    $result=$result->result_array();
    return $result;
}

// For access menu and submenu
public function get_menu_list($login_id)
{
    $this->db->select('*');
    $this->db->from('menu_for_admin');
    $this->db->where('menu_for_admin.user_id',$login_id); 
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

public function get_sub_menu_list($login_id)
{
    $this->db->select('*');
    $this->db->from('sub_menu_for_admin');
    $this->db->where('sub_menu_for_admin.user_id',$login_id); 
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


// For access menu and submenu

public function get_offer()
{

    $query="SELECT * FROM main_offer mf inner join offer_type on offer_type.offer_id=mf.offer_title_type CASE mf.c_type WHEN c_type=1 then inner join voucher on voucher.voucher_id=mf.c_type when 2 then inner join utm_ofr on utm_ofr.utmid=mf.c_type when 3 then inner join offer_sms on offer_sms.sms_id=mf.c_type when 4 then inner join online_deal on online_deal.online_deal_id=mf.c_type"; 
    $result = $this->db->query($query);
    $result=$result->result_array();
    return $result;
}


public function get_offer_ci($condition,$limit,$order_col,$order_type)
{
    $this->db->select('*,mf.offer_id as main_off_id,mf.offer_title,ctype_colour.color_code as color_code_btn,offer_type.color_code as offer_color_code');
    $this->db->from('main_offer mf');
    $this->db->join('offer_type','offer_type.offer_id=mf.offer_title_type');
    $this->db->join('voucher','voucher.offer_id=mf.offer_id','LEFT');
    $this->db->join('utm_ofr','utm_ofr.offer_id=mf.offer_id','LEFT');
    $this->db->join('offer_sms','offer_sms.offer_id=mf.offer_id','LEFT');
    $this->db->join('direct_sale','direct_sale.offer_id=mf.offer_id','LEFT');
    $this->db->join('online_deal','online_deal.offer_id=mf.offer_id','LEFT');
    $this->db->join('ctype_colour','ctype_colour.id=mf.ctype','LEFT');
    $this->db->join('user_login','user_login.loginid=mf.offer_from','LEFT');
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by($order_col,$order_type);
    $this->db->group_by('mf.offer_id');
    $this->db->limit($limit);
    $result=$this->db->get();
    return $result->result_array(); 
}



public function get_offer_details($condition) 
{
    $this->db->select('*,mf.offer_id as main_off_id,ctype_colour.color_code as color_code_btn,offer_type.color_code as offer_color_code');
    $this->db->from('main_offer mf');
    $this->db->join('offer_type','offer_type.offer_id=mf.offer_title_type');
    $this->db->join('voucher','voucher.offer_id=mf.offer_id','LEFT');
    $this->db->join('utm_ofr','utm_ofr.offer_id=mf.offer_id','LEFT');
    $this->db->join('offer_sms','offer_sms.offer_id=mf.offer_id','LEFT');
    $this->db->join('online_deal','online_deal.offer_id=mf.offer_id','LEFT');
    $this->db->join('ctype_colour','ctype_colour.id=mf.ctype','LEFT');
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $result=$this->db->get();
    return $result->result_array();
}


}
?>