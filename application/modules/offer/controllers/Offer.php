<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Offer extends MX_Controller { 


	function __construct()
	{
		$this->load->model('home/home_model');
		$this->load->model('offer_model');
		$this->load->model('admin/admin_model');



		date_default_timezone_set('Asia/Dhaka');
		if($this->session->userdata('language_select')=='bangla')
		{
			$this->lang->load('front', 'bangla');
		}
		else
		{
			$this->lang->load('front', 'english');
		} 


	}
	public function index($cat_name='')
	{
		$data['active']='home';

		$data['header']=$this->home_model->select_with_where('*','id=1','company');
		// $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
		$data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

		$cur_date=date('Y-m-d');

		$data['cat_brand']=array();
		$data['store_offer']=array();
		foreach ($data['category'] as $key => $value) 
		{
			$data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

			foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
			{
				$data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND mf.edate>=DATE("'.$cur_date.'") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
			}	
			
		}

		$data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','top_store=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');
		
		$data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND verify_status=1 AND is_publish=1','28');

		$data['offer_type']=$this->home_model->select_with_where('*','offer_st=1','offer_type');

		$data['offer_category']=$this->home_model->select_with_where('*','menu_st=1','catagory');

		$data['division']=$this->home_model->select_all_acending('divisions','position');


		if($cat_name=='hot_offer')
		{
			$data['cat_id']=0;

			$data['cat_name']='Hot Offer';
			$data['cat_offer_list']=$this->offer_model->get_offer_ci('mf.is_hot=1 AND mf.status=1',18,'mf.updated_at','DESC');
		}
		else if($cat_name=='popular_store_offer')
		{
			$data['cat_id']=1000;
			$get_popular_store=$this->home_model->select_with_where('*','show_on_web=1','user_login');
			$pupular_store_in='';
			foreach ($get_popular_store as $key => $value) 
			{
				$pupular_store_in.=$value['loginid'].',';
			}

			$pupular_store_in=substr($pupular_store_in, 0, -1);

			$data['cat_name']='Popular Store Offer';

			$data['cat_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 and mf.offer_from in ('.$pupular_store_in.')',18,'mf.updated_at','DESC');
		}

		else if($cat_name=='popular_category_offer')
		{
			$data['cat_id']=1000;

			$data['cat_name']='Popular Category Offer';
			$data['cat_offer_list']=$this->offer_model->get_offer_ci('mf.is_hot=1 AND mf.status=1',18,'mf.updated_at','DESC');
		}


		else
		{
			$cat_id=$this->home_model->select_with_where('*','REPLACE(catagory_title," ","%20") LIKE "'.$cat_name.'%"','catagory');
			//$data['area_id']=$this->home_model->select_with_where('id','REPLACE(area_title, " ", "%20") LIKE "'.$name.'%"','area');

			$cat_id=$cat_id[0]['catagory_id'];
			$data['cat_id']=$cat_id;

			$data['cat_name']=str_replace("%20"," ",$cat_name);
			$data['cat_offer_list']=$this->offer_model->get_offer_ci('mf.catagories='.$cat_id.' AND mf.status=1',18,'mf.updated_at','DESC');
		}

		$data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

		$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');

		$data['user_details']=$this->home_model->select_with_where('*','user_id="'.$this->session->userdata('login_id').'"','user_details');

		$this->session->set_userdata('referred_from', current_url());

		
		$this->load->view('index',$data);
	}


	public function offer_payment($offer_id='')
	{
		$data['notification_details']=$this->admin_model->select_with_where('*','view_status=2 and user_id="'.$this->session->userdata('login_id').'"','notifications');

		$data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

		$data['user_info']=$this->home_model->select_with_where('*','loginid="'.$this->session->userdata('loginid').'"','user_login');


		$cur_date=date('Y-m-d');
		$data['cat_brand']=array();
		$data['store_offer']=array();

		foreach ($data['category'] as $key => $value) 
		{
			$data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

			foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
			{
				$data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND (mf.edate>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
			}   

		}

		$data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','show_on_web=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

		$data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND verify_status=1 AND is_publish=1','28');

		$data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

		$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights'); 

		$data['feature_store']=$this->home_model->select_condition_random_with_limit('user_login','show_on_web=1 AND verify_status=1 AND usertype=2',12);


		$data['hot_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.is_hot=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','updated_at','RANDOM');

            //echo "<pre>";print_r($data['hot_offer_list']);die();

		$data['new_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")',8,'mf.offer_id','DESC');

		$data['recent_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.total_seen','DESC');

		$data['user_info']=$this->offer_model->select_where_join('*','user_login u','user_details d','u.loginid=d.user_id','u.loginid="'.$this->session->userdata('login_id').'"');

		$data['division']=$this->offer_model->select_all('divisions');
		$data['district']=$this->offer_model->select_all('districts');
		$data['area']=$this->offer_model->select_all('area');

		$data['offer_id']=$offer_id;

		$data['offer_details']=$this->offer_model->get_offer_details('mf.offer_id="'.$offer_id.'"');

		$this->cart->destroy();

		$val = array
		(
			"id"  => $offer_id,
			"name"  => $data['offer_details'][0]['offer_title'],
			"qty"  => 1,
			"price"  => $data['offer_details'][0]['price'],
		);

		$val=$this->cart->insert($val);


		$this->load->view('offer_payment',$data);
	}




	public function increase_qty($value='')
	{
		$data = array
		(

			"id"  => $_POST['id'],
			"name"  =>$_POST['name'],
			"qty"  => 1,
			"price"  => $_POST['price'],
		);

		$val=$this->cart->insert($data);

		$this->load->view('offer_order_cart_ajax');


	}

	public function decrease_qty($value='')
	{

		// $cart= $this->cart->contents();
		$this->cart->destroy();

		$data = array
		(

			"id"  => $_POST['id'],
			"name"  =>$_POST['name'],
			"qty"  => $_POST['quantity'],
			"price"  => $_POST['price'],
		);

		$val=$this->cart->insert($data);



		
		

		$this->load->view('offer_order_cart_ajax');
	}


	public function order_submit_new_user($value='')
	{
		$username=$this->input->post('username');


		$email=$this->input->post('email');



		$contact=$this->input->post('contact');



		$data_login['username']=$username;

		$data_login['email']=$email;


		$data_login['contact']='88'.$contact;

		$this->session->set_userdata('email' , $this->input->post('email'));

		$this->load->helper('string');

      //$system_password=random_string('alnum',6);

		$activation_key=random_string('alnum', 50);


		$password=$this->input->post('password');

      //$data_login['password']=$this->encryptIt($system_password);

		$data_login['password']=$this->encryptIt($this->input->post('password'));

		$c_password=$this->encryptIt($this->input->post('c_password'));


		$data_login['usertype']=3;
		$data_login['verify_status']=0;
		$data_login['delete_status']=0;
		$data_login['activation_status']=0;
		$data_login['add_date']=date('Y-m-d H:i:s');


		$login_id=$this->offer_model->insert_ret('user_login',$data_login);
		$login_id=$this->offer_model->insert_ret('user_login',$data_login);

      //below for profile name update 

		$mt=$data_login['username']."_".$login_id;
		$pname= preg_replace('/[ ,]+/', '_', trim($mt));
		$pname= str_replace('/', '_', $pname);
		$pname= str_replace('.', '_', $pname);

		$activation_key=$activation_key.$login_id;

		$name['name']=$pname;
		$name['activation_key']=$activation_key;


		$this->offer_model->update_function('loginid', $login_id, 'user_login',$name);


		// insert into user details

		$data_user['user_id']=$login_id;
		$data_user['address']=$this->input->post('address');
		$data_user['division_id']=$this->input->post('division');
		$data_user['district_id']=$this->input->post('district');
		$data_user['area_id']=$this->input->post('area');
		$data_user['postal_code']=$this->input->post('postal_code');

		$this->offer_model->insert_ret('user_details',$data_user);


		// insert into order table

		$auto_gen_id=$login_id.date('YmdHis');

		$data_order['user_id']=$login_id;
		$data_order['gen_order_id']=$auto_gen_id;
		$data_order['offer_id']=$this->input->post('offer_id');
		$data_order['total_amount']=$this->input->post('total_amount');
		$data_order['price']=$this->input->post('price');
		$data_order['shipping_cost']=$this->input->post('shipping_cost');
		$data_order['division_id']=$this->input->post('division');
		$data_order['district_id']=$this->input->post('district');
		$data_order['area_id']=$this->input->post('area');
		$data_order['shipping_address']=$this->input->post('address');
		$data_order['order_status']='pending';
		$data_order['quantity']=$this->input->post('quantity');
		$data_order['created_at']=date('Y-m-d');

		$order_id=$this->offer_model->insert_ret('user_order',$data_order);
		$confirm_payment=$data_order['total_amount']+$data_order['shipping_cost'];


		$msg_from= 'support@deshioffer.com';
		$c_name='Deshioffer-Team';
		$name=$data_login['username'];


		$msg_to=$data_login['email'];
		$this->send_email_function($msg_from,$name,$msg_to,$c_name,$password,$activation_key,$login_id);

		redirect('offer/confirm_payment/'.$order_id.'/'.$confirm_amount);
	}

	public function check_email_phone_unique()
	{
		$email= $this->input->post('email');
		$phone= $this->input->post('contact');
		$phone='88'.$phone;

      //echo $phone;die();

		$exist_email= $this->offer_model->select_with_where('*','email="'.$email.'"','user_login');

		$exist_phone= $this->offer_model->select_with_where('*','contact="'.$phone.'"','user_login');

		if(count($exist_email)>0)
		{
			echo 'Email is already exist.';
		}

		elseif(count($exist_phone)>0)
		{

			echo 'Phone Number is already exist.';
		}

		else
		{
			echo 'success';
		}

	}

	function encryptIt($string) 
	{
		$output = false;
		$encrypt_method = "AES-256-CBC";
		$secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
		$secret_iv = 'This is my secret iv';
        // hash
		$key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
		$iv = substr(hash('sha256', $secret_iv), 0, 16);

		$output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
		$output = base64_encode($output);
		$output=str_replace("=", "", $output);
		return $output;
	}




	public function send_email_function($msg_from,$name,$msg_to,$c_name,$password,$activation_key,$login_id)
	{
		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'mail.deshioffer.com',
			'smtp_port' => 25,
			'smtp_user' => 'noreply@deshioffer.com',
			'smtp_pass' => 'userpass@@@123',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1',
			'wordwrap' => 'TRUE',
			'newline'   => "\r\n",
			'crlf'   => "\r\n",
		);

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from($msg_from,$c_name);
		$this->email->to($msg_to);
		$base=$this->config->base_url();

		$data['title']='Registration Successfully Done';
		$data['name']=$name;
		$data['email']=$msg_to;
		$data['password']=$password;
		$data['activation_key']=$activation_key;
		$data['msg_body']='<b>Congratulation!</b> You have successfully completed your "Registration Process" in Deshioffer.You can activate your account by clicking this <a href="https://deshioffer.com/registration/acc_activation/'.$activation_key.'/'.$login_id.'"> link.</a>. 
		<p><b>Your system password is: '.$password.'</b></p><p><b>Note*: You can change your password from your panel.</b></p>';

		$message=$this->load->view('templete_email/templete_email', $data,'true');

		$this->email->subject('Deshioffer-Registraion Successfull');
		$this->email->message($message);
		$this->email->set_mailtype("html");
		$this->email->send();

		
	
	}


	public function order_submit_old_user($offer_id='')
	{

		$login_id=$this->session->userdata('login_id');

		// insert into order table

		$auto_gen_id=$login_id.date('YmdHis');

		$data_order['user_id']=$login_id;
		$data_order['gen_order_id']=$auto_gen_id;
		$data_order['offer_id']=$offer_id;
		$data_order['total_amount']=$this->input->post('total_amount');
		$data_order['shipping_cost']=$this->input->post('shipping_cost');
		$data_order['division_id']=$this->input->post('division');
		$data_order['district_id']=$this->input->post('district');
		$data_order['area_id']=$this->input->post('area');
		$data_order['shipping_address']=$this->input->post('address');
		$data_order['order_status']='pending';
		$data_order['quantity']=$this->input->post('quantity');
		$data_order['price']=$this->input->post('price');
		$data_order['created_at']=date('Y-m-d');

		$confirm_amount=$data_order['total_amount']+$data_order['shipping_cost'];

		$order_id=$this->offer_model->insert_ret('user_order',$data_order);

		redirect('offer/confirm_payment/'.$order_id.'/'.$confirm_amount);
	}


	public function confirm_payment($order_id,$total_amount)
	{

		$login_id=$this->session->userdata('login_id');
		$data['header']=$this->home_model->select_with_where('*','id=1','company');
			// $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
		$data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

		$cur_date=date('Y-m-d');

		$data['cat_brand']=array();
		$data['store_offer']=array();
		foreach ($data['category'] as $key => $value) 
		{
			$data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

			foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
			{
				$data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND (mf.edate>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
			}	

		}

		$data['is_feature']=$this->home_model->select_condition_random_with_limit('user_login','is_feature=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

		$data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','top_store=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

		$data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND verify_status=1 AND is_publish=1','28');

		$data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

		$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');

			//echo "<pre>";print_r($data['special_offer']);die();

		$data['feature_store']=$this->home_model-> select_condition_random_with_limit('user_login','show_on_web=1 AND verify_status=1 AND usertype=2',12);


		$data['hot_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.is_hot=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','updated_at','RANDOM');

			//echo "<pre>";print_r($data['hot_offer_list']);die();

		$data['new_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")',8,'mf.offer_id','DESC');

		$data['recent_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.total_seen','DESC');

			// top 4 categories offer
		$data['page_category']=$this->home_model->select_with_where('*','front_page_show_st=1','catagory');

			//echo "<pre>";print_r($data['page_category']);die();


		if(array_key_exists(0,$data['page_category']))
		{
			$data['top_0_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][0]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
		}
		if(array_key_exists(1,$data['page_category']))
		{
			$data['top_1_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][1]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
		}

		if(array_key_exists(2,$data['page_category']))
		{
			$data['top_2_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][2]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
		}
		if(array_key_exists(3,$data['page_category']))
		{
			$data['top_3_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][3]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
		}

		if(array_key_exists(4,$data['page_category']))
		{
			$data['top_4_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][4]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
		}



		$data['slider_list']=$this->home_model->select_condition_acending_with_limit('slider','status=1','slider_sl','8');


		$data['slider_advertise']=$this->home_model->select_with_where('*','status=1 AND advertise_position=1','advertise');

		$data['user_details']=$this->home_model->select_with_where('*','loginid="'.$this->session->userdata('login_id').'"','user_login');


		$data['total_amount']=$total_amount;




		$this->load->view('confirm_payment', $data);

	}


	public function get_all_district($value='')
	{
		$division_id=$this->input->post('division_id');

		$data=$this->admin_model->select_with_where('*','division_id="'.$division_id.'"','districts');

		echo json_encode($data);
	}

	public function get_all_area($value='')
	{
		$district_id=$this->input->post('district_id');

		$data['area']=$this->admin_model->select_with_where('*','district_id="'.$district_id.'"','area');

		$data['charge_info']=$this->admin_model->select_with_where('*','district_id="'.$district_id.'"','district_wise_shipment');

		echo json_encode($data);
	}


	public function deals_offer($deals_name)
	{
		$data['active']='home';
		$data['header']=$this->home_model->select_with_where('*','id=1','company');

		$data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

		$data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

		$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');


		$data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','top_store=1 AND usertype=2 AND verify_status=1','28');
		$data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND verify_status=1','28');
		$data['offer_type']=$this->home_model->select_with_where('*','offer_st=1','offer_type');

		$data['offer_category']=$this->home_model->select_with_where('*','menu_st=1','catagory');

		$data['division']=$this->home_model->select_all_acending('divisions','position');


		$cur_date=date('Y-m-d');

		$data['cat_brand']=array();
		$data['store_offer']=array();
		foreach ($data['category'] as $key => $value) 
		{
			$data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

			foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
			{
				$data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND mf.edate>=DATE("'.$cur_date.'") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
			}	
			
		}




		$data['cat_name']=str_replace("%20"," ",$deals_name);

		$deals_name_rmv_spc=$data['cat_name'];

		$data['cat_id']=$this->home_model->select_with_where('*','offer_title="'.$deals_name_rmv_spc.'"','offer_highlights');
		//echo '<pre>';print_r($highlight_id);die();

		$cat_id=$data['cat_id'];

		$highlight_id=$cat_id[0]['id'];
		$p_id=$cat_id[0]['offer_id'];

		$data['offer_id']=explode(',', $p_id);
		$offer_id=$data['offer_id'];
		$offer_id=implode(',', $offer_id);

		//echo '<pre>';print_r($data['offer_id']);die();

		$data['cat_offer_list']=$this->offer_model->get_offer_ci('mf.offer_id in ('.$offer_id.') AND mf.status=1',40,'mf.updated_at','DESC');

		$data['offer_list']=$this->offer_model->get_offer_ci('mf.offer_id in ('.$offer_id.') AND mf.status=1',40,'mf.updated_at','DESC');


		$data['total_offer']=count($this->home_model->select_with_where('*','main_offer.offer_id in ('.$offer_id.') AND status=1','main_offer'));

	    //echo '<pre>';print_r($data['offer_list']);die();
		$this->load->view('deals', $data);

	}

	public function special_offer($deals_name)
	{

		$data['active']='home';
		$data['header']=$this->home_model->select_with_where('*','id=1','company');

		$data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

		$data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

		$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');


		$data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','top_store=1 AND usertype=2  AND verify_status=1','28');
		$data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1  AND verify_status=1','28');
		$data['offer_type']=$this->home_model->select_with_where('*','offer_st=1','offer_type');

		$data['offer_category']=$this->home_model->select_with_where('*','menu_st=1','catagory');

		$data['division']=$this->home_model->select_all_acending('divisions','position');


		$cur_date=date('Y-m-d');

		$data['cat_brand']=array();
		$data['store_offer']=array();
		foreach ($data['category'] as $key => $value) 
		{
			$data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

			foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
			{
				$data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND mf.edate>=DATE("'.$cur_date.'") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
			}	
			
		}




		$data['cat_name']=str_replace("%20"," ",$deals_name);

		$deals_name_rmv_spc=$data['cat_name'];

		$data['cat_id']=$this->home_model->select_with_where('*','offer_title="'.$deals_name_rmv_spc.'"','offer_highlights');
		
		//echo '<pre>';print_r($data['cat_id']);die();

		$cat_id=$data['cat_id'];

		$highlight_id=$cat_id[0]['id'];
		$p_id=$cat_id[0]['offer_id'];

		$data['offer_id']=explode(',', $p_id);
		$offer_id=$data['offer_id'];
		$offer_id=implode(',', $offer_id);

		//echo '<pre>';print_r($data['offer_id']);die();

		$data['cat_offer_list']=$this->offer_model->get_offer_ci('mf.offer_id in ('.$offer_id.') AND mf.status=1',8,'mf.updated_at','DESC');

		$data['offer_list']=$this->offer_model->get_offer_ci('mf.offer_id in ('.$offer_id.') AND mf.status=1',8,'mf.updated_at','DESC');


		$data['total_offer']=count($this->home_model->select_with_where('*','main_offer.offer_id in ('.$offer_id.') AND status=1','main_offer'));

		$data['user_details']=$this->home_model->select_with_where('*','loginid="'.$this->session->userdata('login_id').'"','user_login');

	    //echo '<pre>';print_r($data['offer_list']);die();
		$this->load->view('deals', $data);


	}


	public function offer_details($profile_name='')
	{
		$data['active']='home';
		$data['header']=$this->home_model->select_with_where('*','id=1','company');

		


		
		// $profile_name=implode('_',array_pop($value));
		$profile_name=$this->uri->segment(3);
		$data['profile_name']=$profile_name;

		$value=explode('_', $this->uri->segment(3));

		$offer_id=end($value);

		$user_id=$this->session->userdata('login_id');


		// update into notification table

		$val1 = array('view_status' => 1);

		$this->home_model->update_function2('user_id="'.$user_id.'" and offer_id="'.$offer_id.'"','notifications',$val1);

		$liked_id=$this->offer_model->select_with_where('*','loginid="'.$user_id.'"','user_login');

		// "<pre>";print_r($user_id);die();

		if($liked_id!=null)
		{
			if($liked_id[0]['liked_offer_id']!=null)
			{
				if(in_array($offer_id,explode(',', $liked_id[0]['liked_offer_id'])))
				{
					$data['like_status']="liked";
				}
				else
				{
					$data['like_status']="disliked";
				}
			}
			else
			{
				$data['like_status']="disliked";
			}

		}





		// $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
		$data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

		$cur_date=date('Y-m-d');

		$data['cat_brand']=array();
		$data['store_offer']=array();
		foreach ($data['category'] as $key => $value) 
		{
			$data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

			foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
			{
				$data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND mf.edate>=DATE("'.$cur_date.'") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
			}	
			
		}

		$data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','top_store=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');
		
		$data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND verify_status=1 AND is_publish=1','28');

		$data['offer_details']=$this->offer_model->get_offer_details('mf.profile_name="'.$profile_name.'"');
		
		 // "<pre>";print_r($this->session->userdata('type'));die();

		$cur_date=date('Y-m-d');
		$data['related_offer']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['offer_details'][0]['catagories'],'5','updated_at','RANDOM');

		$data['more_store']=$this->home_model->select_condition_random_with_limit('user_login','verify_status=1 AND usertype=2',6);

		$data['store_info']=$this->home_model->select_where_join('*','user_login','marchant_details','marchant_details.user_id=user_login.loginid','loginid='.$data['offer_details'][0]['offer_from'],'user_login');

		$data['store_total_ads']=count($this->home_model->select_with_where('*','offer_from='.$data['offer_details'][0]['offer_from'],'main_offer'));
		//echo "<pre>";print_r($data['store_info']);die();

		$data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

		$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');

		if($data['offer_details'][0]['total_seen']=='' || $data['offer_details'][0]['total_seen']==null)
		{
			$data['offer_details'][0]['total_seen']=0;
		}


		$user_view_details=$this->offer_model->select_with_where('user_ip','offer_id="'.$data['offer_details'][0]['main_off_id'].'"','user_offer_view');

		// "<pre>";print_r(array_column($user_view_details,'user_ip'));die();


		if(!in_array($this->input->ip_address(),array_column($user_view_details,'user_ip')))
		{
			$data_seen['total_seen']=$data['offer_details'][0]['total_seen']+1;

			$val['offer_id']=$data['offer_details'][0]['main_off_id'];
			$val['user_ip']=$this->input->ip_address();
			$val['created_at']=date('Y-m-d');

			$this->offer_model->insert('user_offer_view',$val);

			$this->offer_model->update_function2('offer_id="'.$data['offer_details'][0]['main_off_id'].'"','main_offer',$data_seen);

			$data['offer_details'][0]['total_seen']=$data_seen['total_seen'];
		}

		



		$fav_product=explode('_', $profile_name);

		// "<pre>";print_r($fav_product);die();

		$data['is_offer_save']=$this->offer_model->select_with_where('*','loginid="'.$this->session->userdata('login_id').'" and fav_offer="'.end($fav_product).'"','user_login');

		$offer_rating=$data['offer_details'][0]['total_rating']/count(explode(',',$data['offer_details'][0]['rated_user_id']));

		$data['offer_rating']=$offer_rating;

		// "<pre>";print_r($data['offer_details'][0]['rated_user_id']);die();

		if(in_array($this->session->userdata('login_id'), explode(',', $data['offer_details'][0]['rated_user_id'])))
		{
			$data['rating_status']="done";	
			$data['offer_rating_individual']=$this->home_model->select_with_where('*','user_id="'.$this->session->userdata('login_id').'" AND store_offer_id="'.$data['offer_details'][0]['main_off_id'].'"','user_rating');	
		}
		else
		{
			$data['rating_status']="not done";
		}


		$data['rating_details']=$this->offer_model->select_where_join_limit('*,u.created_at','user_rating u','user_login ul','ul.loginid=u.user_id','u.status=1 AND u.store_offer_id="'.$data['offer_details'][0]['main_off_id'].'"',4);

		
		$data['user_details']=$this->home_model->select_with_where('*','loginid="'.$this->session->userdata('login_id').'"','user_login');

		// "<pre>";print_r($data);die();

		if($data['offer_details'][0]['ctype']==6)
		{

			$data['order_details']=$this->home_model->select_with_where('*','offer_id="'.$data['offer_details'][0]['main_off_id'].'" AND payment_status="paid"','user_order');

			$this->load->view('offer_details_buy',$data);
		}
		else
		{
			$this->load->view('offer_details',$data);
		}

		
	}


	public function like_unlike_offer()
	{

		$value=explode('_', $this->uri->segment(3));

		$offer_id=end($value);
		// $profile_name=implode('_',array_pop($value));
		$profile_name=$this->uri->segment(3);

		$user_id=$this->session->userdata('login_id');

		$liked_id=$this->offer_model->select_with_where('*','loginid="'.$user_id.'"','user_login');

		$offer_info=$this->offer_model->select_with_where('*','offer_id="'.$offer_id.'"','main_offer');

		if(in_array($offer_id,explode(',', $liked_id[0]['liked_offer_id'])))
		{
			$data['total_like']=$offer_info[0]['total_like']-1;

			// if($data['total_like'] < 0)
			// {
			// 	$data['total_like']=0;
			// }


			$val['liked_offer_id']=$this->removeFromString($liked_id[0]['liked_offer_id'],$offer_id);


			$this->offer_model->update_function2('loginid="'.$user_id.'"','user_login',$val);

			// $like_status='disliked';
		}
		else
		{
			$data['total_like']=$offer_info[0]['total_like']+1;

			// if($liked_id[0]['liked_offer_id']!=null)
			// {
				// $value=$liked_id[0]['liked_offer_id'].','.$offer_id;

			$val['liked_offer_id']=$liked_id[0]['liked_offer_id'].','.$offer_id;

				// "<pre>";print_r($val['liked_offer_id']);die();
			// }
			// else
			// {
			// 	$val['liked_offer_id']=$offer_id;	
			// }

			

			$this->offer_model->update_function2('loginid="'.$user_id.'"','user_login',$val);


			// $like_status='liked';
		}

		$this->offer_model->update_function2('offer_id="'.$offer_id.'"','main_offer',$data);

		redirect('offer/offer_details/'.$profile_name);

	}

	function removeFromString($str, $item) {
		$parts = explode(',', $str);

		// "<pre>";print_r($parts);die();

		

		while(($i = array_search($item, $parts)) !== false) {
			unset($parts[$i]);
		}

		


		return implode(',', $parts);
	}

	public function save_review_post()
	{
		$profile_name=$this->uri->segment(4);

		$data = array('store_offer_id' =>$this->uri->segment(3),
			'user_id' =>$this->session->userdata('login_id'),
			'rating' =>$this->input->post('user_rating'),
			'name' =>$this->input->post('name'),
			'email' =>$this->input->post('email'),
			'review' =>$this->input->post('review'),
			'type' =>1,
			'created_at' =>date('Y-m-d'));

		$this->offer_model->insert('user_rating',$data);

		$val=$this->offer_model->select_with_where('*','offer_id="'.$this->uri->segment(3).'"','main_offer');

		if($val[0]['rated_user_id']!=null)
		{
			$rate=$val[0]['rated_user_id'].','.$this->session->userdata('login_id');
		}
		else
		{
			$rate=$this->session->userdata('login_id');
		}

		$data = array('total_rating' =>$val[0]['total_rating']+$this->input->post('user_rating'),
			'rated_user_id' =>$rate,
		);

		$this->offer_model->update_function2('offer_id="'.$this->uri->segment(3).'"','main_offer',$data);



		redirect('offer/offer_details/'.$profile_name);

		// "<pre>";print_r($this->input->post('user_rating'));die();
	} 


	public function save_offer($profile_name='')
	{	

		$fav_product=explode('_', $profile_name);

		$user_details=$this->offer_model->select_with_where('*','loginid="'.$this->session->userdata('login_id').'"','user_login');

		if($user_details[0]['fav_offer'] !=null)
		{
			if(!in_array(end($fav_product), explode(',', $user_details[0]['fav_offer'])))
			{
				$fav_id_list=$user_details[0]['fav_offer'].','.end($fav_product);
			}
			else
			{
				$fav_id_list=$this->removeFromString($user_details[0]['fav_offer'],end($fav_product));


				// $this->offer_model->update_function2('user_id="'.$user_id.'"','user_details',$val);
			}
		}
		else
		{
			$fav_id_list=end($fav_product);
		}

		// "<pre>";print_r($fav_id_list);die();

		$data = array( 

			'fav_offer' =>$fav_id_list
			
		);

		$this->offer_model->update_function2('loginid="'.$this->session->userdata('login_id').'"','user_login',$data);

		// redirect('home');

		
		// $referred_from = $this->session->userdata('referred_from');
		// redirect($referred_from, 'refresh');

		redirect($_SERVER['HTTP_REFERER']);
	}

	public function category_offer_ajax()
	{
		$cat_id=$this->input->post('cat_id');
		$offer_type=$this->input->post('offer_type');
		$offer_category=$this->input->post('offer_category');
		$location_id=$this->input->post('location_id');
		$offer_store_type=$this->input->post('offer_store_type');
		$duration=$this->input->post('duration');

		$c_date=date('Y-m-d');

		$e_date=date('Y-m-d', strtotime($c_date. ' + '.$duration.' days'));
		//echo $offer_store_type;die();

		if($cat_id!=0)
		{
			$condition='(mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.catagories='.$cat_id;

			if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration==0)
			{
				$condition='mf.offer_title_type='.$offer_type.' and (mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.catagories='.$cat_id;
			}

			if($offer_type==0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration==0)
			{
				$condition='mf.catagories='.$offer_category.' and (mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.catagories='.$cat_id;
			}

			if($offer_type==0 && $offer_category==0 && $location_id==0 && $offer_store_type!='' && $duration==0)
			{
				$condition='mf.offer_store_type='.$offer_store_type.' AND mf.catagories='.$cat_id;
			}

			if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type!='' && $duration==0)
			{
				$condition='mf.offer_title_type='.$offer_type. ' AND mf.offer_store_type='.$offer_store_type.' AND mf.catagories='.$cat_id;
			}


			if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type!='' && $duration!=0)
			{
				$condition='mf.offer_title_type='.$offer_type. ' AND mf.offer_store_type='.$offer_store_type.' AND mf.edate<="'.$e_date.'" AND mf.catagories='.$cat_id;
			}


			if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
			{
				$condition='mf.offer_title_type='.$offer_type. ' AND mf.edate<="'.$e_date.'" AND mf.catagories='.$cat_id;
			}

			if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration==0)
			{
				$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' and (mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.catagories='.$cat_id;
			}

			if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type!='' && $duration==0)
			{
				$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.offer_store_type='.$offer_store_type.' AND mf.catagories='.$cat_id;
			}

			if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type!='' && $duration!=0)
			{
				$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.offer_store_type='.$offer_store_type.' AND mf.edate<="'.$e_date.'" AND mf.catagories='.$cat_id;
			}


			if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
			{
				$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.edate<="'.$e_date.'" AND mf.catagories='.$cat_id;
			}


			if($offer_type==0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
			{
				$condition='(mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.edate<="'.$e_date.'" AND mf.catagories='.$cat_id;
			}
		}
		else
		{
			$condition='(mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.catagories='.$cat_id;

			if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration==0)
			{
				$condition='mf.offer_title_type='.$offer_type.' and (mf.offer_store_type=0 OR mf.offer_store_type=1)';
			}

			if($offer_type==0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration==0)
			{
				$condition='mf.catagories='.$offer_category.' and (mf.offer_store_type=0 OR mf.offer_store_type=1)';
			}

			if($offer_type==0 && $offer_category==0 && $location_id==0 && $offer_store_type!='' && $duration==0)
			{
				$condition='mf.offer_store_type='.$offer_store_type;
			}

			if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration==0)
			{
				$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' and (mf.offer_store_type=0 OR mf.offer_store_type=1)';
			}

			if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type!='' && $duration==0)
			{
				$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.offer_store_type='.$offer_store_type;
			}

			if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type!='' && $duration!=0)
			{
				$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.offer_store_type='.$offer_store_type.' AND mf.edate>="'.$e_date.'"';
			}


			if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
			{
				$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.edate>="'.$e_date.'"';
			}


			if($offer_type==0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
			{
				$condition='(mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.edate<="'.$e_date.'"';
			}
		}

		$data['cat_offer_list']=$this->offer_model->get_offer_ci($condition.' AND mf.status=1',18,'mf.updated_at','DESC');

		$data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

		$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');

		$this->load->view('include/offer_list', $data);
	}

	// public function deal_offer_ajax()
	// {
	// 	$offer_highlights_id=$this->input->post('id');
	// 	$offer_type=$this->input->post('offer_type');
	// 	$offer_category=$this->input->post('offer_category');
	// 	$location_id=$this->input->post('location_id');
	// 	$offer_store_type=$this->input->post('offer_store_type');
	// 	$duration=$this->input->post('duration');

	// 	$c_date=date('Y-m-d');

	// 	$e_date=date('Y-m-d', strtotime($c_date. ' + '.$duration.' days'));
	// 	//echo $offer_store_type;die();

	// 	if($cat_id!=0)
	// 	{
	// 		$condition='(mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.catagories='.$cat_id;

	// 		if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration==0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type.' and (mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.catagories='.$cat_id;
	// 		}

	// 		if($offer_type==0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration==0)
	// 		{
	// 			$condition='mf.catagories='.$offer_category.' and (mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.catagories='.$cat_id;
	// 		}

	// 		if($offer_type==0 && $offer_category==0 && $location_id==0 && $offer_store_type!='' && $duration==0)
	// 		{
	// 			$condition='mf.offer_store_type='.$offer_store_type.' AND mf.catagories='.$cat_id;
	// 		}

	// 		if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type!='' && $duration==0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type. ' AND mf.offer_store_type='.$offer_store_type.' AND mf.catagories='.$cat_id;
	// 		}


	// 		if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type!='' && $duration!=0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type. ' AND mf.offer_store_type='.$offer_store_type.' AND mf.edate<="'.$e_date.'" AND mf.catagories='.$cat_id;
	// 		}


	// 		if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type. ' AND mf.edate<="'.$e_date.'" AND mf.catagories='.$cat_id;
	// 		}

	// 		if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration==0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' and (mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.catagories='.$cat_id;
	// 		}

	// 		if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type!='' && $duration==0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.offer_store_type='.$offer_store_type.' AND mf.catagories='.$cat_id;
	// 		}

	// 		if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type!='' && $duration!=0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.offer_store_type='.$offer_store_type.' AND mf.edate<="'.$e_date.'" AND mf.catagories='.$cat_id;
	// 		}


	// 		if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.edate<="'.$e_date.'" AND mf.catagories='.$cat_id;
	// 		}


	// 		if($offer_type==0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
	// 		{
	// 			$condition='(mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.edate<="'.$e_date.'" AND mf.catagories='.$cat_id;
	// 		}
	// 	}
	// 	else
	// 	{
	// 		$condition='(mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.catagories='.$cat_id;

	// 		if($offer_type!=0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration==0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type.' and (mf.offer_store_type=0 OR mf.offer_store_type=1)';
	// 		}

	// 		if($offer_type==0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration==0)
	// 		{
	// 			$condition='mf.catagories='.$offer_category.' and (mf.offer_store_type=0 OR mf.offer_store_type=1)';
	// 		}

	// 		if($offer_type==0 && $offer_category==0 && $location_id==0 && $offer_store_type!='' && $duration==0)
	// 		{
	// 			$condition='mf.offer_store_type='.$offer_store_type;
	// 		}

	// 		if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration==0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' and (mf.offer_store_type=0 OR mf.offer_store_type=1)';
	// 		}

	// 		if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type!='' && $duration==0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.offer_store_type='.$offer_store_type;
	// 		}

	// 		if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type!='' && $duration!=0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.offer_store_type='.$offer_store_type.' AND mf.edate>="'.$e_date.'"';
	// 		}


	// 		if($offer_type!=0 && $offer_category!=0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
	// 		{
	// 			$condition='mf.offer_title_type='.$offer_type.' AND mf.catagories='.$offer_category.' AND mf.edate>="'.$e_date.'"';
	// 		}


	// 		if($offer_type==0 && $offer_category==0 && $location_id==0 && $offer_store_type=='' && $duration!=0)
	// 		{
	// 			$condition='(mf.offer_store_type=0 OR mf.offer_store_type=1) AND mf.edate<="'.$e_date.'"';
	// 		}
	// 	}

	// 	$data['cat_offer_list']=$this->offer_model->get_offer_ci($condition.' AND mf.status=1',18,'mf.updated_at','DESC');

	// 	$this->load->view('include/offer_list', $data);
	// }

}
