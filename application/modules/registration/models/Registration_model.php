<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Registration_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    } 

    public function check_data($col_name,$data) {
        $this->db->select('*');
        $this->db->from('user_login');
        $this->db->where($col_name, $data);
        $result = $this->db->get();
        return $result->result_array();
    }
        public function get_name($id)
    {
        $this->db->select('*');
        $this->db->from('user_login');
        $this->db->where('loginid', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function check_login($username, $pass) {
        $raw_sql='select * from user_login where (username="'.$username.'" OR email="'.$username.'" OR contact="'.$username.'") AND password="'.$pass.'"';
        $query = $this->db->query($raw_sql);

        return $query->result_array();
    }



    public function insert($tablename, $tabledata)
    {
        $this->db->insert($tablename, $tabledata);
    }


    public function insert_ret($tablename, $tabledata)
    {
        $this->db->insert($tablename, $tabledata);
        return $this->db->insert_id();
    }

    public function select_with_where($selector, $condition, $tablename)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function update_activation_key($email)
    {
        $this->load->helper('string');
        $activation_key=random_string('alnum', 50);
        $data = array(
         'activation_key' => $activation_key
     );
        
        $this->db->where('email', $email);
        $this->db->update('user_login', $data); 
        return $activation_key;
    }
    
    public function update_function($colname,$colvalue,$tablename,$data)
    {
        $this->db->where($colname,$colvalue);
        $this->db->update($tablename,$data); 
    }

}

?>