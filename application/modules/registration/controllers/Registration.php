<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Registration extends MX_Controller {

    //public $counter=0;
  function __construct() {
    parent::__construct();
    $this->load->library("session");
    $this->load->model('registration_model');
    $this->load->model('home/home_model');
    $this->load->model('admin/admin_model');

    $this->load->model('user/user_model');
    $this->load->model('offer/offer_model');

    $this->load->helper('text');
    $this->load->helper(array('form', 'url'));
    $this->load->helper('inflector');
    $this->load->library('encrypt');



  }

  public function index()
  { 

    $this->load->view('index');
  }

  public function head_data()
  {
    $data['company_info']=$this->home_model->select_all('company');
    $id = 1;
    $reg=$this->registration_model->select_with_where('*', 'id='.$id, 'company');
    return $reg;
  }

  public function registration_post() {

    $reg['name'] = $this->input->post('name');

    $login['email'] = $this->input->post('email');
    $login['phone'] = $this->input->post('phone');
    $login['password'] = $this->encryptIt($this->input->post('password'));

        // $reg['login_id']=$this->registration_model->insert_ret('login',$login);


        // $this->registration_model->insert('registration',$reg);

    // "<pre>";print_r($login['password']);die();


       // $this->session->set_userdata('log_scc','Successfully changed your password! Login with your "NEW" password.');
       // redirect('login/index', 'refresh');
  }

// user registration

  public function registration_user($username='',$email='',$contact='')
  {

    $company_info=$this->head_data();
    $data['company_info']=$company_info;

   
      $username=$this->input->post('username');
 
   
      $email=$this->input->post('email');
   

  
      $contact=$this->input->post('contact');
   

    
    $data_login['username']=$username;

    $data_login['email']=$email;
    
    
    $data_login['contact']='88'.$contact;

    $this->session->set_userdata('email' , $this->input->post('email'));

    $this->load->helper('string');

      //$system_password=random_string('alnum',6);

    $activation_key=random_string('alnum', 50);


    $password=$this->input->post('password');

      //$data_login['password']=$this->encryptIt($system_password);

    $data_login['password']=$this->encryptIt($this->input->post('password'));

    $c_password=$this->encryptIt($this->input->post('c_password'));
    

    $data_login['usertype']=3;
    $data_login['verify_status']=0;
    $data_login['delete_status']=0;
    $data_login['activation_status']=0;
    $data_login['add_date']=date('Y-m-d H:i:s');


    $login_id=$this->registration_model->insert_ret('user_login',$data_login);

      //below for profile name update 

    $mt=$data_login['username']."_".$login_id;
    $pname= preg_replace('/[ ,]+/', '_', trim($mt));
    $pname= str_replace('/', '_', $pname);
    $pname= str_replace('.', '_', $pname);
    
    $activation_key=$activation_key.$login_id;

    $name['name']=$pname;
    $name['activation_key']=$activation_key;
    
    
    $this->admin_model->update_function('loginid', $login_id, 'user_login',$name);

    $data_user['user_id']=$login_id;

    $this->registration_model->insert_ret('user_details',$data_user);


    $msg_from= 'support@deshioffer.com';
    $c_name='Deshioffer-Team';
    $name=$data_login['username'];


    $msg_to=$data_login['email'];
    $this->send_email_function($msg_from,$name,$msg_to,$c_name,$password,$activation_key,$login_id);

    echo 1;


  } 

  function registration_successfull()
  {

   $data['notification_details']=$this->admin_model->select_with_where('*','view_status=2 and user_id="'.$this->session->userdata('login_id').'"','notifications');

   $data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

   $data['user_info']=$this->home_model->select_with_where('*','loginid="'.$this->session->userdata('loginid').'"','user_login');


   $cur_date=date('Y-m-d');
   $data['cat_brand']=array();
   $data['store_offer']=array();

   foreach ($data['category'] as $key => $value) 
   {
    $data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

    foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
    {
      $data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND (mf.edate>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
    }   

  }

  $data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','show_on_web=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

  $data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND verify_status=1 AND is_publish=1','28');

  $data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

  $data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights'); 

  $data['feature_store']=$this->home_model->select_condition_random_with_limit('user_login','show_on_web=1 AND verify_status=1 AND usertype=2',12);


  $data['hot_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.is_hot=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','updated_at','RANDOM');

            //echo "<pre>";print_r($data['hot_offer_list']);die();

  $data['new_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")',8,'mf.offer_id','DESC');

  $data['recent_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.total_seen','DESC');




  // "<pre>";print_r($this->session->userdata);die();



   $this->load->view('check_email',$data);
}



public function test()
{
 $this->load->view('new_user_login');
}

public function test_mail()
{
  $msg_from='support@deshioffer.com';
  $name='Deshioffer-Team';
  $msg_to='aman.rabby@gmail.com';

  $this->send_email_function($msg_from,$name,$msg_to);
}


public function send_email_function($msg_from,$name,$msg_to,$c_name,$password,$activation_key,$login_id)
{
  $config = Array(
    'protocol' => 'smtp',
    'smtp_host' => 'mail.deshioffer.com',
    'smtp_port' => 25,
    'smtp_user' => 'noreply@deshioffer.com',
    'smtp_pass' => 'userpass@@@123',
    'mailtype'  => 'html', 
    'charset'   => 'iso-8859-1',
    'wordwrap' => 'TRUE',
    'newline'   => "\r\n",
    'crlf'   => "\r\n",
  );

  $this->load->library('email', $config);
  $this->email->set_newline("\r\n");

  $this->email->from($msg_from,$c_name);
  $this->email->to($msg_to);
  $base=$this->config->base_url();

  $data['title']='Registration Successfully Done';
  $data['name']=$name;
  $data['email']=$msg_to;
  $data['password']=$password;
  $data['activation_key']=$activation_key;
  $data['msg_body']='<b>Congratulation!</b> You have successfully completed your "Registration Process" in Deshioffer.You can activate your account by clicking this <a href="https://deshioffer.com/registration/acc_activation/'.$activation_key.'/'.$login_id.'"> link.</a>. 
  <p><b>Your system password is: '.$password.'</b></p><p><b>Note*: You can change your password from your panel.</b></p>';

  $message=$this->load->view('templete_email/templete_email', $data,'true');

  $this->email->subject('Deshioffer-Registraion Successfull');
  $this->email->message($message);
  $this->email->set_mailtype("html");
  $this->email->send();
}


public function acc_activation($activation_key,$login_id){

  $data['company_info']=$this->home_model->select_all('company');

  $this->load->helper('string');



  if($activation_key!='')
  {
    $exist_link=$this->home_model->select_with_where('*','activation_key="'.$activation_key.'"','user_login');

    // $this->load->view('new_user_login');

    if(count($exist_link)>0)
    {
      $activation_key=random_string('alnum', 50);

      $val['activation_key']=$activation_key.$login_id;
      $val['activation_status']=1;
      $val['verify_status']=1;

      $this->registration_model->update_function('loginid',$login_id,'user_login',$val);
    //   $this->load->view('new_user_login');
     $this->session->set_userdata('log_err','Verificaiton is Successfull');
        redirect('registration/new_user_login');
    }
    else
    {
      $this->load->view('invalid_link');
    }
  }

  else
  {
    $this->load->view('invalid_link',$data);
  }

}

public function new_user_login()
{
    
        $this->load->view('new_user_login');
}




public function update()
{

        //$data['message_error_password']=NULL;
  if($this->input->post('email') && $this->input->post('password'))
  {

   $pass=$this->encryptIt($this->input->post('password'));
   $email=$this->input->post('email');

   $result = $this->registration_model->check_login($email,$pass);

        // echo '<pre>';print_r($result);die();

   if (count($result) > 0) 
   {
    
     $this->session->set_userdata('log_err','Username and Password do not match or your profile is not activated');

    if($result[0]['verify_status']==0)
    {
      $this->session->set_userdata('log_err','Profile is not verified by Admin');
      redirect('registration/new_user_login', 'refresh');
    }

    else
    {

      $this->session->set_userdata('login_id', $result[0]['loginid']);
      $this->session->set_userdata('email' , $result[0]['email']);
      $this->session->set_userdata('type' ,$result[0]['usertype']);
      
      $name=$this->registration_model->get_name($result[0]['loginid']);
      
      $this->session->set_userdata('name' , $name[0]['username']);
      

      $this->session->set_userdata('login_scc', 'Welcome to Admin Dashboard.');

      if($result[0]['usertype']==1)
      {
        redirect('admin','refresh');
      }

      elseif($result[0]['usertype']==2)
      {
                       //echo $this->session->userdata('loginid');
       redirect('merchant','refresh');
     }

     elseif($result[0]['usertype']==3)
     {
       // echo $this->session->userdata('loginid');

       redirect('user','refresh');
     }

     
     
     
   }    
 }
 
 else
 {
      $this->session->set_userdata('log_err','Username and Password do not match or your profile is not activated');
      
      redirect('registration/new_user_login', 'refresh');
      
      
 }

}
}


public function check_email_phone_unique()
{
  $email= $this->input->post('email');
  $phone= $this->input->post('contact');
  $phone='88'.$phone;

      //echo $phone;die();

  $exist_email= $this->registration_model->select_with_where('*','email="'.$email.'"','user_login');

  $exist_phone= $this->registration_model->select_with_where('*','contact="'.$phone.'"','user_login');

  if(count($exist_email)>0)
  {
    echo 'Email is already exist.';
  }

  elseif(count($exist_phone)>0)
  {

    echo 'Phone Number is already exist.';
  }

  else
  {
    echo 'success';
  }

}




public function pass_gen()
{
  $ss=$this->encryptIt('123456');
  echo $ss;
}


function encryptIt($string) 
{
  $output = false;
  $encrypt_method = "AES-256-CBC";
  $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
  $secret_iv = 'This is my secret iv';
        // hash
  $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
  $iv = substr(hash('sha256', $secret_iv), 0, 16);

  $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
  $output = base64_encode($output);
  $output=str_replace("=", "", $output);
  return $output;
}

}
