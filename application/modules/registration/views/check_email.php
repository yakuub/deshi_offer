<title>Deshi Offer | দেশী অফার</title> 

<?php $this->load->view('front/headlink');?>



<body>

	<?php $this->load->view('front/head_nav');?>

	<div class="row">
		<div class="container">
			
			<div align="center" style="height: 400px;">

				<div class="element_box" style="padding: 30px; margin-top: 50px; height: 250;margin-bottom: 50px; width: 530px; background: #FFFFFF; border: 2px solid #F5F5F5">
					
					<div class="row">
						<div class="col-md-12">
							<img src="front_assets/images/tick.jpg" style="width: 30px ; height: 30px;border-radius: 100%;">

						</div>
						<br>
						<br>
						<div class="col-md-12">
							<p style="color: #130F7C;font-size: 20px; ">REGISTRATION SUCCESS</p>

							<hr>
						</div>

						<div class="col-md-12">
							<p style="color: #535252;font-size: 16px;">Thank you, we have sent you email to <b><?=$this->session->userdata('email');?></b>. Please click the link in that message to activate your account.</p>
						</div>
					</div>
				</div>

				<a href="home" style="color: #4C0965;font-size: 23px;font-family: Lato;">Back to Home</a>
				
			</div>
		</div>

	</div>

<?php $this->load->view('front/footer');?>
<?php $this->load->view('front/footerlink');?>
</body>
</html>