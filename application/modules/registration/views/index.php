<?php $this->load->view('admin/headlink'); ?>

<style>
    .text-right {
    text-align: center;
}

</style>

    <body class="menubar-hoverable header-fixed ">
        <!-- BEGIN LOGIN SECTION -->
        <section class="section-account">
            <div class="img-backdrop" style="background-image: url('back_assets/img/img16.jpg')"></div>
            <div class="spacer"></div>
            <div class="card contain-sm style-transparent">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <br/>
                            <span class="text-lg text-bold text-primary">Registration </span><em>(All * fields must be filled out)</em>
                            <br/><br/>
                            <form class="form floating-label"  accept-charset="utf-8" method="post">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name" name="name">

                                    <label for="name">Name(*)</label>

                                </div>
                        
                                <div class="form-group">
                                    <input onkeyup="check_email_unique()" onblur="check_email_unique()" type="email" class="form-control" id="email" name="email">
                                    <label for="email">Email(*)</label>
                                </div>

                                <div class="form-group">
                                    <input onblur="check_phone_unique()" onkeyup="check_phone_unique()" type="text" name="phone" id="phone" class="form-control" >
                                    <label>Phone(*)</label>
                                  
                                </div>


                                <div class="form-group">
                                    <input type="password" class="form-control" id="password" name="password">
                                    <label for="password">Password(*)</label>
                                    
                                </div>


                                 <div class="form-group">
                                    <input type="password" class="form-control" id="cpassword" name="confirm_password">
                                    <label for="cpassword">Confirm Password(*)</label>
                                    
                                </div>


                                <br/>
                                <p id="err_msg" style="color: #f44336;"></p>
                               
                                <div class="row">
                                    
                                    <div class="col-xs-offset-6 col-xs-6 text-right">
                                        <button id="submit_btn" class="btn btn-primary btn-raised" type="submit">Registration</button>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                            </form>
                        </div><!--end .col -->
                        <div class="col-sm-5 col-sm-offset-1 text-center">
                            <br><br><br><br><br><br><br>
                                <h3 class="text-light">
                                   Already Registered?
                                </h3>
                                <a class="btn btn-block btn-raised btn-primary" href="login">Click To Login</a>
                               
                                </div><!--end .col -->
                            </div><!--end .row -->
                        </div><!--end .card-body -->
                    </div><!--end .card -->
                </section>
                <!-- END LOGIN SECTION -->

    <?php $this->load->view('admin/footer'); ?>


    <script>

var exist=false;
        function check_email_unique() 
        {

            var email=$("#email").val();


            $("#err_msg").html('');
             $('#submit_btn').attr('disabled',false);
            $("#email").css({"border-color": "#ccc"});
            
            $.ajax({
                    type: "post",
                    url: "<?php echo site_url('registration/check_email_unique');?>",
                    data : {email:email},                  
                    success : function(data)
                    {

                   // console.log(data);
                        if(data==1)
                        {
                            $('#submit_btn').attr('disabled',true);
                            $("#email").css({"border-color": "#f44336"});
                            $("#err_msg").html('Email already registered');
                            exist=true;
                        }  

                        else
                        {
                            if(exist==false)
                            {
                                $('#submit_btn').attr('disabled',false);
                                $("#err_msg").html('');
                                exist=false;
                            }
                        }

                    }
                });  



        }





        function check_phone_unique() 
        {

            var phone=$("#phone").val();


            $("#err_msg").html('');
             $('#submit_btn').attr('disabled',false);
            $("#phone").css({"border-color": "#ccc"});
            
            $.ajax({
                    type: "post",
                    url: "<?php echo site_url('registration/check_phone_unique');?>",
                    data : {phone:phone},                  
                    success : function(data)
                    {

                    console.log(data);
                        if(data==1)
                        {
                            $('#submit_btn').attr('disabled',true);
                            $("#phone").css({"border-color": "#f44336"});
                            $("#err_msg").html('Phone no already registered');
                            exist=true;
                        }  

                        else
                        {
                            if(exist==false)
                            {
                                $('#submit_btn').attr('disabled',false);
                                $("#err_msg").html('');
                                exist=false;
                        }   }

                    }
                });  



        }



        function check_reg_form() 
        {
            var ok=true;

            $("#err_msg").html('');
            $("#name").css({"border-color": "#ccc"});
            $("#email").css({"border-color": "#ccc"});
            $("#phone").css({"border-color": "#ccc"});
            $("#password").css({"border-color": "#ccc"});

            var name=$("#name").val();
            var email=$("#email").val();
            var phone=$("#phone").val();
            var pass=$("#password").val();
            var cpass=$("#cpassword").val();

            if(name=='')
            {
                $("#name").css({"border-color": "#f44336"});
                $("#err_msg").html('* Fields can not be empty');

                ok=false;
            }

            if(email=='')
            {
                $("#email").css({"border-color": "#f44336"});
                $("#err_msg").html('* Fields can not be empty');
                ok=false;
            }

            if(phone=='')
            {
                $("#phone").css({"border-color": "#f44336"});
                $("#err_msg").html('* Fields can not be empty');
                ok=false;
            }

            if(phone.length!=11)
            {
                $("#phone").css({"border-color": "#f44336"});
                $("#err_msg").html('Enter a valid phone number Ex: 01XXXXXXXXX');
                ok=false;
            }

            if(pass=='')
            {
                $("#password").css({"border-color": "#f44336"});
                $("#err_msg").html('* Fields can not be empty');
                ok=false;
            }

            if(pass!=cpass)
            {
                $("#password").css({"border-color": "#f44336"});
                $("#cpassword").css({"border-color": "#f44336"});
                $("#err_msg").html('Password not matched');
                ok=false;
            }

            return ok;
        }

    </script>


    </body>
</html>
