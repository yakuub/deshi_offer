<?php    

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function check_login($email, $pass) {
        $this->db->select('*');
        $this->db->from('ratul_login');
        $this->db->where('user_email', $email);
        $this->db->where('Password', $pass);
        $this->db->where('act_status', 1);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function check_login_customer($email, $pass) {
        $this->db->select('*');
        $this->db->from('customer_login');
        $this->db->where('email', $email);
        $this->db->where('password', $pass);
        $this->db->where('status', 1);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_name($id)
    {
        $this->db->select('*');
        $this->db->from('customer_registration');
        $this->db->where('id', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function password_change($email,$data)
    {
        $this->db->where('email',$email);
        $this->db->update('login',$data);
    }

    public function select_group_table_data($table,$condition,$group_col)
    {
        $this->db->select('*,SUM(price-((price*discount)/100)) as total');
        $this->db->from($table);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->group_by($group_col);
        $this->db->order_by('created_at', 'desc');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function select_transaction_table_data($table,$condition)
    {
        $this->db->select('*,transaction.created_at as tran_date');
        $this->db->from($table);
        $this->db->join('customer_order','customer_order.order_id=transaction.order_id');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('transaction.created_at', 'desc');
        $this->db->group_by('customer_order.order_id');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function select_join_where($selector,$table_name,$join_table,$join_condition,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition);
        $where = '('.$condition . ')';
        $this->db->where($where);
        $result=$this->db->get();
        return $result->result_array();
    }


    public function select_join_three_table($selector,$table_name,$join_table1,$join_table2,$join_condition1,$join_condition2,$condition)
    {
        // $this->db->distinct($selector);
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table1,$join_condition1);
        $this->db->join($join_table2,$join_condition2);
        $where = '('.$condition.')';
        $this->db->where($where);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_join_four_table($selector,$table_name,$join_table1,$join_table2,$join_table3,$join_condition1,$join_condition2,$join_condition3,$condition)
    {
        // $this->db->distinct($selector);
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table1,$join_condition1);
        $this->db->join($join_table2,$join_condition2);

        $this->db->join($join_table3,$join_condition3);
        $where = '('. $condition .')';
        $this->db->where($where);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_join_four_table_left_join($selector,$table_name,$join_table1,$join_table2,$join_table3,$join_condition1,$join_condition2,$join_condition3,$condition)
    {
        // $this->db->distinct($selector);
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table1,$join_condition1,'LEFT');
        $this->db->join($join_table2,$join_condition2);

        $this->db->join($join_table3,$join_condition3);
        $where = '('. $condition .')';
        $this->db->where($where);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_join_four_table_limit_left_join($selector,$table_name,$join_table1,$join_table2,$join_table3,$join_condition1,$join_condition2,$join_condition3,$condition,$lim,$start)
    {
        // $this->db->distinct($selector);
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table1,$join_condition1,'LEFT');
        $this->db->join($join_table2,$join_condition2);

        $this->db->join($join_table3,$join_condition3);
        $where = '('. $condition .')';
        $this->db->where($where);
        $this->db->limit($lim,$start);
        $result=$this->db->get();
        return $result->result_array();
    }


}

?>