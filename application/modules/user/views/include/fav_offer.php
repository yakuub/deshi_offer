 <div class="col-md-12"> 
                    <h3 class="text-center">Active offers</h3>

                    <div class="offers_head">

                         <?php if (count($active_fav_offer_list)==0) {?>
                        
                        <p class="text-center" style="font-size: 18px;">You have no active offers. Press <i class="fa fa-heart" style="font-size: 16px;"></i> icon to save any offer.</p>

                   <?php  } ?>

                    </div>
                     

                    <?php foreach ($active_fav_offer_list as $value) { ?>
                        
                    <div class="col-md-4 col-xs-12">
                        <div class="a_offer_box col-md-12">
                            <div class="col-md-6 p_0"><span class="btn btn-danger" style="background: <?=$value['offer_color_code'];?>"><?=$value['offer_type_tilte'];?></span></div>
                            <div class="col-md-6 p_0">
                                <div class="heart pull-right" onclick="delete_favourite('<?=$value['main_off_id'];?>')" id="favourite_<?=$value['main_off_id'];?>">
                                    <input type="checkbox" class="heart__checkbox">
                                    <div class="heart__icon" style="background:red"></div>
                                </div>
                            </div>
                            <div class="offer_img_dt col-md-12 p_0">
                                <div class="col-md-5 col-xs-5 offer_images">
                                    <a target="_deshioffer" href="offer/offer_details/<?=$value['profile_name'];?>"><img style="height: 60px" src="uploads/offer/<?=$value['offer_featured_file'];?>" class="img-responsive">
                                </div>
                                <?php if($value['discount']>0){?>
                                <div class="percentage" style="right: 140px;top: 18%;font-size: 13px;"><?=$value['discount'];?>%</div>
                                <?php } ?>
                                <div class="col-md-7 col-xs-7 pr_0 off_details">
                                    <span><?=word_limiter($value['offer_title'],3);?></span>
                                     <?php if($value['sale_price']!=''){?>
                                    <span><strong><?=$value['sale_price'];?></strong></span>
                                <?php }?>
                                  <!--   <span class="txt-block">Old Price 50000</span> -->

                                    <?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($value['edate']);
                                           $diff = $date2 - $date1;
                                           $day=round($diff / 86400);?>
                                        <?php if($value['edate']=="0000-00-00"){?>
                                        <span class="text-info" style="font-weight: bold">Unlimite</span>
                                        <?php } else if($day<0){?>
                                        <span class="text-danger" style="font-weight: bold">Expired</span>
                                        <?php } else if($day>0){?>
                                        <span class="text-danger">Expire in <?=$day;?> Days.</span>
                                        <?php } else { ?>
                                        <span class="text-danger">Expire in Today</p>
                                        <?php } ?>
                                   
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 text-center fa-ofr-img">
                            <a href="store/offer/<?=$value['name'];?>"><img style="width: 80px;height: 26px;" src="uploads/user/<?=$value['profile_pic'];?>" alt=""></a></div>

                            <div class="col-md-6 p_0">
                            <?php if($value['ctype']==1){?>
                                <a onclick="get_vouchar('<?=$value['main_off_id'];?>');" class="offer_buton" style="background: <?=$value['color_code_btn'];?>">
                                <span class="offer_buton">
                                <i class="fa fa-gift"></i> <?=$value['string_name'];?>
                                </span>
                                </a>
                            <?php } else if($value['ctype']==2){?>
                             <a onclick="get_code('<?=$value['main_off_id'];?>');" class="offer_buton" style="background: <?=$value['color_code_btn'];?>">
                            <span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                          <?php } else if($value['ctype']==3){?>
                             <a onclick="get_sms('<?=$value['main_off_id'];?>');" class="offer_buton" style="background: <?=$value['color_code_btn'];?>">
                            <span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                          <?php } else if($value['ctype']==4){?>
                             <a target="_blank" class="offer_buton" href="<?=$value['ref_link'];?>" style="background: <?=$value['color_code_btn'];?>">
                            <span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                         <?php } else if($value['ctype']==5){?>
                            <a target="_blank" class="offer_buton" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>">
                                <span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                          <?php } else if($value['ctype']==6){?>
                            <a target="_blank" class="offer_buton" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>">
                           <span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                          <?php } else{ ?>
                            <a target="_blank" class="offer_buton" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>">
                            <span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a><?php }?>
                             

                                
                            </div>
                        </div>
                    </div>

                    <?php } ?>
                     
                   
                   

                </div>

                

                <div class="col-md-12">
                    
                
                    <h3 class="text-center">Inactive offers</h3>
                   
                    <div class="offers_head">
                         <?php if (count($inactive_fav_offer_list)>0) { ?>

                        <p class="text-center" style="font-size: 16px;">Offers you saved already! Usually it takes 30 days to delete automatically icon to save any offer</p>

                        <?php }?>
                    </div>

                     

               <?php foreach ($inactive_fav_offer_list as $value) { ?>

                        
                    <div class="col-md-4 col-xs-12">
                        <div class="a_offer_box col-md-12">
                            <div class="col-md-6 p_0"><span class="btn btn-danger" style="background: <?=$value['offer_color_code'];?>"><?=$value['offer_type_tilte'];?></span></div>
                            <div class="col-md-6 p_0">
                                <a class="heart pull-right" href="offer/save_offer/<?=$value['main_off_id']?>" >
                                    <input type="checkbox" class="heart__checkbox">
                                    <div class="heart__icon" style="background:red"></div>
                                </a>
                            </div>

                            <div class="offer_img_dt col-md-12 p_0">
                                <div class="col-md-5 col-xs-5 offer_images">
                                    <a target="_deshioffer" href="offer/offer_details/<?=$value['profile_name'];?>"><img style="height: 60px" src="uploads/offer/<?=$value['offer_featured_file'];?>" class="img-responsive">
                                </div>
                                <?php if($value['discount']>0){?>
                                <div class="percentage" style="right: 140px;top: 18%;font-size: 13px;"><?=$value['discount'];?>%</div>
                                <?php } ?>
                                <div class="col-md-7 col-xs-7 pr_0 off_details">
                                    <span><?=word_limiter($value['offer_title'],6);?></span>
                                     <?php if($value['sale_price']!=''){?>
                                    <span><strong><?=$value['sale_price'];?></strong></span>
                                <?php }?>
                                  <!--   <span class="txt-block">Old Price 50000</span> -->

                                    <?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($value['edate']);
                                           $diff = $date2 - $date1;
                                           $day=round($diff / 86400);?>
                                        <?php if($value['edate']=="0000-00-00"){?>
                                        <span class="text-info" style="font-weight: bold">Unlimite</span>
                                        <?php } else if($day<0){?>
                                        <span class="text-danger" style="font-weight: bold">Expired</span>
                                        <?php } else if($day>0){?>
                                        <span class="text-danger">Expire in <?=$day;?> Days.</span>
                                        <?php } else { ?>
                                        <span class="text-danger">Expire in Today</p>
                                        <?php } ?>
                                   
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 text-center fa-ofr-img">
                            <a href="store/offer/<?=$value['name'];?>"><img style="width: 80px;height: 26px;" src="uploads/user/<?=$value['profile_pic'];?>" alt=""></a></div>

                            <div class="col-md-6 p_0">
                            <?php if($value['ctype']==1){?>
                                <a onclick="get_vouchar('<?=$value['main_off_id'];?>');" class="offer_buton" style="background: <?=$value['color_code_btn'];?>">
                               <span class="">
                                <i class="fa fa-gift"></i> <?=$value['string_name'];?>
                                </span>
                                </a>
                            <?php } else if($value['ctype']==2){?>
                             <a onclick="get_code('<?=$value['main_off_id'];?>');" class="offer_buton" style="background: <?=$value['color_code_btn'];?>">
                            <span class="">
                            <i class="fa fa-gift"></i> <?=$value['string_name'];?>
                            </span></a>
                          <?php } else if($value['ctype']==3){?>
                             <a onclick="get_sms('<?=$value['main_off_id'];?>');" class="offer_buton" style="background: <?=$value['color_code_btn'];?>">
                            <span class="">
                            <i class="fa fa-gift"></i> <?=$value['string_name'];?>
                            </span></a>
                          <?php } else if($value['ctype']==4){?>
                             <a target="_blank" class="offer_buton" href="<?=$value['ref_link'];?>" style="background: <?=$value['color_code_btn'];?>">
                            <span class="">
                            <i class="fa fa-gift"></i> <?=$value['string_name'];?>
                            </span></a>
                         <?php } else if($value['ctype']==5){?>
                            <a target="_blank" class="offer_buton" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>">
                                <span class="">
                            <i class="fa fa-gift"></i> <?=$value['string_name'];?>
                            </span></a>
                          <?php } else if($value['ctype']==6){?>
                            <a target="_blank" class="offer_buton" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>">
                           <span class="">
                            <i class="fa fa-gift"></i> <?=$value['string_name'];?>
                            </span></a>
                          <?php } else{ ?>
                            <a target="_blank" class="offer_buton" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>">
                            <span class="">
                            <i class="fa fa-gift"></i> <?=$value['string_name'];?>
                            </span></a><?php }?>
                             

                                
                            </div>
                        </div>
                    </div>

                    <?php }?>
                </div>
               