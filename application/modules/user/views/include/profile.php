<div class="col-md-12">

   <?php if($this->session->userdata('password_change_msg')){ ?>
    <div class="alert alert-success" id="msg">
        <button class="close" data-close="alert"></button>
        <span><h3 class="text-center"><?=$this->session->userdata('password_change_msg');?></h3></span>
    </div>
<?php } $this->session->unset_userdata('password_change_msg');?>


<h3 class="text-center"> Profile & Settings</h3>

<ul class="nav nav-tabs p_setting_nav">
    <li class="active col-xs-3" id="p_info_li"><a data-toggle="tab" href="#p_info">Personal Info</a></li>

    <li class="col-xs-3" id="c_pass_li"><a data-toggle="tab" href="#c_pass">Change Password</a></li>

    <li class=" col-xs-3" id="s_conn_li"><a data-toggle="tab" href="#s_conn">Social Connection</a></li>

    <li class=" col-xs-3" id="preference_li"><a data-toggle="tab" href="#preference">Preference</a></li>
</ul>
<div class="tab-content">
    <div id="p_info" class="tab-pane fade in active">
        <form class="col-md-12 p_info_form" method="post" action="user/update_profile/<?=$profile_info[0]['loginid'];?>" enctype="multipart/form-data" >
            <div class="col-md-3">
                <label for="username" class="control-label">Name:</label>
            </div>
            <div class="col-md-9">
                <input type="text" name="username" value="<?=$profile_info[0]['username'];?>" class="form-control">
            </div>

            <div class="col-md-3">
                <label for="email" class="control-label">E-mail</label>
            </div>
            <div class="col-md-9">
                <input type="email" name="email" value="<?=$profile_info[0]['email'];?>" class="form-control">
            </div>
            <div class="col-md-3">
                <label for="contact" class="control-label">Mobile</label>
            </div>
            <div class="col-md-9">
                <input type="number" name="contact" value="<?=$profile_info[0]['contact'];?>" class="form-control">
            </div>

            <div class="col-md-3">
                <label for="birth_date" class="control-label">Birthday:</label>
            </div>

            <div class="col-md-9">
                <div id="datepicker" class="input-group date" data-date-format="mm-dd-yyyy" style="margin-bottom: 12px;">
                    <input class="form-control" name="dob" value="<?=$profile_info[0]['dob'];?>" type="date">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
            </div>
            <div class="col-md-3">
                <label for="profile_pic" class="control-label">Profile Picture</label>
            </div>

            <div class="col-md-9 profile_pic">

              <div class="form-group">
                 <div class="fileinput fileinput-new" data-provides="fileinput">
                  <div class=" border border-secondary fileinput-new thumbnail" style="width: 200px; height: 150px;">
                    <img src="uploads/user/profile_pic/<?=$profile_info[0]['profile_pic'];?>" data-src="holder.js/100%x100%" alt="...">
                </div>
                <div class="border border-secondary fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                <div>
                    <span class="border border-secondary btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" id="hospital_logo" name="file"></span>
                    <a href="#" class="border border-secondary btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-3">
        <label for="gender" class="control-label">Gender</label>
    </div>
    <div class="col-md-9">
        <div class="switch-field">
            <input type="radio" id="switch_left" name="gender"  <?php if($profile_info[0]['gender']=='1'){echo "checked";} ?> value="1">
            <label for="switch_left" style="margin-right: 10px;">Male</label>
            <input type="radio" id="switch_right" name="gender" <?php if($profile_info[0]['gender']=='2'){echo "checked";} ?> value="2" />
            <label for="switch_right">Female</label>
        </div>
    </div>
    <div class="col-md-3">
        <label for="mobile" class="control-label">Marital Status</label>
    </div>
    <div class="col-md-9">
        <div class="switch-field">
            <input type="radio" id="switch_left1" name="merital_status" <?php if($profile_info[0]['merital_status']=='1'){echo "checked";} ?> value="1" >
            <label for="switch_left1" style="margin-right: 10px;">Single</label>
            <input type="radio" id="switch_right1" name="merital_status" <?php if($profile_info[0]['merital_status']=='2'){echo "checked";} ?> value="2" />
            <label for="switch_right1">Married</label>
        </div>
    </div>
    <div class="col-md-3">
        <label for="mobile" class="control-label">Division</label>
    </div>
    <div class="col-md-9">
        <div class="form-group">
            <select name="division_id" id="division_id" onchange="get_district()" class="form-control select-style location js-example-placeholder-single-1 s2 s2arow width100 s2close s2border" data-placeholder="Select Division">
                <option>Select Division</option>
                <?php foreach ($division_list as $key => $row) { ?>

                    <option <?php if ($profile_info[0]['division_id']==$row['id']) 
                    { 
                      echo "selected";
                  }?>

                  value="<?=$row['id'];?>">
                  <?=$row['name'];?> 
              </option>
          <?php } ?>

      </select>
  </div>
</div>
<div class="col-md-3">
    <label for="mobile" class="control-label">District</label>
</div>
<div class="col-md-9">
    <div class="form-group">
        <select name="district_id" id="district_id" onchange="get_area()" class="form-control select-style location js-example-placeholder-single-1 s2 s2arow width100 s2close s2border" data-placeholder="Select District">
            <option>Select District</option>

            <?php foreach ($district_list as $key => $row) { ?>

                <option <?php if ($profile_info[0]['district_id']==$row['id']) 
                { 
                  echo "selected";
              }?>

              value="<?=$row['id'];?>">
              <?=$row['name'];?> 
          </option>
      <?php } ?>

  </select>
</div>
</div>
<div class="col-md-3">
    <label for="mobile" class="control-label">Area</label>
</div>
<div class="col-md-9">
    <div class="form-group">
        <select name="area_id" id="area_id" class="form-control select-style location js-example-placeholder-single-2 s2 s2arow width100 s2close s2border">
            <option>Select Area</option>
            <?php foreach ($area_list as $key => $row) { ?>
                <option <?php if ($profile_info[0]['area_id']==$row['area_id']) 
                { 
                  echo "selected";
              }?>

              value="<?=$row['area_id'];?>">
              <?=$row['area_title'];?> 
          </option>
      <?php } ?>

  </select>
</div>
</div>
<div class="col-md-3">
    <label for="save" class="control-label"></label>
</div>
<div class="col-md-9">
    <input type="submit" class="btn btn-success" style="border-radius: 0;border: none;" value="SAVE">
</div>
</form>
</div>



<div id="c_pass" class="tab-pane fade">
    <form  method="post" action="user/update_password/<?=$profile_info[0]['loginid']?>" class="col-md-12 c_pass_form">
        <div class="col-md-3">
            <label for="old_pass" class="control-label">Old Password:</label>
        </div>
        <div class="col-md-9">
            <input type="password" required="" name="old_password" class="form-control">
        </div>
        <div class="col-md-3">
            <label for="new_pass" class="control-label">New Password:</label>
        </div>
        <div class="col-md-9">
            <input type="password"  required="" name="password" class="form-control">
        </div>
        <div class="col-md-3">
            <label for="confirm_pass" class="control-label">Confirm Password:</label>
        </div>
        <div class="col-md-9">
            <input type="password"  required="" name="confirm_pass" class="form-control">
        </div>
        <div class="col-md-3">
            <label for="save" class="control-label"></label>
        </div>
        <div class="col-md-9 text-center">
            <input type="submit" class="btn btn-success" style="border-radius: 0;border: none;" value="UPDATE PASSWORD">
        </div>
    </form>
</div>
<div id="s_conn" class="tab-pane fade">
    <div class="s_conn_head col-md-12">
        <h4 class="text-center">Social Connection</h4>
        <p class="text-left user_sc_txt">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat totam rerum odit sequi soluta atque ex, possimus, error quaerat. Rem!</p>
    </div>
    <div class="col-md-12" style="padding-top: 10px;">
        <div class="col-md-2 col-xs-2"><i class="fa fa-facebook s_conn_icon"></i></div>
        <div class="col-md-7 col-xs-10">
            <h4 style="margin: 0">FACEBOOK</h4>
            <p><i>Your facebook is not connected to Deshioffer</i></p>
        </div>
        <div class="col-md-3">
            <span class="btn btn-danger">Connected</span>
        </div>
    </div>
    <div class="col-md-12" style="padding-top: 10px;">
        <div class="col-md-2 col-xs-2"><i class="fa fa-instagram s_conn_icon icoinsta"></i></div>
        <div class="col-md-7 col-xs-10">
            <h4 style="margin: 0">INSTAGRAM</h4>
            <p><i>Your instagram is not connected to Deshioffer</i></p>
        </div>
        <div class="col-md-3">
            <span class="btn btn-success">Connected</span>
        </div>
    </div>
    <div class="col-md-12" style="padding-top: 10px;">
        <div class="col-md-2 col-xs-2"><i class="fa fa-google-plus s_conn_icon icogplus"></i></div>
        <div class="col-md-7 col-xs-10">
            <h4 style="margin: 0">GOOGLE PLUS</h4>
            <p><i>Your google-plus is not connected to Deshioffer</i></p>
        </div>
        <div class="col-md-3">
            <span class="btn btn-danger">Connected</span>
        </div>
    </div>
    <div class="col-md-12" style="padding-top: 10px;">
        <div class="col-md-2 col-xs-2"><i class="fa fa-twitter s_conn_icon"></i></div>
        <div class="col-md-7 col-xs-10">
            <h4 style="margin: 0">TWITTER</h4>
            <p><i>Your twitter is not connected to Deshioffer</i></p>
        </div>
        <div class="col-md-3">
            <span class="btn btn-success">Connected</span>
        </div>
    </div>
</div>

<div id="preference" class="tab-pane fade">
    <div class="row">

        <div class="container">

            <div class="col-md-11 col-md-offset-1">

                <p style="margin-top:10px; color: #091D2D; font-size: 21px; font-family: Open Sans " class="text-center">Preferences</p>

                <p class="text-center" style="margin-top:10px;margin-bottom: 30px; color: #838A90; font-size: 17px; font-family: Open Sans ">Help us to understand your likes & Dislikes so that we can suggest you best offers according to your preference. Just select on what your preferne look like.</p>

                <?php foreach ($preference_info as $key => $value) { 

                    if(in_array($value['id'],explode(',', $profile_info[0]['preference_id'])))
                        { ?>

                          <div class="col-md-3">
                            <button style="margin-bottom:20px;height: 42px;width: 135px;" class="btn  btn-success btn-block form-control pref_button" onclick="unsave_pref('<?=$value['id']?>')"><?=$value['preference_title']?></button>
                        </div>  

                    <?php } else { ?>

                       <div class="col-md-3">
                        <button  style="margin-bottom:20px;color:white; height: 42px;width: 135px;background: #BCBCBC" class="btn form-control" onclick="save_pref('<?=$value['id']?>')"><?=$value['preference_title']?></button>
                    </div>

                <?php }

                ?>




            <?php } ?>

        </div>

    </div>



</div>
</div>
</div>
</div>

<script>

    function get_district() 
    {
        var division_id=$("#division_id").val();

        $.ajax({
            url: "<?php echo site_url('admin/get_district');?>",
            type: "post",
            data: {division_id:division_id},
            success: function(msg)
            {
                    // alert(msg);
                    $("#district_id").html(msg);
                }      
            });  
    }



    function get_area() 
    {
        var district_id=$("#district_id").val();

        $.ajax({
            url: "<?php echo site_url('admin/get_area');?>",
            type: "post",
            data: {district_id:district_id},
            success: function(msg)
            {
                    //alert(msg);
                    $("#area_id").html(msg);
                }      
            });  
    }
</script>