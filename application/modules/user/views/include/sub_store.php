
                <div class="col-md-12">
                    <h3 class="subc_h_txt"> Manage Your Alert</h3>
                    <div class="subc_info">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium voluptatum laboriosam similique saepe corporis fuga, ratione, rerum eum assumenda cum, architecto distinctio. Officiis, voluptas, aut.
                        </p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, nam quas maiores.</p>
                    </div>
                    <div class="col-md-12 a_stores">
                        <span>Stores Followed:</span>
                        <span style="font-size: 20px;font-weight: 600">
                            <?=count($user_favourite_store) ?></span>
                    </div>
                    <div class="select_store col-md-12">
                        <div class="col-md-5 p_0">
                            <div class="form-group">
                                <select id="" name="" class="form-control select2 select-style location js-example-placeholder-single-4 s2 s2arow width100 s2close s2border">
                                    <?php foreach ($store_list as $value) { ?>
                                    <option value="<?=$value['loginid']?>"><?=$value['username']?></option>
                                    <?php }?>       
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 text-center">
                            <span><strong>OR</strong></span>
                        </div>
                        <div class="col-md-5 p_0">
                            <input type="text" class="form-control" placeholder="Search by category name">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <?php foreach ($user_favourite_store as $value) { ?>
                        
                    <div class="col-md-3 col-xs-6">
                        <div class="store-box">
                           <a href="store/offer/<?=$value['name'];?>">
                            <img src="uploads/user/<?=$value['profile_pic'];?>" style="height:100%" class="img-responsive" alt=""></a> 
                        </div>
                        <span class="btn btn-danger close_store_btn" onclick="delete_store('<?=$value['loginid'];?>')">X</span>
                    </div>

                <?php }?>
                    
                </div>