 
<div class="col-md-12" id="my_sub_id"> 


    <h3 class="text-center">My Subscriptions</h3> 

    <ul class="nav nav-tabs p_setting_nav">
        <li class="<?php if($this->session->userdata('sub_tab')=='sub_cat') echo 'active'?> col-xs-4"><a data-toggle="tab" href="#sub_cat">Subscribed Category</a></li>

        <li class="<?php if($this->session->userdata('sub_tab')=='sub_store') echo 'active'?> col-xs-4"><a data-toggle="tab" href="#sub_store">Subscribed Store</a></li>

        <li class="<?php if($this->session->userdata('sub_tab')=='sub_offer') echo 'active'?> col-xs-4"><a data-toggle="tab" href="#save_offer">Saved Offer</a></li>

    </ul>

    <div class="tab-content" >
        <div id="sub_cat" class="tab-pane fade <?php if($this->session->userdata('sub_tab')=='sub_cat') echo 'in active'?>">
            <div class="col-md-12" style="margin-top: 20px;">
                <h3 class="subc_h_txt"> Manage Your Alert</h3>
                <div class="subc_info">
                    <p>

                    </p>

                </div>
                <div class="col-md-12 a_stores">
                    <span>Category Followed:</span>
                    <span style="font-size: 20px;font-weight: 600">
                     <?=count($user_favourite_cat) ?></span>
                 </div>
                 <div class="select_store col-md-12">
                    <div class="col-md-5 p_0">
                        <div class="form-group">

                            <select  onchange='clickSelectedOption(this)' name="cat_list" id="cat_list" class="form-control">
                                <option value="">Select Category</option>
                                <?php foreach ($all_cat as $key => $value) { 

                                    if(!in_array($value['catagory_id'],explode(',',$sub_cat)))
                                    {



                                        ?>

                                        <option onclick="sub_cat_fun('<?=$value['catagory_id']?>')" value="<?=$value['catagory_id']?>"><?=$value['catagory_title']?></option>
                                    <?php  } } ?>

                                </select>


                         <!-- <p>
                            <select class="locationMultiple form-control" multiple="multiple">
                              <option value="1d4h7g">Abondance (74360)</option>
                              <option value="lf9k9d">Alby-Sur-Cheran (74540)</option>
                              ...
                          </select>
                      </p> -->
                  </div>
              </div>
          </div>
      </div>

      <div class="row store_sec cat_pop_head">
          <?php foreach ($user_favourite_cat as $key => $value) {?>
            <div class="col-md-3 col-sm-3 col-xs-6 ">
                <a class="marchant_img col-md-11" href="category/offer/<?=$value['catagory_title'];?>">
                    <img style="width: 100%; height: 100%" src="uploads/category/<?=$value['catagory_pic'];?>" class="img-responsive" alt="">
                    <div class="cat_name"><?=$value['catagory_title'];?></div>
                    <div class="store_overlay"><?=$category_offer_amount[$key];?> Offers</div>
                </a>
                <span class="btn btn-danger close_store_btn" onclick="delete_cat('<?=$value['catagory_id'];?>')">X</span>
            </div>
        <?php } ?>
        <div class="clearfix"></div>   
    </div>

</div>



<div id="sub_store" class="tab-pane fade <?php if($this->session->userdata('sub_tab')=='sub_store') echo 'in active'?>" style="margin-top: 20px;">
    <div class="col-md-12">
        <h3 class="subc_h_txt"> Manage Your Alert</h3>
        <div class="subc_info">
        </div>
        <div class="col-md-12 a_stores">
            <span>Stores Followed:</span>
            <span style="font-size: 20px;font-weight: 600">
                <?=count($user_favourite_store) ?></span>
            </div>
            <div class="select_store col-md-12">
                <div class="col-md-6 p_0">
                    <div class="form-group">
                        <select  onchange='clickSelectedOption(this)' name="sub_store_list" class="form-control" id="sub_store_list">
                            <option value="">Select Store</option>
                            <?php foreach ($store_list as $value) {
                                if(!in_array($value['loginid'],explode(',',$sub_store)))
                                {


                                    ?> 
                                    <option onclick="sub_store_fun('<?=$value['loginid']?>')" value="<?=$value['loginid']?>"><?=$value['username']?></option>
                                <?php } } ?>       
                            </select>
                        </div>
                    </div>

                </div>
            </div>


            <div class="row store_sec cat_pop_head">
              <?php foreach ($user_favourite_store as $key => $value) {?>
                <div class="col-md-3 col-sm-3 col-xs-6 ">
                    <a class="marchant_img col-md-11" href="store/offer/<?=$value['name'];?>">
                        <img style="width: 100%; height: 100%" 
                        src="uploads/user/<?=$value['profile_pic'];?>" class="img-responsive" alt="">
                        <div class="store_overlay"><?=$top_store_brand_offer[$key];?> Offers</div>
                    </a>
                    <span class="btn btn-danger close_store_btn" onclick="delete_store('<?=$value['loginid'];?>')">X</span>
                </div>
            <?php } ?>
            <div class="clearfix"></div>   
        </div>


    </div>


    <div id="save_offer" class="tab-pane fade <?php if($this->session->userdata('sub_tab')=='sub_offer') echo 'in active'?>">
        <div class="col-md-12">

            <?php if ($fav_offer[0]['fav_offer']==null) { ?>

                <div class="card" style="background:white;margin-top: 30px;border: 2px solid #f4f5f7;height:120px;box-shadow: 0px 2px #d8d8d8">
                  <div class="card-body text-center" style="padding-top: 5px;">
                    <p style="color: #3D474F;font-size:17px;">You have no active saved offers. Click the <i class="fa fa-heart" aria-hidden="true"></i> icon on any offer to save it for future purchase.</p><br>

                    <a href="offer" class="btn" style="background-color:#0090FF !important;color: white;">Browse Deals</a>
                </div> 
            </div>

        <?php } else {
            foreach ($active_fav_offer_list as $key => $value) { ?>

                <div class="col-md-6">
                    <div class="card" style="background:white;margin-top: 30px;border: 2px solid #f4f5f7;height:120px;box-shadow: 0px 2px #d8d8d8">
                        <div class="card-body">
                            <div class="row" style="padding-top:10px;">
                                <div class="col-md-2">
                                    <a target="_blank" href="offer/offer_details/<?=$value['profile_name'];?>"><img style="height: 60px" src="uploads/offer/<?=$value['offer_featured_file'];?>" class="img-responsive">
                                    </a>
                                </div>

                                <div class="col-md-8" style="padding: 0px ! important;">
                                    <div style="font-size: 16px;font-weight: bold;"><a style="color: <?=$value['offer_color_code'];?>;" target="_blank" href="offer/offer_details/<?=$value['profile_name'];?>"><?=$value['offer_type_tilte'];?></a></div>
                                    <div style="font-size: 13px;color: #373A3C">
                                        <a target="_blank" href="offer/offer_details/<?=$value['profile_name'];?>"><?=$value['offer_title'];?>
                                    </a>

                                </div>
                                <span style="font-size: 13px;color: #DF2218">Expires In:<?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($value['edate']);
                                $diff = $date2 - $date1;
                                $day=round($diff / 86400);?>
                                <?php if($value['edate']=="0000-00-00"){?>
                                    <span class="text-info" style="font-weight: bold">Unlimited</span>
                                <?php } else if($day<0){?>
                                    <span class="text-danger" style="font-weight: bold">Expired</span>
                                <?php } else if($day>0){?>
                                    <span class="text-danger"><?=$day;?> Days.</span>
                                <?php } else { ?>
                                    <span class="text-danger">Today</p></span>
                                    <?php } ?></span>
                                    <span style="color:#BDBDBD;font-size: 12px;"> <i class="fa fa-circle" aria-hidden="true"></i></span>
                                    <span style="font-size: 12px;color: #0090FF;font-weight: bold;">View All: <a href="offer"><?=$value['username']?> Offer</a></span>

                                </div>


                                <div class="col-md-2 text-center"> 
                                    <a class="" onclick="delete_favourite('<?=$value['main_off_id'];?>')" href="javascript:void(0)" id=""><i class="fa fa-heart" style="font-size:24px;color: red;"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <br> 
                    </div>
                </div>

            <?php } } ?>
        </div>
    </div>
</div>
</div>
</div>


<script>

    function get_district() 
    {
        var division_id=$("#division_id").val();

        $.ajax({
            url: "<?php echo site_url('admin/get_district');?>",
            type: "post",
            data: {division_id:division_id},
            success: function(msg)
            {
                    // alert(msg);
                    $("#district_id").html(msg);
                }      
            });  
    }



    function get_area() 
    {
        var district_id=$("#district_id").val();

        $.ajax({
            url: "<?php echo site_url('admin/get_area');?>",
            type: "post",
            data: {district_id:district_id},
            success: function(msg)
            {
                    //alert(msg);
                    $("#area_id").html(msg);
                }      
            });  
    }
</script>