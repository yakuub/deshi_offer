            <div id="order_div">
              <?php if ($order_info==null) { ?>

                <div class="card" style="background:white;margin-top: 30px;border: 2px solid #f4f5f7;height:120px;box-shadow: 0px 2px #d8d8d8">
                  <div class="card-body text-center" style="padding-top: 5px;">
                    <p style="color: #3D474F;font-size:17px;">No order made ! Browse offer</p><br>

                    <a href="offer" class="btn" style="background-color:#0090FF !important;color: white;">Browse Deals</a>
                  </div> 
                </div>

              <?php } else { ?>

                <div class="col-md-12" >

                  <div class="col-md-12 cb_table_filter" style="font-family: Open Sans;">
                    <h4 style="padding: 10px 15px;font-family: Open Sans;font-size: 22px;color: #0E0E0E;font-weight: bold;">My orders</h4>
                    <div class="col-md-3">
                      <div class="form-group" style="margin-bottom: 0">
                        <select onchange="clickSelectedOption(this)" name="" class="form-control select-style location cashback_dropdown s2 s2arow width100 s2close s2border">
                          <option onclick="filtered_data('all')" value="all">Search By</option>
                          <?php foreach ($all_order as $key => $value) { 

                            if($value['id']==$selected_order_id){?>
                              <option selected onclick="filtered_data('<?=$value['id']?>')" value="<?=$value['id']?>"><?=$value['gen_order_id']?></option>
                            <?php } else { ?>
                              <option  onclick="filtered_data('<?=$value['id']?>')" value="<?=$value['id']?>"><?=$value['gen_order_id']?></option>
                            <?php } } ?>
                          </select>
                        </div>
                      </div>


                      <div class="col-md-offset-1 col-md-3">
                       <div class="form-group" style="margin-bottom: 0">
                        <div class="date">
                          <div class="input-group input-append date" id="datepicker_from">
                            <input onfocus="(this.type='date')" type="text" placeholder="Date From" class="form-control" id="date_from" name="date" value="DATE FROM"/>
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3"> 
                      <div class="form-group" style="margin-bottom: 0">
                        <div class="date">
                          <div class="input-group input-append date" id="datepicker_to">
                            <input onfocus="(this.type='date')" type="text" class="form-control" id="date_to" name="date" value="DATE TO"/>
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <button onclick="search_val()" class="btn btn-primary btn-block btn-c-b">Search</button>
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                <table class="cashback_table" id="cb_table" style="font-size:16px;font-family: Open Sans;">
                  <thead>
                    <tr>
                      <th>Order #</th>
                      <th>Date</th>
                      <th>Product</th>
                      <th>Tran. ID</th>
                      <th>Price</th>
                      <th>Total Amount</th>
                      <th>Method</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="postsList">

                    <?php foreach ($order_info as $key => $value) { ?>
                     <tr>
                      <td data-column="Order"><?=$value['gen_order_id']?></td>
                      <td data-column="Date"><?=$value['created_at']?></td>
                      <td data-column="Product"><?=$value['offer_title']?></td>
                      <td data-column="Transaction"><?=$value['auto_gen_id']?></td>
                      <td data-column="Price"><?=$value['price']?></td>
                      <td data-column="Total Amount"><?=$value['total_amount']?></td>
                      <td data-column="Method">
                        <?php if($value['total_amount']==1){ echo "Cash"; }
                        else if($value['total_amount']==2) { echo "check" ;} else { echo "Bkash"; }?></td>
                        <td data-column="Status"><?=$value['order_status']?></td>
                        <td data-column="Action"><a href="javascript:void(0)" onclick="load_tab_data('order_summary','<?=$value['id']?>')">View Order</a></td>
                      </tr>
                    <?php } ?>



                  </tbody>
                </table>

                <div class="table-footer">
                  <div class="rowcount">Showing <?=count($order_info)?> out of <?=count($order_info)?></div>

                  <!-- Paginate -->
                  <!-- <div style='margin-top: 10px;' id='pagination'></div> -->

                  <nav aria-label="Page navigation" id="pagination">
                    <ul class="pagination cashback-pagination">
                      <?=$pagination?>
                    </ul>
                  </nav>
                </div>

              <?php } ?>

            </div>

          </div>