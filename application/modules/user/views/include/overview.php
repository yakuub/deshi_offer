<div style="margin: 10px 126px 0px 81px">

               <?php if($this->session->userdata('login_scc')){ ?>
                <div class="alert alert-success" id="msg">
                    <button class="close" data-close="alert"></button>
                    <span><h3 class="text-center"><?=$this->session->userdata('login_scc');?></h3></span>
                </div>
            <?php } $this->session->unset_userdata('login_scc');?>

        </div>

        <div class="col-md-12">
            <h3>Announcement</h3>
            <div class="ance_mnt">
                <i class="fa fa-quote-left qt_icon_bdrop" aria-hidden="true"></i>
                <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus vero quis nobis temporibus aliquam iusto. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus vero quis nobis temporibus aliquam iusto.</p>
            </div>
        </div>
        <div class="col-md-12">
            <h3>Overview</h3>
            <div class="row">
                <div class="col-md-4 ovbp">
                    <div class="overview_box1 col-md-12">
                        <div class="col-md-3 p_0"><i class="fa fa-leaf"></i></div>
                        <div class="col-md-9 p_0">
                            <p class="num_ov"><?=strlen($user_info_data[0]['subscribed_category']) ? count(explode(',', $user_info_data[0]['subscribed_category'])) : 0;?></p>
                            <p class="f-15">Category Subscribed</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ovbp">
                    <div class="overview_box2 col-md-12">
                        <div class="col-md-3 p_0"><i class="fa fa-shopping-bag"></i></div>
                        <div class="col-md-9 p_0">
                            <p class="num_ov"><?=strlen($user_info_data[0]['subscribed_store']) ? count(explode(',', $user_info_data[0]['subscribed_store'])) : 0;?>
                            <p class="f-15">Store Subscribed</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ovbp">
                    <div class="overview_box3 col-md-12">
                        <div class="col-md-3 p_0"><i class="fa fa-heart"></i></div>
                        <div class="col-md-9 p_0">
                            <p class="num_ov"><?=strlen($user_info_data[0]['fav_offer']) ? count(explode(',', $user_info_data[0]['fav_offer'])) : 0;?></p>
                            <p class="f-15">Favorite Offer</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <h3>Newsletter</h3>
            <div class="nl_box col-md-12">
                <div class="col-md-7">
                    <span>You are currently subscribed to the following newsletter:</span>
                </div>
                <div class="col-md-3">
                    <span><strong>Weekly Offers</strong></span>
                </div>
                <div class="col-md-2 text-right">
                    <button class="btn btn-secondary" onclick="load_tab_data('newsletter_edit')"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;Edit</button></button>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class=" col-md-12 p_0">
                <div class="col-md-6 act p_0">
                    <h3>Recent Activity</h3>
                    <ul>
                        <?php foreach ($all_activity as $key => $value) {
                            # code...
                         ?>
                        <li><a href="javascript:void(0)"><?=$value['description']?></a></li>
                        <span><i><?=$value['day_cal']?> days ago</i></span>

                        <?php } ?>
                    </ul>
                </div>
                <div class="col-md-6 notify p_0">
                    <h3>Weekly Offers</h3>
                    <ul>
                        <?php foreach ($weekly_offer_info as $key => $value) {
                            # code...
                         ?>
                        <li><a style="color: blue;font-style: italic;" href="offer/offer_details/<?=$value['profile_name']?>"><?=$value['offer_title']?></a></li>
                        <span><i><?=$value['day_cal']?></i> days ago</span>
                        <i class="fa fa-circle circle-notify"></i>

                    <?php } ?>
                        
                    </ul>
                </div>
            </div>
        </div>