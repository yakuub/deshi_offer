<div class="order-summary">
  <div class="col-md-12 text-center">
    <h3 class="text-center" style="color:#0F0E0E ;font-size: 34px;font-family: Open Sans">Order Summary</h3>
    <hr>
    <div class="col-md-4" style="color:#707070 ;font-size: 16px;font-family: Open Sans">ORDER # <?=$orders[0]['gen_order_id']?></div>
    <div class="col-md-4" style="color:#707070 ;font-size: 16px;font-family: Open Sans"><?=date_format(date_create($orders[0]['created_at']),"d M, Y");?></div>
    <div class="col-md-4" style="color:#707070 ;font-size: 16px;font-family: Open Sans"><a href="print-order.php">PRINT ORDER</a></div>
  </div>
  <div class="order-status col-md-12">
    <div class="row">
      <div class="col-md-6" style="color:#0F0E0E ;font-size: 18px;font-family: Open Sans;font-weight: bold;">Order Status: <span style="color: red"><?=$orders[0]['order_status']?></span></div>
      <div class="col-md-6 text-right" style="color:#0F0E0E ;font-size: 18px;font-family: Open Sans;font-weight: bold;">Payment Status: <span style="color: green;"><?=$orders[0]['payment_status']?></span></div>
    </div>
  </div>

  <div class="order-details-table">
    <table class="odtable" id="cb_table">
      <thead style="color:#0F0E0E ;
      font-family: Open Sans;font-size: 18px;">
        <tr>
          <th>Product Name</th>
          <th>Price (tk)</th>
          <th>Qty</th>
          <th style="text-align: right;">Subtotal (tk)</th>

        </tr>
      </thead>
      <tbody style="
      font-family: Open Sans;font-size: 18px;">
      <tr style="height: 100px;">
        <td data-column="Product" style="text-align: left !important; font-size: 16px;width: 500px;"><?=$orders[0]['offer_title']?></td>
        <td  data-column="Total Amount"><?=$orders[0]['price']?></td>
        <td data-column="Qty"><?=$orders[0]['quantity']?></td>
        <td style="text-align: right;" data-column="Subtotal"><?=$orders[0]['total_amount']?></td>
      </tr>

      <tr style="background-color:#F4F4F4;">
        <td style="text-align: right;color: #707070;" colspan="3">Subtotal</td>
        <td style="text-align: right;"><?=$orders[0]['total_amount']?></td> 
      </tr>
      <tr style="background-color:#F4F4F4;">
        <td style="text-align: right;color: #707070;" colspan="3">Shipping & Handling</td>
        <td style="text-align: right;"><?=$orders[0]['shipping_cost']?></td> 
      </tr>
      <tr style="background-color:#F4F4F4;">
        <td style="text-align: right;color: #0F0E0E;" colspan="3"><b>Grand Total</b></td>
        <td style="text-align: right;"><b></b><?=$orders[0]['total_amount']+$orders[0]['shipping_cost']?></td> 
      </tr>



    </tbody>



  </table>
</div>

<div class="order-info-details">
  <h4 class="odinfhd">ORDER INFORMATION</h4>
  <div class="shp_address col-md-12">
    <div class="col-md-6"><h4>Shipping Address</h4>
      <ul class="p_0">
        <li><?=$orders[0]['shipping_address']?></li>
      </ul>
    </div>
    <div class="col-md-6"><h4>Billing Address</h4>
      <ul class="p_0">
       <li></li>
     </ul>
   </div>
   <div class="col-md-6"><h4>Shipping method</h4>
    <ul class="p_0">
      <li><?php if($orders[0]['total_amount']==1){ echo "Flat Rate-Fixed"; }
      else if($orders[0]['total_amount']==2) { echo "" ;} else { echo ""; }?></li>
    </ul>
  </div>  
  <div class="col-md-6"><h4>Payment method</h4>
    <ul class="p_0">
      <li><?php if($orders[0]['total_amount']==1){ echo "Cash"; }
      else if($orders[0]['total_amount']==2) { echo "check" ;} else { echo "Bkash"; }?></td></li>
    </ul>
  </div>  
</div>
</div>
</div>
