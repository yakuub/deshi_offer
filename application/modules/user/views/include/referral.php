<div class="container">
  <h2 class="text-center" style="color:#2493F0">Refer & Get <span style="color:#0BBD39">10%</span>  of your friends earnings</h2>
  <h6 class="text-center">Refer your friends and get additional % of their cash back earnings forever.</h6>

  

  <div class="row">
    <div class="col-md-12 text-center">
      <img src="front_assets/images/target.png" style="width: 100px;height: 100px;">
    </div>

    <h5 class="text-center" style="margin-bottom: 20px;">Refer your friends by sharing your unique link below to earn extra cash</h5>

    <div class="col-md-12 text-center" style="margin-bottom: 40px;">
      <span><img src="front_assets/images/fb.jpg" style="width: 50px;height: 50px;"></span>
      <span><img src="front_assets/images/instagram.jpg" style="width: 50px;height: 50px;"></span>
      <span><img src="front_assets/images/twitter.jpg" style="width: 50px;height: 50px;"></span>
      <span><img src="front_assets/images/pinterest.jpg" style="width: 50px;height: 50px;"></span>
      <!-- <span><img src="front_assets/images/target.png" style="width: 50px;height: 50px;"></span> -->
    </div>

    <div class="col-md-8 col-sm-offset-2">
      <div class="input-group">
        <input readonly="" type="text" class="form-control">
        <span class="input-group-btn">
          <button class="btn btn-primary" type="button">Copy Link</button>
        </span>
      </div>

      <h3 class="text-center">Conditions</h3>
      <hr>

      <span style="float: left;margin-right: 5px;"><i class="fa fa-check-circle"></i></span><span><h5>Referral bonus is applicable only on the cashback earnings by your friend.</h5></span>

      <span style="float: left;margin-right: 5px;"><i class="fa fa-check-circle"></i></span><span><h5>We will credit bonus to your account when your friends cashback is confirmed.</h5></span>
      <span style="float: left;margin-right: 5px;"><i class="fa fa-check-circle"></i></span><span><h5>There is no upper limit on the number of friends you can refer</h5></span>
      <span style="float: left;margin-right: 5px;"><i class="fa fa-check-circle"></i></span><span><h5>We will credit bonus to your account when your friends cash back is confirmed.</h5></span>
      <span style="float: left;margin-right: 5px;"><i class="fa fa-check-circle"></i></span><span><h5>Do I need a PayPal account to redeem a reward?</h5></span>

      <h5 style="color:#2493F0;margin-top: 40px;">Ex: If your friend earns tk.1000 confirmed cashback, You will get tk 100 as bonus</h5>
    </div>

  </div>

  <div class="card w-75 col-sm-offset-1" style="background: #DEDFDE;">
    <div class="card-body text-center">
      <br>
      <span><i class="fa fa-users fa-2x" aria-hidden="true"></i></span>

      <h4><b>Your Referrals:</b></h4>
      <form class="form-inline">

        <div class="form-group">

          <input type="text" class="form-control" id="" placeholder="">
        </div>
        <button type="submit" class="btn btn-info"><i class="fa fa-search" aria-hidden="true"></i></button>

      </form>

      <br>
    </div>
  </div>

  <div class="col-sm-offset-1" style="margin-top: 20px;">
   <table class="cashback_table" id="cb_table">
    <thead>
      <tr>
        <th>Order #</th>
        <th>Date</th>
        <th>Product</th>
        <th>Transaction ID</th>
        <th>Price</th>
        <th>Method</th>
        <th>Status</th>
        <th>Action by</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td data-column="Order">CD90EMF83</td>
        <td data-column="Date">18.03.2018</td>
        <td data-column="Product">Menz watch</td>
        <td data-column="Transaction">CD90EMF834JDI</td>
        <td data-column="Price">20.00</td>
        <td data-column="Method">Bkash</td>
        <td data-column="Status">Pending</td>
        <td data-column="Action"><a href="order-info.php">View Order</a></td>
      </tr>


    </tbody>
  </table>
  <div class="table-footer">
    <div class="rowcount">Showing 6</div>

    <nav aria-label="Page navigation">
      <ul class="pagination cashback-pagination">
        <li>
          <a href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li>
          <a href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
      </ul>
    </nav>
  </div>

</div>


</div>

