<div class="container">
  <div id="icon"></div>
  <h3 class="text-center"> Profile & Settings</h3>
  <ul class="nav nav-tabs p_setting_nav" style="margin-bottom: 15px;">
    <li class="active col-xs-4"><a data-toggle="tab" href="#myearn">My earnings</a></li>
                        <!-- 
                        -->
                        <li class="col-xs-4"><a data-toggle="tab" href="#withdraw">Withdraw</a></li>
                        <!--
                        -->
                        <li class="col-xs-4"><a data-toggle="tab" href="#paysetting">Payments Settings</a></li>
                      </ul>

                      <div class="tab-content">
                        <div id="myearn" class="tab-pane fade in active">
                          <div class="col-md-7">
                            <div class="row">
                              <div class="col-md-6 ovbp">
                                <div class="cash_box1 col-md-12">
                                  <div class="col-md-3"><i class="fa fa-leaf"></i></div>
                                  <div class="col-md-9">
                                    <p class="num_ov">0</p>
                                    <p class="f-12">Available Balance</p>
                                  </div>
                                  <i class="fa fa-info-circle info-btn"></i>
                                </div>
                              </div>
                              <div class="col-md-6 ovbp">
                                <div class="cash_box2 col-md-12">
                                  <div class="col-md-3"><i class="fa fa-leaf"></i></div>
                                  <div class="col-md-9">
                                    <p class="num_ov">0</p>
                                    <p class="f-12">Pending Earnings</p>
                                  </div>
                                  <i class="fa fa-info-circle info-btn"></i>


                                </div>
                              </div>

                              <div class="col-md-12 ovbp">
                                <div class="cash_box3 col-md-12">
                                  <div class="col-md-3"><i class="fa fa-leaf"></i></div>
                                  <div class="col-md-9">
                                    <p class="num_ov">04</p>
                                    <p class="f-15">Category Subscribed</p>

                                  </div>
                                  <i class="fa fa-info-circle info-btn"></i>

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-5">

                            <div class="faq_cashback">
                             <h4 class="faq_cash_header">Frequentry asked questions</h4> 
                             <ul>
                              <li><a href="">Lorem ipsum dolor sit amet, consectetur?</a></li>
                              <li><a href="">Lorem ipsum dolor sit amet, consectetur?</a></li>
                              <li><a href="">Lorem ipsum dolor sit amet, consectetur?</a></li>
                            </ul>
                            <div style="text-align: center;">Have more questions? <a href="">Visit Our Faq</a></div>
                          </div>
                        </div>

                        <div class="col-md-12 text-center">
                         <h4>Cashback Purchases</h4>
                         <h5><i>Monitor ALL Purchases made via deshiofferand track your cashback offer</i></h5>
                       </div>

                       <div class="col-md-12">
                        <div class="">
                          <div class="col-md-12 cb_table_filter">
                            <div class="col-md-3">
                             <div class="form-group" style="margin-bottom: 0">
                              <select name="" class="form-control select-style location cashback_dropdown s2 s2arow width100 s2close s2border">
                                <option value="1">Lorem ipsum.</option>
                                <option value="2">Lorem ipsum.</option>
                                <option value="3">Lorem ipsum.</option>
                              </select>
                            </div>
                          </div>

                          <div class="col-md-offset-1 col-md-3">
                           <div class="form-group" style="margin-bottom: 0">
                            <div class="date">
                              <div class="input-group input-append date" id="date_from">
                                <input type="text" class="form-control" name="date" value="DATE FROM"/>
                                <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3"> 
                          <div class="form-group" style="margin-bottom: 0">
                            <div class="date">
                              <div class="input-group input-append date" id="date_to">
                                <input type="text" class="form-control" name="date" value="DATE TO"/>
                                <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <button class="btn btn-primary btn-block btn-c-b">Search</button>
                        </div>
                      </div>
                    </div>
                    <table class="cashback_table" id="cb_table">
                      <thead>
                        <tr>
                          <th>Date</th>
                          <th>Merchant</th>
                          <th>Product</th>
                          <th>Amount</th>
                          <th>Cashback</th>
                          <th>Type</th>
                          <th>Status</th>
                          <th>Comfirmed by</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td data-column="Date">01.02.2018</td>
                          <td data-column="Merchant">Ajkerdeal.com</td>
                          <td data-column="Product">Menz watch</td>
                          <td data-column="Amount">200.00</td>
                          <td data-column="Cashback">20.00</td>
                          <td data-column="Type">Purchase</td>
                          <td data-column="Status">Pending</td>
                          <td data-column="Comfirmed by">18.03.2018</td>
                        </tr>

                        <tr>
                          <td data-column="Date">01.02.2018</td>
                          <td data-column="Merchant">Ajkerdeal.com</td>
                          <td data-column="Product">Menz watch</td>
                          <td data-column="Amount">200.00</td>
                          <td data-column="Cashback">20.00</td>
                          <td data-column="Type">Purchase</td>
                          <td data-column="Status">Pending</td>
                          <td data-column="Comfirmed by">18.03.2018</td>
                        </tr>
                        <tr>
                         <td data-column="Date">01.02.2018</td>
                         <td data-column="Merchant">Ajkerdeal.com</td>
                         <td data-column="Product">Menz watch</td>
                         <td data-column="Amount">200.00</td>
                         <td data-column="Cashback">20.00</td>
                         <td data-column="Type">Purchase</td>
                         <td data-column="Status">Pending</td>
                         <td data-column="Comfirmed by">18.03.2018</td>
                       </tr>
                       <tr>
                        <td data-column="Date">01.02.2018</td>
                        <td data-column="Merchant">Ajkerdeal.com</td>
                        <td data-column="Product">Menz watch</td>
                        <td data-column="Amount">200.00</td>
                        <td data-column="Cashback">20.00</td>
                        <td data-column="Type">Purchase</td>
                        <td data-column="Status">Pending</td>
                        <td data-column="Comfirmed by">18.03.2018</td>
                      </tr>
                      <tr>

                        <td data-column="Date">01.02.2018</td>
                        <td data-column="Merchant">Ajkerdeal.com</td>
                        <td data-column="Product">Menz watch</td>
                        <td data-column="Amount">200.00</td>
                        <td data-column="Cashback">20.00</td>
                        <td data-column="Type">Purchase</td>
                        <td data-column="Status">Pending</td>
                        <td data-column="Comfirmed by">18.03.2018</td>
                      </tr>
                    </tbody>
                  </table>

                  <div class="table-footer">
                    <div class="rowcount">Showing 6</div>

                    <nav aria-label="Page navigation">
                      <ul class="pagination cashback-pagination">
                        <li>
                          <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                          </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                          <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                          </a>
                        </li>
                      </ul>
                    </nav>
                  </div>

                </div>


              </div>


              <div id="withdraw" class="tab-pane fade">
                <div class="col-md-12">
                  <div class="avp-b text-center">
                    <h4>Available Balance: Tk <span style="color: #1AA1EF">0</span></h4>
                    <span>Please not that you must have minimum of <b>tk 250.00</b> confirmed cashback to to place a payout system</span>
                  </div>
                </div>

                <div class="col-md-12">
                 <div class="req_payout">
                   <div class="row">
                     <div class="col-md-6">
                       <div class="request_payout_box">
                         <span>Request Payout from</span>
                       </div>
                     </div>
                     <div class="col-md-6"></div>
                   </div>
                 </div>
               </div>

               <div class="m_withdraw">
                <div class="col-md-12 text-center">

                 <h5>Monitor ALL Withdraw made via deshiofferand track your cashback offer</h5>
               </div>

               <div class="col-md-12">
                <div class="">
                  <div class="col-md-12 cb_table_filter">
                    <div class="col-md-3">
                     <div class="form-group" style="margin-bottom: 0">
                      <select name="" class="form-control select-style location cashback_dropdown s2 s2arow width100 s2close s2border">
                        <option value="1">Lorem ipsum.</option>
                        <option value="2">Lorem ipsum.</option>
                        <option value="3">Lorem ipsum.</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-offset-1 col-md-3">
                   <div class="form-group" style="margin-bottom: 0">
                    <div class="date">
                      <div class="input-group input-append date" id="date_from_wd">
                        <input type="text" class="form-control" name="date" value="DATE FROM"/>
                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3"> 
                  <div class="form-group" style="margin-bottom: 0">
                    <div class="date">
                      <div class="input-group input-append date" id="date_to_wd">
                        <input type="text" class="form-control" name="date" value="DATE TO"/>
                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <button class="btn btn-primary btn-block btn-c-b">Search</button>
                </div>
              </div>
            </div>
            <table class="cashback_table" id="cb_table">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Merchant</th>
                  <th>Product</th>
                  <th>Amount</th>
                  <th>Cashback</th>
                  <th>Type</th>
                  <th>Status</th>
                  <th>Comfirmed by</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td data-column="Date">01.02.2018</td>
                  <td data-column="Merchant">Ajkerdeal.com</td>
                  <td data-column="Product">Menz watch</td>
                  <td data-column="Amount">200.00</td>
                  <td data-column="Cashback">20.00</td>
                  <td data-column="Type">Purchase</td>
                  <td data-column="Status">Pending</td>
                  <td data-column="Comfirmed by">18.03.2018</td>
                </tr>

                <tr>
                  <td data-column="Date">01.02.2018</td>
                  <td data-column="Merchant">Ajkerdeal.com</td>
                  <td data-column="Product">Menz watch</td>
                  <td data-column="Amount">200.00</td>
                  <td data-column="Cashback">20.00</td>
                  <td data-column="Type">Purchase</td>
                  <td data-column="Status">Pending</td>
                  <td data-column="Comfirmed by">18.03.2018</td>
                </tr>
                <tr>
                 <td data-column="Date">01.02.2018</td>
                 <td data-column="Merchant">Ajkerdeal.com</td>
                 <td data-column="Product">Menz watch</td>
                 <td data-column="Amount">200.00</td>
                 <td data-column="Cashback">20.00</td>
                 <td data-column="Type">Purchase</td>
                 <td data-column="Status">Pending</td>
                 <td data-column="Comfirmed by">18.03.2018</td>
               </tr>
               <tr>
                <td data-column="Date">01.02.2018</td>
                <td data-column="Merchant">Ajkerdeal.com</td>
                <td data-column="Product">Menz watch</td>
                <td data-column="Amount">200.00</td>
                <td data-column="Cashback">20.00</td>
                <td data-column="Type">Purchase</td>
                <td data-column="Status">Pending</td>
                <td data-column="Comfirmed by">18.03.2018</td>
              </tr>
              <tr>

                <td data-column="Date">01.02.2018</td>
                <td data-column="Merchant">Ajkerdeal.com</td>
                <td data-column="Product">Menz watch</td>
                <td data-column="Amount">200.00</td>
                <td data-column="Cashback">20.00</td>
                <td data-column="Type">Purchase</td>
                <td data-column="Status">Pending</td>
                <td data-column="Comfirmed by">18.03.2018</td>
              </tr>
            </tbody>
          </table>

          <div class="table-footer">
            <div class="rowcount">Showing 6</div>

            <nav aria-label="Page navigation">
              <ul class="pagination cashback-pagination">
                <li>
                  <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                </li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                  <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                </li>
              </ul>
            </nav>
          </div>

        </div>
      </div>
    </div>

    <div id="paysetting" class="tab-pane fade">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias alias assumenda impedit, cumque beatae quas earum soluta omnis eveniet repellendus.</p>
    </div>

  </div>
</div>