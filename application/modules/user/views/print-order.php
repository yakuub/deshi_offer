<?php $baseurl="http://localhost/deshi_offer";?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <link rel="shortcut icon" type="image/ico" href="images/favicon.ico" />
  <link rel="stylesheet" href="<?=$baseurl?>/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=$baseurl?>/assets/css/custom.css">
  <link rel="stylesheet" href="<?=$baseurl?>/assets/css/non-responsive.css">
</head>

<body>
<div class="p_order_top">
	<div class="container">
	   <div class="p_top_header col-md-12">
	   <div class="row">
	   	<div class="col-md-7 col-xs-7"><span class="invoice-logo">INVOICE</span></div>
	   <div class="col-md-5 col-xs-5 receipt_txt"><span>Receipt #212212 - July 3,2018</span></div>
	   </div>
	   </div>
 <div class="col-md-12">
 	
  <div class="pp_detailes row">
  		   <div class="col-md-7 col-xs-7 p_t_order">
	   	               <div class="row">
	   	               	<span class="col-md-3 col-xs-3 p_head">Prepared for</span><span class="col-md-9 col-xs-9 hrc"><hr class="hr-custom"></span>
	   	               </div>
	   	               <ul class="p_0">
	   	               	<li><b>Saidul Islam</b></li>
	   	               	<li>ratulking@gmail.com</li>
	   	               	<li>0170000000</li>
	   	               </ul>

	   </div>
	   <div class="col-md-5 col-xs-5 p_f_order"><span class="pto_head">Payment to</span>
	    				<ul class="p_0">
	   	               	<li><b>Deshioffer.com</b></li>
	   	               	<li>payment@deshioffer.com</li>
	   	               	<li>0170000000</li>
	   	               </ul></div>
  </div>
 </div>
	</div>
</div>

   <div class="container">
   	<div class="p_o_description col-md-12">
   		


<table class="p_o_table">
  <thead>
  <tr>
    <th colspan="3">Description</th>
    <th>Qty</th>
    <th>Price (tk.)</th>
    <th>Subtotal (tk.)</th>
  </tr>
  </thead>

    <tbody>
  <tr>
    <td colspan="3">Apple Iphone X 128gb White</td>
    <td>1X</td>
    <td>120,000.00</td>
    <td>120,000.00</td>
  </tr>
  <tr>
    <td colspan="3">Apple Iphone X 128gb White</td>
    <td>1X</td>
    <td>120,000.00</td>
    <td>120,000.00</td>
  </tr>

 <tfoot>
 	 <tr>
  	<td colspan="5">Subtotal</td>
  	<td>240,000.00</td>
  </tr>

  <tr>
  	<td colspan="5">Vat</td>
  	<td>3400.00</td>
  </tr>
  <tr>
  	<td colspan="5">Shipping</td>
  	<td>600.00</td>
  </tr>
  <tr>
  	<td colspan="5">Total</td>
  	<td>244,000.00</td>
  </tr>
 </tfoot>

  </tbody>
</table>




  

  

   	</div>
   </div>




<footer style="padding: 10px 0;">
	<div class="container">
		<div class="th_footer col-md-12 col-xs-12">
			<div class="col-md-6 col-xs-6 text-left">
		<p class="p_0">Thank you for your purchase!</p>
	</div>
	<div class="col-md-6 col-xs-6 text-right">
		<button class="btn btn-primary">Save as PDF</button>
	</div>
		</div>
	<div class="col-md-3 col-xs-3">Deshioffer Ltd. 2018</div>
	<div class="col-md-3 col-xs-3">www.deshioffer.com</div>
	<div class="col-md-3 col-xs-3">support@deshioffer.com</div>
	<div class="col-md-3 col-xs-3">01960123456</div>
	</div>
	
</footer>


</body>