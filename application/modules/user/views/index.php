<title>Deshi Offer | দেশী অফার</title>  

<?php $this->load->view('user/header_link');?>

<style>

  .pop_category .col-md-2, .all_category_div .col-md-2 {
    width: 25%;
  }
  .img_circle_nt > img
  {
    height: 100%;
  }
  a.offer_buton
  {
   display: block;
   line-height: 30px;
   height: 30px;
   color: #fff;
   background-color: #31A3FF;
   text-align: center;
   font-size: 13px;
   width: 115px;
   text-decoration: none;
   transition: background-color .3s;
   text-align: center;       
   border-radius: 3px;
   cursor: pointer;
   font-family: "opensans regular";
 }
 .offer_buton:hover
 {
  text-decoration: none;
}
.cat_name
{
  height: 20px !important;
}


</style>

<style type="text/css">

  /* Base for label styling */
  [type="checkbox"]:not(:checked),
  [type="checkbox"]:checked {
    position: absolute;
    left: -9999px;
  }
  [type="checkbox"]:not(:checked) + label,
  [type="checkbox"]:checked + label {
    position: relative;
    padding-left: 1.95em;
    cursor: pointer;
  }

  /* checkbox aspect */
  [type="checkbox"]:not(:checked) + label:before,
  [type="checkbox"]:checked + label:before {
    content: '';
    position: absolute;
    left: 0; top: 0;
    width: 3.00em; height: 3.00em;
    border: 2px solid #ccc;
    background: #fff;
    border-radius: 4px;
    box-shadow: inset 0 1px 3px rgba(0,0,0,.1);
  }
  /* checked mark aspect */
  [type="checkbox"]:not(:checked) + label:after,
  [type="checkbox"]:checked + label:after {
    content: '\2713\0020';
    position: absolute;
    top: .15em; left: .22em;
    font-size: 2.8em;
    line-height: 0.8;
    color: #09ad7e;
    transition: all .2s;
    font-family: 'Lucida Sans Unicode', 'Arial Unicode MS', Arial;
  }
  /* checked mark aspect changes */
  [type="checkbox"]:not(:checked) + label:after {
    opacity: 0;
    transform: scale(0);
  }
  [type="checkbox"]:checked + label:after {
    opacity: 1;
    transform: scale(1);
  }
  /* disabled checkbox */
  [type="checkbox"]:disabled:not(:checked) + label:before,
  [type="checkbox"]:disabled:checked + label:before {
    box-shadow: none;
    border-color: #bbb;
    background-color: #ddd;
  }
  [type="checkbox"]:disabled:checked + label:after {
    color: #999;
  }
  [type="checkbox"]:disabled + label {
    color: #aaa;
  }
  /* accessibility */
  [type="checkbox"]:checked:focus + label:before,
  [type="checkbox"]:not(:checked):focus + label:before {
    border: 2px dotted blue;
  }

  /* hover style just for information */
  label:hover:before {
    border: 2px solid #4778d9!important;
  }






  /* Useless styles, just for demo design */
  .txtcenter {
    margin-top: 4em;
    font-size: .9em;
    text-align: center;
    color: #aaa;
  }
  .copy {
   margin-top: 2em; 
 }
 .copy a {
   text-decoration: none;
   color: #4778d9;
 }
</style>

<body>

  <?php $this->load->view('user/header');?>    
  <?php $this->load->view('user/mega_menu');?>



  <!-- mobile-sidebar start -->



  <div class="wrapper sidebar-wrapper-mb">



    <!-- Sidebar Holder -->
    <nav id="mobile-sidebar" class="active">
      <ul class="list-unstyled components nav nav-tabs" role="navigation">
        <li class="nav-item">
          <a href="#overview" data-toggle="tab" role="tab">
            <i class="fa fa-home"></i>
            Overview
          </a>
        </li>
        <li class="nav-item">
          <a href="#profile" data-toggle="tab" role="tab">
            <i class="fa fa-user"></i>
            Profile
          </a>
        </li>
        <li class="nav-item">
          <a href="#mycashback" data-toggle="tab" role="tab">
           <span class="bdt-mobile">&#x9f3;</span>
           My Cashback
         </a>
       </li>
       <li class="nav-item">
        <a href="#myorders" data-toggle="tab" role="tab">
          <i class="fa fa-shopping-basket"></i>
          My Orders
        </a>
      </li>
      <li class="nav-item">
        <a href="#my_subscription" data-toggle="tab" role="tab">
          <i class="fa fa-gift"></i>
          Subscribed Category
        </a>
      </li>

      <li class="nav-item">
        <a href="#fav_offer" data-toggle="tab" role="tab">
          <i class="fa fa-heart"></i>
          Favorite Offer
        </a>
      </li>
      <li class="nav-item">
        <a href="#newsletter" data-toggle="tab" role="tab">
          <i class="fa fa-envelope"></i>
          Newsletter
        </a>
      </li>
      <li class="nav-item">
        <a href="#notification" data-toggle="tab" role="tab">
          <i class="fa fa-envelope"></i>
          Notification
        </a>
      </li>
      <li class="nav-item">
        <a href="#referrel" data-toggle="tab" role="tab">
          <i class="fa fa-envelope"></i>
          Referral
        </a>
      </li>
      <br>
      <br>
      <li class="nav-item">
        <a href="#logout" data-toggle="tab" role="tab">
          <i class="fa fa-sign-out"></i>
          Log-out
        </a>
      </li>

    </li>
  </ul>


</nav>

<!-- Page Content Holder -->
<div id="mobile-content">

  <nav class="navbar-default" style="background: none;">
    <div class="container-fluid">

      <div class="navbar-header">
        <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn" style="position: fixed;padding: 10px 15PX;">
          <i class="fa fa-cog"></i>

        </button>
      </div>


    </div>
  </nav>  
</div>
</div>

<!-- mobile-sidebar end -->


<div class="container">
  <div class="col-md-3 sidebar_tab">
    <div class="profile_details">
      <div class="user_pr_img">
        <img width="100%" src="uploads/user/profile_pic/<?=$this->session->userdata('p_image');?>" alt=""> 

      </div>
      <p class="text-center" style="color: #31A3FF;font-weight: 600;font-size: 15px;"><?=$this->session->userdata('name');?></p>
      <!-- <hr class="hr_line"> -->
      <div class="avp">
        <div>Available Tk: <span>0</span> </div>
        <div>Pending Tk: 0</div>

      </div>
    </div>

    <div class="tabbable">
      <ul class="nav nav-pills nav-stacked">
        <li onclick="load_tab_data('overview')">
          <a class="active" href="#overview" data-toggle="tab"><span class="icon-user-panel"><i class="fa fa-th-large"></i></span>Overview
          </a>
        </li>

        <li onclick="load_tab_data('profile')">
          <a href="#profile" data-toggle="tab"><span class="icon-user-panel"><i class="fa fa-user"></i></span>Profile
          </a>
        </li>

        <li onclick="load_tab_data('mycashback')">
          <a href="#mycashback" data-toggle="tab"><span class="icon-user-panel"><span class="bdt">&#x9f3;</span></span>My Cashback
          </a>
        </li>

        <li onclick="load_tab_data('myorders')">
          <a href="#myorders" data-toggle="tab"><span class="icon-user-panel">   <i class="fa fa-shopping-basket"></i></span>My Orders
          </a>
        </li>

        <li onclick="load_tab_data('my_subscription')">
          <a href="#my_subscription" data-toggle="tab"><span class="icon-user-panel"><i class="fa fa-leaf"></i></span>My Subscriptions
          </a>
        </li>

                <!-- <li onclick="load_tab_data('sub_store')">
                    <a href="#sub_store" data-toggle="tab"><span class="icon-user-panel"><i class="fa fa-shopping-bag"></i></span>Subscribed Store
                    </a>
                  </li> -->

                  <!-- <li onclick="load_tab_data('fav_offer')">
                    <a href="#fav_offer" data-toggle="tab"><span class="icon-user-panel"><i class="fa fa-heart"></i></span>Favourite Offer
                    </a>
                  </li> -->

                  <li onclick="load_tab_data('newsletter')">
                    <a href="#newsletter" data-toggle="tab"><span class="icon-user-panel"><i class="fa fa-newspaper-o" style="font-weight: 600"></i></span>Newsletter
                    </a>
                  </li>

                  <li onclick="load_tab_data('notification')">
                    <a href="#notification" data-toggle="tab"><span class="icon-user-panel"><i class="fa fa-bell"></i></span>Notifications
                    </a> <span class="notify_no" id="notification_outer"><?=count($notification_details)?></span> 
                  </li>

                  <li onclick="load_tab_data('referral')">
                    <a href="#referral" data-toggle="tab"><span class="icon-user-panel"><i class="fa fa-th-large"></i></span>Refer & Earn
                    </a>
                  </li>
                  <br>
                  <br>
                  <li onclick="load_tab_data('s_out')">
                    <a href="#s_out" data-toggle="tab"><span class="icon-user-panel"><i class="fa fa-sign-out"></i></span>Sign Out
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="tab-content col-md-9 tabbable_tab_content">


              <div class="tab-pane active" id="overview">

              </div>


              <div class="tab-pane tabbable_tab_content" id="mycashback">

              </div>


              <div class="tab-pane" id="myorders">

              </div>



              <div class="tab-pane tabbable_tab_content" id="profile">

              </div>

              <div class="tab-pane tabbable_tab_content" id="my_subscription">

              </div>

              <div class="tab-pane tabbable_tab_content" id="sub_store">

              </div>

             <!--  <div class="tab-pane tabbable_tab_content" id="fav_offer">

             </div> -->


             <div class="tab-pane tabbable_tab_content" id="newsletter">


             </div>



             <div class="tab-pane tabbable_tab_content" id="notification">

             </div>


             <div class="tab-pane" id="referral">

             </div>

           </div>
         </div>

         <script type="text/javascript">
           $.fn.select2.defaults.set("theme", "bootstrap");
           $(".locationMultiple").select2({
            width: null
          })
        </script> 

        <script type="text/javascript">
         $.fn.select2.defaults.set("theme", "bootstrap");
         $(".locationMultiple").select2({
          width: null
        })
      </script> 

      <?php $this->load->view('user/footer');?>

      <?php $this->load->view('user/footer_link');?>



      <script>

        function save_pref(p_id) {
         $.ajax({
          url: "<?php echo site_url('user/save_pref');?>",
          type: "post",
          data: {p_id:p_id},
          success: function(msg)
          {

            $("#profile").html(msg);

          }      
        }); 
       }

       function unsave_pref(p_id) {
         $.ajax({
          url: "<?php echo site_url('user/unsave_pref');?>",
          type: "post",
          data: {p_id:p_id},
          success: function(msg)
          {

            $("#profile").html(msg);

          }      
        }); 
       }

       function load_tab_data(tab_name,order_id) 
       {

        // alert(order_id);

        $.ajax({
          url: "<?php echo site_url('user/load_tab_data');?>",
          type: "post",
          data: {tab_name:tab_name,order_id:order_id},
          success: function(msg)
          {
            if(tab_name=="order_summary")
            {
             $("#order_div").html(msg);

           }
           else if(tab_name=="newsletter_edit") {
            $("#overview").html(msg);
          }
          else
          {
           $("#"+tab_name).html(msg);

         }

         $("#sub_store_list").select2({
          placeholder: "Select One",
          allowClear: true,
          width:'200px'
        }); 


         $("#cat_list").select2({
          placeholder: "Select One",
          allowClear: true,
          width:'200px'
        }); init();



    // $.getScript('https://code.jquery.com/jquery-2.2.0.min.js');


  }      
}); 
      }

      function filtered_data(arg)
      {

        var order_id=arg;

        $.ajax({
          url: "<?php echo site_url('user/filtered_data_by_order_id');?>",
          type: "post",
          data: {order_id:order_id},
          success: function(msg)
          {

            $("#order_div").html(msg);

          }      
        }); 
      }

      function search_val(argument) 
      {
        var date_from=$('#date_from').val();
        var date_to=$('#date_to').val();

        $.ajax({
          url: "<?php echo site_url('user/search_offer_by_date ');?>",
          type: "post",
          data: {date_from:date_from,date_to:date_to},
          success: function(msg)
          {

            $("#order_div").html(msg);

          }      
        }); 

      }
    </script>

    <script type="text/javascript">
      var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;

      function clickSelectedOption(select) {

        // alert('hi');
        // if(!is_chrome) return;
        select.options[select.selectedIndex].click();
      }
    </script>

    <script type='text/javascript'>

     $(document).on("click",".page_list_num", function(){ 

      $(".page_list_num").attr("href",'javascript:void(0)'); 
      var pageno = $(this).data('ci-pagination-page');
      loadPagination(pageno);

    });




     loadPagination(0);

     // Load pagination
     function loadPagination(pagno){

      $.ajax({
        url: '<?=base_url()?>user/load_tab_data/myorders/'+pagno,
        type: 'get',
        success: function(data){
          $('#order_div').html(data);


          
        }
      });
    }



  </script>

  <script>
    var url = document.location.toString();




    if (url.match('#')) 
    { 


      $('.nav-stacked li a[href="#' + url.split('#')[1] + '"]').tab('show');

      $("#c_pass").attr("class", "textboxRight");


      var element=document.getElementById('c_pass');

      element.classList.add('in','active');



     //  if(url.split('#').length > 2)
     //  {

        // alert(url.split('#')[1]);

        

     //    // $('#p_info').classList.remove('tab-pane');
     //    // element2.classList.remove();

     //   // $("#p_info").attr("class", "");


     //   var element2 = document.getElementById(url.split('#')[2]+'_li');

     //   element1.classList.add('in');
     //   element2.classList.add('active');

     // }


     $(".container").scrollTop(0);
   }
   else
   {
    $('.nav-tabs--left li a[href="#dashboard').tab('show');
    load_tab_data('dashboard');
  } 
</script>



<script>
 function search_ctrl(){    
  var width = $(window).width();
  if(width < 767){
    $(".form_wrap").css("display","none");
    $(".search_btn").attr("value","Go");
  }
  else{
    $(".form_wrap").css("display","block");
    $(".search_btn").attr("value","Show Offers");
  }
}
$(".src_btn").stop().click(function(){
  if($(window).width() < 767){
    $(this).siblings(".form_wrap").slideToggle(300);
  }
  else{
    $(this).siblings(".form_wrap").slideDown(0);
  }
});
search_ctrl();
$(window).on("resize",search_ctrl);
$(".post_offer_btn, .pop_up_wrap").hover(function(){
  $(".pop_up_wrap").stop().fadeIn(200);
},function(){
  $(".pop_up_wrap").fadeOut(200);
});

</script>
<script>


  $('.act li').each(function() {
    var back = ["green", "blue", "gray", "lightblue", "orange", "purple", "magenta"];
    var rand = back[Math.floor(Math.random() * back.length)];
    $(this).css('border-left-color', rand);
  });

    // div.pseudoStyle("before","color","purple");
  </script>
  <script>
    $('.circle-notify').each(function() {
      var back = ["green", "blue", "gray", "lightblue", "orange", "purple", "magenta"];
      var rand = back[Math.floor(Math.random() * back.length)];
      $(this).css('color', rand);
    });
  </script>
  <script>
    $(function() {
      $("#datepicker").datepicker({
        autoclose: true,
        todayHighlight: true
      }).datepicker('update', new Date());
    });

    $(function() {
      $("#datepicker_from").datepicker({
        autoclose: true,
        todayHighlight: true
      }).datepicker('update', new Date());
    });

    $(function() {
      $("#datepicker_to").datepicker({
        autoclose: true,
        todayHighlight: true
      }).datepicker('update', new Date());
    });
  </script>
  <script>
    $('#chooseFile').bind('change', function() {
      var filename = $("#chooseFile").val();
      if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
      } else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
      }
    });
  </script>

  

  <script type="text/javascript">

    function sub_cat_fun(cat_id) {



      $.ajax({
        url: "<?php echo site_url('user/insert_sub_cat_id');?>",
        type: "post",
        data: {cat_id:cat_id},
        success: function(data)
        {


         $.notify("Category successfully subscribed","success");

         $('#my_sub_id').html(data);


         $("#cat_list").select2({
          placeholder: "Select One",
          allowClear: true,
          width:'200px'
        }); 
         $("#sub_store_list").select2({
          placeholder: "Select One",
          allowClear: true,
          width:'200px'
        });init();

         



       }
     });
    }


  </script>


  <script type="text/javascript">
    function sub_store_fun(log_id) {

      $.ajax({
        url: "<?php echo site_url('user/insert_sub_store_id');?>",
        type: "post",
        data: {log_id:log_id},
        success: function(data)
        {
          $.notify("Store successfully subscribed","success");

          $('#my_sub_id').html(data);

          $("#cat_list").select2({
            placeholder: "Select One",
            allowClear: true,
            width:'200px'
          });

          $("#sub_store_list").select2({
            placeholder: "Select One",
            allowClear: true,
            width:'200px'
          }); init();


        }
      });
    }

  </script>





  <script>
    $(document).ready(function() {
      $(".floating_nav").attr("display","none");
    });


  </script>
  <script>
    $(".form-fg-pass").css("display","none");
    $(".recover_pass").click(function () {
      $(".form-signin").css("display","none")
      $(".form-fg-pass").css("display","block")
        // $("#").show();   
      });

    $(".b2login").click(function () {
      $(".form-signin").css("display","block")
      $(".form-fg-pass").css("display","none")
    });
  </script>


  <script type="text/javascript">

   $(document).ready(function () {
     $('#sidebarCollapse').on('click', function () {
       $('#mobile-sidebar').toggleClass('active');
     });
   });
 </script>


 <script>
  $('#date_from').datepicker({
    todayHighlight: true,
    format: "dd-mm-yyyy",
    viewMode: "month",
    minViewMode: "month"
  });
  $("#date_from").on("changeDate", function(event) {

  });
</script>
<script>
  $('#date_to').datepicker({
    todayHighlight: true,
    format: "dd-mm-yyyy",
    viewMode: "month",
    minViewMode: "month"
  });
  $("#date_to").on("changeDate", function(event) {

  });
</script>

<script>
  $('#date_from_wd').datepicker({
    todayHighlight: true,
    format: "dd-mm-yyyy",
    viewMode: "month",
    minViewMode: "month"
  });
  $("#date_from_wd").on("changeDate", function(event) {

  });
</script>
<script>
  $('#date_to_wd').datepicker({
    todayHighlight: true,
    format: "dd-mm-yyyy",
    viewMode: "month",
    minViewMode: "month"
  });
  $("#date_to_wd").on("changeDate", function(event) {

  });
</script>

<script>
  $('#date_from_orders').datepicker({
    todayHighlight: true,
    format: "dd-mm-yyyy",
    viewMode: "month",
    minViewMode: "month"
  });
  $("#date_from_orders").on("changeDate", function(event) {

  });
</script>
<script>
  $('#date_to_orders').datepicker({
    todayHighlight: true,
    format: "dd-mm-yyyy",
    viewMode: "month",
    minViewMode: "month"
  });
  $("#date_to_orders").on("changeDate", function(event) {

  });
</script>

<script>
  var url = document.location.toString();
  if (url.match('#')) 
  { 

    $('.nav nav-tabs li a[href="#' + url.split('#')[1] + '"]').tab('show');
          //alert(url.split('#')[1]);

          load_tab_data(url.split('#')[1]);
        }
        else
        {
          $('.nav nav-tabs li a[href="#overview').tab('show');
          load_tab_data('overview');
        } 

      </script>


      <script>

        function delete_favourite(offer_id) 
        {
          var login_id="<?=$this->session->userdata('login_id');?>"

          $.ajax({
            url: "<?php echo site_url('user/delete_favourite_offer');?>",
            type: "post",
            data: {offer_id:offer_id,login_id:login_id},
            success: function(data)
            {

              $.notify("Offer successfully unsubscribed","success");
              $('#my_sub_id').html(data);
            }      
          });

        }

      </script>


      <script>

        function delete_cat(cat_id) 
        {


         console.log(cat_id);
         $.ajax({
          url: "<?php echo site_url('user/delete_favourite_cat');?>",
          type: "post",
          data: {cat_id:cat_id},
          success: function(data)
          {
            $.notify("Category successfully unsubscribed","success");

            $('#my_sub_id').html(data);

            $("#cat_list").select2({
              placeholder: "Select One",
              allowClear: true,
              width:'200px'
            });
            $("#sub_store_list").select2({
              placeholder: "Select One",
              allowClear: true,
              width:'200px'
            }); init();
          }      
        });

       }

     </script>

     <script>

      function delete_store(login_id) 
      {


       $.ajax({
        url: "<?php echo site_url('user/delete_favourite_store');?>",
        type: "post",
        data: {login_id:login_id},
        success: function(data)
        {
         $.notify("Store successfully unsubscribed","success");
         
         $('#my_sub_id').html(data);

         $("#cat_list").select2({
          placeholder: "Select One",
          allowClear: true,
          width:'200px'
        });

         $("#sub_store_list").select2({
          placeholder: "Select One",
          allowClear: true,
          width:'200px'
        });init();

       }      
     });

     }

   </script>

 </body>
 </html>