<?php if (!defined('BASEPATH')) exit('No direct script access allowed');             

class User extends MX_Controller {  

    //public $counter=0;
    function __construct()
    {
        date_default_timezone_set('Asia/Dhaka');

        $this->load->model('admin/admin_model');
        $this->load->model('home/home_model');
        $this->load->model('user/user_model');
        $this->load->model('offer/offer_model');
        $this->load->library("pagination");


        // $this->session->set_userdata('password_change_msg','');

        if($this->session->userdata('language_select')=='bangla')
        {
            $this->lang->load('admin', 'bangla');
        }
        else
        {
            $this->lang->load('admin', 'english');
        }

        $login_id =   $this->session->userdata('login_id');
        $type     =    $this->session->userdata('type');
        $name     =    $this->session->userdata('name');
        $email     =    $this->session->userdata('email');

       //echo '<pre>';print_r($name);die();


        if($login_id=='' || $type==1 || $type==2)
        {
            $this->session->unset_userdata('login_id');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('type'); 
            $this->session->set_flashdata('Error','You Have No Access to User Panel');
            redirect('home','refresh');
        }

        $this->session->set_userdata('sub_tab','sub_cat');
    }

    public function index()
    {

       $data['notification_details']=$this->admin_model->select_with_where('*','view_status=2 and status=1 and user_id="'.$this->session->userdata('login_id').'"','notifications');

       $data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

       $data['user_info']=$this->home_model->select_with_where('*','loginid="'.$this->session->userdata('loginid').'"','user_login');


       $cur_date=date('Y-m-d');
       $data['cat_brand']=array();
       $data['store_offer']=array();

       foreach ($data['category'] as $key => $value) 
       {
        $data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

        foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
        {
            $data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND (mf.edate>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
        }   

    }

    $data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','show_on_web=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

    $data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND verify_status=1 AND is_publish=1','28');

    $data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

    $data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights'); 

    $data['feature_store']=$this->home_model->select_condition_random_with_limit('user_login','show_on_web=1 AND verify_status=1 AND usertype=2',12);


    $data['hot_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.is_hot=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','updated_at','RANDOM');

            //echo "<pre>";print_r($data['hot_offer_list']);die();

    $data['new_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")',8,'mf.offer_id','DESC');

    $data['recent_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.total_seen','DESC');

       //$this->session->set_userdata('login_scc','Welcome To User Panel');
    $login_id=$this->session->userdata('login_id');
    $data['user_info']=$this->admin_model->select_with_where('*','loginid="'.$login_id.'"','user_login');

    $this->load->view('index',$data);

}

public function load_tab_data($arg="",$order_id="")
{
    $data['company_info']=$this->home_model->select_all('company');
    $login_id=$this->session->userdata('login_id');
    $tab_name=$this->input->post('tab_name');
    $cur_date=date('Y-m-d');

    if($tab_name==null)
    {
        $tab_name=$arg;
    }

        // echo $login_id;die();

    if($tab_name=='overview') 
    {
        $data['user_info_data']=$this->admin_model->select_with_where('*','loginid="'.$login_id.'"','user_login');

        $data['all_offer']=$this->admin_model->select_with_where('*,DATEDIFF(now(),date(sdate)) as day_cal','date(sdate) between date_sub(now(),INTERVAL 1 WEEK) and now() and date(edate) > curdate()','main_offer');

        $data['all_activity']=$this->admin_model->select_with_where_decending_limit('*,DATEDIFF(now(),date(created_at)) as day_cal','user_id="'.$login_id.'"','recent_activity',7);

        $data['weekly_offer_info']=$this->admin_model->select_join_where_limit('*,DATEDIFF(now(),date(m.sdate)) as day_cal','weekly_offer w','main_offer m','m.offer_id=w.offer_id','w.status=1',7);
        // "<pre>";print_r($data['profile_info']);die();
    }

    if($tab_name=='order_summary')
    {
        $data['user_info_data']=$this->admin_model->select_with_where('*','loginid="'.$login_id.'"','user_login');

        $login_id=$this->session->userdata('login_id');
        
        $data['orders']=$this->user_model->select_join_four_table_left_join('*,o.created_at,o.total_amount,o.price','user_order o','order_transaction ot','main_offer mo','offer_type t','o.id=ot.order_id','o.offer_id=mo.offer_id','mo.offer_title_type=t.offer_id','o.user_id="'.$login_id.'" AND o.id="'.$_POST['order_id'].'"');
        // "<pre>";print_r($data['profile_info']);die();
    }


    if($tab_name=='profile')
    {
        $data['division_list']=$this->admin_model->select_all('divisions');

        $data['district_list']=$this->admin_model->select_all('districts');

        $data['area_list']=$this->admin_model->select_all('area');

        $data['profile_info']=$this->admin_model->select_where_left_join('*','user_login','user_details','user_details.user_id=user_login.loginid','user_details.user_id='.$login_id);

        $data['preference_info']=$this->admin_model->select_all('preference');

            // echo "<pre>";print_r($data['profile_info']);die();
    }

    if($tab_name=='mycashback')
    {
        $data['division_list']=$this->admin_model->select_all('divisions');

        $data['district_list']=$this->admin_model->select_all('districts');


    }
    
    if($tab_name=='myorders')
    {

        $data['all_order']=$this->admin_model->select_with_where('*','user_id="'.$login_id.'"','user_order');


        $data['selected_order_id']="";

        $login_id=$this->session->userdata('login_id');
        $data['orders']=$this->user_model->select_join_four_table('*,o.created_at,o.total_amount,o.price,o.id','user_order o','order_transaction ot','main_offer mo','offer_type t','o.id=ot.order_id','o.offer_id=mo.offer_id','mo.offer_title_type=t.offer_id','o.user_id="'.$login_id.'"');

        // Row per page
        $rowperpage = 10;


        if($this->uri->segment(4)==null)
        {
            $rowno=0;
        }
        else
        {
           $rowno=$this->uri->segment(4); 

       }




    // Row position
       if($rowno != 0){
          $rowno = ($rowno-1) * $rowperpage;
           // "<pre>";print_r($rowno);die();
      }

    // All records count
      $allcount = count($data['orders']);

    // Get records
      $users_record = $this->user_model->select_join_four_table_limit_left_join('*,o.created_at,o.total_amount,o.price,o.id','user_order o','order_transaction ot','main_offer mo','offer_type t','o.id=ot.order_id','o.offer_id=mo.offer_id','mo.offer_title_type=t.offer_id','o.user_id="'.$login_id.'"',$rowperpage,$rowno);


      //  if($rowno != 0){
      //    "<pre>";print_r($users_record);
      //    "<pre>";print_r($rowno);
      //    "<pre>";print_r($rowperpage);die();
      // }

    // Pagination Configuration
      $config['base_url'] = '';
      $config['use_page_numbers'] = TRUE;
      $config['total_rows'] = $allcount;
      $config['per_page'] = $rowperpage;
      $config['attributes'] = array('class' => 'page_list_num');

    // Initialize
      $this->pagination->initialize($config);

    // Initialize $data Array
      $data['pagination'] = $this->pagination->create_links();
      $data['order_info'] = $users_record;
      $data['row'] = $rowno;

        // "<pre>";print_r($data['pagination']);die();

  }
  if($tab_name=='my_subscription')
  {


    $data['fav_cat']=$this->admin_model->select_with_where('*','loginid='.$login_id,'user_login');

    $data['all_cat']=$this->admin_model->select_all('catagory');

    if($data['fav_cat'][0]['subscribed_category']==null)
    {
        $sub_cat=0;
    }
    else
    {
        $sub_cat=$data['fav_cat'][0]['subscribed_category'];
    }


    if($data['fav_cat'][0]['subscribed_store']==null)
    {

     $sub_store=0;
 }
 else
 {
    $sub_store=$data['fav_cat'][0]['subscribed_store'];
}

$data['user_favourite_cat']=$this->admin_model->select_with_where('*','catagory.catagory_id in ('.$sub_cat.')','catagory');

$data['sub_cat']=$sub_cat;
$data['sub_store']=$sub_store;

// "<pre>";print_r($data['user_favourite_cat']);die();


$data['user_favourite_store']=$this->admin_model->select_with_where('*','loginid in ('.$sub_store.')','user_login');



$data['store_list']=$this->admin_model->select_with_where('*','usertype=2 AND verify_status=1','user_login');

$data['fav_store']=$this->admin_model->select_with_where('*','loginid='.$login_id,'user_login');


$data['fav_offer']=$this->admin_model->select_with_where('*','loginid='.$login_id,'user_login');

if($data['fav_offer'][0]['fav_offer']!=null)
{
    $data['active_fav_offer_list']=$this->offer_model->get_offer_ci('mf.offer_id in ('.$data['fav_offer'][0]['fav_offer'].')  AND status=1',40,'mf.updated_at','DESC');

        // "<pre>";print_r($data['active_fav_offer_list']);die();

        // AND (mf.edate >= DATE("'.$cur_date.'"))

    $data['inactive_fav_offer_list']=$this->offer_model->get_offer_ci('mf.offer_id in ('.$data['fav_offer'][0]['fav_offer'].') AND (mf.edate < DATE("'.$cur_date.'")) AND status=1',40,'mf.updated_at','DESC');
}
$data['page_category']=$this->home_model->select_with_where('*','front_page_show_st=1','catagory');

$data['category_offer_amount']=array();

foreach ($data['page_category'] as $key => $value) 
{
    $data['category_offer_amount'][$key]=count($this->home_model->select_with_where('*','status=1 AND catagories='.$value['catagory_id'],'main_offer'));
}

$data['top_brand_feature']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND verify_status=1 AND is_publish=1','30');

$data['top_store_brand_offer']=array();

foreach ($data['top_brand_feature'] as $key => $value) 
{
    $data['top_store_brand_offer'][$key]=count($this->home_model->select_with_where('*','status=1 AND offer_from='.$value['loginid'],'main_offer'));
}


}

if($tab_name=='newsletter' || $tab_name=='newsletter_edit')
{
    $data['division_list']=$this->admin_model->select_all('divisions');

    $data['district_list']=$this->admin_model->select_all('districts');

    $tab_name='newsletter';
    
    $data['user_info']=$this->admin_model->select_with_where('*','loginid="'.$login_id.'"','user_login');
}

if($tab_name=='notification')
{
    $data['notification_details']=$this->user_model->select_join_four_table('*','notifications n','main_offer m','catagory c','user_login u','m.offer_id=n.offer_id','m.catagories=c.catagory_id','m.offer_from=u.loginid','n.status=1 and  user_id="'.$this->session->userdata('login_id').'"');

    $data['notification_details_visited_no']=count($this->admin_model->select_with_where('*','view_status=2 and status=1 and user_id="'.$this->session->userdata('login_id').'"','notifications'));

   
    
    if($data['notification_details']!=null)
    {
        $start = new \DateTime($data['notification_details'][0]['created_at']);
        $end   = new \DateTime(date('Y-m-d H:i:s'));

        $interval = $end->diff($start);

        if($interval->d == 0)
        {
            $day="";
        }
        else
        {
            $day=$interval->d." day ";
        }

        if($interval->h == 0)
        {
            $hour="";
        }

        else
        {
            $hour=$interval->h.' hours ';
        }

        if($interval->i == 0)
        {
            $min="";
        }

        else
        {
         $min=$interval->i.' minutes '; 
     }

     $data['time'] = $day.$hour. $min.' ago';
 }




    // sprintf(
    //     '%03d days %02d:%02d:%02d',
    //     $interval->d,
    //      $interval->h,
    //     $interval->i,
    //     $interval->s
    // );

    // "<pre>";print_r($time);die();
} 

if($tab_name=='referral')
{
 $data['division_list']=$this->admin_model->select_all('divisions');

 $data['district_list']=$this->admin_model->select_all('districts');
}

if($tab_name=='logout')
{
    $data['logout']='';
}
        //echo $tab_name;

$this->load->view('include/'.$tab_name,$data);

}


public function filtered_data_by_order_id($value='')
{
    $order_id=$this->input->post('order_id');
    $login_id=$this->session->userdata('login_id');

     $data['all_order']=$this->admin_model->select_with_where('*','user_id="'.$login_id.'"','user_order');

    $login_id=$this->session->userdata('login_id');
    $data['orders']=$this->user_model->select_join_four_table_left_join('*,o.created_at','user_order o','order_transaction ot','main_offer mo','offer_type t','o.id=ot.order_id','o.offer_id=mo.offer_id','mo.offer_title_type=t.offer_id','o.user_id="'.$login_id.'"');

        // Row per page
    $rowperpage = 10;

    $row=0;

    // All records count
    $allcount = count($data['orders']);

    // Pagination Configuration
    $config['base_url'] = '';
    $config['use_page_numbers'] = TRUE;
    $config['total_rows'] = $allcount;
    $config['per_page'] = $rowperpage;
    $config['attributes'] = array('class' => 'page_list_num');

    // Initialize
    $this->pagination->initialize($config);

    // Initialize $data Array
    $data['pagination'] = $this->pagination->create_links();

    if($order_id=="all")
    {
        $data['order_info']=$this->user_model->select_join_four_table_left_join('*,o.created_at','user_order o','order_transaction ot','main_offer mo','offer_type t','o.id=ot.order_id','o.offer_id=mo.offer_id','mo.offer_title_type=t.offer_id','o.user_id="'.$login_id.'"');
    }
    else
    {
        $data['order_info']=$this->user_model->select_join_four_table_left_join('*,o.created_at','user_order o','order_transaction ot','main_offer mo','offer_type t','o.id=ot.order_id','o.offer_id=mo.offer_id','mo.offer_title_type=t.offer_id','o.user_id="'.$login_id.'" and o.id="'.$order_id.'"');
    }





    $data['selected_order_id']=$order_id;

    $this->load->view('include/myorders',$data);

}

public function insert_newsletter_data($value='')
{
    $newsletter_id=$this->input->post('newsletter');

    $val['newsletter_id']=implode(',',$newsletter_id);

    $user_id=$this->session->userdata('login_id');

    $this->admin_model->update_function('loginid', $user_id, 'user_login',$val);

    // "<pre>";print_r($val['newsletter_id']);die();

    redirect('user');

    
}

public function insert_sub_cat_id($value='')
{
    $cat_id=$this->input->post('cat_id');

    $user_id=$this->session->userdata('login_id');

    $user_info=$this->admin_model->select_with_where('*','loginid="'.$user_id.'"','user_login');


    if(!in_array($cat_id,explode(',',$user_info[0]['subscribed_category'])))
    {
        if($user_info[0]['subscribed_category']!=null)
        {
            $val['subscribed_category']=$user_info[0]['subscribed_category'].','.$cat_id;

        }
        else
        {
            $val['subscribed_category']=$cat_id;
        }

        $this->admin_model->update_function('loginid', $user_id, 'user_login',$val);
    }
    $this->session->set_userdata('sub_tab' , 'sub_cat');

    $this->load_tab_data('my_subscription');
}

public function insert_sub_store_id($value='')
{
    $log_id=$this->input->post('log_id');

    $user_id=$this->session->userdata('login_id');

    $user_info=$this->admin_model->select_with_where('*','loginid="'.$user_id.'"','user_login');


    if(!in_array($log_id,explode(',',$user_info[0]['subscribed_store'])))
    {
        if($user_info[0]['subscribed_store']!=null)
        {
            $val['subscribed_store']=$user_info[0]['subscribed_store'].','.$log_id;

        }
        else
        {
            $val['subscribed_store']=$log_id;
        }

        $this->admin_model->update_function('loginid', $user_id, 'user_login',$val);
    }

    $this->session->set_userdata('sub_tab' , 'sub_store');

    $this->load_tab_data('my_subscription');
}

public function save_pref($value='')
{

    $preference_id=$this->input->post('p_id');

    $user_id=$this->session->userdata('login_id');

    $user_info=$this->admin_model->select_with_where('*','loginid="'.$user_id.'"','user_login');


    if(!in_array($preference_id,explode(',',$user_info[0]['preference_id'])))
    {
        if($user_info[0]['preference_id']!=null)
        {
            $val['preference_id']=$user_info[0]['preference_id'].','.$preference_id;

        }
        else
        {
            $val['preference_id']=$preference_id;
        }

        $this->admin_model->update_function('loginid', $user_id, 'user_login',$val);
    }

    $this->load_tab_data('profile');
}

public function unsave_pref($value='')
{
   $user_id=$this->session->userdata('login_id');

   $preference_id=$this->input->post('p_id');

        // echo '<pre>';print_r($offer_id);die();

   $user_info=$this->admin_model->select_with_where('*','loginid="'.$user_id.'"','user_login');

        //echo '<pre>';print_r($favourite_offer_id);die();

   $val['preference_id']=$this->removeFromString($user_info[0]['preference_id'],$preference_id);

   $this->admin_model->update_function('loginid', $user_id, 'user_login',$val);

   $this->load_tab_data('profile');


}

public function update_profile($login_id)
{
    $data['username']=$this->input->post('username');
    $data['email']=$this->input->post('email');
    $data['contact']=$this->input->post('contact');

    $this->admin_model->update_function('loginid', $login_id, 'user_login',$data);


    $user['dob']=$this->input->post('dob');
    $user['gender']=$this->input->post('gender');
    $user['merital_status']=$this->input->post('merital_status');
    $user['division_id']=$this->input->post('division_id');
    $user['district_id']=$this->input->post('district_id');
    $user['area_id']=$this->input->post('area_id');



    $this->admin_model->update_function('user_id', $login_id, 'user_details',$user);


    //below for profile name update 

    $mt=$data['username']."_".$login_id;
    $pname= preg_replace('/[ ,]+/', '_', trim($mt));
    $name['name']=$pname;
    $this->admin_model->update_function('loginid', $login_id, 'user_login',$name);

    // below for user profile pic 

    if($_FILES['file']['name'])
    {  
        $name_generator=$this->name_generator($_FILES['file']['name'],$login_id);
        $i_ext = explode('.', $_FILES['file']['name']);
        $target_path = $name_generator.'.'.end($i_ext);;
        $size = getimagesize($_FILES['file']['tmp_name']);

        if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/user/profile_pic/' . $target_path))
        {
            $data_img['profile_pic'] = $target_path;
        }

        $this->admin_model->update_function('loginid', $login_id, 'user_login',$data_img);

        if ($size[0] == 1200 || $size[1] == 500) 
        {

        } 
        else {
        $imageWidth = 1200; //Contains the Width of the Image

        $imageHeight = 500;

        $this->resize($imageWidth, $imageHeight, "uploads/user/profile_pic/" . $target_path, "uploads/user/profile_pic/" . $target_path);
    }

    $this->session->set_userdata('p_image',$data_img['profile_pic']);

}


redirect('user#profile','refresh');
}

public function name_generator($name='',$id)
{
    $data_link['title_link'] = preg_replace('/[ ,]+/', '_', trim($name));
    $data_link['title_link']=$data_link['title_link'].'_'.$id;
    $data_link['title_link'] = str_replace("'", '', $data_link['title_link']);
    $data_link['title_link'] = str_replace("@", '', $data_link['title_link']);
    $data_link['title_link'] = str_replace("/", '', $data_link['title_link']);
    $data_link['title_link'] = str_replace(".", '_', $data_link['title_link']);
    return $data_link['title_link'];
}



public function update_password($login_id)
{


  $old_pass=$this->encryptIt($this->input->post('old_password'));

          //echo $old_pass;die();

  $old_password=$this->admin_model->select_with_where('*','loginid='.$login_id,'user_login');

          // echo '<pre>'; print_r($old_password);die();

  if($old_password[0]['password']==$old_pass){

    $data['password']=$this->encryptIt($this->input->post('password'));

    $confirm_pass=$this->encryptIt($this->input->post('confirm_pass'));

    if ($data['password']==$confirm_pass) {

        $this->admin_model->update_function('loginid', $login_id, 'user_login',$data);

        $this->session->set_userdata('password_change_msg','Update successfully done');

        // "<pre>";print_r($this->session->userdata);die();

        redirect('user#profile#c_pass','refresh');


    }
    else{

        $this->session->set_userdata('password_change_msg','Wrong Confirm Password.');

         // "<pre>";print_r($this->session->userdata);die();

        redirect('user#profile#c_pass','refresh');

    }
}

else{

    $this->session->set_userdata('password_change_msg','Wrong Old Password.');

     // "<pre>";print_r($this->session->userdata);die();

    redirect('user#profile#c_pass','refresh');



}

}


public function get_district()
{
    $division_id=$this->input->post('division_id');
    $get_district=$this->admin_model->select_with_where('*', 'division_id='.$division_id, 'districts');

    echo "<option></option>";
    foreach ($get_district as $key => $value) {
        echo '<option value="'.$value["id"].'">'.$value["name"].' '.$value["bn_name"].'</option>';
    }
}

public function get_area()
{
    $district_id=$this->input->post('district_id');
    $get_area=$this->admin_model->select_with_where('*', 'district_id='.$district_id, 'area');

    echo "<option></option>";
    foreach ($get_area as $key => $value) {
        echo '<option value="'.$value["area_id"].'">'.$value["area_title"].'</option>';
    }
}



public function add_favourite()
{
        //$login_id=$this->session->userdata('login_id');

    $login_id=$this->input->post('login_id');

    $offer_id=$this->input->post('offer_id');

        //$data['fav_product']=$offer_id;

    $pre_offer_id=$this->admin_model->select_where_left_join('*','user_login','user_details','user_details.user_id=user_login.loginid','user_details.user_id='.$login_id);

        //echo '<pre>';print_r($pre_offer_id);die();
    $pre_favorite=$pre_offer_id[0]['fav_product'];
    $fav_offer_id=$this->admin_model->fav_products($offer_id);
        //$unf_offer_id=$fav_offer_id[0]['fa'];
       // echo '<pre>';print_r($fav_offer_id);die();


    if($pre_offer_id[0]['fav_product']=='' || $pre_offer_id[0]['fav_product']==0) 
    {

        $data['fav_product']=$offer_id;

    }

    else
    {
        if (count($fav_offer_id)>0) 
        {
            $data['fav_product']='';
            $pre_favorite_array=explode(',', $pre_favorite);
                //echo '<pre>';print_r($pre_favorite_array);die();
            foreach ($pre_favorite_array as $value) 
            {
                if($offer_id!=$value)
                {
                    $data['fav_product'].=$value.','; 
                }
            }
            $data['fav_product']=substr($data['fav_product'],0,-1); 
        }
        else
        {
         $data['fav_product']=$pre_favorite.','.$offer_id; 
     }



 }

 $this->admin_model->update_function('user_id', $login_id, 'user_details',$data); 

}

public function search_offer_by_date($value='')
{
   

    $date_from=$this->input->post('date_from');

    $date_to=$this->input->post('date_to');

    $login_id=$this->session->userdata('login_id');

     $data['all_order']=$this->admin_model->select_with_where('*','user_id="'.$login_id.'"','user_order');

    $data['orders']=$this->user_model->select_join_four_table_left_join('*,o.created_at','user_order o','order_transaction ot','main_offer mo','offer_type t','o.id=ot.order_id','o.offer_id=mo.offer_id','mo.offer_title_type=t.offer_id','o.user_id="'.$login_id.'"');

        // Row per page
    $rowperpage = 10;

    $row=0;

    // All records count
    $allcount = count($data['orders']);


    // Pagination Configuration
    $config['base_url'] = '';
    $config['use_page_numbers'] = TRUE;
    $config['total_rows'] = $allcount;
    $config['per_page'] = $rowperpage;
    $config['attributes'] = array('class' => 'page_list_num');

    // Initialize
    $this->pagination->initialize($config);

    // Initialize $data Array
    $data['pagination'] = $this->pagination->create_links();

    $data['order_info']=$this->user_model->select_join_four_table_left_join('*,o.created_at','user_order o','order_transaction ot','main_offer mo','offer_type t','o.id=ot.order_id','o.offer_id=mo.offer_id','mo.offer_title_type=t.offer_id','o.user_id="'.$login_id.'" AND date(o.created_at) between "'.$date_from.'" and "'.$date_to.'"');

    $this->load->view('include/myorders',$data);


}

public function pagination_order($rowno=0)
{

    $login_id=$this->session->userdata('login_id');
    $data['order_info']=$this->user_model->select_join_four_table('*,o.created_at','user_order o','order_transaction ot','main_offer mo','offer_type t','o.id=ot.order_id','o.offer_id=mo.offer_id','mo.offer_title_type=t.offer_id','o.user_id="'.$login_id.'"');

        // Row per page
    $rowperpage = 5;

    // Row position
    if($rowno != 0){
      $rowno = ($rowno-1) * $rowperpage;
  }

    // All records count
  $allcount = count($data['order_info']);

    // Get records
  $users_record = $this->user_model->select_join_four_table('*,o.created_at','user_order o','order_transaction ot','main_offer mo','offer_type t','o.id=ot.order_id','o.offer_id=mo.offer_id','mo.offer_title_type=t.offer_id','o.user_id="'.$login_id.'"','"'.$rowperpage.'"','"'.$rowno.'"');

    // Pagination Configuration
  // $config['base_url'] = base_url().'index.php/user/pagination_order';
  $config['use_page_numbers'] = TRUE;
  $config['total_rows'] = $allcount;
  $config['per_page'] = $rowperpage;

    // Initialize
  $this->pagination->initialize($config);

    // Initialize $data Array
  $data['pagination'] = $this->pagination->create_links();
  $data['order_info'] = $users_record;
  $data['row'] = $rowno;

  // echo json_encode($data);
}

public function delete_favourite_offer()
{

    $login_id=$this->input->post('login_id');

    $offer_id=$this->input->post('offer_id');

        // echo '<pre>';print_r($offer_id);die();

    $favourite_offer=$this->admin_model->user_favourite_offer($login_id,$offer_id);

        //echo '<pre>';print_r($favourite_offer_id);die();

    $val['fav_offer']=$this->removeFromString($favourite_offer[0]['fav_offer'],$offer_id);

    $this->admin_model->update_function('loginid', $login_id, 'user_login',$val);

    $this->session->set_userdata('sub_tab' , 'sub_offer');

    $this->load_tab_data('my_subscription'); 

}


public function delete_favourite_cat()
{

    $cat_id=$this->input->post('cat_id');

        //echo '<pre>';print_r($cat_id);die();

    $user_id=$this->session->userdata('login_id');

    $user_info=$this->admin_model->select_with_where('*','loginid="'.$user_id.'"','user_login');

    $val['subscribed_category']=$this->removeFromString($user_info[0]['subscribed_category'],$cat_id);

    $this->admin_model->update_function('loginid', $user_id, 'user_login',$val);

    $this->session->set_userdata('sub_tab' , 'sub_cat');

    $this->load_tab_data('my_subscription'); 

}

function removeFromString($str, $item) {
    $parts = explode(',', $str);

        // "<pre>";print_r($parts);die();

    if(count($parts)==1)
    {
        return null;
    }

    while(($i = array_search($item, $parts)) !== false) {
        unset($parts[$i]);
    }

    return implode(',', $parts);
}


public function delete_favourite_store()
{

    $login_id=$this->input->post('login_id');

    $user_id=$this->session->userdata('login_id');

    $user_info=$this->admin_model->select_with_where('*','loginid="'.$user_id.'"','user_login');

    $val['subscribed_store']=$this->removeFromString($user_info[0]['subscribed_store'],$login_id);

    $this->admin_model->update_function('loginid', $user_id, 'user_login',$val);  

    $this->session->set_userdata('sub_tab' , 'sub_store');

    $this->load_tab_data('my_subscription'); 

}

public function close_notification($value='')
{


  // $user_id=$this->session->userdata('login_id');

    $notification_id=$this->input->post('notification_id');

    $val['status']=2;

    $this->admin_model->update_function('id', $notification_id, 'notifications',$val);

    $this->load_tab_data('notification');   
}


public function close_notification_all($value='')
{


  $user_id=$this->session->userdata('login_id');

  $val['view_status']=2;

  $this->admin_model->update_function('user_id', $user_id, 'notifications',$val);

  $this->load_tab_data('notification');   
}



public function get_code_modal()
{
    $offer_id=$this->input->post('offer_id');
    $data['offer_details']=$this->offer_model->get_offer_details('mf.offer_id='.$offer_id);
        //echo "<pre>";print_r($data['get_offer_details']);
    $data['store_info']=$this->home_model->select_with_where('*','loginid='.$data['offer_details'][0]['offer_from'],'user_login');
    $this->load->view('offer/include/coupon_modal', $data);
}

public function get_sms_modal()
{
    $data['offer_id']=$this->input->post('offer_id');
    $data['offer_details']=$this->offer_model->get_offer_details('mf.offer_id='.$data['offer_id']);
        //echo "<pre>";print_r($data['get_offer_details']);
    $data['store_info']=$this->home_model->select_with_where('*','loginid='.$data['offer_details'][0]['offer_from'],'user_login');
    $this->load->view('offer/include/sms_modal', $data);
}




public function send_sms()
{
    $this->load->library('sendsms_library');

    $number=$this->input->post('number');
    $offer_id=$this->input->post('offer_id');
    $offer_details=$this->offer_model->get_offer_details('mf.offer_id='.$offer_id);

    $company_name="Deshioffer";
    $msg_to='880'.$number;
    $msg_des=$offer_details[0]['offer_title'].' '.$offer_details[0]['sms'];

    
    $sms_mobile_no=$this->home_model->sms_mobile_number($offer_id,$msg_to);

    if (count($sms_mobile_no)>0) 
    {

        echo 1;
        
    }
    else
    {
        $report=$this->sendsms_library->send_single_sms($company_name,$msg_to,$msg_des);

        if($offer_details[0]['mobile_number']!='')
        {
            $data_sms['mobile_number']=$offer_details[0]['mobile_number'].','.$msg_to;
        }

        else
        {
            $data_sms['mobile_number']=$msg_to;
        }
        $this->home_model->update_function('offer_id',$offer_id,'offer_sms',$data_sms);

        if($report=='err')
        {
            echo 0;
        }
        else
        {
            echo 2;
        }

    }

}

public function get_qr_code_modal()
{
    $data['offer_id']=$this->input->post('offer_id');
    $data['offer_details']=$this->offer_model->get_offer_details('mf.offer_id='.$data['offer_id']);
        //echo "<pre>";print_r($data['get_offer_details']);
    $data['store_info']=$this->home_model->select_with_where('*','loginid='.$data['offer_details'][0]['offer_from'],'user_login');


    $this->load->library('qr_code/ciqrcode');

    $params['data'] = 'Offer Title:'.$data['offer_details'][0]['offer_title'].' '.'Voucher Code: '.$data['offer_details'][0]['voucher_code'].' '.'Discount:'.$data['offer_details'][0]['discount'].' '.'Expire Date:'.$data['offer_details'][0]['edate'];

    $params['level'] = 'H';
    $params['size'] = 10;
    $params['savename'] = FCPATH.'uploads/vouchar_code/'.$data['offer_details'][0]['offer_title'].'_qr_code_image'.'.png';

        //$data['qr_image']=$params['savename'];

    $data['qr_image']= base_url().'uploads/vouchar_code/'.$data['offer_details'][0]['offer_title'].'_qr_code_image'.'.png';
    $this->ciqrcode->generate($params);


    $this->load->view('offer/include/qr_code_modal', $data);
}

public function save_image()
{
    $data['offer_id']=$this->input->post('offer_id');
        //$btn_name=$this->input->post('name');


    $data['offer_details']=$this->offer_model->get_offer_details('mf.offer_id='.$data['offer_id']);

    $this->load->library('qr_code/ciqrcode');

    $params['data'] = 'Offer Title:'.$data['offer_details'][0]['offer_title'].' '.'Voucher Code: '.$data['offer_details'][0]['voucher_code'].' '.'Discount:'.$data['offer_details'][0]['discount'].' '.'Expire Date:'.$data['offer_details'][0]['edate'];

    $params['level'] = 'H';
    $params['size'] = 10;
    $params['savename'] = FCPATH.'uploads/vouchar_code/'.$data['offer_details'][0]['offer_title'].'_qr_code_image'.'.png';


    $this->ciqrcode->generate($params);

        // if ($btn_name=='download') 
        // {
        //  //file_get_contents($params['savename']);
        //  echo base_url().'uploads/vouchar_code/'.$data['offer_details'][0]['offer_title'].'_qr_code_image'.'.png';
        // }


}



public function orders()
{
       // Common Date Start

    $login_id=   $this->session->userdata('login_id');
    $data['user_page_title']='My Orders';
    $data['active']='orders';
    $data['header']=$this->home_model->select_with_where('*','id=1','company');
    $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
    $data['category']=$this->home_model->select_all_acending('category','name');
    $data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
        // Common Data End
        //Page Data starts
    $login_id=   $this->session->userdata('login_id');
    $data['orders']=$this->user_model->select_group_table_data('customer_order','user_id='.$login_id,'order_id');
        //Page Data End
    $this->load->view('orders',$data);
}

public function view_order_modal()
{
    $data['order_id']=$this->input->post('order_id');
    $data['order_details']=$this->home_model->select_where_join('*','customer_order','product','product.p_id=customer_order.product_id','order_id="'.$data['order_id'].'"');


    $this->load->view('include/order_details', $data);

}


public function transaction()
{
        // Common Date Start

    $login_id=   $this->session->userdata('login_id');
    $data['user_page_title']='My Transaction';
    $data['active']='transaction';
    $data['header']=$this->home_model->select_with_where('*','id=1','company');
    $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
    $data['category']=$this->home_model->select_all_acending('category','name');
    $data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
        // Common Data End
        //Page Data starts
    $login_id=   $this->session->userdata('login_id');
    $data['transaction']=$this->user_model->select_transaction_table_data('transaction','transaction.user_id='.$login_id);
        //Page Data End

        //echo "<pre>";print_r($data['transaction']);die();
    $this->load->view('transaction',$data);
}

public function change_password()
{
        // Common Date Start

    $login_id=$this->session->userdata('login_id');
    $data['active']='change_password';
    $data['user_page_title']='Password Change';
    $data['header']=$this->home_model->select_with_where('*','id=1','company');
    $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
    $data['category']=$this->home_model->select_all_acending('category','position');
    $data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
        // Common Data End


    $this->load->view('change_password',$data);
}

public function change_password_post()
{
    $login_id=$this->session->userdata('login_id');
    $old_pass=$this->encryptIt($this->input->post('old_password'));

    $get_old_pass=$this->home_model->select_with_where('*','id='.$login_id,'login');

        // $match_old_pass=$this->home_model->select_with_where('*','id='.$login_id.' AND password='.$old_pass,'customer_login');

    if($get_old_pass[0]['password']==$old_pass)
    {
        $data['password']=$this->encryptIt($this->input->post('password'));
        $this->home_model->update_function('id',$login_id,'login',$data);
        $this->session->set_userdata('log_scc', 'Password updated successfully');
        redirect('user/change_password','refresh');
    }

    else
    {
        $this->session->set_userdata('log_err', 'Incorrect Old Password');
        redirect('user/change_password','refresh');
    }

}


function encryptIt($string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
    $secret_iv = 'This is my secret iv';
        // hash
    $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    $output = base64_encode($output);

    return $output;
}

public function resize($height, $width, $source, $destination)
{
    $this->load->library('image_lib');
    $config['image_library']        = 'gd2';
    $config['source_image']         = $source;
    $config['overwrite']            = TRUE;
    $image_config['quality']        = "100%";
    $image_config['maintain_ratio'] = FALSE;
    $config['height']               = $height;
    $config['width']                = $width;
        $config['new_image']            = $destination; //you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
    
    public function resize2($height, $width)
    {
        $this->load->library('image_lib');
        $config['image_library']        = 'gd2';
        $config['source_image']         = "uploads/watermark.png";
        $config['overwrite']            = TRUE;
        $image_config['quality']        = "100%";
        $image_config['maintain_ratio'] = FALSE;
        $config['height']               = $height;
        $config['width']                = $width;
        $config['new_image']            = "uploads/product/"; //you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }



}
