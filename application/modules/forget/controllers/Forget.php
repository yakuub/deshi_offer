<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forget extends MX_Controller {

	public function forget_password() 
	{

		$company_info=$this->head_data();
		$data['company_info']=$company_info;

		$username=$this->input->post('username');
		$data_login['username']=$username;
		$data_login['email']=$this->input->post('email');
		$contact='880'.$this->input->post('contact');
		$data_login['contact']=$contact;

		$this->load->helper('string');

      //$system_password=random_string('alnum',6);

		$activation_key=random_string('alnum', 50);


		$password=$this->input->post('password');

      //$data_login['password']=$this->encryptIt($system_password);

		$data_login['password']=$this->encryptIt($this->input->post('password'));

		$c_password=$this->encryptIt($this->input->post('c_password'));


		$data_login['usertype']=3;
		$data_login['verify_status']=0;
		$data_login['delete_status']=0;
		$data_login['add_date']=date('Y-m-d H:i:s');


		$login_id=$this->registration_model->insert_ret('user_login',$data_login);

      //below for profile name update 

		$mt=$data_login['username']."_".$login_id;
		$pname= preg_replace('/[ ,]+/', '_', trim($mt));
		$pname= str_replace('/', '_', $pname);
		$pname= str_replace('.', '_', $pname);

		$activation_key=$activation_key.$login_id;

		$name['name']=$pname;
		$name['activation_key']=$activation_key;


		$this->admin_model->update_function('loginid', $login_id, 'user_login',$name);

		$data_user['user_id']=$login_id;

		$this->registration_model->insert_ret('user_details',$data_user);


		$msg_from= 'support@deshioffer.com';
		$c_name='Deshioffer-Team';
		$name=$data_login['username'];


		$msg_to=$data_login['email'];
		$this->send_email_function($msg_from,$name,$msg_to,$c_name,$password,$activation_key,$login_id);

		echo 1;

		
	}

	public function update_password()
	{
		$this->load->model('home/home_model');
		$data['company_info']=$this->home_model->select_with_where('*', 'id=1', 'company');
		$data['message_error_password']=NULL;
		if($this->input->post('pas_wrd'))
		{
			$this->form_validation->set_rules('pas_wrd', 'Password', 'required|matches[con_pas_wrd]');
			$this->form_validation->set_rules('con_pas_wrd', 'Password Confirmation', 'required');


			if($this->form_validation->run() == false)
			{
				$data['message_error_password'] = validation_errors();
				$this->load->view('view_forget_password',$data);
			}

			else
			{
				$tmp_password= $this->session->userdata('tmp_password');
				$password=$this->encryptIt($this->input->post('pas_wrd'));
				$this->load->model('forget_model');
				$this->forget_model->update_password($tmp_password,$password);
				$this->forget_model->update_password_reset($tmp_password,$password);
				$this->session->unset_userdata('tmp_password');
				$this->session->unset_userdata('email');

				$this->session->set_userdata('log_scc','Password Update Successfully.Now you can log in through your new password');
				redirect('login','refresh');
			}
		}
	}
	public function getlink($tmp_pass)
	{
		$this->load->model('home/home_model');
		$area_retreive_id=$this->session->userdata('area_retreive_id');
		$tmp_password= $this->session->userdata('tmp_password');
		
		$data['company_info']=$this->home_model->select_with_where('*', 'login_id='.$area_retreive_id, 'company');
		$this->load->model('forget_model');
		$get_email=$this->forget_model->get_email($tmp_pass);
		if($get_email)
		{
			$data['message_error_password']=NULL;
			$this->load->view('view_forget_password',$data);
		}
		else
		{
			$this->session->set_userdata('log_err','Forget Password is expired. Please Try again');
			redirect('login','refresh');
		}
	}

	function encryptIt($q) {
		$cryptKey = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
		$qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
		return( $qEncoded );
	}
}