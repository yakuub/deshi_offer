<?php $this->load->view('front/headlink');?>
<body>
<?php $this->load->view('front/head_nav');?> 
<?php $this->load->view('front/mega_menu');?>

<div class="container"><!--All Catagory container start-->
    <div class="col-md-12 all_catagory"><!--All Catagory start-->
      <div class="col-md-12 all_catagory_title"><!--title start-->
        <h1>Popular Catagories</h1>
        <p>Browse popular catagories and get the best Deals & Offers from your nearest stores.</p>
      </div> <!--title start-->
      
      <div class="col-md-12 popular_cat">
        <div class="col-xs-12 heading">
        <div class="heading_icon">
          <img src="front_assets/images/resource/home/fea-cat.svg" alt="" class="img-responsive" id="cat_img">
        </div>
        <p class="fea_brnd">Featured Category</p>
      </div>
        <div class="row pop_category">
          <?php foreach ($feature_category as $key => $value) {?>
          <div class="col-md-2 col-sm-4 col-xs-6 cat_sec">
            <?php if($value['catagory_pic']==''){?>
              <img src="uploads/category_default.jpg" class="" alt="">
            <?php } else{ ?>
              <img src="uploads/category/<?=$value['catagory_pic'];?>" class="" alt="">
            <?php } ?>
            <div class="cat_name"><?=$value['catagory_title'];?></div>
            <div class="cat_overlay">
              <a href="category/offer/<?=$value['catagory_title'];?>">
                <span><?=$category_offer_amount[$key];?> OFFERS</span>
              </a>
            </div>
          </div>
        <?php } ?>
          
          <div class="clearfix"></div>
        </div>
      </div>

      <div class="col-md-12 f_w_ad"><!--full width banner-->
        <div class="col-sm-12">
          <a target="_blank" href="#"><img src="front_assets/images/promo_banner.png" alt="" class="img-responsive"></a>
        </div>
      </div><!--full width banner-->
      <div class="col-md-12 all_cat">
        <div class="col-xs-12 heading">
        <div class="heading_icon">
          <img src="front_assets/images/resource/home/popcat.svg" alt="" class="img-responsive" id="cat_img">
        </div>
        <p class="fea_brnd">All Categories </p>
      </div>
        <div class="row all_category_div">
          <?php foreach ($all_category as $key => $value) {?>
          <div class="col-md-2 col-sm-4 col-xs-6 cat_sec">
            <?php if($value['catagory_pic']==''){?>
              <img src="uploads/category_default.jpg" class="" alt="">
            <?php } else{ ?>
              <img src="uploads/category/<?=$value['catagory_pic'];?>" class="" alt="">
            <?php } ?>
            <div class="cat_name"><?=$value['catagory_title'];?></div>
            <div class="cat_overlay">
              <a href="category/offer/<?=$value['catagory_title'];?>">
                <span><?=$category_offer_amount_total[$key];?> OFFERS</span>
              </a>
            </div>
          </div>
          <?php } ?>
      
          <div class="clearfix"></div>
        </div>
      </div>
    </div><!--All Catagory end--> 

<div class="container">
  

</div>

  </div><!--All Catagory container end--> 

 <!--   <div class="container">
    <div class="col-md-12 mail_subscribe">
    <div class="col-md-6 subscribe_title"><img src="front_assets/images/subscribe.svg" alt="subscribe to Deshioffer">
      <p>Subscribe to email alerts for new Offers & Deals</p>
      <div class="clearfix"></div>
    </div>
    <form class="col-md-6 subscribe_input" action=""><input type="text" id="subscribe" placeholder="Enter your Email to subscribe"><input value="▶" type="submit"></form>
    <div class="clearfix"></div>
  </div><!--subscribe end
</div> -->
 
<!-- 
  <div class="container-fluid number_wrapper">
    <div class="container number">  
      <div class="col-sm-4 col-xs-4">
        <div class="col-sm-2 col-xs-12 deal_img_wrap">
          <div class="deal_img center-block">
            <img class="img-responsive" src="front_assets/images/offerbx.svg" alt="">
          </div>          
        </div>
        <div class="col-sm-10 col-xs-12 deal_img_wrap">
          <p class="count">2532</p>
          <p>Offers And Deals <span>For You</span></p>
        </div>        
      </div>
      <div class="col-sm-4 col-xs-4">
        <div class="col-sm-2 col-xs-12 deal_img_wrap">
          <div class="deal_img center-block">
            <img class="img-responsive" src="front_assets/images/merchantbx.svg" alt="">
          </div>
        </div>
        <div class="col-sm-8 col-xs-12 deal_img_wrap">
          <p class="count">5361</p>
          <p>Merchants Associated</p>
        </div>        
      </div>
      <div class="col-sm-4 col-xs-4">
        <div class="col-sm-2 col-xs-12 deal_img_wrap">
          <div class="deal_img center-block">
            <img class="img-responsive" src="front_assets/images/sofferbx.svg" alt="">
          </div>
        </div>
        <div class="col-sm-8 col-xs-12 deal_img_wrap">
          <p class="count">8952</p>
          <p>Exclusive offers <span>For You</span> </p>
        </div>        
      </div>
    </div><!--  NUMBER CLASS CLOSED HERE -->
  <!-- </div> NUMBER_WRAPPER CLASS CLOSED HERE
  <br> -->

<?php $this->load->view('front/footer');?>
<?php $this->load->view('front/footerlink');?>

  <script>
    $(".floating_nav").hide();
    $(window).on('resize load', function(){
      $(".cat_pg_ad1").css("min-height", $(".cat_pg_ad2").height()+"px");
    });
    $(".subscribe").click(function(){
      $(".subscribe_fix_div").fadeIn(200);
      $(".subscribe_fix_div").css({"display": "-webkit-box", "display": "-ms-flexbox", "display":"flex"});
      $(".subscribe_fix_div .form_close").fadeIn(200);
    });
    $(".subscribe_fix_div .form_close").click(function(){
      $(".subscribe_fix_div").fadeOut(200);
      $(".subscribe_fix_div .form_close").fadeOut(200);
    });
    function search_ctrl(){   
      var width = $(window).width();
      if(width < 767){
        $(".form_wrap").css("display","none");
        $(".search_btn").attr("value","Go");
      }
      else{
        $(".form_wrap").css("display","block");
        $(".search_btn").attr("value","Show Offers");
      }
    }
    $(".src_btn").stop().click(function(){
      if($(window).width() < 767){
        $(this).siblings(".form_wrap").slideToggle(300);
      }
      else{
        $(this).siblings(".form_wrap").slideDown(0);
      }
    });
    search_ctrl();
    $(window).on("resize",search_ctrl);
    $(".post_offer_btn, .pop_up_wrap").hover(function(){
      $(".pop_up_wrap").stop().fadeIn(200);
    },function(){
      $(".pop_up_wrap").fadeOut(200);
    });

  </script>

    
       <script>
    $(".form-fg-pass").css("display","none");
    $(".recover_pass").click(function () {
        $(".form-signin").css("display","none")
        $(".form-fg-pass").css("display","block")
        // $("#").show();   
    });

    $(".b2login").click(function () {
      $(".form-signin").css("display","block")
        $(".form-fg-pass").css("display","none")
        
        // $("#").show();   
    });
   </script>

</body>

</html>
