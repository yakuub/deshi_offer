<?php foreach ($cat_offer_list as $key => $value) {?>
   <div class="col-md-4 product_wrap_left">
      <div class="col-md-12 product_each_wrapper">
          <div class="col-md-12 product excl-product">
            <div class="product_head col-md-12">
              <div class="tag" style="background: <?=$value['offer_color_code'];?>"><?=$value['offer_type_tilte'];?></div>
              <div class="favourite glyphicon glyphicon-heart" onclick="add_favourite('<?=$value['main_off_id'];?>')" id="favourite_<?=$value['main_off_id'];?>">
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="col-md-6 product_img col-xs-6">
              <a href="offer/offer_details/<?=$value['profile_name'];?>"><img style="height: 110px" src="uploads/offer/<?=$value['offer_featured_file'];?>" class="img-responsive">
                </a>
              <?php if($value['discount']>0){?>
              <div class="percentage"><?=$value['discount'];?>%</div>
            <?php } ?>
            </div>
            <div class="col-md-6 product_description col-xs-6">
              <a target="_deshioffer" href="offer/offer_details/<?=$value['profile_name'];?>"><p class="product_title"><?=word_limiter($value['offer_title'],3);?>
              </p></a>
         
          <p class="offr_tag"><i class="fa fa-tags offr_fa"></i>
          <?php if($value['offer_store_type']==0){echo "Store Offer";}else{echo "Online Offer";}?></p> <!--<span class="offr_tag"><i class="fa fa-map-marker rv_icon"></i> Gulshan</span> -->
             <p  class="offr_usd"><i class="fa  fa-user-plus offr_fa"></i> <span class="offr_usd_c"><?=$value['total_seen'];?> </span> Used Already</p>
             <?php if($value['sale_price']!=''){?>
            <p class="offr_tag">Price: <?=$value['sale_price'];?> &#x9f3;</p>
            <?php } ?>
             <?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($value['edate']);
              $diff = $date2 - $date1;
              $day=round($diff / 86400);?>
              <?php if($day>0){?>
              <p class="expire">Expire in <?=$day;?> Days.</p>
            <?php } else { ?>
              <p class="expire">Expire in Today</p>
            <?php } ?>
            
              <div class="clearfix"></div>
            </div>
            <div class="amazon"><a href="store/offer/<?=$value['name'];?>"><img style="width: 95px;height: 26px;" src="uploads/user/<?=$value['profile_pic'];?>" alt=""></a></div>
             <?php if($value['ctype']==1){?>
            <a onclick="get_vouchar('<?=$value['main_off_id'];?>');" class="offer_btn" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } else if($value['ctype']==2){?>
            <a class="offer_btn"  onclick="get_code('<?=$value['main_off_id'];?>');" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>

          <?php } else if($value['ctype']==3){?>

            <a  class="offer_btn"  onclick="get_sms('<?=$value['main_off_id'];?>');" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>

          <?php } else if($value['ctype']==4){?>
              <a target="_blank" class="offer_btn" href="<?=$value['ref_link'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } else if($value['ctype']==5){?>
              <a target="_blank" class="offer_btn" href="<?=$value['utm_link'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } else{ ?>
              <a class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } ?>
          </div>
        </div>
      
   </div>
<?php } ?>