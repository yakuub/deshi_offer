<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends MX_Controller {


	function __construct()
	{
		$this->load->model('home/home_model');
		$this->load->model('offer/offer_model');

		date_default_timezone_set('Asia/Dhaka');
		if($this->session->userdata('language_select')=='bangla')
		{
			$this->lang->load('front', 'bangla');
		}
		else
		{
			$this->lang->load('front', 'english');
		} 
		

	}
	public function index()
	{
		$data['active']='home';
		$data['header']=$this->home_model->select_with_where('*','id=1','company');
		// $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
		$data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

		$cur_date=date('Y-m-d');

		$data['cat_brand']=array();
		$data['store_offer']=array();
		foreach ($data['category'] as $key => $value) 
		{
			$data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

			foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
			{
				$data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND mf.edate>=DATE("'.$cur_date.'") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
			}	
			
		}

		$data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','top_store=1 AND usertype=2 AND is_publish=1','28');
		$data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND is_publish=1','28');

		$data['page_category']=$this->home_model->select_with_where('*','front_page_show_st=1','catagory');
		
		$data['feature_category']=$this->home_model->select_with_where('*','featured_cat_st=1 AND menu_st=1','catagory');

		$data['category_offer_amount']=array();
		
		foreach ($data['page_category'] as $key => $value) 
		{
			$data['category_offer_amount'][$key]=count($this->home_model->select_with_where('*','status=1 AND catagories='.$value['catagory_id'],'main_offer'));
		}

		$data['all_category']=$this->home_model->select_with_where('*','menu_st=1','catagory');

		$data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

		$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');

		$data['category_offer_amount_total']=array();
		
		foreach ($data['all_category'] as $key => $value) 
		{
			$data['category_offer_amount_total'][$key]=count($this->home_model->select_with_where('*','status=1 AND catagories='.$value['catagory_id'],'main_offer'));
		}

		$data['user_details']=$this->home_model->select_with_where('*','loginid="'.$this->session->userdata('login_id').'"','user_login');

		$this->load->view('index',$data);

	}


	public function offer($cat_name='')
	{
		$data['active']='home';
		$data['header']=$this->home_model->select_with_where('*','id=1','company');
		// $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
		$data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

		$cur_date=date('Y-m-d');

		$data['cat_brand']=array();
		$data['store_offer']=array();
		foreach ($data['category'] as $key => $value) 
		{
			$data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

			foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
			{
				$data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND mf.edate>=DATE("'.$cur_date.'") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
			}	
			
		}

		$data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','top_store=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');
		$data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND verify_status=1 AND is_publish=1','28');
		$data['offer_type']=$this->home_model->select_with_where('*','offer_st=1','offer_type');

		$data['offer_category']=$this->home_model->select_with_where('*','menu_st=1','catagory');

		$data['division']=$this->home_model->select_all_acending('divisions','position');

		$data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

		$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');


		$cat_id=$this->home_model->select_with_where('*','REPLACE(catagory_title," ","%20") LIKE "'.$cat_name.'%"','catagory');
			//$data['area_id']=$this->home_model->select_with_where('id','REPLACE(area_title, " ", "%20") LIKE "'.$name.'%"','area');

		$cat_id=$cat_id[0]['catagory_id'];
		$data['cat_id']=$cat_id;

		$data['cat_name']=str_replace("%20"," ",$cat_name);
		$data['offer_list']=$this->offer_model->get_offer_ci('mf.catagories='.$cat_id.' AND mf.status=1',8,'mf.updated_at','DESC');

		$data['total_offer']=count($this->home_model->select_with_where('*','catagories='.$cat_id.' AND status=1','main_offer'));

		$data['cat_offer_list']=array();
		//echo count($data['total_offer']);
		//echo $data['offer_list'][7]['main_off_id'];
		if($data['total_offer']>8)
		{
			
			$data['cat_offer_list']=$this->offer_model->get_offer_ci('mf.catagories='.$cat_id.' AND mf.status=1 AND mf.offer_id<'.$data['offer_list'][7]['main_off_id'],18,'mf.updated_at','DESC');
		}
		//echo "<pre>";print_r($data['cat_offer_list']);die();

		$data['user_details']=$this->home_model->select_with_where('*','loginid="'.$this->session->userdata('login_id').'"','user_login');

		// echo "<pre>";print_r($data['user_details']);die();

		$this->session->set_userdata('referred_from', current_url());
		$this->load->view('offer',$data);
	}

}
