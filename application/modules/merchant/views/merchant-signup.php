<title>Deshi Offer | দেশী অফার</title>
<?php $this->load->view('merchant/header_link');?>
<body>
<?php $this->load->view('merchant/header');?>    
<?php $this->load->view('merchant/mega_menu');?>
<style>
  .floating_nav{
    display: none;
  }
</style>
    <div class="top-section">
    	<div class="container">
    		 <div class="col-md-9 top-section-ls text-center">
    		 	  <div class="col-md-offset-1 col-md-9 col-xs-12">
    		 	  	<h2><span style="color:#29ADF9;"> PROMOTE </span> YOUR BRAND</h2>
    		 	   <h5>WITH BANGLADESHI'S FIRST AND #1 OFFER DISCOUNT PLATFORM</h5>
    		 	  <div class="top-section-boxes">
    		 	  	<div class="row">
    		 	  		<div class="col-md-4">
    		 	  	  <div class="top-section-box">
    		 	  	  	 <p>JOIN</p>
						 <p>DESHIOFFER</p>
    		 	  	  </div>
    		 	  </div>
    		 	  <div class="col-md-4">
    		 	  	 <div class="top-section-box br_lr_w">
    		 	  	 	 <p>ADD</p>
						 <p>COUPON</p>
    		 	  	 </div>
    		 	  </div>
    		 	  <div class="col-md-4">
    		 	  	  <div class="top-section-box">
    		 	  	  	 <p>INCREASE</p>
						 <p>SALES</p>
    		 	  	  </div>
    		 	  </div>
    		 	  	</div>
    		 	  </div>
    				<a class="btn btn-primary top-section-btn" href=#m-panel-reg>REGISTER</a>
    		 	  </div>
    		 </div>
    		 <div class="col-md-3 col-xs-12 form-bg">
                    <form action="" class="login-form-merchant" style="height: 285px">
                        <h4 class="text-center" style="padding: 20px; color: #91BEF8">LOG INTO PARTNER</h4>
                        <div class="input-group col-md-10" style="margin: 12px auto">
                          <span class="input-group-addon icon_mr_log"><i class="fa fa-user fa-fw" aria-hidden="true" style="color: grey;"></i></span>
                          <input class="form-control input_mr_log" type="email" placeholder="Enter Email id">
                        </div>
 
                        <div class="input-group col-md-10" style="margin: 12px auto">
                          <span class="input-group-addon icon_mr_log"><i class="fa fa-lock fa-fw" aria-hidden="true" style="color: grey;"></i></span>
                          <input class="form-control input_mr_log" type="password" placeholder="Enter password">
                        </div>

                       <div class="text-center">
                            <input class="m_f_btn" type="submit" value="LOG IN" style="border-radius: 20px; background-color: black; border: none; color: white; margin: 15px 0;padding: 10px 30px;">
                            <p><a style="text-decoration: none; color: #E4D50B" href="javascript:void(0)" class="lost_pass_merchant">Forgot password?</a></p>
                       </div>
                        
                    </form>

                     <form action="" class="f_pass_login" style="height: 285px">
                        <h4 class="text-center" style="padding: 20px; color: #91BEF8; font-size: 18px ">RESET PARTNER PASSWORD</h4>
                        <div class="input-group col-md-10" style="margin: 12px auto">
                          <span class="input-group-addon icon_mr_log"><i class="fa fa-user fa-fw" aria-hidden="true" style="color: grey;"></i></span>
                          <input class="form-control input_mr_log" type="email" placeholder="Enter Email id">
                        </div>

                

                       <div class="text-center">
                            <input class="m_f_btn" type="submit" value="RESET PASSWORD" style="border-radius: 20px; background-color: black; border: none; color: white; margin: 15px 0;padding: 10px 30px;">
                            <p><a style="text-decoration: none; color: #E4D50B" href="javascript:void(0)" class="back_login">Back to login</a></p>
                       </div>
                        
                    </form>

                </div>
    	</div>
    </div>   
</div>

<div class="middle-section">
    	<div class="container">
    		<h2 class="text-center mp_hd_txt">WHY JOIN DESHI OFFER</h2>
				<div class="col-md-4">
					<div class="mid-section-box">
						<span><i class="fa fa-plus-circle"></i></span>
									  <p>BRAND PAGE</p>
						<span>Lorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet.</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="mid-section-box">
						<span><i class="fa fa-star"></i></span>
									  <p>BRAND PAGE</p>
						<span>Lorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet.</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="mid-section-box">
						<span><i class="fa fa-diamond"></i></span>
									  <p>BRAND PAGE</p>
						<span>Lorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet.</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="mid-section-box">
						<span><i class="fa fa-cog"></i></span>
									  <p>BRAND PAGE</p>
						<span>Lorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet.</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="mid-section-box">
						<span><i class="fa fa-heart"></i></span>
									  <p>BRAND PAGE</p>
						<span>Lorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet.</span>
					</div>
				</div>
				<div class="col-md-4">
					<div class="mid-section-box">
						<span><i class="fa fa-plus-circle"></i></span>
									  <p>BRAND PAGE</p>
						<span>Lorem ipsum dolor sit amet, consectetur. Lorem ipsum dolor sit amet.</span>
					</div>
				</div>

    	</div>



    </div>


  <div class="faq-section">
  	 <div class="container">
  	 	<h2 class="text-center mp_hd_txt">FAQ's</h2>
  	 	 <div class="col-md-12">
  	 	 	<div class="faq-box container">
  	 	   <div class="faq-qs">
  	 	   	 <p>Lorem ipsum dolor sit?</p>
  	 	   	 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit nihil necessitatibus consequatur vitae dignissimos, inventore. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate sit necessitatibus labore.</p>
  	 	   </div>
  	 	   <div class="faq-qs">
  	 	   	 <p>Lorem ipsum dolor sit?</p>
  	 	   	 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit nihil necessitatibus consequatur vitae dignissimos, inventore. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae non sit ea.</p>
  	 	   </div>
  	 	   <div class="faq-qs">
  	 	   	 <p>Lorem ipsum dolor sit?</p>
  	 	   	 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit nihil necessitatibus consequatur vitae dignissimos, inventore. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis et fuga minus!</p>
  	 	   </div>
  	 	   <div class="faq-qs">
  	 	   	 <p>Lorem ipsum dolor sit?</p>
  	 	   	 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit nihil necessitatibus consequatur vitae dignissimos, inventore. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore ipsam sequi perferendis!</p>
  	 	   </div>
  	 	   <div class="faq-qs">
  	 	   	 <p>Lorem ipsum dolor sit?</p>
  	 	   	 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit nihil necessitatibus consequatur vitae dignissimos, inventore. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi optio, recusandae cumque.</p>
  	 	   </div>
  	 	 </div>
  	 	 </div>
  	 </div>
  </div>

  <div class="register-section" id="m-panel-reg">
  	 <div class="container">
  	 	<h2 class="text-center mp_hd_txt">REGISTER</h2>
  	 	 <div class="col-xs-12">
  	 	 	
  	 	 	<form action="" method="POST" class="merchant-reg-form form-horizontal">

  	 	<div class="form-group">
      <label class="control-label col-sm-4" for="email">Company Name:</label>
      <div class="col-sm-5">
        <input type="email" class="form-control" id="email" placeholder="Enter Brand/Store" name="email">
      </div>
    </div>
  	 	 		
  	 		<div class="form-group">
      <label class="control-label col-sm-4" for="email">Contact Email:</label>
      <div class="col-sm-5">
        <input type="email" class="form-control" id="email" placeholder="" name="email">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-4" for="email">Contact Number:</label>
      <div class="col-sm-5">
        <input type="email" class="form-control" id="email" placeholder="" name="email">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-4" for="pwd">Password:</label>
      <div class="col-sm-5">          
        <input type="password" class="form-control" id="pwd" placeholder="" name="pwd">
      </div>
    </div>


    <div class="form-group">
      <label class="control-label col-sm-4" for="pwd">Confirm Password:</label>
      <div class="col-sm-5">          
        <input type="password" class="form-control" id="pwd" placeholder="" name="pwd">
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-6 col-sm-10">
        <button type="submit" class="btn btn-primary reg-form-btn">JOIN</button>
      </div>
    </div>
    
  	 		
  	 		
  	 	</form>
  	 	 	
  	 	 </div>
  	 </div>
  </div>


  <div class="query-section text-center">
  	 <div class="container">
  	 	<h3>For any query regarding partner issues feel free to contact our partner support:</h3>
  	 	  <div class="query-bottom">
  	 	  	<span><i class="fa fa-phone"></i>1232132132</span> 
  	 	  	 <span style="color: #ccc">|</span>
  	 	    <span><i class="fa fa-envelope"></i>partner@deshioffer.com</span>
  	 	  </div>
  	 </div>
  </div>



<?php $this->load->view('merchant/footer');?>

<?php $this->load->view('merchant/footer_link');?>
    
	<script>
		function search_ctrl(){		
			var width = $(window).width();
			if(width < 767){
				$(".form_wrap").css("display","none");
				$(".search_btn").attr("value","Go");
			}
			else{
				$(".form_wrap").css("display","block");
				$(".search_btn").attr("value","Show Offers");
			}
		}
		$(".src_btn").stop().click(function(){
			if($(window).width() < 767){
				$(this).siblings(".form_wrap").slideToggle(300);
			}
			else{
				$(this).siblings(".form_wrap").slideDown(0);
			}
		});
		search_ctrl();
		$(window).on("resize",search_ctrl);
		$(".post_offer_btn, .pop_up_wrap").hover(function(){
			$(".pop_up_wrap").stop().fadeIn(200);
		},function(){
			$(".pop_up_wrap").fadeOut(200);
		});
	</script>


   <script>
   	$(".f_pass_login").css("display","none");
   	$(".lost_pass_merchant").click(function () {
        $(".login-form-merchant").css("display","none")
        $(".f_pass_login").css("display","block")
        // $("#").show();   
    });

    $(".back_login").click(function () {
    	$(".login-form-merchant").css("display","block")
        $(".f_pass_login").css("display","none")
        
        // $("#").show();   
    });
   </script>

   <script>
      $(document).ready(function(){
  // Add smooth scrolling to all links
  $(".top-section-btn").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 1000, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
});
    </script>
               <script>
    $(".form-fg-pass").css("display","none");
    $(".recover_pass").click(function () {
        $(".form-signin").css("display","none")
        $(".form-fg-pass").css("display","block")
        // $("#").show();   
    });

    $(".b2login").click(function () {
      $(".form-signin").css("display","block")
        $(".form-fg-pass").css("display","none")
        
        // $("#").show();   
    });
   </script>

<script>
    $(document).ready(function() {
        $(".floating_nav").attr("display","none");
    }
    

</script>
   
</body>
</html>