<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Merchant extends MX_Controller 
{

    function __construct()
    {
        date_default_timezone_set('Asia/Dhaka');

        $this->load->model('admin/admin_model');

        if($this->session->userdata('language_select')=='bangla')
        {
            $this->lang->load('admin', 'bangla');
        }
        else
        {
            $this->lang->load('admin', 'english');
        }

        // $login_id=   $this->session->userdata('login_id');
        // $reg_id =    $this->session->userdata('reg_id');
        // $name   =    $this->session->userdata('name');
        // $type   =    $this->session->userdata('type');
        // $district_id=$this->session->userdata('district_id');

        // if($login_id=='' || $type!=2)
        // {
        //     $this->session->unset_userdata('login_id');
        //     $this->session->unset_userdata('reg_id');
        //     $this->session->unset_userdata('name');
        //     $this->session->unset_userdata('district_id');
        //     $this->session->unset_userdata('type');
        //     $this->session->set_userdata('log_err','Enter Email and Password First');
        //     redirect('login','refresh');
        // }
    }
    public function index()
    {
        
       $this->load->view('merchant-signup');
       
    }


    public function add_market()
    {

        $reg_id=$this->session->userdata('reg_id');
       
        $id=$this->admin_model->select_with_where('id', 'reg_id='.$reg_id, 'market');
        
        if(count($id)>0)
        {
            redirect('market_admin/edit_market','refresh');
        }
        else
        {
            $data['active']='market';
            $data['left_nav_active']='add';
            $data['page_title']=$this->lang->line('market_label').' '.$this->lang->line('btn_add_label');
     
            $data['breadcrumbs']= '<li><span class="active">'.$data['page_title'].'</span></li>';

            $data['head_data']=$this->head_data();
           

            $data['division_list']=$this->admin_model->select_all_name_ascending('name','divisions');

            $this->load->view('market/add_market',$data);
        }
    }



    public function add_market_post()
    {
 

        $data['division_id']=$this->input->post('division_id');
        $data['district_id']=$this->input->post('district_id');
        $data['area_id']=$this->input->post('area_id');
        $data['name']=$this->input->post('name');
        $data['address']=$this->input->post('address');
        $data['details']=$this->input->post('details');

        $reg_id=$this->session->userdata('reg_id');
        $data['reg_id']=$reg_id;
        $data['contact']=$this->input->post('contact');
        $data['email']=$this->input->post('email');

        $data['fb_link']=$this->input->post('fb_link');
        $data['twitter_link']=$this->input->post('twitter_link');
        $data['linked_link']=$this->input->post('linked_link');

        $data['created_at']=date('Y-m-d h:i:a');


        $market_id=$this->admin_model->insert_ret('market',$data);

        $full_open_day=$this->input->post('full_open_day');
        if($full_open_day)
        {
            foreach ($full_open_day as $key => $value) {
               $data_sc['day']=$value;
               $data_sc['market_id']=$market_id;
               $data_sc['open_time']=date("H:i", strtotime($this->input->post('full_opening_time')));
               $data_sc['close_time']=date("H:i", strtotime($this->input->post('full_closing_time')));
               $data_sc['created_at']=date('Y-m-d h:i:a');
               $this->admin_model->insert('market_schedule',$data_sc);
            }
        }

        $half_open_day=$this->input->post('half_open_day');
        if($half_open_day)
        {
            foreach ($half_open_day as $key => $value) {
               $data_sc1['day']=$value;
               $data_sc1['market_id']=$market_id;
               $data_sc1['open_time']=date("H:i", strtotime($this->input->post('half_opening_time')));
               $data_sc1['close_time']=date("H:i",strtotime($this->input->post('half_closing_time')));
               $data_sc1['created_at']=date('Y-m-d h:i:a');
               $this->admin_model->insert('market_schedule',$data_sc1);
            }
        }

        $data_pro['profile_name'] = preg_replace('/[ ,]+/', '_', trim($data['name']));
        $data_pro['profile_name']=$data_pro['profile_name'].'_'.$market_id;
        $data_pro['profile_name'] = str_replace("'", '', $data_pro['profile_name']);

        $this->admin_model->update_function('id', $market_id, 'market', $data_pro);


        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);

        for($i=0; $i<$cpt; $i++)
        {           
            $_FILES['userfile']['name']= uniqid().'_'.underscore($files['userfile']['name'][$i]);
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i]; 

            $oldFileName = $_FILES['userfile']['name'];
            $_FILES['userfile']['name'] =str_replace("'","", $oldFileName);
            $this->upload->initialize($this->set_upload_options($_FILES['userfile']['name'],'market/'));
            $this->upload->do_upload();  

            if($i==0)
            {
                
                $main_image['main_image']=$_FILES['userfile']['name'];

                $this->admin_model->update_function('id', $market_id, 'market', $main_image);

            }

            else
            {
                $data['market_details']=$this->admin_model->select_with_where('*', 'id='.$market_id, 'market');

                if($data['market_details'][0]['gallery_image']=='')
                {
                    $data_image['gallery_image']=$_FILES['userfile']['name'];
                }
                else
                {
                    $data_image['gallery_image']=$data['market_details'][0]['gallery_image'].','.$_FILES['userfile']['name'];
                }
                $this->admin_model->update_function('id', $market_id, 'market', $data_image);
            }



        }

        redirect('market_admin/edit_market','refresh');
    }


    public function edit_market()
    {

        $reg_id=$this->session->userdata('reg_id');
       
        $id=$this->admin_model->select_with_where('id', 'reg_id='.$reg_id, 'market');
        
        if(count($id)>0)
        {
            
            $data['active']='market';
            $data['left_nav_active']='edit';
            $data['page_title']=$this->lang->line('market_label').' '.$this->lang->line('btn_edit_label');
     
            $data['breadcrumbs']= '<li><span class="active">'.$data['page_title'].'</span></li>';

            $data['head_data']=$this->head_data();

            $reg_id=$this->session->userdata('reg_id');


            $id=$this->admin_model->select_with_where('id', 'reg_id='.$reg_id, 'market');
            $id=$id[0]['id'];

            $data['market_details']=$this->admin_model->select_with_where('*', 'id='.$id, 'market');

            $data['market_schedule']=$this->admin_model->select_with_where('*', 'market_id='.$id, 'market_schedule');
           // echo "<pre>"; print_r($data['market_schedule']);die();            

            if($data['market_details'][0]['reg_id']==$reg_id)
            {

                $data['division_list']=$this->admin_model->select_all_name_ascending('name','divisions');
                $data['district_list']=$this->admin_model->select_all_name_ascending('name','districts');
                $data['area_list']=$this->admin_model->select_all_name_ascending('area_title','area');

                $data['market_list']=$this->admin_model->select_all_name_ascending('name','market');

                $this->load->view('market/edit_market',$data);
            }

            else
            {
                $data['unauthorised_msg']=$this->lang->line('unauthorised_msg');
                $this->load->view('blank',$data);
            }
        }
        else
        {
            redirect('market_admin/add_market','refresh');
        }
    }


    public function update_market_post($id)
    {
        
        $data['address']=$this->input->post('address');
        $data['details']=$this->input->post('details');

        $data['contact']=$this->input->post('contact');
        $data['email']=$this->input->post('email');

        $data['fb_link']=$this->input->post('fb_link');
        $data['twitter_link']=$this->input->post('twitter_link');
        $data['linked_link']=$this->input->post('linked_link');

        $data['status']=2;
        
        $this->admin_model->update_function('id', $id, 'market', $data);

        $new_open_day=$this->input->post('new_open_day');
        if($new_open_day)
        {
            foreach ($new_open_day as $key => $value) {
               $data_sc['day']=$value;
               $data_sc['market_id']=$id;
               $data_sc['open_time']=date("H:i", strtotime($this->input->post('new_opening_time')));
               $data_sc['close_time']=date("H:i", strtotime($this->input->post('new_closing_time')));
               $data_sc['created_at']=date('Y-m-d h:i:a');
               $this->admin_model->insert('market_schedule',$data_sc);
            }
        }

        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);

        for($i=0; $i<$cpt; $i++)
        {           
            $_FILES['userfile']['name']= uniqid().'_'.underscore($files['userfile']['name'][$i]);
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i]; 

            $oldFileName = $_FILES['userfile']['name'];
            $_FILES['userfile']['name'] =str_replace("'","", $oldFileName);
            $this->upload->initialize($this->set_upload_options($_FILES['userfile']['name'],'market/'));
            $this->upload->do_upload();  

            if($i==0)
            {
                if($files['userfile']['name'][$i]!='')
                {
                    $main_image=$this->input->post('main_image');
                    $path_to_file = 'uploads/market/'.$main_image;
                    if(unlink($path_to_file)) {}
                    $p_image['main_image']=$_FILES['userfile']['name'];
                    $this->admin_model->update_function('id', $id, 'market', $p_image);
                }
            }

            else
            {
               if($files['userfile']['name'][$i]!='')
                { 
                    $data['market_details']=$this->admin_model->select_with_where('*', 'id='.$id, 'market');

                    if($data['market_details'][0]['gallery_image']=='')
                    {
                        $data_image['gallery_image']=$_FILES['userfile']['name'];
                    }
                    else
                    {
                        $data_image['gallery_image']=$data['market_details'][0]['gallery_image'].','.$_FILES['userfile']['name'];
                    }
                    $this->admin_model->update_function('id', $id, 'market', $data_image);
                }
            }
        }

        $this->session->set_userdata('scc_alt', $this->lang->line('update_scc_msg'));
        redirect('market_admin/edit_market','refresh');
    }




    public function market_file()
    {
        $data['active']='market_file';

        $reg_id=$this->session->userdata('reg_id');
        $id=$this->admin_model->select_with_where('id', 'reg_id='.$reg_id, 'market');
        
        if(count($id)>0)
        {
           $data['left_nav_active']='edit';
        }
        else
        {
            $data['left_nav_active']='add';
        }


        $data['page_title']=$this->lang->line('market_label').' '.$this->lang->line('lottery_label');
        $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('lottery_label');
 
        $data['breadcrumbs']= '<li><span class="active">'.$data['page_title'].'</span></li>';

        $data['head_data']=$this->head_data();
        if(count($id)>0)
        {
            $data['market_file']=$this->admin_model->select_with_where('*', 'market_id='.$id[0]['id'], 'market_file');
        }
        else
        {
            $data['market_file']=array();
        }
        //echo "<pre>";print_r($data['market_file']);die();
        $this->load->view('market/file_list',$data);
    }

    public function add_market_file()
    {
        $data['active']='market_file';

        $reg_id=$this->session->userdata('reg_id');
        $id=$this->admin_model->select_with_where('id', 'reg_id='.$reg_id, 'market');
        
        if(count($id)>0)
        {
           $data['left_nav_active']='edit';
        }
        else
        {
            $data['left_nav_active']='add';
        }

        $data['page_title']=$this->lang->line('market_label').' '.$this->lang->line('lottery_label');
 
        $data['breadcrumbs']= '<li><span class="active">'.$data['page_title'].'</span></li>';

        $data['head_data']=$this->head_data();
       

        $this->load->view('market/add_file',$data);
    }


    public function add_market_file_post()
    {
 
        $reg_id=$this->session->userdata('reg_id');
        $id=$this->admin_model->select_with_where('id', 'reg_id='.$reg_id, 'market');
        
        $data['file_title']=$this->input->post('name');
        $data['market_id']=$id[0]['id'];
        $data['created_at']=date('Y-m-d h:i:a');
        

        $file_id=$this->admin_model->insert_ret('market_file',$data);

        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);

        for($i=0; $i<$cpt; $i++)
        {           
            $_FILES['userfile']['name']= uniqid().'_'.underscore($files['userfile']['name'][$i]);
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i]; 

            $oldFileName = $_FILES['userfile']['name'];
            $_FILES['userfile']['name'] =str_replace("'","", $oldFileName);
            $this->upload->initialize($this->set_upload_options($_FILES['userfile']['name'],'market/market_file/'));
            $this->upload->do_upload();  

            
            $data['market_file']=$this->admin_model->select_with_where('*', 'id='.$file_id, 'market_file');

            if($data['market_file'][0]['file_name']=='')
            {
                $data_image['file_name']=$_FILES['userfile']['name'];
            }
            else
            {
                $data_image['file_name']=$data['market_file'][0]['file_name'].','.$_FILES['userfile']['name'];
            }
            $this->admin_model->update_function('id', $file_id, 'market_file', $data_image);
            
        }
        $this->session->set_userdata('scc_alt', $this->lang->line('insert_scc_msg'));
        redirect('market_admin/market_file','refresh');
    }



    public function edit_market_file($id)
    {
        $data['active']='market_file';

        $reg_id=$this->session->userdata('reg_id');
        $r_id=$this->admin_model->select_with_where('id', 'reg_id='.$reg_id, 'market');
        
        if(count($r_id)>0)
        {
           $data['left_nav_active']='edit';
        }
        else
        {
            $data['left_nav_active']='add';
        }

        $data['page_title']=$this->lang->line('market_label').' '.$this->lang->line('file_label').' '.$this->lang->line('btn_edit_label');
 
        $data['breadcrumbs']= '<li><span class="active">'.$data['page_title'].'</span></li>';

        $data['head_data']=$this->head_data();
        $data['market_file_details']=$this->admin_model->select_with_where('*', 'id='.$id, 'market_file');
        if($r_id[0]['id']==$data['market_file_details'][0]['market_id'])
        {

            $this->load->view('market/edit_file',$data);
        }
        else
        {
            $data['unauthorised_msg']=$this->lang->line('unauthorised_msg');
            $this->load->view('blank',$data);
        }
    }





    public function update_market_file_post($file_id)
    {

        $data['file_title']=$this->input->post('name');     

        $this->admin_model->update_function('id', $file_id, 'market_file', $data);

        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);

        for($i=0; $i<$cpt; $i++)
        {           
            $_FILES['userfile']['name']= uniqid().'_'.underscore($files['userfile']['name'][$i]);
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i]; 

            $oldFileName = $_FILES['userfile']['name'];
            $_FILES['userfile']['name'] =str_replace("'","", $oldFileName);
            $this->upload->initialize($this->set_upload_options($_FILES['userfile']['name'],'market/market_file/'));
            $this->upload->do_upload();  

            
            $data['market_file']=$this->admin_model->select_with_where('*', 'id='.$file_id, 'market_file');

            if($data['market_file'][0]['file_name']=='')
            {
                $data_image['file_name']=$_FILES['userfile']['name'];
            }
            else
            {
                $data_image['file_name']=$data['market_file'][0]['file_name'].','.$_FILES['userfile']['name'];
            }
            $this->admin_model->update_function('id', $file_id, 'market_file', $data_image);
            
        }
        $this->session->set_userdata('scc_alt', $this->lang->line('update_scc_msg'));
        redirect('market_admin/market_file','refresh');
    }
    

    public function update_market_schedule()
    {
       $id=$this->input->post('id');
       $data_sc['day']=$this->input->post('day');
       $data_sc['open_time']=date("H:i", strtotime($this->input->post('open_time')));
       $data_sc['close_time']=date("H:i", strtotime($this->input->post('close_time')));
       $this->admin_model->update_function('id', $id, 'market_schedule', $data_sc);
    }


    public function delete_market_file($id)
    {
        $data['active']='market_file';

        $reg_id=$this->session->userdata('reg_id');
        $r_id=$this->admin_model->select_with_where('id', 'reg_id='.$reg_id, 'market');
        
        if(count($r_id)>0)
        {
           $data['left_nav_active']='edit';
        }
        else
        {
            $data['left_nav_active']='add';
        }

        $data['page_title']=$this->lang->line('market_label').' '.$this->lang->line('file_label').' '.$this->lang->line('btn_edit_label');
 
        $data['breadcrumbs']= '<li><span class="active">'.$data['page_title'].'</span></li>';

        $data['head_data']=$this->head_data();
        $reg_id=$this->session->userdata('reg_id');
        $r_id=$this->admin_model->select_with_where('id', 'reg_id='.$reg_id, 'market');
        $market_details=$this->admin_model->select_with_where('*', 'id='.$id, 'market_file');

        if($r_id[0]['id']==$market_details[0]['market_id'])
        {
           
            $img_name=$market_details[0]['file_name'];
            $path_to_file = 'uploads/market/market_file/'.$img_name;
            if(unlink($path_to_file)) 
            {
            }

            $this->admin_model->delete_function_cond('market_file', 'id='.$id);
            $this->session->set_userdata('scc_alt', $this->lang->line('delete_scc_msg'));
            redirect('market_admin/market_file','refresh');
        }
        else
        {
            $data['unauthorised_msg']=$this->lang->line('unauthorised_msg');
            $this->load->view('blank',$data);
        }
    }

    public function delete_gallery_img()
    {
        $shop_id=$this->input->post('shop_id');
        $img_name=$this->input->post('img_name');

        $market_details=$this->admin_model->select_with_where('*', 'id='.$shop_id, 'market');

        $img_gallery=explode(',', $market_details[0]['gallery_image']);
        $data_image['gallery_image']=NULL;
        foreach ($img_gallery as $key => $img) 
        {
           
           if($img==$img_name)
           {

           }
           else
           {
                $data_image['gallery_image'].=$img.',';
           }
           
        }

        $data_image['gallery_image']=substr($data_image['gallery_image'], 0, -1); // remove last comma

        $path_to_file = 'uploads/market/'.$img_name;
        if(unlink($path_to_file)) 
        {
            $this->admin_model->update_function('id', $shop_id, 'market', $data_image);
            echo "delete successfull";
        }
    }

    public function delete_market_schedule($id)
    {
        $data['active']='market';

        $reg_id=$this->session->userdata('reg_id');
        $r_id=$this->admin_model->select_with_where('id', 'reg_id='.$reg_id, 'market');
        
        if(count($r_id)>0)
        {
           $data['left_nav_active']='edit';
        }
        else
        {
            $data['left_nav_active']='add';
        }

        $data['page_title']=$this->lang->line('market_label').' '.$this->lang->line('file_label').' '.$this->lang->line('btn_edit_label');
 
        $data['breadcrumbs']= '<li><span class="active">'.$data['page_title'].'</span></li>';

        $data['head_data']=$this->head_data();
        $reg_id=$this->session->userdata('reg_id');
        $r_id=$this->admin_model->select_with_where('id', 'reg_id='.$reg_id, 'market');
        $market_details=$this->admin_model->select_with_where('*', 'id='.$id, 'market_file');

        if($r_id[0]['id']==$market_details[0]['market_id'])
        {
           

            $this->admin_model->delete_function_cond('market_schedule', 'id='.$id);
            $this->session->set_userdata('scc_alt', $this->lang->line('delete_scc_msg'));
            redirect('market_admin','refresh');
        }
        else
        {
            $data['unauthorised_msg']=$this->lang->line('unauthorised_msg');
            $this->load->view('blank',$data);
        }
    }


    public function delete_file()
    {
        $id=$this->input->post('id');
        $img_name=$this->input->post('img_name');

        $market_details=$this->admin_model->select_with_where('*', 'id='.$id, 'market_file');

        $img_gallery=explode(',', $market_details[0]['file_name']);
        $data_image['file_name']=NULL;
        foreach ($img_gallery as $key => $img) 
        {
           
           if($img==$img_name)
           {

           }
           else
           {
                $data_image['file_name'].=$img.',';
           }
           
        }

        $data_image['file_name']=substr($data_image['file_name'], 0, -1); // remove last comma

        $path_to_file = 'uploads/market/market_file/'.$img_name;
        if(unlink($path_to_file)) 
        {
            $this->admin_model->update_function('id', $id, 'market_file', $data_image);
            echo "delete successfull";
        }
    }





////////////////////********  General Function Starts *******///////////////////////////

    public function resize($height, $width, $source, $destination)
    {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source;
        $config['overwrite'] = TRUE;
        $image_config['quality'] = "100%";
        $image_config['maintain_ratio'] = FALSE;
        $config['height'] = $height;
        $config['width'] = $width;
        $config['new_image'] = $destination; //you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

     public function resize2($height,$width)
    {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = "uploads/watermark.png";
        $config['overwrite'] = TRUE;
        $image_config['quality'] = "100%";
        $image_config['maintain_ratio'] = FALSE;
        $config['height'] = $height;
        $config['width'] = $width;
        $config['new_image'] = "uploads/product/";//you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }



    private function set_upload_options($file_name,$folder_name)
    {   
        //upload an image options
        $url=base_url();

        $config = array();
         $config['file_name'] = $file_name;
        $config['upload_path'] = 'uploads/'.$folder_name;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']      = '0';
        $config['overwrite']     = TRUE;

        return $config;
    }




    public function watermark_image($imageWidth,$imageHeight)
    {
        $file = 'uploads/watermark.png';
        $newfile ='uploads/watermark.png';
         
        if (!copy($file, $newfile)) {
           // echo "failed to copy $file...\n";
        }else{
           // echo "copied $file into $newfile\n";
        }
        
        //shell_exec("cp -r ".$file." ".$newfile);
        //echo $imageWidth.' '.$imageHeight;
        //die();
        $this->resize2($imageWidth,$imageHeight);
                
            
    }




     public function overlay($path)
    {
        $config['image_library']    = 'gd2';
        $config['source_image']     = 'uploads/product/'.$path;
        $config['wm_type']          = 'overlay';
        $config['wm_overlay_path']  = 'uploads/watermark.png'; //the overlay image
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $config['wm_padding'] = '0';
     // $config['wm_x_transp'] = '200';
        //$config['wm_y_transp'] = '20';
        $config['wm_opacity'] = '50';

        $this->image_lib->initialize($config);
    
        
        if (!$this->image_lib->watermark()) {
            echo $this->image_lib->display_errors();
        }
    }

    public function head_data()
    {
        $login_id = 1;
        $data=$this->admin_model->select_with_where('*', 'login_id='.$login_id, 'company');
        return $data;
    }

    public function get_sub_category()
    {
        $cat_id=$this->input->post('cat_id');
        $get_sub_category=$this->admin_model->select_with_where('*', 'cat_id='.$cat_id, 'sub_category');

        echo "<option></option>";
        foreach ($get_sub_category as $key => $value) {
            echo '<option value="'.$value["id"].'">'.$value["sub_name"].'</option>';
        }
    }


    public function get_district()
    {
        $division_id=$this->input->post('division_id');
        $get_district=$this->admin_model->select_with_where('*', 'division_id='.$division_id, 'districts');

        echo "<option></option>";
        foreach ($get_district as $key => $value) {
            echo '<option value="'.$value["id"].'">'.$value["name"].' '.$value["bn_name"].'</option>';
        }
    }


    public function get_area()
    {
        $district_id=$this->input->post('district_id');
        $get_area=$this->admin_model->select_with_where('*', 'district_id='.$district_id, 'area');

        echo "<option></option>";
        foreach ($get_area as $key => $value) {
            echo '<option value="'.$value["area_id"].'">'.$value["area_title"].'</option>';
        }
    }

///////////////////////********  General Function End *******////////////////////////////////

}

