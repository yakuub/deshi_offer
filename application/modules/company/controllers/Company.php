<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Company extends MX_Controller {
//CI_Controller
    //public $counter=0;
    function __construct() {
        parent::__construct();
         $this->load->model('admin/admin_model');
        if($this->session->userdata('language_select')=='bangla')
        {
            $this->lang->load('admin', 'bangla');
        }
        else
        {
            $this->lang->load('admin', 'english');
        }

        $login_id=   $this->session->userdata('login_id');
        $name   =    $this->session->userdata('name');
        $role   =    $this->session->userdata('role');
        
        if($login_id=='' || $role==0)
        {
            $this->session->unset_userdata('login_id');
            $this->session->unset_userdata('name');
            $this->session->unset_userdata('role');
            $this->session->set_userdata('log_err','Enter Email and Password First');
            redirect('login','refresh');
        }
        
        if($this->session->userdata('area_retreive_id')=='')
        {
            redirect('home','refresh');
        }

    }

    public function index()
    {
        $data['head_data'][0]['name']='Company Info';
        $data['page_title']=$this->lang->line('Website Initial Information');
        $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('company_info_label').'</span></li>';
        $this->load->view('company',$data);   
    }

    public function add_company_post()
    {
        $data['login_id']=$this->session->userdata('login_id');;
        $data['name']=$this->input->post('name');
        $data['phone']=$this->input->post('contact');
        $data['email']=$this->input->post('email');
        $data['address']=$this->input->post('address');
        $data['fb_link']=$this->input->post('fb_link');
        $data['twitter_link']=$this->input->post('twitter_link');
        $data['linked_link']=$this->input->post('linked_link');
        $data['logo']='logo.png';

        if ($_FILES['userfile']['name']!='') 
        {
            $oldFileName = uniqid().'_'.underscore($_FILES['userfile']['name']);
            $_FILES['userfile']['name'] =str_replace("'","", $oldFileName);
            $this->upload->initialize($this->set_upload_options($_FILES['userfile']['name'],'logo/'));
            $this->upload->do_upload();
            $this->resize(70, 180, 'uploads/logo/'.$_FILES['userfile']['name'], 'uploads/logo/'.$_FILES['userfile']['name']);
            $data['logo']=$_FILES['userfile']['name'];   
        }
        
        $data['created_at'] =date('Y-m-d h:i:a');

        $this->admin_model->insert_ret('company', $data);

        redirect('admin','refresh');
    }


    public function resize($height, $width, $source, $destination)
    {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source;
        $config['overwrite'] = TRUE;
        $image_config['quality'] = "100%";
        $image_config['maintain_ratio'] = FALSE;
        $config['height'] = $height;
        $config['width'] = $width;
        $config['new_image'] = $destination; //you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    private function set_upload_options($file_name,$folder_name)
    {   
        //upload an image options
        $url=base_url();

        $config = array();
         $config['file_name'] = $file_name;
        $config['upload_path'] = 'uploads/'.$folder_name;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']      = '0';
        $config['overwrite']     = TRUE;

        return $config;
    }
}
