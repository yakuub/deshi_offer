<?php $this->load->view('admin/headlink'); ?>
<style>
    .time-label{
        margin-top: 5px;
    }
    .delete_img
    {
        position: absolute;
        top: 20px;
        color: #444 !important;
        font-weight: 700;
        border: 1px solid #ccc;
        padding: 5px;
        right: 30px;
        background: white !important;
        transition: .5s !important;
    }
    .delete_img:hover
    {
        color: white !important;
        background: #ed6b75 !important;
        transition: .5s !important;
    }
	.page-header-fixed .page-container {
     margin-top: 0px;
}
</style>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
    
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->

            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="margin-left:20%;width:60%">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-newspaper-o"></i> <?=$this->lang->line('company_info_label').' '.$this->lang->line('form_label');?></div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        
                                        <a href="javascript:;" class="remove"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form  class="form-horizontal form-bordered form-row-stripped" action="company/add_company_post" method="post" enctype="multipart/form-data">
                                        <div class="form-body">

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Title</label>
                                                <div class="col-md-9">
                                                    <input required="" name="name" type="text" class="form-control"> 
												</div>
                                            </div>
											
											<div class="form-group">
                                                <label class="control-label col-md-3"> <?=$this->lang->line('contact_label').' '.$this->lang->line('phone_tag_label');?></label>
                                                <div class="col-md-4">
                                                    <input required="" class="form-control" name="contact" id="mask_number" type="text" />
                                                    
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('email_label');?>
                                                </label>
                                                <div class="col-md-9">
                                                    <input name="email" type="email" class="form-control"> 
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('address_tag_label');?>
                                                </label>
                                                <div class="col-md-9">
                                                    <input name="address" type="text" class="form-control"> 
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('facebook_link_label');?>
                                                </label>
                                                <div class="col-md-9">
                                                    <input name="fb_link" type="text" class="form-control"> 
                                                </div>
                                            </div>


                                             <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('twitter_link_label');?>
                                                </label>
                                                <div class="col-md-9">
                                                    <input name="twitter_link" type="text" class="form-control"> 
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('linked_link_label');?>
                                                </label>
                                                <div class="col-md-9">
                                                    <input name="linked_link" type="text" class="form-control"> 
                                                </div>
                                            </div>
 
                                        
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Logo</label>
                                                <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="back_assets/img_default.png" alt="Your Image" />            
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>
                                                            <span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>
                                                            <input type="file" name="userfile"> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?=$this->lang->line('remove_label');?> </a>
                                                    </div>
                                                     <div class="clearfix margin-top-10">
                                                        <span class="label label-danger">NOTE!</span> <?=$this->lang->line('img_resolution_msg');?> (180 x 70)px </div> 
                                                </div>
                                                </div>
                                            </div>  
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">
                                                        <i class="fa fa-check"></i> <?=$this->lang->line('btn_submit_label');?></button>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->


            <!-- BEGIN Right SIDEBAR -->
            <!-- END Right SIDEBAR -->


        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->

      <?php $this->load->view('admin/footerlink'); ?>

    <script>
var pre_img_count=$("#pre_img_count").val();
var div_count=1+parseFloat(pre_img_count);

        function delete_gallery_img(shop_id,img_name) 
        {
            $.ajax({
                url: "<?php echo site_url('admin/delete_gallery_img');?>",
                type: "post",
                data: {shop_id:shop_id,img_name:img_name},
                success: function(msg)
                {
                    // alert(msg);
                    $("#my_div").load(location.href + " #my_div");
                    pre_img_count=pre_img_count-1;
                   
                    div_count=1+parseFloat(pre_img_count);

                    //alert(pre_img_count);
                   //$("#sub_cat_id").html(msg);
                }      
            });  
        }


        function get_sub_category() 
        {
            var cat_id=$("#cat_id").val();

            $.ajax({
                url: "<?php echo site_url('admin/get_sub_category');?>",
                type: "post",
                data: {cat_id:cat_id},
                success: function(msg)
                {
                    // alert(msg);
                   $("#sub_cat_id").html(msg);
                }      
            });  
        }


        function get_district() 
        {
            var division_id=$("#division_id").val();

            $.ajax({
                url: "<?php echo site_url('admin/get_district');?>",
                type: "post",
                data: {division_id:division_id},
                success: function(msg)
                {
                    // alert(msg);
                   $("#district_id").html(msg);
                }      
            });  
        }



        function get_area() 
        {
            var district_id=$("#district_id").val();

            $.ajax({
                url: "<?php echo site_url('admin/get_area');?>",
                type: "post",
                data: {district_id:district_id},
                success: function(msg)
                {
                    //alert(msg);
                   $("#area_id").html(msg);
                   
                }      
            });  
        }
    </script>
      <!-- Add Product Image Div -->

    <script>
 

        var div_count=1+parseFloat(pre_img_count);
        //alert(div_count);
        function add_image_div() 
        {
           //alert(div_count);
           if(div_count==9)
           {
            $("#add_image_div").hide();
           }

           
              $("#add_image_div").before(
                '<div class="col-md-3">'
                +'<div class="fileinput fileinput-new" data-provides="fileinput">'
                    +'<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">'
                        +'<img src="back_assets/img_default.png" alt="Your Image" /> </div>'
                    +'<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>'
                    +'<div>'
                        +'<span class="btn default btn-file">'
                            +'<span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>'
                            +'<span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>'
                            +'<input type="file" name="userfile[]"> </span>'
                        +'<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?=$this->lang->line('remove_label');?> </a>'
                    +'</div>'
                   
                +'</div>'
                +'</div>');


              if(div_count==3 || div_count==6)
            {
                $("#add_image_div").before('<label class="control-label col-md-3"></label>');
            }
              div_count++;

          }
    </script>

    </body>

</html>