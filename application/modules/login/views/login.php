<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<!DOCTYPE html>
<base href="<?php echo base_url();?>">
<html>
  <head>
    <title>Deshi Offer</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="Deshi Offer" name="keywords">
    <meta content="Deshi Offer" name="author">
    <meta content="Deshi Offer" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="assets/favicon.png" rel="shortcut icon">
    <link href="assets/apple-touch-icon.png" rel="apple-touch-icon">
    <link href="//fast.fonts.net/cssapi/175a63a1-3f26-476a-ab32-4e21cbdb8be2.css" rel="stylesheet" type="text/css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>

    <link href="assets/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="assets/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="assets/bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="assets/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="assets/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="assets/css/main.css?version=2.6" rel="stylesheet">
  </head>
  <body class="auth-wrapper">
    <div class="all-wrapper menu-side with-pattern">
      <div class="auth-box-w">
        <div class="logo-w">
          <a href="index.php"><img alt="" src="assets/img/logo-big.png"></a>
        </div>
        <h4 class="auth-header">
          Login Form 
        </h4>
                <?php if($this->session->userdata('log_err')){ ?>
                <p>
                  
                    <span><?=$this->session->userdata('log_err');?></span>
                </p>
                 <?php } $this->session->unset_userdata('log_err');?>

                  <?php if($this->session->userdata('log_scc')){ ?>
                <p>
                
                    <span><?=$this->session->userdata('log_scc');?></span>
                </p>
                 <?php } $this->session->unset_userdata('log_scc');?>

       <?php echo form_open('login/login_check')?>
          <div class="form-group">
            <label for="">Username</label><input class="form-control" placeholder="Enter your username" name="username" type="text" required>
            <div class="pre-icon os-icon os-icon-user-male-circle"></div>
          </div>
          <div class="form-group">
            <label for="">Password</label><input class="form-control" placeholder="Enter your password"  name="password" type="password" required>
            <div class="pre-icon os-icon os-icon-fingerprint"></div>
          </div>
          <div class="buttons-w">
            <button class="btn btn-primary">Log me in</button>
            <div class="form-check-inline">
              <label class="form-check-label"><input class="form-check-input" type="checkbox">Remember Me</label>
            </div>
          </div>
         <?php echo form_close()?>
      </div>
    </div>
  </body>
</html>
