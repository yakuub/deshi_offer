<?php $this->load->view('front/headlink');?>


<style type="text/css">
  .form-bg
  {
    background-color: #eee !important;
  }
</style>

<body style="margin:0;padding: 1px;border: 3px solid #ccc">
  <?php $this->load->view('front/head_nav');?>

  <div style="margin:0 auto;border: 1px solid #efefef;background: #eee;padding: 30px 50px;">




    <div class="row">
      <div class="container">



        <div class="col-md-6 col-md-offset-3">

          <div align="center">
            <p style="font-weight: bold;color: #130F7C;font-family: Open Sans;">CREATE NEW PASSWORD</p>
            <p style="color: #535252;font-family: Open Sans;">You have requested to reset the password</p>
          </div>

          <div class="element_box" style="padding: 30px; margin-top: 10px; height: 250;margin-bottom: 40px; width: 530px; background: #FFFFFF; border: 2px solid #F5F5F5">

           <div class="alert alert-danger alrt_form" id="err_div_reg_modal" style="display: none">
            <label id="m_msg"></label>
          </div>

         

        <form action="login/change_password_post" onsubmit="return password_check()" method="post">

          <input type="hidden" value="<?=$login_id?>" name="login_id">

          <div class="form-group">
            <label for="new_password">New Password:</label>
            <input type="password" class="form-control" id="new_password" placeholder="Enter New Password" name="new_password"  required="">
          </div>
          <div class="form-group">
            <label for="re_password">Retype Password:</label>
            <input type="password" class="form-control" id="re_password" placeholder="Retype password" name="re_password"  required="">
          </div>

          <button type="submit" class="btn btn-primary">Reset My Password</button>
        </form>


      </div>

      <a href="home" style="color: #4C0965;font-size: 23px;font-family: Lato;">Back to Home</a>

    </div>
  </div>

</div>

</div>


<script type="text/javascript">

  function password_check(argument) {

    password=$('#new_password').val();
    con_password=$('#re_password').val();

    if(password!=con_password)
    {
      $("#err_div_reg_modal").show();
      $("#m_msg").html('Password not matched');
      $("#agreement").prop( "disabled", false );
      return false;
    }

  }
</script>

</body>
<?php $this->load->view('front/footer');?>
<?php $this->load->view('front/footerlink');?>


</html>