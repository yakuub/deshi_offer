<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_model extends CI_Model  {
    
 function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }


    public function check_login($username, $pass) {
        $raw_sql='select * from user_login where (email="'.$username.'" OR contact="'.$username.'") AND password="'.$pass.'" AND activation_status=1';
        $query = $this->db->query($raw_sql);

        return $query->result_array();
    }

    public function get_name($id)
    {
        $this->db->select('*');
        $this->db->from('user_login');
        $this->db->where('loginid', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

     public function password_change($login_id,$data)
    {
        $this->db->where('loginid',$login_id);
        $this->db->update('user_login',$data);
    }

}

?>