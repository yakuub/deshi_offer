<?php if (!defined('BASEPATH'))
exit('No direct script access allowed'); 

class Login extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('offer/offer_model');
        $this->load->model('home/home_model');
        $this->load->model('registration/registration_model');
        $this->load->model('admin/admin_model');
        $this->load->helper('inflector');
        // $this->load->library('encrypt');
        

    }

    public function index()
    {
        $data['head_data']=$this->home_model->select_with_where('*', 'id=1', 'company');
        $data['tab_active']='login';
        $this->load->view('login',$data);
    }

    public function login_check()
    {

     $this->form_validation->set_rules('username', 'Username', 'required|trim');
     $this->form_validation->set_rules('password', 'Password', 'required|trim');


     if ($this->form_validation->run() == FALSE)
     {

      $this->load->view('login');

  } 
  else
  {
    $username=$this->input->post('username');
    $pass=$this->encryptIt($this->input->post('password'));   
        //echo '<pre>';print_r($pass);die();

    $result = $this->login_model->check_login($username,$pass);

        //echo '<pre>';print_r($result);die();

    if (count($result) > 0) 
    {


        if($result[0]['verify_status']==0)
        {
            $this->session->set_userdata('log_err','Profile not verified by Admin');
            redirect('login', 'refresh');
        }

        else
        {

            $this->session->set_userdata('login_id', $result[0]['loginid']);



            $this->session->set_userdata('email' , $result[0]['email']);

            $this->session->set_userdata('type' ,$result[0]['usertype']);

            $name=$this->login_model->get_name($result[0]['loginid']);

            $this->session->set_userdata('name' , $name[0]['username']);


            $this->session->set_userdata('login_scc', 'Welcome to Admin Dashboard.');

            // "<pre>";print_r( $this->session->userdata());die();

            if($result[0]['usertype']==1)
            {
                redirect('admin','refresh');
            }

            elseif($result[0]['usertype']==2)
            {
                       //echo $this->session->userdata('loginid');
             redirect('merchant','refresh');
         }

         elseif($result[0]['usertype']==3)
         {
           // echo $this->session->userdata('loginid');

           redirect('user','refresh');
       }




   }    
}


else 
{
    $this->session->set_userdata('log_err','Email or Password is Incorrect.');
    redirect('login', 'refresh');
}
} 


}

    // public function login_check() 
    // {

    //     $email = $this->input->post('email');
    //     $password = $this->encryptIt($this->input->post('password'));


    //     $res = $this->login_model->check_login($email, $password);
    //     //print_r($res);
    //     if (count($res) > 0) 
    //     {

    //         $this->session->set_userdata('login_id', $res[0]['id']);
    //         $this->session->set_userdata('reg_id' , $res[0]['reg_id']);
    //         $this->session->set_userdata('type' ,$res[0]['type']);
    //         $this->session->set_userdata('district_id', $res[0]['district_id']);

    //         // $this->session->set_userdata('login_id', 1);
    //         // $this->session->set_userdata('reg_id' , 6);
    //         // $this->session->set_userdata('type' ,0);
    //         // $this->session->set_userdata('district_id', 0);


    //         $name=$this->login_model->get_name($res[0]['reg_id']);

    //         $this->session->set_userdata('name' , $name[0]['name']);


    //         $this->session->set_userdata('login_scc', 'Welcome to Admin Dashboard.');

    //         if($res[0]['type']==1)
    //         {
    //             redirect('admin','refresh');
    //         }

    //         elseif($res[0]['type']==2)
    //         {
    //            //echo $this->session->userdata('login_id');
    //            redirect('merchant','refresh');
    //         }

    //         elseif($res[0]['type']==3)
    //         {
    //             redirect('user','refresh');
    //         }    
    //     }


    //     else 
    //     {
    //         $this->session->set_userdata('log_err','Email or Password is Incorrect.');
    //         redirect('login', 'refresh');
    //     }

    // }

public function ajax_login_check()
{
    $email=$this->input->post('username');
    $password = $this->encryptIt($this->input->post('password'));
    if(is_numeric($email))
    {
        if($email[0]==0)
        {    
            $email[0]="";
        }
    }

    $res = $this->login_model->check_login($email, $password);
        //echo "<pre>";print_r($res);die();
    if(count($res) > 0)
    {
        
        if($res[0]['verify_status']==0)
        {
            echo 'not_verify';
        }
        else
        {
        $this->session->set_userdata('login_id',$res[0]['loginid']);
        $this->session->set_userdata('email',$res[0]['email']);
        $this->session->set_userdata('type',$res[0]['usertype']);     
        $this->session->set_userdata('name' , $res[0]['username']);
        $this->session->set_userdata('p_image' , $res[0]['profile_pic']);
        $this->session->set_userdata('sub_tab' , 'sub_cat');
        echo $res[0]['usertype'];
        }
        

    }
    else
    {
        echo 0;
    }
}


public function new_user_login_check()
{
    $email=$this->input->post('email');
    $password = $this->encryptIt($this->input->post('password'));
    if(is_numeric($email))
    {
        if($email[0]==0)
        {    
            $email[0]="";
        }
    }

    $res = $this->login_model->check_login($email, $password);
        //echo "<pre>";print_r($res);die(); 
    if(count($res)>0)
    {
        $this->session->set_userdata('login_id',$res[0]['loginid']);
        $this->session->set_userdata('email',$res[0]['email']);
        $this->session->set_userdata('type',$res[0]['usertype']);     
        $this->session->set_userdata('name' , $res[0]['name']);
        $this->session->set_userdata('p_image' , $res[0]['profile_pic']);
        echo $res[0]['user_type'];
    }
    else
    {
        echo 0;
    }
}



public function pass_gen()
{
    $ss=$this->encryptIt('123456');
    echo $ss;
}

public function change_password($activation_key='',$login_id='')
{
    $data['active']='home';
    $data['header']=$this->home_model->select_with_where('*','id=1','company');
            // $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
    $data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

    $data['login_id']=$login_id;

    $cur_date=date('Y-m-d');

    $data['cat_brand']=array();
    $data['store_offer']=array();
    foreach ($data['category'] as $key => $value) 
    {
        $data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

        foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
        {
            $data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND (mf.edate>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
        }   

    }

    $data['is_feature']=$this->home_model->select_condition_random_with_limit('user_login','is_feature=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

    $data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','top_store=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

    $data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND verify_status=1 AND is_publish=1','28');

    $data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

    $data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');

            //echo "<pre>";print_r($data['special_offer']);die();

    $data['feature_store']=$this->home_model-> select_condition_random_with_limit('user_login','show_on_web=1 AND verify_status=1 AND usertype=2',12);


    $data['hot_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.is_hot=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','updated_at','RANDOM');

            //echo "<pre>";print_r($data['hot_offer_list']);die();

    $data['new_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")',8,'mf.offer_id','DESC');

    $data['recent_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.total_seen','DESC');

            // top 4 categories offer
    $data['page_category']=$this->home_model->select_with_where('*','front_page_show_st=1','catagory');

            //echo "<pre>";print_r($data['page_category']);die();


    if(array_key_exists(0,$data['page_category']))
    {
        $data['top_0_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][0]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
    }
    if(array_key_exists(1,$data['page_category']))
    {
        $data['top_1_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][1]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
    }

    if(array_key_exists(2,$data['page_category']))
    {
        $data['top_2_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][2]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
    }
    if(array_key_exists(3,$data['page_category']))
    {
        $data['top_3_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][3]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
    }

    if(array_key_exists(4,$data['page_category']))
    {
        $data['top_4_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][4]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
    }



    $data['slider_list']=$this->home_model->select_condition_acending_with_limit('slider','status=1','slider_sl','8');


    $data['slider_advertise']=$this->home_model->select_with_where('*','status=1 AND advertise_position=1','advertise');

    $data['user_details']=$this->home_model->select_with_where('*','loginid="'.$this->session->userdata('login_id').'"','user_login');


    $this->load->helper('string');



    if($activation_key!='')
    {
        $exist_link=$this->home_model->select_with_where('*','forget_password_token="'.$activation_key.'"','user_login');

    // $this->load->view('new_user_login');

        if(count($exist_link)>0)
        {
          $activation_key=random_string('alnum', 50);

          $val['forget_password_token']=$activation_key.$login_id;
          $this->offer_model->update_function('loginid',$login_id,'user_login',$val);

          $this->load->view('view_forget_password',$data);

      }
      else
      {
          $this->load->view('registration/invalid_link');
      }
  }

  else
  {
    $this->load->view('registration/invalid_link',$data);
}



}

public function change_password_post()
{
    $login_id = $this->input->post('login_id');
    $pass = $this->encryptIt($this->input->post('new_password'));
    $this->load->helper('string');

    $forget_password_token=random_string('alnum', 50).$login_id;
    $data = array('password' => $pass,
       'forget_password_token' =>$forget_password_token);
    $this->login_model->password_change($login_id,$data);

    $this->session->set_userdata('pass_change_scc','Successfully changed your password! Login with your "NEW" password.');
    
    // redirect('login/change_password');

    $this->load->view('success_msg');

}

public function registration()
{
   $data['comp_name']=$this->input->post('comp_name');
   $data['comp_title']=$this->input->post('comp_title');
   $data['comp_address']=$this->input->post('comp_address');
   $data['comp_phone']=$this->input->post('comp_phone');

   $this->db->insert('company',$data);
}


function decryptIt($string) {
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
    $secret_iv = 'This is my secret iv';
    // hash
    $key = hash('sha256', $secret_key);
    
    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    return $output;
}

function encryptIt($string) 
{


    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
    $secret_iv = 'This is my secret iv';
        // hash
    $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);

    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    $output = base64_encode($output);
    $output=str_replace("=", "", $output);
    return $output;
}


public function forget_password() 
{

    $email=$this->input->post('email');

    $data_login['user_info']=$this->admin_model->select_with_where('*','email="'.$email.'"','user_login');

    if($data_login['user_info']!=null && $email != null)
    {
        $this->load->helper('string');



        $msg_from= 'support@deshioffer.com';
        $c_name='Deshioffer-Team';
        $name=$data_login['user_info'][0]['username'];


        $val['forget_password_token']=random_string('alnum', 50).$data_login['user_info'][0]['loginid'];

        $this->admin_model->update_function('email', $email, 'user_login',$val);


        $msg_to=$email;

        $this->send_email_function($msg_from,$name,$msg_to,$c_name,$val['forget_password_token'],$data_login['user_info'][0]['loginid']);

        echo 1;
    }
    else
    {
        echo 2;
    }

    


}

public function send_email_function($msg_from,$name,$msg_to,$c_name,$activation_key,$login_id)
{
  $config = Array(
    'protocol' => 'smtp',
    'smtp_host' => 'mail.deshioffer.com',
    'smtp_port' => 25,
    'smtp_user' => 'noreply@deshioffer.com',
    'smtp_pass' => 'userpass@@@123',
    'mailtype'  => 'html', 
    'charset'   => 'iso-8859-1',
    'wordwrap' => 'TRUE',
    'newline'   => "\r\n",
    'crlf'   => "\r\n",
);

  $this->load->library('email', $config);
  $this->email->set_newline("\r\n");

  $this->email->from($msg_from,$c_name);
  $this->email->to($msg_to);
  $base=$this->config->base_url();

  $data['title']='Change Your Password';
  $data['name']=$name;
  $data['email']=$msg_to;
  $data['activation_key']=$activation_key;
  $data['msg_body']='You have to click on this link to change your password <a href="https://deshioffer.com/login/change_password/'.$activation_key.'/'.$login_id.'"> link.</a>.';

  $message=$this->load->view('registration/templete_email/templete_email', $data,'true');

  $this->email->subject('Deshioffer- Forget Password');
  $this->email->message($message);
  $this->email->set_mailtype("html");
  $this->email->send();
}



}
