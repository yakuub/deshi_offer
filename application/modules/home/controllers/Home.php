<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');    
class Home extends MX_Controller {


	function __construct()
	{
		$this->load->model('home_model');
		$this->load->model('product/product_model');
		$this->load->model('admin/admin_model');
		$this->load->model('offer/offer_model');
		

		date_default_timezone_set('Asia/Dhaka');
		if($this->session->userdata('language_select')=='bangla')
		{
			$this->lang->load('front', 'bangla');
		}
		else
		{
			$this->lang->load('front', 'english');
		} 

		$login_id =   $this->session->userdata('login_id');
		$type     =    $this->session->userdata('type');
		$name     =    $this->session->userdata('name');
		$email     =    $this->session->userdata('email');


	}
	public function index()
	{
		$data['active']='home';
		$data['header']=$this->home_model->select_with_where('*','id=1','company');
			// $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
		$data['category']=$this->home_model->select_all_condition_order('catagory','top_menu_st=1 AND menu_st=1','top_menu_sl','ASC');

		$cur_date=date('Y-m-d');

		$data['cat_brand']=array();
		$data['store_offer']=array();
		foreach ($data['category'] as $key => $value) 
		{
			$data['cat_brand'][$key]=$this->home_model->get_shop_category('mf.catagories='.$value['catagory_id'].' AND user_login.usertype=2');

			foreach ($data['cat_brand'][$key] as $b_key => $b_value) 
			{
				$data['store_offer'][$key][$b_key]=count($this->home_model->select_with_where('offer_id','mf.status=1 AND (mf.edate>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00") AND mf.offer_from='.$b_value['loginid'],'main_offer mf'));
			}	

		}

		$data['is_feature']=$this->home_model->select_condition_random_with_limit('user_login','is_feature=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

		$data['top_store']=$this->home_model->select_condition_random_with_limit('user_login','top_store=1 AND usertype=2 AND verify_status=1 AND is_publish=1','28');

		$data['top_brand']=$this->home_model->select_condition_random_with_limit('user_login','usertype=2 AND is_brand=1 AND verify_status=1 AND is_publish=1','28');

		$data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

		$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');

			//echo "<pre>";print_r($data['special_offer']);die();

		$data['feature_store']=$this->home_model-> select_condition_random_with_limit('user_login','show_on_web=1 AND verify_status=1 AND usertype=2',12);


		$data['hot_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.is_hot=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','updated_at','RANDOM');

			//echo "<pre>";print_r($data['hot_offer_list']);die();

		$data['new_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")',8,'mf.offer_id','DESC');

		$data['recent_offer_list']=$this->offer_model->get_offer_ci('mf.status=1 AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.total_seen','DESC');

			// top 4 categories offer
		$data['page_category']=$this->home_model->select_with_where('*','front_page_show_st=1','catagory');

			//echo "<pre>";print_r($data['page_category']);die();


		if(array_key_exists(0,$data['page_category']))
		{
			$data['top_0_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][0]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
		}
		if(array_key_exists(1,$data['page_category']))
		{
			$data['top_1_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][1]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
		}

		if(array_key_exists(2,$data['page_category']))
		{
			$data['top_2_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][2]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
		}
		if(array_key_exists(3,$data['page_category']))
		{
			$data['top_3_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][3]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
		}

		if(array_key_exists(4,$data['page_category']))
		{
			$data['top_4_category_list']=$this->offer_model->get_offer_ci('mf.status=1 AND mf.catagories='.$data['page_category'][4]['catagory_id'].' AND (mf.show_expire_date>=DATE("'.$cur_date.'") OR mf.edate="0000-00-00")','8','mf.updated_at','DESC');
		}



		$data['slider_list']=$this->home_model->select_condition_acending_with_limit('slider','status=1','slider_sl','8');


		$data['slider_advertise']=$this->home_model->select_with_where('*','status=1 AND advertise_position=1','advertise');

		$data['user_details']=$this->home_model->select_with_where('*','loginid="'.$this->session->userdata('login_id').'"','user_login');

		$this->session->set_userdata('referred_from', current_url());

			//$data['latest_list']=$this->home_model->select_condition_decending_with_limit('product','status=1','12');

		    //echo "<pre>";print_r($data['slider_list']);die();

		$this->load->view('index',$data);
	}


	public function quickview_product_ajax($value='')
	{
		$data['name']='';
		$p_id=$this->input->post('p_id');
		$data['color_list']=$this->home_model->select_all('color');
		$data['size_list']=$this->home_model->select_all('size');
		$data['product_details']=$this->product_model->product_details('p_id='.$p_id);
		$this->load->view('product/include/product_quick_view', $data);
	}

	public function get_search_result()
	{
		$search_item=$this->input->post('search_item');

		$get_data=$this->home_model->select_with_where_decending2('*','offer_title LIKE "%'.$search_item.'%"','main_offer','offer_id');
		
		$get_category=$this->home_model->select_with_where_decending2('*','catagory_title LIKE "%'.$search_item.'%"','catagory','catagory_id');

		if((count($get_data)>0)||(count($get_category)>0) )
		{
			foreach ($get_data as $key => $value) {
				echo '<li><a target="_deshioffer" href="offer/offer_details/'.$value["profile_name"].'"><img src="uploads/offer/'.$value["offer_featured_file"].'" alt="">'.$value["offer_title"].'</a></li>';
			}

			foreach ($get_category as $key => $value) {
				echo '<li><a target="_deshioffer" href="offer/'.$value["catagory_title"].'"><img src="uploads/category/'.$value["catagory_pic"].'" alt="">'.$value["catagory_title"].'</a></li>';
			}
		}
		else
		{
			echo '<li>No Data Found</li>';
		}
	}

	public function category_subscription_modal()
	{

		$data['catagory_id']=$this->input->post('catagory_id');
		$this->load->view('include/subscribe_modal', $data);

	}

	public function add_subscribed_catagory()
	{

		$email=$this->input->post('email');
		$catagory_id=$this->input->post('catagory_id');

		$exist_subscribed_catagory=$this->home_model->select_with_where('*','email="'.$email.'"','user_login'); 

		$unreg_user_subscription=$this->home_model->select_with_where('*','email="'.$email.'"','subscription'); 



		if(count($exist_subscribed_catagory)>0)
		{
			$pre_subscribed_catagory=$exist_subscribed_catagory[0]['subscribed_category'];

			$subscribed_cat_id=$this->home_model->subscribed_catagory($email,$catagory_id);

	       //echo '<pre>';print_r($subscribed_cat_id);die();

			if(count($subscribed_cat_id)>=1)
			{
				echo 1;
			}

			else
			{
				$data_update['subscribed_category']='';
				if($pre_subscribed_catagory=='' || $pre_subscribed_catagory==null || $pre_subscribed_catagory==0)
				{
					$data_update['subscribed_category']=$catagory_id;
				}
				else
				{
					$data_update['subscribed_category']=$pre_subscribed_catagory.','.$catagory_id;
				}

				$this->home_model->update_function('loginid',$exist_subscribed_catagory[0]['loginid'],'user_login',$data_update);
				echo 2;
			}
		}

		else 
		{
			$pre_subscribed_catagory=$unreg_user_subscription[0]['subscribed_category'];

			$subscribed_cat_id=$this->home_model->unreg_email_subscribed_catagory($email,$catagory_id);

	       //echo '<pre>';print_r($pre_subscribed_catagory);die();

			if(count($subscribed_cat_id)>=1)
			{
				echo 1;
			}

			else
			{
				$data_sub['subscribed_category']='';

				if($pre_subscribed_catagory=='' || $pre_subscribed_catagory==null || $pre_subscribed_catagory==0)
				{
					$data_sub['email']=$email;
					$data_sub['subscribed_category']=$catagory_id;

					if (count($unreg_user_subscription)==0) 
					{
						$data_sub['created_at']= date('Y-m-d H:i:s');
						$this->home_model->insert_ret('subscription',$data_sub);

					}
					else
					{	
						$this->home_model->update_function('email',$email,'subscription',$data_sub);
					}
					echo 2;
				}
				else
				{

					$data_sub['subscribed_category']=$pre_subscribed_catagory.','.$catagory_id;

					$this->home_model->update_function('email',$email,'subscription',$data_sub);
					echo 2;
				}

			}
		}

	}


	public function get_code_modal()
	{
		$offer_id=$this->input->post('offer_id');
		$data['offer_details']=$this->offer_model->get_offer_details('mf.offer_id='.$offer_id);

		$val['total_used']=$data['offer_details'][0]['total_used']+1;

		$this->offer_model->update_function2('offer_id="'.$offer_id.'"','main_offer',$val);

		//echo "<pre>";print_r($data['get_offer_details']);
		$data['store_info']=$this->home_model->select_with_where('*','loginid='.$data['offer_details'][0]['offer_from'],'user_login');

		$data['total_offer']=count($this->home_model->select_with_where('*','offer_from='.$data['offer_details'][0]['offer_from'],'main_offer'));

		$data['store_info_details']=$this->home_model->select_with_where('*','user_id='.$data['offer_details'][0]['offer_from'],'marchant_details');
		$this->load->view('offer/include/coupon_modal', $data);
	}

	public function get_sms_modal()
	{
		$data['offer_id']=$this->input->post('offer_id');
		$data['offer_details']=$this->offer_model->get_offer_details('mf.offer_id='.$data['offer_id']);

		$val['total_used']=$data['offer_details'][0]['total_used']+1;

		$this->offer_model->update_function2('offer_id="'.$data['offer_id'].'"','main_offer',$val);

		//echo "<pre>";print_r($data['get_offer_details']);
		$data['store_info']=$this->home_model->select_with_where('*','loginid='.$data['offer_details'][0]['offer_from'],'user_login');

		$data['total_offer']=count($this->home_model->select_with_where('*','offer_from='.$data['offer_details'][0]['offer_from'],'main_offer'));

		$data['sms_offer_info']=$this->home_model->select_with_where('*','offer_id='.$data['offer_id'],'offer_sms');

		$data['store_info_details']=$this->home_model->select_with_where('*','user_id='.$data['offer_details'][0]['offer_from'],'marchant_details');
		$this->load->view('offer/include/sms_modal', $data);
	}

	public function open_deal_activate($value='')
	{
		$data['offer_id']=$this->input->post('offer_id');
		$data['offer_details']=$this->offer_model->get_offer_details('mf.offer_id='.$data['offer_id']);

		$val['total_used']=$data['offer_details'][0]['total_used']+1;

		$this->offer_model->update_function2('offer_id="'.$data['offer_id'].'"','main_offer',$val);
		//echo "<pre>";print_r($data['get_offer_details']);
		$data['store_info']=$this->home_model->select_with_where('*','loginid='.$data['offer_details'][0]['offer_from'],'user_login');

		$data['total_offer']=count($this->home_model->select_with_where('*','offer_from='.$data['offer_details'][0]['offer_from'],'main_offer'));

		$data['sms_offer_info']=$this->home_model->select_with_where('*','offer_id='.$data['offer_id'],'offer_sms');

		$data['store_info_details']=$this->home_model->select_with_where('*','user_id='.$data['offer_details'][0]['offer_from'],'marchant_details');
		
		$this->load->view('offer/include/deal_activated_modal', $data);
	}

	


	public function send_sms()
	{
		$this->load->library('sendsms_library');

		$number=$this->input->post('number');
		$offer_id=$this->input->post('offer_id');
		$offer_details=$this->offer_model->get_offer_details('mf.offer_id='.$offer_id);

		$company_name="Deshioffer";
		$msg_to='880'.$number;
		$msg_des=$offer_details[0]['offer_title'].' '.$offer_details[0]['sms'];


		$sms_mobile_no=$this->home_model->sms_mobile_number($offer_id,$msg_to);

		if (count($sms_mobile_no)>0) 
		{
			
			echo 1;

		}
		else
		{
			$report=$this->sendsms_library->send_single_sms($company_name,$msg_to,$msg_des);

			if($offer_details[0]['mobile_number']!='')
			{
				$data_sms['mobile_number']=$offer_details[0]['mobile_number'].','.$msg_to;
			}
			
			else
			{
				$data_sms['mobile_number']=$msg_to;
			}
			$this->home_model->update_function('offer_id',$offer_id,'offer_sms',$data_sms);

			if($report=='err')
			{
				echo 0;
			}
			else
			{
				echo 2;
			}
			
		}
		
	}

	public function get_qr_code_modal()
	{
		


		$data['offer_id']=$this->input->post('offer_id');
		$data['offer_details']=$this->offer_model->get_offer_details('mf.offer_id='.$data['offer_id']);

		$val['total_used']=$data['offer_details'][0]['total_used']+1;

		$this->offer_model->update_function2('offer_id="'.$data['offer_id'].'"','main_offer',$val);
		//echo "<pre>";print_r($data['get_offer_details']);
		$data['store_info']=$this->home_model->select_with_where('*','loginid='.$data['offer_details'][0]['offer_from'],'user_login');

		$data['total_offer']=count($this->home_model->select_with_where('*','offer_from='.$data['offer_details'][0]['offer_from'],'main_offer'));

		$data['store_info_details']=$this->home_model->select_with_where('*','user_id='.$data['offer_details'][0]['offer_from'],'marchant_details');


		$this->load->library('qr_code/ciqrcode');
		
		$params['data'] = 'Offer Title:'.$data['offer_details'][0]['offer_title'].' '.'Voucher Code: '.$data['offer_details'][0]['voucher_code'].' '.'Discount:'.$data['offer_details'][0]['discount'].' '.'Expire Date:'.$data['offer_details'][0]['edate'];

		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'uploads/vouchar_code/'.$data['offer_details'][0]['offer_title'].'_qr_code_image'.'.png';
		
		//$data['qr_image']=$params['savename'];

		$data['qr_image']= base_url().'uploads/vouchar_code/'.$data['offer_details'][0]['offer_title'].'_qr_code_image'.'.png';
		$this->ciqrcode->generate($params);


		$this->load->view('offer/include/qr_code_modal', $data);
	}

	public function save_image()
	{
		$data['offer_id']=$this->input->post('offer_id');
		//$btn_name=$this->input->post('name');


		$data['offer_details']=$this->offer_model->get_offer_details('mf.offer_id='.$data['offer_id']);

		$this->load->library('qr_code/ciqrcode');
		
		$params['data'] = 'Offer Title:'.$data['offer_details'][0]['offer_title'].' '.'Voucher Code: '.$data['offer_details'][0]['voucher_code'].' '.'Discount:'.$data['offer_details'][0]['discount'].' '.'Expire Date:'.$data['offer_details'][0]['edate'];

		$params['level'] = 'H';
		$params['size'] = 10;
		$params['savename'] = FCPATH.'uploads/vouchar_code/'.$data['offer_details'][0]['offer_title'].'_qr_code_image'.'.png';
		
		
		$this->ciqrcode->generate($params);

		// if ($btn_name=='download') 
		// {
		// 	//file_get_contents($params['savename']);
		// 	echo base_url().'uploads/vouchar_code/'.$data['offer_details'][0]['offer_title'].'_qr_code_image'.'.png';
		// }
		
		
	}




}
