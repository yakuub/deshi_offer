<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content modal_nl">
    <div class="modal-header" style="background-color: #6C08C5; padding: 25px 40px;">
      <button type="button" class="close" data-dismiss="modal" style="color: #fff; opacity:0.7;position: absolute;left: 96%;top: 3%;" >&times;</button>
      <p class="text-center" style="color: #fff">Don't miss any offer, Subscribe to email alerts for new offers and deals to the following category.</p>
      <hr style="border:1px solid #F7DA0A">
      <p class="text-center" style="color: #F7DA0A"><strong>SUBSCRIBE</strong></p>
      <i class="fa fa-telegram fa-tg-nl" aria-hidden="true"></i>
      <h4 class="modal-title text-center" style="color: #fff">Newsletter</h4>

    </div>
    <div class="modal-body">

      <div id="loading_img" style="display:none;position: fixed;z-index: 10;top: 40%;left: 45%;width: 9%;">
             <img style="width: 100%" src="front_assets/loading-spinner-blue.gif">     
      </div>

      <div class="alert alert-danger alrt_form" id="err_div" style="display: none">
        <label id="m_msg"></label>
      </div>

      <div class="alert alert-info alrt_form" id="s_div" style="display: none">
        <label id="s_msg"></label>
      </div>

      <div class="newsletter_form">
        <input type="hidden" value="<?=$catagory_id;?>" name="catagory_id" id="catagory_id_modal">
          <input class="form-control w_75" placeholder="Enter Your Email" id="email" name="email" type="email" required="">
        <input type="submit" value="Submit" onclick="subscribed_catagory_ajax()" class="btn-primary" style="font-size: 15px; border-radius: 5px;border:none; box-shadow: none; display: block; margin: 0 auto; width: 25%; padding: 5px 0;background: #450082" >
      </div>
    </div>

  </div>

</div>