<?php $start2=1;$start3=1; $last_item=count($offer_list); 
foreach ($offer_list as $key => $value) { ?> 
   <?php if($key==0){?>
      <div class="col-md-4 product_wrap_left">
      <?php } ?>
      <?php if($key==0 ||$key==1 ||$key==2){?>
         <div class="col-md-12 product_each_wrapper">
            <div class="col-md-12 product excl-product">
               <div class="product_head col-md-12">
                  <div class="tag" style="background: <?=$value['offer_color_code'];?>"><?=$value['offer_type_tilte'];?></div>

                  <?php if ($this->session->userdata('type')==3 &&in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>


                     <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart" style="font-size:24px;color: red;"></i>

                     </a>

                  <?php } else if($this->session->userdata('type')==3 && !in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>

                   <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>


                   <?php } else { ?>
                     <a class="favourite" data-toggle="modal" data-target="#login_modal" href="javaScript:void(0)" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>
                      
                     <?php } ?>


                     <div class="clearfix"></div>
                  </div>
                  <div class="col-md-6 product_img col-xs-6">
                     <a target="_deshioffer" href="offer/offer_details/<?=$value['profile_name'];?>"><img style="height: 110px" src="uploads/offer/<?=$value['offer_featured_file'];?>" class="img-responsive">
                     </a>
                     <?php if($value['discount']>0){?>
                        <div class="percentage"><?=$value['discount'];?>%</div>
                     <?php } ?>
                  </div>
                  <div class="col-md-6 product_description col-xs-6">
                     <a class="product_title" target="_deshioffer" href="offer/offer_details/<?=$value['profile_name'];?>">
                        <?=word_limiter($value['offer_title'],5);?>     
                     </a>
                     <p class="offr_tag"><i class="fa fa-tags offr_fa"></i>
                        <?php if($value['offer_store_type']==0){echo "Store Offer";}else{echo "Online Offer";}?>
                     </p>
                     <!--<span class="offr_tag"><i class="fa fa-map-marker rv_icon"></i> Gulshan</span> -->
                     <p  class="offr_price"><i class="fa  fa-user-plus offr_fa"></i> <span class="offr_usd_c"><?=$value['total_seen'];?> </span> Used Already</p>
                     <?php if($value['sale_price']!=''){?>
                        <p class="offr_price">Price: <?=$value['sale_price'];?> &#x9f3;</p>
                     <?php } ?>
                     <?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($value['edate']);
                     $diff = $date2 - $date1;
                     $day=round($diff / 86400);?>
                     <?php if($value['edate']=="0000-00-00"){?>
                        <p class="expire" style="font-weight: bold">Unlimite</p>
                     <?php } else if($day<0){?>
                        <p class="expire" style="font-weight: bold">Expired</p>
                     <?php } else if($day>0){?>
                        <p class="expire">Expire in <?=$day;?> Days.</p>
                     <?php } else { ?>
                        <p class="expire">Expire in Today</p>
                     <?php } ?>

                     <div class="clearfix"></div>
                  </div>
                  <div class="amazon"><a href="store/offer/<?=$value['name'];?>"><img style="width: 95px;height: 26px;" src="uploads/user/<?=$value['profile_pic'];?>" alt=""></a></div>
                  <?php if($day<0 && $value['edate']!="0000-00-00"){?>
                     <a target="_blank" href="offer/offer_details/<?=$value['profile_name'];?>" class="offer_btn" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                  <?php } else {?>
                     <?php if($value['ctype']==1){?>
                        <a onclick="get_vouchar('<?=$value['main_off_id'];?>');" class="offer_btn" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                     <?php } else if($value['ctype']==2){?>
                        <a class="offer_btn"  onclick="get_code('<?=$value['main_off_id'];?>');" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                     <?php } else if($value['ctype']==3){?>
                        <a  class="offer_btn"  onclick="get_sms('<?=$value['main_off_id'];?>');" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                     <?php } else if($value['ctype']==4){?>
                        <a target="_blank" class="offer_btn" href="<?=$value['ref_link'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                     <?php } else if($value['ctype']==5){?>
                        <a target="_blank" class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                     <?php } else if($value['ctype']==6){?>
                        <a target="_blank" class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                     <?php } else{ ?>
                        <a target="_blank" class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                     <?php } } ?>
                  </div>
               </div>
            <?php } ?>
            <?php if($key==2 || (($last_item<3) && ($key+1)==$last_item)) {?>
            </div>
         <?php } ?>
         <!--product left end-->
         <?php if($key==3){ $start2=0;?>
            <div class="col-md-4 product_special_wrap">
            <?php } ?>
            <?php if($key==3){?>
               <div class="col-md-12 product_special product_each_wrapper">
                  <div class="col-md-12 product">
                     <div class="product_head col-md-12">
                        <div class="tag" style="background: <?=$value['offer_color_code'];?>"><?=$value['offer_type_tilte'];?></div>
                        <?php if ($this->session->userdata('type')==3 &&in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>


                           <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart" style="font-size:24px;color: red;"></i>

                           </a>

                        <?php } else if($this->session->userdata('type')==3 && !in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>

                         <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>


                         <?php } else { ?>
                           <a class="favourite" data-toggle="modal" data-target="#login_modal" href="javaScript:void(0)" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>
                            
                           <?php } ?>
                           <div class="clearfix"></div>
                        </div>
                        <a class="product_title text-center" style="display: block;" target="_deshioffer" href="offer/offer_details/<?=$value['profile_name'];?>">
                           <?=word_limiter($value['offer_title'],5);?>
                        </a>
                        <div class="col-md-12 product_img col-xs-6">
                           <img style="height: 150px" src="uploads/offer/<?=$value['offer_featured_file'];?>" class="img-responsive">
                           <?php if($value['discount']>0){?>
                              <div class="percentage"><?=$value['discount'];?>%</div>
                           <?php } ?>
                        </div>
                        <div class="col-md-12 product_description col-xs-6">
                           <p class="offr_tag"><i class="fa fa-tags offr_fa"></i> Store Offer</p>
                           <!--<span class="offr_tag"><i class="fa fa-map-marker rv_icon"></i> Gulshan</span> -->
                           <p  class="offr_usd"><i class="fa  fa-user-plus offr_fa"></i> <span class="offr_usd_c"><?=$value['total_seen'];?> </span> Used Already</p>
                           <?php if($value['sale_price']!=''){?>
                              <p class="offr_tag">Price: <?=$value['sale_price'];?> &#x9f3;</p>
                           <?php } ?>
                           <?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($value['edate']);
                           $diff = $date2 - $date1;
                           $day=round($diff / 86400);?>
                           <?php if($value['edate']=="0000-00-00"){?>
                              <p class="expire" style="font-weight: bold">Unlimite</p>
                           <?php } else if($day<0){?>
                              <p class="expire" style="font-weight: bold">Expired</p>
                           <?php } else if($day>0){?>
                              <p class="expire">Expire in <?=$day;?> Days.</p>
                           <?php } else { ?>
                              <p class="expire">Expire in Today</p>
                           <?php } ?>

                           <div class="clearfix"></div>

                        </div>
                        <div class="amazon"><a href="store/offer/<?=$value['name'];?>"><img style="width: 95px;height: 26px;" src="uploads/user/<?=$value['profile_pic'];?>" alt=""></a></div>
                        <?php if($day<0 && $value['edate']!="0000-00-00"){?>
                           <a target="_blank" href="offer/offer_details/<?=$value['profile_name'];?>" class="offer_btn" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                        <?php } else {?>
                           <?php if($value['ctype']==1){?>
                              <a onclick="get_vouchar('<?=$value['main_off_id'];?>');" class="offer_btn" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                           <?php } else if($value['ctype']==2){?>
                              <a class="offer_btn"  onclick="get_code('<?=$value['main_off_id'];?>');" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                           <?php } else if($value['ctype']==3){?>
                              <a  class="offer_btn"  onclick="get_sms('<?=$value['main_off_id'];?>');" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                           <?php } else if($value['ctype']==4){?>
                              <a target="_blank" class="offer_btn" href="<?=$value['ref_link'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                           <?php } else if($value['ctype']==5){?>
                              <a target="_blank" class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                           <?php } else if($value['ctype']==6){?>
                              <a target="_blank" class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                           <?php } else{ ?>
                              <a target="_blank" class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                           <?php } } ?>
                        </div>
                     </div>
                  <?php } ?>
                  <?php if($key==4){?>
                     <div class="col-md-12 product_each_wrapper">
                        <div class="col-md-12 product excl-product">
                           <div class="product_head col-md-12">
                              <div class="tag" style="background: <?=$value['offer_color_code'];?>"><?=$value['offer_type_tilte'];?></div>


                              <?php if ($this->session->userdata('type')==3 &&in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>


                                 <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart" style="font-size:24px;color: red;"></i>

                                 </a>

                              <?php } else if($this->session->userdata('type')==3 && !in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>

                               <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>


                               <?php } else { ?>
                                 <a class="favourite" data-toggle="modal" data-target="#login_modal" href="javaScript:void(0)" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>
                                  
                                 <?php } ?>
                                 <div class="clearfix"></div>
                              </div>
                              <div class="col-md-6 product_img col-xs-6">
                                 <a href="offer/offer_details/<?=$value['profile_name'];?>"><img style="height: 110px" src="uploads/offer/<?=$value['offer_featured_file'];?>" class="img-responsive">
                                 </a>
                                 <?php if($value['discount']>0){?>
                                    <div class="percentage"><?=$value['discount'];?>%</div>
                                 <?php } ?>
                              </div>
                              <div class="col-md-6 product_description col-xs-6">
                                 <a class="product_title" target="_deshioffer" href="offer/offer_details/<?=$value['profile_name'];?>">
                                    <?=word_limiter($value['offer_title'],5);?>     
                                 </a>
                                 <p class="offr_tag"><i class="fa fa-tags offr_fa"></i>
                                    <?php if($value['offer_store_type']==0){echo "Store Offer";}else{echo "Online Offer";}?>
                                 </p>
                                 <!--<span class="offr_tag"><i class="fa fa-map-marker rv_icon"></i> Gulshan</span> -->
                                 <p  class="offr_price"><i class="fa  fa-user-plus offr_fa"></i> <span class="offr_usd_c"><?=$value['total_seen'];?> </span> Used Already</p>
                                 <?php if($value['sale_price']!=''){?>
                                    <p class="offr_price">Price: <?=$value['sale_price'];?> &#x9f3;</p>
                                 <?php } ?>
                                 <?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($value['edate']);
                                 $diff = $date2 - $date1;
                                 $day=round($diff / 86400);?>
                                 <?php if($value['edate']=="0000-00-00"){?>
                                    <p class="expire" style="font-weight: bold">Unlimite</p>
                                 <?php } else if($day<0){?>
                                    <p class="expire" style="font-weight: bold">Expired</p>
                                 <?php } else if($day>0){?>
                                    <p class="expire">Expire in <?=$day;?> Days.</p>
                                 <?php } else { ?>
                                    <p class="expire">Expire in Today</p>
                                 <?php } ?>

                                 <div class="clearfix"></div>
                              </div>
                              <div class="amazon"><a href="store/offer/<?=$value['name'];?>"><img style="width: 95px;height: 26px;" src="uploads/user/<?=$value['profile_pic'];?>" alt=""></a></div>

                              <?php if($day<0 && $value['edate']!="0000-00-00"){?>
                                 <a target="_blank" href="offer/offer_details/<?=$value['profile_name'];?>" class="offer_btn" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                              <?php } else {?>
                                 <?php if($value['ctype']==1){?>
                                    <a onclick="get_vouchar('<?=$value['main_off_id'];?>');" class="offer_btn" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                 <?php } else if($value['ctype']==2){?>
                                    <a class="offer_btn"  onclick="get_code('<?=$value['main_off_id'];?>');" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                 <?php } else if($value['ctype']==3){?>
                                    <a  class="offer_btn"  onclick="get_sms('<?=$value['main_off_id'];?>');" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                 <?php } else if($value['ctype']==4){?>
                                    <a target="_blank" class="offer_btn" href="<?=$value['ref_link'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                 <?php } else if($value['ctype']==5){?>
                                    <a target="_blank" class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                 <?php } else if($value['ctype']==6){?>
                                    <a target="_blank" class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                 <?php } else{ ?>
                                    <a target="_blank" class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                 <?php } } ?>
                              </div>
                           </div>
                        <?php } ?>
                        <?php if($key==4 || (($last_item<5) && ($key+1)==$last_item) && ($last_item>=3)){?>
                        </div>
                     <?php } ?>
                     <!--product special end-->
                     <?php if($key==5){?>
                        <div class="col-md-4 product_wrap_right">
                        <?php } ?>
                        <?php if($key==5 ||$key==6 ||$key==7){?>
                           <div class="col-md-12 product_each_wrapper">
                              <div class="col-md-12 product excl-product">
                                 <div class="product_head col-md-12">
                                    <div class="tag" style="background: <?=$value['offer_color_code'];?>"><?=$value['offer_type_tilte'];?></div>
                                    

                                    <?php if ($this->session->userdata('type')==3 &&in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>


                                       <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart" style="font-size:24px;color: red;"></i>

                                       </a>

                                    <?php } else if($this->session->userdata('type')==3 && !in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>

                                     <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>


                                     <?php } else { ?>
                                       <a class="favourite" data-toggle="modal" data-target="#login_modal" href="javaScript:void(0)" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>
                                        
                                       <?php } ?>
                                       <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-6 product_img col-xs-6">
                                       <a href="offer/offer_details/<?=$value['profile_name'];?>"><img style="height: 110px" src="uploads/offer/<?=$value['offer_featured_file'];?>" class="img-responsive">
                                       </a>
                                       <?php if($value['discount']>0){?>
                                          <div class="percentage"><?=$value['discount'];?>%</div>
                                       <?php } ?>
                                    </div>
                                    <div class="col-md-6 product_description col-xs-6">
                                       <a class="product_title" target="_deshioffer" href="offer/offer_details/<?=$value['profile_name'];?>">
                                          <?=word_limiter($value['offer_title'],5);?>     
                                       </a>
                                       <p class="offr_tag"><i class="fa fa-tags offr_fa"></i>
                                          <?php if($value['offer_store_type']==0){echo "Store Offer";}else{echo "Online Offer";}?>
                                       </p>
                                       <!--<span class="offr_tag"><i class="fa fa-map-marker rv_icon"></i> Gulshan</span> -->
                                       <p  class="offr_price"><i class="fa  fa-user-plus offr_fa"></i> <span class="offr_usd_c"><?=$value['total_seen'];?> </span> Used Already</p>
                                       <?php if($value['sale_price']!=''){?>
                                          <p class="offr_price">Price: <?=$value['sale_price'];?> &#x9f3;</p>
                                       <?php } ?>
                                       <?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($value['edate']);
                                       $diff = $date2 - $date1;
                                       $day=round($diff / 86400);?>
                                       <?php if($value['edate']=="0000-00-00"){?>
                                          <p class="expire" style="font-weight: bold">Unlimite</p>
                                       <?php } else if($day<0){?>
                                          <p class="expire" style="font-weight: bold">Expired</p>
                                       <?php } else if($day>0){?>
                                          <p class="expire">Expire in <?=$day;?> Days.</p>
                                       <?php } else { ?>
                                          <p class="expire">Expire in Today</p>
                                       <?php } ?>

                                       <div class="clearfix"></div>
                                    </div>
                                    <div class="amazon"><a href="store/offer/<?=$value['name'];?>"><img style="width: 95px;height: 26px;" src="uploads/user/<?=$value['profile_pic'];?>" alt=""></a></div>
                                    <?php if($day<0 && $value['edate']!="0000-00-00"){?>
                                       <a target="_blank" href="offer/offer_details/<?=$value['profile_name'];?>" class="offer_btn" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                    <?php } else {?>
                                       <?php if($value['ctype']==1){?>
                                          <a onclick="get_vouchar('<?=$value['main_off_id'];?>');" class="offer_btn" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                       <?php } else if($value['ctype']==2){?>
                                          <a class="offer_btn"  onclick="get_code('<?=$value['main_off_id'];?>');" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                       <?php } else if($value['ctype']==3){?>
                                          <a  class="offer_btn"  onclick="get_sms('<?=$value['main_off_id'];?>');" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                       <?php } else if($value['ctype']==4){?>
                                          <a target="_blank" class="offer_btn" href="<?=$value['ref_link'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                       <?php } else if($value['ctype']==5){?>
                                          <a target="_blank" class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                       <?php } else if($value['ctype']==6){?>
                                          <a target="_blank" class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                       <?php } else{ ?>
                                          <a target="_blank" class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>" style="background: <?=$value['color_code_btn'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
                                       <?php } } ?>
                                    </div>
                                 </div>
                              <?php } ?>
                              <?php if($key==7 || (($last_item<8) && ($key+1)==$last_item) && ($last_item>=5)){?>
                              </div>
                           <?php } } ?>
                           <!--product right end-->
