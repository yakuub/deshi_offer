<?php $start2=1;$start3=1; $last_item=count($recent_offer_list); foreach ($top_one_category_list as $key => $value) { ?>
  <?php if($key==0){?> 
    <div class="col-md-4 product_wrap_left">
    <?php } ?>
    <?php if($key==0 ||$key==1){?>
      <div class="col-md-12 product_each_wrapper">
        <div class="col-md-12 product excl-product">
          <div class="product_head col-md-12">
            <div class="tag" style="background: $value['color_code'];?>"><?=$value['offer_type_tilte'];?></div>
            <?php if ($this->session->userdata('type')==3 &&in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>


                     <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart" style="font-size:24px;color: red;"></i>

                     </a>

                  <?php } else if($this->session->userdata('type')==3 && !in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>

                    <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>


                    <?php } else { ?>
                     <a class="favourite" data-toggle="modal" data-target="#login_modal" href="javaScript:void(0)" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>
              
                  <?php } ?>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-6 product_img col-xs-6">
            <a href="offer/offer_details/<?=$value['profile_name'];?>"><img src="uploads/offer/<?=$value['offer_featured_file'];?>" class="img-responsive">
            </a>
            <?php if($value['discount']>0){?>
              <div class="percentage"><?=$value['discount'];?></div>
            <?php } ?>
          </div>
          <div class="col-md-6 product_description col-xs-6">
            <p class="product_title"><?=$value['offer_title'];?>
          </p>

          <p class="offr_tag"><i class="fa fa-tags offr_fa"></i>
            <?php if($value['offer_store_type']==0){echo "Store Offer";}else{echo "Online Offer";}?></p> <!--<span class="offr_tag"><i class="fa fa-map-marker rv_icon"></i> Gulshan</span> -->
            <p  class="offr_usd"><i class="fa  fa-user-plus offr_fa"></i> <span class="offr_usd_c"><?=$value['total_used'];?> </span> Used Already</p>
            <?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($value['edate']);
            $diff = $date2 - $date1;
            $day=round($diff / 86400);?>
            <?php if($day>0){?>
              <p class="expire">Expire in <?=$day;?> Days.</p>
            <?php } else { ?>
              <p class="expire">Expire in Today</p>
            <?php } ?>
            <?php if($value['ctype']==4){?>
              <a target="_blank" onclick="deal_activate('<?=$value['main_off_id'];?>')" class="offer_btn" href="<?=$value['ref_link'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } else if($value['ctype']==5){?>
              <a target="_blank" onclick="deal_activate('<?=$value['main_off_id'];?>')" class="offer_btn" href="<?=$value['utm_link'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } else{ ?>
              <a class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } ?>
            <div class="clearfix"></div>
          </div>
          <!-- <div class="amazon"><img src="front_assets/images/amazon.png" alt=""></div> -->
        </div>
      </div>
    <?php } ?>
    <?php if($key==2 || (($last_item<2) && ($key+1)==$last_item)) {?>
    </div>
  <?php } ?>

  <!--product left end-->

  <?php if($key==2){ $start2=0;?>
    <div class="col-md-4 product_special_wrap">
    <?php } ?>
    <?php if($key==2){?>
      <div class="col-md-12 product_special product_each_wrapper">
        <div class="col-md-12 product">
          <div class="product_head col-md-12">
            <div class="tag" style="background: $value['color_code'];?>"><?=$value['offer_type_tilte'];?></div>
            
            <?php if ($this->session->userdata('type')==3 &&in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>


                     <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart" style="font-size:24px;color: red;"></i>

                     </a>

                  <?php } else if($this->session->userdata('type')==3 && !in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>

                    <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>


                    <?php } else { ?>
                     <a class="favourite" data-toggle="modal" data-target="#login_modal" href="javaScript:void(0)" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>
              
                  <?php } ?>
            <div class="clearfix"></div>
          </div>
          <p class="product_title"><?=$value['offer_title'];?></p>
          <div class="col-md-12 product_img col-xs-6">
            <img src="uploads/offer/<?=$value['offer_featured_file'];?>" class="img-responsive">
            <?php if($value['discount']>0){?>
              <div class="percentage"><?=$value['discount'];?></div>
            <?php } ?>
          </div>
          <div class="col-md-12 product_description col-xs-6">
            <p class="offr_tag"><i class="fa fa-tags offr_fa"></i> Store Offer</p> <!--<span class="offr_tag"><i class="fa fa-map-marker rv_icon"></i> Gulshan</span> -->
            <p  class="offr_usd"><i class="fa  fa-user-plus offr_fa"></i> <span class="offr_usd_c"><?=$value['total_used'];?> </span> Used Already</p>
            <?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($value['edate']);
            $diff = $date2 - $date1;
            $day=round($diff / 86400);?>
            <?php if($day>0){?>
              <p class="expire">Expire in <?=$day;?> Days.</p>
            <?php } else { ?>
              <p class="expire">Expire in Today</p>
            <?php } ?>
            <?php if($value['ctype']==4){?>
              <a target="_blank" onclick="deal_activate('<?=$value['main_off_id'];?>')" class="offer_btn" href="<?=$value['ref_link'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } else if($value['ctype']==5){?>
              <a target="_blank" onclick="deal_activate('<?=$value['main_off_id'];?>')" class="offer_btn" href="<?=$value['utm_link'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } else{ ?>
              <a class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } ?>
            <div class="clearfix"></div>
          </div>
          <div class="amazon"><img src="front_assets/images/amazon.png" alt=""></div>
        </div>
      </div>
    <?php } ?>

    <?php if($key==2 || (($last_item<3) && ($key+1)==$last_item) && ($last_item>=2)){?>
    </div>
  <?php } ?>

  <!--product special end-->

  <?php if($key==3){?>
    <div class="col-md-4 product_wrap_right">
    <?php } ?>
    <?php if($key==3 ||$key==4){?>
      <div class="col-md-12 product_each_wrapper">
        <div class="col-md-12 product excl-product">
          <div class="product_head col-md-12">
            <div class="tag" style="background: $value['color_code'];?>"><?=$value['offer_type_tilte'];?></div>
            <?php if ($this->session->userdata('type')==3 &&in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>


                     <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart" style="font-size:24px;color: red;"></i>

                     </a>

                  <?php } else if($this->session->userdata('type')==3 && !in_array($value['main_off_id'], explode(',', $user_details[0]['fav_product']))) { ?>

                    <a class="favourite" href="offer/save_offer/<?=$value['profile_name'];?>" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>


                    <?php } else { ?>
                     <a class="favourite" data-toggle="modal" data-target="#login_modal" href="javaScript:void(0)" id="favourite_<?=$value['main_off_id'];?>"><i class="fa fa-heart-o" style="font-size:24px"></i>
              
                  <?php } ?>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-6 product_img col-xs-6">
            <a href="offer/offer_details/<?=$value['profile_name'];?>"><img src="uploads/offer/<?=$value['offer_featured_file'];?>" class="img-responsive">
            </a>
            <?php if($value['discount']>0){?>
              <div class="percentage"><?=$value['discount'];?></div>
            <?php } ?>
          </div>
          <div class="col-md-6 product_description col-xs-6">
            <p class="product_title"><?=$value['offer_title'];?>
          </p>

          <p class="offr_tag"><i class="fa fa-tags offr_fa"></i>
            <?php if($value['offer_store_type']==0){echo "Store Offer";}else{echo "Online Offer";}?></p> <!--<span class="offr_tag"><i class="fa fa-map-marker rv_icon"></i> Gulshan</span> -->
            <p  class="offr_usd"><i class="fa  fa-user-plus offr_fa"></i> <span class="offr_usd_c"><?=$value['total_used'];?> </span> Used Already</p>
            <?php $date1=strtotime(date('Y-m-d'));$date2=strtotime($value['edate']);
            $diff = $date2 - $date1;
            $day=round($diff / 86400);?>
            <?php if($day>0){?>
              <p class="expire">Expire in <?=$day;?> Days.</p>
            <?php } else { ?>
              <p class="expire">Expire in Today</p>
            <?php } ?>
            <?php if($value['ctype']==4){?>
              <a target="_blank" onclick="deal_activate('<?=$value['main_off_id'];?>')" class="offer_btn" href="<?=$value['ref_link'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } else if($value['ctype']==5){?>
              <a target="_blank" onclick="deal_activate('<?=$value['main_off_id'];?>')" class="offer_btn" href="<?=$value['utm_link'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } else{ ?>
              <a class="offer_btn" href="offer/offer_details/<?=$value['profile_name'];?>"><span><img src="front_assets/images/btnimg.svg" class="offer_btn_icon"></span><span class="offer_btn_icon_gap">|</span> <?=$value['string_name'];?></a>
            <?php } ?>
            <div class="clearfix"></div>
          </div>
          <!-- <div class="amazon"><img src="front_assets/images/amazon.png" alt=""></div> -->
        </div>
      </div>
    <?php } ?>

    <?php if($key==4 || (($last_item<5) && ($key+1)==$last_item) && ($last_item>=3)){?>
    </div>
  <?php } } ?>

