<?php $this->load->view('front/headlink');?>
<style>
  .redBackground{color:red}
  .carousel-inner > .item > a > img
  {
    height: 289px !important;  
  }  
</style>
<body id="test">
  <?php $this->load->view('front/head_nav');?>
  <div class="container-fluid floating_nav_wrap">

    <div class="container">
      <div class="floating_nav">
        <!-- <div class="top"></div> -->
        <div class="scroll_nav">
          <a href="#stores">
            <img src="front_assets/images/resource/home/cart2.svg" alt="" class="img-responsive" id="cat_imgt">
            <div class="pop"><p>Featured Stores</p></div>
          </a>
        </div>

        <div class="scroll_nav">
          <a href="#cat_sec_1">
            <img src="front_assets/images/resource/home/fnl2.svg" alt="" class="img-responsive" id="cat_imgt">
            <div class="pop"><p><?=$page_category[0]['catagory_title'];?></p></div>
          </a>
        </div>

        <div class="scroll_nav">
          <a href="#cat_sec_2">
            <img src="front_assets/images/resource/home/mng.svg" alt="" class="img-responsive" id="cat_imgt">
            <div class="pop"><p><?=$page_category[1]['catagory_title'];?></p></div>
          </a>
        </div>
        <div class="scroll_nav">
          <a href="#cat_sec_3">
            <img src="front_assets/images/resource/home/htoff.svg" alt="" class="img-responsive" id="cat_imgt">
            <div class="pop"><p><?=$page_category[2]['catagory_title'];?></p></div>
          </a>
        </div>
        <div class="scroll_nav">
          <a href="#cat_sec_4">
            <img src="front_assets/images/resource/home/enh.svg" alt="" class="img-responsive" id="cat_imgt">
            <div class="pop"><p><?=$page_category[3]['catagory_title'];?></p></div>
          </a>
        </div>
        <div class="scroll_nav">
          <a href="#cat_sec_5">
            <img src="front_assets/images/resource/home/fnr.svg" alt="" class="img-responsive" id="cat_imgt">
            <div class="pop"><p><?=$page_category[4]['catagory_title'];?></p></div>
          </a>
        </div>
      </div>
    </div>
  </div> 
  <?php $this->load->view('front/mega_menu');?>

  <div id="banner" class="container-fluid banner_wrapper" style="height: 90px;">

    <div class="container banner">
      <div class="slider col-sm-8">
        <div id="myCarousel" class="carousel carousel-fade slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <?php foreach ($slider_list as $key => $value) {?>
              <li style="width: 12.5%" data-target="#myCarousel" data-slide-to="<?=$key;?>" <?php if($key==0){ echo 'class="active"';}?>> <?=word_limiter($value['slidertitle'],4);?></li>
            <?php } ?>
              <!-- <li data-target="#myCarousel" data-slide-to="1">Ajkerdeal up 60% Sale OFF</li>
              <li data-target="#myCarousel" data-slide-to="2">Bagdoom Special Sale Offer</li>
              <li data-target="#myCarousel" data-slide-to="3">bKash 10% Cash Back</li>
              <li data-target="#myCarousel" data-slide-to="4">Oppo New Selfie phone</li>
              <li data-target="#myCarousel" data-slide-to="5">bKash 10% Cash Back</li>
              <li data-target="#myCarousel" data-slide-to="6">Bagdoom Special Sale Offer</li>
              <li data-target="#myCarousel" data-slide-to="7">Ajkerdeal up 60% Sale OFF</li> -->
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <?php foreach ($slider_list as $key => $value) {?>
                <div class="item <?php if($key==0){echo 'active';}?>">
                  <a href="<?=$value['slider_link']?>"><img height="100%" width="100%" src="uploads/slider/<?=$value['img'];?>" alt="Chania"></a>
                </div>
              <?php } ?>

              
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
        <!--  SLIDER CLASS CLOSED HERE -->

        <div class="col-sm-4 banner_img">
          <div class="col-sm-12" style="margin-bottom: 0;">
            <img src="uploads/paid_banner/<?=$slider_advertise[0]['advertise_image'];?>" alt="" class="img-responsive">
          </div>
        </div>
        <!--  BANNER_IMG CLASS CLOSED HERE -->
      </div>
      <!--  BANNER CLASS CLOSED HERE -->
    </div>
    <!--  BANNER_WRAPPER CLASS CLOSED HERE -->
    <?php if(count($feature_store)>0){?>
      <div id="stores" class="container-fluid stores_wrapper">
        <div class="container stores">
          <div class="col-xs-12 heading">
            <div class="heading_icon">
              <img src="front_assets/images/resource/home/cart2.svg" alt="" class="img-responsive" id="cat_img">
            </div>
            <p class="col-sm-6 col-xs-6 heading_left">Featured Stores</p>
            <p class="col-sm-6 col-xs-6 text-right text-14"><a href="store">SHOW ALL </a></p>
          </div>
          <div class="clearfix"></div>

          <!--  HEADING CLASS CLOSED HERE -->
<!-- 
          <div class="row">
            <div class="store-carousel">
             <div class="carousel-wrap">
               <div class="owl-carousel owl-theme">
                <div class="item"> 
                  <?php foreach ($feature_store as $key => $value) {?>
                    <div class="col-sm-2 col-xs-6 stores_brand_img">
                      <div class="col-sm-12">
                        <img src="uploads/user/<?=$value['profile_pic'];?>" alt="" class="img-responsive">
                        <a target="_deshioffer" href="store/offer/<?=$value['name'];?>" class="stores_brand_img_overlay"><?=$value['total_post'];?> offers</a>
                      </div>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div> -->

        <div class="clearfix"></div><br>
        <div class="row store_sec cat_pop_head">
          <?php foreach ($feature_store as $key => $value) {?>
            <div class="col-md-2 col-sm-3 col-xs-6 ">
              <a class="marchant_img col-md-12" href="store/offer/<?=$value['name'];?>">
                <img style="width: 100%; height: 100%" src="uploads/user/<?=$value['profile_pic'];?>" class="img-responsive" alt="">
                <div class="store_overlay"><?=$value['total_post'];?> Offers</div>
              </a>
            </div>
          <?php } ?>

          <div class="clearfix"></div>
        </div>
        <!--  STORES_BRAND CLASS CLOSED HERE -->

      </div>
      <!--  STORES CLASS CLOSED HERE -->
    </div>
  <?php } ?>
  <!--  STORES_WRAPPER CLASS CLOSED HERE -->


  <div class="container-fluid">
    <div id="exclusives" class="container">
      <div class="col-md-12 product_section">
        <div class="tab_option_wrap">
          <div class="col-md-4 tab_label_btn tab_option col-xs-4" tab-id="tab_1">
            <div class="col-md-12 ft14">Hot Offers<a href="category_offer.html" class="tab_link">≡</a>
            </div>
          </div>
          <div class="col-md-4 tab_label_btn tab_option col-xs-4" tab-id="tab_2">
            <div class="col-md-12 ft14 tab_option_active">New Offers<a href="category_offer.html" class="tab_link">≡</a>
            </div>
          </div>
          <div class="col-md-4 tab_label_btn tab_option col-xs-4" tab-id="tab_3">
            <div class="col-md-12 ft14">Recent Offers<a href="category_offer.html" class="tab_link">≡</a>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="product_wrap col-md-12 tg_tab" id="tab_1">
          <?php $data['offer_list']=$hot_offer_list;?>
          <?php $this->load->view('include/promo_offer',$data);?>
        </div>

        <!--product wrap end-->

        <div class="product_wrap col-md-12 tg_tab tg_tab_show" id="tab_2">
          <?php $data['offer_list']=$new_offer_list;?>
          <?php $this->load->view('include/promo_offer',$data);?>
        </div>
        <!--product wrap end-->

        <div class="product_wrap col-md-12 tg_tab" id="tab_3">
          <?php $data['offer_list']=$recent_offer_list;?>
          <?php $this->load->view('include/promo_offer',$data);?>
        </div>
        <!--product wrap end-->
      </div>
      <!--product section end-->

        <!-- <span class="invisible_section">
      <div class="col-md-12 mail_subscribe">
        <div class="col-md-6 subscribe_title"><img src="front_assets/images/subscribe.svg" alt="subscribe to Deshioffer">
          <p>Don't Miss any Offer Subscribe to Deshioffer</p>
          <div class="clearfix"></div>
        </div>
        <form class="col-md-6 subscribe_input" action=""><input type="text" id="subscribe" placeholder="Enter your Email to subscribe"><input value="▶" type="submit"></form>
        <div class="clearfix"></div>
      </div> </span><!--subscribe end-->
      <!--banner-->
        <!-- <div class="col-md-12 f_w_ad">
        <div class="col-sm-12">
          <a target="_blank" href="#"><img src="front_assets/images/ad_banner.png" alt="" class="img-responsive"></a>
        </div>
      <div class="clearfix"></div>
    </div> -->
    <!--banner end-->
    <!--fasion and style-->
    <?php foreach ($page_category as $key => $value) {?>
      <?php $data['category_offer']=${ 'top_' .$key.'_category_list'};?>

      <div id="cat_sec_<?=($key+1);?>" class="col-md-12 product_section">
        <div class="section_title"> <img src="front_assets/images/resource/home/fnl2.svg" alt="" class="img-responsive" id="cat_img"><span><?=$value['catagory_title'];?></span>
          <div class="clearfix"></div>
        </div>
        <div class="product_wrap col-md-12">


          <?php $this->load->view('include/category_offer', $data);?>
        </div>
        <!--product wrap end-->
        <div class="col-md-12 fashion_footer">
          <div class="col-md-4 tab_label_btn left">
            <a target="_deshioffer" href="category/offer/<?=$value['catagory_title'];?>" class="col-md-12"><span class="tab_link">≡</span>SHOW ALL <?=$value['catagory_title'];?> OFFERS</a>
          </div>
          <div class="col-md-8 subscribe" id="cat_sub_modal" onclick="get_subscribe_modal('<?=$value['catagory_id'];?>')">
            <img src="front_assets/images/subscribe.svg" alt="">
            <p>Subscribe to email alerts for new Offers and Deals</p>

          </div>

        </div>
      </div>

    <?php  } ?>



    <!--fasion and style end-->
    <!--banner-->

    <!--banner end-->
    <!--health and food-->

    <!--health and food end-->
    <!--banner-->

    <!--banner end-->
    <!--travel and tourism-->

    <!--banner end-->
    <!--ANOTHER SECTION-->

    <!--ANOTHER SECTIONend-->
    <!--banner-->

    <!--banner end-->
    <!--ANOTHER SECTION-->

    <!--ANOTHER SECTIONend-->
  </div>
     <!--  <div class="container-fluid number_wrapper">
        <div class="container number">
          <div class="col-sm-4 col-xs-4">
            <div class="col-sm-2 col-xs-12 deal_img_wrap">
              <div class="deal_img center-block">
                <img class="img-responsive" src="front_assets/images/offerbx.svg" alt="">
              </div>
            </div>
            <div class="col-sm-10 col-xs-12 deal_img_wrap">
              <p class="count">2532</p>
              <p>Offers <span class="invisible_section">And Deals </span></p>
            </div>
          </div>
          <div class="col-sm-4 col-xs-4">
            <div class="col-sm-2 col-xs-12 deal_img_wrap">
              <div class="deal_img center-block">
                <img class="img-responsive" src="front_assets/images/merchantbx.svg" alt="">
              </div>
            </div>
            <div class="col-sm-10 col-xs-12 deal_img_wrap">
              <p class="count">5361</p>
              <p>Merchants <span class="invisible_section">Associated</span></p>
            </div>
          </div>
          <div class="col-sm-4 col-xs-4">
            <div class="col-sm-2 col-xs-12 deal_img_wrap">
              <div class="deal_img center-block">
                <img class="img-responsive" src="front_assets/images/sofferbx.svg" alt="">
              </div>
            </div>
            <div class="col-sm-10 col-xs-12 deal_img_wrap">
              <p class="count">8952</p>
              <p>Exclusive <span class="invisible_section"> Offers For You</span> </p>
            </div>
          </div>
        </div> -->
        <!--  NUMBER CLASS CLOSED HERE -->
      </div>
      <!--  NUMBER_WRAPPER CLASS CLOSED HERE -->


      <!-- SUBSCRIPTION MODAL FOR NEWSLETTER STARTS -->
      

      <div class="modal fade" id="subscribe" class="subscribe" role="dialog">

      </div>

      <!-- SUBSCRIPTION MODAL FOR NEWSLETTER ENDS -->



      <!--Social links end-->
    </div>



    
    <?php $this->load->view('front/footer');?>
    <?php $this->load->view('front/footerlink');?>

    <script>
      $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        navText: [
        "<i class='fa fa-caret-left featured-left'></i>",
        "<i class='fa fa-caret-right featured-right'></i>"
        ],
        autoplay: false,
        autoplayHoverPause: true,
        responsive: {
          0: {
            items: 1
          },
          600: {
            items: 3
          },
          1000: {
            items: 6
          }
        }
      })

    </script>

    <script>
      $(".pop_subscribe_form span").click(function() {
        $(".pop_subscribe").fadeOut(300);
      });
      $(".pop_bottom span").click(function() {
        $(".pop_bottom_wrapper").fadeOut(300);
      });
      function search_ctrl() {
        var width = $(window).width();
        if (width < 767) {
          $(".form_wrap").css("display", "none");
          $(".search_btn").attr("value", "Go");
        } else {
          $(".form_wrap").css("display", "block");
          $(".search_btn").attr("value", "Show Offers");
        }
      }
      $(".src_btn").stop().click(function() {
        if ($(window).width() < 767) {
          $(this).siblings(".form_wrap").slideToggle(300);
        } else {
          $(this).siblings(".form_wrap").slideDown(0);
        }
      });
      search_ctrl();
      $(window).on("resize", search_ctrl);
      $(".post_offer_btn, .pop_up_wrap").hover(function() {
        $(".pop_up_wrap").stop().fadeIn(200);
      }, function() {
        $(".pop_up_wrap").fadeOut(200);
      });
    </script>

    
    <script>
      $(".form-fg-pass").css("display","none");
      $(".recover_pass").click(function () {
        $(".form-signin").css("display","none")
        $(".form-fg-pass").css("display","block")
        // $("#").show();   
      });

      $(".b2login").click(function () {
        $(".form-signin").css("display","block")
        $(".form-fg-pass").css("display","none")
      });
    </script>



    <script>
      function add_favourite(offer_id) 
      {
        var login_id="<?=$this->session->userdata('login_id');?>"

        if(login_id=='')
        {
          $("#login_modal").modal('show');
          
        }

        else{
          $.ajax({
            url: "<?php echo site_url('user/add_favourite');?>",
            type: "post",
            data: {offer_id:offer_id,login_id:login_id},
            success: function(msg)
            {
                      // alert(msg);
                      console.log(msg);
                      $('#favourite_'+offer_id).notify("Added To Favourite", "success");


                    }      
                  });
        }  



      }
    </script>

    <script>
      function get_subscribe_modal(catagory_id)
      {

       console.log(catagory_id);
       $.ajax({
        url: "<?php echo site_url('home/category_subscription_modal');?>",
        type: "post",
        data: {catagory_id:catagory_id},
        success: function(msg)
        {
                      //console.log(msg);
                      $('#subscribe').modal('show');
                      $('#subscribe').html(msg);


                    }      
                  });  
     }
   </script>

   <script>
    function subscribed_catagory_ajax() 
    {

     var email=$("#email").val();

     var catagory_id = $('#catagory_id_modal').val();

     $("#err_div").hide();
     $("#s_div").hide();

     if(email=='')
     {
      $("#err_div").show();
      $("#m_msg").html('Please Enter your Email');

      return false;
    }

      // console.log(catagory_id);
        // ok=false;
        $("#loading_img").show();
        $.ajax({
          url: "<?php echo site_url('home/add_subscribed_catagory');?>",
          type: "post",
          data: {email:email,catagory_id:catagory_id,},
          success: function(msg)
          {

            if(msg==0)
            {
              $("#err_div").show();
              $("#m_msg").html('Your email not exist in our system.');
                      //email not exist
                    } 
                    else if(msg==1)
                    {
                      $("#err_div").show();
                      $("#m_msg").html('Your have already subscribed this category.');
                    }
                    else
                    {
                      $("#s_div").show();
                      $("#s_msg").html('SUBSCRIPTION successfull.');
                    }
                    $("#loading_img").hide();
                  }      
                });  

      }
    </script>



    <script>

      $('.favourite').on('click',function(){
        $(this).toggleClass('redBackground');
      });

    </script>


  </body>

  </html>
