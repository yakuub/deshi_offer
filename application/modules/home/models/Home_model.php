<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Home_model extends CI_Model
{
    ////// Basic Model Function Starts ///////
    public function insert($table_name,$data)
    {
     $this->db->insert($table_name, $data);
 }

 public function insert_ret($tablename, $tabledata)
 {
    $this->db->insert($tablename, $tabledata);
    return $this->db->insert_id();
}

public function update_function($columnName, $columnVal, $tableName, $data)
{
    $this->db->where($columnName, $columnVal);
    $this->db->update($tableName, $data);
}

public function update_function2($condition, $tableName, $data)
{
 $where = '( ' . $condition . ' )';
 $this->db->where($where);
 $this->db->update($tableName, $data);
}

public function delete_function_cond($tableName, $cond)
{
    $where = '( ' . $cond . ' )';
    $this->db->where($where);
    $this->db->delete($tableName);
}
public function delete_function($tableName, $columnName, $columnVal)
{
    $this->db->where($columnName, $columnVal);
    $this->db->delete($tableName);
}

public function select_all($table_name)
{
   $this->db->select('*');
   $this->db->from($table_name);
   $query = $this->db->get();
   $result = $query->result_array();
   return $result;
}

public function select_all_decending($table_name)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $this->db->order_by('created_at','DESC');
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

public function select_all_acending($table_name,$col_name)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $this->db->order_by($col_name,'ASC');
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

public function select_all_condition_order($table_name,$condition,$col_name,$order_val)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by($col_name,$order_val);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

public function select_condition_decending($table_name,$condition)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('created_at','DESC');
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


public function select_condition_decending_with_limit($table_name,$condition,$limit)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('updated_at','DESC');
    $this->db->limit($limit);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

public function select_condition_acending_with_limit($table_name,$condition,$order_col,$limit)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by($order_col,'ASC');
    $this->db->limit($limit);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


public function select_condition_random_with_limit($table_name,$condition,$limit)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('updated_at','RANDOM');
    $this->db->limit($limit);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


public function select_condition_random($table_name,$condition)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('updated_at','RANDOM');
    // $this->db->limit($limit);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


public function select_offer_random_with_limit($table_name,$condition,$limit)
{
    $this->db->select('*');
    $this->db->from($table_name);
    $this->db->join('offer_type','offer_type.offer_id=main_offer.offer_title_type','LEFT');
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('updated_at','RANDOM');
    $this->db->limit($limit);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


public function select_with_where($selector, $condition, $tablename)
{
   $this->db->select($selector);
   $this->db->from($tablename);
   $where = '(' . $condition . ')';
   $this->db->where($where);
   $result = $this->db->get();
   return $result->result_array();
}

public function select_with_where_decending($selector, $condition, $tablename)
{
    $this->db->select($selector);
    $this->db->from($tablename);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('created_at','DESC');
    $result = $this->db->get();
    return $result->result_array();
} 

public function select_with_where_decending2($selector, $condition, $tablename,$order_by_col)
{
    $this->db->select($selector);
    $this->db->from($tablename);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by($order_by_col,'DESC');
    $result = $this->db->get();
    return $result->result_array();
} 

public function select_where_join($selector,$table_name,$join_table,$join_condition,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition);
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

public function get_address_details($condition)
{
    $this->db->select('*,divisions.name as div_name,districts.name as dist_name');
    $this->db->from('marchant_details md');
    $this->db->join('area','area.area_id=md.area_id','LEFT');
    $this->db->join('divisions','divisions.id=md.division_id','LEFT');
    $this->db->join('districts','districts.id=md.district_id','LEFT');
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

public function select_where_left_join($selector,$table_name,$join_table,$join_condition,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'left');
    $where = '(' . $condition . ')';
    $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
    $result=$this->db->get();
    return $result->result_array();
}

    ////// Basic Model Function End ///////

public function get_shop_category($condition)
{
    $this->db->select('user_login.name,user_login.username,user_login.profile_pic,mf.catagories,user_login.loginid');
    $this->db->from('main_offer mf');
    $this->db->join('user_login','user_login.loginid=mf.offer_from');
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->group_by('mf.offer_from');
    $this->db->limit('10');
    $result=$this->db->get();
    return $result->result_array();
}


public function columns($database, $table)
{
        //$query = "SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_DEFAULT, COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS  WHERE table_name = '$table'AND table_schema = '$database'";  
    $query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS 
    WHERE table_name = '$table'
    AND table_schema = '$database'";    
    $result = $this->db->query($query) or die ("Schema Query Failed"); 
    $result=$result->result_array();
    return $result;
}

// For access menu and submenu
public function get_menu_list($login_id)
{
    $this->db->select('*');
    $this->db->from('menu_for_admin');
    $this->db->where('menu_for_admin.user_id',$login_id); 
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

public function get_sub_menu_list($login_id)
{
    $this->db->select('*');
    $this->db->from('sub_menu_for_admin');
    $this->db->where('sub_menu_for_admin.user_id',$login_id); 
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}




// For access menu and submenu

public function get_due_list($selector,$table_name,$join_table,$join_condition,$join_table2,$join_condition2,$group_by,$condition)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,'LEFT');
    $this->db->join($join_table2,$join_condition2,'LEFT');
    $this->db->group_by($group_by); 
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

// For BUY and SELL details
public function sell_details($sell_id)
{
    $this->db->select('*,sell.created_at as sell_date');
    $this->db->from('sell');
    $this->db->join('sell_details','sell_details.sell_id=sell.sell_id','LEFT');
    $this->db->join('customer','sell.cust_id=customer.cust_id','LEFT');
    $this->db->join('product','sell_details.p_id=product.p_id','LEFT');
    $this->db->join('unit','product.p_unit_id=unit.unit_id','LEFT');
    $this->db->where('sell.sell_id',$sell_id); 
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}


public function buy_details($buy_id)
{
    $this->db->select('*,buy.created_at as buy_date');
    $this->db->from('buy');
    $this->db->join('buy_details','buy_details.buy_id=buy.buy_id','LEFT');
    $this->db->join('supplier','buy.supp_id=supplier.supp_id','LEFT');
    $this->db->join('product','buy_details.p_id=product.p_id','LEFT');
    $this->db->join('unit','product.p_unit_id=unit.unit_id','LEFT');
    $this->db->where('buy.buy_id',$buy_id); 
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

// For BUY and SELL details
// For header nav notification 
public function check_product_alert()
{
    $this->db->select('*');
    $this->db->from('product');
    $this->db->where('p_alert_qty > p_qty'); 
    $query = $this->db->get();
    $result = $query->result_array();
    return $result;
}

// For header nav notification End

// Index page bar chart
public function get_monthly_report_for_chart($selector,$table_name,$group_by)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->group_by($group_by);  
    $result=$this->db->get();
    return $result->result_array();
}


// For daily Report
public function get_daily_report_info($selector,$table_name,$join_table,$join_condition,$join_type,$like_col,$like_val)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,$join_type);
        // $where = '(' . $condition . ')';
        // $this->db->where($where);
    $this->db->like($like_col, $like_val, 'after');  
    $result=$this->db->get();
    return $result->result_array();
}

// For monthly and yearly report
public function get_monthly_report_info($selector,$table_name,$join_table,$join_condition,$join_type,$like_col,$like_val,$like_type,$group_by_val)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,$join_type);
        // $where = '(' . $condition . ')';
        // $this->db->where($where);
    $this->db->like($like_col, $like_val, $like_type);  
    $this->db->group_by($group_by_val);  
    $result=$this->db->get();
    return $result->result_array();
}

public function get_report_info_group($selector,$table_name,$join_table,$join_condition,$join_type,$like_col,$like_val,$like_type,$group_name)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->join($join_table,$join_condition,$join_type);
        // $where = '(' . $condition . ')';
        // $this->db->where($where);
    $this->db->like($like_col, $like_val, $like_type);  
    $this->db->group_by($group_name);  
    $result=$this->db->get();
    return $result->result_array();
}

public function count_amount($selector,$table_name,$like_col, $like_val, $like_type,$group_name)
{
    $this->db->select($selector);
    $this->db->from($table_name);
    $this->db->like($like_col, $like_val, $like_type);
    $this->db->group_by($group_name);   
    $result=$this->db->get();
    return $result->result_array();
}

public function order_by_last_row($selector, $condition, $tablename)
{
    $this->db->select($selector);
    $this->db->from($tablename);
    $where = '(' . $condition . ')';
    $this->db->where($where);
    $this->db->order_by('id',"desc");
    $this->db->limit(1);
    $result = $this->db->get();
    return $result->result_array();
}

public function subscribed_catagory($email,$catagory_id)
{
  $query = $this->db->query('SELECT * FROM user_login WHERE email="'.$email.'" AND FIND_IN_SET('.$catagory_id.',user_login.subscribed_category)');

  return $query->result_array();
}

public function unreg_email_subscribed_catagory($email,$catagory_id)
{
  $query = $this->db->query('SELECT * FROM subscription WHERE email="'.$email.'" AND FIND_IN_SET('.$catagory_id.',subscription.subscribed_category)');

  return $query->result_array();
}

public function subscribed_store($email,$loginid)
{
  $query = $this->db->query('SELECT * FROM user_login WHERE email="'.$email.'" AND FIND_IN_SET('.$loginid.',user_login.subscribed_store)');

  return $query->result_array();
}

public function unreg_email_subscribed_store($email,$loginid)
{
  $query = $this->db->query('SELECT * FROM subscription WHERE email="'.$email.'" AND FIND_IN_SET('.$loginid.',subscription.subscribed_store)');

  return $query->result_array();
}

public function sms_mobile_number($offer_id,$number)
{
  $query = $this->db->query('SELECT * FROM offer_sms WHERE offer_id='.$offer_id.' AND FIND_IN_SET('.$number.',offer_sms.mobile_number)');

  return $query->result_array();
}

}
?>