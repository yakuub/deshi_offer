<?php $this->load->view('front/headlink');?>
  <body>
        <!--Header Start-->
        <?php $this->load->view('front/head_nav');?>
        <!--End of Header Area-->
          
        <!--About Area Start-->
        <div class="home-hello-info">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="f-title text-center">
              <h3 class="title text-uppercase">About Us</h3>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-7 col-sm-12 col-xs-12">
            <div class="about-page-cntent">
              <?=$header[0]['about_us'];?>
            </div>
          </div>
          <div class="col-md-5 col-sm-12 col-xs-12">
            <div class="img-element">
              <img alt="banner1" src="uploads/company/<?=$header[0]['about_us_image'];?>">
            </div>
          </div>
        </div>
      </div>
    </div>
        <!--End of ABout ARea-->
        <!--Our Team Start-->
    <!--<div class="home-our-team">-->
    <!--  <div class="container">-->
    <!--    <div class="row">-->
    <!--      <div class="col-md-12">-->
    <!--        <div class="f-title text-center">-->
    <!--          <h3 class="title text-uppercase">Meet the team</h3>-->
    <!--        </div>-->
    <!--      </div>-->
    <!--    </div>-->
    <!--    <div class="row">-->
    <!--      <?php foreach ($team_list as $key => $value) {?>-->
    <!--        <div class="col-md-3 col-sm-4">-->
    <!--          <div class="item-team text-center">-->
    <!--            <div class="team-info">-->
    <!--              <div class="team-img">-->
    <!--                <img width="250" height="250" alt="team1" class="img-responsive" src="uploads/team/<?=$value['image'];?>">-->
    <!--                <div class="mask">-->
    <!--                  <div class="mask-inner">-->
    <!--                    <a href="#"><i class="fa fa-facebook"></i></a>-->
    <!--                    <a href="#"><i class="fa fa-twitter"></i></a>-->
    <!--                  </div>-->
    <!--                </div>-->
    <!--              </div>-->
    <!--            </div>-->
    <!--            <h4><?=$value['name'];?></h4>-->
    <!--            <h5><?=$value['designation'];?></h5>-->
    <!--          </div>-->
    <!--        </div>-->
    <!--      <?php } ?>-->
          
    <!--    </div>-->
    <!--  </div>-->
    <!--</div>-->
        <!--End of Our Team--> 
        <?php $this->load->view('front/footer');?>
        <?php $this->load->view('front/footerlink');?>
      
    </body>
</html>