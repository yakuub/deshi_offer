<?php $this->load->view('front/header_link');?>
<body>
<div class="page"> 
  <!-- Header -->
  <?php $this->load->view('front/header');?>
  <!-- end header --> 
    <!-- Navbar -->
  <?php $this->load->view('front/nav');?>
  <!-- end nav -->  
 <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <ul>
          <li class="home"> <a title="Go to Home Page" href="home">Home</a><span>&mdash;›</span></li>
          <li class="home"> <a href="#" title="">Page</a><span>&mdash;›</span></li>
          <li class="category13"><strong>Privacy Policy</strong></li>
        </ul>
      </div>
    </div>
  </div>
  <!-- main-container -->
  <div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        <section class="col-main col-sm-9 wow bounceInUp animated">
          <div class="page-title">
            <h2>Privacy Policy</h2>
          </div>
          <div class="static-contain">
            <?=$header[0]['privacy_policy'];?>
          </div>
        </section>
        <aside class="col-right sidebar col-sm-3 wow bounceInUp animated">
          <?php $this->load->view('front/page_nav'); ?>
        </aside>
      </div>
    </div>
  </div>
  <!--End main-container --> 
  <?php $this->load->view('front/footer');?>
  <!-- End Footer --> 
</div>

<?php $this->load->view('front/footer_link');?>
</body>
</html>