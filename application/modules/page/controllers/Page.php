<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Page extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('home/home_model');
        $this->load->helper('inflector');
        $this->load->library('encrypt');
        


    }

    public function index()
    {
        $this->about_us();
    }

    public function about_us()
    {
       /// Common Date Start
            
            $data['active']='about_us';
            $data['header']=$this->home_model->select_with_where('*','id=1','company');
            // $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
            // $data['category']=$this->home_model->select_all_acending('category','name');
            // $data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
        // Common Data End
           // $data['team_list']=$this->home_model->select_all('team');
           
           $data['deals_offer']=$this->home_model->select_with_where_decending('*','offer_type=1 AND status=1','offer_highlights');

			$data['special_offer']=$this->home_model->order_by_last_row('*','offer_type=0 AND status=1','offer_highlights');

        $this->load->view('about_us',$data);
    }

    public function privacy_policy()
    {
       /// Common Date Start
            $data['active']='privacy_policy';
            $data['header']=$this->home_model->select_with_where('*','id=1','company');
            $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
            $data['category']=$this->home_model->select_all_acending('category','name');
            $data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
        // Common Data End
        $this->load->view('privacy_policy',$data);
    }

    public function terms_and_condition()
    {
       /// Common Date Start
            $area_retreive_id=$this->session->userdata('area_retreive_id');
            $data['active']='terms_and_condition';
            $data['header']=$this->home_model->select_with_where('*','id=1','company');
            $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
            $data['category']=$this->home_model->select_all_acending('category','name');
            $data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
        // Common Data End
        $this->load->view('terms_and_condition',$data);
    }

    
}
