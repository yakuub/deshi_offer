<?php $this->load->view('front/headlink');?>
  <body>
        <!--Header Start-->
        <?php $this->load->view('front/head_nav');?>
        <!--End of Header Area-->
          
        <!--Contact Us Area Start-->
        <div class="contact-us-area area-padding">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <div class="sidebar-content">
                            <div class="section-title no-margin"><h2>Denim Life BD Info</h2></div>
                            <div class="popular-tags">
                                <p><strong>Address :</strong> <?=$header[0]['address'];?></p>
                                <p><strong>Email :</strong> <?=$header[0]['email'];?></p>
                                <p><strong>Phone :</strong> <?=$header[0]['phone'];?></p>
                            </div>
                        </div>
                     
                    </div>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="contact-us-area">
                            <!-- google-map-area start -->
                            <div class="google-map-area">
                                <!--  Map Section -->
                                <div id="contacts" class="map-area">
                                    <div id="googleMap" style="width:100%;height:430px;"></div>
                                </div>
                            </div>
                            <!-- google-map-area end -->
              <!-- contact us form start -->
              <div class="contact-us-form">
                <div class="page-title">
                                    <h1>Checkout</h1>
                                </div>
                <div class="contact-form">
                  <span class="legend">Contact Information</span>
                  <form action="mail.php" method="post">
                    <div class="form-top">
                      <div class="form-group col-sm-6 col-md-6 col-lg-6">
                        <label>Name<span class="required" title="required">*</span></label>
                        <input name="name" type="text" class="form-control">
                      </div>
                      <div class="form-group col-sm-6 col-md-6 col-lg-6">
                        <label>Email<span class="required" title="required">*</span></label> 
                        <input name="email" type="email" class="form-control">
                      </div>                    
                      <div class="form-group col-sm-6 col-md-6 col-lg-6">
                        <label>Telephone<span class="required" title="required">*</span></label>
                        <input name="number" type="text" class="form-control">
                      </div>  
                      <div class="form-group col-sm-12 col-md-12 col-lg-12">
                        <label>Comment<span class="required" title="required">*</span></label>
                        <textarea name="message" class="yourmessage"></textarea>
                      </div>                        
                    </div>
                    <div class="submit-form form-group col-sm-12 submit-review">
                      <p class="floatright"><sup>*</sup> Required Fields</p>
                                            <div class="clearfix"></div>
                                            <button class="button floatright" type="submit"><span>Submit</span></button>
                    </div>
                  </form>
                </div>
              </div>
              <!-- contact us form end -->
            </div>          
          </div>
                </div>
            </div>
        </div>
        <!--End of Contact Us ARea-->     
        <?php $this->load->view('front/footer');?>
        <?php $this->load->view('front/footerlink');?>


        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuU_0_uLMnFM-2oWod_fzC0atPZj7dHlU"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script>
      function initialize() {
        var mapOptions = {
        zoom: 15,
        scrollwheel: false,
        center: new google.maps.LatLng(23.81033, 90.41252)
        };

        var map = new google.maps.Map(document.getElementById('googleMap'),
          mapOptions);


        var marker = new google.maps.Marker({
        position: map.getCenter(),
        animation:google.maps.Animation.BOUNCE,
        icon: '<?=base_url();?>front_assets/img/map-marker.png',
        map: map
        });

      }

      google.maps.event.addDomListener(window, 'load', initialize);
    </script> 
      
    </body>
</html>