<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contact_us extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('home/home_model');
        $this->load->helper('inflector');
        $this->load->library('encrypt');
        


    }

    public function index()
    {
        /// Common Date Start
            $data['active']='contact_us';
            $data['header']=$this->home_model->select_with_where('*','id=1','company');
            $data['brand']=$this->home_model->select_all_acending('brand','brand_name');
            $data['category']=$this->home_model->select_all_acending('category','name');
            $data['sub_category']=$this->home_model->select_all_acending('sub_category','sub_name');
        // Common Data End
    	$this->load->view('index',$data);
    }

    
}
