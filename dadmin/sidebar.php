        <div class="menu-mobile menu-activated-on-click color-scheme-dark">
          <div class="mm-logo-buttons-w">
            <a class="mm-logo" href="dashboard.php"><img src="img/logo.png"><span>Clean Admin</span></a>
            <div class="mm-buttons">
              <div class="content-panel-open">
                <div class="os-icon os-icon-grid-circles"></div>
              </div>
              <div class="mobile-menu-trigger">
                <div class="os-icon os-icon-hamburger-menu-1"></div>
              </div>
            </div>
          </div>
          <div class="menu-and-user">
            <div class="logged-user-w">
              <div class="avatar-w">
                <img alt="" src="img/avatar1.jpg">
              </div>
              <div class="logged-user-info-w">
                <div class="logged-user-name">
                 <?php echo $user_name?>
                </div>
                <div class="logged-user-role">
                  Administrator
                </div>
              </div>
            </div>
            <ul class="main-menu">
              <li class="has-menu">
                <a href="dashboard.php">
                  <div class="icon-w">
                    <div class="os-icon os-icon-window-content"></div>
                  </div>
                  <span>Dashboard</span></a>

              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
				  
                    <div class="os-icon os-icon-emoticon-smile"></div>
                  </div>
                  <span>Manage Admin</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="add_member.php?add_mem=1">Add Admin</a>
                  </li>
                  <li>
                    <a href="member_list.php?mem_list=1">Admin List</a>
                  </li>
                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-user-male-circle2"></div>
                  </div>
                  <span>Manage Merchant</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="add_member.php?add_mem=2">Add Merchant</a>
                  </li>
                  <li>
                    <a href="member_list.php?mem_list=2">Merchant List</a>
                  </li>
                </ul>
              </li> 
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-user-male-circle"></div>
                  </div>
                  <span>Manage User</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="add_member.php?add_mem=2">Add User</a>
                  </li>
                  <li>
                    <a href="member_list.php?mem_list=3">User List</a>
                  </li>
                </ul>
              </li> 
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-delivery-box-2"></div>
                  </div>
                  <span>Manage Area</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Add Division</a>
                  </li>
                  <li>
                    <a href="#">Add District</a>
                  </li>
                  <li>
                    <a href="#">Add Area</a>
                  </li>

                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-grid-squares"></div>
                  </div>
                  <span>Manage Category</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Add Category</a>
                  </li>
                  <li>
                    <a href="#">Category List</a>
                  </li>


                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-newspaper"></div>
                  </div>
                  <span>Manage Offer</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Add Offer Type</a>
                  </li>
                  <li>
                    <a href="#">Add Target People</a>
                  </li>
                  <li>
                    <a href="post_offer.php">Add New Offer</a>
                  </li>
                  <li>
                    <a href="#">Offer List</a>
                  </li>
                  <li>
                    <a href="#">Active Offer</a>
                  </li>
                  <li>
                    <a href="#">Upcoming Offer</a>
                  </li>
                  <li>
                    <a href="#">Expired Offer</a>
                  </li>
                  <li>
                    <a href="#">Deleted Offer</a>
                  </li>
                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-tasks-checked"></div>
                  </div>
                  <span>Boost</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Boost Offer</a>
                  </li>
                  <li>
                    <a href="#">Boost Collection</a>
                  </li>

                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-grid-squares"></div>
                  </div>
                  <span>Manage Subscriber</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Add New Subscriber</a>
                  </li>
                  <li>
                    <a href="#">Subscriber List</a>
                  </li>

                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-grid-squares"></div>
                  </div>
                  <span>Manage Promotional Mail</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Mail Subject</a>
                  </li>
                  <li>
                    <a href="#">Add Email Template</a>
                  </li> 
                  <li>
                    <a href="#">Send Mail</a>
                  </li>
                  <li>
                    <a href="#">Mail List</a>
                  </li>
                </ul>
              </li>	  
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-grid-squares"></div>
                  </div>
                  <span>Manage Promotional SMS</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">SMS Subject</a>
                  </li>
                  <li>
                    <a href="#">Add SMS Template</a>
                  </li> 
                  <li>
                    <a href="#">Send SMS</a>
                  </li>
                  <li>
                    <a href="#">SMS List</a>
                  </li>
                </ul>
              </li>	
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-grid-squares"></div>
                  </div>
                  <span>Manage Advertisement</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Advertise Title</a>
                  </li>
                  <li>
                    <a href="#">Set Advertise</a>
                  </li> 
                  <li>
                    <a href="#">Advertise List</a>
                  </li>

                </ul>
              </li>	
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-grid-squares"></div>
                  </div>
                  <span>Manage Support</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Add Support Title</a>
                  </li>
                  <li>
                    <a href="#">Query List</a>
                  </li> 


                </ul>
              </li>	
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-grid-squares"></div>
                  </div>
                  <span>Manage Web CMS</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Add New Data</a>
                  </li>
                  <li>
                    <a href="#">Page Content</a>
                  </li> 
                  <li>
                    <a href="#">Contact Request</a>
                  </li> 

                </ul>
              </li>	 
            </ul>
            <div class="mobile-menu-magic">
              <h4>
               Deshi-Offer Admin
              </h4>

            </div>
          </div>
        </div>
        <div class="desktop-menu menu-side-w menu-activated-on-click">
          <div class="logo-w">
            <a class="logo" href="#"><img src="img/logo.png"><span>Clean Admin</span></a>
          </div>
          <div class="menu-and-user">
            <div class="logged-user-w">
              <div class="avatar-w">
                <img alt="" src="img/avatar1.jpg">
              </div>
              <div class="logged-user-info-w">
                <div class="logged-user-name">
                    <?php echo $user_name?>
                </div>
                <div class="logged-user-role">
                  Administrator
                </div>
              </div>
              <div class="logged-user-menu">
                <div class="avatar-w">
                  <img alt="" src="img/avatar1.jpg">
                </div>
                <div class="logged-user-info-w">
                  <div class="logged-user-name">
                      <?php echo $user_name?>
                  </div>
                  <div class="logged-user-role">
                    Administrator
                  </div>
                </div>
                <div class="bg-icon">
                  <i class="os-icon os-icon-wallet-loaded"></i>
                </div>
                <ul>
                  <li>
                    <a href="#"><i class="os-icon os-icon-mail-01"></i><span>Incoming Mail</span></a>
                  </li>
                  <li>
                    <a href="users_profile_big.html"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
                  </li>
                  <li>
                    <a href="users_profile_small.html"><i class="os-icon os-icon-coins-4"></i><span>Billing Details</span></a>
                  </li>
                  <li>
                    <a href="#"><i class="os-icon os-icon-others-43"></i><span>Notifications</span></a>
                  </li>
                  <li>
                    <a href="logout.php"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                  </li>
                </ul>
              </div>
            </div>
            <ul class="main-menu">
              <li class="has-menu">
                <a href="dashboard.php">
                  <div class="icon-w">
                    <div class="os-icon os-icon-window-content"></div>
                  </div>
                  <span>Dashboard</span></a>

              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
				  
                    <div class="os-icon os-icon-emoticon-smile"></div>
                  </div>
                  <span>Manage Admin</span></a>
                <ul class="sub-menu">
                  <li>
                     <a href="add_member.php?add_mem=1">Add Admin</a>
                  </li>
                  <li>
                     <a href="member_list.php?mem_list=1">Admin List</a>
                  </li>
                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-user-male-circle2"></div>
                  </div>
                  <span>Manage Merchant</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="add_merchant.php">Add Merchant</a>
                  </li>
                  <li>
                    <a href="member_list.php?mem_list=2">Merchant List</a>
                  </li>
                </ul>
              </li> 
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-user-male-circle"></div>
                  </div>
                  <span>Manage User</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="add_member.php?add_mem=3">Add User</a>
                  </li>
                  <li>
                    <a href="member_list.php?mem_list=3">User List</a>
                  </li>
                </ul>
              </li> 
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-delivery-box-2"></div>
                  </div>
                  <span>Manage Area</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="area_add.php?add=1">Manage Division</a>
                  </li>
                  <li>
                    <a href="area_add.php?add=2">Manage District</a>
                  </li>
                  <li>
                    <a href="area_add.php?add=3">Manage Area</a>
                  </li>

                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-grid-squares"></div>
                  </div>
                  <span>Manage Category</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="catagory.php">Add Category</a>
                  </li>
                  <li>
                    <a href="catagory.php">Category List</a>
                  </li>


                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-newspaper"></div>
                  </div>
                  <span>Manage Offer</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="offer_type.php">Add Offer Type</a>
                  </li>
                  <li>
                    <a href="target_people.php">Add Target People</a>
                  </li>
                  <li>
                    <a href="post_offer.php">Add New Offer</a>
                  </li>
                  <li>
                    <a href="offer_list.php">Offer List</a>
                  </li>
				  <li>
                    <a href="offer_btn.php">Manage Offer Type Title</a>
                  </li>
                  <li>
                    <a href="#">Active Offer</a>
                  </li>
                  <li>
                    <a href="#">Upcoming Offer</a>
                  </li>
                  <li>
                    <a href="#">Expired Offer</a>
                  </li>
                  <li>
                    <a href="#">Deleted Offer</a>
                  </li>
                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-tasks-checked"></div>
                  </div>
                  <span>Boost</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Boost Offer</a>
                  </li>
                  <li>
                    <a href="#">Boost Collection</a>
                  </li>

                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-grid-circles"></div>
                  </div>
                  <span>Manage Subscriber</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Add New Subscriber</a>
                  </li>
                  <li>
                    <a href="#">Subscriber List</a>
                  </li>

                </ul>
              </li>
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-grid-squares"></div>
                  </div>
                  <span>Manage Promotional Mail</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Mail Subject</a>
                  </li>
                  <li>
                    <a href="#">Add Email Template</a>
                  </li> 
                  <li>
                    <a href="#">Send Mail</a>
                  </li>
                  <li>
                    <a href="#">Mail List</a>
                  </li>
                </ul>
              </li>	  
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-text-input"></div>
                  </div>
                  <span>Manage Promotional SMS</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">SMS Subject</a>
                  </li>
                  <li>
                    <a href="#">Add SMS Template</a>
                  </li> 
                  <li>
                    <a href="#">Send SMS</a>
                  </li>
                  <li>
                    <a href="#">SMS List</a>
                  </li>
                </ul>
              </li>	
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-delivery-box-2"></div>
                  </div>
                  <span>Manage Advertisement</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Advertise Title</a>
                  </li>
                  <li>
                    <a href="#">Set Advertise</a>
                  </li> 
                  <li>
                    <a href="#">Advertise List</a>
                  </li>

                </ul>
              </li>	
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-ui-55"></div>
                  </div>
                  <span>Manage Support</span></a>
                <ul class="sub-menu">
                  <li>
                    <a href="#">Add Support Title</a>
                  </li>
                  <li>
                    <a href="#">Query List</a>
                  </li> 


                </ul>
              </li>	
              <li class="has-sub-menu">
                <a href="#">
                  <div class="icon-w">
                    <div class="os-icon os-icon-tasks-checked"></div>
                  </div>
                  <span>Manage Web CMS</span></a>
                <ul class="sub-menu">
				                  <li>
                    <a href="slider.php">Manage Slider</a>
                  </li>
				                  <li>
                    <a href="slider.php?adv=adv">Manage Adv</a>
                  </li>  
                  <li>
                    <a href="#">Add New Data</a>
                  </li>
                  <li>
                    <a href="#">Page Content</a>
                  </li> 
                  <li>
                    <a href="#">Contact Request</a>
                  </li> 

                </ul>
              </li>	 
            </ul>
            <div class="side-menu-magic">
              <h4>
                Deshi-offer Admin
              </h4>


            </div>
          </div>
        </div>