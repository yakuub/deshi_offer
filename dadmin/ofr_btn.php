        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="dashboard.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="dashboard.php">Products</a>
            </li>
            <li class="breadcrumb-item">
              <span>Laptop with retina screen</span>
            </li>
          </ul>
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box"><div class="row">
          <h5 class="form-header">
           Manage Offer Type
          </h5>
<?php
  if(isset($_GET['success']))
  {
	$msg=$_GET['success'];  
	if($msg=="picture_success")
	   {   
            echo"<div class='alert alert-success' style='color:green;font-size:16px'>
			  <strong>Success!</strong> Data Successfully Added
			</div>";
	   }
	elseif($msg=="picture_select")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'>
			  <strong>Sorry !</strong> Please Select Picture
			</div>";
	   }
	elseif($msg=="picture_large")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'>
			  <strong>Sorry !</strong> Please Select a Large Picture
			</div>";
	   }
	elseif($msg=="picture_invalid")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'>
			  <strong>Sorry !</strong> PNG Format Supported.Not Others
			</div>";
	   } 
	elseif($msg=="update")
	   {   
            echo"<div class='alert alert-success' style='color:green;font-size:16px'>
			  <strong>Success !</strong> Data Updated..!
			</div>";
	   } 
	   
  }
?>
  <div class="col-sm-12">
    <div class="element-wrapper">
      <div class="element-box">
	  <?php
	  if(isset($_GET['edit']))
	  {
		  $ofid=$_GET['ofid'];

        $divsion=mysql_query("select id,name,color_code,string_name from ctype_colour where id=$ofid");
        while($divdata=mysql_fetch_array($divsion))
              {
                $c_id=$divdata['0'];
                $name=$divdata['1'];
                $color_code=$divdata['2'];
				$string_name=$divdata['3'];
	?>
	        <form id="formValidate" action="Action/offer.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="loginid" value="<?php echo $loginid?>">
<input type="hidden" name="c_id" value="<?php echo $c_id?>">
          <div class="form-group">
            <label for=""> Offer Title <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" placeholder="Offer Title" readonly value="<?php echo $name?>" name="offer_title" required="required" type="text">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>
          <div class="form-group">
            <label for=""> Offer String <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" placeholder="Offer string_name"  value="<?php echo $string_name?>" name="string_name" required="required" type="text">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>


		            <div class="form-group">
            <label for=""> Color Code <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control jscolor" readonly value="<?php echo $color_code?>" placeholder="Color Code" name="color_code" required="required" type="text">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>
          <div class="form-buttons-w">
            <button class="btn btn-primary" name="edit_ctype" type="submit"> Edit</button>
          </div>
        </form>
<?php
			  }	
	  }
	  else
	  {
	  ?>

		<?php
	  }
	  ?>
      </div>
    </div>
                <div class="element-box">
                  <h5 class="form-header">
                  Change Button Title
                  </h5>

                  <div class="table-responsive">
                    <?php
?>

                    <table id="dataTable1" width="100%" class="table table-striped table-lightfont">
          <thead>
          <tr>
         
          <th>Offer Type</th>
           <th>Button Title</th>
           <th>Color</th>
		   <th>Edit</th>
		      

          </tr>
          </thead>
          <tfoot>
          <tr>
         
          <th>Offer Type</th>
           <th>Button Title</th>
           <th>Color</th>
		   <th>Edit</th>
		      

          </tr>
          </tfoot>
          <tbody>
        
            <?php
        $divsion=mysql_query("select id,name,color_code,string_name from ctype_colour");
        while($divdata=mysql_fetch_array($divsion))
              {
                $c_id=$divdata['0'];
                $name=$divdata['1'];
                $color_code=$divdata['2'];
				$string_name=$divdata['3'];

				
                echo "
              <tr>
                  <td>$name</td>
                  <td>$string_name</td>
				 <td><div style='background:$color_code'>$string_name</div></td>
                <td><a href='offer_btn.php?ofid=$c_id&edit=edit'>Edit</a></td>
				
				
               </tr>
                ";
              }

            ?>


          </tbody>
          </table>
          <?php


          ?>
                  </div>
                </div>
              
  </div>
</div>
              <div class="floated-chat-btn">
                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
              </div>
              <div class="floated-chat-w">
                <div class="floated-chat-i">
                  <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                  </div>
                  <div class="chat-head">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-messages">
                    <div class="message">
                      <div class="message-content">
                        Hi, how can I help you?
                      </div>
                    </div>
                    <div class="date-break">
                      Mon 10:20am
                    </div>
                    <div class="message">
                      <div class="message-content">
                        Hi, my name is Mike, I will be happy to assist you
                      </div>
                    </div>
                    <div class="message self">
                      <div class="message-content">
                        Hi, I tried ordering this product and it keeps showing me error code.
                      </div>
                    </div>
                  </div>
                  <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                      <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-panel">
              <div class="content-panel-close">
                <i class="os-icon os-icon-close"></i>
              </div><div class="element-wrapper">
  <h6 class="element-header">
    Agents List
  </h6>
  <div class="element-box-tp">
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar1.jpg">
        </div>
        <div class="pt-user-name">
          Mark Parson
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            Last Login:<strong>Online Now</strong>
          </li>
          <li>
            Tickets:<strong>12</strong>
          </li>
          <li>
            Response Time:<strong>2 hours</strong>
          </li>
        </ul>
        <div class="pt-btn">
          <a class="btn btn-success btn-sm" href="#">Send Message</a>
        </div>
      </div>
    </div>
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar3.jpg">
        </div>
        <div class="pt-user-name">
          Ken Morris
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            Last Login:<strong>Online Now</strong>
          </li>
          <li>
            Tickets:<strong>8</strong>
          </li>
          <li>
            Response Time:<strong>4 hours</strong>
          </li>
        </ul>
        <div class="pt-btn">
          <a class="btn btn-success btn-sm" href="#">Send Message</a>
        </div>
      </div>
    </div>
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar2.jpg">
        </div>
        <div class="pt-user-name">
          John Newman
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            Last Login:<strong>Online Now</strong>
          </li>
          <li>
            Tickets:<strong>14</strong>
          </li>
          <li>
            Response Time:<strong>1 hour</strong>
          </li>
        </ul>
        <div class="pt-btn">
          <a class="btn btn-danger btn-sm" href="#">Offline Now</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="element-wrapper">
  <h6 class="element-header">
    Side Tables
  </h6>
  <div class="element-box">
    <h5 class="form-header">
      Table in white box
    </h5>
    <div class="form-desc">You can put a table tag inside an <code>.element-box</code> class wrapper and add <code>.table</code> class to it to get something like this:
    </div>
    <div class="controls-above-table">
      <div class="row">
        <div class="col-sm-12">
          <a class="btn btn-sm btn-primary" href="#">Download CSV</a><a class="btn btn-sm btn-danger" href="#">Delete</a>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-lightborder">
        <thead>
          <tr>
            <th>
              Customer
            </th>
            <th class="text-center">
              Status
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              John Mayers
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Kelly Brans
            </td>
            <td class="text-center">
              <div class="status-pill red" data-title="Cancelled" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Tim Howard
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Joe Trulli
            </td>
            <td class="text-center">
              <div class="status-pill yellow" data-title="Pending" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Fred Kolton
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
            </div>
          </div>
        </div>