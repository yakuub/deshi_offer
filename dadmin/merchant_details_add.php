        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="dashboard.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="dashboard.php">Products</a>
            </li>
            <li class="breadcrumb-item">
              <span>Laptop with retina screen</span>
            </li>
          </ul>
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box"><div class="row">
<?php
if(isset($_GET['add_details']))
{
$edi_id=$_GET['edi_id'];
$uname=$_GET['uname'];
$t="select user_id,profile_headline,division_id,district_id,area_id,address,about_us,support_email,facebook_link,twitter_link,
linkedin_link,instagram_link,youtube_link,map_long,map_lati,profile_pic from marchant_details where user_id=$edi_id";
$tp=mysql_query($t);
$tpo=mysql_fetch_object($tp);
$pheadline=$tpo->profile_headline;
$longitude=$tpo->map_long;
$latitude=$tpo->map_lati;
$fbpage=$tpo->facebook_link;
$youtube_url=$tpo->youtube_link;
$twitter_url=$tpo->twitter_link;
$linked_in=$tpo->linkedin_link;
$Instragram=$tpo->instagram_link;
$scontact_no=$tpo->support_email;
$division=$tpo->division_id;
$District=$tpo->district_id;
$Area=$tpo->area_id;
$merchant_add=$tpo->address;
$merchant_desc=$tpo->about_us;
?>
  <div class="col-sm-12">
    <div class="element-wrapper">
      <div class="element-box">
        <form id="formValidate" method="post" action="Action/merchant_details_add.php" enctype="multipart/form-data">
<input class="form-control"  name="uname"  value="<?php echo $uname?>" placeholder="Enter Username" required="required" type="hidden">
<input class="form-control"  name="edi_id"  value="<?php echo $edi_id?>" placeholder="Enter Username" required="required" type="hidden">
          <h5 class="form-header">
            Add Details of <?php echo $uname?>
          </h5>
		    <div class="form-group">
            <label for="">Profile Headline</label><input class="form-control" name="pheadline" value="<?php echo $pheadline?>"  placeholder="Enter Headline" required="required" type="text">
         
          </div>

		    <div class="form-group">
            <label for="">Featured Image</label><input class="form-control"  placeholder="Post Title" required="required" name="files[]" type="file">
         
          </div>
		  		    <div class="form-group">
            <label for="">Adv Banner Headline</label><input class="form-control" name="ad_headline" value="<?php echo $pheadline?>"  placeholder="Enter Adv Banner Headline" required="required" type="text">
         
          </div>
		  		  		    <div class="form-group">
            <label for="">G-Map Longitude</label><input class="form-control" name="longitude" value="<?php echo $longitude?>" placeholder="Enter Adv G-Map Longitude" required="required" type="text">
         
          </div>
		  		  		  		    <div class="form-group">
            <label for="">G-Map latitude</label><input class="form-control" name="latitude" value="<?php echo $latitude?>"  placeholder="Enter Adv Banner Headline" required="required" type="text">
         
          </div>
		  		  		  		  		    <div class="form-group">
            <label for="">Facebook Page</label><input class="form-control" name="fbpage" value="<?php echo $fbpage?>"  placeholder="Enter Facebook Page" required="required" type="url">
         
          </div>
		  		  		  		  		  		    <div class="form-group">
            <label for="">Youtube URL</label><input class="form-control" name="youtube_url"  value="<?php echo $youtube_url?>"  placeholder="Enter Youtube URL" required="required" type="url">
         
          </div>
		  <div class="form-group">
            <label for="">Twitter URL</label><input class="form-control" name="twitter_url"  value="<?php echo $twitter_url?>"  placeholder="Enter Twitter URL" required="required" type="url">
         
          </div>
		  <div class="form-group">
            <label for="">Link-din URL</label><input class="form-control" name="linked_in" value="<?php echo $linked_in?>" placeholder="Enter Link-din URL" required="required" type="url">
         
          </div> 
		  <div class="form-group">
            <label for="">Instragram URL</label><input class="form-control" name="Instragram" value="<?php echo $Instragram?>"  placeholder="Enter Instragram URL" required="required" type="url">
         
          </div> 		  
		  		    <div class="form-group">
            <label for="">Support Email Add </label><input class="form-control" name="scontact_no" value="<?php echo $scontact_no?>"  placeholder="Enter Support Contact No" required="required" type="text">
          
          </div>
          <div class="form-group">
            <label for="">Division </label>
			<select class="form-control" name="division">
              <?php
			  $di="select id,name from divisions";
			  $dip=mysql_query($di);
			  while($dd=mysql_fetch_array($dip))
			        {
					$divid=$dd['0'];	
					$div_name=$dd['1'];
					echo "<option value='$divid'>$div_name</option>";	
					}
			  ?>
            </select>
			
           
          </div> 	
          <div class="form-group">
            <label for="">District </label>
			<select class="form-control" name="District">
              <?php
			  $di="select id,name from districts";
			  $dip=mysql_query($di);
			  while($dd=mysql_fetch_array($dip))
			        {
					$divid=$dd['0'];	
					$div_name=$dd['1'];
					echo "<option value='$divid'>$div_name</option>";	
					}
			  ?>
            </select>
			
           
          </div> 
	
          <div class="form-group">
            <label for="">Area </label>
			<select class="form-control" name="Area">
              <?php
			  $di="select area_id,area_title from area";
			  $dip=mysql_query($di);
			  while($dd=mysql_fetch_array($dip))
			        {
					$divid=$dd['0'];	
					$div_name=$dd['1'];
					echo "<option value='$divid'>$div_name</option>";	
					}
			  ?>
            </select>
			
           
          </div> 
		              <div class="form-group">
              <label>Address <span style="color:red;font-family:verdana">(*)</span></label><textarea class="form-control"   name="merchant_add" rows="3" required><?php echo $merchant_add?></textarea>
            </div> 
            <div class="form-group">
              <label>About Merchant<span style="color:red;font-family:verdana">(*)</span></label><textarea class="form-control" id="ckeditor1" name="merchant_desc" rows="3" required><?php echo $merchant_desc?></textarea>
            </div> 

          <div class="form-buttons-w">
          
			<input type="submit" name="submit" class="btn btn-primary" value="Add Data">
          </div>
        </form>
      </div>
    </div>
  </div>
<?php

}

?>


  
  <!--Upper for Member Add-->
</div>
              <div class="floated-chat-btn">
                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
              </div>
              <div class="floated-chat-w">
                <div class="floated-chat-i">
                  <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                  </div>
                  <div class="chat-head">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-messages">
                    <div class="message">
                      <div class="message-content">
                        Hi, how can I help you?
                      </div>
                    </div>
                    <div class="date-break">
                      Mon 10:20am
                    </div>
                    <div class="message">
                      <div class="message-content">
                        Hi, my name is Mike, I will be happy to assist you
                      </div>
                    </div>
                    <div class="message self">
                      <div class="message-content">
                        Hi, I tried ordering this product and it keeps showing me error code.
                      </div>
                    </div>
                  </div>
                  <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                      <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-panel">
              <div class="content-panel-close">
                <i class="os-icon os-icon-close"></i>
              </div><div class="element-wrapper">
  <h6 class="element-header">
    Latest Member List
  </h6>
  <div class="element-box-tp">
  <?php
  $mem="select * from user_login where usertype!=1";
  $memp=mysql_query($mem);
  if($memp=true)
   {
	   while($md=mysql_fetch_array($memp))
	         {
			  $uid=$md['0'];	
			  $name=$md['1'];	
              $pass=$md['2'];
			  $contact=$md['3'];
			  $email=$md['4'];
			  $utype=$md['5'];
			  $pic=$md['9'];
			  if(($pic=="")or(empty($pic)))
			  {
				  $pic="img/avatar1.jpg";
			  }
			  else
			  {
				$pic="Action/Profilepic/avatar1.jpg";  
			  }
			  $vstatus=$md['7'];
			  if($vstatus==1)
			  {
				  $vt="Active";
			  }
			  elseif($vstatus==0)
			  {
				  $vt="Inactive";
			  }
			  if($utype==1)
			    {
					$utitle="Admin";
				}
		     elseif($utype==2)
			   {
				 $utitle="Merchant";  
			   }
		     elseif($utype==3)
			   {
				 $utitle="User";  
			   }   
			  //Below For Last Login Details
			  $login_det="select logintime from userlog where loginid='' order by log_id desc limit 1";
			  $login_det_p=mysql_query($login_det);
			  if($login_det_p==true)
			      {
					  $loginpc=mysql_num_rows($login_det_p);
					  if($loginpc!=0)
					     {
							 while($ld=mysql_fetch_array())
							 {
							   $last_time=$ld['0'];	 
							 }
						 }
						 else
						 {
							  $last_time="Not Logged in Yet";	  
						 }
					  
				  }
			  //Upper For Last Login Details
			  ?>
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar1.jpg">
        </div>
        <div class="pt-user-name">
         <?php echo $name ?>
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            User Type :<strong><?php echo $utitle?></strong>
          </li>
          <li>
            Last Login Time:<strong><?php echo $last_time?></strong>
          </li>

        </ul>
        <div class="pt-btn">
          <a class="btn btn-success btn-sm" href="#">Send Message</a>
        </div>
      </div>
    </div>  
			  <?php
			  
			 }
   }
  
  ?>



  </div>
</div>

            </div>
          </div>
        </div>