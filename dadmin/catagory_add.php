        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="dashboard.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="dashboard.php">Products</a>
            </li>
            <li class="breadcrumb-item">
              <span>Laptop with retina screen</span>
            </li>
          </ul>
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box"><div class="row">
  <div class="col-sm-12">
    <div class="element-wrapper">
      <div class="element-box">
	  <?php
  if(isset($_GET['success']))
  {
	$msg=$_GET['success'];  
	if($msg=="picture_success")
	   {   
            echo"<div class='alert alert-success' style='color:green;font-size:16px'>
			  <strong>Success!</strong> Data Successfully Added..!
			</div>";
	   }
	elseif($msg=="successu")
	   {   
            echo"<div class='alert alert-success' style='color:green;font-size:16px'>
			  <strong>Success!</strong> Data Successfully Updated..!
			</div>";
	   } 
	   
	elseif($msg=="picture_select")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'>
			  <strong>Sorry !</strong> Please Select Picture ..!
			</div>";
	   }
	elseif($msg=="picture_large")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'>
			  <strong>Sorry !</strong> Please Select a Large Picture ..!
			</div>";
	   }
	elseif($msg=="picture_invalid")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'>
			  <strong>Sorry !</strong> PNG Format Supported.Not Others..!
			</div>";
	   } 
	elseif($msg=="successd")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'>
			  <strong>Success !</strong> Data Delete Done ..!
			</div>";
	   }  
	   
  }
?>
<?php
if(isset($_GET['edit']))
{
	$catid=$_GET['catid'];
        $divsion=mysql_query("select catagory_id,catagory_title,catagory_description,
		menu_st,add_by,featured_cat_st,top_menu_sl,front_page_show_st,top_menu_st  from catagory where catagory_id=$catid
");
        while($sdata=mysql_fetch_array($divsion))
              {
					$catagory_id=$sdata['0'];
					$catagory_title=$sdata['1'];
                    $catagory_description=$sdata['2'];
					$menu_st=$sdata['3'];
					if($menu_st==0)
					  {
						$mst="No";  
						}
					elseif($menu_st==1)
					  {
						$mst="Yes";  
						}
					$add_by=$sdata['4'];
					$featured_cat_st=$sdata['5'];
					if($featured_cat_st==0)
					  {
						$cmst="No";  
						}
					elseif($featured_cat_st==1)
					  {
						$cmst="Yes";  
						}
	               $top_menu_sl=$sdata['6'];
					$front_show=$sdata['7'];
					$tmenu_st=$sdata['8'];
					if($front_show==0)
					  {
						$fmst="No";  
						}
					elseif($front_show==1)
					  {
						$fmst="Yes";  
						}
					if($tmenu_st==0)
					  {
						$tmst="No";  
						}
					elseif($tmenu_st==1)
					  {
						$tmst="Yes";  
						}	
			  }
?>
        <form id="formValidate" action="Action/offer.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="loginid" value="<?php echo $loginid?>">
<input type="hidden" name="catagory_id" value="<?php echo $catagory_id?>">

          <div class="form-group">
            <label for=""> Catagory Title <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" value="<?php echo $catagory_title?>" placeholder="Catagory Title" name="catagory_title" required="required" type="text">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>
 <div class="form-group row" >
  <label class="control-label"> Show on Menu <span style="color:red;font-family:verdana">(*)</span></label>
                              
<select name="show_menu" class="form-control" data-original-title="Show on Menu">
 <option value="<?php echo $menu_st?>"><?php echo $mst?></option>
  <option value="1">Yes</option>
  <option value="0">No</option>

</select>                                

                            </div> 
							
						
 <div class="form-group row" >
  <label class="control-label"> Show on Featured <span style="color:red;font-family:verdana">(*)</span></label>
                              
<select name="featured" class="form-control" data-original-title="Show on Menu">
<option value="<?php echo $featured_cat_st?>"><?php echo $cmst?></option>
  <option value="1">Yes</option>
  <option value="0">No</option>

</select>                                

                            </div> 
 <div class="form-group row" >
  <label class="control-label"> Show on Front Page <span style="color:red;font-family:verdana">(*)</span></label>
                              
<select name="front_show" class="form-control" data-original-title="Show on Menu">

<option value="<?php echo $front_show?>"><?php echo $fmst?></option>
  <option value="1">Yes</option>
  <option value="0">No</option>

</select>                                

                            </div> 
							
 <div class="form-group row" >
  <label class="control-label"> Show on Top Menu <span style="color:red;font-family:verdana">(*)</span></label>
                              
<select name="tshow_menu" class="form-control" data-original-title="Show on Menu">

<option value="<?php echo $tmenu_st?>"><?php echo $tmst?></option>
  <option value="1">Yes</option>
  <option value="0">No</option>

</select>                                

                            </div> 							
 <div class="form-group row" >
  <label class="control-label"> Menu Sl <span style="color:red;font-family:verdana">(*)</span></label>
 <input type="text" name="menu_sl"class="form-control" value="<?php echo $top_menu_sl?>">                             
                          

                            </div>    							
          <div class="form-group">
            <label for=""> Image  (PNG/JPG: Max 200KB) <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" placeholder="Offer Title" name="files[]"  type="file">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>
            <div class="row" id="smsbody" style="display:none" >
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="">Description</label>
				 
               <textarea class="form-control" id="ckeditor2" name="catagory_des" rows="3"></textarea>
                </div>
              </div>
			    </div>
          <div class="form-buttons-w">
            <button class="btn btn-primary" name="Catagory_edit" type="submit"> Submit</button>
          </div>
        </form>
<?php	
	
}
elseif(isset($_GET['delete_cat']))
{
$catid=$_GET['catid'];	
$dlt="delete from catagory  where catagory_id=$catid";
$dltp=mysql_query($dlt);
if($dltp==true)
{
	 echo"<script> location.replace('catagory.php?success=successd'); </script>"; 	
}
}
else
{?>
        <form id="formValidate" action="Action/offer.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="loginid" value="<?php echo $loginid?>">


          <div class="form-group">
            <label for=""> Catagory Title <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" placeholder="Catagory Title" name="catagory_title" required="required" type="text">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>
 <div class="form-group row" >
  <label class="control-label"> Show on Menu <span style="color:red;font-family:verdana">(*)</span></label>
                              
<select name="show_menu" class="form-control" data-original-title="Show on Menu">
  <option value="1">Yes</option>
  <option value="0">No</option>

</select>                                

                            </div> 
 <div class="form-group row" >
  <label class="control-label"> Show on Fetured <span style="color:red;font-family:verdana">(*)</span></label>
                              
<select name="featured" class="form-control" data-original-title="Show on Menu">
  <option value="1">Yes</option>
  <option value="0">No</option>

</select>                                

                            </div> 
 <div class="form-group row" >
  <label class="control-label"> Show on Front Page <span style="color:red;font-family:verdana">(*)</span></label>
                              
<select name="front_show" class="form-control" data-original-title="Show on Menu">

  <option value="1">Yes</option>
  <option value="0">No</option>

</select>                                

                            </div> 		
 <div class="form-group row" >
  <label class="control-label"> Menu Sl <span style="color:red;font-family:verdana">(*)</span></label>
 <input type="text" name="menu_sl"class="form-control">                             
                          

                            </div>    							
          <div class="form-group">
            <label for=""> Image  (PNG/JPG: Max 200KB) <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" placeholder="Offer Title" name="files[]"  required="required" type="file">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>
            <div class="row" id="smsbody" style="display:none" >
              <div class="col-sm-12">
                <div class="form-group">
                  <label for="">Description</label>
				 
               <textarea class="form-control" id="ckeditor2" name="catagory_des" rows="3"></textarea>
                </div>
              </div>
			    </div>
          <div class="form-buttons-w">
            <button class="btn btn-primary" name="Catagory" type="submit"> Submit</button>
          </div>
        </form>
		<?php
}
		?>
      </div>
    </div>
                <div class="element-box">
                  <h5 class="form-header">
                   Added Catagory List
                  </h5>

                  <div class="table-responsive">
                    <?php
?>

                    <table id="dataTable1" width="100%" class="table table-striped table-lightfont">
          <thead>
          <tr>
         
                                 <th>Catagory Title</th>
                               
                                <th>Description</th>

<th>Menu Status</th>
		   <th>Edit</th>
		       <th>Delete</th>

          </tr>
          </thead>
          <tfoot>
          <tr>
         
                                 <th>Catagory Title</th>
                               
                                <th>Description</th>

<th>Menu Status</th>
		   <th>Edit</th>
		       <th>Delete</th>

          </tr>
          </tfoot>
          <tbody>
        
            <?php
        $divsion=mysql_query("select catagory_id,catagory_title,catagory_description,menu_st,add_by  from catagory
");
        while($sdata=mysql_fetch_array($divsion))
              {
					$catagory_id=$sdata['0'];
					$catagory_title=$sdata['1'];
                    $catagory_description=$sdata['2'];
					$menu_st=$sdata['3'];
					if($menu_st==0)
					  {
						$mst="Un-Publish";  
						}
					elseif($menu_st==1)
					  {
						$mst="Publish";  
						}
					$add_by=$sdata['4'];

                echo "
              <tr>

								<td>$catagory_title</td>
                                <td>$catagory_description</td>
								<td>$mst</td>
                <td><a href='catagory.php?edit=edit&catid=$catagory_id'>Edit</a></td>
				<td><a href='catagory.php?delete_cat=delete_cat&catid=$catagory_id'>Delete</a></td>
				
               </tr>
                ";
              }

            ?>


          </tbody>
          </table>
          <?php


          ?>
                  </div>
                </div>
              
  </div>
</div>
              <div class="floated-chat-btn">
                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
              </div>
              <div class="floated-chat-w">
                <div class="floated-chat-i">
                  <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                  </div>
                  <div class="chat-head">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-messages">
                    <div class="message">
                      <div class="message-content">
                        Hi, how can I help you?
                      </div>
                    </div>
                    <div class="date-break">
                      Mon 10:20am
                    </div>
                    <div class="message">
                      <div class="message-content">
                        Hi, my name is Mike, I will be happy to assist you
                      </div>
                    </div>
                    <div class="message self">
                      <div class="message-content">
                        Hi, I tried ordering this product and it keeps showing me error code.
                      </div>
                    </div>
                  </div>
                  <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                      <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-panel">
              <div class="content-panel-close">
                <i class="os-icon os-icon-close"></i>
              </div><div class="element-wrapper">
  <h6 class="element-header">
    Agents List
  </h6>
  <div class="element-box-tp">
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar1.jpg">
        </div>
        <div class="pt-user-name">
          Mark Parson
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            Last Login:<strong>Online Now</strong>
          </li>
          <li>
            Tickets:<strong>12</strong>
          </li>
          <li>
            Response Time:<strong>2 hours</strong>
          </li>
        </ul>
        <div class="pt-btn">
          <a class="btn btn-success btn-sm" href="#">Send Message</a>
        </div>
      </div>
    </div>
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar3.jpg">
        </div>
        <div class="pt-user-name">
          Ken Morris
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            Last Login:<strong>Online Now</strong>
          </li>
          <li>
            Tickets:<strong>8</strong>
          </li>
          <li>
            Response Time:<strong>4 hours</strong>
          </li>
        </ul>
        <div class="pt-btn">
          <a class="btn btn-success btn-sm" href="#">Send Message</a>
        </div>
      </div>
    </div>
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar2.jpg">
        </div>
        <div class="pt-user-name">
          John Newman
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            Last Login:<strong>Online Now</strong>
          </li>
          <li>
            Tickets:<strong>14</strong>
          </li>
          <li>
            Response Time:<strong>1 hour</strong>
          </li>
        </ul>
        <div class="pt-btn">
          <a class="btn btn-danger btn-sm" href="#">Offline Now</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="element-wrapper">
  <h6 class="element-header">
    Side Tables
  </h6>
  <div class="element-box">
    <h5 class="form-header">
      Table in white box
    </h5>
    <div class="form-desc">You can put a table tag inside an <code>.element-box</code> class wrapper and add <code>.table</code> class to it to get something like this:
    </div>
    <div class="controls-above-table">
      <div class="row">
        <div class="col-sm-12">
          <a class="btn btn-sm btn-primary" href="#">Download CSV</a><a class="btn btn-sm btn-danger" href="#">Delete</a>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-lightborder">
        <thead>
          <tr>
            <th>
              Customer
            </th>
            <th class="text-center">
              Status
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              John Mayers
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Kelly Brans
            </td>
            <td class="text-center">
              <div class="status-pill red" data-title="Cancelled" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Tim Howard
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Joe Trulli
            </td>
            <td class="text-center">
              <div class="status-pill yellow" data-title="Pending" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Fred Kolton
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
            </div>
          </div>
        </div>