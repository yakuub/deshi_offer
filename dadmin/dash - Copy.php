        <div class="content-w">
          <div class="top-menu-secondary">
            <ul>
              <li class="active">
                <a href="#">Home</a>
              </li>
              <li>
                <a href="#">Projects</a>
              </li>
              <li>
                <a href="#">Customers</a>
              </li>
              <li>
                <a href="#">Reports</a>
              </li>
            </ul>
          </div>
          <div class="content-i">
            <div class="content-box">
              <div class="os-tabs-w">
                <div class="os-tabs-controls">
                  <ul class="nav nav-tabs upper">
                    <li class="nav-item">
                      <a class="nav-link active" data-toggle="tab" href="#tab_overview">Active</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#tab_sales">Overview</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#tab_sales">Closed</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#tab_sales">Required</a>
                    </li>
                  </ul>
                  <ul class="nav nav-pills smaller hidden-md-down">
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#">Today</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link active" data-toggle="tab" href="#">7 Days</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#">14 Days</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" data-toggle="tab" href="#">Last Month</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="element-wrapper">
                    <div class="element-actions">
                      <form class="form-inline justify-content-sm-end">
                        <select class="form-control form-control-sm rounded">
                          <option value="Pending">
                            Today
                          </option>
                          <option value="Active">
                            Last Week 
                          </option>
                          <option value="Cancelled">
                            Last 30 Days
                          </option>
                        </select>
                      </form>
                    </div>
                    <h6 class="element-header">
                      Sales Dashboard
                    </h6>
                    <div class="element-content">
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="element-box el-tablo">
                            <div class="label">
                              Products Sold
                            </div>
                            <div class="value">
                              57
                            </div>
                            <div class="trending trending-up">
                              <span>12%</span><i class="os-icon os-icon-arrow-up2"></i>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="element-box el-tablo">
                            <div class="label">
                              Gross Profit
                            </div>
                            <div class="value">
                              $457
                            </div>
                            <div class="trending trending-down-basic">
                              <span>12%</span><i class="os-icon os-icon-arrow-2-down"></i>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="element-box el-tablo">
                            <div class="label">
                              New Customers
                            </div>
                            <div class="value">
                              125
                            </div>
                            <div class="trending trending-down-basic">
                              <span>9%</span><i class="os-icon os-icon-graph-down"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="floated-chat-btn">
                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
              </div>
              <div class="floated-chat-w">
                <div class="floated-chat-i">
                  <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                  </div>
                  <div class="chat-head">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-messages">
                    <div class="message">
                      <div class="message-content">
                        Hi, how can I help you?
                      </div>
                    </div>
                    <div class="date-break">
                      Mon 10:20am
                    </div>
                    <div class="message">
                      <div class="message-content">
                        Hi, my name is Mike, I will be happy to assist you
                      </div>
                    </div>
                    <div class="message self">
                      <div class="message-content">
                        Hi, I tried ordering this product and it keeps showing me error code.
                      </div>
                    </div>
                  </div>
                  <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                      <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>