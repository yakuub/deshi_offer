        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="dashboard.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="dashboard.php">Products</a>
            </li>
            <li class="breadcrumb-item">
              <span>Laptop with retina screen</span>
            </li>
          </ul>
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box"><div class="row">
  <div class="col-sm-12">
    <div class="element-wrapper">
      <div class="element-box">
        <form id="formValidate">
          <h5 class="form-header">
            Form Validation
          </h5>
          <div class="form-desc">
            Validation of the form is made possible using powerful validator plugin for bootstrap. <a href="http://1000hz.github.io/bootstrap-validator/" target="_blank">Learn more about Bootstrap Validator</a>
          </div>
          <div class="form-group">
            <label for=""> Email address</label><input class="form-control" data-error="Your email address is invalid" placeholder="Enter email" required="required" type="email">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for=""> Password</label><input class="form-control" data-minlength="6" placeholder="Password" required="required" type="password">
                <div class="help-block form-text text-muted form-control-feedback">
                  Minimum of 6 characters
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="">Confirm Password</label><input class="form-control" data-match-error="Passwords don&#39;t match" placeholder="Confirm Password" required="required" type="password">
                <div class="help-block form-text with-errors form-control-feedback"></div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for=""> Regular select</label><select class="form-control">
              <option value="New York">
                New York
              </option>
              <option value="California">
                California
              </option>
              <option value="Boston">
                Boston
              </option>
              <option value="Texas">
                Texas
              </option>
              <option value="Colorado">
                Colorado
              </option>
            </select>
          </div>
          <div class="form-group">
            <label for=""> Multiselect</label><select class="form-control select2" multiple="true">
              <option selected="true">
                New York
              </option>
              <option selected="true">
                California
              </option>
              <option>
                Boston
              </option>
              <option>
                Texas
              </option>
              <option>
                Colorado
              </option>
            </select>
          </div>
          <fieldset class="form-group">
            <legend><span>Section Example</span></legend>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for=""> First Name</label><input class="form-control" data-error="Please input your First Name" placeholder="First Name" required="required" type="text">
                  <div class="help-block form-text with-errors form-control-feedback"></div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="">Last Name</label><input class="form-control" data-error="Please input your Last Name" placeholder="Last Name" required="required" type="text">
                  <div class="help-block form-text with-errors form-control-feedback"></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label for=""> Date of Birth</label><input class="single-daterange form-control" placeholder="Date of birth" type="text" value="04/12/1978">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label for="">Twitter Username</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      @
                    </div>
                    <input class="form-control" placeholder="Twitter Username" type="text">
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="">Date Range Picker</label><input class="multi-daterange form-control" type="text" value="03/31/2017 - 04/06/2017">
            </div>
            <div class="form-group">
              <label> Example textarea</label><textarea class="form-control" rows="3"></textarea>
            </div>
          </fieldset>
          <div class="form-check">
            <label class="form-check-label"><input class="form-check-input" type="checkbox">I agree to terms and conditions</label>
          </div>
          <div class="form-buttons-w">
            <button class="btn btn-primary" type="submit"> Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
              <div class="floated-chat-btn">
                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
              </div>
              <div class="floated-chat-w">
                <div class="floated-chat-i">
                  <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                  </div>
                  <div class="chat-head">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-messages">
                    <div class="message">
                      <div class="message-content">
                        Hi, how can I help you?
                      </div>
                    </div>
                    <div class="date-break">
                      Mon 10:20am
                    </div>
                    <div class="message">
                      <div class="message-content">
                        Hi, my name is Mike, I will be happy to assist you
                      </div>
                    </div>
                    <div class="message self">
                      <div class="message-content">
                        Hi, I tried ordering this product and it keeps showing me error code.
                      </div>
                    </div>
                  </div>
                  <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                      <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-panel">
              <div class="content-panel-close">
                <i class="os-icon os-icon-close"></i>
              </div><div class="element-wrapper">
  <h6 class="element-header">
    Agents List
  </h6>
  <div class="element-box-tp">
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar1.jpg">
        </div>
        <div class="pt-user-name">
          Mark Parson
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            Last Login:<strong>Online Now</strong>
          </li>
          <li>
            Tickets:<strong>12</strong>
          </li>
          <li>
            Response Time:<strong>2 hours</strong>
          </li>
        </ul>
        <div class="pt-btn">
          <a class="btn btn-success btn-sm" href="#">Send Message</a>
        </div>
      </div>
    </div>
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar3.jpg">
        </div>
        <div class="pt-user-name">
          Ken Morris
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            Last Login:<strong>Online Now</strong>
          </li>
          <li>
            Tickets:<strong>8</strong>
          </li>
          <li>
            Response Time:<strong>4 hours</strong>
          </li>
        </ul>
        <div class="pt-btn">
          <a class="btn btn-success btn-sm" href="#">Send Message</a>
        </div>
      </div>
    </div>
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar2.jpg">
        </div>
        <div class="pt-user-name">
          John Newman
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            Last Login:<strong>Online Now</strong>
          </li>
          <li>
            Tickets:<strong>14</strong>
          </li>
          <li>
            Response Time:<strong>1 hour</strong>
          </li>
        </ul>
        <div class="pt-btn">
          <a class="btn btn-danger btn-sm" href="#">Offline Now</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="element-wrapper">
  <h6 class="element-header">
    Side Tables
  </h6>
  <div class="element-box">
    <h5 class="form-header">
      Table in white box
    </h5>
    <div class="form-desc">You can put a table tag inside an <code>.element-box</code> class wrapper and add <code>.table</code> class to it to get something like this:
    </div>
    <div class="controls-above-table">
      <div class="row">
        <div class="col-sm-12">
          <a class="btn btn-sm btn-primary" href="#">Download CSV</a><a class="btn btn-sm btn-danger" href="#">Delete</a>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <table class="table table-lightborder">
        <thead>
          <tr>
            <th>
              Customer
            </th>
            <th class="text-center">
              Status
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              John Mayers
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Kelly Brans
            </td>
            <td class="text-center">
              <div class="status-pill red" data-title="Cancelled" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Tim Howard
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Joe Trulli
            </td>
            <td class="text-center">
              <div class="status-pill yellow" data-title="Pending" data-toggle="tooltip"></div>
            </td>
          </tr>
          <tr>
            <td>
              Fred Kolton
            </td>
            <td class="text-center">
              <div class="status-pill green" data-title="Complete" data-toggle="tooltip"></div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
            </div>
          </div>
        </div>