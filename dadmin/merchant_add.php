        <div class="content-w">
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="dashboard.php">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="dashboard.php">Products</a>
            </li>
            <li class="breadcrumb-item">
              <span>Laptop with retina screen</span>
            </li>
          </ul>
          <div class="content-panel-toggler">
            <i class="os-icon os-icon-grid-squares-22"></i><span>Sidebar</span>
          </div>
          <div class="content-i">
            <div class="content-box"><div class="row">
			

  <div class="col-sm-12">
    <div class="element-wrapper">
      <div class="element-box">
	            <h5 class="form-header">
            Add New Merchant
			<?php
  if(isset($_GET['success']))
  {
	$msg=$_GET['success'];  
	if($msg=="picture_success")
	   {   
            echo"<div class='alert alert-success' style='color:green;font-size:16px'>
			  <strong>Success!</strong> Data Successfully Added
			</div>";
	   }
		elseif($msg=="picture_successd")
	   {   
            echo"<div class='alert alert-success' style='color:green;font-size:16px'>
			  <strong>Success!</strong> Data Successfully Updated
			</div>";
	   } 
	   
	elseif($msg=="picture_select")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'>
			  <strong>Sorry !</strong> Please Select Picture
			</div>";
	   }
	elseif($msg=="picture_large")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'>
			  <strong>Sorry !</strong> Please Select a Large Picture
			</div>";
	   }
	elseif($msg=="picture_invalid")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'>
			  <strong>Sorry !</strong> PNG Format Supported.Not Others
			</div>";
	   }
		elseif($msg=="submit")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'> 
			  <strong>Sorry !</strong> Please Submit the Button..!
			</div>";
	   } 
	   		elseif($msg=="emailexisr")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'> 
			  <strong>Sorry !</strong> Email already Exist !
			</div>";
	   } 
	   		elseif($msg=="uname")
	   {   
            echo"<div class='alert alert-success' style='color:red;font-size:16px'> 
			  <strong>Sorry !</strong> Email already Exist !
			</div>";
	   } 
	   
	   
  }
?>
          </h5>
		  <?php
		  if(isset($_GET['edit']))
		  {
			  $edi_id=$_GET['edi_id'];
			  
  $mem="select loginid,name,password,contact,email,usertype,verify_status,profile_pic,show_on_web,total_post,is_brand
from user_login where loginid=$edi_id";
  //echo $mem;
  $memp=mysql_query($mem);
  if($memp==true)
   {
	
	   while($md=mysql_fetch_row($memp))
	         {
				
			  $uid=$md['0'];		  
			  $name=$md['1'];	
              $pass=$md['2'];
			  $contact=$md['3'];
			  $email=$md['4'];
			  $utype=$md['5'];
			  $vstatus=$md['6'];
			  $pic=$md['7'];

	
			  if(($pic=="")or(empty($pic)))
			  {
				  $pic="img/avatar1.jpg";
			  }
			  else
			  {
				$pic="Action/Profilepic/avatar1.jpg";  
			  }			  
			  if($vstatus==1)
			  {
				  $vt="Active";
			  }
			  elseif($vstatus==0)
			  {
				  $vt="Inactive";
			  }
			  $show_on_web=$md['8'];
			  if($show_on_web==1)
			  {
				  $st="Yes";
			  }
			  elseif($show_on_web==0)
			  {
				  $st="Yes";
			  } 
			
			  $is_brand=$md['10'];
			  if($is_brand==1)
			  {
				  $bt="Yes";
			  }
			  elseif($is_brand==0)
			  {
				  $bt="Yes";
			  }
			  ?>
        <form id="formValidate"  action="Action/user_add.php" method="post" enctype="multipart/form-data">
        <input type="hidden" value="<?php echo $uid?>" name="mrid"/>
          <div class="form-group">
            <label for=""> Email address  <span style="color:red;font-family:verdana">(*)</span></label>
			<input class="form-control" name="emailaddress" data-error="Your email address is invalid" value="<?php echo $email?>" placeholder="Enter email" required="required" type="email">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>
		    <div class="form-group">
            <label for="">Merchant Login Name  <span style="color:red;font-family:verdana">(*)</span></label>
			<input class="form-control" name="username" value="<?php echo $name?>"  placeholder="Enter Username" required="required" type="text">
         
          </div>
          <div class="form-group">
            <label for=""> Logo (PNG)  <span style="color:red;font-family:verdana">(*)</span></label>
			<input class="form-control" placeholder="Offer Title" name="files[]"  type="file">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>
		            <div class="form-group">
            <label for="">Is Featured  <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control" name="featured">
              <option value="<?php echo $show_on_web?>" selected><?php echo $st?></option>
              <option value="1">
               Yes
              </option>
              <option value="0">
                No
              </option>
            </select>
          </div>
		  		            <div class="form-group">
            <label for="">Is Top Stores  <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control" name="top_stores">
              <option value="<?php echo $top_stores?>" selected><?php echo $top_st?></option>
              <option value="1">
               Yes
              </option>
              <option value="0">
                No
              </option>
            </select>
          </div>
		 <div class="form-group">
            <label for="">Is Brand  <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control" name="is_brand">
			   <option value="<?php echo $is_brand?>" selected><?php echo $bt?></option>
              <option value="1">
               Yes
              </option>
              <option value="0">
                No
              </option>
            </select>
          </div>
		   
		  
		  		    <div class="form-group">
            <label for="">Contact No  <span style="color:red;font-family:verdana">(*)</span></label>
			<input class="form-control" name="contact_no" value="<?php echo $contact?>"  placeholder="Enter Contact No" required="required" type="text">
          
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for=""> Password  <span style="color:red;font-family:verdana">(*)</span></label>
				<input class="form-control" name="password" data-minlength="6" placeholder="Password" value="<?php echo $pass?>" required="required" type="password">
                <div class="help-block form-text text-muted form-control-feedback">
                  Minimum of 6 characters
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="">Confirm Password  <span style="color:red;font-family:verdana">(*)</span></label>
				<input class="form-control" name="conpass" data-match-error="Passwords don&#39;t match" value="<?php echo $pass?>" placeholder="Confirm Password" required="required" type="password">
                <div class="help-block form-text with-errors form-control-feedback"></div>
              </div>
            </div>
          </div>




          <div class="form-buttons-w">          
			<input type="submit" name="edit" class="btn btn-primary" value="Add New">
          </div>
        </form>
		<?php
		  
			 }
			 }
			 else
			 {
				 echo mysql_error();
			 }
		  }
		  else
			  
			  {
				  ?>
<form id="formValidate"  action="Action/user_add.php" method="post" enctype="multipart/form-data">

          <div class="form-group">
            <label for=""> Email address  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="emailaddress" data-error="Your email address is invalid" placeholder="Enter email" required="required" type="email">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>
		    <div class="form-group">
            <label for="">Merchant Login Name  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="username"  placeholder="Enter Username" required="required" type="text">
         
          </div>
          <div class="form-group">
            <label for=""> Logo  (PNG)  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" placeholder="Offer Title" name="files[]"  required="required" type="file">
            <div class="help-block form-text with-errors form-control-feedback"></div>
          </div>
		            <div class="form-group">
            <label for="">Is Featured  <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control" name="featured">
              <option value="1">
               Yes
              </option>
              <option value="0">
                No
              </option>
            </select>
          </div>
		  		  		            <div class="form-group">
            <label for="">Is Top Stores  <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control" name="top_stores">
          
              <option value="1">
               Yes
              </option>
              <option value="0">
                No
              </option>
            </select>
          </div>
		 		            <div class="form-group">
            <label for="">Is Brand  <span style="color:red;font-family:verdana">(*)</span></label><select class="form-control" name="is_brand">
              <option value="1">
               Yes
              </option>
              <option value="0">
                No
              </option>
            </select>
          </div>
		   
		  
		  		    <div class="form-group">
            <label for="">Contact No  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="contact_no"  placeholder="Enter Contact No" required="required" type="text">
          
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for=""> Password  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="password" data-minlength="6" placeholder="Password" required="required" type="password">
                <div class="help-block form-text text-muted form-control-feedback">
                  Minimum of 6 characters
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="">Confirm Password  <span style="color:red;font-family:verdana">(*)</span></label><input class="form-control" name="conpass" data-match-error="Passwords don&#39;t match" placeholder="Confirm Password" required="required" type="password">
                <div class="help-block form-text with-errors form-control-feedback"></div>
              </div>
            </div>
          </div>




          <div class="form-buttons-w">          
			<input type="submit" name="submit" class="btn btn-primary" value="Add New">
          </div>
        </form>	  
				  <?php
			  }
		?>
      </div>
    </div>
  </div>


  
  <!--Upper for Member Add-->
</div>
              <div class="floated-chat-btn">
                <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
              </div>
              <div class="floated-chat-w">
                <div class="floated-chat-i">
                  <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                  </div>
                  <div class="chat-head">
                    <div class="user-w with-status status-green">
                      <div class="user-avatar-w">
                        <div class="user-avatar">
                          <img alt="" src="img/avatar1.jpg">
                        </div>
                      </div>
                      <div class="user-name">
                        <h6 class="user-title">
                          John Mayers
                        </h6>
                        <div class="user-role">
                          Account Manager
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="chat-messages">
                    <div class="message">
                      <div class="message-content">
                        Hi, how can I help you?
                      </div>
                    </div>
                    <div class="date-break">
                      Mon 10:20am
                    </div>
                    <div class="message">
                      <div class="message-content">
                        Hi, my name is Mike, I will be happy to assist you
                      </div>
                    </div>
                    <div class="message self">
                      <div class="message-content">
                        Hi, I tried ordering this product and it keeps showing me error code.
                      </div>
                    </div>
                  </div>
                  <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                      <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-panel">
              <div class="content-panel-close">
                <i class="os-icon os-icon-close"></i>
              </div><div class="element-wrapper">
  <h6 class="element-header">
    Latest Member List
  </h6>
  <div class="element-box-tp">
  <?php
  $mem="select * from user_login where usertype!=1";
  $memp=mysql_query($mem);
  if($memp=true)
   {
	   while($md=mysql_fetch_array($memp))
	         {
			  $uid=$md['0'];	
			  $name=$md['1'];	
              $pass=$md['2'];
			  $contact=$md['3'];
			  $email=$md['4'];
			  $utype=$md['5'];
			  $pic=$md['9'];
			  if(($pic=="")or(empty($pic)))
			  {
				  $pic="img/avatar1.jpg";
			  }
			  else
			  {
				$pic="Action/Profilepic/avatar1.jpg";  
			  }
			  $vstatus=$md['7'];
			  if($vstatus==1)
			  {
				  $vt="Active";
			  }
			  elseif($vstatus==0)
			  {
				  $vt="Inactive";
			  }
			  if($utype==1)
			    {
					$utitle="Admin";
				}
		     elseif($utype==2)
			   {
				 $utitle="Merchant";  
			   }
		     elseif($utype==3)
			   {
				 $utitle="User";  
			   }   
			  //Below For Last Login Details
			  $login_det="select logintime from userlog where loginid='' order by log_id desc limit 1";
			  $login_det_p=mysql_query($login_det);
			  if($login_det_p==true)
			      {
					  $loginpc=mysql_num_rows($login_det_p);
					  if($loginpc!=0)
					     {
							 while($ld=mysql_fetch_array())
							 {
							   $last_time=$ld['0'];	 
							 }
						 }
						 else
						 {
							  $last_time="Not Logged in Yet";	  
						 }
					  
				  }
			  //Upper For Last Login Details
			  ?>
    <div class="profile-tile">
      <div class="profile-tile-box">
        <div class="pt-avatar-w">
          <img alt="" src="img/avatar1.jpg">
        </div>
        <div class="pt-user-name">
         <?php echo $name ?>
        </div>
      </div>
      <div class="profile-tile-meta">
        <ul>
          <li>
            User Type :<strong><?php echo $utitle?></strong>
          </li>
          <li>
            Last Login Time:<strong><?php echo $last_time?></strong>
          </li>

        </ul>
        <div class="pt-btn">
          <a class="btn btn-success btn-sm" href="#">Send Message</a>
        </div>
      </div>
    </div>  
			  <?php
			  
			 }
   }
  
  ?>



  </div>
</div>

            </div>
          </div>
        </div>